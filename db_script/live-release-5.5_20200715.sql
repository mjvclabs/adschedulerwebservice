CREATE TABLE `play_list_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Actual_end_time` datetime NOT NULL,
  `Actual_start_time` datetime NOT NULL,
  `Cluster_priority` int(11) NOT NULL,
  `Comment` varchar(255) NOT NULL,
  `conflicting_schedules` tinyblob NOT NULL,
  `Date` datetime NOT NULL,
  `Play_cluster` int(11) NOT NULL,
  `Play_order` int(11) NOT NULL,
  `Retry_count` int(11) NOT NULL,
  `Schedule_end_time` datetime NOT NULL,
  `Schedule_hour` int(11) NOT NULL,
  `Schedule_start_time` datetime NOT NULL,
  `sequence_num` int(11) NOT NULL,
  `Status` varchar(255) NOT NULL,
  `TimeBelt_end_time` datetime NOT NULL,
  `TimeBelt_start_time` datetime NOT NULL,
  `Advert_id` int(11) NOT NULL,
  `Channel_id` int(11) NOT NULL,
  `Schedule_id` int(11) NOT NULL,
  `Work_order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY  (`Advert_id`) REFERENCES advertisement_def(`Advert_id`),
  FOREIGN KEY  (`Channel_id`) REFERENCES channel_detail(`Channel_id`),
  FOREIGN KEY  (`Schedule_id`) REFERENCES schedule_def(`Schedule_id`),
  FOREIGN KEY (`Work_order_id`) REFERENCES work_order(`Work_order_id`)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `advert_setting` ADD COLUMN `lap` int(11);
ALTER TABLE `advert_setting` ADD COLUMN `type` int(11);

CREATE TABLE `play_list_temp_sequence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Schedule_start_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pages` (`page_name`) VALUES ('PlaylistLapSetting');
INSERT INTO `authorities` (`authority`, `username`) VALUES ('ROLE_PlaylistLapSetting', 'SU');

INSERT INTO `pages` (`page_name`) VALUES ('PlaylistView');
INSERT INTO `authorities` (`authority`, `username`) VALUES ('ROLE_PlaylistView', 'SU');

SET SQL_SAFE_UPDATES=0;
UPDATE `advert_setting` SET `type`='0' WHERE `type` is null;
SET SQL_SAFE_UPDATES=1;