ALTER TABLE play_list_history
ADD COLUMN lable INT(11) NULL DEFAULT NULL AFTER conflicting_schedules;

UPDATE play_list_history SET lable=0 WHERE Work_order_id !=0 and Schedule_id !=1;
UPDATE play_list_history SET lable=1 WHERE Work_order_id=0 and Schedule_id=1;
UPDATE play_list_history SET lable=2 WHERE Work_order_id=1 and Schedule_id=1;