CREATE DEFINER=`vcluser`@`127.0.0.1` TRIGGER `vcl_schedule_db`.`schedule_def_AFTER_UPDATE` AFTER UPDATE ON `schedule_def` FOR EACH ROW
BEGIN
	IF NEW.Channel_id <> OLD.Channel_id THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Channel_id', OLD.Channel_id, NEW.Channel_id, NEW.user, NOW());
	END IF;
	IF NEW.Advert_id <> OLD.Advert_id THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Advert_id', OLD.Advert_id, NEW.Advert_id, NEW.user, NOW());
	END IF;
	IF NEW.Work_order_id <> OLD.Work_order_id THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Work_order_id', OLD.Work_order_id, NEW.Work_order_id, NEW.user, NOW());
	END IF;
    IF NEW.Schedule_start_time <> OLD.Schedule_start_time THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Schedule_start_time', OLD.Schedule_start_time, NEW.Schedule_start_time, NEW.user, NOW());
	END IF;
	IF NEW.Schedule_end_time <> OLD.Schedule_end_time THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Schedule_end_time', OLD.Schedule_end_time, NEW.Schedule_end_time, NEW.user, NOW());
	END IF;
	IF NEW.Status <> OLD.Status THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Status', OLD.Status, NEW.Status, NEW.user, NOW());
	END IF; 
    IF (NEW.Priority_id IS NULL AND OLD.Priority_id IS NOT NULL) THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Priority_id', OLD.Priority_id, 'NULL', NEW.user, NOW());
	END IF;
    IF (OLD.Priority_id IS NULL AND NEW.Priority_id IS NOT NULL) THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Priority_id', 'NULL', NEW.Priority_id, NEW.user, NOW());
	END IF;
    IF (NEW.Priority_id <> OLD.Priority_id) THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'Priority_id', OLD.Priority_id, NEW.Priority_id, NEW.user, NOW());
	END IF;
	IF NEW.month <> OLD.month THEN  
		INSERT into vcl_schedule_db.general_audit (table_name, record_id, field, old_data, new_data, user, date_and_time) 
		VALUES ('schedule_def', OLD.Schedule_id ,'month', OLD.month, NEW.month, NEW.user, NOW());
	END IF;
END


CREATE DEFINER=`vcluser`@`127.0.0.1` TRIGGER `vcl_schedule_db`.`play_list_AFTER_UPDATE` AFTER UPDATE ON `play_list` FOR EACH ROW
BEGIN
	UPDATE vcl_schedule_db.schedule_def 
	   SET Status = NEW.Status, Actual_start_time = NEW.Actual_start_time, Actual_end_time = NEW.Actual_end_time
	   WHERE Schedule_id = NEW.Schedule_id;
END