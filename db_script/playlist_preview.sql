INSERT INTO `pages` (`page_name`) VALUES ('PlaylistView');
INSERT INTO `authorities` (`authority`, `username`) VALUES ('ROLE_PlaylistView', 'SU');

CREATE TABLE `play_list_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Actual_end_time` datetime NOT NULL,
  `Actual_start_time` datetime NOT NULL,
  `Cluster_priority` int(11) NOT NULL,
  `Comment` varchar(255) NOT NULL,
  `conflicting_schedules` tinyblob NOT NULL,
  `Date` datetime NOT NULL,
  `Play_cluster` int(11) NOT NULL,
  `Play_order` int(11) NOT NULL,
  `Retry_count` int(11) NOT NULL,
  `Schedule_end_time` datetime NOT NULL,
  `Schedule_hour` int(11) NOT NULL,
  `Schedule_start_time` datetime NOT NULL,
  `sequence_num` int(11) NOT NULL,
  `Status` varchar(255) NOT NULL,
  `TimeBelt_end_time` datetime NOT NULL,
  `TimeBelt_start_time` datetime NOT NULL,
  `Advert_id` int(11) NOT NULL,
  `Channel_id` int(11) NOT NULL,
  `Schedule_id` int(11) NOT NULL,
  `Work_order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKd8fgdmm0brhr9yc2uqcfbfqci` (`Advert_id`),
  KEY `FK1l5fuc99w60lrcbnlccnkja8l` (`Channel_id`),
  KEY `FKghgyr3p5rgwodm1lgwm7y6w1h` (`Schedule_id`),
  KEY `FKnxujnb19ir8ubjrgb9q7te76x` (`Work_order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

CREATE TABLE `play_list_temp_sequence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Schedule_start_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;