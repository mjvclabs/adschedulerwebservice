insert into `pages` (`page_name`) values ('TagFillers');
insert into`authorities` (`authority`, `username`) values ('ROLE_TagFillers', 'SU');

CREATE TABLE `filler_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advert_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3ck7n2qjkthhe1uelfcs9cq3` (`advert_id`),
  KEY `FK1v6sipmjjao6e0qmiyf1q1moi` (`channel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

insert into filler_tag(advert_id, channel_id)
select ad.Advert_id as advert_id, cd.Channel_id as channel_id from advertisement_def ad right join channel_detail cd on 1 = 1
where ad.Advert_type = 'FILLER' and ad.status = 1 and ad.enabled = 1 and ad.Advert_id != 1
and ad.Advert_id != 0 and ad.status != 3 order by cd.Channel_id