ALTER TABLE advertisement_def
ADD COLUMN lastModifydate DATETIME NOT NULL DEFAULT '2018-12-01 00:00:00' AFTER suspend_date,
ADD COLUMN vedioType VARCHAR(255) NOT NULL DEFAULT 'SD' AFTER lastModifydate;

ALTER TABLE attachments
ADD COLUMN description VARCHAR(255) NULL DEFAULT 'No description' AFTER type;

ALTER TABLE channel_detail
ADD COLUMN opsChannelId INT(11) NOT NULL AFTER last_update;

ALTER TABLE client_details
ADD COLUMN status INT(11) NOT NULL AFTER vat_no;

ALTER TABLE work_order
ADD COLUMN autoStatus INT(11) NOT NULL DEFAULT 0 AFTER woType,
ADD COLUMN lastModifyDate DATETIME NOT NULL DEFAULT '2018-12-01 00:00:00' AFTER autoStatus,
ADD COLUMN manualStatus INT(11) NOT NULL DEFAULT 0 AFTER lastModifyDate,
ADD COLUMN revenueMonth VARCHAR(255) NOT NULL DEFAULT 'not set' AFTER manualStatus,
ADD COLUMN systemTypeype INT(11) NOT NULL DEFAULT 0 AFTER revenueMonth;

ALTER TABLE work_order
ADD COLUMN inventoryType INT(11) NULL AFTER systemTypeype;


UPDATE client_details SET status='2' WHERE Client_id>='1' and Enabled=0;
UPDATE work_order SET Client='4', Bill_client='4' WHERE Work_order_id='2';

UPDATE work_order SET inventoryType='1' WHERE Work_order_id>='0';
