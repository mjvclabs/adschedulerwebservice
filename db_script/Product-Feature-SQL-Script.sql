INSERT INTO `vcl_schedule_db`.`pages` (`page_name`) VALUES ('CreateProduct');
INSERT INTO `vcl_schedule_db`.`pages` (`page_name`) VALUES ('UpdateProduct');
INSERT INTO `vcl_schedule_db`.`pages` (`page_name`) VALUES ('AllProduct');


INSERT INTO `vcl_schedule_db`.`authorities` (`authority`, `username`) VALUES ('ROLE_CreateProduct', 'SU');
INSERT INTO `vcl_schedule_db`.`authorities` (`authority`, `username`) VALUES ('ROLE_UpdateProduct', 'SU');
INSERT INTO `vcl_schedule_db`.`authorities` (`authority`, `username`) VALUES ('ROLE_AllProduct', 'SU');
