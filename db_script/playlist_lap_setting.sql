INSERT INTO `pages` (`page_name`) VALUES ('PlaylistLapSetting');
INSERT INTO `authorities` (`authority`, `username`) VALUES ('ROLE_PlaylistLapSetting', 'SU');

SET SQL_SAFE_UPDATES=0;
UPDATE `advert_setting` SET `type`='0' WHERE `type` is null;
SET SQL_SAFE_UPDATES=1;