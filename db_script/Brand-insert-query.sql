/*
-- Query: SELECT * FROM vcl_schedule_db.brand
LIMIT 0, 50000

-- Date: 2019-08-29 16:35
*/
INSERT INTO `brand` (`id`,`name`) VALUES (1,'POP PASPANGUWA');
INSERT INTO `brand` (`id`,`name`) VALUES (2,'4EVER');
INSERT INTO `brand` (`id`,`name`) VALUES (3,'A BAURS');
INSERT INTO `brand` (`id`,`name`) VALUES (4,'MENTHOLATUM ACNES');
INSERT INTO `brand` (`id`,`name`) VALUES (5,'OXY');
INSERT INTO `brand` (`id`,`name`) VALUES (6,'CALTEX');
INSERT INTO `brand` (`id`,`name`) VALUES (7,'MIHIRA');
INSERT INTO `brand` (`id`,`name`) VALUES (8,'RASA');
INSERT INTO `brand` (`id`,`name`) VALUES (9,'SINGAPORE');
INSERT INTO `brand` (`id`,`name`) VALUES (10,'APRILIA');
INSERT INTO `brand` (`id`,`name`) VALUES (11,'ABANS');
INSERT INTO `brand` (`id`,`name`) VALUES (12,'HAIER');
INSERT INTO `brand` (`id`,`name`) VALUES (13,'HONEYWELL');
INSERT INTO `brand` (`id`,`name`) VALUES (14,'LG');

INSERT INTO `product` VALUES (1,'BODY WASH'),(2,'FACE WASH'),(3,'AIRLINES'),(4,'WOMENS RANGE'),(5,'CORPORATE');