# PlayListDAOImpl

SELECT p.* FROM play_list WHERE p.Channel_id=?1 and p.Date=?2;
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = p.Advert_id WHERE p.Schedule_hour=?1 AND p.Status=0 AND cd.Channel_id =?2 AND aid.Advert_type = 'ADVERT';
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = p.Advert_id WHERE p.Schedule_hour=?1 AND p.Status=0 AND cd.Channel_id =?2 AND (aid.Advert_type= 'CRAWLER' OR aid.Advert_type= 'V_SHAPE' OR aid.Advert_type= 'L_SHAPE');
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = p.Advert_id WHERE p.Schedule_hour=?1 AND p.Status=0 AND cd.Channel_id =?2 AND aid.Advert_type = 'LOGO';
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id WHERE p.Date=startDate AND p.Date<endDate AND p.Schedule_hour=?2 AND (Status=1 OR Status=2 OR Status=3) AND cd.Channel_id =?1;
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id WHERE p.Date=startDate AND p.Date<endDate AND p.Schedule_hour=?4 AND p.TimeBelt_start_time>=?2 AND p.TimeBelt_end_time<=?3 AND (Status=1 OR Status=2 OR Status=3) AND cd.Channel_id =?1;
SELECT p.* FROM play_list WHERE Playlist_id=?1;

DELETE FROM play_list;
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = p.Advert_id WHERE p.Schedule_hour=?2 AND p.Status=2 AND cd.Channel_id =?1 AND p.lable=0 AND aid.Advert_type = 'ADVERT';
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def advertisement ON advertisement.Advert_id = p.Advert_id WHERE (p.Status!=9 AND p.Status!=6) AND (advertisement.File_Available=0 OR advertisement.enabled=0) AND p.Schedule_end_time>?2 AND p.Schedule_start_time<?3 AND p.Schedule_hour<=?4 AND  cd.Channel_id =?1; 
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = p.Advert_id WHERE p.Schedule_hour=?4 AND p.Schedule_end_time>?2 AND p.Schedule_start_time<?3 AND p.Status=0 AND cd.Channel_id =?1 AND aid.Advert_type= 'ADVERT'; 
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = p.Advert_id WHERE p.Schedule_hour=?4 AND p.Schedule_end_time>?2 AND p.Schedule_start_time<?3 AND p.Status=0 AND cd.Channel_id =?1 AND (aid.Advert_type= 'CRAWLER' OR aid.Advert_type= 'V_SHAPE' OR aid.Advert_type= 'L_SHAPE');  
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = p.Advert_id WHERE p.Schedule_end_time>?2 AND p.Schedule_start_time<?3 AND (p.Status!=9 OR p.Status!=6 OR p.Status!=8) AND cd.Channel_id =?1 AND aid.Advert_type= 'LOGO'; 


# PlayListScheduleDAOimp

SELECT p.* FROM play_list;
SELECT count(p.*) FROM play_list;
SELECT p.* FROM play_list limit=?1 AND offset=?2;
SELECT p.* FROM play_list WHERE Date=?1 ORDER BY channel ASC, Schedule_start_time ASC;
SELECT p.* FROM play_list p JOIN work_order wo ON p.Work_order_id=wo.Work_order_id WHERE wo.Work_order_id=?1 ORDER BY channel ASC, Schedule_start_time ASC;
DELETE FROM play_list WHERE Schedule_id=?1;

DELETE FROM play_list WHERE Playlist_id=?1;
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id=cd.Channel_id JOIN work_order wo ON p.Work_order_id=wo.Work_order_id JOIN advertisement_def _advert ON p.Advert_id=_advert.Advert_id WHERE p.Comment<>'filler_generated' AND Status!=6 AND cd.Channel_id=?1 AND wo.Work_order_id=?2 AND _advert.Advert_id=?3 AND _advert.Advert_type=?4 AND (p.Date>=?5 AND p.Date<=?6) AND (p.Schedule_hour>=?7 AND p.Schedule_hour<=?8) ORDER BY Play_cluster ASC, Play_order ASC, Schedule_hour DESC, Schedule_start_time DESC, Schedule_end_time DESC;
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id=cd.Channel_id JOIN work_order wo ON p.Work_order_id=wo.Work_order_id JOIN advertisement_def _advert ON p.Advert_id=_advert.Advert_id WHERE p.Comment<>'filler_generated' AND Status!=6 AND cd.Channel_id=?1 AND wo.Work_order_id=?2 AND _advert.Advert_id=?3 AND (_advert.Advert_type=?4 OR _advert.Advert_type='FILLER') AND (p.Date>=?5 AND p.Date<=?6) AND (p.Schedule_hour>=?7 AND p.Schedule_hour<=?8) ORDER BY Play_cluster ASC, Play_order ASC, Schedule_hour DESC, Schedule_start_time DESC, Schedule_end_time DESC
SELECT p.* FROM play_list p JOIN channel_detail cd ON p.Channel_id=cd.Channel_id JOIN work_order wo ON p.Work_order_id=wo.Work_order_id WHERE p.Comment<>'filler_generated' AND Status!=6 AND cd.Channel_id=?1 AND wo.Work_order_id=?2 ORDER BY Play_cluster ASC, Play_order ASC;

# SchedulerDAOimp

SELECT sdd.* FROM schedule_def;
SELECT sdd.* FROM schedule_def sdd WHERE sdd.Schedule_id IN (SELECT Schedule_id FROM schedule_def sd LEFT JOIN work_order wo ON sd.Work_order_id = wo.Work_order_id WHERE sd.Status != 6 AND wo.Work_order_id = ?1) ORDER BY sdd.Channel_id, sdd.Schedule_start_time, sdd.Schedule_end_time, sdd.Advert_id, sdd.Date;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE wo.Permission_status != 3 AND (sdd.Date>=?1 AND sdd.Date<=?2) AND (sdd.Status = 5 OR sdd.Status = 8 OR sdd.Status = 9) AND cd.Enabled = 1 AND cd.Channel_id = ?3 AND aid.Advert_id = ?4 ORDER BY sdd.Channel_id, sdd.Schedule_start_time, sdd.Schedule_end_time, sdd.Advert_id ASC;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE wo.Work_order_id = ?1 ORDER BY sdd.Status, sdd.Date ASC;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE wo.Work_order_id = ?1 AND sdd.Status != 6 AND sdd.Status != 9 AND aid.Advert_id = ?2;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE wo.Work_order_id = ?1 AND sdd.Date = ?2;
SELECT sdd.* FROM schedule_def sdd WHERE sdd.Date = ?1 ORDER BY sdd.Channel_id, sdd.Schedule_start_time ASC;


SELECT sdd.* FROM schedule_def sdd WHERE sdd.Schedule_id = ?1;
SELECT sdd.* FROM schedule_def sdd JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE aid.Advert_id = ?1 AND sdd.Date IN (SELECT MAX(sdd.Date) FROM schedule_def);
SELECT sdd.* FROM schedule_def sdd JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id JOIN client_details cnt ON cnt.Client_id = aid.Client_id WHERE cnt.Client_id = ?1 AND sdd.Date IN (SELECT MAX(sdd.Date) FROM schedule_def);
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE wo.Work_order_id = ?1 AND (SELECT COUNT(*) AS scheduleCount FROM schedule_def);
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE wo.Work_order_id = ?1 AND cd.Channel_id=?2 AND sdd.Status!=6 AND sdd.Status!=9 AND (SELECT COUNT(*) AS slotCount FROM schedule_def);
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE wo.Work_order_id = ?1 AND sdd.Status=2 AND (SELECT COUNT(sdd.*) AS slotCount FROM schedule_def);
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE wo.Work_order_id = ?1 AND sdd.Status=2;
SELECT sdd.* FROM schedule_def sdd JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE sdd.Status!=6 AND sdd.Status!=9 AND (sdd.Date>=?1 AND sdd.Date<=?2) ORDER BY sdd.Channel_id, Date, Actual_start_time, Actual_end_time ASC;
SELECT sdd.* FROM schedule_def sdd RIGHT JOIN (SELECT Channel_id, Schedule_id AS sid, Date, Actual_end_time AS ast, Schedule_start_time AS sst FROM vcl_schedule_db.schedule_def WHERE (Date BETWEEN ?1 AND ?2) AND (Status = 5 OR Status = 8) UNION SELECT Channel_id, Schedule_id AS sid, Date, Actual_start_time AS ast, Schedule_start_time AS sst FROM schedule_def WHERE (Date BETWEEN ?3 AND ?4) AND Status NOT IN (5, 8, 9, 0) UNION SELECT scd.Channel_id, scd.Schedule_id AS sid, scd.Date, DATE_FORMAT(tmb.start_time, \"%H:%i:%s\") AS ast, Schedule_start_time AS sst FROM schedule_def scd LEFT JOIN time_slot_schedule_map tsm ON scd.Schedule_id = tsm.Schedule_id LEFT JOIN time_belts tmb ON tmb.time_belt_id = tsm.Time_belt_id WHERE (scd.Date BETWEEN ?3 AND ?4) AND scd.Status = 0 ORDER BY Channel_id ASC, Date ASC, sst ASC, ast ASC) AS a ON sd.Schedule_id = a.sid;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE sdd.Work_order_id=?1 AND sdd.Status!=6 AND sdd.Status!=9 AND (sdd.Date>=?1 AND sdd.Date<=?2) ORDER BY sdd.Channel_id, Date, Actual_start_time ASC;
SELECT sdd.* FROM schedule_def sd RIGHT JOIN (SELECT Channel_id, Schedule_id AS sid, Date, Actual_end_time AS ast, Schedule_start_time AS sst FROM vcl_schedule_db.schedule_def WHERE Work_order_id= ?1 AND (Date BETWEEN ?2 AND ?3) AND (Status = 5 OR Status = 8) UNION SELECT Channel_id, Schedule_id AS sid, Date, Actual_start_time AS ast, Schedule_start_time AS sst FROM schedule_def WHERE Work_order_id= ?4 AND (Date BETWEEN ?5 AND ?6) AND Status NOT IN (5, 6, 8, 9, 0) UNION SELECT scd.Channel_id, scd.Schedule_id AS sid, scd.Date, DATE_FORMAT(tmb.start_time, "%H:%i:%s") AS ast, Schedule_start_time AS sst FROM schedule_def scd LEFT JOIN time_slot_schedule_map tsm ON scd.Schedule_id = tsm.Schedule_id LEFT JOIN time_belts tmb ON tmb.time_belt_id = tsm.Time_belt_id WHERE scd.Work_order_id= ?4 AND (scd.Date BETWEEN ?5 AND ?6) AND scd.Status = 0 ORDER BY Channel_id ASC, Date ASC, sst ASC, ast ASC) AS a ON sd.Schedule_id = a.sid;
SELECT sdd.* FROM schedule_def sdd JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE sdd.Channel_id=?1 AND sdd.Status!=6 AND sdd.Status!=9 AND (sdd.Date>=?2 AND sdd.Date<=?3) ORDER BY sdd.Channel_id, Date, Actual_start_time, Actual_end_time ASC;
SELECT sdd.* FROM schedule_def sd RIGHT JOIN (SELECT Channel_id, Schedule_id AS sid, Date, Actual_end_time AS ast, Schedule_start_time AS sst FROM vcl_schedule_db.schedule_def WHERE Channel_id= ?1 AND (Date BETWEEN ?2 AND ?3) AND (Status = 5 OR Status = 8) UNION SELECT Channel_id, Schedule_id AS sid, Date, Actual_start_time AS ast, Schedule_start_time AS sst FROM schedule_def WHERE Channel_id= ?4 AND (Date BETWEEN ?5 AND ?6) AND Status NOT IN (5, 6, 8, 9, 0) UNION SELECT scd.Channel_id, scd.Schedule_id AS sid, scd.Date, DATE_FORMAT(tmb.start_time, "%H:%i:%s") AS ast, Schedule_start_time AS sst FROM schedule_def scd LEFT JOIN time_slot_schedule_map tsm ON scd.Schedule_id = tsm.Schedule_id LEFT JOIN time_belts tmb ON tmb.time_belt_id = tsm.Time_belt_id WHERE scd.Channel_id= ?4 AND (scd.Date BETWEEN ?5 AND ?6) AND scd.Status = 0 ORDER BY Channel_id ASC, Date ASC, sst ASC, ast ASC) AS a ON sd.Schedule_id = a.sid;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE sdd.Work_order_id=?2 AND sdd.Channel_id=?1 AND sdd.Status!=6 AND sdd.Status!=9 AND (sdd.Date>=?3 AND sdd.Date<=?4) ORDER BY sdd.Channel_id, Date, Actual_start_time, Actual_end_time ASC;
SELECT sdd.* FROM schedule_def sd RIGHT JOIN (SELECT Channel_id, Schedule_id AS sid, Date, Actual_end_time AS ast, Schedule_start_time AS sst FROM vcl_schedule_db.schedule_def WHERE Channel_id= ?1 AND Work_order_id= ?2 AND (Date BETWEEN ?3 AND ?4) AND (Status = 5 OR Status = 8) UNION SELECT Channel_id, Schedule_id AS sid, Date, Actual_start_time AS ast, Schedule_start_time AS sst FROM schedule_def WHERE Channel_id= ?5 AND Work_order_id= ?6 AND (Date BETWEEN ?7 AND ?8) AND Status NOT IN (5, 6, 8, 9, 0) UNION SELECT scd.Channel_id, scd.Schedule_id AS sid, scd.Date, DATE_FORMAT(tmb.start_time, "%H:%i:%s") AS ast, Schedule_start_time AS sst FROM schedule_def scd LEFT JOIN time_slot_schedule_map tsm ON scd.Schedule_id = tsm.Schedule_id LEFT JOIN time_belts tmb ON tmb.time_belt_id = tsm.Time_belt_id WHERE scd.Channel_id= ?5 AND scd.Work_order_id= ?6 AND (scd.Date BETWEEN ?7 AND ?8) AND scd.Status = 0 ORDER BY Channel_id ASC, Date ASC, sst ASC, ast ASC) AS a ON sd.Schedule_id = a.sid;
SELECT sdd.* FROM schedule_def sdd JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Channel_id=?1 AND (sdd.Status!=6 AND sdd.Status!=9) AND aid.Advert_type="ADVERT" OR (aid.Advert_type="CRAWLER" OR aid.Advert_type="V_SHAPE" OR aid.Advert_type="L_SHAPE") OR aid.Advert_type="LOGO" OR (aid.Advert_type="ADVERT" OR aid.Advert_type="CRAWLER" OR aid.Advert_type="V_SHAPE" OR aid.Advert_type="L_SHAPE") OR (aid.Advert_type="ADVERT" OR (aid.Advert_type="LOGO" OR aid.Advert_type="CRAWLER" OR aid.Advert_type="V_SHAPE" OR aid.Advert_type="L_SHAPE") AND sdd.Date=?2 AND sdd.Schedule_start_time>=?4;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Work_order_id=?1 AND sdd.Status!=6 AND sdd.Status!=9 AND aid.AdvertId=?2;
SELECT sdd.* FROM schedule_def sdd JOIN priority_def pd ON sdd.Priority_id = pd.priority_id WHERE sdd.Priority_id=?1 AND sdd.Date BETWEEN ?2 AND ?3 ORDER BY sdd.Date ASC;
SELECT sdd.* FROM schedule_def sdd JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE aid.Advert_id=?1 AND (sdd.Status=0 OR sdd.Status=1 OR sdd.Status=3 OR sdd.Status=7);
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE sdd.Work_order_id=?1 AND sdd.Date>=?2 AND sdd.Status=0;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Work_order_id=?1 AND sdd.Advert_type="ADVERT" AND sdd.Status!=6 AND sdd.Status!=9;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Work_order_id=?1 AND sdd.Status!=6 AND sdd.Status!=9 AND cd.Channel_id=?3 AND aid.Duration=?2 ORDER BY sdd.Date, sdd.Actual_start_time ASC;
SELECT sdd.* FROM schedule_def sdd JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Advert_id=?1 AND sdd.Status=0;
SELECT sdd.* FROM schedule_def sdd JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Advert_id=?1 AND sdd.Status=9 AND sdd.Comment="Advertisement Suspend";
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Work_order_id=?1 AND sdd.Date>=?2 AND sdd.Advert_id=?3 AND sdd.Advert_type="ADVERT" AND (sdd.Status!=6 AND sdd.Status!=9);
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Work_order_id=?1 AND sdd.Advert_id=?2 AND sdd.Advert_type="ADVERT" AND (sdd.Status!=6 AND sdd.Status!=9);


SELECT sdd.* FROM schedule_def sdd JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Channel_id=?1 AND sdd.Status!=6 AND aid.Advert_id=?4 sdd.Date=?2 AND sdd.Schedule_start_time>=?3;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE sdd.Work_order_id=?1 AND sdd.Status!=6 AND sdd.Date=?2 AND sdd.Schedule_start_time>=?3;
SELECT sdd.* FROM schedule_def WHERE Schedule_id IN ?1;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE sdd.Work_order_id=?1 AND sdd.Status!=6 AND sdd.Status!=9 AND (SELECT COUNT(*) AS slotCount FROM schedule_def);
SELECT sdd.* FROM schedule_def sdd JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE sdd.Channel_id=?1 AND sdd.Date=?2 AND (sdd.Status!=6 OR sdd.Status!=9);
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE sdd.Work_order_id=?1 AND sdd.Advert_id=?2 AND sdd.Status!=6 AND (SELECT COUNT(*) AS slotCount FROM schedule_def);
SELECT sdd.* FROM schedule_def sdd JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE sdd.Channel_id=?1 AND sdd.Status!=6 AND sdd.Date>=?2 AND sdd.Date<=?3 ORDER BY cd.Channel_id, sdd.Date, sdd.Actual_start_time, Actual_end_time ASC;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id WHERE sdd.WorkOrder_id IN ?1 AND sdd.Status!=6 AND wo.Agency_client IN ?2 AND wo.Client=?3 ORDER BY cd.Channel_id, sdd.Date, sdd.Actual_start_time, sdd.Actual_end_time ASC;
SELECT sdd.* FROM schedule_def sdd JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE aid.Advert_id=?1 ORDER BY sdd.Date, sdd.Actual_start_time DESC;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE sdd.WorkOrder_id=?1 AND sdd.Status=6 AND sdd.Comment="Work order Hold";
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id JOIN channel_detail cd ON sdd.Channel_id = cd.Channel_id JOIN advertisement_def aid ON aid.Advert_id = sdd.Advert_id WHERE wo.Permission_status !=3 AND (sdd.Date>=?1 AND sdd.Date<=?2) AND (sdd.Status=5 OR sdd.Status=8 OR sdd.Status=9 OR sdd.Statsus=11) AND cd.Enabled=1 AND cd.Channel_id IN ?1 AND wo.Work_order_id IN ?2 AND sdd.Status=?3 AND aid.Advert_name LIKE ?4 AND aid.Advert_type=?5 ORDER BY sdd.Channel_id, sdd,Schedule_start_time, sdd.Schedule_end_time, sdd.Date ASC;
SELECT COUNT(*) FROM schedule_def sc LEFT JOIN time_slot_schedule_map tssm ON sc.Schedule_id=tssm.Schedule_id LEFT JOIN time_belts tb ON tssm.Time_belt_id=tb.time_belt_id WHERE sc.Work_order_id=%s AND sc.Status!=6 AND tb.start_time >='1970-01-01 18:00:00' AND tb.start_time < '1970-01-01 23:00:00';
SELECT COUNT(*) FROM schedule_def sc LEFT JOIN time_slot_schedule_map tssm ON sc.Schedule_id=tssm.Schedule_id LEFT JOIN time_belts tb ON tssm.Time_belt_id=tb.time_belt_id WHERE sc.Work_order_id=%s AND sc.Status!=6 AND tb.start_time >='1970-01-01 09:00:00' AND tb.start_time < '1970-01-01 18:00:00';
SELECT sd.* FROM schedule_def sd LEFT JOIN work_order wo ON sd.Work_order_id = wo.Work_order_id LEFT JOIN channel_detail cd ON sd.Channel_id = cd.Channel_id WHERE sd.Date=%s AND sd.status!=9 AND sd.status!=6 AND sd.status!=8 AND wo.Permission_status !=3 AND wo.Permission_status !=4 AND cd.Enabled=1;
SELECT sd.* FROM schedule_def sdLEFT JOIN work_order wo ON sd.Work_order_id = wo.Work_order_id LEFT JOIN channel_detail cd ON sd.Channel_id = cd.Channel_id WHERE sd.Date=%s AND sd.status='%d' AND wo.Permission_status !=3 AND wo.Permission_status !=4 AND cd.Enabled=1;
SELECT sd.* FROM schedule_def sd LEFT JOIN work_order wo ON sd.Work_order_id = wo.Work_order_id LEFT JOIN channel_detail cd ON sd.Channel_id = cd.Channel_id WHERE sd.Date='%s' AND sd.status='%d' AND cd.Channel_id='%d' AND wo.Permission_status !=3 AND wo.Permission_status !=4 AND cd.Enabled=1;
SELECT sd.* FROM schedule_def sd LEFT JOIN work_order wo ON wo.Work_order_id = sd.Work_order_id WHERE sd.Date=%s AND sd.Channel_id =%d AND (sd.Status=5 OR sd.Status=2) AND wo.Permission_status != 3;
SELECT sd.* FROM schedule_def sd LEFT JOIN work_order wo ON wo.Work_order_id = sd.Work_order_id WHERE sd.Date='%s' AND (sd.Status!=8 AND sd.Status!=2 AND sd.Status!=9 AND sd.Status!=6) AND wo.Permission_status != 3;
SELECT sdd.* FROM schedule_def sdd JOIN work_order wo ON sdd.Work_order_id = wo.Work_order_id WHERE wo.Work_order_id=?1 AND sdd.Status=6 AND sdd.Comment LIKE "Work order Hold";
SELECT sd.Work_order_id, sd.Channel_id, sd.Schedule_id, sd.Advert_id, sd.Date, sd.Schedule_start_time, sd.Schedule_end_time, sd.Actual_start_time, sd.Actual_end_time, ad.Advert_name FROM schedule_def sd LEFT JOIN advertisement_def ad ON sd.Advert_id = ad.Advert_id WHERE sd.Work_order_id = ?1 AND sd.Status = 2 AND sd.Channel_id = ?2 ORDER BY sd.Advert_id limit ?3 offset ?4;
SELECT sd.Work_order_id, sd.Channel_id, sd.Schedule_id, sd.Advert_id, sd.Date, sd.Schedule_start_time, sd.Schedule_end_time, sd.Actual_start_time, sd.Actual_end_time, ad.Advert_name FROM schedule_def sd LEFT JOIN advertisement_def ad ON sd.Advert_id = ad.Advert_id WHERE sd.Channel_id = ?1 AND sd.Date = ?2 AND sd.Status IN (2, 10) AND sd.Actual_start_time >= ?3 AND sd.Actual_start_time <= ?4;
SELECT COUNT(*) FROM schedule_def WHERE Work_order_id = ?1 AND Status = 2;
SELECT COUNT(*) FROM schedule_def where Work_order_id = ?1 AND Status = 10;


# Indexing - PlayList

CREATE INDEX Date_idx ON `vcl_schedule_db`.`play_list`(Date);
CREATE INDEX Schedule_hour_idx ON `vcl_schedule_db`.`play_list`(Schedule_hour);
CREATE INDEX Status_idx ON `vcl_schedule_db`.`play_list`(Status);
CREATE INDEX TimeBelt_start_time_idx ON `vcl_schedule_db`.`play_list`(TimeBelt_start_time);
CREATE INDEX TimeBelt_end_time_idx ON `vcl_schedule_db`.`play_list`(TimeBelt_end_time);
CREATE INDEX Schedule_end_time_idx ON `vcl_schedule_db`.`play_list`(Schedule_end_time);


# Indexing - Schedule def

CREATE INDEX Schedule_id_idx ON `vcl_schedule_db`.`schedule_def`(Schedule_id);
CREATE INDEX Schedule_start_time_idx ON `vcl_schedule_db`.`schedule_def`(Schedule_start_time);
CREATE INDEX Schedule_end_time_idx ON `vcl_schedule_db`.`schedule_def`(Schedule_end_time);
CREATE INDEX Actual_start_time_idx ON `vcl_schedule_db`.`schedule_def`(Actual_start_time);
CREATE INDEX Actual_end_time_idx ON `vcl_schedule_db`.`schedule_def`(Actual_end_time);
CREATE INDEX Date_idx ON `vcl_schedule_db`.`schedule_def`(Date);
CREATE INDEX Status_idx ON `vcl_schedule_db`.`schedule_def`(Status);