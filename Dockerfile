FROM jetty:9.3-jre8
ADD ./target/nash.war /var/lib/jetty/webapps/root.war
EXPOSE 8080