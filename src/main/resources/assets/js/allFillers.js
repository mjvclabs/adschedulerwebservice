var fetchDone;

$(document).ready(function () {
    loadingOpen();
    fetchDone = _.after(2, function(){
        loadAdvert();
        loadingClose();
    });
    setClientDrop();
    loadChannels();
});


async function loadChannels() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            for (var i in data) {
                $('#dd_channel').append('<option value="' + data[i].channelid + '">' + data[i].channelname + '</option>');
            }
            fetchDone();
        },
        error: function (jqXHR, textStatus, thrownError) {
            alert("Error has occurred while fetching channels.");
            fetchDone();
        }
    });
}

async function setClientDrop() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getAllClientOrderByName(),
        success: function (data)
        {
            closeDialog();
            var clientDrop = '<span class="input-group-addon">Clients</span><select id="client_list" name="client_id" class="form-control">';
            clientDrop += '<option VALUE="-111">--All clients--</option>';
            for (var i in data)
            {
                var isValidClientName = /.*[a-zA-Z]+.*/.test(data[i].clientname);
                if (data[i].clienttype == 'Client' && isValidClientName) {
                    clientDrop += '<option VALUE="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
            }
            clientDrop += '</select>';
            $('#clientList_div').html(clientDrop);
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error has occurred while fetching clients.");
            fetchDone();
        }
    });
}

function loadAdvert() {
    var searchResultsTable = $('#all_advertisement_table').DataTable({
        "bFilter": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": URI.fillerTag.getAllFillers(),
            "type": 'GET',
            "data": function (d) {
                var table = $('#all_advertisement_table').DataTable();
                d.page = (table != undefined) ? table.page.info().page : 0;
                d.size = (table != undefined) ? table.page.info().length : 5;
                d.advertId = numberValidation($("#adverId").val()) ? $("#adverId").val() : '';
                d.advertName = $("#adverName").val();
                d.clientId = $("#client_list").val();
                d.channelId = $('#dd_channel').val();
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.totalElements;
                json.recordsFiltered = json.totalElements;
                json.data = json.content;
                return JSON.stringify(json);
            }
        },
        "columns": [
            {
                "width": "5%",
                "data": "advertid",
                "orderable": false
            },
            {"width": "25%",
                "data": "advertname",
                "orderable": false
            },
            {"width": "5%",
                "data": "language",
                "orderable": false,
                "render": function (data) {
                    return  setLanguage(data);
                }
            },
            {
                "width": "20%",
                "data": "client.clientname",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "createDate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "expireDate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "suspendDate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "statustName",
                "orderable": false
            },
            {
                "width": "10%",
                "orderable": false,
                "render": function (data, type, full) {
                    return  $('<Button/>').attr('title', 'View').attr('type', 'submit').attr('id', full.advertid)
                    .attr('onclick', 'viewTagAdvert(this.id)').attr('class', 'btn btn_color')
                    .attr('data-toggle', 'modal').append($('<span>Tag</span>')).wrap('<div/>').parent().html();
                }
            }
        ],
        "createdRow": function (row, data, index) {
            if (!data.fileAvailable) {
                //Dummy advert
                $('td', row).addClass('dummy-avert');
            } else if (data.enabled === 0 && data.status === 1) {
                //Closed workOrder
                $('td', row).addClass('expir-avert');
            } else if (data.enabled === 0 && data.status === 2) {
                $('td', row).addClass('suspend-avert');
            }
        }

    });

    $("#btn-search").click(function () {
        searchResultsTable.ajax.reload();
    });
}

function viewTagAdvert(advertId) {
    window.open(URI.fillerTag.tagFillerPageUrl(advertId));
}
