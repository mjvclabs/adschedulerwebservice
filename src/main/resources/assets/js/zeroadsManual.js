var channelAdvertmap = [];
var newAdvertIdMap = {};
var dataTableLoaded = false;
$(document).ready(function () {
 setAllChannels();
});
function setChannelAdvertData(channelID) {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.zero.getZeroAdsManualPlayListUrl(),
        data: {
            channelId: channelID
        },
        success: function (data)
        {
            channelAdvertmap = data;
            buildTable();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

//Delete Channel_ads_map table selected row
function deleteChannelAdvertMapRecorde(id)
{
    var ret = confirm("Do you want to delete this row?");
    if (!ret) {
        return;
    } else {
        for (var key in channelAdvertmap) {
            if (channelAdvertmap[key].zeroAdsId === stoi(id)) {
                delete channelAdvertmap[key];
            }
        }
        buildTable();
    }
}

function buildTable() {

    var unicId = 0;
    var table = '<thead><tr><th>ID</th><th>Advertisement</th><th>Order</th><th></th></tr></thead><tbody>';
    for (var key in channelAdvertmap) {
        table += '<tr><td>' + channelAdvertmap[key].advertId + '</td>';
        table += '<td>' + channelAdvertmap[key].advertName + '</td>';
        table += '<td><input id="' + channelAdvertmap[key].channelId + '_' + channelAdvertmap[key].advertId + '_'+ unicId +'" type="text" class="txt-input font-A-14" value="' + channelAdvertmap[key].orderNum + '" onkeyup="setOrderNumber(this.id)"></td>';
        table += '<td><button id="' + channelAdvertmap[key].zeroAdsId + '" onclick="deleteChannelAdvertMapRecorde(this.id)"><span class="glyphicon glyphicon-remove"></span></button></td>';
        table += '</tr>';
         channelAdvertmap[key].tempId = unicId;
        unicId++;
    }
    table += ' </tbody>';
    $('#tbl_zero_ads_manual').html(table);
}

function saveData() {
    channelAdvertmapJSON = [];
    var channelId = $('#channel_list').val();
    for (var zeroAdsKey in channelAdvertmap) {
        var data = {zeroAdsId: channelAdvertmap[zeroAdsKey].zeroAdsId, channelId: channelAdvertmap[zeroAdsKey].channelId, channelName: channelAdvertmap[zeroAdsKey].channelName, advertId: channelAdvertmap[zeroAdsKey].advertId, advertName: channelAdvertmap[zeroAdsKey].advertName, orderNum: channelAdvertmap[zeroAdsKey].orderNum};
        channelAdvertmapJSON.push(data);
    }
  //  if (channelAdvertmapJSON.length != 0) {

        var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
        var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

        $.ajax({
            beforeSend: function (request) {
                   request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            type: 'POST',
            dataType: "json",
            url:URI.zero.saveZeroAdsManualPlayListUrl(),
            data: {
                channelData: JSON.stringify(channelAdvertmapJSON),
                channelId: channelId
            },
            success: function (data)
            {
                if (data) {
                    viewMessageToastTyped("successfully", "success");
                    setChannelAdvertData(channelId);
                }
                closeDialog();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("Login.html");
                }
                else {
                    alert("Error");
                }
            }
        });
//    } else {
//        viewMessageToastTyped("No data", "success");
//    }
}

function setAllChannels() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url:  URI.channeldetail.getAllEnabledChannelsUrl(),
        success: function (data)
        {
            for (var i in data) {
                $('#channel_list').append($('<option/>', {
                    value: data[i].channelid,
                    text: data[i].channelname
                }));
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function setChannelId() {
    var channelId = $('#channel_list').val();
    //if (channelId !== 0) {
    setChannelAdvertData(channelId);
    //  }
}

function setAllAdvert() {
    showDialog();
    newAdvertIdMap = {};
    var clientID = "-111";
    var adverName = "-111";
    var advertCategory = "-111";
    var advertID =  "-111";
    var f_data = clientID + "," + advertCategory + "," + adverName + "," + advertID;

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementFilterForZeroManualUrl(),
        data: {
            fiterData: f_data
        },
        success: function (data)
        {
            var numRows = 0;
            var trHTML = '<thead><tr><th>ID</th><th>Advertisement</th><th>Type</th><th></th></tr></thead><tbody>';
            for (var i in data)
            {
//                var advrtData = advertIdMap[data[i].advertid];
//                if (advrtData == null) {
                newAdvertIdMap[data[i].advertid] = data[i].advertname;
                trHTML += '<tr id="' + data[i].advertid + '_row">';
                trHTML += '<td>' + data[i].advertid + '</td>';
                trHTML += '<td>' + data[i].advertname + '</td>';
                trHTML += '<td>' + getAdvertType(data[i].adverttype) + '</td>';
                trHTML += '<td><input id="' + data[i].advertid + '" type = "checkbox" name = "checkbox" value = ""></td>';
                trHTML += '</tr>';
//                }
            }
            trHTML += '</tbody>';
            $('#tbl_all_advert').html(trHTML);
            setDataTable();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            } else {
                alert("Error");
            }
        }
    });

}

function setDataTable() {
    if(dataTableLoaded){
        $('#tbl_all_advert').DataTable().destroy();
    }
    dataTableLoaded = true;
    $('#tbl_all_advert').DataTable(
    {   "bFilter": false,
        "responsive": true,
        "processing": true,
    });
}

function advertNameChange() {

    var clientID = "-111";
    var adverName = $("#txt_search").val();
    var advertCategory = "-111";
    if (adverName == "" || adverName == " ") {
        adverName = "-111"
    }
    var advertID = $("#txt_advertId").val();
    if (advertID == "" || advertID == " ") {
        advertID = "-111"
    }
    var f_data = clientID + "," + advertCategory + "," + adverName + "," + advertID;
    getFilterData(f_data);
}

function getFilterData(p_fiterData) {
    showDialog();
    newAdvertIdMap = {};
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementFilterForZeroManualUrl(),
        data: {
            fiterData: p_fiterData
        },
        success: function (data)
        {
            var numRows = 0;
            var trHTML = '<thead><tr><th>ID</th><th>Advertisement</th><th>Type</th><th></th></tr></thead><tbody>';
            for (var i in data)
            {
//                var advrtData = advertIdMap[data[i].advertid];
//                if (advrtData == null) {
                newAdvertIdMap[data[i].advertid] = data[i].advertname;
                trHTML += '<tr id="' + data[i].advertid + '_row">';
                trHTML += '<td>' + data[i].advertid + '</td>';
                trHTML += '<td>' + data[i].advertname + '</td>';
                trHTML += '<td>' + getAdvertType(data[i].adverttype) + '</td>';
                trHTML += '<td><input id="' + data[i].advertid + '" type = "checkbox" name = "checkbox" value = ""></td>';
                trHTML += '</tr>';
//                }
            }
            trHTML += '</tbody>';
            $('#tbl_all_advert').html(trHTML);
            closeDialog();
            setDataTable();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function addAdvert() {
    for (var advertkey in newAdvertIdMap) {
        if (document.getElementById(advertkey)!=null && document.getElementById(advertkey).checked) {
            var channelId = $('#channel_list').val();
            if (channelId !== '-1') {
//                var alredyAdded = false;
//                for (var zeroAdsKey in channelAdvertmap) {
//                    if (advertkey == channelAdvertmap[zeroAdsKey].advertId) {
//                        alredyAdded = true;
//                    }
//                }
//                if (!alredyAdded) {
                    var data = {zeroAdsId: 0, channelId: channelId, channelName: "", advertId: advertkey, advertName: newAdvertIdMap[advertkey], orderNum: 0};
                    channelAdvertmap.push(data);
//                } else {
//                    viewMessageToastTyped("This adevtesement is alredy added", "info");
//                }
            } else {
                viewMessageToastTyped("Please select the channel", "info");
            }
            buildTable();
        }
    }
}

function setOrderNumber(id) {

    var advertId = id.split("_")[1];
    var channelId = id.split("_")[0];
    var tempId = id.split("_")[2];
    var value = $('#' + id).val();

    for (var AdsKey in channelAdvertmap) {
        if (channelAdvertmap[AdsKey].orderNum == stoi(value) && value != "") {
            viewMessageToastTyped("This order number has already added", "info");
             //$('#' + id).val("");
            return;
        }
    }

    for (var zeroAdsKey in channelAdvertmap) {
        if (advertId == channelAdvertmap[zeroAdsKey].advertId && channelId == channelAdvertmap[zeroAdsKey].channelId && tempId == channelAdvertmap[zeroAdsKey].tempId) {

            if (stoi(value) == value) {
                channelAdvertmap[zeroAdsKey].orderNum = value;
            } else {
                viewMessageToastTyped("Please enter number", "info");
            }
        }
    }
}