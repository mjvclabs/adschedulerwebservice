$(document).ready(function () {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementCategoryAllListUrl(),
        success: function (data)
        {
            var numRows = 0;

            var trHTML = '';
            trHTML += '<table id="advrt_category_channel_main" class="table table-bordered table-workorder">';
            trHTML += '<thead class="tb_align_center"><tr><th>Id</th><th>Category</th></tr></thead>';
            trHTML += '<tbody>';

            for (var i in data)
            {
                trHTML += '<tr class="tb_align_center"> <td><a href="/admin/advertisement-category/update" id="' + data[i].advertCategoryId + '~' + data[i].category + '" onclick="getId(this.id)">' + data[i].advertCategoryId + '</a></td><td>' + data[i].category + '</td></tr>';

            }
            trHTML += '</tbody>';
            trHTML += '</table>';

            $('#advrt_category_main_table').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

});

function getId(id)
{
    // Check browser support
    if (typeof (Storage) !== "undefined") {
        //Store
        localStorage.setItem("Advrt_category_id", id);
    }
}
