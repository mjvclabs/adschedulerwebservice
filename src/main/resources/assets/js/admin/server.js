$(document).ready(function () {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.servers.serverdetailsUrl(),
        success: function (data)
        {
            closeDialog();
            var table_data = '';
            $.each(data, function (index, item) {
                table_data += '<tr>';
                table_data += '<td><a href="/admin/server/update?id=' + item.serverId + '">' + item.serverId + '</a></td>';
                table_data += '<td>' + item.serverIP + '</td>';
                table_data += '<td>' + item.serverPort + '</td>';
                table_data += '<td>' + item.cardPort + '</td>';
                table_data += '<td>' + (item.inCard + 1) + '</td>';
                table_data += '<td>' + (item.outCard + 1) + '</td>';
                table_data += '</tr>';
            });
            $('#server_main tbody').html(table_data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

});
