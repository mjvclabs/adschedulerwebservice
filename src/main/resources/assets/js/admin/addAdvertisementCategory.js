function addAdvertCategory()
{
    var v_category = document.getElementById("txt_advert_category").value;

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementCategorySaveUrl(),
        data: {
            category: v_category,
        },
        success: function (data)
        {
            if (data.flag) {
                document.getElementById("txt_advert_category").value = '';
                window.location = URI.advertisement.advertisementCategoryHomePageUrl();
            }else{
                alert(data.message);
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function addAdvertCategoryAfterValidation(form) {
    if(validateInputFields(form)) {
        addAdvertCategory();
    }
}

$( function() {
    var allAdvertisements = [];
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementCategoryAllListUrl(),
        success: function (data){
            for(var i in data){
                allAdvertisements.push(data[i].category);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
    $( "#txt_advert_category" ).autocomplete({
        source: allAdvertisements
    });
} );