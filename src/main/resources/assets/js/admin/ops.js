function importDeal() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.ops.importDealUrl(),
        success: function (data)
        {
            closeDialog();
            alert(data + " deals are imported.");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });

}

function importCustomSchedule() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.ops.importCustomSchedulelUrl(),
        success: function (data)
        {
            closeDialog();
            alert(data + " deals are imported.");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });

}

function importBidItem() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.ops.importBidItemUrl(),
        success: function (data)
        {
            closeDialog();
            alert(data + " deals are imported.");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });

}

function importPromotion() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.ops.importPromotionUrl(),
        success: function (data)
        {
            closeDialog();
            alert(data + " deals are imported.");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });

}
