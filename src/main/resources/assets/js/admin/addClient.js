function addClient()
{
    var client_name = document.getElementById("txt_client_name").value;
    var client_type = $("#client_typeDropdown").val();
    var address = document.getElementById("txt_address").value;
    var email = document.getElementById("txt_email").value;
    var vat = document.getElementById("txt_vat_num").value;
    var contact_person = document.getElementById("txt_contact_person").value;
    var tel_num = document.getElementById("txt_tel_num").value;
    var cx_cade = document.getElementById("txt_cx_cade").value;
    var details = document.getElementById("txt_details").value;
    var balack_list = '0';
    if (document.getElementById('blackList_radio_yes').checked) {
        balack_list = 2;
    } else {
        balack_list = 1;
    }
    if (client_type == '') {
        hasError($("#client_typeDropdown"), true, 'Please Select Client Type');
        return;
    } else {
        hasError($("#client_typeDropdown"), false, '');
    }

    var v_clientData = '{"clientId":0,"clientName":"' + client_name + '","clientType":"' + client_type + '","address":"' + address + '","email":"' + email + '","vat":"' + vat + '","contact_person":"' + contact_person + '","telephone_num":"' + tel_num + '","cx_cade":"' + cx_cade + '","contact_details":"' + details + '","balack_list":' + balack_list + '}';

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.saveAndUpdateClientUrl(),
        data: {
            clientData: v_clientData,
        },
        success: function (data)
        {
            if (data != -1) {
                document.getElementById("txt_client_name").value = '';
                document.getElementById("client_typeDropdown").selectedIndex = 0;
                document.getElementById("txt_address").value = '';
                document.getElementById("txt_email").value = '';
                document.getElementById("txt_tel_num").value = '';
                document.getElementById("txt_details").value = '';
                /*if (typeof(Storage) !== "undefined") {
                 //Store
                 localStorage.setItem("client_id", data);
                 window.location=web_page_path+"/Admin/UpdateClient.html";
                 }*/
                window.location = URI.client.clientListPageUrl();
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

/*document.getElementById('file').onchange = function(){
 
 var file = this.files[0];
 
 var reader = new FileReader();
 reader.onload = function(progressEvent){
 // Entire file
 console.log(this.result);
 
 // By lines
 var lines = this.result.split('\n');
 for(var line = 0; line < lines.length; line++){
 console.log(lines[line]);
 }
 };
 reader.readAsText(file);
 };*/

$("document").ready(function () {

    $("#files").change(function () {
        alert('changed!');
        var file = this.files[0];

        var reader = new FileReader();
        reader.onload = function (progressEvent) {
            // Entire file
            console.log(this.result);

            // By lines
            var lines = this.result.split('\n');
            var count = 1;
            for (var line = 0; line < lines.length; line++) {
                console.log(lines[line]);
                var client = lines[line].split('\t');
                var clientName = client[0];
                clientName = clientName.replace(/["']/g, "");
                var address = client[1];
                address = address.replace(/["']/g, "");

                var v_clientData = '{"clientId":0,"clientName":"' + clientName + '","clientType":"Client","address":"' + address + '","email":"","vat":"","contact_person":"","telephone_num":"","cx_cade":"","contact_details":"","balack_list":1 }';
                console.log(clientName);
                console.log(v_clientData);
                console.log(client.length);
                console.log(client[0]);
                console.log(count);
                count++;

                $.ajax({
                    type: 'GET',
                    dataType: "json",
                    contentType: 'application/json',
                    url: URI.client.saveAndUpdateClientUrl(),
                    data: {
                        clientData: v_clientData,
                    },
                    success: function (data)
                    {
                        if (data != -1) {
                            document.getElementById("txt_client_name").value = '';
                            document.getElementById("client_typeDropdown").selectedIndex = 0;
                            document.getElementById("txt_address").value = '';
                            document.getElementById("txt_email").value = '';
                            document.getElementById("txt_tel_num").value = '';
                            document.getElementById("txt_details").value = '';
                            /*if (typeof(Storage) !== "undefined") {
                             //Store
                             localStorage.setItem("client_id", data);
                             window.location=web_page_path+"/Admin/UpdateClient.html";
                             }*/
                        }
                        closeDialog();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        closeDialog();
                        if (errorThrown == 'Unauthorized') {
                            localStorage.clear();
                            window.location.replace("../Login.html");
                        }
                        else {
                            alert("Error");
                        }
                    }
                })
            }
        };
        reader.readAsText(file);

    });
});

function addClientAfterValidation(form) {
    if(validateInputFields(form)){
        addClient();
    }
}

$( function() {
    var allClients = [];
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getAllClientUrl(),
        success: function (data){
            for(var i in data){
                allClients.push(data[i].clientname);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
    $( "#txt_client_name" ).autocomplete({
        source: allClients
    });
} );