var flag = 0;
var iSelectedDateDif = 0;
var iWorkOrderId = -111;
var iChannelId = -111;
var iAdvertId = -111;
var iType = '-111';
var iStart = -111;
var iEnd = -111;

var timepicke = false;

$(function () {
    $('#date-selecter').datetimepicker({
        pickTime: false,
    });
});

$(document).ready(function () {
    WorkOrderDetailsCreator();
    TypeDetailCreator();
    document.getElementById("setected-date").value = getDate();
});
var datePickeDate = '';
function goToDate() {
    datePickeDate = document.getElementById("setected-date").value;
    if(datePickeDate!=''){
        timepicke = true;
        CreateTableBody();
    }else{
        viewMessageToastTyped("Please select date", "Info");
    }
}

function getDate()
{
    var result = new Date();
    result.setDate(result.getDate() + iSelectedDateDif);

    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    date = yyyy + '/' + mm + '/' + dd;
    return yyyy + '/' + mm + '/' + dd;
}

function clickNextDay()
{
    iSelectedDateDif++;
    timepicke = false;
    CreateTableBody();
}

function clickPreviousDay()
{
    iSelectedDateDif--;
    timepicke = false;
    CreateTableBody();
}

function CreateTableBody()
{
    var date = '';
    if (timepicke) {
        date = datePickeDate;
    } else {
        date = getDate();
    }
    console.log('date :' + date);
    $('#lbe_day').html(date);
    if (iWorkOrderId == -111 && iChannelId == -111 && iAdvertId == -111) {
        var table = '<table class="table table-bordered table-workorder"><thead><tr><th>Time Belt</th><th>Channel Name</th><th>Advert Name</th><th>Client</th><th>Scheduled time</th><th>Aired time</th><th>Duration(Sec)</th><th>Cluster</th><th>Type</th><th>Status</th></tr></thead>';
        table += '</tbody></table>';
        $('#schedule_main_table').html(table);
        return;
    }
    showDialog();
    if($('#channelDropdown_admin').val() == null){
        iChannelId = "-111";
    }
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getOneDaySchedulesViewUrl(),
        data: {
            workOrderId: iWorkOrderId,
            channelId: iChannelId,
            advertId: iAdvertId,
            advertType: iType,
            date: date,
            startHour: iStart,
            endHour: iEnd
        },
        success: function (data)
        {
            var table = '<table class="table table-bordered table-workorder"><thead><tr><th class="tb_align_center">Time Belt</th><th class="tb_align_center">Channel Name</th><th class="tb_align_center">Advert Name</th><th class="tb_align_center">Client</th><th class="tb_align_center">Scheduled time</th><th class="tb_align_center">Shifted from</th><th class="tb_align_center">Aired time</th><th class="tb_align_center">Duration(Sec)</th><th class="tb_align_center">Cluster</th><th class="tb_align_center">Type</th><th class="tb_align_center">Status</th></tr></thead>';
            for (var i in data)
            {
                var time_belt = RemoveSec(data[i].scheduleStartTime) + ' - ' + RemoveSec(data[i].scheduleEndTime);
                var itemList = data[i].scheduleItems;
                itemList = getSortedList(itemList, data[i].scheduleStartTime, data[i].scheduleEndTime);
                for (j in itemList)
                {
                    if (time_belt != '')
                        table += '<tr class="time-belt-border">';
                    else
                        table += '<tr>';

                    var cell_class = 'data-cell';
                    if (itemList[j].status == 'Played')
                        cell_class = 'data-cell-green';
                    else if (itemList[j].status == 'Stopped')
                        cell_class = 'data-cell-violet';
                    else if (itemList[j].status == 'Scheduled')
                        cell_class = 'data-cell-orange';
                    else if (itemList[j].status == 'Expired' || itemList[j].status == 'Timed Out' || itemList[j].status == 'Suspended') {
                        cell_class = 'data-cell-gray';
                    }
                    else if (itemList[j].status == 'StoppedByAdmin' || itemList[j].status == 'No Media'){
                        cell_class = 'data-cell-red';
                    }else if (itemList[j].status == 'Skipped'){
                        cell_class = 'data-cell-red';
                    }

                    var startEndtime = itemList[j].startAndEndTime.split("-");
                    var cluster = '';
                    if (itemList[j].clusterNum != -1) {
                        cluster = itemList[j].clusterNum + 1;
                    }
                    var status = itemList[j].status;
                    if (status == 'Played') {
                        status = 'Aired';
                    }
                    var timeRange = RemoveSec(startEndtime[0]) + '-' + RemoveSec(startEndtime[1]);
                    if (itemList[j].advertType == 'FILLER') {
                        timeRange = '';
                    }

                    var initTime = "";
                    if (itemList[j].initialStartEndTime != null) {
                        timeString = itemList[j].initialStartEndTime.replace(/1970-01-01 /g, '');
                        spltTimeString = timeString.split("-");
                        initTime = RemoveSec(spltTimeString[0]) + '-' + RemoveSec(spltTimeString[1]);
                    }

                    if (itemList[j].isFileAvailable == 0 && cell_class != 'data-cell-red') {

                        table += '<td class="time-belt-cell tb_align_center"><b>' + time_belt + '</b></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + itemList[j].channelName + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + itemList[j].advertname + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + itemList[j].client + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + timeRange + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + initTime + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + itemList[j].palyTime + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + itemList[j].duration + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + cluster + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + getAdvertType(itemList[j].advertType) + '</font></td>';
                        table += '<td class="' + cell_class + ' tb_align_center"><font color="red">' + status + '</font></td>';
                        table += '</tr>';
                    } else {

                        table += '<td class="time-belt-cell tb_align_center"><b>' + time_belt + '</b></td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + itemList[j].channelName + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + itemList[j].advertname + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + itemList[j].client + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + timeRange + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + initTime + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + itemList[j].palyTime + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + itemList[j].duration + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + cluster + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + getAdvertType(itemList[j].advertType) + '</td>';
                        table += '<td class="' + cell_class + ' tb_align_center">' + status + '</td>';
                        table += '</tr>';

                    }

                    time_belt = '';
                }
            }

            table += '</tbody></table>';
            $('#schedule_main_table').html(table);
            //$('#lbe_day').html(date);

            if(iChannelId == null){
                $('#channelDropdown_admin').val("-111");
            }else{
                $('#channelDropdown_admin').val(iChannelId);
            }

            $('#AdvertimentDropdown_admin').val(iAdvertId);
            if($('#AdvertimentDropdown_admin').val()==null){
                $('#AdvertimentDropdown_admin').val("-111");
                iAdvertId=-111;
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function getSortedList(playListItems, startTime, endTime) {
    var defaultTimeBelt = startTime + '-' + endTime;
    //remove zero advert which are not aired
    var plItems = _.filter(playListItems, function(item) {
        var isZeroAdvert = "Zero advert work order" == item.workOrderId;
        if(!isZeroAdvert){
            return true;
        }else if(item.status == 'Scheduled' || item.status == 'Played') {
            return true;
        }
        return false;
    });

    var clusterNumbers = _.sortBy(_.uniq(_.map(plItems, function(item) {
        return item.clusterNum;
    })));
    clusterNumbers = _.without(clusterNumbers, -1);
    if(clusterNumbers.length == 0){ //if adverts are not scheduled yet
        return playListItems;
    }
    var itemList = _.map(plItems, function(item) {
        var endTime = -1;
        if(item.palyTime != defaultTimeBelt){
            endTime = item.palyTime.split("-")[1];
        }
        return _.extend(item, { playEndTime: endTime });
    });

    var itemsWithTimeInfo =  _.filter(itemList, function(item) {
//        return item.playEndTime != -1;
        return (item.playEndTime != -1 || item.status == "Suspended");
    });
    itemsWithTimeInfo = _.sortBy(itemsWithTimeInfo, 'playEndTime');
    var itemsNoTimeInfo =  _.filter(itemList, function(item) {
        return item.playEndTime == -1;
    })

    _.forEach(clusterNumbers, function(cn) {
        var noTimeInfoCluster =  _.filter(itemsNoTimeInfo, function(item) {
            return item.clusterNum == cn;
        });
        if(noTimeInfoCluster.length > 0) {
            updateItemsWithTimeInfoPlayList(itemsWithTimeInfo, noTimeInfoCluster, cn);
        }
    })
    return itemsWithTimeInfo;
}

function updateItemsWithTimeInfoPlayList(itemsWithTimeInfo, noTimeInfoCluster, cn){
    var clusterNumArray = _.map(itemsWithTimeInfo, function(item){
        return item.clusterNum;
    })
    var lastIndex = itemsWithTimeInfo.length > 0 ? _.lastIndexOf(clusterNumArray, cn) : 0;
    if(lastIndex == -1) { //if no previous advertisements
        lastIndex = itemsWithTimeInfo.length;
    }
    _.forEach(noTimeInfoCluster, function(item) {
        lastIndex++;
        itemsWithTimeInfo.splice(lastIndex, 0, item);
    })
}

function WorkOrderDetailsCreator()
{
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderListJsonUrl(),
        success: function (data)
        {
            closeDialog();
            var trHTML = '';
            trHTML += ' <select id="workOrderDropdown_admin" name="WorkOrder" class="form-control" onchange="dropSelectionChange()">';
            trHTML += ' <option value="-111" > --------- None --------- </option>';
            for (var i in data)
            {
                trHTML += '<option value="' + data[i].workorderid + '">' + data[i].workorderid + '_' + data[i].ordername + '</option>';
            }
            trHTML += '</select>';
            trHTML += '<span id="snp_msg" class="text-danger"></span>';
            trHTML += '<span class="help-block"></span>';

            $('#workOrder_list_div').html(trHTML);
            $('#workOrderDropdown_admin').val(iWorkOrderId);

            ChannelDetailCreator(-111);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function ChannelDetailCreator(v_workOrderId)
{
    if (v_workOrderId != -111) {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.workOrder.workOrderChannelListUrl(),
            data: {
                workOrderId: v_workOrderId,
            },
            success: function (data)
            {
                closeDialog();
                var trHTML = '';
                trHTML += ' <select id="channelDropdown_admin" name="channelName" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option value="-111" > --------- All --------- </option>';
                for (var i in data)
                {
                    trHTML += '<option value="' + data[i].channelid + '">' + data[i].channelname + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#cheannel_list_div').html(trHTML);
                if(iChannelId == null){
                    $('#channelDropdown_admin').val("-111");
                }else{
                    $('#channelDropdown_admin').val(iChannelId);
                }
                AdvertDetailsCreator(v_workOrderId);
                closeDialog();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })
    }
    else {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.channeldetail.listOrderByNameUrl(),
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select id="channelDropdown_admin" name="channelName" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option value="-111" > --------- None --------- </option>';
                for (var i in data)
                {
                    trHTML += '<option value="' + data[i].channelid + '">' + data[i].channelname + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#cheannel_list_div').html(trHTML);
                AdvertDetailsCreator(v_workOrderId);
                closeDialog();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        });
    }
}

function AdvertDetailsCreator(v_workOrderId)
{
    console.time("concatenation");
    if (v_workOrderId != -111) {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.scheduler.clientOrderAdvertListUrl(),
            data: {
                workOrderId: v_workOrderId,
                advertType: 'All',
            },
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select select id="AdvertimentDropdown_admin" name="Advertiment" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option value="-111"> --------- All --------- </option>';
                for (var i in data)
                {
                    trHTML += '<option value="' + data[i].advertId + '">' +  data[i].advertId+"-"+data[i].advertName + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#advertisement_list_div').html(trHTML);
                $('#AdvertimentDropdown_admin').val(iAdvertId);


                closeDialog();
                CreateTableBody();

                console.timeEnd("concatenation");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })
    }
    else {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.advertisement.advertisementAllListOrderByNameUrl(),
            data: {
                workOrderId: v_workOrderId,
            },
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select select id="AdvertimentDropdown_admin" name="Advertiment" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option value="-111"> --------- None --------- </option>';
                for (var i in data)
                {
                    trHTML += '<option value="' + data[i].advertid + '">'+ data[i].advertid+'-' + data[i].advertname + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#advertisement_list_div').html(trHTML);
                closeDialog();
                CreateTableBody();

                console.timeEnd("concatenation");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })
    }
}

function TypeDetailCreator()
{
    var drop_type = $('#advert_type_list_div');

    var trHTML = '';
    trHTML += '<select select id="AdvertTypeDropdown_admin" name="Type" class="form-control" onchange="dropSelectionChange()">';
    trHTML += '<option value="-111"> --------- All --------- </option>';
    trHTML += '<option value="ADVERT">  TVC  </option>';
    trHTML += '<option value="L_SHAPE">  L-Squeeze  </option>';
    trHTML += '<option value="V_SHAPE">  V-Squeeze  </option>';
    trHTML += '<option value="CRAWLER">  Crawler-Overlay  </option>';
    trHTML += '<option value="LOGO">  Logo  </option>';

    trHTML += '</select><span class="help-block"></span>';
    drop_type.html(trHTML);

}

function dropSelectionChange()
{
    datePickeDate = document.getElementById("setected-date").value;
    if(datePickeDate==''){
        viewMessageToastTyped("Please select date", "Info")
        return;
    }
    iWorkOrderId = $('#workOrderDropdown_admin').val();
    if(iChannelId == null){
        iChannelId = $('#channelDropdown_admin').val("-111");
    }else{
        iChannelId = $('#channelDropdown_admin').val();
    }

    iAdvertId = $('#AdvertimentDropdown_admin').val();
    if(iAdvertId == null){
        $('#AdvertimentDropdown_admin').val("-111");
    }else{
        $('#AdvertimentDropdown_admin').val(iAdvertId);
    }
    iType = $('#AdvertTypeDropdown_admin').val();
    iStart = $('#start_hour').val();
    iEnd = $('#end_hour').val();

    console.log('iWorkOrderId :' + iWorkOrderId);
    console.log('iChannelId :' + iChannelId);
    console.log('iAdvertId :' + iAdvertId);
    console.log('iType :' + iType);
    console.log('iStart :' + iStart);
    console.log('iEnd :' + iEnd);

    ChannelDetailCreator(iWorkOrderId);
}

function generateExcel() {
    var date = '';
    if (timepicke) {
        date = datePickeDate;
    } else {
        date = getDate();
    }
    if (iWorkOrderId == -111 && iChannelId == -111 && iAdvertId == -111) {
        return;
    }
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.generateOneDayScheduleExcel(),
        data: {
            workOrderId: iWorkOrderId,
            channelId: iChannelId,
            advertId: iAdvertId,
            advertType: iType,
            date: date,
            startHour: iStart,
            endHour: iEnd
        },
        success: function (data)
        {
            closeDialog();
            window.location = URI.scheduler.downloadOneDayScheduelExcellUrl();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function changeStartHour() {

    var endHour=$('#end_hour').val();
    var startHour= $('#start_hour').val();

    if(stoi(startHour)>stoi(endHour)){
        $('#end_hour').val(startHour);
    }

    dropSelectionChange();

}

function changeEndHour() {

    var endHour=$('#end_hour').val();
    var startHour= $('#start_hour').val();

    if(stoi(startHour)>stoi(endHour)){
        $('#start_hour').val(endHour);
    }
    dropSelectionChange();
}