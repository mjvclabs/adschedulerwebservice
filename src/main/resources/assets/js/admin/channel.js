$(document).ready(function () {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url:URI.channeldetail.getAllChannelUrl(),
        success: function (data)
        {
            var numRows = 0;

            var trHTML = '';
            trHTML += '<table id="channel_main" class="table table-bordered table-workorder">';
            trHTML += '<thead class="tb_align_center"><tr><th>Id</th><th>Long name</th><th>Short name</th><th>Video format</th></tr></thead>';
            trHTML += '<tbody>';

            for (var i in data)
            {
                if(data[i].enabled)
                {
                    trHTML += '<tr class="tb_align_center"> <td><a href="/admin/channel/update" id="' + data[i].channelid + '" onclick="getId(this.id)">' + data[i].channelid + '</a></td><td>' + data[i].channelname + '</td><td>' + data[i].shortname + '</td><td>' + data[i].videoformat + '</td></tr>';

                }else{
                    trHTML += '<tr class="tb_align_center"> <td><a href="/admin/channel/update" id="' + data[i].channelid + '" onclick="getId(this.id)">' + data[i].channelid + '</a></td><td><font color="#D3D3D3">' + data[i].channelname + '</font></td><td><font color="#D3D3D3">' + data[i].shortname + '</font></td><td><font color="#D3D3D3">' + data[i].videoformat + '</font></td></tr>';

                }
                
            }
            trHTML += '</tbody>';
            trHTML += '</table>';

            $('#channel_main_table').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

});

function getId(id)
{
    // Check browser support
    if (typeof (Storage) !== "undefined") {
        //Store
        localStorage.setItem("Channel_id", id);
    }
}
