function addChannel()
{
    //validateInputFields(document.getElementsByTagName('new_channel_form'));
    var long_name = document.getElementById("txt_channel_long").value;
    var short_name = document.getElementById("txt_channel_short").value;
    var video_format = document.getElementById("txt_video_format").value;
    var remarks = document.getElementById("txt_remarks").value;
    var isManual = $('#rd_Manual').is(':checked');
    var manualDelay = stoi($('#txt_delay').val());
    var opsId = stoi($('#txt_ops_id').val());
    var logoPosition_x = $('#txt_logoPositon_x').val();
    var logoPosition_y = $('#txt_logoPositon_y').val();

    var v_channelData = '{"channelId":0,"longName":"' + long_name + '","shortName":"' + short_name 
            + '","vFormat":"' + video_format + '","remarks":"' + remarks 
            + '","isManual":' + isManual + ',"manualDelay":' + manualDelay 
            + ',"opsId":' + opsId+',"logoPosition":"' + logoPosition_x + 'x' + logoPosition_y
            + '"}';
    
    //Console.log(v_channelData);
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.saveAdnUpdateChannelUrl(),
        data: {
            channelData: v_channelData,
        },
        success: function (data)
        {
            if (data != -1) {
                document.getElementById("txt_channel_long").value = '';
                document.getElementById("txt_channel_short").value = '';
                document.getElementById("txt_video_format").value = '';
                document.getElementById("txt_remarks").value = '';
                window.location = URI.channeldetail.channelListPageUrl();
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function toggleFrameDelay(flag){
    if(flag)
        $('#id_manualdelay').show();
    else
        $('#id_manualdelay').hide();
}

function addChannelAfterValidation(form) {
    if(validateInputFields(form)){
          addChannel();
    }
}

$( function() {
    var allChannels = [];
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.getAllChannelUrl(),
        success: function (data){
            for(var i in data){
                allChannels.push(data[i].channelname);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
    $( "#txt_channel_long" ).autocomplete({
        source: allChannels
    });
} );