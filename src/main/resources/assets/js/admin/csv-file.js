function validateCSVFile() {
            var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
            var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

            $.ajax({
                beforeSend: function (request) {
                   request.setRequestHeader(csrf_headerName, csrf_paramValue);
                },
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.inventory.csvFileValidationURL(),
            success: function (data)
            {
                 alert(data.message);
                  closeDialog();
            },
            error: function (jqXHR, textStatus, thrownError) {
              closeDialog();
               alert("error");
            }
        })
}

var files;
var stTable='csv';
var iId='99';

function Attachment(table, id) {
    stTable = table;
    iId = id;
}
;

Attachment.prototype.createView = function (data) {
    //alert(data);
    //trHTML += '<tr > <td class="tb_align_center"><a href="UpdateClient.html" id="' + data[i].clientid + '" onclick="getId(this.id)">' + data[i].clientid + '</a></td><td class="tb_align_center">' + data[i].clientname + '</td><td class="tb_align_center">' + data[i].clienttype + '</td><td class="tb_align_center">' + data[i].address + '</td><td class="tb_align_center">' + data[i].emailaddress + '</td><td class="tb_align_center">' + data[i].telephonenum + '</td><td class="tb_align_center">' + data[i].contactdetails + '</td><td class="tb_align_center" style="width:20px;cursor: pointer;"><lebal onclick="attachmentView(' + data[i].clientid + ')">&#128206;</lebal></td></tr>';
    $('#attachment_view_popup').modal('show');
    loadAttachmentsList();
};


$(document).ready(function () {
    $("#fileupload").on('change', function (event) {
        setProgress(0);
        console.log(' event fired' + event.target.files[0].name);
        files = event.target.files;
    });


});

function fileUploading() {
    $('#state').addClass('glyphicon-upload');
    $("#state").css("color", "#1E90FF");
}

function fileUploaded() {
    $('#state').addClass('glyphicon-ok').removeClass('glyphicon-upload');
    $("#state").css("color", "#5cb85c");
}

function progressHandlingFunction(evt) {
    if (evt.lengthComputable) {
        var percentComplete = evt.loaded / evt.total;
        percentComplete = parseInt(percentComplete * 100);
        setProgress(percentComplete);
        console.log(percentComplete);
    }
}

function setProgress(value) {
    $('#progress .bar').css('width', value + '%');
    if (value === 100) {
        console.log("file successfully uploaded");
    }
}

function processFileUpload()
{
    if (files == undefined) {
        alert('Please Select a File.');
        return;
    }
    var description = "No description";
    if (document.getElementById("txt_description") != null) {
        description = document.getElementById("txt_description").value;
    }

    showDialog();
    console.log("file upload clicked");
    var oUploadForm = new FormData();
    oUploadForm.append("file", files[0]);
    oUploadForm.append("ref_table", "" + stTable);
    oUploadForm.append("ref_id", "" + iId);
    oUploadForm.append("description", description);

    fileUploading();

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");


    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", progressHandlingFunction, false);
            return xhr;
        },
        dataType: 'json',
        url: URI.attachments.uploadFileUrl(),
        data: oUploadForm,
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        success: function (data) {
            fileUploaded();
            closeDialog();
            var attachmentID = stoi(data);
            console.log(attachmentID);
            validateCSVFile();

            $('#fileupload').val('');
            files = undefined;
        },
        error: function (result) {
            closeDialog();
            alert('File Upload Error.');
        }
    });
}

