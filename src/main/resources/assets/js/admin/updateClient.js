$(document).ready(function () {
    var client_id = localStorage.getItem("client_id");
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getClientUrl(),
        data: {
            clientId: client_id,
        },
        success: function (data)
        {
            document.getElementById("txt_client_id").value = data.clientid;
            /*var client_id='<input type="text" class="form-control" id="txt_client_id" name="Client Id" value="'+data.clientid+'" required="" title="Please enter Client id"  readonly><span class="help-block"></span>';
             $('#Client_id_div').html(client_id);*/

            document.getElementById("txt_client_name").value = data.clientname;
            /*var client_name='<input type="text" class="form-control" id="txt_client_name" name="Client name" value="'+data.clientname+'" required="" title="Please enter Client name" placeholder="Client name"><span class="help-block"></span>';
             $('#client_name_div').html(client_name);*/

            if (data.clienttype == "Agency")
            {
                var clientType = document.getElementById("client_typeDropdown");
                clientType.selectedIndex = 2;
            }
            else {
                var clientType = document.getElementById("client_typeDropdown");
                clientType.selectedIndex = 1;
            }

            document.getElementById("txt_address").value = data.address;
            document.getElementById("txt_email").value = data.emailaddress;
            document.getElementById("txt_tel_num").value = data.telephonenum;
            document.getElementById("txt_details").value = data.contactdetails;
            document.getElementById("txt_vat_num").value = data.vat_no;
            document.getElementById("txt_contact_person").value = data.contact_person;
            document.getElementById("txt_cx_cade").value = data.cx_cade;

            if (data.enabled) {
                var button = '<label class="control-label col-sm-4 col-md-4 col-lg-3 radio_lable" ><input id="blackList_radio_yes" type="radio" name="optradio" value="1"/>Yes</label><label class="control-label ccol-sm-4 col-md-4 col-lg-3 radio_lable" ><input id="blackList_radio_no" type="radio" name="optradio" value="0" checked="checked"/>No</label>';
                $('#redio_button').html(button);
            } else {
                var button = '<label class="control-label col-sm-4 col-md-4 col-lg-3 radio_lable" ><input id="blackList_radio_yes" type="radio" name="optradio" value="1"  checked="checked"/>Yes</label><label class="control-label ccol-sm-4 col-md-4 col-lg-3 radio_lable" ><input id="blackList_radio_no" type="radio" name="optradio" value="0"/>No</label>';
                $('#redio_button').html(button);
            }

            if (data.canDelete) {
                   $("#client-remove-btn").removeAttr('disabled');
            } else {
                   $("#client-remove-btn").attr('disabled','disabled');
            }

            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    })
});

function updateClient()
{
    var client_id = document.getElementById("txt_client_id").value;
    var client_name = document.getElementById("txt_client_name").value;
    var client = document.getElementById("client_typeDropdown");
    var client_type = client.options[client.selectedIndex].value;
    var address = document.getElementById("txt_address").value;
    var email = document.getElementById("txt_email").value;
    var tel_num = document.getElementById("txt_tel_num").value;
    var details = document.getElementById("txt_details").value;
    var vat = document.getElementById("txt_vat_num").value;
    var contact_person = document.getElementById("txt_contact_person").value;
    var cx_cade = document.getElementById("txt_cx_cade").value;
    var balack_list = '0';
    if (document.getElementById('blackList_radio_yes').checked) {
        balack_list = 2;
    } else {
        balack_list = 1;
    }

    var v_clientData = '{"clientId":' + client_id + ',"clientName":"' + client_name + '","clientType":"' + client_type + '","address":"' + address + '","email":"' + email + '","vat":"' + vat + '","contact_person":"' + contact_person + '","telephone_num":"' + tel_num + '","cx_cade":"' + cx_cade + '","contact_details":"' + details + '","balack_list":' + balack_list + '}';

    showDialog();

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.saveAndUpdateClientUrl(),
        data: {
            clientData: v_clientData
        },
        success: function (data)
        {
            closeDialog();
            if (data != -1) {
                alert("Successfully client update");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function removeClient() {
    showDialog();
    var ret = confirm("Are you sure you want to delete this Client?");
    if (!ret){
        closeDialog();
        return;
    }

    var clientId = localStorage.getItem("client_id");
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.disableClientUrl(),
        data: {
            clientId: clientId
        },
        success: function (data)
        {
            closeDialog();
            window.location = URI.client.clientListPageUrl();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function updateClientAfterValidation(form) {
    if(validateInputFields(form)){
        updateClient();
    }
}