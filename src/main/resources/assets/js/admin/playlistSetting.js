var dsId;
$(document).ready(function () {
    loadChannels();
})

function loadChannels(){
    showDialog();
     $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.channelDetailUrl(),
        success: function (data)
        {
            var sortedChannels = _.sortBy(data, "channelname");
            $.each(sortedChannels, function (index, value) {
                $('#dd_channel').append($('<option/>', {
                    value: value.channelid,
                    text: value.channelname
                }));
            });
            closeDialog();
            loadSettings();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error has occurred while fetching data.");
        }
     })
}

function loadSettings(){
    var cid = $('#dd_channel').val();
    showDialog();
     $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.playlistSetting.loadByChannel(cid, 'MAX_ADVERT_COUNT'),
        success: function (data)
        {
            $('#setting_tb').empty();
            $.each(data, function (i, a) {
                var $tr = $('<tr>').attr({ class : 'tb_align_center'});
                $tr.append($('<td>').text(a.minLimit));
                $tr.append($('<td>').text(a.maxLimit));
                $tr.append($('<td>').text(a.maxSlots));
                //delete button column
                var $deleteBtn = $('<button title="View" type="submit" onclick="showDeleteSettingModal(this.id)" class="btn btn_readcolor" data-toggle="modal" data-target="#advert_view_pop">');
                $deleteBtn.attr({ id : a.id});
                $deleteBtn.append($('<span>Delete</span>'));
                var $btnCol = $('<td>');
                $btnCol.append($deleteBtn);
                $tr.append($btnCol);
                $('#setting_tb').append($tr);
            })
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error has occurred while fetching playlist setting data.");
        }
     })
}

function validateForm() {
    var checker = false;
    var hasMinLimit = false;
    var hasMaxLimit = false;

    if ($('#txt_min_limit').val() == '') {
        hasError($("#txt_min_limit"), true, 'Min limit can not be empty.');
        checker = true;
    }else if(!numberValidation($('#txt_min_limit').val())){
        hasError($("#txt_min_limit"), true, 'Min limit should be a number.');
        checker = true;
    }else{
        hasMinLimit = true;
    }


    if ($('#txt_max_limit').val() == '') {
       hasError($("#txt_max_limit"), true, 'Max limit can not be empty.');
       checker = true;
    }else if(!numberValidation($('#txt_min_limit').val())){
       hasError($("#txt_max_limit"), true, 'Max limit should be a number.');
       checker = true;
    }else{
        hasMaxLimit = true;
    }

    if(hasMinLimit && hasMaxLimit){
        if(parseInt($('#txt_min_limit').val()) >= parseInt($('#txt_max_limit').val())){
            hasError($("#txt_max_limit"), true, 'Max limit should be greater than min limit.');
            hasError($("#txt_min_limit"), true, 'Max limit should be greater than min limit.');
            checker = true;
        }
    }

    if ($('#txt_spot_count').val() == '') {
       hasError($("#txt_spot_count"), true, 'Spot count can not be empty.');
       checker = true;
    }else if(!numberValidation($('#txt_min_limit').val())){
       hasError($("#txt_spot_count"), true, 'Spot count should be a number.');
       checker = true;
    }

    if (checker) {
        return false;
    }
    return true;
}


function numberValidation(vlaue) {
    return !isNaN(vlaue);
}

function pListSettingSave(){
    if (!validateForm()){
        closeDialog();
        return;
    }
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    showDialog();
    var postData = {
        'channelDetails'    : { 'channelid' : $('#dd_channel').val()},
        'minLimit'          : $('#txt_min_limit').val(),
        'maxLimit'          : $('#txt_max_limit').val(),
        'maxSlots'          : $('#txt_spot_count').val(),
        'type'              : 'MAX_ADVERT_COUNT',
        'lap'               : -1,
    }
    $.ajax({
        url:    URI.playlistSetting.save(),
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: JSON.stringify(postData),
        processData: false,
        type: 'post',
        success: function (data){
            closeDialog();
            resetFields();
            viewMessageToastTyped("Playlist setting has been successfully saved.", "Success");
            loadSettings();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if(jqXHR.status == 412){
                viewMessageToastTyped("An existing setting conflicts with the new setting.", "Error");
            }
        }
    });
}

function resetFields(){
    hasError($("#txt_min_limit"), false);
    hasError($("#txt_max_limit"), false);
    hasError($("#txt_spot_count"), false);
    $("#txt_min_limit").val('');
    $("#txt_max_limit").val('');
    $("#txt_spot_count").val('');

}

function showDeleteSettingModal(id) {
    dsId = id;
    $('#dsModal').modal('toggle');
}

function deleteSetting(){
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    showDialog();
    var postData = {
        'id' : dsId
    }
    $.ajax({
        url:    URI.playlistSetting.deleteSetting(),
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: JSON.stringify(postData),
        processData: false,
        type: 'post',
        success: function (data){
            closeDialog();
            viewMessageToastTyped("Playlist setting has been successfully deleted.", "Success");
            loadSettings();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            viewMessageToastTyped("Error has occurred while deleting the setting.", "Error");
        }
    });
}