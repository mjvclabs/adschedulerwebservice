$(document).ready(function () {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getAllClientUrl(),
        success: function (data)
        {
            var numRows = 0;

            var trHTML = '';
            trHTML += '<table id="client_main" class="table table-bordered table-workorder">';
            trHTML += '<thead class="tb_align_center"><tr><th>Id</th><th>Name</th><th>Type</th><th>Address</th><th>Email</th><th>Contact</th><th>Details</th><th style="width:20px"></th></tr></thead>';
            trHTML += '<tbody>';

            for (var i in data)
            {
                if(data[i].enabled){
                    trHTML += '<tr > <td class="tb_align_center"><a href="/admin/client/update" id="' + data[i].clientid + '" onclick="getId(this.id)">' + data[i].clientid + '</a></td><td class="tb_align_center">' + data[i].clientname + '</td><td class="tb_align_center">' + data[i].clienttype + '</td><td class="tb_align_center">' + data[i].address + '</td><td class="tb_align_center">' + data[i].emailaddress + '</td><td class="tb_align_center">' + data[i].telephonenum + '</td><td class="tb_align_center">' + data[i].contactdetails + '</td><td class="tb_align_center" style="width:20px;cursor: pointer;"><lebal onclick="attachmentView('+ data[i].clientid + ')">&#128206;</lebal></td></tr>';
                }else{
                    trHTML += '<tr > <td class="tb_align_center"><a href="/admin/client/update" id="' + data[i].clientid + '" onclick="getId(this.id)">' + data[i].clientid + '</a></td><td class="tb_align_center"><font color="#D3D3D3">' + data[i].clientname + '</font></td><td class="tb_align_center"><font color="#D3D3D3">' + data[i].clienttype + '</font></td><td class="tb_align_center"><font color="#D3D3D3">' + data[i].address + '</font></td><td class="tb_align_center"><font color="#D3D3D3">' + data[i].emailaddress + '</font></td><td class="tb_align_center"><font color="#D3D3D3">' + data[i].telephonenum + '</font></td><td class="tb_align_center"><font color="#D3D3D3">' + data[i].contactdetails + '</font></td><td class="tb_align_center" style="width:20px;cursor: pointer;"><lebal onclick="attachmentView('+ data[i].clientid + ')">&#128206;</lebal></td></tr>';
                }
                
            }
            trHTML += '</tbody>';
            trHTML += '</table>';

            $('#client_main_table').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

});

function getId(id)
{
    // Check browser support
    if (typeof (Storage) !== "undefined") {
        //Store
        localStorage.setItem("client_id", id);
    }
}

function attachmentView(id){   
    var attachmentPopUp = new Attachment('ClientDetails', id);
    attachmentPopUp.createView(id);
}
