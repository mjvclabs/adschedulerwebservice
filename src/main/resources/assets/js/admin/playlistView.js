$(document).ready(function() {
    setChannelData();
    $('#from_date, #to_date').datepicker({
        format: 'yyyy-mm-dd'
    });
    var fromDate = moment().startOf('year').format('YYYY-MM-DD');
    var toDate = moment().startOf('today').format('YYYY-MM-DD');
})

function setChannelData(){
    $.ajax({
        url: URI.channeldetail.listOrderByNameUrl(),
        dataType: 'json',
        contentType: 'application/json',
        processData: false,
        type: 'get',
        success: function( districts, textStatus, jQxhr ){
            districts.forEach(function(c){
                $('#channel_dd').append($('<option>', {
                    value: c.channelid,
                    text: c.channelname
                }));
            });
               $('#channel_dd').multiselect({ buttonWidth: '160px', maxHeight: 300 });
               $('#channel_dd').multiselect('refresh');
        },
        error: function( data, textStatus, jQxhr ){
            console.log('Error has occurred while getting channels.');
        }
   });

}

function generatePlayList(){
    showDialog();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    var channelId = $("#channel_dd").val();
    var fromDate = $("#from_date").val();
    var toDate = $("#to_date").val();
    var fromTime = $("#from_time").val();
    var toTime = $("#to_time").val();

    if(channelId=="" || channelId==" "){
        closeDialog();
        viewMessageToastTyped("Please select channel", "Info");
        return;
    }

    if(fromDate=="" || fromDate==" "){
        closeDialog();
        viewMessageToastTyped("Please enter from date", "Info");
        return;
    }

    if(toDate=="" || toDate==" "){
        closeDialog();
        viewMessageToastTyped("Please enter to date", "Info");
        return;
    }

    if(fromTime=="" || fromTime==" "){
        closeDialog();
        viewMessageToastTyped("Please enter from time", "Info");
        return;
    }

    if(toTime=="" || toTime==" "){
        closeDialog();
        viewMessageToastTyped("Please enter to time", "Info");
        return;
    }

    var postData = { 'channelIds': channelId, 'sfromDate': fromDate, 'stoDate':  toDate, 'sfromTime':  fromTime, 'stoTime':  toTime};
    $.ajax({
        url:URI.playlistView.generatePlayList(),
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: JSON.stringify(postData),
        processData: false,
        type: 'POST',
        success: function (seqNum){
            closeDialog();
            if(seqNum != -1){
                window.location = URI.playlistView.downloadPlaylistReport(seqNum);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}