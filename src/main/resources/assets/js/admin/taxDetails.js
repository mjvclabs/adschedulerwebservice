/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var pData;

$(document).ready(function () {
    loadTaxDetails();
});

function loadTaxDetails() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.taxDetails.listUrl() ,
        success: function (data)
        {
            closeDialog();
            
            pData = data;
            var table_data = '';
            $.each(data, function (i, item) {
                table_data += '<tr>';
                table_data += '<td>' + item.taxcategory + '</td>';
                table_data += '<td id="rate_cell_' + i + '">' + item.rate.toFixed(2) + '</td>';
                table_data += '<td id="edit_cell_' + i + '"><button type="button"  style="float:none;" class="close close-icon" aria-label="Update" onclick="updateRow(' + i + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
                table_data += '<?tr>';
            });
            $('#taxDetails_table_data').html(table_data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function updateRow(cell_id) {
    var cellData = '<input type="text" class="form-control Padding0px" id="edit_input_' + cell_id + '" value="' + $('#rate_cell_' + cell_id).html() + '" required="" title="Please enter Tax Rate" >';
    var cellEdit = '<td><button type="button" class="close close-icon" style="float:none;" aria-label="Close" onclick="removeRow(' + cell_id + ')"><span class="glyphicon glyphicon-remove"></span></button></td>';
    $('#rate_cell_' + cell_id).html(cellData);
    $('#edit_cell_' + cell_id).html(cellEdit);
}

function removeRow(cell_id) {
    var cellEdit = '<td id="edit_cell_' + cell_id + '"><button type="button" style="float:none;" class="close close-icon" aria-label="Update" onclick="updateRow(' + cell_id + ')"><span class="glyphicon glyphicon-edit"></span></button></td>';
    $('#rate_cell_' + cell_id).html(pData[cell_id].rate);
    $('#edit_cell_' + cell_id).html(cellEdit);
}

function saveData() {
    showDialog();

    var updatedList = [];
    $.each(pData, function (i, item)
    {
        if ($('#edit_input_' + i).length && (($('#edit_input_' + i).val() != item.rate))) {
            var updatedObject = {};
            updatedObject["taxid"] = item.taxid;
            updatedObject["taxcategory"] = item.taxcategory;
            updatedObject["rate"] = parseFloat($('#edit_input_' + i).val());
            updatedList.push(updatedObject);
        }
    });
    var sendData = JSON.stringify(updatedList);
    console.log(sendData);

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.taxDetails.updateUrl(),
        data: {
            taxData: sendData,
        },
        success: function (data)
        {
            closeDialog();
            loadTaxDetails();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

