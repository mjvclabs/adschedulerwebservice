
var d_Type_one = 'yyy/mm/dd';
var d_Type_two = 'mm/dd';
var flag = 0;
var vDate = '';

$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    vDate = yyyy + '/' + mm + '/' + dd;

    WorkOrderDetailsCreater();
});

function setDateToLable(today)
{
    vDate = today;
    var dayName = getDay(today);
    var day = 1;

    var table = '<table id="workOrder_main" class="table table-bordered table-workorder she_panel"><thead id="table_headr"><tr><th><span>Time_Belt</span></th>';

    if (dayName == 0)
    {
        var start_date = '<label id="lbe_prev" class="float-right lable_forms">' + today + '</label>';
        var end_date = '<label id="lbe_next" class="lable_forms">' + addDays(today, 6, d_Type_one) + '</label>';
        $('#start_date_div').html(start_date);
        $('#end_date_div').html(end_date);


        table += '<th>Sunday(' + addDays(today, 0, d_Type_two) + ')</th><th>Monday(' + addDays(today, day++, d_Type_two) + ')</th><th>Tuesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Wednesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Thursday(' + addDays(today, day++, d_Type_two) + ')</th><th>Friday(' + addDays(today, day++, d_Type_two) + ')</th><th>Saturday(' + addDays(today, day++, d_Type_two) + ')</th></tr></thead><tbody>';
        createTableBody_v2(table, today);
    }
    else if (dayName == 1)
    {
        day = -1;

        var start_date = '<label id="lbe_prev" class="float-right lable_forms">' + addDays(today, day, d_Type_one) + '</label>';
        $('#start_date_div').html(start_date);

        table += '<th>Sunday(' + addDays(today, day++, d_Type_two) + ')</th><th>Monday(' + addDays(today, day++, d_Type_two) + ')</th><th>Tuesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Wednesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Thursday(' + addDays(today, day++, d_Type_two) + ')</th><th>Friday(' + addDays(today, day++, d_Type_two) + ')</th><th>Saturday(' + addDays(today, day++, d_Type_two) + ')</th>';


        var end_date = '<label id="lbe_next" class="lable_forms">' + addDays(today, day - 1, d_Type_one) + '</label>';
        $('#end_date_div').html(end_date);

        day = -1;
        createTableBody_v2(table, addDays(today, day++, d_Type_one));
    }
    else if (dayName == 2)
    {
        day = -2;

        var start_date = '<label id="lbe_prev" class="float-right lable_forms">' + addDays(today, day, d_Type_one) + '</label>';
        $('#start_date_div').html(start_date);

        table += '<th>Sunday(' + addDays(today, day++, d_Type_two) + ')</th><th>Monday(' + addDays(today, day++, d_Type_two) + ')</th><th>Tuesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Wednesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Thursday(' + addDays(today, day++, d_Type_two) + ')</th><th>Friday(' + addDays(today, day++, d_Type_two) + ')</th><th>Saturday(' + addDays(today, day++, d_Type_two) + ')</th>';

        var end_date = '<label id="lbe_next" class="lable_forms">' + addDays(today, day - 1, d_Type_one) + '</label>';
        $('#end_date_div').html(end_date);

        day = -2;
        createTableBody_v2(table, addDays(today, day++, d_Type_one));

    }
    else if (dayName == 3)
    {
        day = -3;

        var start_date = '<label id="lbe_prev" class="float-right lable_forms">' + addDays(today, day, d_Type_one) + '</label>';
        $('#start_date_div').html(start_date);

        table += '<th>Sunday(' + addDays(today, day++, d_Type_two) + ')</th><th>Monday(' + addDays(today, day++, d_Type_two) + ')</th><th>Tuesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Wednesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Thursday(' + addDays(today, day++, d_Type_two) + ')</th><th>Friday(' + addDays(today, day++, d_Type_two) + ')</th><th>Saturday(' + addDays(today, day++, d_Type_two) + ')</th>';

        var end_date = '<label id="lbe_next" class="lable_forms">' + addDays(today, day - 1, d_Type_one) + '</label>';
        $('#end_date_div').html(end_date);

        day = -3;
        createTableBody_v2(table, addDays(today, day++, d_Type_one));

    }
    else if (dayName == 4)
    {
        day = -4;

        var start_date = '<label id="lbe_prev" class="float-right lable_forms">' + addDays(today, day, d_Type_one) + '</label>';
        $('#start_date_div').html(start_date);

        table += '<th>Sunday(' + addDays(today, day++, d_Type_two) + ')</th><th>Monday(' + addDays(today, day++, d_Type_two) + ')</th><th>Tuesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Wednesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Thursday(' + addDays(today, day++, d_Type_two) + ')</th><th>Friday(' + addDays(today, day++, d_Type_two) + ')</th><th>Saturday(' + addDays(today, day++, d_Type_two) + ')</th>';

        var end_date = '<label id="lbe_next" class="lable_forms">' + addDays(today, day - 1, d_Type_one) + '</label>';
        $('#end_date_div').html(end_date);

        day = -4;
        createTableBody_v2(table, addDays(today, day++, d_Type_one));

    }
    else if (dayName == 5)
    {
        day = -5;

        var start_date = '<label id="lbe_prev" class="float-right lable_forms">' + addDays(today, day, d_Type_one) + '</label>';
        $('#start_date_div').html(start_date);

        table += '<th>Sunday(' + addDays(today, day++, d_Type_two) + ')</th><th>Monday(' + addDays(today, day++, d_Type_two) + ')</th><th>Tuesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Wednesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Thursday(' + addDays(today, day++, d_Type_two) + ')</th><th>Friday(' + addDays(today, day++, d_Type_two) + ')</th><th>Saturday(' + addDays(today, day++, d_Type_two) + ')</th>';

        var end_date = '<label id="lbe_next" class="lable_forms">' + addDays(today, day - 1, d_Type_one) + '</label>';
        $('#end_date_div').html(end_date);

        day = -5;
        createTableBody_v2(table, addDays(today, day++, d_Type_one));

    }
    else if (dayName == 6)
    {
        day = -6;

        var start_date = '<label id="lbe_prev" class="float-right lable_forms">' + addDays(today, day, d_Type_one) + '</label>';
        $('#start_date_div').html(start_date);

        table += '<th>Sunday(' + addDays(today, day++, d_Type_two) + ')</th><th>Monday(' + addDays(today, day++, d_Type_two) + ')</th><th>Tuesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Wednesday(' + addDays(today, day++, d_Type_two) + ')</th><th>Thursday(' + addDays(today, day++, d_Type_two) + ')</th><th>Friday(' + addDays(today, day++, d_Type_two) + ')</th><th>Saturday(' + addDays(today, day++, d_Type_two) + ')</th>';

        var end_date = '<label id="lbe_next" class="lable_forms">' + addDays(today, day - 1, d_Type_one) + '</label>';
        $('#end_date_div').html(end_date);

        day = -6;
        createTableBody_v2(table, addDays(today, day++, d_Type_one));
    }
}

function getDay(date)
{
    var d = new Date(date);
    return d.getDay();
}

function addDays(date, days, type)
{
    var result = new Date(date);
    result.setDate(result.getDate() + days);

    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    date = yyyy + '/' + mm + '/' + dd;

    if (type == 'yyy/mm/dd') {
        return yyyy + '/' + mm + '/' + dd;
    }
    else if (type == 'mm/dd')
    {
        return mm + '/' + dd;
    }
}



function clickNextWeek()
{
    var date = document.getElementById('lbe_next').textContent;
    date = addDays(date, 1, d_Type_one);
    setDateToLable(date);
}

function clickPreviousWeek()
{
    var date = document.getElementById('lbe_prev').textContent;
    date = addDays(date, -7, d_Type_one);
    setDateToLable(date);
}

function createTableBody_v2(table, startDate)
{
    var endDate = document.getElementById('lbe_next').textContent;

    var drop_chan = document.getElementById('channelDropdown_admin');
    var channelId = drop_chan.options[drop_chan.selectedIndex].value;

    var drop_wrk = document.getElementById('workOrderDropdown_admin');
    var workOrderId = drop_wrk.options[drop_wrk.selectedIndex].value;

    var drop_adv = document.getElementById('AdvertimentDropdown_admin');
    var advertId = drop_adv.options[drop_adv.selectedIndex].value;

    if (workOrderId == -111 && channelId == -111 && advertId == -111) {
        table += '</tbody></table>';
        $('#workOrder_main_table').html(table);
        return;
    }

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.finalAllScheduleUrl(),
        data: {
            filterData: '{"workOrderId":' + workOrderId + ',"channelId":' + channelId + ',"advertId":' + advertId + ',"startDate":"' + startDate + '","endDate":"' + endDate + '"}',
        },
        success: function (data)
        {
            for (var i in data)
            {
                var itemList = data[i].scheduleItem;
                table += '<tr><td class="tb_align_center">' + RemoveSec(data[i].schedulestarttime) + ' - ' + RemoveSec(data[i].scheduleendtime) + '</td>';

                for (var j = 0, max = 7; j < max; j++)
                {
                    var color_class = 'bg-green';
                    if (itemList[j].utilizationCategory == 1)
                        color_class = 'bg-yellow';
                    else if (itemList[j].utilizationCategory == 2)
                        color_class = 'bg-red';

                    table += '<td class="tb_align_center" id="' + itemList[j].date + '_' + data[i].timeBeltId + '_t" style="padding:2px";><button id=' + itemList[j].date + '_' + data[i].timeBeltId + ' type="button" onclick="modelTest(this.id)" class="btn btn-link btn-xs td-box ' + color_class + '" data-toggle="modal" data-target="#slotDetailModal"><span class="count-fnt">' + itemList[j].airedCount + '/ ' + itemList[j].advertCount + '</span></br><span class="time-fnt">' + itemList[j].utilizedAdvertTime + ' / ' + itemList[j].totalAdvertTime + '</span></button></td>';
                }
                table += '</tr>';
            }

            table += '</tbody></table>';
            $('#workOrder_main_table').html(table);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function modelTest(id)
{
    var drop_chan = document.getElementById('channelDropdown_admin');
    var channelId = drop_chan.options[drop_chan.selectedIndex].value;

    var drop_wrk = document.getElementById('workOrderDropdown_admin');
    var workOrderId = drop_wrk.options[drop_wrk.selectedIndex].value;

    var drop_adv = document.getElementById('AdvertimentDropdown_admin');
    var advertId = drop_adv.options[drop_adv.selectedIndex].value;

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getSelectedScheduleAdvertUrl(),
        data: {
            selectdate: id,
            filterData: '{"workOrderId":' + workOrderId + ',"channelId":' + channelId + ',"advertId":' + advertId + ',"startDate":"No_data","endDate":"No_data"}',
        },
        success: function (data)
        {
            var advertTable = '<table id="advertisement_table_pop" class="table table-bordered table-workorder"><thead><tr class="tb_align_center"><th class="tb_align_center tb_no_white_space">WO Id</th><th class="tb_align_center tb_no_white_space">Channel</th><th class="tb_align_center tb_no_white_space">Advertisement</th><th class="tb_align_center tb_no_white_space">Scheduled time</th><th class="tb_align_center tb_no_white_space">Actual time</th><th class="tb_align_center tb_no_white_space">Duration(Sec)</th><th class="tb_align_center tb_no_white_space">Client</th><th class="tb_align_center tb_no_white_space">Status</th></tr></thead><tbody>';
            for (var i in data)
            {
                var starEndTime = (data[i].startAndEndTime).split("-");
                advertTable += '<tr class="tb_align_center tb_no_white_space"><td>' + data[i].workOrderId + '</td><td>' + data[i].channelName + '</td><td>' + data[i].advertname + '</td><td>' + RemoveSec(starEndTime[0]) + '-' + RemoveSec(starEndTime[1]) + '</td><td>' + data[i].palyTime + '</td><td>' + data[i].duration + '</td><td>' + data[i].client + '</td><td>' + data[i].status + '</td></tr>';
            }

            advertTable += '</tbody></table>';
            $('#advertisement_table_pop_div').html(advertTable);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function channelDetailCreater()
{
    var drop_wrk = document.getElementById('workOrderDropdown_admin');
    var v_workOrderId = drop_wrk.options[drop_wrk.selectedIndex].value;
    if (v_workOrderId != -111)
    {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.workOrder.workOrderChannelListUrl(),
            data: {
                workOrderId: v_workOrderId,
            },
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select id="channelDropdown_admin" name="channelName" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option VALUE="-111" > ------------ All ------------ </option>';
                for (var i in data)
                {
                    trHTML += '<option VALUE="' + data[i].channelid + '">' + data[i].channelname + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#cheannel_list_div').html(trHTML);
                AdvertDetailsCreater();
               closeDialog();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })

    } else {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.channeldetail.listOrderByNameUrl(),
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select id="channelDropdown_admin" name="channelName" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option VALUE="-111" > ------------ None ------------ </option>';
                for (var i in data)
                {
                    trHTML += '<option VALUE="' + data[i].channelid + '">' + data[i].channelname + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#cheannel_list_div').html(trHTML);
                AdvertDetailsCreater();
                closeDialog();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })
    }
}

function WorkOrderDetailsCreater()
{
    console.time("concatenation");

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderListJsonUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += ' <select id="workOrderDropdown_admin" name="WorkOrder" class="form-control" onchange="dropSelectionChangeInWorkOrder()">';
            trHTML += ' <option VALUE="-111" > ------------ None ------------ </option>';
            for (var i in data)
            {
                trHTML += '<option VALUE="' + data[i].workorderid + '">' + data[i].workorderid + '_' + data[i].ordername + '</option>';
            }
            trHTML += '</select>';
            trHTML += '<span id="snp_msg" class="text-danger"></span>';
            trHTML += '<span class="help-block"></span>';

            $('#workOrder_list_div').html(trHTML);

            console.timeEnd("concatenation");

            console.time("concatenation");
            channelDetailCreater();
            console.timeEnd("concatenation");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function AdvertDetailsCreater()
{
    var drop_wrk = document.getElementById('workOrderDropdown_admin');
    var v_workOrderId = drop_wrk.options[drop_wrk.selectedIndex].value;
    if (v_workOrderId == -111)
    {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.advertisement.advertisementAllListOrderByNameUrl(),
            data: {
                workOrderId: v_workOrderId,
            },
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select select id="AdvertimentDropdown_admin" name="Advertiment" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option VALUE="-111"> ------------ None ------------ </option>';
                for (var i in data)
                {
                    trHTML += '<option VALUE="' + data[i].advertid + '">' + data[i].advertname + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#advertisement_list_div').html(trHTML);
                closeDialog();
                setDateToLable(vDate);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })
    } else {
        showDialog();
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.scheduler.clientOrderAdvertListUrl(),
            data: {
                workOrderId: v_workOrderId,
                advertType: 'All',
            },
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select select id="AdvertimentDropdown_admin" name="Advertiment" class="form-control" onchange="dropSelectionChange()">';
                trHTML += ' <option VALUE="-111"> ------------ All ------------ </option>';
                for (var i in data)
                {
                    trHTML += '<option VALUE="' + data[i].advertId + '">' + data[i].advertName + '</option>';
                }
                trHTML += '</select>';
                trHTML += '<span id="snp_msg" class="text-danger"></span>';
                trHTML += '<span class="help-block"></span>';

                $('#advertisement_list_div').html(trHTML);
                closeDialog();
                setDateToLable(vDate);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("../Login.html");
                }
                else {
                    alert("Error");
                }
            }
        })
    }
}

function dropSelectionChange()
{
    var date = document.getElementById('lbe_prev').textContent;
    setDateToLable(date);
}

function dropSelectionChangeInWorkOrder()
{
    vDate = document.getElementById('lbe_prev').textContent;

    channelDetailCreater();
}
