$(document).ready(function () {
    showDialog();
    var server_id = $('#serverId').val();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.servers.getServerbyIdUrl(),
        data: {
            serverId: server_id,
        },
        success: function (data)
        {
            closeDialog();
            $('#txt_server_id').val(data.serverId);
            $('#txt_server_ip').val(data.serverIP);
            $('#txt_server_port').val(data.serverPort);
            $('#txt_card_port').val(data.cardPort);
            $('#txt_service_path').val(data.servicePath);
            $('#cmbInCard').val(data.inCard);
            $('#cmbOutCard').val(data.outCard);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
});

function updateServer() {
    server_id = $('#txt_server_id').val();
    server_ip = $('#txt_server_ip').val();
    server_port = $('#txt_server_port').val();
    card_port = $('#txt_card_port').val();
    service_path = $('#txt_service_path').val();
    service_path = service_path.replace(/\\/g, '\\\\');
    in_card = $('#cmbInCard').val();
    out_card = $('#cmbOutCard').val();

    model_data = '{"serverID":' + server_id + ',"serverIP":"' + server_ip + '","serverPort":' + server_port + ',"channelPort":' + card_port + ',"servicePath":"' + service_path + '","inCard":' + in_card + ',"outCard":' + out_card + '}';
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.servers.updateServerBasicDataUrl(),
        data: {
            serverData: model_data,
        },
        success: function (data)
        {
            window.location.href = URI.servers.allServerPageUrl();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function getParameterByNamefromUrl(name) {
    url = window.location.href;
    console.log(url);

    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function updateServerAfterValidation(form) {
    if(validateInputFields(form)){
        updateServer();
    }
}