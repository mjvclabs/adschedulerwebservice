function addServer()
{
    server_ip = $('#txt_server_ip').val();
    server_port = $('#txt_server_port').val();
    card_port = $('#txt_card_port').val();
    service_path = $('#txt_service_path').val();
    service_path = service_path.replace(/\\/g, '\\\\');
    in_card = $('#cmbInCard').val();
    out_card = $('#cmbOutCard').val();

    model_data = '{"serverID":' + -1 + ',"serverIP":"' + server_ip + '","serverPort":' + server_port + ',"channelPort":' + card_port + ',"servicePath":"' + service_path + '","inCard":' + in_card + ',"outCard":' + out_card + '}';
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.servers.insertServerUrl(),
        data: {
            serverData: model_data,
        },
        success: function (data)
        {
            window.location.href = URI.servers.allServerPageUrl();
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function addServerAfterValidation(form) {
    if(validateInputFields(form)){
         addServer();
    }
}
