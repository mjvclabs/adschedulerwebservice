$(document).ready(function () {
    $('#tbody_data').on('scroll', function() {
         $('#tbody_left').scrollTop($(this).scrollTop());
    });
    initSelectedDateRange();
    function initSelectedDateRange(){
        if($('#d-radio')[0].checked){
            var date = new Date($('#date').val());
            if(date != null){
               setDate(date);
               disableDateRangeInputs();
            }else{
               resetToDefaultDateRangePicker();
            }
        }else if($('#dr-radio')[0].checked){
            setDateRange();
        }
    }

    $('#dr-radio').on('ifChecked', function(){
         $('#cb-sunday').iCheck('enable');
         $('#cb-monday').iCheck('enable');
         $('#cb-tuesday').iCheck('enable');
         $('#cb-wednesday').iCheck('enable');
         $('#cb-thursday').iCheck('enable');
         $('#cb-friday').iCheck('enable');
         $('#cb-saturday').iCheck('enable');
         $('#date').prop('disabled', true);
         $('#fromDate').prop('disabled', false);
         $('#toDate').prop('disabled', false);
         $('#date').val('').datepicker('update');
         resetToDefaultDateRangePicker();
    });

    $('#d-radio').on('ifChecked', function(){
        disableDateRangeInputs();
        resetToDefaultDateRangePicker();
    });

    function disableDateRangeInputs(){
        $('#cb-sunday').iCheck('disable');
        $('#cb-monday').iCheck('disable');
        $('#cb-tuesday').iCheck('disable');
        $('#cb-wednesday').iCheck('disable');
        $('#cb-thursday').iCheck('disable');
        $('#cb-friday').iCheck('disable');
        $('#cb-saturday').iCheck('disable');

        $('#cb-sunday').iCheck('check');
        $('#cb-monday').iCheck('check');
        $('#cb-tuesday').iCheck('check');
        $('#cb-wednesday').iCheck('check');
        $('#cb-thursday').iCheck('check');
        $('#cb-friday').iCheck('check');
        $('#cb-saturday').iCheck('check');

        $('#date').prop('disabled', false);
        $('#fromDate').prop('disabled', true);
        $('#toDate').prop('disabled', true);

        if($('#fromDate').data('datepicker')){
          $('#fromDate').data('datepicker').setDate(null);
        }

        if($('#toDate').data('datepicker')){
          $('#toDate').data('datepicker').setDate(null);
        }
    }

     $('#cb-sunday, #cb-monday, #cb-tuesday, #cb-wednesday, #cb-thursday, #cb-friday, #cb-saturday').on('ifToggled', function(){
       setDateRange();
    });

    $( "#date, #fromDate, #toDate").datepicker({
        format: 'yyyy-mm-dd',
        changeMonth: true,
        changeYear: true,
        startDate: new Date()
    });

    $('#fromDate, #toDate').on('changeDate', function(e){
        setDateRange();
    });

    var inventoryOverrideRequest = false;

    function setDateRange(){
        var channelId = $('#inventory\\.channel\\.channelid').val();
        var fromDateText = $('#fromDate').val();
        var toDateText = $('#toDate').val();
        var fromDate = new Date(fromDateText);
        var toDate = new Date(toDateText);

        if(fromDate != null && toDate != null){

            if(fromDate.getTime() > toDate.getTime()){
                  toastr.error("'From Date' should be less than or equal to 'To Date' ", 'Invalid Date Range');
                  resetDateRangePicker(fromDate);
                  return;
            }

            var options = {
                success :  function(resultList, textStatus, jQxhr ){
                    inventoryOverrideRequest = false;
                    var selectedDates = getDates(fromDate, toDate);
                    $('#selectedDateRange').datepicker('destroy');
                    $('#selectedDateRange').datepicker({
                       defaultViewDate: {
                           year: fromDate.getFullYear(),
                           month: fromDate.getMonth()
                       },
                       beforeShowDay: function(date) {
                           if(containsDate(selectedDates, date)){
                                if(hasInventoryForDate(resultList, moment(date).format('YYYY-MM-DD'))){
                                    inventoryOverrideRequest = true;
                                    return {
                                       enabled : false,
                                       classes: 'bg-orange'
                                    };
                                }else{
                                    return {
                                       enabled : false,
                                       classes: 'bg-green'
                                    };
                                }
                           }
                           return { enabled : false };
                       }
                   });
                },
                error : function( jqXhr, textStatus, errorThrown ){
                   toastr.error('Error has occurred while checking existing inventories.', 'Inventory Error');
                }
            };

            findInventoryExistDates(channelId, fromDateText, toDateText, options);

        }else if(fromDate != null && toDate == null){
               resetDateRangePicker(fromDate);
        }else{
             resetToDefaultDateRangePicker();
        }
    }

    function findInventoryExistDates(channelId, fromDate, toDate, options){
         var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
         var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

         $.ajax({
              url: URI.inventory.findInventoryExistDates(channelId, fromDate, toDate),
              dataType: 'json',
              contentType: 'application/json',
              beforeSend: function (request) {
                  request.setRequestHeader(csrf_headerName, csrf_paramValue);
              },
              processData: false,
              type: 'get',
              success: _.bind(options.success, this),
              error:  _.bind(options.error, this)
         });
    }

    function resetToDefaultDateRangePicker(){
        $('#selectedDateRange').datepicker('destroy');
        $('#selectedDateRange').datepicker({
            format: 'yyyy-mm-dd',
            changeMonth: true,
            changeYear: true,
            beforeShowDay: function(date) {
               return { enabled : false };
            }
        });
    }

    function resetDateRangePicker(fromDate){
                $('#selectedDateRange').datepicker('destroy');
                $('#selectedDateRange').datepicker({
                    defaultViewDate: {
                        year: fromDate.getFullYear(),
                        month: fromDate.getMonth()
                    },
                    beforeShowDay: function(date) {
                        return { enabled : false };
                    }
               });
    }

    $('#date').on('changeDate', function(e){
         var date = $('#date').val();
         var channelId = $('#inventory\\.channel\\.channelid').val();
         showDialog();
         location.href = URI.inventory.editInventoryByChannelAndDate(channelId, date);
    });

    function setDate(selectedDate){

            var options = {
                success :  function(resultList, textStatus, jQxhr ){
                    inventoryOverrideRequest = false;
                    $('#selectedDateRange').datepicker('destroy');
                    $('#selectedDateRange').datepicker({
                       defaultViewDate: {
                           year: selectedDate.getFullYear(),
                           month: selectedDate.getMonth()
                       },
                       beforeShowDay: function(date) {
                           if(selectedDate.toDateString() == date.toDateString()){
                                if(hasInventoryForDate(resultList, moment(date).format('YYYY-MM-DD'))){
                                    inventoryOverrideRequest = true;
                                    return {
                                       enabled : false,
                                       classes: 'bg-orange'
                                    };
                                }else{
                                    return {
                                       enabled : false,
                                       classes: 'bg-green'
                                    };
                                }
                           }
                           return { enabled : false };
                       }
                   });
                },
                error : function( jqXhr, textStatus, errorThrown ){
                   toastr.error('Error has occurred while checking existing inventories.', 'Inventory Error');
                }
            };

           var channelId = $('#inventory\\.channel\\.channelid').val();
           var dateText = $('#date').val();
           findInventoryExistDates(channelId, dateText, dateText, options);
    }

    function containsDate(selectedDates, date){
        for(var i=0; i < selectedDates.length; i++) {
            if(selectedDates[i].toDateString() == date.toDateString()){
              return true;
            }
        }
        return false;
    }

    function hasInventoryForDate(existingDates, date){
        for(var i=0; i < existingDates.length; i++) {
            if(existingDates[i] == date){
                  return true;
            }
        }
        return false;
    }

    Date.prototype.addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    function getDates(startDate, stopDate) {

        var weekSettings = [
           $('#cb-sunday')[0].checked,
           $('#cb-monday')[0].checked,
           $('#cb-tuesday')[0].checked,
           $('#cb-wednesday')[0].checked,
           $('#cb-thursday')[0].checked,
           $('#cb-friday')[0].checked,
           $('#cb-saturday')[0].checked
        ];

        var dateArray = new Array();
        var currentDate = new Date(startDate);
        while (currentDate <= stopDate) {
            if(weekSettings[currentDate.getDay()]){
                dateArray.push(new Date (currentDate));
            }
           currentDate.setDate(currentDate.getDate() + 1);
        }
        return dateArray;
    }

    $channelDropDown = $('#inventory\\.channel\\.channelid');
    $channelDropDown.on('change', function(e){
        showDialog()
        location.href = URI.inventory.viewInventoryByChannelId($channelDropDown.val());
    });

    $("#inventory-form").submit(function(e){
        e.preventDefault();

        var dateBased = $('#d-radio')[0].checked;
        if(dateBased){
           var date = $('#date').val();
           if(date.length == 0){
              toastr.error("'Date' should not be a empty", 'Invalid Date');
              return;
           }
        }else{
            var fromDateText = $('#fromDate').val();
            var toDateText = $('#toDate').val();
            if(fromDateText.length == 0 || toDateText.length == 0){
                toastr.error("Both 'From Date' and 'To Date' should not be empty", 'Invalid Date Range');
                return;
            }
            var fromDate = new Date(fromDateText);
            var toDate = new Date(toDateText);
            if(fromDate != null && toDate != null){
                if(fromDate.getTime() > toDate.getTime()){
                      toastr.error("'From Date' should be less than or equal to 'To Date' ", 'Invalid Date Range');
                      resetDateRangePicker(fromDate);
                      return;
                }
            }
        }

        if(inventoryOverrideRequest){
            $('#inventoryOverrideModal').modal('toggle');
        }else{
            $('#overrideOperation').val(false);
            showDialog();
            this.submit();
        }
    });
   // $('#inventory-table-div').doubleScroll();
 });

 function overrideInventory(){
    $('#overrideOperation').val(true);
    showDialog();
    document.getElementById("inventory-form").submit();
 }

 function notOverrideInventory(){
    $('#overrideOperation').val(false);
    showDialog();
    document.getElementById("inventory-form").submit();
 }
