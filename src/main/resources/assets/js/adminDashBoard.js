var mapChannels = {};
var mapServers = {};
var mapChannelServer = {};
var mapChannelSelected = {};
var mapIsProcessing = {};

$(document).ready(function () {
    loadChannels();
    loadServers();
    setInterval(updateServers, 5000);
});

function loadServers() {
    showDialog();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.servers.serverdetailsUrl(),
        success: function (data)
        {
            closeDialog();
            mapServers = {};
            mapChannelServer = {};

            var table_data = '';
            $.each(data, function (index, item) {

                if (item.channelDetails == "null")
                    item.channelDetails = -1;

                table_data += '<tr>';
                if (item.status == 0)
                    table_data += '<td id="status_' + item.serverId + '"><label style="color:red;">&#9632;</label></td>';
                else
                    table_data += '<td id="status_' + item.serverId + '"><label style="color:green;">&#9654;</label></td>';
                table_data += '<td id="errorReport_' + item.serverId + '">' + item.errorReport + '</td>';
                table_data += '<td>' + item.serverIP + '</td>';
                table_data += '<td>' + item.cardPort + '</td>';
                table_data += '<td id="freeDiskSpace_' + item.serverId + '">' + (item.freeDiskSpace / 1073741824).toFixed(3) + '</td>';
                table_data += '<td id="freeMemory_' + item.serverId + '">' + (item.freeMemory / 1073741824).toFixed(3) + '</td>';
                table_data += '<td id="Channeld_' + item.serverId + '">' + getChannelDropDown(item.serverId) + '</td>';
                if (item.status == 0 && !mapIsProcessing[item.serverId])
                    table_data += '<td id="startServer_' + item.serverId + '"><button onclick="startServer(' + item.serverId + ')"> Start </button></td>';
                else if (item.status == 0 && mapIsProcessing[item.serverId])
                    table_data += '<td id="startServer_' + item.serverId + '"><button onclick=""> Starting </button></td>';
                else
                    table_data += '<td id="startServer_' + item.serverId + '"><button onclick="stopServer(' + item.serverId + ')"> Stop </button></td>';
                table_data += '</tr>';

                mapServers[item.serverId] = item;
                mapChannelServer[item.serverId] = item.channelDetails;

                if (!(item.serverId in mapIsProcessing))
                    mapIsProcessing[item.serverId] = false;
            });
            $('#server_main tbody').html(table_data);

            $.each(data, function (index, item) {
                getChannelDropDown(item.serverId);
                if (item.status != 0)
                    $('#cmbChannel_' + item.serverId).attr('disabled', true);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

}

function updateServers() {
    console.log("tick");
    showDialog();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.servers.serverdetailsUrl(),
        success: function (data)
        {
            closeDialog();
            mapServers = {};
            mapChannelServer = {};

            $.each(data, function (index, item) {

                if (item.channelDetails == "null")
                    item.channelDetails = -1;

                if (item.status == 0)
                    $('#status_' + item.serverId).html('<label style="color:red;">&#9632;</label>');
                else
                    $('#status_' + item.serverId).html('<label style="color:green;">&#9654;</label>');

                $('#errorReport_' + item.serverId).html(item.errorReport);
                $('#freeDiskSpace_' + item.serverId).html((item.freeDiskSpace / 1073741824).toFixed(3));
                $('#freeMemory_' + item.serverId).html((item.freeMemory / 1073741824).toFixed(3));
                if (item.status != 0)
                    $('#Channeld_' + item.serverId).html(getChannelDropDown(item.serverId));
                if (item.status == 0 && !mapIsProcessing[item.serverId])
                    $('#startServer_' + item.serverId).html('<button onclick="startServer(' + item.serverId + ')"> Start </button>');
                else if (item.status != 0) {
                    $('#startServer_' + item.serverId).html('<button onclick="stopServer(' + item.serverId + ')"> Stop </button>');
                    mapIsProcessing[item.serverId] = false;
                }
                mapServers[item.serverId] = item;
                mapChannelServer[item.serverId] = item.channelDetails;
            });

            $.each(data, function (index, item) {
                getChannelDropDown(item.serverId);
                if (item.status != 0)
                    $('#cmbChannel_' + item.serverId).attr('disabled', true);
                else
                    $('#cmbChannel_' + item.serverId).attr('disabled', false);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}



function loadChannels() {
    showDialog();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.getAllChannelUrl(),
        success: function (data)
        {
            closeDialog();
            mapChannels = {};
            mapChannelSelected = {};

            $.each(data, function (index, item) {
                mapChannels[item.channelid] = item;
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function getChannelDropDown(id) {
    stDropDown = '<div class="dropdown"><select id="cmbChannel_' + id + '" class="form-control" required="" onchange="onChannelSelectChanged(this.id)">';
    stDropDown += '<option value="-1">None</option>';
    mapChannelSelected['cmbChannel_' + id] = -1;

    for (key in mapChannels) {
        if (mapChannels.hasOwnProperty(key)) {
            value = mapChannels[key];
            if (value.serverDetails == null) {
                stDropDown += '<option value=' + value.channelid + '>' + value.channelname + '</option>';
            } else if (value.serverDetails.serverId == id) {
                stDropDown += '<option value=' + value.channelid + ' selected>' + value.channelname + '</option>';
                mapChannelSelected['cmbChannel_' + id] = value.channelid;
            }
        }
    }
    stDropDown += '</div>';
    return stDropDown;

}

function startServer(id) {
    server_id = id;
    channel_id = $('#cmbChannel_' + id).val();
    if (channel_id == -1) {
        alert("Please Select a Channel..!")
        return;
    }

    model_data = '{"serverID":' + server_id + ',"command":"Start","channelId":' + channel_id + '}';
    showDialog();
    mapIsProcessing[server_id] = true;
    $('#startServer_' + server_id).html('<button disabled> Starting </button>');
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.servers.updateServerDynamicDataUrl(),
        data: {
            serverData: model_data,
        },
        success: function (data)
        {
            closeDialog();
            loadChannels();
            loadServers();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function stopServer(id) {
    var retVal = confirm("Do you want to Stop this Channel?");
    if (retVal == false) {
        return;
    }

    server_id = id;
    //channel_id = $('#cmbChannel_' + id ).val();

    model_data = '{"serverID":' + server_id + ',"command":"Stop","channelId":-1}';
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.servers.updateServerDynamicDataUrl(),
        data: {
            serverData: model_data,
        },
        success: function (data)
        {
            closeDialog();
            loadChannels();
            loadServers();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function onChannelSelectChanged(id) {
    console.log(id);
    iPrevChannel = -1;
    if (mapChannelSelected[id] != undefined)
        iPrevChannel = mapChannelSelected[id];

    iChannelId = stoi($('#' + id).val());

    for (key in mapChannelSelected) {
        if (mapChannelSelected.hasOwnProperty(key)) {
            value = mapChannelSelected[key];
            if (id != key && value == iChannelId) {
                $('#' + id).val(iPrevChannel);
                return;
            }
        }
    }
    mapChannelSelected[id] = iChannelId;
}