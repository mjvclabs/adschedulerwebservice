/* global parseFloat, server_path, web_page_path */

var jsonchannelUpdateList = [];
var jsonAllChannelList = '';
var workOrderId_pub = 0;

var usedAmount_pub = 0;
var totalBudget_pub = 0;
var selectedChannelValue_pub = 0;

var workOrderData;
var lstAverts = [];
var mapAdverts = {};
var addedAdverts = {};

var viTotalSecondsNBonus = 0;
var viTotalSecondsBonus = 0;

var totalAdvertCount = [];
var usedSpotArray;
var billStatus = 0;

var bonous = false;

$(function () {
    $('#form-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});

$(function () {
    $('#to-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});


$(document).ready(function () {
    setUser();
    // setAdvertisementDetails();
    loadAdvertTable();
});

/*Read*/
function setUser() {
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.userDetails.getMeUserUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += ' <div class="dropdown"><select id="user_name" name="wo_type" class="form-control" >';
            trHTML += '<option value="">Select Marketing Ex</option>';
            for (var i in data)
            {
                trHTML += '<option value="' + data[i].userName + '">' + data[i].userName + '</option>';
            }
            trHTML += '</select></div><span class="help-block"></span>';
            $('#user_drop').html(trHTML);
            channelDetailCreater();
        },
        error: function (jqXHR, textStatus, thrownError) {
        }
    });
}

/*Read*/
function getWorkOrderDeteils() {
    showDialog();
    var id = stoi(getParameterByNamefromUrl('work_id'));

    ///get work order details from service
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.selectedWorkOrderUrl(),
        data: {
            workOrderId: id
        },
        success: function (data)
        {
            closeDialog();
            workOrderData = data;
            workOrderId_pub = data[0].workOrderId;
            document.getElementById("txt_wo_id").value = data[0].workOrderId;
            document.getElementById("user_name").value = data[0].seller;
            document.getElementById("product-name").value = data[0].productName;
            document.getElementById("revenue-month").value = data[0].revenueMonth;

            var _agency = data[0].agency;
            if (_agency == 0) {
                $("#direct_client_radio").prop("checked", true)
                ClientClick(2, data[0].workOrderStatus);
            } else {
                $("#client_radio").prop("checked", true)
                ClientClick(1, data[0].workOrderStatus);
            }
            $("#billClientDropdown").val(data[0].billingClient);
//            document.getElementById("fromDate").value = (data[0].startdate.split(' '))[0];
//            document.getElementById("toDate").value = (data[0].enddate.split(' '))[0];
            $('#fromDate').datetimepicker("setDate", new Date((data[0].startdate.split(' '))[0])).change();
            $('#toDate').datetimepicker("setDate", new Date((data[0].enddate.split(' '))[0])).change();
            document.getElementById("comment").value = data[0].comment;

            if (data[0].b_spots_visibility == 1) {
                document.getElementById("visible").checked = true;
            } else {
                document.getElementById("visible").checked = false;
            }

            document.getElementById("wo_type_Dropdown").value = data[0].woTyep;
            document.getElementById("txt_total_budget").value = getDecimal(parseFloat(data[0].totalbudget));
            document.getElementById("txt_schedule_ref").value = data[0].scheduleRef;
            document.getElementById("manual-status").value = data[0].manualStatus;
            totalBudget_pub = data[0].totalbudget;
            billStatus = data[0].billStatus;
            if (billStatus != 0) {
                document.getElementById("txt_logo_spots").disabled = true;
                document.getElementById("txt_Crawler_spots").disabled = true;
                document.getElementById("txt_v_squeeze_spots").disabled = true;
                document.getElementById("txt_l_squeeze_spots").disabled = true;
            }
            if (data[0].commission == 15) {
                $("#commison_15").prop("checked", true);
                //document.getElementById("commison_15").value = 15;
            } else {
                $("#commison_0").prop("checked", true);
                //document.getElementById("commison_0").value = 0;
            }

            var arr = data[0].channelList;
            for (var i in arr) {
                var channelArray = '{"channelId":' + arr[i].channelid + ',"workOrderId":' + arr[i].workorderid + ',"tvc_numOfSpots":' + arr[i].numbertofspot + ',"tvc_bonusSpots":' + arr[i].bonusSpots + ',"tvc_package":' + arr[i].tvcPackage + ',"logo_numOfSpots":' + arr[i].logoSpots + ',"logo_bonusSpots":' + arr[i].logo_B_spots + ',"logo_package":' + arr[i].logoPackage + ',"crowler_numOfSpots":' + arr[i].crowlerSpots + ',"crowler_bonusSpots":' + arr[i].crowler_B_Spots + ',"crowler_package":' + arr[i].crowlerPackage + ',"v_sqeeze_numOfSpots":' + arr[i].v_sqeezeSpots + ',"v_sqeeze_bonusSpots":' + arr[i].v_sqeeze_B_Spots + ',"v_sqeeze_package":' + arr[i].vSqeezePackage + ',"l_sqeeze_numOfSpots":' + arr[i].l_sqeezeSpots + ',"l_sqeeze_bonusSpots":' + arr[i].l_sqeeze_B_Spots + ',"l_sqeeze_package":' + arr[i].lSqeezePackage + ',"package":' + arr[i].packageValue + ',"ave_package":' + arr[i].averagePackageValue
                        + ',"tvc_brkdown":{}'
                        + ',"tvc_brkdown_b":{}'
                        + '}';

                var obj = JSON.parse(channelArray);
                obj.tvc_brkdown = arr[i].tvcSpotCountList;
                obj.tvc_brkdown_b = arr[i].tvcBonusSpotCountList;
                jsonchannelUpdateList.push(obj);

                map_ChannelSpotCountDuration_NonBonus[arr[i].channelid] = arr[i].tvcSpotCountList;
                map_ChannelSpotCountDuration_Bonus[arr[i].channelid] = arr[i].tvcBonusSpotCountList;

            }
            for (var j in jsonchannelUpdateList)
            {
                usedAmount_pub = usedAmount_pub + jsonchannelUpdateList[j].package;
            }
            usedAmount_pub = usedAmount_pub.toFixed(2);

            if (data[0].workOrderStatus === 3) {
                //document.getElementById("user_name").disabled = true;
//                document.getElementById("wo_type_Dropdown").disabled = true;
                document.getElementById("billClientDropdown").disabled = true;

                document.getElementById("commison_15").disabled = true;
                document.getElementById("commison_0").disabled = true;
                document.getElementById("direct_client_radio").disabled = true;
                document.getElementById("client_radio").disabled = true;

                //document.getElementById("product-name").readOnly = true;
                //document.getElementById("txt_schedule_ref").readOnly = true;


            }
            /*----------------------------Comment for the release--------------------------------------*/
            if(data[0].workOrderStatus === 4){
               // var resumeBtn = '<button type="button" onclick="resumeWo()" class="btn btn_inactive add btn_yellocolor">Resume</button>';
               // $('#suspend-and-resume-id').html(resumeBtn);
                $('#btn-suspend-id').attr("disabled", true);
            }
            /*------------------- -----------------------------------------------*/

            calculateTotalBuget();
            setSelectedChannelDrop();
            setAdvertisementDetails();
            getAttachmentDetais();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error " + textStatus);
                alert("Error " + errorThrown);
                alert("Error " + jqXHR);
            }
        }
    });

}

/*Read*/
function clientDetailsCreater() {
    agencyClientList = '<div class="dropdown"><select id="agencyDropdown" name="clientName" class="form-control"><option id="-1" value="">Select Agency</option>';
    clientList = '<div class="dropdown"><select id="clientDropdown" name="clientName" onchange="setAdvertisementDetails()" class="form-control"><option id="-1" value="">Select Client</option>';
    allClientList = '<div class="dropdown"><select id="allclientDropdown" name="clientName" onchange="setAdvertisementDetails()" class="form-control"><option id="-1" value="">Select Client</option>';
    billClientList = '<div class="dropdown"><select id="billClientDropdown" name="clientName" onchange="setAdvertisementDetails()" class="form-control"><option id="-1" value="">Select Client</option>';
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.client.getAllClientOrderByName(),
        success: function (data)
        {
            for (var i in data)
            {
                if (data[i].clienttype == 'Agency')
                {
                    agencyClientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
                else if (data[i].clienttype == 'Client')
                {
                    clientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
                allClientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                billClientList += '<option id="' + data[i].clientid + '" value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
            }
            agencyClientList += '	</select></div><span class="help-block"></span>';
            clientList += '</select></div><span class="help-block"></span>';
            allClientList += '</select></div><span class="help-block"></span>';
            billClientList += '</select></div><span class="help-block"></span>';
            $('#agency').html(agencyClientList);
            $('#client').html(clientList);
            $('#billing_client').html(billClientList);

            getWorkOrderDeteils();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function ClientClick(vale, woStatus) {
    var clientDro = '';
    if (vale == 2)
    {
        clientDro = '<div class="form-group"><label class="control-label col-sm-3 col-md-3 col-lg-3 lable_forms" for="client">Client</label><div class="control-label col-sm-8 col-md-8 col-lg-8 input_width" id ="client_all">' + allClientList + '<span class="help-block"></span></div></div>';
        $('#client_div').html(clientDro);
        if (workOrderData[0].agency == 0)
        {
            document.getElementById("allclientDropdown").value = workOrderData[0].clientName;
            if (woStatus === 3) {
                document.getElementById("allclientDropdown").disabled = true;
            }
        }
    }
    else if (vale == 1)
    {
        clientDro = '<div class="form-group"><label class="control-label col-sm-3 col-md-3 col-lg-3 lable_forms" for="agency">Agency</label><div class="control-label col-sm-8 col-md-8 col-lg-9 input_width" id ="agency">' + agencyClientList + '<span class="help-block"></span></div></div>';
        clientDro += '<div class="form-group"><label class="control-label col-sm-3 col-md-3 col-lg-3 lable_forms" for="client">End Client</label><div class="control-label col-sm-8 col-md-8 col-lg-9 input_width" id ="client">' + clientList + '<span class="help-block"></span></div></div>';
        $('#client_div').html(clientDro);
        if (workOrderData[0].agency != 0)
        {
            document.getElementById("agencyDropdown").value = workOrderData[0].agency;
            document.getElementById("clientDropdown").value = workOrderData[0].clientName;
            if (woStatus === 3) {
                document.getElementById("agencyDropdown").disabled = true;
                document.getElementById("clientDropdown").disabled = true;
            }
        }
    }

}

/*Read*/
//get all channel list from service
function channelDetailCreater() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += ' <select id="channelDropdown" name="channelName" onchange="changeChannels(this.value)"  class="form-control middle_select">';
            trHTML += ' <option VALUE="-111" >Select Channel</option>';
            for (var i in data)
            {
                trHTML += '<option VALUE="' + data[i].channelid + '|' + data[i].channelname + '">' + data[i].channelname + '</option>';
                var channelData = '{"channelId":' + data[i].channelid + ',"channelname":"' + data[i].channelname + '"}';
                if (jsonAllChannelList == '') {
                    channelData = '[' + channelData + ']';
                    jsonAllChannelList = JSON.parse(channelData);
                } else {
                    var obj = JSON.parse(channelData);
                    jsonAllChannelList.push(obj);
                }
            }
            trHTML += '</select>';
            $('#channelList').html(trHTML);
            clientDetailsCreater();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
}

function addChannel() {

    var selectedChannel = $('#channelDropdown').val().split('|');
    var channelName = selectedChannel[1];
    var channelId = selectedChannel[0];

    hasError($('#channelDropdown'), false, 'error');
    if (channelId != -111)
    {
        var tvc_spots = stoi($('#txt_tvc_spots').val());
        var tvc_spots_b = stoi($('#txt_tvc_spots_b').val());
        var logo_spots = stoi($('#txt_logo_spots').val());
        var logo_spots_b = stoi($('#txt_logo_spots_b').val());
        var crawler_spots = stoi($('#txt_Crawler_spots').val());
        var crawler_spots_b = stoi($('#txt_Crawler_spots_b').val());
        var v_squeeze_spots = stoi($('#txt_v_squeeze_spots').val());
        var v_squeeze_spots_b = stoi($('#txt_v_squeeze_spots_b').val());
        var l_squeeze_spots = stoi($('#txt_l_squeeze_spots').val());
        var l_squeeze_spots_b = stoi($('#txt_l_squeeze_spots_b').val());

        var tvc_package_val = parseFloat($('#txt_tvc_spots_packege').val());
        if (isNaN(tvc_package_val)) {
            tvc_package_val = 0;
        }
        var logo_package_val = parseFloat($('#txt_logos_spots_packege').val());
        if (isNaN(logo_package_val)) {
            logo_package_val = 0;
        }
        var crawler_package_val = parseFloat($('#txt_crawler_spots_packege').val());
        if (isNaN(crawler_package_val)) {
            crawler_package_val = 0;
        }
        var l_squeeze_package_val = parseFloat($('#txt_l_squeeze_spots_packege').val());
        if (isNaN(l_squeeze_package_val)) {
            l_squeeze_package_val = 0;
        }
        var v_squeeze_package_val = parseFloat($('#txt_v_squeeze_spots_packege').val());
        if (isNaN(v_squeeze_package_val)) {
            v_squeeze_package_val = 0;
        }

        var package_value = parseFloat($('#txt_package').val());
        if (isNaN(package_value)) {
            package_value = 0;
        }
        var average_package_value = parseFloat($('#txt_average_package').val());
        if (isNaN(average_package_value)) {
            average_package_value = 0;
        }

        var workOrderId_p = 70;
        var channelArray = '{"channelId":' + channelId + ',"workOrderId":' + workOrderId_p + ',"tvc_numOfSpots":'
                + tvc_spots + ',"tvc_bonusSpots":' + tvc_spots_b + ',"tvc_package":' + tvc_package_val + ',"logo_numOfSpots":'
                + logo_spots + ',"logo_bonusSpots":' + logo_spots_b + ',"logo_package":' + logo_package_val
                + ',"crowler_numOfSpots":' + crawler_spots + ',"crowler_bonusSpots":' + crawler_spots_b + ',"crowler_package":'
                + crawler_package_val + ',"v_sqeeze_numOfSpots":' + v_squeeze_spots + ',"v_sqeeze_bonusSpots":'
                + v_squeeze_spots_b + ',"v_sqeeze_package":' + v_squeeze_package_val + ',"l_sqeeze_numOfSpots":'
                + l_squeeze_spots + ',"l_sqeeze_bonusSpots":' + l_squeeze_spots_b + ',"l_sqeeze_package":'
                + l_squeeze_package_val + ',"package":' + package_value + ',"ave_package":' + average_package_value
                + ',"tvc_brkdown":{}'
                + ',"tvc_brkdown_b":{}'
                + '}';

        var obj = JSON.parse(channelArray);
        if (channelId in map_ChannelSpotCountDuration_NonBonus)
            obj.tvc_brkdown = map_ChannelSpotCountDuration_NonBonus[channelId];
        if (channelId in map_ChannelSpotCountDuration_Bonus)
            obj.tvc_brkdown_b = map_ChannelSpotCountDuration_Bonus[channelId];

        usedAmount_pub = 0;
        for (var i = 0; i < jsonchannelUpdateList.length; i++)
        {
            if (jsonchannelUpdateList[i].channelId == obj.channelId) {
                jsonchannelUpdateList.splice(i, 1);
                i--;
            } else {
                usedAmount_pub = parseFloat(usedAmount_pub) + parseFloat(jsonchannelUpdateList[i].package);
            }
        }
        usedAmount_pub = parseFloat(usedAmount_pub) + parseFloat(obj.package);
        usedAmount_pub = usedAmount_pub.toFixed(2);
        jsonchannelUpdateList.push(obj);

        calculateTotalBuget();
        setSelectedChannelDrop();

        document.getElementById("txt_tvc_spots").value = '';
        document.getElementById("txt_tvc_spots_b").value = '';
        document.getElementById("txt_logo_spots").value = '';
        document.getElementById("txt_logo_spots_b").value = '';
        document.getElementById("txt_Crawler_spots").value = '';
        document.getElementById("txt_Crawler_spots_b").value = '';
        document.getElementById("txt_v_squeeze_spots").value = '';
        document.getElementById("txt_v_squeeze_spots_b").value = '';
        document.getElementById("txt_l_squeeze_spots").value = '';
        document.getElementById("txt_l_squeeze_spots_b").value = '';
        document.getElementById("txt_package").value = '';
        document.getElementById("txt_average_package").value = '';
        document.getElementById("txt_tvc_spots_packege").value = '';
        document.getElementById("txt_logos_spots_packege").value = '';
        document.getElementById("txt_crawler_spots_packege").value = '';
        document.getElementById("txt_v_squeeze_spots_packege").value = '';
        document.getElementById("txt_l_squeeze_spots_packege").value = '';

        document.getElementById("channelDropdown").value = '-111';

        $('#txt_TotalSeconds').val('');
    } else {
        hasError($('#channelDropdown'), true, 'error');
    }
    getTotalDurationfromTVCBreckdown(channelId);
}

function changeChannels(channelvalue) {
    var channelId = channelvalue.split("|");

    document.getElementById("txt_tvc_spots").value = '';
    document.getElementById("txt_tvc_spots_b").value = '';
    document.getElementById("txt_logo_spots").value = '';
    document.getElementById("txt_logo_spots_b").value = '';
    document.getElementById("txt_Crawler_spots").value = '';
    document.getElementById("txt_Crawler_spots_b").value = '';
    document.getElementById("txt_v_squeeze_spots").value = '';
    document.getElementById("txt_v_squeeze_spots_b").value = '';
    document.getElementById("txt_l_squeeze_spots").value = '';
    document.getElementById("txt_l_squeeze_spots_b").value = '';
    document.getElementById("txt_package").value = '';
    document.getElementById("txt_average_package").value = '';
    document.getElementById("txt_tvc_spots_packege").value = '';
    document.getElementById("txt_logos_spots_packege").value = '';
    document.getElementById("txt_crawler_spots_packege").value = '';
    document.getElementById("txt_v_squeeze_spots_packege").value = '';
    document.getElementById("txt_l_squeeze_spots_packege").value = '';

    b_IsFound = false;
    selectedChannelValue_pub = 0;
    if (jsonchannelUpdateList != '') {
        for (var i in jsonchannelUpdateList) {
            if (jsonchannelUpdateList[i].channelId == channelId[0]) {
                document.getElementById("txt_tvc_spots").value = jsonchannelUpdateList[i].tvc_numOfSpots;
                document.getElementById("txt_tvc_spots_b").value = jsonchannelUpdateList[i].tvc_bonusSpots;
                document.getElementById("txt_logo_spots").value = jsonchannelUpdateList[i].logo_numOfSpots;
                document.getElementById("txt_logo_spots_b").value = jsonchannelUpdateList[i].logo_bonusSpots;
                document.getElementById("txt_Crawler_spots").value = jsonchannelUpdateList[i].crowler_numOfSpots;
                document.getElementById("txt_Crawler_spots_b").value = jsonchannelUpdateList[i].crowler_bonusSpots;
                document.getElementById("txt_v_squeeze_spots").value = jsonchannelUpdateList[i].v_sqeeze_numOfSpots;
                document.getElementById("txt_v_squeeze_spots_b").value = jsonchannelUpdateList[i].v_sqeeze_bonusSpots;
                document.getElementById("txt_l_squeeze_spots").value = jsonchannelUpdateList[i].l_sqeeze_numOfSpots;
                document.getElementById("txt_l_squeeze_spots_b").value = jsonchannelUpdateList[i].l_sqeeze_bonusSpots;
                document.getElementById("txt_package").value = jsonchannelUpdateList[i].package;
                document.getElementById("txt_average_package").value = jsonchannelUpdateList[i].ave_package;
                document.getElementById("txt_tvc_spots_packege").value = jsonchannelUpdateList[i].tvc_package;
                document.getElementById("txt_logos_spots_packege").value = jsonchannelUpdateList[i].logo_package;
                document.getElementById("txt_crawler_spots_packege").value = jsonchannelUpdateList[i].crowler_package;
                document.getElementById("txt_v_squeeze_spots_packege").value = jsonchannelUpdateList[i].v_sqeeze_package;
                document.getElementById("txt_l_squeeze_spots_packege").value = jsonchannelUpdateList[i].l_sqeeze_package;

                b_IsFound = true;
                selectedChannelValue_pub = jsonchannelUpdateList[i].package;
                break;
            }
        }
    }
    if (!b_IsFound) {
        ///tvc spots breakdown => Not Saved channels /////////////////////////
        map_ChannelSpotCountDuration_Bonus[stoi(channelId[0])] = {};
        map_ChannelSpotCountDuration_NonBonus[stoi(channelId[0])] = {};
    }

    ///
    if (!(channelId[0] in map_ChannelSpotCountDuration_Bonus)) {
        map_ChannelSpotCountDuration_Bonus[stoi(channelId[0])] = {};
    }
    if (!(channelId[0] in map_ChannelSpotCountDuration_NonBonus)) {
        map_ChannelSpotCountDuration_NonBonus[stoi(channelId[0])] = {};
    }

    /// set total seconds //
    getTotalDurationfromTVCBreckdown(channelId[0]);
    $('#txt_TotalSeconds').val(viTotalSecondsNBonus + viTotalSecondsBonus);
}

function setSelectedChannelDrop() {
    var drop_channelList = '<select id="selected_ChannelDropdown" name="channelName" onchange="selectedChannelsChange(this.value)" class="form-control middle_select"><option value="-1">Select Channel</option>';
    for (var i in jsonchannelUpdateList)
    {
        for (var j in jsonAllChannelList)
        {
            if (jsonchannelUpdateList[i].channelId == jsonAllChannelList[j].channelId) {
                console.log(jsonAllChannelList[j].channelname);
                drop_channelList += '<option value="' + jsonAllChannelList[j].channelId + '">' + jsonAllChannelList[j].channelname + '</option>';
            }
        }
    }
    drop_channelList += '</select>';
    $('#selectedChannelList').html(drop_channelList);
    selectedChannelsChange(-1);
}

function selectedChannelsChange(channelid) {
    if (channelid == -1) {
        table = '<table class="table table-bordered"><thead><tr><th></th><th>Spots</th><th>Bonus</th><th>Value(Rs)</th></tr></thead><tbody>';
        table += '<tr><td>TVC</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>Logo</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>Crawler</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>V Squeeze</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td>L Squeeze</td><td>0</td><td>0</td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td class="cspan" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Total value</label></td><td align="right" class="currency-input">000.00</td></tr>';
        table += '<tr><td class="cspan2" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Average value</label></td><td align="right" class="currency-input">000.00</td></tr>';
        table += '</tbody></table>';
        $('#selectedChannelDetails').html(table);

        return;
    }
    for (var i in jsonchannelUpdateList)
    {
        if (jsonchannelUpdateList[i].channelId == channelid) {
            var table = '<table class="table table-bordered"><thead><tr><th></th><th>Spots</th><th>Bonus</th><th>Value(Rs)</th></tr></thead><tbody>';
            table += '<tr><td>TVC</td><td>' + jsonchannelUpdateList[i].tvc_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].tvc_bonusSpots + '</td><td align="right" class="currency-input">' + getDecimal(parseFloat(jsonchannelUpdateList[i].tvc_package)) + '</td></tr>';
            table += '<tr><td>Logo</td><td>' + jsonchannelUpdateList[i].logo_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].logo_bonusSpots + '</td><td align="right" class="currency-input">' + getDecimal(parseFloat(jsonchannelUpdateList[i].logo_package)) + '</td></tr>';
            table += '<tr><td>Crawler</td><td>' + jsonchannelUpdateList[i].crowler_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].crowler_bonusSpots + '</td><td align="right" class="currency-input">' + getDecimal(parseFloat(jsonchannelUpdateList[i].crowler_package)) + '</td></tr>';
            table += '<tr><td>V Squeeze</td><td>' + jsonchannelUpdateList[i].v_sqeeze_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].v_sqeeze_bonusSpots + '</td><td align="right" class="currency-input">' + getDecimal(parseFloat(jsonchannelUpdateList[i].v_sqeeze_package)) + '</td></tr>';
            table += '<tr><td>L Squeeze</td><td>' + jsonchannelUpdateList[i].l_sqeeze_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].l_sqeeze_bonusSpots + '</td><td align="right" class="currency-input">' + getDecimal(parseFloat(jsonchannelUpdateList[i].l_sqeeze_package)) + '</td></tr>';
            table += '<tr><td class="cspan" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Total value</label></td><td align="right" class="currency-input">' + getDecimal(parseFloat(jsonchannelUpdateList[i].package)) + '</td></tr>';
            table += '<tr><td class="cspan2" colspan="3"><label class="lable_forms_bottom_se cspan" for="client">Average value</label></td><td align="right" class="currency-input">' + getDecimal(parseFloat(jsonchannelUpdateList[i].ave_package)) + '</td></tr>';
            table += '</tbody></table>';
            $('#selectedChannelDetails').html(table);
            break;
        }
    }
}

function deleteChannel() {
    var ret = confirm("Are you sure you want to remove this Channel?");
    if (!ret) {
        return;
    }
    else {
        var channelId = document.getElementById("selected_ChannelDropdown").value;
        var woId = workOrderId_pub;
        if (channelId != -1) {
            $.ajax({
                type: 'GET',
                dataType: "json",
                async: false,
                contentType: 'application/json',
                url: URI.scheduler.selectedChannelSlotCountUrl(),
                data: {
                    workOrderId: woId,
                    channelId: channelId
                },
                success: function (data)
                {
                    closeDialog();
                    if (data == 0) {
                        for (var i in jsonchannelUpdateList)
                        {
                            if (jsonchannelUpdateList[i].channelId == channelId) {
                                usedAmount_pub = usedAmount_pub - jsonchannelUpdateList[i].package;
                                jsonchannelUpdateList.splice(i, 1);
                            }
                        }
                        setSelectedChannelDrop();
                        calculateTotalBuget();
                    } else {
                        alert("You can't detele this channel. Advertisements are scheduled");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    closeDialog();
                }
            });
        } else {
            alert("Please select channel");
        }
    }
}

function createChannelUpdateTable(chann_lis) {
    var chann_table = '<table id="update_channel_table" class="table"><thead><tr><th>Channel name</th><th>TVC count</th><th>TVC count (Bonus)</th><th>TVC Package value</th><th>Logos count</th><th>Logos count(Bonus)</th><th>Logos Package value</th><th>Crawler overlayer count</th><th>Crawler overlayer count(Bonus)</th><th>Crawler Package value</th><th>V squeeze count</th><th>V squeeze count(Bonus)</th><th>V squeeze Package value</th><th>L squeeze</th><th>L squeeze count(Bonus)</th><th>L squeeze Package value</th><th>Full Package value</th><th>Average Package value</th><th></th></thead><tbody>';
    for (var i in chann_lis)
    {
        chann_table += '<tr id="' + chann_lis[i].orderedchannellistid + 'row" onclick="selectedRow(this)"><td id="' + chann_lis[i].channelid + '">' + chann_lis[i].channelname + '</td><td>' + chann_lis[i].numbertofspot + '</td><td>' + chann_lis[i].bonusSpots + '</td><td>' + chann_lis[i].tvcPackage + '</td><td>' + chann_lis[i].logoSpots + '</td><td>' + chann_lis[i].logo_B_spots + '</td><td>' + chann_lis[i].logoPackage + '</td><td>' + chann_lis[i].crowlerSpots + '</td><td>' + chann_lis[i].crowler_B_Spots + '</td><td>' + chann_lis[i].crowlerPackage + '</td><td>' + chann_lis[i].v_sqeezeSpots + '</td><td>' + chann_lis[i].v_sqeeze_B_Spots + '</td><td>' + chann_lis[i].vSqeezePackage + '</td><td>' + chann_lis[i].l_sqeezeSpots + '</td><td>' + chann_lis[i].l_sqeeze_B_Spots + '</td><td>' + chann_lis[i].lSqeezePackage + '</td><td>' + chann_lis[i].packageValue + '</td><td>' + chann_lis[i].averagePackageValue + '</td><td><button type="submit" id="' + chann_lis[i].orderedchannellistid + '" onclick="update_deleteChannel((this.id))" class="btn btn-success btn-block">Delete</button></td></tr>';
    }
    chann_table += '</tbody></table>';
    $('#updateChannelList').html(chann_table);
}


////////////////////////////////////////////
function channelDetailsChanged(id) {
    if (!spotCountValidation()) {
        return;
    }

    var total_package = calculatePackageTotal();

    if (!checkBuggetLimit(total_package, id)) {
        total_package = calculatePackageTotal();
        var tvc_package = parseFloat($('#txt_tvc_spots_packege').val());
        var average_package = calculateAverageValue(tvc_package);

        $('#txt_average_package').val(parseFloat(average_package).toFixed(2));
        $('#txt_package').val(parseFloat(total_package).toFixed(2));
    } else {
        var tvc_package = stoi($('#txt_tvc_spots_packege').val());
        var average_package = calculateAverageValue(tvc_package);

        $('#txt_average_package').val(parseFloat(average_package).toFixed(2));
        $('#txt_package').val(parseFloat(total_package).toFixed(2));
    }
}

function validateForm() {

    var checker = false;
    if ($('#user_name').val() == '') {
        hasError($("#user_name"), true, 'Select Marketing Ex.');
        checker = true;
    }
    if ($('#product-name').val() == '') {
        hasError($("#product-name"), true, 'Enter Product Name.');
        checker = true;
    }
    if ($('#txt_schedule_ref').val() == '') {
        hasError($("#txt_schedule_ref"), true, 'Enter Schedule Ref.');
        checker = true;
    }
    if ($('#txt_total_budget').val() == '') {
        hasError($("#txt_total_budget"), true, 'Enter Total budget.');
        checker = true;
    }
    if ($('#wo_type_Dropdown').val() == '') {
        hasError($("#wo_type_Dropdown"), true, 'Select WO type.');
        checker = true;
    }
    if ($('#billClientDropdown').val() == '') {
        hasError($("#billClientDropdown"), true, 'Please Select Client.');
        checker = true;
    }
    if ($('#fromDate').val() == '') {
        hasError($("#form-date"), true, 'Select Start Date.');
        checker = true;
    }
    if ($('#toDate').val() == '') {
        hasError($("#to-date"), true, 'Select End Date.');
        checker = true;
    }
    if (checkExistScheduleRef()) {
        hasError($("#txt_schedule_ref"), true, 'There is exist schedule ref.');
        viewMessageToastTyped("There is exist schedule ref.", "error");
        checker = true;
    }
    if (checker) {
        return false;
    }

    if ($('#client_radio').is(':checked')) {
        hasError($("#agencyDropdown"), ($('#agencyDropdown').val() == ''), 'Please Select Agency.');
        hasError($("#clientDropdown"), ($('#clientDropdown').val() == ''), 'Please Select Client.');
        if ($('#clientDropdown').val() == '' || $('#agencyDropdown').val() == '')
            return false;
    }
    else {
        hasError($("#allclientDropdown"), ($('#allclientDropdown').val() == ''), 'Please Select Client.');
        if ($('#allclientDropdown').val() == '')
            return false;
    }

    if (new Date($('#fromDate').val()).getTime() > new Date($('#toDate').val()).getTime()) {
        hasError($("#to-date"), true, 'Invalid End Date.');
        return false;
    }

    if ($('#billClientDropdown').val() == '' || $('#fromDate').val() == '' || $('#toDate').val() == '' ||
            $('#user_name').val() == '' || $('#product-name').val() == '')
        return false;

    return true;
//
}

function spotCountValidation() {
    $('input', '#divChannelDetails').each(function () {
        hasError($(this), false, '')

        var vlaue = $(this).val();
        if (vlaue == null || vlaue == "")
            $(this).val('');
        else if (isNaN(vlaue)) {
            hasError($(this), true, 'error')
            $(this).val('');
        }
    });
    return true;
}

function calculateAverageValue(total_value) {
    var tvc_spots = parseFloat($('#txt_tvc_spots').val());
//    var tvc_spots_b = stoi($('#txt_tvc_spots_b').val());
//    var logo_spots = stoi($('#txt_logo_spots').val());
//    var logo_spots_b = stoi($('#txt_logo_spots_b').val());
//    var crawler_spots = stoi($('#txt_Crawler_spots').val());
//    var crawler_spots_b = stoi($('#txt_Crawler_spots_b').val());
//    var v_squeeze_spots = stoi($('#txt_v_squeeze_spots').val());
//    var v_squeeze_spots_b = stoi($('#txt_v_squeeze_spots_b').val());
//    var l_squeeze_spots = stoi($('#txt_l_squeeze_spots').val());
//    var l_squeeze_spots_b = stoi($('#txt_l_squeeze_spots_b').val());
//    stoi($('#txt_TotalSeconds').val());

    var total_spots = tvc_spots;// + tvc_spots_b + logo_spots + logo_spots_b + crawler_spots + crawler_spots_b + v_squeeze_spots + v_squeeze_spots_b + l_squeeze_spots + l_squeeze_spots_b;


    if (total_spots > 0) {
        return (total_value / total_spots);
    } else {
        return 0;
    }
}

function checkBuggetLimit(total_value, id) {
    var v = (usedAmount_pub - selectedChannelValue_pub + total_value);
    v = v.toFixed(2);
    if (totalBudget_pub < v) {
        alert("Budget over limit");
        document.getElementById(id).value = "";
        return false;
    } else {
        return true;
    }
}

function calculatePackageTotal() {
    var tvc_p_va = $('#txt_tvc_spots_packege').val();
    var tvc_p = 0;
    if (tvc_p_va != "") {
        tvc_p = parseFloat(tvc_p_va);
    }
    var logo_p_va = $('#txt_logos_spots_packege').val()
    var logo_p = 0;
    if (logo_p_va != "") {
        logo_p = parseFloat(logo_p_va);
    }
    var crow_p_va = $('#txt_crawler_spots_packege').val();
    var crow_p = 0;
    if (crow_p_va != "") {
        crow_p = parseFloat(crow_p_va);
    }
    var l_sqeeze_p_va = $('#txt_v_squeeze_spots_packege').val();
    var l_sqeeze_p = 0;
    if (l_sqeeze_p_va != "") {
        l_sqeeze_p = parseFloat(l_sqeeze_p_va);
    }
    var v_sqees_p_va = $('#txt_l_squeeze_spots_packege').val();
    var v_sqees_p = 0;
    if (v_sqees_p_va != "") {
        v_sqees_p = parseFloat(v_sqees_p_va);
    }
    var newoTalValue = tvc_p + logo_p + crow_p + l_sqeeze_p + v_sqees_p;
    return newoTalValue;
}

function stoi(value) {
    if (value == null || value == "" || isNaN(value))
        return 0;
    else
        return parseInt(value);
}

/*Write*/
function updateWorkOrder() {
    showDialog();
    if (!validateForm()){
        closeDialog();
        return;
    }

    if (jsonchannelUpdateList == "") {
        closeDialog();
        alert("Please add a Channel.");
        return;
    }

    var v_workOrderId = workOrderId_pub;//document.getElementById("workOrderId").value;//stoi(getParameterByNamefromUrl('work_id'));
    var v_productName = document.getElementById("product-name").value;
    var v_startDate = document.getElementById("fromDate").value;
    var v_endDate = document.getElementById("toDate").value;
    var v_comment = document.getElementById("comment").value;
    v_comment = v_comment.split("\n").join("\\n");
    var V_billClient = document.getElementById("billClientDropdown").value;
    var v_agecny = '';
    var v_client = '';

    if (document.getElementById('client_radio').checked) {
        v_agecny = $('#agencyDropdown').val();
        v_client = $('#clientDropdown').val();
    } else {
        v_client = $('#allclientDropdown').val();
        v_agecny = 0;
    }

    var v_channel_List = JSON.stringify(jsonchannelUpdateList);

    var v_visibility = '0';
    if (document.getElementById('visible').checked)
        v_visibility = 1;
    else
        v_visibility = 2;

    var v_advert_List = JSON.stringify(lstAverts);
    var user = document.getElementById("user_name").value;

    var v_wo_type = $('#wo_type_Dropdown').val();
    var v_total_budget = $('#txt_total_budget').val().replace(/,/g, "");
    var v_schedule_ref = $('#txt_schedule_ref').val();
    var v_commission = '';
    var v_revenue_month = $('#revenue-month').val();
    var v_manual_status = $('#manual-status').val();
    if (document.getElementById('commison_15').checked) {
        v_commission = 15;
    } else {
        v_commission = 0;
    }
    var updateData = '{"userName":"' + user + '","workOrderId":' + v_workOrderId + ',"productName":"' + v_productName + '","startDate":"' + v_startDate + '","endDate":"' + v_endDate + '","comment":"' + v_comment + '","visibility":' + v_visibility + ',"client":' + v_client + ',"agency":' + v_agecny + ',"billClient":' + V_billClient + ',"woType":"' + v_wo_type + '","scheduleRef":"' + v_schedule_ref + '","commission":' + v_commission + ',"totalBudget":' + v_total_budget + ',"revenueMonth":"' + v_revenue_month + '","manualStatus":"' + v_manual_status + '"}';

    $.ajax({
        type: 'GET',
        dataType: "json",
        async: true,
        contentType: 'application/json',
        url: URI.workOrder.workOrderUpdateUrl(),
        data: {
            workOrderData: updateData,
            channelData: v_channel_List,
            advertList: v_advert_List
        },
        success: function (data)
        {
            closeDialog();
            alert("Successfully Updated ");
            window.location = URI.workOrder.updateWorkOrderUrl(v_workOrderId);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

/*Read*/
function getHistoryDetails() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.audit.getWorkOredrAuditUrl(),
        data: {
            workOrderId: workOrderId_pub
        },
        success: function (data)
        {
            closeDialog();

            var trHTML = '';
            trHTML += '<table id="workOrderHistory_table_pop" class="table table-responsive table-bordered table-workorder">';
            trHTML += '<thead><tr><th class="col-md-1">Time</th><th class="col-md-2">Field</th><th class="col-md-4">Old</th><th class="col-md-4">New</th><th class="col-md-1">User</th></tr></thead>';
            trHTML += '<tbody>';

            for (var i in data)
            {
                trHTML += '<tr><td class="col-md-2">' + data[i].date + '</td><td class="col-md-2">' + data[i].field + '</td><td class="col-md-3">' + data[i].oldData + '</td><td class="col-md-4">' + data[i].newData + '</td><td class="col-md-1">' + data[i].user + '</td></tr>';
            }
            trHTML += '</tbody>';
            trHTML += '</table>';

            $('#workOrderHistory_table_pop_div').html(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

//task_139 ///////////////////////////////////////////////
$(document).ready(function () {
});

function advetListCollapse(element) {
    if ($(element).text() == String.fromCharCode(9650)) {
        $('#advert_list_table').hide();
        $(element).text(String.fromCharCode(9660));
    }
    else {
        $('#advert_list_table').show();
        $(element).text(String.fromCharCode(9650));
    }
}

function attachmentsListCollapse(element) {
    if ($(element).text() == String.fromCharCode(9650)) {
        $('#attachment_table').hide();
        $(element).text(String.fromCharCode(9660));
    }
    else {
        $('#attachment_table').show();
        $(element).text(String.fromCharCode(9650));
    }
}

/*Read*/
function setAdvertisementDetails()//
{
    var v_clienName = '';
    if (document.getElementById('client_radio').checked) {
        v_clienName = document.getElementById("clientDropdown");
    } else {
        v_clienName = document.getElementById("allclientDropdown");

    }
    var clientID = v_clienName.options[v_clienName.selectedIndex].id;

    var v_billClient = document.getElementById("billClientDropdown");
    var billClientID = v_billClient.options[v_billClient.selectedIndex].id;

    if (clientID == -1 && billClientID == -1) {
        return;
    }

    showDialog();
    var advertDrop = '<select id="advertDropdown_pop" name="channelName" class="form-control selectpicker" data-live-search="true"><option id="-1" value="-1">Select Advertsement</option>';
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementSelectedClientListUrl(),
        data: {
            clieanOne: clientID,
            clieanTwo: billClientID
        },
        success: function (data)
        {
            closeDialog();
            for (var i in data) {
                advertDrop += '<option id=' + data[i].advertid + 'data-tokens="' + data[i].advertname + '" value="' + data[i].advertid + '">' + data[i].advertid + '-' + data[i].advertname + '</option>';
                mapAdverts[data[i].advertid] = data[i];
            }

            advertDrop += '</select>';
            $('#advert_list_pop_div').html(advertDrop);
            $('#advert_list_table').hide();
            $('#advertDropdown_pop').selectpicker('refresh');

        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
    $('.selectpicker').selectpicker({
        size: 10
    });
}

/*Read*/
function loadAdvertTable()
{
    showDialog();
    lstAverts = [];
    var v_workOrderId = stoi(getParameterByNamefromUrl('work_id'));
    console.log(v_workOrderId);
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.clientOrderAdvertListUrl(),
        data: {
            workOrderId: v_workOrderId,
            advertType: 'All'
        },
        success: function (data)
        {
            for (var i in data) {
                mapAdverts[data[i].advertid] = data[i];
            }
            closeDialog();
            advertTableMain = '';
            advertTablePopup = '';
            advert_count = 0;
            advertTableMain = '';
            for (var i in data)
            {
                advert = data[i];
                advertTableMain += '<tr><td id="' + advert.advertId + '"><a href="#" id="' + advert.advertId + '" onclick="getId(this.id)">' + advert.advertId + '</a></td><td>' + advert.advertName + '</td><td>' + setLanguage(advert.language) + '</td><td>' + advert.duration + '</td><td>' + getAdvertType(advert.type) + '</td><td>' + advert.lastModifydate + '</td></tr>';
                advertTablePopup += '<tr id=tr_' + advert.advertId + '><td id="' + advert.advertId + '"><a href="#" id="' + advert.advertId + '" onclick="getId(this.id)">' + advert.advertId + '</a></td><td>' + advert.advertName + '</td><td>' + setLanguage(advert.language) + '</td><td>' + advert.duration + '</td><td>' + getAdvertType(advert.type) + '</td><td>' + advert.lastModifydate + '</td><td style="width:20px;"><button type="button" class="close close-icon" aria-label="Close" style="font-size:16px;color:red;" onclick="removeAdvert(' + advert.advertId + ')"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
                lstAverts.push(advert.advertId);
                addedAdverts[advert.advertId] = advert;
                $("#advertDropdown_pop option[value='" + advert.advertId + "']").remove();
                console.log(advert.advertId);
                advert_count++;
            }
            $('#advert_main_table tbody').html(advertTableMain);
            $('#advertisement_table_pop tbody').html(advertTablePopup);
            $('#lbl_advert_count').html(advert_count + ' Advertisements');
            $('#advertDropdown_pop').selectpicker('refresh');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function getId(id) {
    window.open(URI.advertisement.updateAdvertisementUrl(id));
}

function setAdvertTable() {
    var flag = true;
    advertTableMain = '';
    advert_count = 0;
    lstAverts.forEach(function (id) {
        advert = mapAdverts[id];
        if(advert == undefined ){
            advert = addedAdverts[id]
            flag = false;
        }else{
            flag = true;
        }

        if(flag){
            advertTableMain += '<tr><td id="' + advert.advertid + '"><a href="#" id="' + advert.advertid + '" onclick="getId(this.id)">' + advert.advertid + '</a></td><td>' + advert.advertname + '</td><td>' + setLanguage(advert.language) + '</td><td>' + advert.duration + '</td><td>' + getAdvertType(advert.adverttype) + '</td><td>' + advert.lastModifydate + '</td> </tr>';
        }else{
            advertTableMain += '<tr><td id="' + advert.advertId + '"><a href="#" id="' + advert.advertId + '" onclick="getId(this.id)">' + advert.advertId + '</a></td><td>' + advert.advertName + '</td><td>' + setLanguage(advert.language) + '</td><td>' + advert.duration + '</td><td>' + getAdvertType(advert.type) + '</td><td>' + advert.lastModifydate + '</td> </tr>';
        }
        console.log(id);
        advert_count++;
    });
    $('#advert_main_table tbody').html(advertTableMain);
    $('#lbl_advert_count').html(advert_count + ' Advertisements');
}

function addAdvertisement() {
    advert_id = $("#advertDropdown_pop").val();
    if (advert_id == -1) {
        return;
    }

    var alredy = false;
    $('#advertisement_table_pop tr').each(function (i, row) {
        if (i > 0) {
            cell = row.cells[0];
            var advertID = stoi(cell.id);
            if (advert_id == advertID) {
                alredy = true;
            }
        }
    });

    if (alredy) {
        alert("This advertisement alredy added.");
        return;
    }

    advert = mapAdverts[advert_id];
    row_html = '<tr id=tr_' + advert.advertid + '><td id="' + advert.advertid + '"><a href="#" id="' + advert.advertid + '" onclick="getId(this.id)">' + advert.advertid + '</a></td><td>' + advert.advertname + '</td><td>' + setLanguage(advert.language) + '</td><td>' + advert.duration + '</td><td>' + getAdvertType(advert.adverttype) + '</td><td>' + advert.lastModifydate + '</td><td style="width:20px;"><button type="button" class="close close-icon" aria-label="Close" style="font-size:16px;color:red;" onclick="removeAdvert(' + advert.advertid + ')"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
    $('#advertisement_table_pop tbody').append(row_html);

    $("#advertDropdown_pop option[value='" + advert_id + "']").remove();
    $('#advertDropdown_pop').selectpicker('refresh');
}

function removeAdvert(id) {
    var advert = addedAdverts[id];
    if(advert !== undefined){
        if( advert.canDelete){
            $('#tr_' + id).remove();
            advert = mapAdverts[id];
            $('#advertDropdown_pop').append($('<option>', {value: advert.advertid, text: advert.advertname}));
            $('#advertDropdown_pop').selectpicker('refresh');
        }else{
            viewMessageToastTyped("This advertisement is alredy scheduled.", "Warning");
        }
    }else{
        $('#tr_' + id).remove();
        advert = mapAdverts[id];
        $('#advertDropdown_pop').append($('<option>', {value: advert.advertid, text: advert.advertname}));
        $('#advertDropdown_pop').selectpicker('refresh');
    }

}

function saveAdvertisements() {
    lstAverts = [];

    $('#advertisement_table_pop tr').each(function (i, row) {
        if (i > 0) {
            cell = row.cells[0];
            advert_id = stoi(cell.id);
            console.log(advert_id);
            lstAverts.push(advert_id);
        }
    });
     setAdvertTable();
      viewMessageToastTyped("Advertisement saved successfully", "Success");
      $('#addAdvertisementModal').modal('toggle');
}


///Task_141 //////////////////////////////////////////
var b_IsBonusTVCSpotPopup = false;
var map_ChannelSpotCountDuration_NonBonus = {};
var map_ChannelSpotCountDuration_Bonus = {};
var map_SpotCountDuration = {};
var map_SpotCountDuration_NonBonus = {};

function onclickTVCSpotCount() {
    if (billStatus != 0) {
        alert("you can't change spot count");
        return;
    }
    iChannelId = stoi($('#channelDropdown').val().split("|")[0]);
    if (iChannelId == -111)
        return;

    b_IsBonusTVCSpotPopup = false;
    map_SpotCountDuration = map_ChannelSpotCountDuration_NonBonus[iChannelId];
    $('#spotcount_view_popup').modal('toggle');
    bonous = false;
    createSpotCountTable();
}

function onclickTVCSpotCountBonus() {
    iChannelId = $('#channelDropdown').val().split("|")[0];
    if (iChannelId == -111)
        return;

    b_IsBonusTVCSpotPopup = true;
    map_SpotCountDuration = map_ChannelSpotCountDuration_Bonus[iChannelId];
    map_SpotCountDuration_NonBonus = map_ChannelSpotCountDuration_NonBonus[iChannelId];
    $('#spotcount_view_popup').modal('toggle');
    bonous = true;
    createSpotCountTable();
}

function addSpotCount() {
    iDuration = stoi($('#txt_duration').val());
    iSpotCount = stoi($('#txt_spots').val());

    if (iDuration in map_SpotCountDuration)
        map_SpotCountDuration[iDuration] = map_SpotCountDuration[iDuration] + iSpotCount;
    else
        map_SpotCountDuration[iDuration] = iSpotCount;
    createSpotCountTable();
}

function removeRow(id) {
    var checkUpdate = true;
    var totalNonBonus = 0;

    if (map_SpotCountDuration_NonBonus.length != 0) {
        totalNonBonus = map_SpotCountDuration_NonBonus[id];
    }

    for (var i in totalAdvertCount)
    {
        if ((totalAdvertCount[i].channelID == iChannelId) && (totalAdvertCount[i].advertDuration == id)) {
            if (bonous) {
                var bonusSpot = totalAdvertCount[i].advetCount - totalNonBonus;
                map_SpotCountDuration[id] = bonusSpot;
                checkUpdate = false;
            } else {
                if (totalAdvertCount[i].advetCount > totalNonBonus) {
                    map_SpotCountDuration[id] = totalNonBonus;
                    checkUpdate = false;
                } else {
                    map_SpotCountDuration[id] = totalAdvertCount[i].advetCount;
                    checkUpdate = false;
                }

            }
        }
    }
    if (checkUpdate) {
        delete map_SpotCountDuration[id];
    }
    createSpotCountTable();
}

function createSpotCountTable() {
    tblData = '';
    i_TotalSeconds = 0;
    for (var key in map_SpotCountDuration) {
        tblData += '<tr id="row_spot_' + key + '"><td>' + key + '</td><td>' + map_SpotCountDuration[key] + '</td><td style="width:20px;"><button type="button" class="close close-icon" aria-label="Close" style="font-size:16px;color:red;" onclick="removeRow(' + key + ')"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';

        iDuration = key;
        iSpotCount = map_SpotCountDuration[key];

        iSeconds = iDuration * iSpotCount;
        i_TotalSeconds += iSeconds;
    }
    $('#tblSpotCount tbody').html(tblData);
    $('#lbl_TotalCount').html(Math.ceil(i_TotalSeconds / 30));
    setRemainSpotPerScecond();
}

function setRemainSpotPerScecond() {

    showDialog();
    var v_workOrderId = stoi(getParameterByNamefromUrl('work_id'));
    console.log(v_workOrderId);
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getWorkOrderRemainSecondUrl(),
        data: {
            workOrderId: v_workOrderId,
            channelId: iChannelId
        },
        success: function (data)
        {
            closeDialog();
            var remainingDuration = 0;
            usedSpotArray = data.remainList;
            totalAdvertCount = data.totalAdvertCount;

            if (usedSpotArray.length == 0) {
                for (var i in map_SpotCountDuration)
                {
                    var remaininSpot = map_SpotCountDuration[i];
                    var remaininSecon = remaininSpot * i;
                    remainingDuration += remaininSecon;
                }
            }
            for (var i in usedSpotArray)
            {
                if (usedSpotArray[i].channelID == iChannelId) {
                    var totalSpot = map_SpotCountDuration[usedSpotArray[i].advertDuration];
                    var usedSpot = usedSpotArray[i].advetCount;
                    if (map_SpotCountDuration_NonBonus.length != 0 && bonous) {
                        var totalNonBonusSpot = map_SpotCountDuration_NonBonus[usedSpotArray[i].advertDuration];
                        if (totalNonBonusSpot < usedSpot) {
                            usedSpot = usedSpot - totalNonBonusSpot;
                        } else {
                            usedSpot = 0;
                        }
                    }
                    if (usedSpot > totalSpot) {
                        usedSpot = totalSpot;
                    }
                    var remaininSpot = totalSpot - usedSpot;
                    var remaininSecon = remaininSpot * usedSpotArray[i].advertDuration;
                    remainingDuration += remaininSecon;
                }
            }

            $('#lbl_TotalRemainCount').html(remainingDuration);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

}

function saveSpotCount() {
    i_TotalSeconds = 0;
    for (var key in map_SpotCountDuration) {
        iDuration = key;
        iSpotCount = map_SpotCountDuration[key];

        iSeconds = iDuration * iSpotCount;
        i_TotalSeconds += iSeconds;
    }
    i_SpotCount = Math.ceil(i_TotalSeconds / 30);

    if (b_IsBonusTVCSpotPopup) {
        $('#txt_tvc_spots_b').val(i_SpotCount);
        viTotalSecondsBonus = i_TotalSeconds;
    }
    else {
        $('#txt_tvc_spots').val(i_SpotCount);
        viTotalSecondsNBonus = i_TotalSeconds;
    }
    $('#spotcount_view_popup').modal('toggle');
    $('#txt_TotalSeconds').val(viTotalSecondsNBonus + viTotalSecondsBonus);

    tvc_package = stoi($('#txt_tvc_spots_packege').val());
    $('#txt_average_package').val(parseFloat(calculateAverageValue(tvc_package)).toFixed(2));
}

function numberValidation_el(element) {
    hasError($(element), false, '');

    var vlaue = $(element).val();
    if (vlaue == null || vlaue == "") {
        $(element).val('');
        return false;
    }
    else if (isNaN(vlaue)) {
        hasError($(element), true, 'error')
        $(element).val('');
        return false;
    }
    return true;
}

function getTotalDurationfromTVCBreckdown(iChannelId) {
    viTotalSecondsNBonus = 0;
    viTotalSecondsBonus = 0;

    mapSpotCount = map_ChannelSpotCountDuration_NonBonus[iChannelId];
    for (var key in mapSpotCount) {
        iDuration = stoi(key) * mapSpotCount[key];
        viTotalSecondsNBonus += iDuration;
    }

    mapSpotCount = map_ChannelSpotCountDuration_Bonus[iChannelId];
    for (var key in mapSpotCount) {
        iDuration = stoi(key) * mapSpotCount[key];
        viTotalSecondsBonus += iDuration;
    }
}

function setBudgetAfterValidation(value){
    if(numberValidation(value)){
        setBudget(value);
    }
}

/*--------Task 137-------*/
function setBudget(value) {
    totalBudget_pub = parseFloat(value).toFixed(2);
    calculateTotalBuget();
}

function calculateTotalBuget() {
    var budgetLbl = '<label class="col-md-9 lbl_total_budget">Total budget : ' + getDecimal(parseFloat(usedAmount_pub)) + '/' + getDecimal(parseFloat(totalBudget_pub)) + '</label>';
    $('#lbl_total_budget').html(budgetLbl);
}
/*------Task 137 end-----*/

/*--------Task 138-------*/
function channelSummery() {

    var summeryTable = '<table id="channel_summery_table_pop" class="table table-bordered table-workorder">';
    summeryTable += '<thead><tr><th>Channel</th><th>Type</th><th>Duration</th><th>Spot</th><th>Bonus</th><th>Value</th></tr></thead><tbody>';

    var totalbuget = 0.0;
    for (var i in jsonchannelUpdateList)
    {
        if (jsonchannelUpdateList[i].tvc_numOfSpots !== 0 || jsonchannelUpdateList[i].tvc_bonusSpots !== 0) {
            var tvcBreakDown = jsonchannelUpdateList[i].tvc_brkdown;
            var tvcBreakDown_bons = jsonchannelUpdateList[i].tvc_brkdown_b;
            var rowCount = 0;
            var firstRow = '';
            var otherRow = '';
            for (var key in tvcBreakDown) {
                var bonusSpot = tvcBreakDown_bons[key];
                if (bonusSpot === undefined) {
                    bonusSpot = 0;
                }
                if (rowCount === 0) {
                    summeryTable += '<tr><td rowspan="';
                    firstRow += '">' + getChannelName(jsonchannelUpdateList[i].channelId) + '</td><td>TVC</td><td>' + key + '</td><td>' + tvcBreakDown[key] + '</td><td>' + bonusSpot + '</td><td rowspan="';
                    otherRow += ' class="currency-input">' + getDecimal(jsonchannelUpdateList[i].tvc_package) + '</td></tr>';
                } else {
                    otherRow += '<tr><td>TVC</td><td>' + key + '</td><td>' + tvcBreakDown[key] + '</td><td>' + bonusSpot + '</td></tr>';
                }
                rowCount++;
            }
            for (var key_b in tvcBreakDown_bons) {
                var spot = tvcBreakDown[key_b];
                if (spot === undefined) {
                    if (rowCount === 0) {
                        summeryTable += '<tr><td rowspan="';
                        firstRow += '">' + getChannelName(jsonchannelUpdateList[i].channelId) + '</td><td>TVC</td><td>' + key_b + '</td><td>0</td><td>' + tvcBreakDown_bons[key_b] + '</td><td rowspan="';
                        otherRow += ' class="currency-input">' + getDecimal(jsonchannelUpdateList[i].tvc_package) + '</td></tr>';
                    } else {
                        otherRow += '<tr><td>TVC</td><td>' + key_b + '</td><td>0</td><td>' + tvcBreakDown_bons[key_b] + '</td></tr>';
                    }
                    rowCount++;
                }
            }
            totalbuget += jsonchannelUpdateList[i].tvc_package;
            summeryTable += rowCount + firstRow + rowCount + otherRow;
        }
        if (jsonchannelUpdateList[i].logo_numOfSpots !== 0 || jsonchannelUpdateList[i].logo_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonchannelUpdateList[i].channelId) + '</td><td>Logo</td><td>0</td><td>' + jsonchannelUpdateList[i].logo_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].logo_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonchannelUpdateList[i].logo_package) + '</td></tr>';
            totalbuget += jsonchannelUpdateList[i].crowler_package;
        }
        if (jsonchannelUpdateList[i].crowler_numOfSpots !== 0 || jsonchannelUpdateList[i].crowler_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonchannelUpdateList[i].channelId) + '</td><td>Crowler</td><td>0</td><td>' + jsonchannelUpdateList[i].crowler_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].crowler_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonchannelUpdateList[i].crowler_package) + '</td></tr>';
            totalbuget += jsonchannelUpdateList[i].logo_package;
        }
        if (jsonchannelUpdateList[i].v_sqeeze_numOfSpots !== 0 || jsonchannelUpdateList[i].v_sqeeze_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonchannelUpdateList[i].channelId) + '</td><td>Vsqueeze</td><td>0</td><td>' + jsonchannelUpdateList[i].v_sqeeze_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].v_sqeeze_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonchannelUpdateList[i].v_sqeeze_package) + '</td></tr>';
            totalbuget += jsonchannelUpdateList[i].v_sqeeze_package;
        }
        if (jsonchannelUpdateList[i].l_sqeeze_numOfSpots !== 0 || jsonchannelUpdateList[i].l_sqeeze_bonusSpots !== 0) {
            summeryTable += '<tr><td>' + getChannelName(jsonchannelUpdateList[i].channelId) + '</td><td>Lsqueeze</td><td>0</td><td>' + jsonchannelUpdateList[i].l_sqeeze_numOfSpots + '</td><td>' + jsonchannelUpdateList[i].l_sqeeze_bonusSpots + '</td><td class="currency-input">' + getDecimal(jsonchannelUpdateList[i].l_sqeeze_package) + '</td></tr>';
            totalbuget += jsonchannelUpdateList[i].l_sqeeze_package;
        }
    }
    summeryTable += '<tr><td colspan="5">Total value</td><td>' + getDecimal(totalbuget) + '</td></tr>';
    summeryTable += '</tbody></table>';
    $('#channel_summery_table_pop_div').html(summeryTable);
}

function getChannelName(channelId) {

    for (var i in jsonAllChannelList)
    {
        if (jsonAllChannelList[i].channelId == channelId) {
            return jsonAllChannelList[i].channelname;
        }
    }
}
/*------Task 138 end-----
 * <td class="cspan2" colspan="3">
 * */

///Task_140 //////////////////////////////////////////
iCommission = 0;
iNBT = 0;
iVAT = 0;
iTeleLvy = 0;
iOtherLvy = 0;

function onclickTotalBudgetTxt(val) {

    if (billStatus != 0) {
        alert("you can't change  total budget");
        return;
    }
    setTaxDetails(val);

    $('#budgetplan_view_popup').modal('toggle');
}

function setTotalBudget() {
    iTotalBudget = parseFloat($('#txt_WithoutTax').val().replace(",", "")).toFixed(2);
    if (usedAmount_pub > iTotalBudget) {
        alert("The budget less than used amount");
        return;
    }
    if (iTotalBudget == "NaN")
        iTotalBudget = 0;
    $('#txt_total_budget').val(getDecimal(parseFloat(iTotalBudget)));

    $('#txt_WithoutTax').val("");
    $('#budgetplan_view_popup').modal('toggle');
    setBudget(iTotalBudget);
}

/*Read*/
function setTaxDetails(val) {
    showDialog();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.bill.getAllTaxDetailsUrl(),
        success: function (data)
        {
            closeDialog();
            data.forEach(function (item) {
                if (item.taxcategory == "NBT")
                    iNBT = item.rate;
                else if (item.taxcategory == "VAT")
                    iVAT = item.rate;
                else if (item.taxcategory == "Telecommunication Levy")
                    iTeleLvy = item.rate;
                else if (item.taxcategory == "Other Government Levy")
                    iOtherLvy = item.rate;
            });

            if ($("#commison_15").prop("checked"))
                iCommission = 15;
            else
                iCommission = 0;

            $('#lbl_Commission').html(iCommission + '%');
            $('#lbl_NBT').html(iNBT + '%');
            $('#lbl_VAT').html(iVAT + '%');
            $('#lbl_TeleLevy').html(iTeleLvy + '%');
            $('#lbl_OtherLevy').html(iOtherLvy + '%');

            $("#txt_WithoutTax").val(val);
            var withoutTax = calculateTotalWithTax(parseFloat(val.replace(",","")));
            $('#txt_WithTax').val(withoutTax.toFixed(2));
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function  txt_WOTaxChanged() {
    if (!numberValidation_el($('#txt_WithoutTax'))) {
        $('#txt_WithTax').val("");
        return;
    }
    iTotal = calculateTotalWithTax(parseFloat($('#txt_WithoutTax').val()));
    $('#txt_WithTax').val(iTotal.toFixed(2));
}

function  txt_WithTaxChanged() {
    if (!numberValidation_el($('#txt_WithTax'))) {
        $('#txt_WithoutTax').val("");
        return;
    }
    iTotal = calculateTotalWOTax(parseFloat($('#txt_WithTax').val()));
    $('#txt_WithoutTax').val(iTotal.toFixed(2));
}

function calculateTotalWOTax(iTotal) {
    iTotal = iTotal / (1 + (iVAT + iTeleLvy + iOtherLvy) / 100);
    iTotal = iTotal / (1 + iNBT / 100);
    iTotal = iTotal / (1 - iCommission / 100);

    return iTotal;
}

function calculateTotalWithTax(iTotal) {
    iTotal -= (iTotal * iCommission) / 100;
    iTotal += (iTotal * iNBT) / 100;
    iTotal += (iTotal * (iVAT + iTeleLvy + iOtherLvy)) / 100;
    return iTotal;
}

function holdWo() {
    var ret = confirm("Do you want to hold this Work order?");
    if (!ret) {
        return;
    }
    showDialog();
    var id = stoi(getParameterByNamefromUrl('work_id'));

    ///get work order details from service
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.holdWorkOrderUrl(),
        data: {
            workOrderId: id
        },
        success: function (data)
        {
            if (data) {
                alert("Ok");
                window.location = URI.workOrder.updateWorkOrderUrl(id);
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error " + textStatus);
                alert("Error " + errorThrown);
                alert("Error " + jqXHR);
            }
        }
    });
}

function attachmentView() {
    var id = document.getElementById("txt_wo_id").value;
    var attachmentPopUp = new Attachment('work_order', id);
    attachmentPopUp.createView(id);
}

function getAttachmentDetais() {
    var id = document.getElementById("txt_wo_id").value;
    showDialog();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.attachments.getAttachmentsUrl(),
        data: {
            table: "work_order",
            id: id
        },
        success: function (data)
        {
            var attachmentCount = 0;
            closeDialog();
            var tblBody = '';
            $.each(data, function (i, item) {
                tblBody += '<tr>';
                tblBody += '<td>' + item.name + '</td>';
                tblBody += '<td>' + item.createdUser + '</td>';
                tblBody += '<td>' + item.createdDate.split('.')[0] + '</td>';
                tblBody += '<td>' + item.description + '</td>';
                tblBody += '<td>' + '<a href="' + URI.attachments.downloadAttachmentUrl(item.attachmentsId) + '"><span class="glyphicon glyphicon-download-alt"></a>' + '</td>';
                tblBody += '<tr>';
                attachmentCount++;
            });
            $('#attachment_table tbody').html(tblBody);
            $('#lbl_attachments_count').html(attachmentCount + ' Attachments');
            $('#attachment_table').hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function checkExistScheduleRef() {
    var ref = $('#txt_schedule_ref').val();
    var respons = false;
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.checkExistScheduleRefForUpdateUrl(),
        data: {
            scheduleRefe: ref,
            workOrderId: workOrderId_pub
        },
        success: function (data)
        {
            respons = data;
        },
        error: function (jqXHR, textStatus, thrownError) {
            respons = false;
        }
    });

    return respons;
}

function resumeWo(){
    var ret = confirm("Are you sure you want to resume this work oredr?");
    if (!ret) {
        return;
    }

    showDialog();
    var workOrderId = $("#txt_wo_id").val();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.resumeWorkOrderUrl(),
        data: {
            workOrderId : workOrderId,
        },
        success: function (data)
        {
            closeDialog();
            var suspendBn = '<button type="button" class="btn btn_inactive add admin_add" onclick="holdWo()">Suspend</button>';
            $('#suspend-and-resume-id').html(suspendBn);
            alert("successfully Resume");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");
        }
    });
}
