function numberValidation(num)
{
    if (isNaN(num))
    {
        alert("Please enter number");
        return false;
    } else {
        return true;
    }
}

function setLanguage(lang) {
    switch (lang) {
        case "ENG":
            return "Eng";
            break;

        case "EN":
            return "Eng";
            break;

        case "SI":
            return "Sin";
            break;

        case "TA":
            return "Tam";
            break;

        default:
            return lang;
            break;
    }
}

function setLanguage_returnFistLetter(lang) {
    switch (lang) {
        case "Eng":
            return "E";
            break;

        case "EN":
            return "E";
            break;

        case "SI":
            return "S";
            break;

        case "Sin":
            return "S";
            break;

        case "TA":
            return "T";
            break;

        case "Tam":
            return "T";
            break;

        default:
            return lang;
            break;
    }
}

function getAdvertType(type) {
    switch (type) {
        case "ADVERT":
            return "TVC";
            break;

        case "CRAWLER":
            return "Crawler";
            break;

        case "LOGO":
            return "Logo";
            break;

        case "V_SHAPE":
            return "V squeeze";
            break;

        case "L_SHAPE":
            return "L squeeze";
            break;

        case "FILLER":
            return "Filler";
            break;

        case "SLIDE":
            return "Slide";
            break;

        default:
            return type;
            break;
    }
}

function RemoveSec(time) {
    var realTime = time.split(":");
    return realTime[0] + ':' + realTime[1];
}
