var URI = {};

var basePath = window.location.origin;

URI.workOrder = URI.workOrder || (function(){
    return {
         mainPageUrl : function(){
            return basePath + '/workorder';
         },
         allClientDetailsUrl : function() {
             return basePath + '/workOrder/allClientDetails/json';
         },
         workOrderListUrl : function() {
             return basePath + '/workOrder/workOrderList/page';
         },
         workOrderListJsonUrl : function() {
             return basePath + '/workOrder/workOrderList/json';
         },
         workOrderSaveUrl : function(){
             return basePath + '/workOrder/workOrderSave/json';
         },
         checkExistScheduleRefUrl : function(){
             return basePath + '/workOrder/checkExistScheduleRef/json?';
         },
         workOrderListForScheduleUrl : function(){
             return basePath + '/workOrder/workOrderListForSchedule/page';
         },
         workOrderChannelListUrl : function(){
             return basePath + '/workOrder/workOrderChannelList/json';
         },
         workOrderListForBillingUrl : function(){
             return basePath + '/workOrder/workOrderListForBilling/page';
         },
         allWorkOrderListUrl : function(){
             return basePath + '/workOrder/allWorkOrderList/json';
         },
         selectedWorkOrderUrl : function(){
             return basePath + '/workOrder/selectedWorkOrder/json';
         },
         orderAdvertListSaveUrl : function(){
             return basePath + '/workOrder/orderAdvertListSave/json';
         },
         holdWorkOrderUrl : function() {
             return basePath +  '/workOrder/holdWorkOrder/json?';
         },
         resumeWorkOrderUrl : function() {
            return basePath +  '/workOrder/resumeWorkOrder/json?';
         },
         checkExistScheduleRefForUpdateUrl : function() {
             return basePath +  '/workOrder/checkExistScheduleRefForUpdate/json?';
         },
         workOrderUpdateUrl : function() {
             return basePath +  '/workOrder/workOrderUpdate/json';
         },
         updateWorkOrderUrl : function(v_workOrderId) {
             return basePath +  '/workorder/update?work_id=' + v_workOrderId;
         }
    }
}());

URI.userDetails = URI.userDetails || (function(){
    return {
         getMeUserUrl : function() {
             return basePath + '/userdetaills/getMeUser/json';
         },
         allUserUrl : function() {
             return basePath + '/userdetaills/allUser/json';
         },
         newuserUrl : function(){
             return basePath + '/userdetaills/newuser/json';
         },
         getAllUserGroupUrl : function(){
             return basePath + '/userdetaills/getAllUserGroup/json';
         },
         getAllPageUrl : function(){
             return basePath +  '/userdetaills/getAllPage/json';
         },
         saveUserGroupUrl : function() {
             return basePath +  '/userdetaills/saveUserGroup/json';
         },
         updateuserinfoUrl : function(){
             return basePath +  '/userdetaills/updateuserinfo/json';
         },
         changeProfilePageUrl : function (){
             return basePath + '/users-v4/change-profile';
         },
         getSelecteduserUrl : function (){
             return basePath +   '/userdetaills/getSelecteduser/json';
         },
         setStatusUrl : function (){
              return basePath +   '/userdetaills/setStatus/json';
         },
         getUserCheckerUrl : function (username){
            return basePath + '/userdetaills/getChecker/json?username='+username;
         }
    }
}());

URI.channeldetail =  URI.channeldetail || (function(){
     return {
         listOrderByNameUrl : function() {
             return basePath + '/channeldetail/listOrderByName/json';
         },
         channelDetailUrl : function() {
             return basePath + '/channeldetail/list/json';
         },
         getAllChannelUrl : function() {
             return basePath +  '/channeldetail/getAllChannel/json';
         },
         getSelectedChannelUrl : function() {
             return basePath +  '/channeldetail/getSelectedChannel/json';
         },
         saveAdnUpdateChannelUrl : function() {
             return basePath +  '/channeldetail/saveAdnUpdateChannel/json';
         },
         disableChannelUrl : function() {
             return basePath +  '/channeldetail/disableChannel/json';
         },
         channelListPageUrl : function() {
             return basePath +  '/admin/channel/list';
         },
         enableChannelUrl : function() {
             return basePath +  '/channeldetail/enableChannel/json';
         },
         saveAdnUpdateChannelUrl : function() {
             return basePath +  '/channeldetail/saveAdnUpdateChannel/json';
         },
         getAllAutoCahnnelsUrl : function() {
            return basePath +  '/channeldetail/getAllAutoCahnnels/json';
         },
         getAllEnabledChannelsUrl : function() {
            return basePath +  '/channeldetail/getAllEnabledChannels/json';
         }
     }
}());

URI.client =  URI.client || (function(){
     return {
         getAllClientOrderByName : function() {
             return basePath + '/client/getAllClientOrderByName/json';
         },
         saveAndUpdateClientUrl : function() {
              return basePath + '/client/saveAndUpdateClient/json';
         },
         clientListPageUrl : function() {
              return basePath +  '/admin/client/list';
         },
         getAllClientUrl : function() {
              return basePath +  '/client/getAllClient/json' ;
         },
         getClientUrl : function() {
              return basePath +  '/client/getClient/json';
         },
         disableClientUrl : function() {
              return basePath + '/client/disableClient/json';
         },
         clientForWoTabaleUrl : function() {
            return basePath + '/client/clientForWoTable';
         }
     }
}());

URI.advertisement =  URI.advertisement || (function(){
     return {
         advertisementSelectedClientListUrl : function() {
             return basePath + '/advertisement/advertisementSelectedClientList/json';
         },
         getbyidUrl : function() {
             return basePath + '/advertisement/getbyid/json';
         },
         advertisementDeleteByStatusUrl : function() {
             return basePath +  '/advertisement/advertisementDeleteByStatus/json';
         },
         advertisementAllListUrl : function() {
             return basePath +  '/advertisement/advertisementAllList/page';
         },
         advertisementAllListJsonUrl : function() {
             return basePath +  '/advertisement/advertisementAllList/json';
         },
         homePageUrl : function() {
            return basePath + '/advertisement-v4';
         },
         updateAdvertisementUrl : function(advertID) {
             return basePath + '/advertisement-v4/update?advertID=' + advertID;
         },
         advertisementSaveUrl : function() {
             return basePath +  '/advertisement/advertisementSave/json';
         },
         advertisementCategoryAllListUrl : function() {
             return basePath +  '/advertisement/advertisementCategoryAllList/json';
         },
         uploadFileUrl : function() {
             return basePath +  '/advertisement/upload_file';
         },
         advertisementFilterListUrl : function() {
             return basePath +  '/advertisement/advertisementFilterList/json';
         },
         advertisementFilterForZeroManualUrl : function() {
            return basePath +  '/advertisement/advertisementListForZeroAds/json';
         },
         advertisementAllListOrderByNameUrl : function() {
             return basePath +  '/advertisement/advertisementAllListOrderByName/json';
         },
         advertisementCategoryUpdateUrl : function() {
             return basePath +  '/advertisement/advertisementCategoryUpdate/json';
         },
         advertisementCategorySaveUrl : function() {
             return basePath + '/advertisement/advertisementCategorySave/json';
         },
         advertisementCategoryHomePageUrl : function() {
             return basePath + '/admin/advertisement-category/list';
         },
         advertisementMetaDataUpdateUrl : function() {
             return basePath + '/advertisement/advertisementMetaDataUpdate/json';
         },
         advertisementSuspendUrl : function() {
             return basePath + '/advertisement/advertisementSuspend/json';
         },
         advertisementResumeUrl : function() {
             return basePath + '/advertisement/advertisementResume/json';
         },
         getAdvertHistoryUrl : function() {
            return basePath + '/advertisement/getAdvertHistory/json';
         }
     }
}());

URI.bill =  URI.bill || (function(){
     return {
         getAllTaxDetailsUrl : function() {
             return basePath + '/bill/getAllTaxDetails/json';
         },
         getAllInvoiceUrl : function(){
             return basePath + '/bill/getAllInvoice/json';
         },
         generatedBillUrl : function(){
             return basePath + '/bill/generatedBill/json';
         },
         downloadBillUrl : function(){
             return basePath + '/bill/downloadBill/json';
         },
         downloadSapUrl : function(id){
             return basePath + '/bill/downloadSap/json?invoiceId=' + id;
         },
         getInvoiceDetailsUrl : function(){
             return basePath + '/bill/getInvoiceDetails/json';
         },
         editDetailsUrl : function(id){
             return basePath + '/billing/edit-details?invoiceId=' + id;
         },
         getBillSpotDetailsUrl : function(){
             return basePath +  '/bill/getBillSpotDetails/json';
         },
         saveInvoiceUrl : function() {
             return basePath +   '/bill/saveInvoice/json';
         },
         saveInvoiceDetailsUrl : function(){
            return basePath +   '/bill/saveInvoiceDetails/json';
         },
         generateBillsUrl : function() {
            return basePath +   '/bill/generateBills/json';
         },
         downloadBillsUrl : function(fileName) {
            return basePath +   '/bill/downloadGeneratedBills?fileName='+ fileName;
         }
     }
}());

URI.scheduler =  URI.scheduler || (function(){
     return {
         getProrityScheduleListCount : function() {
            return basePath + '/scheduler/prorityListCount/json';
         },
         getMissedSpotUrl : function() {
             return basePath + '/scheduler/getMissedSpot/json';
         },
         timeSlotListUrl : function() {
             return basePath + '/scheduler/timeSlotList/json';
         },
         saveMissedSpotUrl : function() {
             return basePath + '/scheduler/saveMissedSpot/json';
         },
         saveMissedSpotV2Url : function() {
             return basePath + '/scheduler/saveMissedSpot_v2/json';
         },
         saveReschedulSpotUrl : function() {
             return basePath + '/scheduler/saveReschedulSpot/json';
         },
         generateExcelSpotUrl : function() {
             return basePath + '/scheduler/generateExcel/json';
         },
         downloadExcellUrl : function() {
             return basePath + '/scheduler/downloadExcell/json';
         },
         relaseDummyCutsUrl : function() {
             return basePath + '/scheduler/relaseDummyCuts/json';
         },
         clientOrderAdvertListUrl : function() {
             return basePath + '/scheduler/clientOrderAdvertList/json';
         },
         finalAllScheduleUrl : function() {
             return basePath + '/scheduler/finalAllSchedule/json';
         },
         getSelectedScheduleAdvertUrl : function(){
             return basePath + '/scheduler/getSelectedScheduleAdvert/json';
         },
         getOneDaySchedulesUrl : function() {
             return basePath +  '/scheduler/getOneDaySchedules/json';
         },
         getOneDaySchedulesViewUrl : function() {
             return basePath +  '/scheduler/getOneDaySchedulesView/json';
         },
         generateOneDayScheduleExcel : function() {
             return basePath +  '/scheduler/generateOneDayScheduleExcel/json';
         },
         downloadOneDayScheduelExcellUrl : function() {
             return basePath +  "/scheduler/downloadOneDayScheduelExcell/json";
         },
         getWOScheduleViewUrl : function() {
             return basePath + '/scheduler/getWOScheduleView/json';
         },
         scheduleSaveUrl : function() {
             return basePath +  '/scheduler/scheduleSave/json';
         },
         scheduleSaveAndWorkOrderUpdateUrl : function() {
             return basePath + '/scheduler/scheduleSaveAndWorkOrderUpdate/json';
         },
         scheduleSaveAndUpdateUrl : function() {
             return basePath + '/scheduler/scheduleSaveAndUpdate/json';
         },
         getTimeUtilizationUrl : function() {
             return basePath + '/scheduler/getTimeUtilization/json';
         },
         selectedScheduleAvailableSpotsCountUrl : function() {
             return basePath +  '/scheduler/selectedScheduleAvailableSpotsCount/json';
         },
         advertReplaceUrl : function() {
             return basePath +  '/scheduler/advertReplace/json';
         },
         getAvailbleAdvertCountUrl : function() {
             return basePath +  '/scheduler/getAvailbleAdvertCount/json';
         },
         prePatternInsertUrl : function() {
             return basePath +   '/scheduler/prePatternInsert/json';
         },
         schedulePatternSaveUrl : function() {
             return basePath +   '/scheduler/schedulePatternSave/json';
         },
         preAdvertReplaceUrl : function() {
             return basePath +   '/scheduler/preAdvertReplace/json';
         },
         advertReplaceUrl : function() {
             return basePath +   '/scheduler/advertReplace/json';
         },
         preCopyScheduleRowsUrl : function() {
             return basePath +   '/scheduler/preCopyScheduleRows/json';
         },
         copyScheduleRowsUrl : function() {
             return basePath +   '/scheduler/copyScheduleRows/json';
         },
         selectedChannelSlotCountUrl : function() {
             return basePath +   '/scheduler/selectedChannelSlotCount/json?';
         },
         getWorkOrderRemainSecondUrl : function() {
             return basePath +   '/scheduler/getWorkOrderRemainSecond/json';
         },
         getCurrentSchedulePageUrl : function(url_data) {
             return basePath +   '/schedule/current-schedule' + url_data;
         },
         clientOrderAdvertUrl : function() {
            return basePath +   '/schedule/clientOrderAdvertList/json';
         }
     }
}());

URI.fillerTag =  URI.fillerTag || (function(){
     return {
        tagFillerPageUrl: function(advertId) {
           return basePath +  '/fillers/tagging?advertID='+advertId;
        },
        saveOrUpdate: function(advertId) {
           return basePath +  '/fillers/save?advertId='+advertId;
        },
        getChannelsUrl: function(advertId) {
           return basePath +  '/fillers/channels?advertId='+advertId;
        },
        getAllFillers: function() {
            return basePath + '/fillers/search';
        }
     }
}());

URI.timeBelts =  URI.timeBelts || (function(){
     return {
         getListbyChannelUrl : function() {
             return basePath + '/timeBelts/getListbyChannel/json';
         },
         updateClusterCountListUrl : function() {
             return basePath + '/timeBelts/updateClusterCountList/json';
         },
         getListbyWorkOrderUrl : function() {
             return basePath + '/timeBelts/getListbyWorkOrder/json';
         },
         slotUtilizationUrl : function() {
             return basePath + '/scheduler/slotUtilization/json';
         }
     }
}());

URI.priority =  URI.priority || (function(){
     return {
         settingPageUrl : function(){
             return basePath + '/schedule/priority-setting';
         },
         getListbyChannelUrl : function() {
             return basePath + '/priority/getListbyChannel/json';
         },
         insertPriorityUrl : function() {
            return basePath + '/priority/insertPriority/json';
         },
         viewPriorityListUrl : function() {
            return basePath + '/priority/viewPriorityList/json';
         },
         removePrioritiesUrl : function() {
            return basePath + '/priority/removePriorities/json';
         },
         updatePriorityCountListUrl : function() {
            return basePath + '/priority/updatePriorityCountList/json';
         }
     }
}());

URI.report =  URI.report || (function(){
     return {
         transemissionReportUrl : function() {
             return basePath + '/Reports/transemissionReport/json';
         },
         transemissionReportPrintUrl : function() {
             return basePath + '/Reports/transemissionReportPrint/json';
         },
         transemissionReportPrintXMSUrl : function() {
              return basePath + '/Reports/transemissionReportPrintXMS/json';
         },
         downloadPDF1Url : function() {
              return basePath + "/Reports/downloadPDF1/json";
         },
         downloadExcelUrl : function() {
              return basePath + "/Reports/downloadExcel/json";
         },
         getDummyCutReportUrl : function() {
              return basePath + '/Reports/getDummyCutReport/json';
         },
         writeDummyCutReportUrl : function() {
              return basePath + '/Reports/writeDummyCutReport/json';
         },
         downloadDummyCutReportUrl : function() {
              return basePath +  "/Reports/downloadDummyCutReport/json";
         },
         getComAvailabilityHourlyUrl : function() {
              return basePath +  '/Reports/getComAvailabilityHourly/json';
         },
         writeComAvailabilityHourlyReportUrl : function() {
              return basePath +  '/Reports/writeComAvailabilityHourlyReport/json';
         },
         downloadComAvailabilityHourlyReportUrl : function() {
              return basePath +  "/Reports/downloadComAvailabilityHourlyReport/json";
         },
         getComAvailabilityChannelWiseUrl : function() {
              return basePath +   '/Reports/getComAvailabilityChannelWise/json';
         },
         writeComAvailabilityChannelWiseReportUrl : function() {
              return basePath +   '/Reports/writeComAvailabilityChannelWiseReport/json';
         },
         downloadComAvailabilityChannelWiseReportUrl : function() {
              return basePath +   "/Reports/downloadComAvailabilityChannelWiseReport/json";
         },
         getScheduleAnalysisUrl : function() {
              return basePath +   '/Reports/getScheduleAnalysis/json';
         },
         writeScheduleAnalysisReportUrl : function() {
              return basePath +   '/Reports/writeScheduleAnalysisReport/json';
         },
         downloadScheduleAnalysisReportUrl : function() {
              return basePath +   "/Reports/downloadScheduleAnalysisReport/json";
         },
         getYearsValueUrl : function() {
              return basePath +   '/Reports/getYearsValue/json';
         },
         getClientRevenueUrl : function() {
             return basePath +  '/Reports/getClientRevenue/json';
         },
         writeClientRevenueReportUrl : function() {
            return basePath + '/Reports/writeClientRevenueReport/json';
         },
         downloadClientRevenueReport : function() {
            return basePath + "/Reports/downloadClientRevenueReport/json";
         },
         getAgencyRevenueUrl : function() {
            return basePath + '/Reports/getAgencyRevenue/json';
         },
         writeAgencyRevenueReportUrl : function() {
            return basePath + '/Reports/writeAgencyRevenueReport/json';
         },
         downloadAgencyRevenueReportUrl : function() {
            return basePath + "/Reports/downloadAgencyRevenueReport/json";
         },
         getSellerReportUrl : function() {
            return basePath +  '/Reports/getSellerReport/json';
         },
         writeSellerReportUrl : function() {
            return basePath + '/Reports/writeSellerReport/json';
         },
         downloadSellerReportUrl : function() {
            return basePath + "/Reports/downloadSellerReport/json";
         },
         getPaymentDueReportUrl : function() {
            return basePath + '/Reports/getPaymentDueReport/json';
         },
         writePaymentDueReportUrl : function() {
            return basePath +  '/Reports/writePaymentDueReport/json';
         },
         downloadPaymentDueReportUrl : function() {
            return basePath +  "/Reports/downloadPaymentDueReport/json";
         },
         getAdvertIDsUrl : function() {
            return basePath + '/Reports/getAdvertIDs/json';
         },
         getMaterialAnalysisReportUrl : function() {
            return basePath + '/Reports/getMaterialAnalysisReport/json';
         },
         writeMaterialAnalysisReportUrl : function() {
            return basePath +  '/Reports/writeMaterialAnalysisReport/json';
         },
         downloadMaterialAnalysisReportUrl : function() {
            return basePath +  "/Reports/downloadMaterialAnalysisReport/json";
         },
         writeMediaDeleteHistoryReportUrl : function() {
            return basePath + '/Reports/writeMediaDeleteHistoryReport/json';
         },
         downloadMediaDeletionHistoryReportUrl : function() {
            return basePath + "/Reports/downloadMediaDeletionHistoryReport/json";
         },
         getMediaDeleteHistoryUrl : function() {
            return basePath + '/Reports/getMediaDeleteHistory/json';
         },
         missedUrl : function() {
            return basePath +  '/Reports/missed';
         },
         writeMissedSpotCountReportUrl : function() {
            return basePath +  '/Reports/writeMissedSpotCountReport/json';
         },
         downloadMissedSpotCountReportUrl : function() {
            return basePath +  "/Reports/downloadMissedSpotCountReport/json";
         },
         getMissedSpotDataUrl : function() {
            return basePath +   "/Reports/getMissedSpotData";
         },
         writeMissedSpotCountReportUrl_Test : function() {
            return basePath +   "/Reports/writeMissedSpotCountReport/json";
         },
         downloadMissedSpotCountReportUrl : function() {
            return basePath +   "/Reports/downloadMissedSpotCountReport/json";
         },
          writeSchedulePrintReportUrl : function() {
            return basePath +   "/Reports/writeSchedulePrintReport/json";
          },
         downloadSchedulePrintReportUrl : function() {
            return basePath +   "/Reports/downloadSchedulePrintReport/json";
         },
          getUserWiseReportUrl : function() {
            return basePath + '/Reports/getMoshUserReport';
          },
          writeUserWiseReportUrl : function() {
            return basePath +   "/Reports/writeMoshUserReport/json";
          },
          downloadUserWiseReportUrl : function() {
            return basePath +   "/Reports/downloadMoshUserReport/json";
          }
     }
}());

URI.logoContainer =  URI.logoContainer || (function(){
     return {
         getallUrl : function() {
             return basePath + '/logo_container/getall';
         },
         insertLogoListtoContainerUrl : function() {
             return basePath + '/logo_container/insertLogoListtoContainer/json';
         },
         getLogoListbyIdUrl : function() {
             return basePath + '/logo_container/getLogoListbyId/json';
         }
     }
}());

URI.other =  URI.other || (function(){
     return {
         homePageUrl : function() {
             return basePath + '/home';
         }
     }
}());

URI.attachments =  URI.attachments || (function(){
     return {
         getAttachmentsUrl : function() {
             return basePath +  '/attachments/getAttachments/json';
         },
         uploadFileUrl : function() {
             return basePath + "/attachments/upload_file";
         },
         removeAttachmentUrl : function() {
             return basePath + "/attachments/removeAttachment";
         },
         downloadAttachmentUrl : function(id) {
             return basePath + '/attachments/downloadAttachment?id=' + id ;
         }
     }
}());

URI.servers =  URI.servers || (function(){
     return {
         serverdetailsUrl : function() {
             return basePath +  '/serverdetails/list/json';
         },
         getServerbyIdUrl : function() {
            return basePath +  '/serverdetails/getServerbyId/json';
         },
         updateServerBasicDataUrl : function() {
            return basePath +  '/serverdetails/updateServerBasicData/json';
         },
         allServerPageUrl : function() {
            return basePath + '/admin/server/list';
         },
         insertServerUrl : function() {
            return basePath + '/serverdetails/insertServer/json';
         },
         updateServerDynamicDataUrl : function() {
            return basePath + '/serverdetails/updateServerDynamicData/json';
         }
     }
}());

URI.taxDetails =  URI.taxDetails || (function(){
     return {
         listUrl : function() {
             return basePath +  '/taxDetails/list/json';
         },
         updateUrl : function() {
             return basePath +  '/taxDetails/update/json';
         }
     }
}());

URI.audit =  URI.audit || (function(){
     return {
         getWorkOredrAuditUrl : function() {
             return basePath + '/audit/getWorkOredrAudit/json';
         },
         getScheduleAuditUrl : function() {
            return basePath + '/audit/getScheduleAudit';
         }
     }
}());

URI.zero =  URI.zero || (function(){
     return {
         getAllChannelAdvertMapUrl : function() {
             return basePath + '/channeladvert/getAllChannelAdvertMap/json';
         },
         getZeroAdsManualPlayListUrl : function() {
            return basePath + '/channeladvert/getZeroAdsManualPlayList';
         },
         saveChannelAdvertMapUrl : function() {
            return basePath + '/channeladvert/saveChannelAdvertMap/json';
         },
         saveZeroAdsManualPlayListUrl : function() {
            return basePath + '/channeladvert/saveZeroAdsManualPlayList';
         },
         getZeroAdsSettingUrl : function() {
            return basePath + '/channeladvert/getZeroAdsSetting';
         },
         saveZeroAdsSettingMapUrl : function() {
            return basePath + '/channeladvert/saveZeroAdsSettingMap/json';
         }
     }
}());

URI.ops =  URI.ops || (function(){
     return {
         importDealUrl : function() {
             return basePath + '/ops/importDeal';
         },
         importCustomSchedulelUrl : function() {
            return basePath + '/ops/importCustomSchedules';
         },
         importBidItemUrl : function() {
            return basePath + '/ops/importBidItem';
         },
         importPromotionUrl : function() {
            return basePath + '/ops/importPromotion';
         }
     }
}());

URI.dashboard =  URI.dashboard || (function(){
     return {
         manualChannelsUrl : function() {
             return basePath + '/dashboard/getManualChannel';
         },
         autoChannelsUrl : function() {
             return basePath + '/dashboard/getAutoChannel';
         },
         getAllDashboardUserGroupsUrl : function() {
             return basePath + '/dashboard/getAllDashboardGroups';
         },
         getDashboardUserUrl : function(username) {
             return basePath + '/dashboard/getDashboardUser?username='+username;
         }
     }
}());

URI.image =  URI.image || (function(){
     return {
         channelImage : function() {
             var applicationVersion  =  $("meta[name='_applicationVersion']").attr("content");
             return basePath + '/assets-' + applicationVersion +'/ dist/';
         },
         channelImagePath : function(channelId) {
            var applicationVersion  =  $("meta[name='_applicationVersion']").attr("content");
            return basePath + '/channel-logos-' + applicationVersion +'/'+channelId+'.png';
         }
     }
}());

URI.inventory =  URI.inventory || (function(){
     return {
          findInventoryExistDates: function(channelId, fromDate, toDate){
            return basePath + '/inventory/exist-dates?channelId='+channelId+'&fromDate='+fromDate+'&toDate='+toDate;
          },
          viewInventoryByChannelId:function(channelId){
             return basePath + '/inventory/assign-inventory?channelId='+channelId;
          },
          editInventoryByChannelAndDate(channelId, date){
            return basePath + '/inventory/edit-inventory?channelId='+channelId+'&date='+date;
          },
          getInventoryByChannelAndDate:  function(channelId, date){
            return basePath + 'inventory/view-inventory?channelId='+channelId+'&date='+date;
          },
          getInventoryPredictionByChannelAndDate: function(channelId, date){
            return basePath + 'inventory/view-inventory-prediction?channelId='+channelId+'&date='+date;
          },
          csvFileValidationURL: function(){
            return basePath + '/inventory/file-validation';
          }
     }
}());


URI.teamManagement =  URI.teamManagement || (function(){
     return {
          saveTeamDetailUrl: function(){
            return basePath + '/users-v4/save-team-detail';
          },
          loadAllTeamsUrl: function(){
            return basePath + '/users-v4/load-all-teams';
          },
          getTeamlessUser: function(){
            return basePath + '/userdetaills/allTeamlessUser/json';
          }
     }
}());

URI.teamDetails =  URI.teamDetails || (function(){
     return {
          loadAllTeamMembersUrl: function(teamId){
            return basePath + '/users-v4/load-all-team-members?teamId=' + teamId;
          },
          loadTeamAdminsUrl: function(teamId){
            return basePath + '/users-v4/load-team-admins?teamId=' + teamId;
          },
          deleteMemberUrl: function(teamId, username){
            return basePath + '/users-v4/deleteMemberUrl?teamId=' + teamId + '&username=' + username;
          },
          deleteAdminUrl: function(teamId, username){
            return basePath + '/users-v4/deleteAdminUrl?teamId=' + teamId + '&username=' + username;
          },
          updateTeamDetailUrl: function(){
            return basePath + '/users-v4/update-team-detail';
          },
          checkTeamUrl: function(teamName){
            return basePath + '/users-v4/checkTeamName?teamName=' + teamName;
          }
     }
}());

URI.brand =  URI.brand || (function(){
     return {
          getAllBrand: function(){
            return basePath + '/brand/allBrand/json';
          },
          saveBrand(){
            return basePath + '/brand/saveBrand';
          },
          updateBrand(){
            return basePath + '/brand/updateBrand';
          },
          allProductListUrl(){
            return basePath + '/product/allProductList/page';
          },
          validateCode(){
            return basePath + '/brand/validateCode';
          }
     }
}());

URI.product =  URI.product || (function(){
     return {
          getAllProducts: function(){
            return basePath + '/product/allProducts/json';
          },
          getProductsByClient: function(clientId) {
             return basePath + '/product/codeMappings/json?clientId=' + clientId;
          },
          getSelectedProducts: function() {
            return basePath + '/product/selectedCodeMapping';
          }
     }
}());

URI.playlistSetting =  URI.playlistSetting || (function(){
     return {
          save: function(){
            return basePath + '/playlist-setting/save';
          },
          loadByChannel: function(cid, type){
            return basePath + '/playlist-setting/load-by-channel?cid=' + cid + '&type=' + type;
          },
          deleteSetting: function(){
            return basePath + '/playlist-setting/delete';
          }
     }
}());

URI.playlistView =  URI.playlistView || (function(){
     return {
          generatePlayList: function(){
            return basePath + '/playlist-view/generatePlayList';
          },
          downloadPlaylistReport: function(seqNum){
            return basePath + '/playlist-view/downloadPlaylistPreviewReport?seqNum=' + seqNum;
          }
     }
}());
