$(document).ready(function () {
    //setAdvertTable();
    loadAdvert();
    setClientDrop();
});

function setClientDrop() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getAllClientOrderByName(),
        success: function (data)
        {
            closeDialog();
            var clientDrop = '<span class="input-group-addon">Clients</span><select id="client_list" name="client_id" class="form-control">';
            clientDrop += '<option VALUE="-111">--All clients--</option>';
            for (var i in data)
            {
                if (data[i].clienttype == 'Client') {
                    clientDrop += '<option VALUE="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
            }
            clientDrop += '</select>';
            $('#clientList_div').html(clientDrop);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function getId(id) {
    window.open('./advertisement-v4/update?advertID=' + id);
}

function getAdvertId(advertId) {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.getbyidUrl(),
        data: {
            id: advertId
        },
        success: function (data)
        {
            closeDialog();
            var path = data.advertpath;
            if (path == "LOGO_CONTAINER") {
                alert('Invalid preview => Multiple Logos');
                $('#advert_view_pop').modal('toggle');
                return false;
            }
            setPreviewVideo(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function setPreviewVideo(advert){
     var path = advert.advertpath;
     path = path.split(UTIL.common.getFileSeparator());
     var index = path.length - 1;
     var fileName = path[index];
     fileName = fileName.split('.');
     fileName[1] = fileName[1].toLowerCase();
     var fileExtension=fileName[1];

//     var mediaFileRootPath = UTIL.common.getFileSeparator() +
//                                  'media-files-' + UTIL.common.getApplicationVersion() + UTIL.common.getFileSeparator();
     var mediaFileRootPath = UTIL.common.getFileSeparator() +
                                  'media-files' + UTIL.common.getFileSeparator();
     if (fileExtension == 'mpg' && advert.fileAvailable){
         previewVideo(mediaFileRootPath + fileName[0] + '.mp4');
     } else if (fileExtension == 'swf' && advert.fileAvailable){
         previewVideo(mediaFileRootPath + fileName[0] + '.swf');
     } else if ((fileExtension == 'jpg' || fileExtension == 'png') && advert.fileAvailable){
         previewVideo(mediaFileRootPath + fileName[0] + '.' + fileName[1]);
     } else {
         $('#lblNullFile').show();
     }
}

function previewVideo(path) {
    console.log(path);
    console.log((path.substr(path.lastIndexOf('.') + 1)).toLowerCase());

    $("#flash").hide();
    $("#video").hide();
    $("#image").hide();

    $("#image").attr('src', '');
    $("#video").attr('src', '');
    $("#flash").attr('data', '');

    var ext = (path.substr(path.lastIndexOf('.') + 1)).toLowerCase();
    if (ext == "mp4") {
        $("#video").show();
        $("#video").attr('src', path);
        $("#video").attr('poster', '');
    } else if (ext == "swf") {
        $("#flash").show();
        $("#flash").attr('data', path);
    } else {
        $("#image").show();
        $("#image").attr('src', path);
    }
}


function getAdvertType(type) {
    switch (type) {
        case "ADVERT":
            return "TVC";
            break;

        case "CRAWLER":
            return "Crawler";
            break;

        case "LOGO":
            return "Logo";
            break;

        case "V_SHAPE":
            return "V squeeze";
            break;

        case "L_SHAPE":
            return "L squeeze";
            break;

        case "FILLER":
            return "Filler";
            break;

        case "SLIDE":
            return "Slide";
            break;

        default:

    }
}

function deleteAdvert(advertId) {
    var ret = confirm("Are you sure you want to delete this advertisement?");
    if (!ret) {
        return;
    }
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementDeleteByStatusUrl(),
        data: {
            advertid: advertId
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                alert("successfully Deleted");
                window.location = URI.advertisement.homePageUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function loadAdvert() {
    var searchResultsTable = $('#all_advertisement_table').DataTable({
        "bFilter": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": URI.advertisement.advertisementAllListUrl(),
            "type": 'GET',
            "data": function (d) {
                var table = $('#all_advertisement_table').DataTable();
                d.page = (table != undefined) ? table.page.info().page : 0;
                d.size = (table != undefined) ? table.page.info().length : 5;
                d.adverId = numberValidation($("#adverId").val()) ? $("#adverId").val() : '';
                d.adverName = $("#adverName").val();
                d.advertCategory = $("#category_drop").val();
                d.client_id = $("#client_list").val();
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.totalElements;
                json.recordsFiltered = json.totalElements;
                json.data = json.content;
                return JSON.stringify(json);
            }
        },
        "columns": [
            {
                "width": "5%",
                "data": "advertid",
                "orderable": false,
                "render": function (data) {
                    return  $('<a/>').attr('href', '#').attr('id', data).attr('onclick', 'getId(this.id)').append($('<span>' + data + '</span>')).wrap('<div/>').parent()
                            .html();
                }
            },
            {"width": "15%",
                "data": "advertname",
                "orderable": false
            },
            {"width": "5%",
                "data": "duration",
                "orderable": false
            },
            {"width": "5%",
                "data": "language",
                "orderable": false,
                "render": function (data) {
                    return  setLanguage(data);
                }
            },
            {
                "width": "5%",
                "data": "adverttype",
                "orderable": false,
                "render": function (data) {
                    return  getAdvertType(data);
                }
            },
            {
                "width": "20%",
                "data": "client.clientname",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "createDate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "expireDate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "suspendDate",
                "orderable": false
            },
            {
                "width": "5%",
                "data": "statustName",
                "orderable": false
            },
            {
                "width": "5%",
                "orderable": false,
                "render": function (data, type, full) {
                    if (full.fileAvailable) {
                        return  $('<Button/>').attr('title', 'View').attr('type', 'submit').attr('id', full.advertid).attr('onclick', 'getAdvertId(this.id)').attr('class', 'btn btn_color').attr('data-toggle', 'modal').attr('data-target', '#advert_view_pop').append($('<span>View</span>')).wrap('<div/>').parent()
                                .html();
                    } else {
                        return  $('<Button/>').attr('title', 'View').attr('type', 'submit').attr('id', full.advertid).attr('disabled', 'true').attr('class', 'btn btn_color').append($('<span>View</span>')).wrap('<div/>').parent()
                                .html();
                    }
                }
            },
            {
                "width": "5%",
                "orderable": false,
                "render": function (data, type, full) {
                    if (full.canDelete) {
                        return  $('<Button/>').attr('title', 'Delete').attr('type', 'submit').attr('id', full.advertid).attr('onclick', 'deleteAdvert(this.id)').attr('class', 'btn btn_readcolor').append($('<span>Delete</span>')).wrap('<div/>').parent()
                                .html();
                    } else {
                        return  $('<Button/>').attr('title', 'Delete').attr('type', 'submit').attr('id', full.advertid).attr('disabled', 'true').attr('class', 'btn btn_readcolor').append($('<span>Delete</span>')).wrap('<div/>').parent()
                                .html();
                    }
                }
            }
        ],
        "createdRow": function (row, data, index) {
            if (!data.fileAvailable) {
                //Dummy advert
                $('td', row).addClass('dummy-avert');
            } else if (data.enabled === 0 && data.status === 1) {
                //Closed workOrder
                $('td', row).addClass('expir-avert');
            } else if (data.enabled === 0 && data.status === 2) {
                $('td', row).addClass('suspend-avert');
            }
        }

    });

    $("#btn-search").click(function () {
        searchResultsTable.ajax.reload();
    });
}
