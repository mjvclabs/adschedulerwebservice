var mapClients = {};

$(function () {
    $('#div-end-date').datetimepicker({
        pickTime: false
    });
});

$(function () {
    $('#div-start-date').datetimepicker({
        pickTime: false
    });
});

$(function () {
    $('#div-modify-date').datetimepicker({
        pickTime: false
    });
});

$(document).ready(function () {
    loadClients();
    setUser();
    loadWorkOrderTable();
});

function setUser() {

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.userDetails.getMeUserUrl(),
        success: function (data)
        {
            for (var i in data) {
                $("#txt-me").append('<option value="' + data[i].userName + '">' + data[i].userName + '</option>');
            }
        },
        error: function (jqXHR, textStatus, thrownError) {
        }
    })
}

function loadWorkOrderTable() {
    var searchResultsTable = $('#tb-all-schedules').DataTable({
        "bFilter": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": URI.workOrder.workOrderListForScheduleUrl(),
            "type": 'GET',
            "data": function (d) {
                var table = $('#tb-all-schedules').DataTable();
                d.page = (table != undefined) ? table.page.info().page : 0;
                d.size = (table != undefined) ? table.page.info().length : 5;
                d.woid = $('#txt-workOrder-id').val();
                d.agent = $('#txt-agent').val();
                d.client = $('#txt-client').val();
                d.product = $('#txt-product').val();
                d.scheduleRef = $('#txt-schedule-ref').val();
                d.me = $('#txt-me').val();
                d.start = $('#txt-start').val();
                d.end = $('#txt-end').val();
                d.modifyDate = $('#txt-modify-date').val();
                d.status = $('#txt-status').val();
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.totalElements;
                json.recordsFiltered = json.totalElements;
                json.data = json.content;
                return JSON.stringify(json);
            }
        },
        "columns": [
            {
                "width": "5%",
                "data": "workorderid",
                "orderable": false,
                "render": function (data, type, full) {
                    if (full.permissionstatus != 3) {
                        return  $('<a/>').attr('href', './schedule/current-schedule?work_id=' + data + '&work_name=' + full.ordername + '&start_date=' + full.startdate + '&end_date=' + full.enddate)
                                         .append($('<span>' + data + '</span>')).wrap('<div/>').parent()
                                         .html();
                    } else {
                        return  $('<a/>').attr('href', './schedule/schedule-history?work_id=' + data + '&work_name=' + full.ordername + '&start_date=' + full.startdate + '&end_date=' + full.enddate).append($('<span>' + data + '</span>')).wrap('<div/>').parent()
                                .html();
                    }
                }
            },
            {
                "width": "13%",
                "data": "agent",
                "orderable": false,
                "render": function (data) {
                    return  mapClients[data];
                }
            },
            {
                "width": "16%",
                "data": "client",
                "orderable": false,
                "render": function (data) {
                    return  mapClients[data];
                }

            },
            {
                "width": "8%",
                "data": "ordername",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "scheduleRef",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "seller",
                "orderable": false
            },
            {
                "width": "9%",
                "data": "spotCount",
                "orderable": false
            },
            {
                "width": "12%",
                "data": "startdate",
                "orderable": false
            },
            {
                "width": "13%",
                "data": "enddate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "lastModifydate",
                "orderable": false,
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return  date.getFullYear() + "-" + month + "-" + date.getDate();
                }
            },
            {
                "width": "10%",
                "data": "status",
                "orderable": false
            }
        ],
        "createdRow": function (row, data, index) {
            if (data.permissionstatus == 4) {
                //Suspend workOrder
                $('td', row).addClass('suspend-work-order');
            } else if (data.permissionstatus == 3) {
                //Closed workOrder
                $('td', row).addClass('closed-work-order');
            } else if (data.notMatchWithBudget) {
                $('td', row).addClass('budget-missed-match');
            }
        }

    });

    $("#btn-search").click(function () {
        searchResultsTable.ajax.reload();
    });
}

function loadClients() {
    var agentDrp = '<select  id="txt-agent" name="txt-agent" class="form-control selectpicker" data-live-search="true"><option value="">Select Agent</option>';
    var clientDrp = '<select  id="txt-client" name="txt-client" class="form-control selectpicker" data-live-search="true"><option value="">Select Client</option>';
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.client.clientForWoTabaleUrl(),
        success: function (data)
        {
            for (var i in data) {
                mapClients[data[i].clientId] = data[i].clientName;
                if (data[i].clientType == "Agency") {
                    agentDrp += '<option data-tokens="' + data[i].clientName + '" value="' + data[i].clientId + '">' + data[i].clientName + '</option>';
                } else {
                    clientDrp += '<option data-tokens="' + data[i].clientName + '" value="' + data[i].clientId + '">' + data[i].clientName + '</option>';
                }
            }
            mapClients[0] = "No agent";
            agentDrp += '</select>';
            clientDrp += '</select>';
            $('#drp-agent').html(agentDrp);
            $('#drp-client').html(clientDrp);
            $('.selectpicker').selectpicker({
                size: 10
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function clearFilter() {
    document.getElementById("txt-workOrder-id").value = "";
    $("#txt-agent").selectpicker('val', "");
    $("#txt-client").selectpicker('val', "");
    document.getElementById("txt-product").value = "";
    document.getElementById("txt-schedule-ref").value = "";
    document.getElementById("txt-me").value = "";
    document.getElementById("txt-start").value = "";
    document.getElementById("txt-end").value = "";
    document.getElementById("txt-modify-date").value = "";
    document.getElementById("txt-status").value = "select";
}
