var mapClients = {};
var fetchDone;
var selectedWOList = [];
var searchResultsTable;
$(function () {
    $('#div-end-date').datetimepicker({
        pickTime: false
    });
});
$(document).ready(function () {
    fetchDone = _.after(2, function(){
        loadWorkOrderTable();
    });
    loadClients();
    setUser();
});

function setUser() {

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.userDetails.getMeUserUrl(),
        success: function (data)
        {
            var meDrp = '<select  id="txt-me" name="txt-me" class="form-control selectpicker" data-live-search="true"><option value="">Select ME</option>';
            for (var i in data) {
                meDrp += '<option data-tokens="' + data[i].userName + '" value="' + data[i].userName + '">' + data[i].userName + '</option>';
            }
            $('#drp-me').html(meDrp);
            $("#txt-me").selectpicker('refresh');
            fetchDone();
        },
        error: function (jqXHR, textStatus, thrownError) {
        }
    })
}

function changeWOSelectStatus(e) {
    var selectedWOId = parseInt(e.id);
    if(e.checked) {
        selectedWOList.push(selectedWOId);
    } else {
        selectedWOList = _.without(selectedWOList, selectedWOId);
    }
}

function loadWorkOrderTable() {
     searchResultsTable = $('#tb-all-billingWorkOrder').DataTable({
        "bFilter": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": URI.workOrder.workOrderListForBillingUrl(),
            "type": 'GET',
            "data": function (d) {
                var table = $('#tb-all-billingWorkOrder').DataTable();
                d.page = (table != undefined) ? table.page.info().page : 0;
                d.size = (table != undefined) ? table.page.info().length : 5;
                d.woid = $('#txt-workOrder-id').val();
                d.agent = $('#txt-agent').val();
                d.client = $('#txt-client').val();
                d.product = $('#txt-product').val();
                d.me = $('#txt-me').val();
                d.woType = $('#txt-wo-type').val();
                d.invoiceType = $('#txt-invoice-type').val();
                d.scheduleRef = $('#txt-schedule-ref').val();
                d.revMonth = $('#revenue-month').val();
                d.end = $('#txt-end').val();
                d.status = $('#txt-status').val();
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.totalElements;
                json.recordsFiltered = json.totalElements;
                json.data = json.content;
                return JSON.stringify(json);
            }
        },
        "columns": [
            {
                "width": "5%",
                "data": "workorderid",
                "orderable": false,
                "render": function (data, type, full) {
                    var woId = full.workorderid;
                    var isChecked = full.billingStatus != 0;
                    return $('<input type="checkbox"/>').attr({"id": woId, "class": "wo-cb", "disabled": isChecked,
                    "checked": isChecked, "onChange": "changeWOSelectStatus(this)"})
                                            .wrap('<div/>').parent().html();
                }
            },
            {
                "width": "5%",
                "data": "workorderid",
                "orderable": false,
                "render": function (data, type, full) {
                    var invoiceType = (full.commission).replace('%', '-')
                    return  $('<a/>').attr('href', './billing/detail-view?work_id=' + data + '&invoiceTpye=' + invoiceType + '&wo_status=' + full.autoStatus).append($('<span>' + data + '</span>')).wrap('<div/>').parent()
                            .html();
                }
            },
            {
                "width": "13%",
                "data": "agent",
                "orderable": false,
                "render": function (data) {
                    return  mapClients[data];
                }
            },
            {
                "width": "16%",
                "data": "client",
                "orderable": false,
                "render": function (data) {
                    return  mapClients[data];
                }

            },
            {
                "width": "8%",
                "data": "ordername",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "seller",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "woType",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "commission",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "scheduleRef",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "renMonth",
                "orderable": false
            },
            {
                "width": "9%",
                "data": "amount",
                "orderable": false,
                "render": function (data) {
                    return getDecimal(parseFloat(data));
                }
            },
            {
                "width": "7%",
                "data": "spotCount",
                "orderable": false
            },
            {
                "width": "13%",
                "data": "aired",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "enddate",
                "orderable": false,
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return  date.getFullYear() + "-" + month + "-" + date.getDate();
                }
            },
            {
                "width": "10%",
                "data": "status",
                "orderable": false
            }
        ]
    });
    $("#btn-search").click(function () {
        searchResultsTable.ajax.reload();
    });
}

function loadClients() {
    var agentDrp = '<select  id="txt-agent" name="txt-agent" class="form-control selectpicker" data-live-search="true"><option value="">Select Agent</option>';
    var clientDrp = '<select  id="txt-client" name="txt-client" class="form-control selectpicker" data-live-search="true"><option value="">Select Client</option>';
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.client.clientForWoTabaleUrl(),
        success: function (data)
        {
            for (var i in data) {
                mapClients[data[i].clientId] = data[i].clientName;
                if (data[i].clientType == "Agency") {
                    agentDrp += '<option data-tokens="' + data[i].clientName + '" value="' + data[i].clientId + '">' + data[i].clientName + '</option>';
                } else {
                    clientDrp += '<option data-tokens="' + data[i].clientName + '" value="' + data[i].clientId + '">' + data[i].clientName + '</option>';
                }
            }
            mapClients[0] = "No agent";
            agentDrp += '</select>';
            clientDrp += '</select>';
            $('#drp-agent').html(agentDrp);
            $('#drp-client').html(clientDrp);
            $('.selectpicker').selectpicker({
                size: 10
            });
            fetchDone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}
function clearFilter() {
    document.getElementById("txt-workOrder-id").value = "";
    $("#txt-agent").selectpicker('val', "");
    $("#txt-client").selectpicker('val', "");
    $("#txt-me").selectpicker('val', "");
    document.getElementById("txt-product").value = "";
    document.getElementById("txt-wo-type").value = "select";
    document.getElementById("txt-invoice-type").value = "select";
    document.getElementById("txt-schedule-ref").value = "";
    document.getElementById("revenue-month").value = "";
    document.getElementById("txt-end").value = "";
    document.getElementById("txt-status").value = "select";
}
function setDetails(id)
{
    var work_id = document.getElementById(id).innerHTML;
    var work_name = document.getElementById("tNamec2_" + id).innerHTML;
    var start_date = document.getElementById("t1c3_" + id).innerHTML;
    var end_date = document.getElementById("t1c4_" + id).innerHTML;
    var wo_status = document.getElementById("t1c5_" + id).innerHTML;
// Check browser support
    if (typeof (Storage) !== "undefined") {
//Store
        localStorage.setItem("billing_W_details", '",' + work_id + ',' + work_name + ',' + start_date + ',' + end_date + ',' + wo_status + ',"');
    }
}

function generateBills() {
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    var billRequestData = {};
    //need to set selected work orders
    billRequestData['woIds'] = selectedWOList;
    var billingType = $('input[name="billingType"]:checked').val();
    billRequestData['fullBill'] = billingType == 'scheduled';
    var hasValidationErrors = false;
    if(!billingType) {
        alert("Please select a billing type")
        hasValidationErrors = true;
    } else if(selectedWOList.length == 0) {
        alert("Please select work orders for billing")
        hasValidationErrors = true;
    }
    if(hasValidationErrors) {
        return;
    }

    $.ajax({
        type: 'post',
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(billRequestData),
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        url: URI.bill.generateBillsUrl(),
        success: function (data)
        {
            afterSuccessBillGeneration(data.fileName);
        },
        error: function (jqXHR, textStatus, thrownError) {
            console.log('Error has occurred while generating bills.');
        }
    })
}

function afterSuccessBillGeneration(zipFile) {
   $("#report_download").attr({
        "href": URI.bill.downloadBillsUrl(zipFile),
        "aria-pressed": "false",
   });
   $("#report_download").removeAttr("disabled");
     searchResultsTable.ajax.reload();
   selectedWOList = [];
}
