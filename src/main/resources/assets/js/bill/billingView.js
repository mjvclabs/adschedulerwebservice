var v_workOrderId;
var invoice_type = 0;
var selected_invoice = 0;
var total_A = 0;
var total_payable = 0;
var _nbt = '';
var after_sub_total = 0.0;
var pub_datailsTable = '';
var billingStatus=0;

$(document).ready(function () {
    setBillWorkOrderDetails();
});


function setBillWorkOrderDetails()
{
    v_workOrderId = getParameterByNamefromUrl('work_id');
    getWODetails(v_workOrderId);

    var wo_Status = getParameterByNamefromUrl('wo_status');
    if (wo_Status == 3) {
        var button = '<div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="fullBill()" class="btn btn_color pull-right" id="btn_full">Bill scheduled</button></div>' +
        '<div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="airSpotBill()" class="btn btn_color pull-lift" id="btn_air">Bill aired</button></div>';
        $('#bill_category').html(button);
        selected_invoice = 0;
    } else if (wo_Status == 0 || wo_Status == 1) {
        var button = '<div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="fullBill()" class="btn btn_color pull-right" id="btn_new">Bill scheduled</button></div>';
        $('#bill_category').html(button);
        selected_invoice = 2;
    } else if (wo_Status == 2) {
        var button = '<div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="fullBill()" class="btn btn_color pull-right" id="btn_full">Bill scheduled</button></div><div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="airSpotBill()" class="btn btn_color pull-lift" id="btn_air">Bill aired</button></div>';
        $('#bill_category').html(button);
        selected_invoice = 3;
    }
}

function getWODetails(wo_id) {
    showDialog();
    $.ajax({
        type: 'GET',
        async: false,
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.selectedWorkOrderUrl(),
        data: {
            workOrderId: wo_id
        },
        success: function (data)
        {
            closeDialog();
            document.getElementById("lbl-woid").innerHTML = data[0].workOrderId;
            document.getElementById("lbl-agency").innerHTML = data[0].agencyByName;
            document.getElementById("lbl-start").innerHTML = data[0].startdate;

            document.getElementById("lbl-ref").innerHTML = data[0].scheduleRef;
            document.getElementById("lbl-client").innerHTML = data[0].clientByName;
            document.getElementById("lbl-end").innerHTML = data[0].enddate;

            document.getElementById("lbl-budget").innerHTML = getDecimal(data[0].totalbudget);
            document.getElementById("lbl-wotype").innerHTML = data[0].woTyep;
            document.getElementById("lbl-invoice").innerHTML = getParameterByNamefromUrl('invoiceTpye');

            document.getElementById("lbl-comment").innerHTML = data[0].comment;
            setChannelsDetais(data[0].revenueMonth, data[0].revenueLine);
            billingStatus = data[0].billStatus;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
            ;
        }
    });
}

function setChannelsDetais(revMonth, revenueLine)
{
    showDialog();
    var total_WO_amount = 0;
    $.ajax
            ({
                type: 'GET',
                dataType: "json",
                async: false,
                contentType: 'application/json',
                url:  URI.bill.getBillSpotDetailsUrl(),
                data: {
                    workOrderId: v_workOrderId
                },
                success: function (data)
                {
                    closeDialog();
                    var channnelDetails = '';
                    for (var i in data)
                    {
                        var channnelDetailsTable = '<div class="form-group col-sm-12 col-md-12 col-lg-12">';
                        channnelDetailsTable += '<table id="channel_table" class="table table-bordered table-workorder">';
                        channnelDetailsTable += '	<thead><tr><th>Channel</th><th>Rev. Line</th><th>Rev. Month</th><th>TVC Aired/Total</th><th>Logo Aired/Total</th><th>Crowler Aired/Total</th><th>L Sqeeze Aired/Total</th><th>V Sqeeze Aired/Total</th></tr></thead>';
                        channnelDetailsTable += '<tbody>'
                        for (var i in data)
                        {
                            channnelDetailsTable += '<tr> <td class="tb_align_center">' + data[i].channelName + '</a></td><td class="tb_align_center">' + revenueLine + '</td><td class="tb_align_center">' + revMonth + '</td><td class="tb_align_center">' + data[i].tvcAir + '/' + data[i].tvcSpots + '</td><td class="tb_align_center">' + data[i].logoAir + '/' + data[i].logoSpots + '</td><td class="tb_align_center">' + data[i].crowlerAir + '/' + data[i].crowlerSpots + '</td><td class="tb_align_center">' + data[i].l_sqeeze_Air + '/' + data[i].l_sqeezeSpots + '</td><td class="tb_align_center">' + data[i].v_sqeeze_Air + '/' + data[i].v_sqeezeSpots + '</td></tr>';
                        }
                        channnelDetailsTable += '</tbody>';
                        channnelDetailsTable += '</table>';
                        channnelDetailsTable += '</div>';
                    }

                    $('#w_details_div').html(channnelDetailsTable);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    closeDialog();
                    if (errorThrown == 'Unauthorized') {
                        localStorage.clear();
                        window.location.replace("../Login.html");
                    }
                    else {
                        alert("Error");
                    }
                }
            });
}

function setSpotDetails_for_full_Spot(bill_type)
{
    showDialog();
    var total_WO_amount = 0;
    $.ajax
            ({
                type: 'GET',
                dataType: "json",
                async: false,
                contentType: 'application/json',
                url: URI.bill.getBillSpotDetailsUrl(),
                data: {
                    workOrderId: v_workOrderId,
                },
                success: function (data)
                {
                    closeDialog();
                    var channelTable = '<table id="channel_detail_table" class="table table-bordered"><thead class="thead-inverse"><tr><th class="text-center">Channel name</th><th class="text-center">Rate</th><th class="text-center">Total amount</th></tr></thead><tbody>';
                    var channnelDetails = '';
                    var subRow = '';
                    for (var i in data)
                    {
                        subRow = '';
                        var count = 1;
                        if (data[i].tvcPackageAmount != 0)
                        {

                            if (data[i].logoSpotsPackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].logoSpotsPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].logoSpotsPackageAmount) + '</td></tr>';
                                count++;
                            }
                            if (data[i].crowlerPackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].crowlerPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].crowlerPackageAmount) + '</td></tr>';
                                count++;
                            }
                            if (data[i].l_sqeezePackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAmount) + '</td></tr>';
                                count++;
                            }
                            if (data[i].v_sqeezePackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAmount) + '</td></tr>';
                                count++;
                            }
                            channelTable += '<tr><td rowspan="' + count + '" class="text-center">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].tvcPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].tvcPackageAmount) + '</td></tr>';
                            channelTable += subRow;
                        } else if (data[i].logoSpotsPackageAmount != 0) {

                            if (data[i].crowlerPackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].crowlerPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].crowlerPackageAmount) + '</td></tr>';
                                count++;
                            }
                            if (data[i].l_sqeezePackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAmount) + '</td></tr>';
                                count++;
                            }
                            if (data[i].v_sqeezePackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAmount) + '</td></tr>';
                                count++;
                            }

                            channelTable += '<tr><td rowspan="' + count + '">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].logoSpotsPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].logoSpotsPackageAmount) + '</td></tr>';
                            channelTable += subRow;
                        } else if (data[i].crowlerPackageAmount != 0) {
                            if (data[i].l_sqeezePackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAmount) + '</td></tr>';
                                count++;
                            }
                            if (data[i].v_sqeezePackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAmount) + '</td></tr>';
                                count++;
                            }

                            channelTable += '<tr><td rowspan="' + count + '" class="text-center">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].crowlerPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].crowlerPackageAmount) + '</td></tr>';
                            channelTable += subRow;
                        } else if (data[i].l_sqeezePackageAmount != 0) {
                            if (data[i].v_sqeezePackageAmount != 0) {
                                subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAmount) + '</td></tr>';
                                count++;
                            }

                            channelTable += '<tr><td rowspan="' + count + '" class="text-center">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAmount) + '</td></tr>';
                            channelTable += subRow;
                        } else if (data[i].v_sqeezePackageAmount != 0) {
                            channelTable += '<tr><td rowspan="' + count + '" class="text-center">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '/td><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAmount) + '</td></tr>';
                            channelTable += subRow;
                        }

                        total_WO_amount += data[i].tvcPackageAmount;
                        total_WO_amount += data[i].logoSpotsPackageAmount;
                        total_WO_amount += data[i].crowlerPackageAmount;
                        total_WO_amount += data[i].v_sqeezePackageAmount;
                        total_WO_amount += data[i].l_sqeezePackageAmount;

                    }
                    //channelTable += '</tbody></table>';
                    total_A = total_WO_amount;

                    channelTable += '<tr><td>Sub Total :</td><td></td><td class="text-right" id="sub_total" >' + getDecimal(total_A) + '</td></tr>';
                    channelTable += '<tr><td>Adjust total amount: </td><td ><input type="text" class="form-control Padding0px lable_font text-right" id="txt_adjust" name="adjustment" value="0" onkeyup="setAdjustment(this.value)" title="Please enter commission" placeholder="Adjustment"></td><td class="text-right" id="adjust_lbe">' + getDecimal(total_A) + '</td></tr>';

//                    channelTable += '</tbody></table>';

                    /* var sub_total_details = '<label class="control-label col-sm-12 col-md-12 col-lg-12" ></label>';
                     sub_total_details += '<label class="control-label col-sm-7 col-md-7 col-lg-7" ></label>';
                     sub_total_details += '<label class="control-label col-sm-2 col-md-2 col-lg-2 lable_font" style="text-align:right;">Sub Total</label>';
                     sub_total_details += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" >:</label>';
                     sub_total_details += '<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font" id="sub_total" >' + getDecimal(total_A) + '</label>';
                     sub_total_details += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';*/

                    /* var adjust = '<label class="control-label col-sm-6 col-md-6 col-lg-6" ></label>';
                     adjust += '<label class="control-label col-sm-2 col-md-2 col-lg-2 lable_font" style="text-align:right;">Adjust total amount</label>';
                     adjust += '<div class="control-label ccol-sm-1 col-md-1 col-lg-1 lable_font">';
                     adjust += '	<input type="text" class="form-control Padding0px lable_font" id="txt_adjust" name="adjustment" value="0" onkeyup="setAdjustment(this.value)" title="Please enter commission" placeholder="Adjustment">';
                     adjust += '</div>';
                     adjust += '<label class="control-label col-sm-1 col-md-1col-lg-1 lable_font" >:</label>';
                     adjust += '<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font" id="adjust_lbe">' + getDecimal(total_A) + '</label>';
                     adjust += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';*/

//                    $('#sub_total_div').html(sub_total_details);
//                    $('#adjustment_filed').html(adjust);
                    //  $('#channel_detail_div').html(channelTable);
                    setCommissionDetails(channelTable);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    closeDialog();
                    if (errorThrown == 'Unauthorized') {
                        localStorage.clear();
                        window.location.replace("../Login.html");
                    }
                    else {
                        alert("Error");
                    }
                }
            });
}

function setCommissionDetails(detailTable)
{
    var wo_id = v_workOrderId;
    showDialog();
    $.ajax
            ({
                type: 'GET',
                dataType: "json",
                async: false,
                contentType: 'application/json',
                url: URI.workOrder.selectedWorkOrderUrl(),
                data: {
                    workOrderId: wo_id
                },
                success: function (data)
                {
                    closeDialog();
                    var commission = (total_A * data[0].commission) / 100;
                    var after_sub = total_A - commission;

                    detailTable += '<tr><td>Agency Commission: </td><td ><input type="text" class="form-control Padding0px text-right" id="txt_commission" name="commission" value="' + data[0].commission + '" onkeyup="setCommission(this.value)" title="Please enter commission" placeholder="commission"></td><td class="text-right" id="commission_lbe">' + getDecimal(commission) + '</td></tr>';
                    detailTable += '<tr><td>Net amount: </td><td></td><td class="text-right" id="after_sub_total">' + getDecimal(after_sub) + '</td></tr>';

                    /*var agency_commision = '';
                     agency_commision += '		<label class="control-label col-sm-6 col-md-6 col-lg-6" ></label>';
                     agency_commision += '		<label class="control-label col-sm-2 col-md-2 col-lg-2 lable_font" style="text-align:right;">Agency Commission</label>';
                     agency_commision += '		<div class="control-label ccol-sm-1 col-md-1 col-lg-1">';
                     agency_commision += '			<input type="text" class="form-control Padding0px" id="txt_commission" name="commission" value="' + data[0].commission + '" onkeyup="setCommission(this.value)" title="Please enter commission" placeholder="commission">';
                     agency_commision += '		</div>';
                     agency_commision += '		<label class="control-label col-sm-1 col-md-1col-lg-1 lable_font" >:</label>';
                     agency_commision += '		<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font" id="commission_lbe">' + getDecimal(commission) + '</label>';
                     agency_commision += '		<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';
                     */
                    /*var after_total_amount_details = '		<label class="control-label col-sm-7 col-md-7 col-lg-7" ></label>';
                     after_total_amount_details += '		<label class="control-label col-sm-2 col-md-2 col-lg-2 lable_font" style="text-align:right;">Net amount</label>';
                     after_total_amount_details += '		<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" >:</label>';
                     after_total_amount_details += '		<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font" id="after_sub_total" >' + getDecimal(after_sub) + '</label>';
                     after_total_amount_details += '		<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';
                     */
                    //	total_payable=getDecimal(after_sub);
//                    $('#agency_commision_div').html(agency_commision);
//                    $('#after_sub_total_div').html(after_total_amount_details);
                    after_sub_total = getDecimal(after_sub);
                    pub_datailsTable = detailTable;
                    detailTable += '<tr><td id=td-nbt></td><td></td><td class="text-right" id="td-nbt-rate"></td></tr>';
                    detailTable += '<tr><td id=td-vat></td><td></td><td class="text-right" id="td-vat-rate"></td></tr>';
                    detailTable += '<tr><td id=td-ogl></td><td></td><td class="text-right" id="td-ogl-rate"></td></tr>';
                    detailTable += '<tr><td id=td-tcl></td><td></td><td class="text-right" id="td-tcl-rate"></td></tr>';
                    detailTable += '<tr><td>TatalPayable</td><td></td><td class="text-right" id="td-tp-rate"></td></tr>';
                    detailTable += '</tbody></table>';
                    $('#channel_detail_div').html(detailTable);
                    setTaxDetails();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    closeDialog();
                    if (errorThrown == 'Unauthorized') {
                        localStorage.clear();
                        window.location.replace("../Login.html");
                    }
                    else {
                        alert("Error");
                    }
                }
            });
}

function setTaxDetails() {
    var sub_total = after_sub_total;// document.getElementById("after_sub_total").textContent;
    sub_total = sub_total.replace(/,/g, "");
    total_payable = parseFloat(sub_total);
    var bill_button = '<button id="generate-bill" onclick="printBill()" type="submit" class="btn btn_color pull-right">&nbsp;&nbsp;&nbsp;&nbsp; Generate Bill &nbsp;&nbsp;&nbsp;&nbsp;</button>';
    var sap_button = '<button id="sap_btn" onclick="downloadSAP()" type="submit" class="btn btn_color pull-right">&nbsp;&nbsp;&nbsp;&nbsp; download SAP &nbsp;&nbsp;&nbsp;&nbsp;</button>';
    showDialog();
    $.ajax
            ({
                type: 'GET',
                dataType: "json",
                async: false,
                contentType: 'application/json',
                url: URI.bill.getAllTaxDetailsUrl(),
                success: function (data)
                {
                    closeDialog();
                    var taxRow = '';
                    var taxTableRow = '';
                    for (var i in data)
                    {
                        taxTableRow += '<tr>';
                        if (data[i].taxid != 5)
                        {
                            /* taxRow += '<div class="form-group col-sm-12 col-md-12 col-lg-12">';
                             
                             taxRow += '<label class="control-label col-sm-6 col-md-6 col-lg-6" ></label>';
                             taxRow += '<label class="control-label col-sm-3 col-md-3 col-lg-3 lable_font " style="text-align:right;">' + data[i].taxcategory + ' ' + data[i].rate + '%</label>';
                             taxRow += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" >:</label>';
                             taxTableRow += '<td class="text-right">' + data[i].taxcategory + ' ' + data[i].rate + '% :</td><td></td>';
                             */ if (data[i].taxcategory == "NBT") {
                                _nbt = getTax(data[i].rate);
                                document.getElementById("td-nbt").textContent = data[i].taxcategory + ' ' + data[i].rate + '%';
                                document.getElementById("td-nbt-rate").textContent = _nbt;
                            }

                            if (data[i].taxcategory === "Other Government Levy") {
                                document.getElementById("td-ogl").textContent = data[i].taxcategory + ' ' + data[i].rate + '%';
                                document.getElementById("td-ogl-rate").textContent = getVAT(data[i].rate);
                            }

                            if (data[i].taxcategory === "Telecommunication Levy") {
                                document.getElementById("td-tcl").textContent = data[i].taxcategory + ' ' + data[i].rate + '%';
                                document.getElementById("td-tcl-rate").textContent = getVAT(data[i].rate);
                            }

                            if (data[i].taxcategory == "VAT") {
                                /*taxRow += '<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font">' + getVAT(data[i].rate) + '</label>';
                                 taxTableRow += '<td class="text-right">' + getVAT(data[i].rate) + '</td>';*/
                                var vat = getVAT(data[i].rate);
                                document.getElementById("td-vat").textContent = data[i].taxcategory + ' ' + data[i].rate + '%';
                                document.getElementById("td-vat-rate").textContent = vat;
                                vat = vat.replace(/,/g, "");
                                total_payable = parseFloat(total_payable) + parseFloat(vat);
                            } else {
                                /*taxRow += '<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font">' + getTax(data[i].rate) + '</label>';
                                 taxTableRow += '<td class="text-right">' + getTax(data[i].rate) + '</td>'*/
                                var tax = getTax(data[i].rate);
                                tax = tax.replace(/,/g, "");
                                total_payable = parseFloat(total_payable) + parseFloat(tax);
                            }
                            /* taxTableRow += '</tr>';
                             taxRow += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';
                             taxRow += '</Div>';*/
                        }
                    }

                    /* taxRow += '<div class="form-group col-sm-12 col-md-12 col-lg-12">';
                     taxRow += '<label class="control-label col-sm-7 col-md-7 col-lg-7" ></label>';
                     taxRow += '<label class="control-label col-sm-2 col-md-2 col-lg-2 lable_font" style="text-align:right;" >Total Payable</label>';
                     taxRow += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" >:</label>';
                     taxRow += '<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font" id="total_payable">' + getDecimal(total_payable) + '</label>';
                     taxRow += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';
                     taxRow += '</Div>';
                     taxTableRow += '<td class="text-right">Total Payable :</td><td></td><td class="text-right">' + getDecimal(total_payable) + '</td></tr>';*/
                    document.getElementById("td-tp-rate").textContent = getDecimal(total_payable);
                    //pub_datailsTable += taxTableRow + '</tbody></table>';
                    /* $('#tax_div').html(taxTableRow + '</tbody></table>');*/
                    //$('#channel_detail_div').html(pub_datailsTable);
                    //$('#tax_div').html(taxRow);
                    $('#bill_div').html(bill_button);
                    $('#sap_div').html(sap_button);
                    $("#sap_btn").prop("disabled", true);
                    if(billingStatus!=0){
                        $("#generate-bill").prop("disabled", true);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    closeDialog();
                    if (errorThrown == 'Unauthorized') {
                        localStorage.clear();
                        window.location.replace("../Login.html");
                    }
                    else {
                        alert("Error");
                    }
                }
            });
}

function getTax(tax) {
    var total_amount = after_sub_total;//document.getElementById("after_sub_total").textContent;
    total_amount = total_amount.replace(/,/g, "");
    var tax_value = (total_amount * tax) / 100;
    //console.log("Amount :"+total_amount);
    //console.log("Rate :"+tax);
    //console.log("Tax :"+tax_value);
    return getDecimal(tax_value);

}

function getVAT(tax) {
    var _amount = after_sub_total;// document.getElementById("after_sub_total").textContent;
    _amount = _amount.replace(/,/g, "");
    _nbt = _nbt.replace(/,/g, "");
    var total_amount = parseFloat(_nbt) + parseFloat(_amount);
    var tax_value = (total_amount * tax) / 100;
    //console.log("Amount :"+total_amount);
    //console.log("Rate :"+tax);
    //console.log("Tax :"+tax_value);
    return getDecimal(tax_value);
}

function setCommission(value)
{
    if (numberValidation(value))
    {
        var tota_ = document.getElementById("adjust_lbe").textContent;
        tota_ = tota_.replace(/,/g, "");
        var tax_value = (tota_ * value) / 100;
        document.getElementById("commission_lbe").textContent = getDecimal(tax_value);
        var total_amount = document.getElementById("sub_total").textContent;
        total_amount = total_amount.replace(/,/g, "");
        var after_total = tota_ - tax_value;
        document.getElementById("after_sub_total").textContent = getDecimal(after_total);
        after_sub_total = getDecimal(after_total);
        setTaxDetails();
    }
}

function setAdjustment(numb) {
    var tota_ = document.getElementById("sub_total").textContent;
    tota_ = tota_.replace(/,/g, "");
    var comm = document.getElementById("txt_commission").value;
    comm = comm.replace(/,/g, "");
    var currentTotal = 0;
    if (!isNaN(numb) && numb != "" && numb != " ")
    {
        currentTotal = parseFloat(tota_) + parseFloat(numb);
    } else {
        currentTotal = parseFloat(tota_);
    }
    document.getElementById("adjust_lbe").textContent = getDecimal(currentTotal);
    setCommission(comm);
}

var invoiceID = 0;
function printBill()
{
    var wo_id = v_workOrderId;
    var commission_rate = document.getElementById("txt_commission").value;
    var b_total_amount = document.getElementById("sub_total").textContent;
    b_total_amount = b_total_amount.replace(/,/g, "");
    var adjust = document.getElementById("txt_adjust").value;
    adjust = adjust.replace(/,/g, "");

    var billArray = '{"workOrderId":' + wo_id + ',"invoiceType":' + invoice_type + ',"commissionRate":' + commission_rate + ',"totalAmount":"' + b_total_amount + '","adjustment":"' + adjust + '"}';

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.bill.saveInvoiceUrl(),
        data: {
            invoiceData: billArray,
        },
        success: function (data)
        {
            closeDialog();
            var bill_button = '<div class="form-group col-sm-12 col-md-12 col-lg-12"><button onclick="downloadTR()" type="submit" class="btn btn-primary pull-right">&nbsp;&nbsp;&nbsp;&nbsp; Download TR &nbsp;&nbsp;&nbsp;&nbsp;</button></div>';
            if (data != 0) {
                invoiceID = data;
                $("#sap_btn").prop("disabled", false);
                window.location = URI.bill.downloadBillUrl();
                $('#bill_div').html(bill_button);

                if (invoice_type == 1 || invoice_type == 2) {
                    var button = '<div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="fullBill()" class="btn btn-success pull-right" id="btn_full" disabled>Bill scheduled</button></div><div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="airSpotBill()" class="btn btn-success pull-lift" id="btn_air" disabled>Bill aired</button></div>';
                    $('#bill_category').html(button);
                } else if (invoice_type == 3) {
                    var button = '<div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="fullBill()" class="btn btn-success pull-right" id="btn_new" disabled>Bill scheduled</button></div>';
                    $('#bill_category').html(button);
                } else if (invoice_type == 4 || invoice_type == 5) {
                    var button = '<div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="fullBill()" class="btn btn-success pull-right" id="btn_full" disabled>Bill scheduled</button></div><div class="form-group col-sm-6 col-md-6 col-lg-6"><button type="button" onclick="airSpotBill()" class="btn btn-success pull-lift" id="btn_air" disabled>Bill aired</button></div>';
                    $('#bill_category').html(button);
                }
            } else {
                alert("Check");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    })

}

function downloadTR() {
    window.location = URI.report.downloadPDF1Url();
}

function downloadSAP() {
    window.location = URI.bill.downloadSapUrl(invoiceID);
}

function createSpotDetailArray() {
    var chanelTable = document.getElementById("channel_detail_table");
}

function fullBill()
{
    //bill_type=1 expiar schedule, bill to all schedule amount
    setSpotDetails_for_full_Spot(1);
    invoice_type = selected_invoice + 1;
}

function airSpotBill() {
    setSpotDetails_for_air_Spot(2);
    invoice_type = selected_invoice + 2;
}

function setSpotDetails_for_air_Spot(bill_type)
{
    showDialog();
    var total_WO_amount = 0;
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.bill.getBillSpotDetailsUrl(),
        data: {
            workOrderId: v_workOrderId,
        },
        success: function (data)
        {
            closeDialog();
            var channelTable = '<table id="channel_detail_table" class="table table-bordered"><thead class="thead-inverse"><tr><th class="text-center">Channel name</th><th class="text-center">Rate</th><th class="text-center">Total amount</th></tr></thead><tbody>';
            var channnelDetails = '';
            var subRow = '';
            for (var i in data)
            {
                var count = 1;
                if (data[i].tvcPackageAmount != 0)
                {

                    if (data[i].logoSpotsPackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].logoSpotsPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].logoAirPackageAmount) + '</td></tr>';
                        count++;
                    }
                    if (data[i].crowlerPackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].crowlerPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].crowlerAirPackageAmount) + '</td></tr>';
                        count++;
                    }
                    if (data[i].l_sqeezePackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezeAirPackageAmount) + '</td></tr>';
                        count++;
                    }
                    if (data[i].v_sqeezePackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezeAirPackageAmount) + '</td></tr>';
                        count++;
                    }
                    channelTable += '<tr><td rowspan="' + count + '">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].tvcPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].tvcAirPackageAmount) + '</td></tr>';
                    channelTable += subRow;
                } else if (data[i].logoSpotsPackageAmount != 0) {

                    if (data[i].crowlerPackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].crowlerPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].crowlerAirPackageAmount) + '</td></tr>';
                        count++;
                    }
                    if (data[i].l_sqeezePackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezeAirPackageAmount) + '</td></tr>';
                        count++;
                    }
                    if (data[i].v_sqeezePackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezeAirPackageAmount) + '</td></tr>';
                        count++;
                    }

                    channelTable += '<tr><td rowspan="' + count + '">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].logoSpotsPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].logoAirPackageAmount) + '</td></tr>';
                    channelTable += subRow;
                } else if (data[i].crowlerPackageAmount != 0) {
                    if (data[i].l_sqeezePackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezeAirPackageAmount) + '</td></tr>';
                        count++;
                    }
                    if (data[i].v_sqeezePackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezeAirPackageAmount) + '</td></tr>';
                        count++;
                    }

                    channelTable += '<tr><td rowspan="' + count + '">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].crowlerPackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].crowlerAirPackageAmount) + '</td></tr>';
                    channelTable += subRow;
                } else if (data[i].l_sqeezePackageAmount != 0) {
                    if (data[i].v_sqeezePackageAmount != 0) {
                        subRow += '<tr><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezeAirPackageAmount) + '</td></tr>';
                        count++;
                    }

                    channelTable += '<tr><td rowspan="' + count + '">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].l_sqeezeAirPackageAmount) + '</td></tr>';
                    channelTable += subRow;
                } else if (data[i].v_sqeezePackageAmount != 0) {
                    channelTable += '<tr><td rowspan="' + count + '">' + data[i].channelName + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezePackageAvarage) + '</td><td class="text-right">' + getDecimal(data[i].v_sqeezeAirPackageAmount) + '</td></tr>';
                    channelTable += subRow;
                }

                total_WO_amount += data[i].tvcAirPackageAmount;
                total_WO_amount += data[i].logoAirPackageAmount;
                total_WO_amount += data[i].crowlerAirPackageAmount;
                total_WO_amount += data[i].v_sqeezeAirPackageAmount;
                total_WO_amount += data[i].l_sqeezeAirPackageAmount;

            }
            // channelTable += '</tbody></table>';
            total_A = total_WO_amount;

            /* var sub_total_details = '<label class="control-label col-sm-7 col-md-7 col-lg-7 " ></label>';
             sub_total_details += '<label class="control-label col-sm-2 col-md-2 col-lg-2 lable_font" >Sub Total</label>';
             sub_total_details += '<label class="control-label col-sm-1 col-md-1 col-lg-1" >:</label>';
             sub_total_details += '<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font" id="sub_total" >' + getDecimal(total_A) + '</label>';
             sub_total_details += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';
             
             var adjust = '<label class="control-label col-sm-7 col-md-7 col-lg-7 " ></label>';
             adjust += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" >Adjust total amount</label>';
             adjust += '<div class="control-label ccol-sm-1 col-md-1 col-lg-1 lable_font">';
             adjust += '	<input type="text" class="form-control Padding0px" id="txt_adjust" name="adjustment" value="0" onkeyup="setAdjustment(this.value)" title="Please enter commission" placeholder="Adjustment">';
             adjust += '</div>';
             adjust += '<label class="control-label col-sm-1 col-md-1col-lg-1 lable_font" >:</label>';
             adjust += '<label class="control-label col-sm-1 col-md-1 col-lg-1 text-right lable_font" id="adjust_lbe">' + getDecimal(total_A) + '</label>';
             adjust += '<label class="control-label col-sm-1 col-md-1 col-lg-1 lable_font" ></label>';
             
             $('#sub_total_div').html(sub_total_details);
             $('#adjustment_filed').html(adjust);
             $('#channel_detail_div').html(channelTable);*/
            channelTable += '<tr><td>Sub Total :</td><td></td><td class="text-right" id="sub_total" >' + getDecimal(total_A) + '</td></tr>';
            channelTable += '<tr><td>Adjust total amount: </td><td ><input type="text" class="form-control Padding0px lable_font text-right" id="txt_adjust" name="adjustment" value="0" onkeyup="setAdjustment(this.value)" title="Please enter commission" placeholder="Adjustment"></td><td class="text-right" id="adjust_lbe">' + getDecimal(total_A) + '</td></tr>';

            setCommissionDetails(channelTable);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}
