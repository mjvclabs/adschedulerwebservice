var invoiceId = 0;
var invoiceDataId = 0;
$(document).ready(function () {
    loadInvoiceDetails();
});

$(function () {
    $('#invoice-date').datetimepicker({
        pickTime: false
    });
});

$(function () {
    $('#payment-date').datetimepicker({
        pickTime: false
    });
});

function loadInvoiceDetails() {
    showDialog();
    //invoiceId = getParameterByNamefromUrl("invoiceID")
    invoiceId = $('#invoice_id').val();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.bill.getInvoiceDetailsUrl(),
        data: {
            invoiceId: invoiceId
        },
        success: function (data)
        {
            closeDialog();
            if (data.invoiceDataId === null) {
                $('#div-topic').html('<p class="header_well_pa">Add Details<p>');
            } else {
                invoiceDataId = data.invoiceDataId;
                $('#txt-invoice-date').val(data.invoiceDate);
                $('#txt-invoice-no').val(data.invoiceNo);
                $('#txt-hand-over-me').val(data.handOverDate_ME);
                $('#txt-hand-over-agency').val(data.handOverDate_Agency);
                $('#txt-payment-date').val(data.Payment_Date);
                $('#txt-paid-ref').val(data.paidRef);
                $('#txt-paid-amount').val(data.paidAmount);
                $('#txt-difference').val(data.difference);
                $('#txt-clearin-doc').val(data.clearingDocNo);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function addDetails() {

    var invoiceDate = $('#txt-invoice-date').val();
    var invoiceNo = $('#txt-invoice-no').val();
    if (!numberValidation(invoiceNo)) {
        return;
    }
    var handOverDate_ME = $('#txt-hand-over-me').val();
    var handOverDate_Agency = $('#txt-hand-over-agency').val();
    var Payment_Date = $('#txt-payment-date').val();
    var paidRef = $('#txt-paid-ref').val();
    var paidAmount = $('#txt-paid-amount').val();
    if (!numberValidation(paidAmount)) {
        return;
    }
    var difference = $('#txt-difference').val();
    var clearingDocNo = $('#txt-clearin-doc').val();

    var data = '{"invoiceDataID":' + invoiceDataId + ',"invoiceID":' + invoiceId + ',"invoiceDate":"' + invoiceDate + '","invoiceNo":' + removeLeadingZeros(invoiceNo) + ',"handOverDate_ME":"' + handOverDate_ME + '","handOverDate_Agency":"' + handOverDate_Agency + '","Payment_Date":"' + Payment_Date + '","paidRef":"' + paidRef + '","paidAmount":' + removeLeadingZeros(paidAmount) + ',"difference":"' + difference + '","clearingDocNo":"' + clearingDocNo + '"}';

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url:  URI.bill.saveInvoiceDetailsUrl(),
        data: {
            invoiceData: data
        },
        success: function (data)
        {
            closeDialog();
            invoiceDataId = data.invoiceDataId;
            $('#txt-invoice-date').val(data.invoiceDate);
            $('#txt-invoice-no').val(data.invoiceNo);
            $('#txt-hand-over-me').val(data.handOverDate_ME);
            $('#txt-hand-over-agency').val(data.handOverDate_Agency);
            $('#txt-payment-date').val(data.Payment_Date);
            $('#txt-paid-ref').val(data.paidRef);
            $('#txt-paid-amount').val(data.paidAmount);
            $('#txt-difference').val(data.difference);
            $('#txt-clearin-doc').val(data.clearingDocNo);
            viewMessageToastTyped("Successfully saved the details", "success");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function addDetailsAfterValidation(form){
    if(validateInputFields(form)){
        addDetails();
    }
}

function removeLeadingZeros(val){
    val = val.replace(/^0+/, '')
    return val;
}
