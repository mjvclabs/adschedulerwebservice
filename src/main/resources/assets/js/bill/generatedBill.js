$(document).ready(function () {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.bill.getAllInvoiceUrl(),
        success: function (data)
        {
            closeDialog();
            var tableGeneratedBill = '<table id="generated_bill_table" class="table table-bordered table-workorder"><thead class="tb_align_center"><tr><th>Invoice Id</th><th>Work order Id</th><th>Client</th><th>Commission</th><th>Total Amount</th><th></th><th></th><th></th></tr></thead><tbody>';
            for (var i in data) {
                if (data[i]){ // For Jira issue NRM-832
                    tableGeneratedBill += '<tr class="tb_align_center"><td id="t1c1_' + i + '">' + data[i].invoiceid + '</a></td><td id="tNamec2_' + i + '">' + data[i].workOrderId + '</td><td id="t1c2_' + i + '">' + data[i].billingclient + '</td><td id="t2c3_' + i + '">' + data[i].commissionRate + '</td><td id="t1c3_' + i + '">' + data[i].totalAmount + '</td><td><button type="submit" id="' + data[i].invoiceid + '" onclick="editData(this.id)" class="btn btn-default sche_advert_delete" >Edit</button></td><td><button type="submit" id="' + data[i].invoiceid + '" onclick="getBill(this.id)" class="btn btn-default sche_advert_delete" >Print Bill</button></td><td><button type="submit" id="' + data[i].invoiceid + '" onclick="getSapFile(this.id)" class="btn btn-default sche_advert_delete" >SAP File</button></td></tr>';
                }
            }
            tableGeneratedBill += ' </tbody></table>';
            ;
            $('#generated_bill_div').html(tableGeneratedBill);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
});

function getBill(id)
{
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.bill.generatedBillUrl(),
        data: {
            invoiceId: id
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                window.location = URI.bill.downloadBillUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function getSapFile(id) {
    window.location = URI.bill.downloadSapUrl(id);
}

function editData(id) {
    window.location = URI.bill.editDetailsUrl(id);
    //window.location.replace('EditDetails.html?invoiceID=' + id);
}
