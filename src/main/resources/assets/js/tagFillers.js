var iAdvertID = -1;
var fetchDone;
$(document).ready(function () {
    iAdvertID = stoi(getParameterByNamefromUrl('advertID'));
    loadingOpen();
    fetchDone = _.after(2, function(){
        loadFillerTagChannels();
        loadingClose();
    });
    loadAdvertDetails();
    loadChannels();
});

async function loadChannels() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            for (var i in data) {
                $('#dd_channel').append('<option value="' + data[i].channelid + '">' + data[i].channelname + '</option>');
            }
            $('#dd_channel').selectpicker('refresh');
            fetchDone();
        },
        error: function (jqXHR, textStatus, thrownError) {
            alert("Error has occurred while fetching channels.");
            fetchDone();
        }
    });
}

async function loadAdvertDetails() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.getbyidUrl(),
        data: {
            id: iAdvertID
        },
        success: function (data)
        {
            closeDialog();
            document.getElementById("txt_advert_id").value = data.advertid;
            document.getElementById("txt_advert_name").value = data.advertname;
            document.getElementById("txt_client_name").value = data.client.clientname;
            fetchDone();
        }
        ,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error has occurred while fetching advert details.");
            fetchDone();
        }
    })
}

function loadFillerTagChannels() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.fillerTag.getChannelsUrl(iAdvertID),
        data: {
            id: iAdvertID
        },
        success: function (data)
        {
            $('#dd_channel').val(data);
            $('#dd_channel').selectpicker('refresh');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error has occurred while fetching filler tag channel details.");
        }
    })
}

function save() {
    loadingOpen();
    var data = _.map($('#dd_channel').val(), function(d) {
                        return {"channelDetails": { "channelid": parseInt(d)}, "advertisement": { "advertid" : iAdvertID} }
                    });
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    $.ajax({
        url:URI.fillerTag.saveOrUpdate(iAdvertID),
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: JSON.stringify(data),
        processData: false,
        type: 'post',
        success: function (data){
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error has occurred while save/update filler tags.");
            loadingClose();
        }
    });
}