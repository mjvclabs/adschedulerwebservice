/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 var manualURL = '';
 var autoURL = '';
 var mainURL =  '';

$(document).ready(function () {
    manualURL = URI.dashboard.manualChannelsUrl();
    autoURL = URI.dashboard.autoChannelsUrl();
    mainURL =  URI.dashboard.manualChannelsUrl();
    getPageNamefromUrl();
    loadChannels();
});

//var manualURL = URI.dashboard.manualChannelsUrl();
//var autoURL = URI.dashboard.autoChannelsUrl();
//var mainURL =  URI.dashboard.manualChannelsUrl();

var myVar = setInterval(myTimer, 1000);
var countOne = 0;
var countTwo = 0;
var countThree = 0;

function myTimer() {

    if (countOne == 20) {
        countOne = 0;
        countTwo++;
        if (countTwo != 6) {
            getChannelUpdate();
        }
    } else {
        countOne++;
    }

    if (countTwo == 6) {
        countTwo = 0;
        loadChannels();
        setCurrentdate();
    }
}
function setCurrentdate() {
    var Currdate = new Date();
    var Sday = Currdate.getDate();
    var Syear = Currdate.getFullYear();
    var MonIndex = Currdate.getMonth();

    //Declare Full month Array Variable
    var FullMonth = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    //Assign Final Output
    var FinalOut = Sday + " " + FullMonth[MonIndex] + " " + Syear
    document.getElementById("Format1").innerHTML = " " + FinalOut + " "
}

function getPageNamefromUrl() {
    url = window.location.href;
    var page = url.split("/").pop();
    if (page == 'manual') {
        mainURL = manualURL;
    } else {
        mainURL = autoURL;
    }
}

/*-----------channel propaty----------*/
/**
 * Valid channels are getting by service
 */
var allChannel = [];
function loadChannels() {
    var table = document.getElementById("tbl_dashboard");
    while (table.rows.length > 0) {
        table.deleteRow(0);
    }
    var date = getDate();
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: mainURL,
        data: {
            selectedDate: date
        },
        success: function (data)
        {
            if (allChannel.length == 0) {
                allChannel = data;
            } else {
                var check = true;
                for (var i = 0; i < allChannel.length; i++)
                {
                    check = true;
                    for (var j = 0; j < data.length; j++) {
                        if (allChannel[i].channelID == data[j].channelID) {
                            allChannel[i].updated = data[j].updated
                            allChannel[i].runServer = data[j].runServer
                            allChannel[i].logoPath = data[j].logoPath
                            if (allChannel[i].newlyAdded) {
                                var diff = Math.abs(new Date() - allChannel[i].addedtime);
                                if ((diff / 3600000) >= 1) {
                                    allChannel[i].newlyAdded = false;
                                }
                                allChannel[i].updated = false;
                            }
                            data.splice(j, 1);
                            j--;
                            check = false;
                        }
                    }

                    if (check) {
                        allChannel.splice(i, 1);
                        i--;
                    }
                }

                for (var i = 0; i < data.length; i++) {

                    var currentD = new Date();
                    var startHour = new Date();
                    startHour.setHours(09, 00, 0);

                    if (currentD >= startHour) {
                        data[i].newlyAdded = true;
                        data[i].addedtime = new Date();
                        data[i].updated = false;
                        allChannel.push(data[i]);
                    }
                }
            }
            var channels = '<div class="channel-cover">&nbsp;</div><ul>';
            for (var i in allChannel)
            {
                if (allChannel[i].newlyAdded) {
                    channels += new_channel() + '<img class="tv-logo" src="' + URI.image.channelImagePath(allChannel[i].channelID)+ '"/>';
                } else {
                    channels += '<li><img class="tv-logo" src="'+ URI.image.channelImagePath(allChannel[i].channelID)+ '"/>';
                }
                if (allChannel[i].runServer) {
                    channels += backEnd_server_run();
                }
                if (allChannel[i].updated) {
                    channels += channel_data_updated();
                }
                channels += '</li>';
                AddRowTo_tbl_dashboard(allChannel[i].channelID);
            }
            channels += '</ul>';
            $('#channel-column').html(channels);
            getChannelUpdate();
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alert("Error");
        }
    })
}

/*
 * retuen green mark tag
 */
function backEnd_server_run() {
    return '<i class="glyphicon glyphicon glyphicon-ok green"></i>';
}

/*
 * retuen red mark tag
 */
function channel_data_updated() {
    return '<i class="glyphicon glyphicon-arrow-up animated infinite slideOutUp red"></i>';
}

function new_channel() {
    return '<li class="newly-added">';
}
/*-----------channel propaty----------*/
/**
 * Comment
 */
function AddRowTo_tbl_dashboard(channelID) {

    var advertTable = document.getElementById("tbl_dashboard");
    var rowCount = advertTable.rows.length;
    var row = advertTable.insertRow();
    var cell0 = row.insertCell(0);
    cell0.innerHTML = '<div id="' + channelID + '_00"></div>';
    cell0.setAttribute("value", "00.00");
    var cell1 = row.insertCell(1);
    cell1.innerHTML = '<div id="' + channelID + '_01"></div>';
    cell1.setAttribute("value", "01.00");
    var cell2 = row.insertCell(2);
    cell2.innerHTML = '<div id="' + channelID + '_02"></div>';
    cell2.setAttribute("value", "02.00");
    var cell3 = row.insertCell(3);
    cell3.innerHTML = '<div id="' + channelID + '_03"></div>';
    cell3.setAttribute("value", "03.00");
    var cell4 = row.insertCell(4);
    cell4.innerHTML = '<div id="' + channelID + '_04"></div>';
    cell4.setAttribute("value", "04.00");
    var cell5 = row.insertCell(5);
    cell5.innerHTML = '<div id="' + channelID + '_05"></div>';
    cell5.setAttribute("value", "05.00");
    var cell6 = row.insertCell(6);
    cell6.innerHTML = '<div id="' + channelID + '_06"></div>';
    cell6.setAttribute("value", "06.00");
    var cell7 = row.insertCell(7);
    cell7.innerHTML = '<div id="' + channelID + '_07"></div>';
    cell7.setAttribute("value", "07.00");
    var cell8 = row.insertCell(8);
    cell8.innerHTML = '<div id="' + channelID + '_08"></div>';
    cell8.setAttribute("value", "08.00");
    var cell9 = row.insertCell(9);
    cell9.innerHTML = '<div id="' + channelID + '_09"></div>';
    cell9.setAttribute("value", "09.00");
    var cell10 = row.insertCell(10);
    cell10.innerHTML = '<div id="' + channelID + '_10"></div>';
    cell10.setAttribute("value", "10.00");
    var cell11 = row.insertCell(11);
    cell11.innerHTML = '<div id="' + channelID + '_11"></div>';
    cell11.setAttribute("value", "11.00");
    var cell12 = row.insertCell(12);
    cell12.innerHTML = '<div id="' + channelID + '_12"></div>';
    cell12.setAttribute("value", "12.00");
    var cell13 = row.insertCell(13);
    cell13.innerHTML = '<div id="' + channelID + '_13"></div>';
    cell13.setAttribute("value", "13.00");
    var cell14 = row.insertCell(14);
    cell14.innerHTML = '<div id="' + channelID + '_14"></div>';
    cell14.setAttribute("value", "14.00");
    var cell15 = row.insertCell(15);
    cell15.innerHTML = '<div id="' + channelID + '_15"></div>';
    cell15.setAttribute("value", "15.00");
    var cell16 = row.insertCell(16);
    cell16.innerHTML = '<div id="' + channelID + '_16"></div>';
    cell16.setAttribute("value", "16.00");
    var cell17 = row.insertCell(17);
    cell17.innerHTML = '<div id="' + channelID + '_17"></div>';
    cell17.setAttribute("value", "17.00");
    var cell18 = row.insertCell(18);
    cell18.innerHTML = '<div id="' + channelID + '_18"></div>';
    cell18.setAttribute("value", "18.00");
    var cell19 = row.insertCell(19);
    cell19.innerHTML = '<div id="' + channelID + '_19"></div>';
    cell19.setAttribute("value", "19.00");
    var cell20 = row.insertCell(20);
    cell20.innerHTML = '<div id="' + channelID + '_20"></div>';
    cell20.setAttribute("value", "20.00");
    var cell21 = row.insertCell(21);
    cell21.innerHTML = '<div id="' + channelID + '_21"></div>';
    cell21.setAttribute("value", "21.00");
    var cell22 = row.insertCell(22);
    cell22.innerHTML = '<div id="' + channelID + '_22"></div>';
    cell22.setAttribute("value", "22.00");
    var cell23 = row.insertCell(23);
    cell23.innerHTML = '<div id="' + channelID + '_23"></div>';
    cell23.setAttribute("value", "23.00");
}

function test() {
    var data = '<label class="tvc">0</label><label class="lc">0</label><label class="cg">0</label>';
    var dt = '<label class="tvc">3/5<i class="glyphicon glyphicon-arrow-down animated infinite slideOutDown black"></i></label><label class="lc">2/4 </label><label class="cg">1/5</label>'
    $('#8_20').html(data);
    // $('#8_00').html(data);
    $('#8_10').html(dt);
}

function getChannelUpdate() {
    var channelIdList = [];
    for (var i in allChannel) {
        channelIdList.push(allChannel[i].channelID);
    }
    $.ajax({
        async: false,
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.scheduler.getOneDaySchedulesUrl(),
        data: {
            workOrderId: -111,
            channelId: JSON.stringify(channelIdList),
            advertId: -111,
            advertType: -111,
            date: getDate(),
            startHour: -111,
            endHour: -111
        },
        success: function(data) {
            for (var j in data) {
                var hour = getHour(data[j].scheduleStartTime);
                var currentHour = getCurrentHour();

                var hour_id = "#" + data[j].channelId + "_" + hour;
                var itemList = data[j].scheduleItems

                var tvcCount = 0;
                var tvcAiredCount = 0;
                var logoCount = 0;
                var logoAiredCount = 0;
                var cgCount = 0;
                var cgAiredCount = 0;
                for (var k in itemList) {
                    if (itemList[k].status != 'Suspended' && itemList[k].status != 'No Media' && itemList[k].isFileAvailable != 0) {
                        if (itemList[k].advertType == 'ADVERT') {
                            tvcCount++;
                            if (itemList[k].status == 'Played') {
                                tvcAiredCount++;
                            }
                        } else if (itemList[k].advertType == 'LOGO') {
                            logoCount++;
                            if (itemList[k].status == 'Played') {
                                logoAiredCount++;
                            }
                        } else if (itemList[k].advertType == 'L_SHAPE' || itemList[k].advertType == 'V_SHAPE' || itemList[k].advertType == 'CRAWLER') {
                            cgCount++;
                            if (itemList[k].status == 'Played') {
                                cgAiredCount++;
                            }
                        }
                    }
                }

                var tvcValue = '';
                var logoValue = '';
                var cgValue = '';

                if (currentHour > hour) {
                    if (tvcCount != 0) {
                        if (tvcAiredCount < tvcCount) {
                            tvcValue = getTVCTag(tvcAiredCount + "/" + tvcCount + getMissedarrow());
                        } else {
                            tvcValue = getTVCTag(tvcAiredCount + "/" + tvcCount);
                        }
                    }
                    if (logoCount != 0) {
                        if (logoAiredCount < logoCount) {
                            logoValue = getLOGOTag(logoAiredCount + "/" + logoCount + getMissedarrow());
                        } else {
                            logoValue = getLOGOTag(logoAiredCount + "/" + logoCount);
                        }
                    }
                    if (cgCount != 0) {
                        if (cgAiredCount < cgCount) {
                            cgValue = getCGTag(cgAiredCount + "/" + cgCount + getMissedarrow());
                        } else {
                            cgValue = getCGTag(cgAiredCount + "/" + cgCount);
                        }
                    }

                } else {
                    if (tvcCount != 0) {
                        tvcValue = getTVCTag(tvcAiredCount + "/" + tvcCount);
                    }
                    if (logoCount != 0) {
                        logoValue = getLOGOTag(logoAiredCount + "/" + logoCount);
                    }
                    if (cgCount != 0) {
                        cgValue = getCGTag(cgAiredCount + "/" + cgCount);
                    }
                }
                $(hour_id).html(tvcValue + logoValue + cgValue);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            } else {
                alert("Error");
            }
        }
    })
}

function getDate()
{
    var result = new Date();
    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    date = yyyy + '/' + mm + '/' + dd;
    return yyyy + '/' + mm + '/' + dd;
}

function getCurrentHour() {
    var now = new Date();
    var hours = now.getHours();
    return hours;
}

function getHour(time) {
    var realTime = time.split(":");
    return realTime[0];
}

function getMissedarrow() {
    return '<i class="glyphicon glyphicon-arrow-down animated infinite slideOutDown black"></i>';
}

function getTVCTag(count) {

    return '<label class="tvc font-changes">' + count + '</label>';
}

function getLOGOTag(count) {

    return '<label class="lc font-changes">' + count + '</label>';
}

function getCGTag(count) {

    return '<label class="cg font-changes">' + count + '</label>';
}


