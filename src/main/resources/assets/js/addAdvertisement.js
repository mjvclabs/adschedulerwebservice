var iAdvertID = -1;

$(document).ready(function () {
    showDialog();

    loadClients();

    $('#logo_container').hide();
    $('#file_upload').hide();
    $('#logo_container').val("-1");
    $('#txt_advert_path_add_a').val("\\\\f$\\\\TVCs");
    $('#txt_advert_path_add_a').removeAttr('required');
    $("#flash").hide();
    $("#image").hide();
    closeDialog();
});

$(function () {
    $('#expire-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});

$(function () {
    $('#suspend-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});

$(function () {
//    $('#txt_expireDate').datetimepicker("setDate", null).change();
    $('#expire-date').datetimepicker("setDate", null).change(); //For Jira issue NRM-834
});

$(function () {
//    $('#txt_suspendDate').datetimepicker("setDate", null).change();
    $('#suspend-date').datetimepicker("setDate", null).change(); //For Jira issue NRM-834
});

function saveAdvert() {
    var clientName = $("#cmbClient").val();
    if (clientName == -1) {
        viewMessageToastTyped("Please select client", "Warning");
        return;
    }
    var advertName = $.trim($("#txt_advert_name_add_a").val());
    var rg1 = /^[a-zA-Z0-9_@()-\s\\&-]+$/;

    if (!rg1.test(advertName)) {
        closeDialog();
        viewMessageToastTyped("Invalid file name.", "Warning");
        return;
    }

    var advertPath = $("#txt_advert_path_add_a").val();
    var advertType = $("#advertisement_type_Dropdown").val();
    if (advertType == -1) {
        viewMessageToastTyped("Please select Advertisement type", "Warning");
        return;
    }
    var advertCategory = $("#commerical_categoryDropdown").val();
    if (advertCategory == -1) {
        viewMessageToastTyped("Please select Commerical category", "Warning");
        return;
    }
    var duration = stoi($("#txt_duration_add_a").val());
    if(duration <= 0) {
        closeDialog();
        viewMessageToastTyped("Please enter not zero value for the duration.", "Warning");
        return;
    }

    var txt_expireDate = $("#txt_expireDate").val();
    var txt_suspendDate = $("#txt_suspendDate").val();
    var language = $("#cmbLanguage").val();
    if (language == -1) {
        viewMessageToastTyped("Please select Advertisement language", "Warning");
        return;
    }
    var vedioType = $("#cmbVideoType").val();
    if (vedioType == -1) {
        viewMessageToastTyped("Please select Advertisement Video Type", "Warning");
        return;
    }
    var with_p = $("#txt_width").val();
    var height_p = $("#txt_hight").val();
    var x_position = $("#txt_x_position").val();
    var y_position = $("#txt_y_position").val();
    var logo_cont = $("#logo_container").val();
    var ad_path = advertPath + '\\\\' + advertName + '.mpg';
    if (txt_expireDate == "") {
        txt_expireDate = "2100-12-31";
    }
    if (txt_suspendDate == "") {
        txt_suspendDate = "2100-12-31";
    }
    var codeMapping = $('#cmbProduct').val();
    var dataArray = '{"advertId":"","client":"' + clientName + '","advertName":"' + advertName + '","advertPath":"' + ad_path + '","advertType":"' + advertType + '","advertCategory":"' + advertCategory + '","duration":' + stoi(duration) + ',"language":"' + language + '","with":' + with_p + ',"hight":' + height_p + ',"x_position":' + x_position + ',"y_position":' + y_position + ',"logo_container":' + logo_cont + ',"expireDate":"' + txt_expireDate + '","suspendDate":"' + txt_suspendDate + '","vedioType":"' + vedioType +'","codeMapping":"'+ codeMapping + '" }';
    console.log(dataArray);

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementSaveUrl(),
        data: {
            advertData: dataArray,
        },
        success: function (data)
        {
            closeDialog();
            iAdvertID = data;
            console.log("advert_id : " + iAdvertID);

            if (advertType == "LOGO_CONTAINER") {
                saveMultipleLogos();
                return;
            }

            $("#advert_data :input").prop("disabled", true);
            $('#file_upload').show();
            viewMessageToastTyped("Advertisement successfully saved", "success");

        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function saveAdvertAfterValidation(form){
    if(validateInputFields(form)){
        saveAdvert();
    }
}

function loadProducts(){
    var clientId = $('#cmbClient').val();
    if(clientId === "-1"){
        return;
    }
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.product.getProductsByClient(clientId),
        success: function (data)
        {
            closeDialog();
            var trHTML = '';
            trHTML += ' <select id="cmbProduct" name="" class="form-control" required="">';
            data.forEach(function(c){
                var label = c.product.name + ' - ' + c.brand.name + ' | ' + c.code;
                trHTML += '<option value="' + c.id + '">' + label + '</option>';
            })
            trHTML += '</select>';
            trHTML += '<span class="help-block"></span>';
            $('#product_select').html(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function loadClients() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.client.getAllClientOrderByName(),
        success: function (data)
        {
            closeDialog();
            var trHTML = '';
            trHTML += ' <select id="cmbClient" name="" class="form-control" required=""  onChange="loadProducts()">';
            trHTML += '<option value="-1"></option>';
            for (var i in data)
            {
                if (data[i].clienttype == 'Client') {
                    trHTML += '<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                }
            }
            trHTML += '</select>';
            trHTML += '<span class="help-block"></span>';

            $('#client_select').html(trHTML);
            loadLogoContainers();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function loadLogoContainers() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.logoContainer.getallUrl(),
        success: function (data)
        {
            closeDialog();
            var trHTML = '';
            trHTML += '<select id="cmbLogoContainer" name="" class="form-control" required="">';
            trHTML += '<option value="-1"></option>';
            for (var i in data)
            {
                trHTML += '<option value="' + data[i].containerId + '">' + data[i].containerName + '</option>';
            }
            trHTML += '</select>';
            trHTML += '<span class="help-block"></span>';

            $('#logo_select').html(trHTML);
            getCommericalCategory();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function getCommericalCategory() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementCategoryAllListUrl(),
        success: function (data)
        {
            closeDialog();

            var dropDown = '<div class="dropdown"><select id="commerical_categoryDropdown" name="clientName" class="form-control" required="">';
            dropDown += '<option value="-1"></option><option >None</option>';

            for (var i in data)
            {
                dropDown += '<option >' + data[i].category + '</option>';
            }
            dropDown += '</select></div><span class="help-block"></span>';
            $('#dro_commerical_category_add_a').html(dropDown);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function toggleProperties() {
    $header = $('#expand_header');
    //getting the next element
    $content = $header.next();
    $content.slideToggle(500, function () {
        $header.html(function () {
            //change text based on condition
            return $content.is(":visible") ? '<span><label class="control-label col-md-12 lable_font">Advance Properties &#x25B2;</label></span>' :
                    '<span><label class="control-label col-md-12 lable_font">Advance Properties &#x25BC;</label></span>';
        });
    });
}

function advertTypeDropDownChange(type_value) {
    $('#logo_container').hide();
    $('#logo_list_table').hide();
    if (type_value == "ADVERT") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("720");
        $('#txt_hight').val("576");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("0");
        $('#fileupload').attr( "accept", ".mp4, .mov, .avi, .mpg" );
    } else if (type_value == "CRAWLER") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("720");
        $('#txt_hight').val("576");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("0");
        $('#fileupload').attr( "accept", ".swf" );
    } else if (type_value == "V_SHAPE") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("720");
        $('#txt_hight').val("100");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("476");
        $('#fileupload').attr( "accept", ".swf" );
    } else if (type_value == "L_SHAPE") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("600");
        $('#txt_hight').val("476");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("0");
        $('#fileupload').attr( "accept", ".swf" );
    } else if (type_value == "LOGO") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("100");
        $('#txt_hight').val("100");
        $('#txt_x_position').val("520");
        $('#txt_y_position').val("476");
        $('#fileupload').attr( "accept", ".swf" );
        $('#logo_container').show();
    } else if (type_value == "LOGO_CONTAINER") {
        $('#fileupload').attr( "accept", ".swf" );
        $('#logo_list_table').show();
    } else if(type_value == "FILLER"){
        $('#fileupload').attr( "accept", ".mp4, .mov, .avi, .mpg" );
    } else if(type_value == "SLIDE"){
        $('#fileupload').attr( "accept", ".jpg, .png" );
    }

}


$(document).ready(function () {

    function progressHandlingFunction(evt) {
        if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            percentComplete = parseInt(percentComplete * 100);

            setProgress(percentComplete);
            console.log(percentComplete);
        }
    }

    function setProgress(value) {
        $('#progress .bar').css('width', value + '%');
        if (value === 100) {
            //alert("file successfully uploaded");
        }
    }

    var files = [];
    $("#fileupload").on('change', function (event) {
        setProgress(0);
        files = [];
        files = event.target.files;

        if (files.length > 0)
            console.log('event fired' + event.target.files[0].name);
    });

    $("#subbutton").click(function () {
        if (files.length == 0) {
            alert('Please Select a File ..');
        } else {
            showDialog();
            processFileUpload();
        }
    });

    function processFileUpload()
    {
        showDialog();
        console.log("file upload clicked");
        console.log(iAdvertID);
        var oUploadForm = new FormData();
        oUploadForm.append("file", files[0]);
        oUploadForm.append("advert_id", "" + iAdvertID);
        oUploadForm.append("advert_name", "" + $("#txt_advert_name_add_a").val());
        oUploadForm.append("advert_path", "" + $("#txt_advert_path_add_a").val());
        fileUploading();

        var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
        var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

        $.ajax({
            beforeSend: function (request) {
               request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandlingFunction, false);
                return xhr;
            },
            dataType: 'json',
            url: URI.advertisement.uploadFileUrl(),
            data: oUploadForm,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (data) {
                fileUploaded();
                closeDialog();
                $("tr:has(td)").remove();
                setPreviewVideo(data);
            },
            error: function (result) {
                closeDialog();
                alert('File Upload Error.');
            }
        });
    }

});

function setPreviewVideo(advert){
     var path = advert.advertpath;
     path = path.split(UTIL.common.getFileSeparator());
     var index = path.length - 1;
     var fileName = path[index];
     fileName = fileName.split('.');
     fileName[1] = fileName[1].toLowerCase();
     var fileExtension=fileName[1];

//     var mediaFileRootPath = UTIL.common.getFileSeparator() +
//                                  'media-files-' + UTIL.common.getApplicationVersion() + UTIL.common.getFileSeparator();
     var mediaFileRootPath = UTIL.common.getFileSeparator() +
                                  'media-files' + UTIL.common.getFileSeparator();
     if (fileExtension == 'mpg' && advert.fileAvailable)
         previewVideo(mediaFileRootPath + fileName[0] + '.mp4');
     else if (fileExtension == 'swf' && advert.fileAvailable)
         previewVideo(mediaFileRootPath + fileName[0] + '.swf');
     else if ((fileExtension == 'jpg' || fileExtension == 'png') && advert.fileAvailable)
         previewVideo(mediaFileRootPath + fileName[0] + '.' + fileName[1]);
     else
         $('#lblNullFile').show();
}

function previewVideo(path) {
    console.log(path);
    console.log((path.substr(path.lastIndexOf('.') + 1)).toLowerCase());

    $("#flash").hide();
    $("#video").hide();
    $("#image").hide();

    $("#image").attr('src', '');
    $("#video").attr('src', '');
    $("#flash").attr('data', '');

    var ext = (path.substr(path.lastIndexOf('.') + 1)).toLowerCase();
    if (ext == "mp4") {
        $("#video").show();
        $("#video").attr('src', path);
        $("#video").attr('poster', '');
    } else if (ext == "swf") {
        $("#flash").show();
        $("#flash").attr('data', path);
    } else {
        $("#image").show();
        $("#image").attr('src', path);
    }
}

function fileUploading() {
    $('#state').addClass('glyphicon-upload');
    $("#state").css("color", "#1E90FF");
}

function fileUploaded() {
    $('#state').addClass('glyphicon-ok').removeClass('glyphicon-upload');
    $("#state").css("color", "#5cb85c");
}

