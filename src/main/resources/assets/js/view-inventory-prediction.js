$(document).ready(function () {
    var selectedChannel = $('#selectedChannel').val();
    $('#channel').val(selectedChannel);
    $(function () {
        $( "#select-date").datetimepicker({
            pickTime: false
        });
    });

   $('#view-inventory').on('click', function(e){
       location.href = URI.inventory.getInventoryPredictionByChannelAndDate($('#channel').val(),$('#date').val());
  });
});

