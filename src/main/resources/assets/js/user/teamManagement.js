$(document).ready(function () {
    loadAllTeams();
});

function loadAllTeams(){
    showDialog();
    $.ajax({
        url:URI.teamManagement.loadAllTeamsUrl(),
        type: 'get',
        success: function (data){
            for (var i in data) {
                $("#all_teams_table").append('<tr class="tb_align_center"><td id="t1c1_3">' +
                    data[i].name + '</td><td><a href="./team-details?teamId=' + data[i].id + '"' +
                    ' id="' + data[i].id +'" class="btn btn-default sche_advert_delete">Details</a></td></tr>');
            }
            closeDialog();
        },
        error: function () {
            closeDialog();
        }
    });
}

function saveTeamDetail(){
    showDialog();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    var teamName = $("#txt_create_teams").val();
    if(teamName=="" || teamName==" "){
        closeDialog();
        viewMessageToastTyped("Please enter team name", "Info");
        return;
    }
    var postData = { 'name': teamName, 'members': [], 'admins':  $('#txt-me').val()};
    $.ajax({
        url:URI.teamManagement.saveTeamDetailUrl(),
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: JSON.stringify(postData),
        processData: false,
        type: 'post',
        success: function (data){
            closeDialog();
            viewMessageToastTyped("successfully", "Success");
            $("#all_teams_table td").parent().remove();
            loadAllTeams();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}

function checkTeamName(){
     var teamName = $("#txt_create_teams").val();
     if(teamName=="" || teamName==" "){
        viewMessageToastTyped("Please enter team name", "Info");
        return;
     }
    $.ajax({
        url:URI.teamDetails.checkTeamUrl(teamName),
        type: 'get',
        success: function (data){
            if(data){
                saveTeamDetail();
            }else{
                viewMessageToastTyped("Team name already exist.", "success");
            }
        },
        error: function () {
        }
    });
}


$( function() {
    showDialog();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    var userDropDown='';
    $.ajax({
        type: 'POST',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        dataType: "json",
        contentType: 'application/json',
        url: URI.teamManagement.getTeamlessUser(),
        success: function (data){
            for(var i in data){
                userDropDown += '<option id=' + data[i].userName + '" value="' +data[i].userName+ '">' + data[i].userName+ '</option>';
            }
             userDropDown += '</select>';
             $('#txt-me').html(userDropDown);
             $('#txt-me').selectpicker('refresh');
             closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    })
} );
