var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

$(document).ready(function () {
    var teamId = $("#teamId").val();
    var searchResultsTable;
    loadAllTeamMembersOfTeam();
    loadAllTeamAdminsOfTeam();
});

$( function() {
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    var allUsers = [];
    var userDropDown='';
    $.ajax({
        type: 'POST',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        dataType: "json",
        contentType: 'application/json',
        url: URI.teamManagement.getTeamlessUser(),
        success: function (data){
            for(var i in data){
                allUsers.push(data[i].userName);
                userDropDown += '<option id=' + data[i].userName + '" value="' +data[i].userName+ '">' + data[i].userName+ '</option>';
            }
             userDropDown += '</select>';
             $('#txt-me').html(userDropDown);
             $('#txt-admin').html(userDropDown);
             $('#txt-me').selectpicker('refresh');
             $('#txt-admin').selectpicker('refresh');
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
} );

function loadAllTeamMembersOfTeam(){
    $.ajax({
        url:URI.teamDetails.loadAllTeamMembersUrl(teamId.value),
        type: 'get',
        success: function (data){
            var rowCount = $('#team_details_table tr').length;
            for(var i=1; i<rowCount; i++){
                document.getElementById("team_details_table").deleteRow(1);
            }
            for (var i in data) {
                if(data[i] != null){
                    $("#team_details_table").append('<tr class="tb_align_center"><td id="t1c1_3">' + data[i] + '</td>' +
                                ' <td><button' + ' onclick=deleteMember(' + teamId.value  + ',' + "'"+ data[i] + "'" +')' +
                                ' type="submit" class="btn btn-default sche_advert_delete">Remove</button></td></tr>');
                }else{
                    $("#team_details_table").append('<tr class="tb_align_center"><td>No Members available</td><td>-</td></tr>')
                }
            }
        },
        error: function () {
        }
    });
}

function loadAllTeamAdminsOfTeam(){
    $.ajax({
        url:URI.teamDetails.loadTeamAdminsUrl(teamId.value),
        type: 'get',
        success: function (data){
            var rowCount = $('#team_admin_table tr').length;
            for(var i=1; i<rowCount; i++){
                document.getElementById("team_admin_table").deleteRow(1);
            }
            for (var i in data) {
                if(data[i] != null){
                    $("#team_admin_table").append('<tr class="tb_align_center"><td id="t1c1_3">' + data[i] + '</td>' +
                    ' <td><button' + ' onclick=deleteAdmin(' + teamId.value  + ',' + "'"+ data[i] + "'" +')' +
                    ' type="submit" class="btn btn-default sche_advert_delete">Remove</button></td></tr>');
                }else{
                    $("#team_admin_table").append('<tr class="tb_align_center"><td>No Admins available</td><td>-</td></tr>')
                }
            }
        },
        error: function () {
        }
    });
}

function deleteMember(teamId, username){
    $.ajax({
        url:URI.teamDetails.deleteMemberUrl(teamId, username),
        type: 'get',
        success: function (data){
            $("#team_details_table td").parent().remove();
            loadAllTeamMembersOfTeam();
            viewMessageToastTyped("Team member successfully removed", "success");
        },
        error: function () {
        }
    });
}

function deleteAdmin(teamId, username){
    $.ajax({
        url:URI.teamDetails.deleteAdminUrl(teamId, username),
        type: 'get',
        success: function (data){
            $("#team_admin_table td").parent().remove();
                    loadAllTeamAdminsOfTeam();
                    viewMessageToastTyped("Team admin successfully removed", "success");
        },
        error: function () {

        }
    });
}

function saveTeamDetail(){
    showDialog();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    var teamName = $("#txt_create_teams").val();
    if(teamName=="" || teamName==" "){
        closeDialog();
        viewMessageToastTyped("Please enter team name", "Info");
        return;
    }

    var postData = {'id':$('#teamId').val(), 'name': teamName, 'members': $('#txt-me').val(), 'admins':  $('#txt-admin').val()};
    $.ajax({
        url:URI.teamDetails.updateTeamDetailUrl(),
        dataType: "json",
        contentType: 'application/json',
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: JSON.stringify(postData),
        processData: false,
        type: 'post',
        success: function (data){
            closeDialog();
            viewMessageToastTyped("successfully", "Success");
            loadAllTeamMembersOfTeam();
            loadAllTeamAdminsOfTeam();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
        }
    });
}
