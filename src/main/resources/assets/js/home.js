$(document).ready(function () {
    var username = $("#current_user").val();
    checkUser(username);
});

function checkUser(userName) {
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url:  URI.userDetails.getUserCheckerUrl(userName),
        success: function (data)
        {
            if (data == 0) {
                localStorage.setItem("updateUser_id", userName);
                localStorage.setItem("pageKey", "FirstTime");
                window.location = URI.userDetails.changeProfilePageUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                console.log("Error textStatus :" + textStatus);
                console.log("Error errorThrown :" + errorThrown);
                //alert("Error textStatus :" + textStatus);
                //alert("Error errorThrown :" + errorThrown);
            }
        }
    });
}
