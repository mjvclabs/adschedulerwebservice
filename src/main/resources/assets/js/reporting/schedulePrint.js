$(document).ready(function () {
    loadWorkOrderTable();
});

function loadWorkOrderTable() {
    var searchResultsTable = $('#tb-all-schedule-report').DataTable({
        "bFilter": false,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": URI.workOrder.workOrderListForScheduleUrl(),
            "type": 'GET',
            "data": function (d) {
                var table = $('#tb-all-schedule-report').DataTable();
                d.page = (table != undefined) ? table.page.info().page : 0;
                d.size = (table != undefined) ? table.page.info().length : 5;
                d.woid = "";
                d.agent = "";
                d.client = "";
                d.product = "";
                d.scheduleRef = "";
                d.me = "";
                d.start = "";
                d.end = "";
                d.modifyDate = "";
                d.status = "";
            },
            dataFilter: function (data) {
                var json = jQuery.parseJSON(data);
                json.recordsTotal = json.totalElements;
                json.recordsFiltered = json.totalElements;
                json.data = json.content;
                return JSON.stringify(json);
            }
        },
        "columns": [
            {
                "width": "5%",
                "data": "workorderid",
                "orderable": false,
                "render": function (data, type, full) {
                    return  $('<a/>').attr('href', './schedule-view?work_id=' + data + '&work_name=' + full.ordername + '&start_date=' + full.startdate + '&end_date=' + full.enddate).append($('<span>' + data + '</span>')).wrap('<div/>').parent()
                            .html();
                }
            },
            {
                "width": "8%",
                "data": "ordername",
                "orderable": false
            },
            {
                "width": "8%",
                "data": "seller",
                "orderable": false
            },
            {
                "width": "12%",
                "data": "startdate",
                "orderable": false
            },
            {
                "width": "13%",
                "data": "enddate",
                "orderable": false
            },
            {
                "width": "10%",
                "data": "status",
                "orderable": false
            }
        ],
        "createdRow": function (row, data, index) {
            if (data.permissionstatus == 4) {
                //Suspend workOrder
                $('td', row).addClass('suspend-work-order');
            } else if (data.permissionstatus == 3) {
                //Closed workOrder
                $('td', row).addClass('closed-work-order');
            } else if (data.notMatchWithBudget) {
                $('td', row).addClass('budget-missed-match');
            }
        }

    });

    $("#btn-search").click(function () {
        searchResultsTable.ajax.reload();
    });
}

