var jsonData = {}; /// original ComAvailability data for filtering
var tableData = {};

$(document).ready(function () {
    loadClients();
});
function loadClients() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.allClientDetailsUrl(),
        success: function (data)
        {
            closeDialog();
            for (var i in data) {
                if (data[i].clienttype == "Agency") {
                    $('#agency-list').append('<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>');
                } else {
                    $('#client-list').append('<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>');
                }
            }
            workOrderDetailsCreator();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function workOrderDetailsCreator()
{
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderListJsonUrl(),
        success: function (data)
        {
            for (var i in data)
            {
                $('#wo-id-list').append('<option value="' + data[i].workorderid + '">' + data[i].workorderid + '_' + data[i].ordername + '</option>');
            }
            $('#wo-id-list').selectpicker('refresh');
            $('.selectpicker').selectpicker({
                size: 10
            });
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function clearFilter() {
    $("#wo-id-list").selectpicker('val', "-111");
    $("#agency-list").selectpicker('val', "-111");
    document.getElementById("client-list").value = "-111";
    $('#com-availability-value').selectpicker('val', "-1");
}

function getScheduleAnalysisData() {
    showDialog();
    var wo_ids = [];
    var workOrderdrop = $('#wo-id-list').find("option:selected");
    workOrderdrop.each(function () {
        wo_ids.push(stoi($(this).val()));
    });
//    if (wo_ids.length == 0) {
//        alert("Please select WorkOrder");
//        return;
//    }

    var agency_ids = [];
    var agencydrop = $('#agency-list').find("option:selected");
    console.log('Agency');
    agencydrop.each(function () {
        agency_ids.push(stoi($(this).val()));
    });

    var clientId = $('#client-list').val();

    var woId=470;
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

     $.ajax({
         beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
         },
        type: 'POST',
        dataType: "json",
        url: URI.report.getScheduleAnalysisUrl(),
        data: {
            workOrderID: woId,
            workOrderIDS: JSON.stringify(wo_ids),
            agencyIDS: JSON.stringify(agency_ids),
            clientId: clientId
        },
        success: function (data)
        {
            tableData = data;
            buildTable(tableData, 1);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function changeValue() {
    var value = $('#com-availability-value').val();
    buildTable(tableData, value);
}

function buildTable(tableData, value) {
    var tables = '';
    var dateRange = {};
    var timeRange = {};
    for (var tableKey in tableData) {
        jsonData = tableData[tableKey];
        dateRange = {};
        timeRange = {};
        console.log(tableKey);
        if (tableKey === "") {
            return;
        }

        tables += '<div class="row  p-t-10"> <table id="schedule-analysis-table-' + tableKey + '" class="table table-bordered table-workorder">';

        var tableHeadr = '<thead><tr><th>' + tableKey + '</th>';//<th>9:00</th><th>10:00</th><th>11:00</th><th>12:00</th><th>13:00</th><th>14:00</th><th>15:00</th> <th>16:00</th><th>17:00</th></tr></thead>
        var table = '<tbody>';
        var key;
        for (key in jsonData) {
            tableHeadr += '<th>' + key + '</th>';
            dateRange[key] = key;
        }
        tableHeadr += '</thead>';
        var channelData = jsonData[key];
        var channelDetails = channelData.hourDetailsMap;
        for (var infokey in channelDetails) {
            timeRange[infokey] = infokey;
        }

        for (var timeKey in timeRange) {
            console.log(timeKey);
            table += '<tr>';
            table += '<td>' + timeKey + ':00</td>';
            for (var dateKey in dateRange) {
                console.log(dateKey);
                channelData = jsonData[dateKey];
                channelDetails = channelData.hourDetailsMap;
                var channelInfo = channelDetails[timeKey];
                switch (value) {
                    case "1":
                        table += '<td>' + channelInfo.advertCount + '</td>';
                        break;

                    case "2":
                        table += '<td>' + channelInfo.advertDuration + '</td>';
                        break;

                    case "3":
                        table += '<td>' + channelInfo.inventryUtilization + '</td>';
                        break;

                    case "4":
                        table += '<td>' + channelInfo.actualUtilization + '</td>';
                        break;

                    default:
                        table += '<td>' + channelInfo.advertCount + '</td>';
                        break;
                }
            }
            table += '</tr>';
        }

        table += '</tbody>';

        tables += tableHeadr + table + '</table></div>';
    }
    $('#channels-table').html(tables);

    document.getElementById("com-availability-value").value = value;
}

function generateExcel() {
    showDialog();
    var wo_ids = [];
    var workOrderdrop = $('#wo-id-list').find("option:selected");
    workOrderdrop.each(function () {
        wo_ids.push(stoi($(this).val()));
    });

    var agency_ids = [];
    var agencydrop = $('#agency-list').find("option:selected");
    console.log('Agency');
    agencydrop.each(function () {
        agency_ids.push(stoi($(this).val()));
    });

    var clientId = $('#client-list').val();

    var value = $('#com-availability-value').val();
    if (value == -1) {
        closeDialog();
        alert("please selecte value");
        return;
    }

    var woId = 470;
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    $.ajax({
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        url: URI.report.writeScheduleAnalysisReportUrl(),
        data: {
            workOrderID: woId,
            workOrderIDS: JSON.stringify(wo_ids),
            agencyIDS: JSON.stringify(agency_ids),
            clientId: clientId,
            value: value
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                window.location = URI.report.downloadScheduleAnalysisReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}