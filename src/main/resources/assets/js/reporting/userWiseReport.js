$(document).ready(function () {
    channelDetailsDropdone();
});

$(function () {
    $('#div-end-date').datetimepicker({
        pickTime: false
    });
});
$(function () {
    $('#div-start-date').datetimepicker({
        pickTime: false
    });
});

function setTable(data) {
    var trHTML = '<thead><tr><th>Channel Name</th><th>Login Date</th><th>Login Time</th><th>Logout Date</th><th>Logout Time</th><th>Session Duration</th><th>User Name</th><th>IP Address</th></tr></thead>';
    trHTML += '<tbody>';

    for (var i in data)
    {
        trHTML += '<tr>';
        trHTML += '<td>' + data[i].channelName + '</td>';
        trHTML += '<td>' + data[i].loginDate + '</td>';
        trHTML += '<td>' + data[i].loginTime + '</td>';
        trHTML += '<td>' + data[i].logoutDate + '</td>';
        trHTML += '<td>' + data[i].logoutTime + '</td>';
        trHTML += '<td>' + data[i].sessionDuration + '</td>';
        trHTML += '<td>' + data[i].userName + '</td>';
        trHTML += '<td>' + data[i].ipAddress + '</td>';
        trHTML += '</tr>';
    }
    trHTML += '</tbody>';
    $('#user_wise_table').html(trHTML);
}

function search() {

    var start = $('#txt-start').val();
    if(start==""||start==" "){
        viewMessageToastTyped("Please enter start date", "Info");
        return;
    }
    var end = $('#txt-end').val();
    if(end==""||end==" "){
        viewMessageToastTyped("Please enter end date", "Info");
        return;
    }

    var user = '';
    var fiter_user = $('#txt-user').find("option:selected");
    fiter_user.each(function () {
        user += "=" + $(this).val();
    });

    var channelIds = '';
    var fiter_channel = $('#txt-channel').find("option:selected");
    fiter_channel.each(function () {
        channelIds += "=" + $(this).val();
    });

    var filterData = {startDate: start, endDate: end, channelId: channelIds, users: user};

     loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.getUserWiseReportUrl(),
        data: {
            filterData: JSON.stringify(filterData),
        },
        success: function (data)
        {
            setTable(data);
            loadingClose();
        },
        error: function (jqXHR, textStatus, thrownError) {
            loadingClose();
            alert("Error");

        }
    });

}

function channelDetailsDropdone() {
     loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            for (var i in data) {
                $('#txt-channel').append('<option value="' + data[i].channelid + '">' + data[i].channelname + '</option>');
            }
            loadingClose();
            $('#txt-channel').selectpicker('refresh');
            $('.selectpicker').selectpicker({
                size: 10
            });
            $("#txt-channel").selectpicker('val', "-111");
            loadUesrs();
        },
        error: function (jqXHR, textStatus, thrownError) {
            loadingClose();
            alert("Error");

        }
    });
}

function loadUesrs() {
    loadingOpen();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    $.ajax({
        type: 'POST',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.userDetails.allUserUrl(),
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        success: function (data)
        {
            for (var i in data) {
                $('#txt-user').append('<option value="' + data[i].userName + '">' + data[i].name + '</option>');
            }
            loadingClose();
            $('#txt-user').selectpicker('refresh');
            $('.selectpicker').selectpicker({
                size: 10
             });
             $("#txt-user").selectpicker('val', "All");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            alert("Error");

        }
    });
}

function generateExcel() {

    var start = $('#txt-start').val();
    if(start==""||start==" "){
        viewMessageToastTyped("Please enter start date", "Info");
        return;
    }
    var end = $('#txt-end').val();
    if(end==""||end==" "){
        viewMessageToastTyped("Please enter end date", "Info");
        return;
    }

    var user = '';
    var fiter_user = $('#txt-user').find("option:selected");
    fiter_user.each(function () {
        user += "=" + $(this).val();
    });

    var channelIds = '';
    var fiter_channel = $('#txt-channel').find("option:selected");
    fiter_channel.each(function () {
        channelIds += "=" + $(this).val();
    });

    var filterData = {startDate: start, endDate: end, channelId: channelIds, users: user};

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    loadingOpen();
    $.ajax({
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        url: URI.report.writeUserWiseReportUrl(),
        data: {
            filterData: JSON.stringify(filterData),
        },
        success: function (data)
        {
            loadingClose();
            if (data) {
                window.location = URI.report.downloadUserWiseReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function clearFilter() {
    document.getElementById("txt-start").value = "";
    document.getElementById("txt-end").value = "";
    $("#txt-user").selectpicker('val', "");
    document.getElementById("txt-channel").value = "-111";;
}