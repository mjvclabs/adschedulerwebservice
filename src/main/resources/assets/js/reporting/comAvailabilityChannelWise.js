var jsonData = {}; /// original ComAvailability data for filtering
var dateRange = {};
var timeRange = {};

$(function () {
    $('#startDate-selecter').datetimepicker({
        pickTime: false
    });
});

$(function () {
    $('#endDate-selecter').datetimepicker({
        pickTime: false
    });
});

$(document).ready(function () {
    channelDetailsDropdone();
});

function channelDetailsDropdone() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += ' <option VALUE="-111" >Select Channel</option>';
            for (var i in data)
            {
                trHTML += '<option VALUE="' + data[i].channelid + '">' + data[i].channelname + '</option>';
            }
            trHTML += '</select>';
            $('#channel-list').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, thrownError) {
            closeDialog();
            alert("Error");

        }
    });
}

function getComAvailabilityChannelWiseData() {
    showDialog();
    var startDate = $('#start-date').val();//"2017-12-08";
    var endDate = $('#end-date').val();//"2017-12-08";
    var channelId = $('#channel-list').val();

    var dtRegex = new RegExp(/\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])/);
    if (!dtRegex.test(startDate)) {
        closeDialog();
        alert("Please select date");
        return;
    }
    if (!dtRegex.test(endDate)) {
        closeDialog();
        alert("Please select date");
        return;
    }
    if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
        closeDialog();
        alert("Invalid date range");
        return false;
    }

    if (channelId == -111) {
        closeDialog();
        alert("Please select channel");
        return;
    }
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.getComAvailabilityChannelWiseUrl(),
        data: {
            startDate: startDate,
            endDate: endDate,
            channelID: channelId
        },
        success: function (data)
        {
            jsonData = {}; /// original ComAvailability data for filtering
            dateRange = {};
            timeRange = {};
            if(Object.keys(data).length!=0){
                jsonData = data;
                buildTable(jsonData, 1);
            }else{
                viewMessageToastTyped("No data.", "Warning");
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function changeValue() {
    var value = $('#com-availability-value').val();
    if(Object.keys(jsonData).length!=0){
        buildTable(jsonData, value);
    }
}

function buildTable(jsonData, value) {
    var tableHeadr = '<thead><tr><th>Time</th>';//<th>9:00</th><th>10:00</th><th>11:00</th><th>12:00</th><th>13:00</th><th>14:00</th><th>15:00</th> <th>16:00</th><th>17:00</th></tr></thead>
    var table = '<tbody>';
    var key;
    for (key in jsonData) {
        tableHeadr += '<th>' + key + '</th>';
        dateRange[key] = key;
    }
    tableHeadr += '</thead>';
    var channelData = jsonData[key];
    var channelDetails = channelData.hourDetailsMap;
    for (var infokey in channelDetails) {
        timeRange[infokey] = infokey;
    }

    for (var timeKey in timeRange) {
        table += '<tr>';
        table += '<td>' + timeKey + ':00</td>';
        for (var dateKey in dateRange) {
            channelData = jsonData[dateKey];
            channelDetails = channelData.hourDetailsMap;
            var channelInfo = channelDetails[timeKey];
            switch (value) {
                case "1":
                    table += '<td>' + channelInfo.advertCount + '</td>';
                    break;

                case "2":
                    table += '<td>' + channelInfo.advertDuration + '</td>';
                    break;

                case "3":
                    table += '<td>' + channelInfo.inventryUtilization + '</td>';
                    break;

                case "4":
                    table += '<td>' + channelInfo.actualUtilization + '</td>';
                    break;

                default:
                    table += '<td>' + channelInfo.advertCount + '</td>';
                    break;
            }
        }
        table += '</tr>';
    }

    table += '</tbody>';
    document.getElementById("com-availability-value").value = value;
    $('#com-availability-channelWise-table').html(tableHeadr + table);
}

function generateExcel() {
    showDialog();
    var value = $('#com-availability-value').val();
    var startDate = $('#start-date').val();//"2017-12-08";
    var endDate = $('#end-date').val();//"2017-12-08";
    var channelId = $('#channel-list').val();

    var dtRegex = new RegExp(/\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])/);
    if (!dtRegex.test(startDate)) {
        closeDialog();
        alert("Please select date");
        return;
    }
    if (!dtRegex.test(endDate)) {
        closeDialog();
        alert("Please select date");
        return;
    }
    if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
        closeDialog();
        alert("Invalid date range");
        return false;
    }

    if (channelId == -111) {
        closeDialog();
        alert("Please select channel");
        return;
    }
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.writeComAvailabilityChannelWiseReportUrl(),
        data: {
            startDate: startDate,
            endDate: endDate,
            channelId: channelId,
            value: value
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                window.location = URI.report.downloadComAvailabilityChannelWiseReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}