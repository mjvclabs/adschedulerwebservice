$(document).ready(function () {
    loadClients();
    //getClientRevenueData();
});
function loadClients() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.workOrder.allClientDetailsUrl(),
        success: function (data)
        {
            for (var i in data) {
                if (data[i].clienttype == "Agency") {
                    // $('#agency-list').append('<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>');
                } else {
                    $('#client-list').append('<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>');
                }
            }
            $("#client-list").selectpicker("refresh");
            closeDialog();
            $('.selectpicker').selectpicker({
                size: 10
            });
            loadYear();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function loadYear() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.report.getYearsValueUrl(),
        success: function (data)
        {
            for (var key in data) {
                $('#year-list').append('<option value="' + key + '">' + data[key] + '</option>');
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");

        }
    });
}

function clearFilter() {
    $("#client-list").selectpicker('val', "-111");
    document.getElementById("year-list").value = "-111";
    document.getElementById("month-list").value = "-111";
}

function getClientRevenueData() {
    showDialog();
    var client_ids = [];
    var clientdrop = $('#client-list').find("option:selected");
    //console.log('Agency');
    clientdrop.each(function () {
        client_ids.push(stoi($(this).val()));
    });
    var year = $('#year-list').val();
    if (year == -111) {
        alert("Pleas select year");
        closeDialog();
        return;
    }

    var month = $('#month-list').val();

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.getClientRevenueUrl(),
        data: {
            clientIDS: JSON.stringify(client_ids),
            year: year,
            month: month,
        },
        success: function (data)
        {
            buildTable(data);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function buildTable(data) {
    showDialog();
    var table = '';
    var tableHeader = '<thead><tr><th>Client</th>';
    var monthData = {};
    for (var key in data) {
        table += '<tr>';
        table += '<td>' + data[key].clientOrAgencyName + '</td>';
        monthData = data[key].clientAgencyList;
        for (var monthkey in monthData) {
            console.log(monthkey);
            table += '<td>' + getDecimal(monthData[monthkey].value) + '</td>';
        }
        table += '<td>' + getDecimal(data[key].grandTotal) + '</td>';
        table += '</tr>';
    }
    for (var monthkey in monthData) {
        console.log(monthkey);
        tableHeader += '<th>' + monthData[monthkey].monthName + '</th>';
    }
    tableHeader += '<th>Grand Total</th></thead></tr>';
    $('#client-revenue-table').html(tableHeader + table);
    closeDialog();
}

function generateExcel() {
    showDialog();
    var client_ids = [];
    var clientdrop = $('#client-list').find("option:selected");
    //console.log('Agency');
    clientdrop.each(function () {
        client_ids.push(stoi($(this).val()));
    });
    var year = $('#year-list').val();
    if (year == -111) {
        alert("Pleas select year");
        closeDialog();
        return;
    }

    var month = $('#month-list').val();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: URI.report.writeClientRevenueReportUrl(),
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        data: {
            clientIDS: JSON.stringify(client_ids),
            year: year,
            month: month
        },
        success: function (data)
        {
            if (data) {
                window.location = URI.report.downloadClientRevenueReport();
            }
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}
