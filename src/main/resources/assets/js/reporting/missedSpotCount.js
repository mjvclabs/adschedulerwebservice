$(document).ready(function () {
    setMissedSpotCountTable();
    workOrderDetailsCreator();
});

$(function () {
    $('#div-end-date').datetimepicker({
        pickTime: false
    });
});
$(function () {
    $('#div-start-date').datetimepicker({
        pickTime: false
    });
});

function setMissedSpotCountTable() {
     loadingOpen();

    var filterData = {startDate: addDays(new Date(),0), endDate: addDays(new Date(),0), woIds: "", channelIds: "", advertisement: "", advertType: "", spotStatus: "", advertTimeBelt: ""};

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.missedUrl(),
        data: {
            filterData: JSON.stringify(filterData),
        },
        success: function (data)
        {
            setTable(data);
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function setTable(data) {
    var trHTML = '<thead><tr><th>Time Belt</th><th>Channel Name</th><th>Date</th><th>Scheduled Time</th><th>Shifted From</th><th>Aired Time</th><th>Advert Name</th><th>Duration</th><th>Cluster</th><th>Type</th><th>Client</th><th>ID</th><th>Status</th><th>Comment</th><th>Played User</th></tr></thead>';
    trHTML += '<tbody>';

    for (var i in data)
    {
        trHTML += '<tr>';
        trHTML += '<td>' + data[i].scheduleTimeBelt + '</td>';
        trHTML += '<td>' + data[i].channelName + '</td>';
        trHTML += '<td>' + data[i].schedulDate + '</td>';
        trHTML += '<td>' + RemoveSec(data[i].schedulStartTime) + " - " + RemoveSec(data[i].schedulEndTime) + '</td>';
        trHTML += '<td>' + data[i].shiftedTimeBelt + '</td>';
        trHTML += '<td>' + data[i].actualTimeBelt + '</td>';
        trHTML += '<td>' + data[i].advertNme + '</td>';
        trHTML += '<td>' + data[i].duratuion + '</td>';
        trHTML += '<td>' + data[i].cluster + '</td>';
        trHTML += '<td>' + getAdvertType(data[i].advertType) + '</td>';
        trHTML += '<td>' + data[i].client + '</td>';
        trHTML += '<td>' + data[i].advertId + '</td>';
        trHTML += '<td>' + data[i].status + '</td>';
        trHTML += '<td></td>';
        trHTML += '<td></td>';

        trHTML += '</tr>';
    }
    trHTML += '</tbody>';
    // {"scheduleId":23198,"actualTimeBelt":"17:00:00-18:00:00","schedulDate":"2017-08-22","status":"Missed","schedulStartTime":"14:30:00","schedulEndTime":"17:30:00","workOrderId":199,"workOrderName":"Dialog","workOrderEndDate":null,"channelId":1,"channelName":"Test_Channel_1","advertId":4,"advertNme":"Dialog TV 30s Eng","advertType":"ADVERT","duratuion":30,"client":"Dialog Axiata PLC","clientId":4,"priority":""}

    $('#media_delete_table').html(trHTML);
}

function search() {

    var start = $('#txt-start').val();
    if(start==""||start==" "){
        viewMessageToastTyped("Please enter start date", "Info");
        return;
    }
    var end = $('#txt-end').val();
    if(end==""||end==" "){
        viewMessageToastTyped("Please enter end date", "Info");
        return;
    }

    var wok_ids = '';
    var fiter_wok = $('#txt-wk-order').find("option:selected");
    fiter_wok.each(function () {
        wok_ids += "_" + $(this).val();
    });

    var channel = '';
    var fiter_channel = $('#txt-channel').find("option:selected");
    fiter_channel.each(function () {
        channel += "_" + $(this).val();
    });

    var advertWithID = $('#txt-advert').val().split("_");
    var advert="";
    if(advertWithID. length>1){
        advert=advertWithID[1];
    }else{
        advert=$('#txt-advert').val();
    }
    var type = $('#txt-type').val();
    var status = $('#txt-status').val();
    var timeBelt = $('#txt-time-belt').val();

    var filterData = {startDate: start, endDate: end, woIds: wok_ids, channelIds: channel, advertisement: advert, advertType: type, spotStatus: status, advertTimeBelt: timeBelt};

     loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.missedUrl(),
        data: {
            filterData: JSON.stringify(filterData),
        },
        success: function (data)
        {
            setTable(data);
            loadingClose();
        },
        error: function (jqXHR, textStatus, thrownError) {
            loadingClose();
            alert("Error");

        }
    });

}

function channelDetailsDropdone() {
     loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.channeldetail.listOrderByNameUrl(),
        success: function (data)
        {
            for (var i in data) {
                $('#txt-channel').append('<option value="' + data[i].channelid + '">' + data[i].channelname + '</option>');
            }
            $('#txt-channel').selectpicker('refresh');
            $('.selectpicker').selectpicker({
                size: 10
            });
            loadingClose();
        },
        error: function (jqXHR, textStatus, thrownError) {
            loadingClose();
            alert("Error");

        }
    });
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);

    var yyyy = result.getFullYear();
    var mm = result.getMonth() + 1;
    var dd = result.getDate();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    date = yyyy + '/' + mm + '/' + dd;

    return yyyy + '-' + mm + '-' + dd;
}


function workOrderDetailsCreator()
{
     loadingOpen();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.workOrder.workOrderListJsonUrl(),
        success: function (data)
        {
            for (var i in data)
            {
                $('#txt-wk-order').append('<option value="' + data[i].workorderid + '">' + data[i].workorderid + '_' + data[i].ordername + '</option>');
            }
            $('#txt-wk-order').selectpicker('refresh');
            $('.selectpicker').selectpicker({
                size: 10
            });
            loadingClose();
            channelDetailsDropdone();
        },
        error: function (jqXHR, textStatus, errorThrown) {
           loadingClose();
            alert("Error");

        }
    });
}


function generateExcel() {

    var start = $('#txt-start').val();
    if(start==""||start==" "){
        viewMessageToastTyped("Please enter start date", "Info");
        return;
    }
    var end = $('#txt-end').val();
    if(end==""||end==" "){
        viewMessageToastTyped("Please enter end date", "Info");
        return;
    }
    var wok_ids = '';
    var fiter_wok = $('#txt-wk-order').find("option:selected");
    fiter_wok.each(function () {
        wok_ids += "_" + $(this).val();
    });

    var channel = '';
    var fiter_channel = $('#txt-channel').find("option:selected");
    fiter_channel.each(function () {
        channel += "_" + $(this).val();
    });

    var advertWithID = $('#txt-advert').val().split("_");
    var advert="";
    if(advertWithID. length>1){
        advert=advertWithID[1];
    }else{
        advert=$('#txt-advert').val();
    }
    var type = $('#txt-type').val();
    var status = $('#txt-status').val();
    var timeBelt = $('#txt-time-belt').val();

    var filterData = {startDate: start, endDate: end, woIds: wok_ids, channelIds: channel, advertisement: advert, advertType: type, spotStatus: status, advertTimeBelt: timeBelt};

    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");

    loadingOpen();
    $.ajax({
        beforeSend: function (request) {
            request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        type: 'POST',
        dataType: "json",
        url: URI.report.writeMissedSpotCountReportUrl(),
        data: {
            filterData: JSON.stringify(filterData),
        },
        success: function (data)
        {
            loadingClose();
            if (data) {
                window.location = URI.report.downloadMissedSpotCountReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            loadingClose();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

$( function() {
    loadingOpen();
    var allAdvert = [];
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementAllListOrderByNameUrl(),
        success: function (data){
            for(var i in data){
                allAdvert.push(data[i].advertid+"_"+data[i].advertname);
            }
            loadingClose();
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
    $( "#txt-advert" ).autocomplete({
        source: allAdvert
    });
}
);

function clearFilter() {
    document.getElementById("txt-start").value = "";
    document.getElementById("txt-end").value = "";
     $("#txt-wk-order").selectpicker('val', "");
     $("#txt-channel").selectpicker('val', "");
    document.getElementById("txt-advert").value = "";
     document.getElementById("txt-type").value = "-111";
     document.getElementById("txt-status").value = "-1";
     document.getElementById("txt-time-belt").value = "all";

}