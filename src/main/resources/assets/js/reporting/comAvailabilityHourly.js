var jsonData = {}; /// original ComAvailability data for filtering

$(function () {
    $('#date-selecter').datetimepicker({
        pickTime: false
    });
});

function getComAvailabilityData() {
    showDialog();
    var selectedDate = $('#setected-date').val();//"2017-12-08";
    var dtRegex = new RegExp(/\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])/);
    if (!dtRegex.test(selectedDate)) {
        closeDialog();
        alert("Please select date");
        return;
    }
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.getComAvailabilityHourlyUrl(),
        data: {
            date: selectedDate
        },
        success: function (data)
        {
            jsonData = data;
            buildTable(jsonData, 1);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function changeValue() {
    var value = $('#com-availability-value').val();
    buildTable(jsonData, value);
}

function buildTable(jsonData, value) {
    var tableHeadr = '<thead><tr><th>Channel</th>';//<th>9:00</th><th>10:00</th><th>11:00</th><th>12:00</th><th>13:00</th><th>14:00</th><th>15:00</th> <th>16:00</th><th>17:00</th></tr></thead>
    var setHeadr = true;
    var table = '<tbody>';
    for (var key in jsonData) {
        var channelData = jsonData[key];
        var channelDetails = channelData.hourDetailsMap;
        table += '<tr>';
        table += '<td>' + channelData.channelName + '</td>';
        for (var infokey in channelDetails) {
            var channelInfo = channelDetails[infokey];
            if (setHeadr) {
                tableHeadr += '<th>' + channelInfo.time + '</th>';
            }
            switch (value) {
                case "1":
                    table += '<td>' + channelInfo.advertCount + '</td>';
                    break;

                case "2":
                    table += '<td>' + channelInfo.advertDuration + '</td>';
                    break;

                case "3":
                    table += '<td>' + channelInfo.inventryUtilization + '</td>';
                    break;

                case "4":
                    table += '<td>' + channelInfo.actualUtilization + '</td>';
                    break;

                default:
                    table += '<td>' + channelInfo.advertCount + '</td>';
                    break;
            }
        }
        table += '</tr>';
        setHeadr = false;
    }
    tableHeadr += '</thead>';
    table += '</tbody>';
    document.getElementById("com-availability-value").value = value;
    $('#com-availability-table').html(tableHeadr + table);
}

function generateExcel() {
    showDialog();
    var selectedDate = $('#setected-date').val();//"2017-12-08";
    var value = $('#com-availability-value').val();
    var dtRegex = new RegExp(/\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])/);
    if (!dtRegex.test(selectedDate)) {
        closeDialog();
        alert("Please select date");
        return;
    }
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.writeComAvailabilityHourlyReportUrl(),
        data: {
            date: selectedDate,
            value: value
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                window.location = URI.report.downloadComAvailabilityHourlyReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}