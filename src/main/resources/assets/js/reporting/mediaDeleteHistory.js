$(document).ready(function () {
    setMediaDeleteTable();
});

function setMediaDeleteTable() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.report.getMediaDeleteHistoryUrl(),
        success: function (data)
        {
            var trHTML = '<thead><tr><th>ID</th><th>Advert Name</th><th>Deleted Date</th><th>Deleted User</th></tr></thead>';
            trHTML += '<tbody>';

            for (var i in data)
            {
                trHTML += '<tr>';
                trHTML += '<td>' + data[i].advertID + '</td>';
                trHTML += '<td>' + data[i].advertName + '</td>';
                trHTML += '<td>' + data[i].deleteDate + '</td>';
                trHTML += '<td>' + data[i].deleteUser + '</td>';
                trHTML += '</tr>';
            }
            trHTML += '</tbody>';

            $('#media_delete_table').html(trHTML);
            closeDialog();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function generateExcel() {
    showDialog();
    var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
    var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: URI.report.writeMediaDeleteHistoryReportUrl(),
        beforeSend: function (request) {
           request.setRequestHeader(csrf_headerName, csrf_paramValue);
        },
        success: function (data)
        {
            closeDialog();
            if (data) {
                window.location = URI.report.downloadMediaDeletionHistoryReportUrl();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("../Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

