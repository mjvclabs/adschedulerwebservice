var iAdvertID = -1;
var advertStatus = 0;

$(document).ready(function () {
    $('#lblNullFile').hide();
    $('#txt_advert_path_add_a').val("\\\\\\\\172.22.240.36\\\\f$\\\\TVCs");
    iAdvertID = stoi(getParameterByNamefromUrl('advertID'));

    loadClients();
//    .then(function(){
//        $('#suspend-date').datetimepicker().on('changeDate', function(e) {
//            var selectedDate = moment(e.date).format("YYYY-MM-DD");
//            var expDate = document.getElementById("txt_expireDate").value;
//            if(new moment().format("YYYY-MM-DD") <= selectedDate && expDate >= selectedDate){
//                var resumeBtn = '<button type="button" onclick="resumeAdvertisement()" class="btn btn-block btn_yellocolor">Resume</button>';
//                $('#suspend_div').html(resumeBtn);
//            }
//            else{
//                var suspendBtn = '<button type="button" onclick="suspendAdvertisement()" class="btn btn-block btn_readcolor">Suspend</button>';
//                $('#suspend_div').html(suspendBtn);
//            }
//        });
//    });
});

$(function () {
    $('#form-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});

$(function () {
    $('#suspend-date').datetimepicker({
        pickTime: false,
        startDate: new Date()
    });
});

function loadClients() {
//    return new Promise(function(resolve, reject) {
        $.ajax({
            type: 'GET',
            dataType: "json",
            contentType: 'application/json',
            url: URI.client.getAllClientOrderByName(),
            success: function (data)
            {
                var trHTML = '';
                trHTML += ' <select id="cmbClient" name="" class="form-control" onChange="loadProducts()">';
                trHTML += '<option value="-1">Select Client</option>';
                for (var i in data)
                {
                    if (data[i].clienttype == 'Client') {
                        trHTML += '<option value="' + data[i].clientid + '">' + data[i].clientname + '</option>';
                    }
                }
                trHTML += '</select>';
                trHTML += '<span class="help-block"></span>';

                $('#client_select').html(trHTML);
                loadLogoContainers();
//                resolve();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                closeDialog();
                if (errorThrown == 'Unauthorized') {
                    localStorage.clear();
                    window.location.replace("Login.html");
                }
                else {
                    alert("Error");
                }
//                reject();
            }
        })
//    });
}

function loadLogoContainers() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.logoContainer.getallUrl(),
        success: function (data)
        {
            var trHTML = '';
            trHTML += '<select id="cmbLogoContainer" name="" class="form-control">';
            trHTML += '<option value="-1">Select Container</option>';
            for (var i in data)
            {
                trHTML += '<option value="' + data[i].containerId + '">' + data[i].containerName + '</option>';
            }
            trHTML += '</select>';
            trHTML += '<span class="help-block"></span>';

            $('#logo_select').html(trHTML);
            getCommericalCategory()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function getCommericalCategory() {
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementCategoryAllListUrl(),
        success: function (data)
        {
            closeDialog();

            var dropDown = '<div class="dropdown"><select id="commerical_categoryDropdown" name="clientName" class="form-control" required>';
            dropDown += '<option value="-1"></option><option >None</option>';

            for (var i in data)
            {
                dropDown += '<option >' + data[i].category + '</option>';
            }
            dropDown += '</select></div><span class="help-block"></span>';
            $('#dro_commerical_category_add_a').html(dropDown);
            loadAdvertDetails()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

var codeMapId = -1;
function loadAdvertDetails() {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.getbyidUrl(),
        data: {
            id: iAdvertID
        },
        success: function (data)
        {
            closeDialog();
            if(data.codeMapping != null){
                codeMapId = data.codeMapping.id;
            }
            document.getElementById("txt_advert_id_add_a").value = data.advertid;
            document.getElementById("cmbClient").value = data.client.clientid;
            if(!data.canClientChange){
                $("#cmbClient").prop("disabled", true);
            }
            document.getElementById("txt_advert_name_add_a").value = data.advertname;
            advertTypeDropDownChange(data.adverttype);
            var path = data.advertpath;
            path = path.replace(/\\/g, '\\\\');
            document.getElementById("txt_advert_path_add_a").value = path;
            if (data.adverttype != -1) {
                document.getElementById("advertisement_type_Dropdown").value = data.adverttype;
            } else {
                document.getElementById("advertisement_type_Dropdown").value = -1;
            }

            if (path == "LOGO_CONTAINER") {
                document.getElementById("txt_advert_path_add_a").value = path;
                document.getElementById("advertisement_type_Dropdown").value = data.adverttype;
            }
            if (data.commercialcategory != -1) {
                document.getElementById("commerical_categoryDropdown").value = data.commercialcategory;
            } else {
                document.getElementById("commerical_categoryDropdown").value = -1;
            }

            if (data.language != -1) {
                var lang = setLanguage(data.language);
                document.getElementById("cmbLanguage").value = lang;
            } else {
                document.getElementById("cmbLanguage").value = -1;
            }
            if (data.vedioType != "") {
                var lang = setLanguage(data.vedioType);
                document.getElementById("cmbVideoType").value = lang;
            } else {
                document.getElementById("cmbVideoType").value = -1;
            }
            document.getElementById("txt_duration_add_a").value = data.duration;
//            document.getElementById("txt_expireDate").value = data.expireDate;
//            document.getElementById("txt_suspendDate").value = data.suspendDate;
            $('#form-date').datetimepicker("setDate", data.expireDate).change();
            $('#suspend-date').datetimepicker("setDate", data.suspendDate).change();
            document.getElementById("txt_width").value = data.width;
            document.getElementById("txt_hight").value = data.hight;
            document.getElementById("txt_x_position").value = data.xposition;
            document.getElementById("txt_y_position").value = data.yposition;
            document.getElementById("cmbLogoContainer").value = data.logoContainerId;
            advertStatus = data.status;
            if (data.status == 2 && data.expireDate >= data.suspendDate && new moment().format("YYYY-MM-DD") <= data.suspendDate) {
                var resumeBtn = '<button type="button" onclick="resumeAdvertisement()" class="btn btn-block btn_yellocolor">Resume</button>';
                $('#suspend_div').html(resumeBtn);
            }
            else if(data.status == 2){
                document.getElementById("suspend_button").style.display = "none"
            }
            else if(new moment().format("YYYY-MM-DD") > data.expireDate){
                $('#suspend_button').prop('disabled', true);
            }

            if (path == "LOGO_CONTAINER") {
                document.getElementById("txt_advert_path_add_a").value = '';
                document.getElementById("advertisement_type_Dropdown").value = "LOGO_CONTAINER";
                $('#logo_list_table').show();
                loadMultipleLogoData(data);
                $('#lblNullFile').show();
                return;
            }
            if(data.fileAvailable){
                setPreviewVideo(data);
            }
            loadProducts();
        }
        ,
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function loadMultipleLogoData(advert) {
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.logoContainer.getLogoListbyIdUrl(),
        data: {
            id: iAdvertID
        },
        success: function (data)
        {
            var advetTable = '<table id="advertisement_table_pop" class="table table-bordered table-workorder">';
            advetTable += '<thead class="tb_align_center"><tr><th>ID</th><th>Name</th><th>Duration(Sec)</th><th>Type</th><th></th></tr></thead><tbody>';
            stAdvertListTbl = '';

            $.each(data, function (i, item) {
                advetTable += '<tr id="' + i + 'row_pop" class="tb_align_center"><td>' + item.advertid + '</td><td>' + item.advertname + '</td><td>' + item.duration + '</td><td>' + getAdvertType(item.type) + '</td><td><button type="submit" id="' + i + '" onclick="deleteAdvert((this.id))" class="btn btn-default sche_advert_delete">Delete</button></td></tr>';
                stAdvertListTbl += '<tr><td>' + item.advertid + '</td><td>' + item.advertname + '</td><td>' + item.duration + '</td></tr>';
            });
            advetTable += '</tbody></table>';
            $('#advertisement_table_pop_div').html(advetTable);
            $('#advert_main_table tbody').html(stAdvertListTbl);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}

function updateAdvertAfterValidation(form){
    if(validateInputFields(form)){
        updateAdvert();
    }
}

function updateAdvert() {

    showDialog();
    var advertId = $("#txt_advert_id_add_a").val();
    var clientName = $("#cmbClient").val();
    if (clientName == -1) {
        closeDialog();
        viewMessageToastTyped("Please select client", "Warning");
        return;
    }

    var advertName = $("#txt_advert_name_add_a").val();
    var advertPath = $("#txt_advert_path_add_a").val();

    //var rg1 = /^[^\\/:\*\?"<>\|\./]+$/; // forbidden characters \ / : * ? " < > | .

    var rg1 = /^[a-zA-Z0-9_@()-\s\\&-]+$/;

    if (!rg1.test(advertName)) {
        closeDialog();
        viewMessageToastTyped("Invalid file name.", "Warning");
        return;
    }

    var advertType = $("#advertisement_type_Dropdown").val();
    if (advertType == -1) {
        viewMessageToastTyped("Please select Advertisement type", "Warning");
        return;
    }
    var advertCategory = $("#commerical_categoryDropdown").val();
    if (advertCategory == -1) {
        closeDialog();
        viewMessageToastTyped("Please select Commerical category", "Warning");
        return;
    }
    var duration = stoi($("#txt_duration_add_a").val());
    if(duration <= 0) {
        closeDialog();
        viewMessageToastTyped("Please enter not zero value for the duration.", "Warning");
        return;
    }
    var expireDate = $("#txt_expireDate").val();
    var suspendDate = $("#txt_suspendDate").val();
    var language = $("#cmbLanguage").val();
    if (language == -1) {
        closeDialog();
        viewMessageToastTyped("Please select Advertisement language", "Warning");
        return;
    }
    var vedioType = $("#cmbVideoType").val();
    if (vedioType == -1) {
        viewMessageToastTyped("Please select Advertisement Video Type", "Warning");
        return;
    }
    var with_p = $("#txt_width").val();
    var height_p = $("#txt_hight").val();
    var x_position = $("#txt_x_position").val();
    var y_position = $("#txt_y_position").val();
    var logo_cont = $("#cmbLogoContainer").val();
    var ad_path = advertPath + '\\\\' + advertName + '.mpg';

    var codeMapping = $('#cmbProduct').val();

    var dataArray = '{"advertId":"' + advertId + '","client":"' + clientName + '","advertName":"' + advertName + '","advertPath":"' + ad_path + '","advertType":"' + advertType + '","advertCategory":"' + advertCategory + '","duration":' +stoi(duration)  + ',"language":"' + language + '","with":' + with_p + ',"hight":' + height_p + ',"x_position":' + x_position + ',"y_position":' + y_position + ',"logo_container":' + logo_cont + ',"expireDate":"' + expireDate + '","suspendDate":"' + suspendDate + '","vedioType":"' + vedioType + '","codeMapping":"'+ codeMapping + '"}';
    console.log(dataArray);

    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementMetaDataUpdateUrl(),
        data: {
            advertData: dataArray,
        },
        success: function (data)
        {
            closeDialog();
            if (advertType == "LOGO_CONTAINER")
                saveMultipleLogos();
            alert("successfully updated");

            var susDate = document.getElementById("txt_suspendDate").value;
            var expDate = document.getElementById("txt_expireDate").value;
            if((new moment().format("YYYY-MM-DD") <= susDate) && (expDate >= susDate) && (advertStatus == 2)){
                var resumeBtn = '<button type="button" onclick="resumeAdvertisement()" id="suspend_button" class="btn btn-block btn_yellocolor">Resume</button>';
                $('#suspend_div').html(resumeBtn);
            } else if(new moment().format("YYYY-MM-DD") > susDate){
                document.getElementById("suspend_button").style.display = "none"
            } else{
                var resumeBtn = '<button type="button" onclick="suspendAdvertisement()" id="suspend_button" class="btn btn-block btn_readcolor_advert_suspend">Suspend</button>';
                $('#suspend_div').html(resumeBtn);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");
        }
    })
}

function toggleProperties() {
    $header = $('#expand_header');
    //getting the next element
    $content = $header.next();
    $content.slideToggle(500, function () {
        $header.html(function () {
            //change text based on condition
            return $content.is(":visible") ? '<span><label class="control-label col-md-12 lable_font">Advance Properties &#x25B2;</label></span>' :
                    '<span><label class="control-label col-md-12 lable_font">Advance Properties &#x25BC;</label></span>';
        });
    });
}

function advertTypeDropDownChange(type_value) {
    $('#logo_container').hide();
    $('#logo_list_table').hide();
    if (type_value == "ADVERT") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("720");
        $('#txt_hight').val("576");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("0");
        $('#fileupload').attr( "accept", ".mp4, .mov, .avi, .mpg" );
    } else if (type_value == "CRAWLER") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("720");
        $('#txt_hight').val("576");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("0");
        $('#fileupload').attr( "accept", ".swf" );
    } else if (type_value == "V_SHAPE") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("720");
        $('#txt_hight').val("100");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("476");
        $('#fileupload').attr( "accept", ".swf" );
    } else if (type_value == "L_SHAPE") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("600");
        $('#txt_hight').val("476");
        $('#txt_x_position').val("0");
        $('#txt_y_position').val("0");
        $('#fileupload').attr( "accept", ".swf" );
    } else if (type_value == "LOGO") {
        $('#txt_duration_add_a').val("0");
        $('#txt_width').val("100");
        $('#txt_hight').val("100");
        $('#txt_x_position').val("520");
        $('#txt_y_position').val("476");
        $('#fileupload').attr( "accept", ".swf" );
        $('#logo_container').show();
    } else if (type_value == "LOGO_CONTAINER") {
        $('#fileupload').attr( "accept", ".swf" );
        $('#logo_list_table').show();
    } else if(type_value == "FILLER"){
        $('#fileupload').attr( "accept", ".mp4, .mov, .avi, .mpg" );
    } else if(type_value == "SLIDE"){
        $('#fileupload').attr( "accept", ".jpg, .png" );
    }

}

$(document).ready(function () {

    function progressHandlingFunction(evt) {
        if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            percentComplete = parseInt(percentComplete * 100);

            setProgress(percentComplete);
            console.log(percentComplete);
        }
    }

    function setProgress(value) {
        $('#progress .bar').css('width', value + '%');
        if (value === 100) {
            //alert("file successfully uploaded");
        }
    }

    var files = [];
    $("#fileupload").on('change', function (event) {
        setProgress(0);
        files = [];
        files = event.target.files;

        if (files.length > 0)
            console.log('event fired' + event.target.files[0].name);
    });

    $("#subbutton").click(function () {
        if (files.length == 0) {
            alert('Please Select a File ..');
        } else {
            showDialog();
            processFileUpload();
        }
    });

    function processFileUpload()
    {
        showDialog();
        console.log("file upload clicked");
        console.log(iAdvertID);
        var oUploadForm = new FormData();
        oUploadForm.append("file", files[0]);
        oUploadForm.append("advert_id", "" + iAdvertID);
        oUploadForm.append("advert_name", "" + $("#txt_advert_name_add_a").val());
        oUploadForm.append("advert_path", "" + $("#txt_advert_path_add_a").val());
        fileUploading();

        var csrf_headerName  = $("meta[name='_csrf_header']").attr("content");
        var csrf_paramValue  = $("meta[name='_csrf']").attr("content");
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", progressHandlingFunction, false);
                return xhr;
            },
            beforeSend: function (request) {
                request.setRequestHeader(csrf_headerName, csrf_paramValue);
            },
            dataType: 'json',
            url: URI.advertisement.uploadFileUrl(),
            data: oUploadForm,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (data) {
                closeDialog();
                fileUploaded();
                $("tr:has(td)").remove();
                $('#lblNullFile').hide();
                setPreviewVideo(data);
            },
            error: function (result) {
                closeDialog();
                alert('File Upload Error.');
            }
        });
    }

});

function setPreviewVideo(advert){
     var path = advert.advertpath;
     path = path.split(UTIL.common.getFileSeparator());
     var index = path.length - 1;
     var fileName = path[index];
     fileName = fileName.split('.');
     fileName[1] = fileName[1].toLowerCase();
     var fileExtension=fileName[1];

//     var mediaFileRootPath = UTIL.common.getFileSeparator() +
//                                  'media-files-' + UTIL.common.getApplicationVersion() + UTIL.common.getFileSeparator();
     var mediaFileRootPath = UTIL.common.getFileSeparator() +
                                  'media-files' + UTIL.common.getFileSeparator();
     if (fileExtension == 'mpg' && advert.fileAvailable)
         previewVideo(mediaFileRootPath + fileName[0] + '.mp4');
     else if (fileExtension == 'swf' && advert.fileAvailable)
         previewVideo(mediaFileRootPath + fileName[0] + '.swf');
     else if ((fileExtension == 'jpg' || fileExtension == 'png') && advert.fileAvailable)
         previewVideo(mediaFileRootPath + fileName[0] + '.' + fileName[1]);
     else
         $('#lblNullFile').show();
}

function previewVideo(path) {
    console.log(path);
    console.log((path.substr(path.lastIndexOf('.') + 1)).toLowerCase());


    $("#flash").hide();
    $("#video").hide();
    $("#image").hide();

    $("#image").attr('src', '');
    $("#video").attr('src', '');
    $("#flash").attr('data', '');

    var ext = (path.substr(path.lastIndexOf('.') + 1)).toLowerCase();
    if (ext == "mp4") {
        $("#video").show();
        $("#video").attr('src', path);
        $("#video").attr('poster', '');
    } else if (ext == "swf") {
        $("#flash").show();
        $("#flash").attr('data', path);
    } else {
        $("#image").show();
        $("#image").attr('src', path);
    }
}

function suspendAdvertisement() {

    var ret = confirm("Are you sure you want to suspend this advertisement?");
    if (!ret) {
        return;
    }

    showDialog();
    var advertId = $("#txt_advert_id_add_a").val();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementSuspendUrl(),
        data: {
            advertid: advertId,
        },
        success: function (data)
        {
            var expire_dt = document.getElementById("txt_expireDate").value;
            var suspend_dt = document.getElementById("txt_suspendDate").value;
            if(new moment().format("YYYY-MM-DD") <= suspend_dt && suspend_dt <= expire_dt){
                var resumeBtn = '<button type="button" onclick="resumeAdvertisement()" class="btn btn-block btn_yellocolor">Resume</button>';
                $('#suspend_div').html(resumeBtn);
            }
            closeDialog();
            alert("successfully hold");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");
        }
    });
}

function resumeAdvertisement() {

    showDialog();
    var advertId = $("#txt_advert_id_add_a").val();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.advertisement.advertisementResumeUrl(),
        data: {
            advertid: advertId,
        },
        success: function (data)
        {
            var resumeBtn = '<button type="button" onclick="suspendAdvertisement()" class="btn btn-block btn_readcolor_advert_suspend">Suspend</button>';
            $('#suspend_div').html(resumeBtn);
            closeDialog();
            alert("successfully resume");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            alert("Error");
        }
    });

}

function fileUploading() {
    $('#state').addClass('glyphicon-upload');
    $("#state").css("color", "#1E90FF");
}

function fileUploaded() {
    $('#state').addClass('glyphicon-ok').removeClass('glyphicon-upload');
    $("#state").css("color", "#5cb85c");
}

function getAdvertData() {

    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        async: false,
        contentType: 'application/json',
        url: URI.advertisement.getAdvertHistoryUrl(),
        data: {
            advertId: iAdvertID
        },
        success: function (data)
        {
            closeDialog();

            var trHTML = '';
            trHTML += '<thead><th class="col-md-1">Date and Time</th><th class="col-md-4">Field</th><th class="col-md-2">Old</th><th class="col-md-2">New</th><th class="col-md-2">User</th></tr></thead>';
            trHTML += '<tbody>';


            for (var i in data)
            {
                trHTML += '<tr><td>' + data[i].logTime + '</td><td>' + data[i].field + '</td><td>' + data[i].oldData + '</td><td>' + data[i].newData + '</td><td>' + data[i].user + '</td></tr>';
            }

            trHTML += '</tbody>';

            $('#advert-material-data').html(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    });
}

function loadProducts(){
    var clientId = $('#cmbClient').val();
    if(clientId === "-1"){
        return;
    }
    showDialog();
    $.ajax({
        type: 'GET',
        dataType: "json",
        contentType: 'application/json',
        url: URI.product.getProductsByClient(clientId),
        success: function (data)
        {
            closeDialog();
            var trHTML = '';
            trHTML += ' <select id="cmbProduct" name="" class="form-control" required="">';
            data.forEach(function(c){
                var label = c.product.name + ' - ' + c.brand.name + ' | ' + c.code;
                trHTML += '<option value="' + c.id + '">' + label + '</option>';
            })
            trHTML += '</select>';
            trHTML += '<span class="help-block"></span>';
            $('#product_select').html(trHTML);

            if(codeMapId != -1){
                document.getElementById("cmbProduct").value = codeMapId;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            closeDialog();
            if (errorThrown == 'Unauthorized') {
                localStorage.clear();
                window.location.replace("Login.html");
            }
            else {
                alert("Error");
            }
        }
    })
}
