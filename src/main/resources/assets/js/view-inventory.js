$(document).ready(function () {

    $(function () {
        $( "#select-date").datetimepicker({
            pickTime: false,
            startDate: new Date()
        });
    });

   $channelDropDown =  $('#channel\\.id');
   $dateVal = $('#date');

   var now = $('#date').val();
   var now = new Date(now);
   $("#date-slider").val(now.toDateString());

   $dateVal.on('change', function(e){
       location.href = URI.inventory.getInventoryByChannelAndDate($channelDropDown.val(),$dateVal.val());
   });
   //loader.hide();

   $channelDropDown =  $('#channel\\.channelid');
   $dateVal = $('#date');

   $channelDropDown.on('change', function(e){
        location.href = URI.inventory.getInventoryByChannelAndDate($channelDropDown.val(),$dateVal.val());
   });

   $('#view-inventory').on('click', function(e){
       location.href = URI.inventory.getInventoryByChannelAndDate($channelDropDown.val(),$dateVal.val());
  });
});


function nextDate() {
    var now = $('#date').val();
    var now = new Date(now);
    now.setDate(now.getDate() + 1);

    $channelDropDown =  $('#channel\\.channelid');

    var d = new Date(now),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    location.href = URI.inventory.getInventoryByChannelAndDate($channelDropDown.val(),[year, month, day].join('-'));
}

 function previousDate() {
        var now = $('#date').val();
        var now = new Date(now);
        now.setDate(now.getDate() - 1);

        $channelDropDown =  $('#channel\\.channelid');

        var d = new Date(now),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

        location.href = URI.inventory.getInventoryByChannelAndDate($channelDropDown.val(),[year, month, day].join('-'));
  }