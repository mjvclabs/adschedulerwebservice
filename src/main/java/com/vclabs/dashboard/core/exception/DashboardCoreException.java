package com.vclabs.dashboard.core.exception;

/**
 * Created by dperera on 18/09/2018.
 */
public class DashboardCoreException extends Exception {

    public DashboardCoreException(String message){
        super(message);
    }

    public DashboardCoreException(Throwable cause){
        super(cause);
    }
}
