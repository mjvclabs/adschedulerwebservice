package com.vclabs.dashboard.core.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import javax.jms.*;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by dperera on 13/09/2018.
 */
@Configuration
@PropertySource(value = "classpath:message-broker.properties")
@ComponentScan(basePackages = {"com.vclabs.dashboard.core"})
public class MessagingConfiguration {

    @Value("${activemq.broker.url}")
    private String DEFAULT_BROKER_URL;

    @Bean
    public ConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(DEFAULT_BROKER_URL);
        connectionFactory.setTrustedPackages(Arrays.asList("com.vclabs.dashboard.core"));
        return connectionFactory;
    }

    @Bean
    public ConnectionFactory cachingConnectionFactory(){
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setTargetConnectionFactory(connectionFactory());
        connectionFactory.setSessionCacheSize(10);
        return connectionFactory;
    }

    @Bean("topicJmsTemplate")
    public JmsTemplate topicJmsTemplate(){
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        template.setPubSubDomain(true);
        template.setMessageConverter(converter());
        return template;
    }

    @Bean("queueJmsTemplate")
    public JmsTemplate queueJmsTemplate(){
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        template.setMessageConverter(converter());
        return template;
    }

    @Bean
    public MessageConverter converter(){
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter() {
            @Override
            public Object fromMessage(Message message) throws JMSException, MessageConversionException {
                return message instanceof ObjectMessage ? ((ObjectMessage) message).getObject() : super.fromMessage(message);
            }
            @Override
            protected TextMessage mapToTextMessage(Object object, Session session, ObjectMapper objectMapper) throws JMSException, IOException {
                final TextMessage rv = super.mapToTextMessage(object, session, objectMapper);
                rv.setStringProperty("content-type", "application/json;charset=UTF-8");
                return rv;
            }
        };
        messageConverter.setTargetType(MessageType.TEXT);
        return messageConverter;
    }

}
