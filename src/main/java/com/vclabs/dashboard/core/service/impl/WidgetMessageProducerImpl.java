package com.vclabs.dashboard.core.service.impl;

import com.vclabs.dashboard.core.service.WidgetMessageProducer;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by dperera on 12/09/2018.
 */
@Service
public class WidgetMessageProducerImpl implements WidgetMessageProducer {

    @Autowired
    @Qualifier("topicJmsTemplate")
    private JmsTemplate template;

    @Override
    public void send(String destinationName, BasicWidgetDM data) {
        template.convertAndSend(destinationName, data);
    }
}
