package com.vclabs.dashboard.core.service;

import com.vclabs.dashboard.core.data.convert.ConverterName;
import com.vclabs.dashboard.core.data.dto.User;
import com.vclabs.dashboard.core.data.dto.UserGroup;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.exception.DashboardCoreException;

import java.util.List;


/**
 * Created by dperera on 13/09/2018.
 */
public interface DashboardManager {

    <T extends BasicWidgetDM> T convert(ConverterName converterName, Object... parameters);

    void convertAndSend(ConverterName converterName, String destination, Object... parameters);

    User saveUser(User user) throws DashboardCoreException;

    User getUser(String username);

    List<UserGroup> getUserGroups();

    User updateUserPassword(User user) throws DashboardCoreException;

    User update(User user);

}
