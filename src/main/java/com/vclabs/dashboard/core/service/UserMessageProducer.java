package com.vclabs.dashboard.core.service;

import com.vclabs.dashboard.core.data.dto.User;

/**
 * Created by dperera on 12/09/2018.
 */
public interface UserMessageProducer {

    void sendNewUserMessage(User user);

}
