package com.vclabs.dashboard.core.service.impl;

import com.vclabs.dashboard.core.data.dto.User;
import com.vclabs.dashboard.core.data.dto.UserGroup;
import com.vclabs.dashboard.core.exception.DashboardCoreException;
import com.vclabs.dashboard.core.service.UserMessageProducer;
import com.vclabs.dashboard.core.service.WidgetMessageProducer;
import com.vclabs.dashboard.core.data.convert.ConverterName;
import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dperera on 13/09/2018.
 */
@Service("dashboardManager")
@PropertySource(value = "classpath:dashboard-web.properties")
public class DashboardManagerImpl implements DashboardManager {

    @Value("${dashboard.web.user.groups.url}")
    private String DASHBOARD_WEB_USER_GROUPS_URL;

    @Value("${dashboard.web.user.save.url}")
    private String DASHBOARD_WEB_USER_SAVE_URL;

    @Value("${dashboard.web.credential}")
    private String DASHBOARD_WEB_CREDENTIAL;

    @Value("${dashboard.web.user.update_password.url}")
    private String DASHBOARD_WEB_USER_PWD_UPDATE_URL;

    @Value("${dashboard.web.user.details.url}")
    private String DASHBOARD_WEB_USER_DETAILS_URL;

    @Value("${dashboard.web.user.update.url}")
    private String DASHBOARD_WEB_USER_UPDATE_URL;

    @Autowired
    private List<WidgetDMConverter> widgetDMConverters;

    @Autowired
    private WidgetMessageProducer widgetMessageProducer;

    @Autowired
    private UserMessageProducer userMessageProducer;

    @Override
    public <T extends BasicWidgetDM> T convert(ConverterName converterName, Object... parameters) {
        WidgetDMConverter converter = getConverterByName(converterName);
        return converter != null ? (T) converter.convert(parameters) : null;
    }

    @Override
    public void convertAndSend(ConverterName converterName, String destination, Object... parameters) {
        BasicWidgetDM dataModel = convert(converterName, parameters);
        widgetMessageProducer.send(destination, dataModel);
    }

    private WidgetDMConverter getConverterByName(ConverterName converterName){
        for (WidgetDMConverter converter : widgetDMConverters){
            if(converter.getName() == converterName){
                return converter;
            }
        }
        return null;
    }

    @Transactional
    @Override
    public User saveUser(User user) throws DashboardCoreException {
        HttpEntity<User> request = new HttpEntity<>(user, getHttpRequestHeaders());
        RestTemplate restTemplate = new RestTemplate();
        try{
            ResponseEntity<User> response = restTemplate.exchange(DASHBOARD_WEB_USER_SAVE_URL, HttpMethod.POST, request, User.class);
            return response.getBody();
        }catch (HttpServerErrorException ex){
            throw new DashboardCoreException(ex.getResponseBodyAsString());
        }
    }

    @Override
    public User getUser(String username) {
        HttpEntity<String> request = new HttpEntity<>(getHttpRequestHeaders());
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<User> response = restTemplate.exchange(DASHBOARD_WEB_USER_DETAILS_URL, HttpMethod.GET, request, User.class, username);
        return response.getBody();
    }

    @Transactional
    @Override
    public User updateUserPassword(User user) throws DashboardCoreException {
        HttpEntity<User> request = new HttpEntity<>(user, getHttpRequestHeaders());
        RestTemplate restTemplate = new RestTemplate();
        try{
            ResponseEntity<User> response = restTemplate.exchange(DASHBOARD_WEB_USER_PWD_UPDATE_URL, HttpMethod.POST, request, User.class);
            return response.getBody();
        }catch (HttpServerErrorException ex){
            throw new DashboardCoreException(ex.getResponseBodyAsString());
        }
    }


    @Override
    public User update(User user) {
        HttpEntity<User> request = new HttpEntity<>(user, getHttpRequestHeaders());
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<User> response = restTemplate.exchange(DASHBOARD_WEB_USER_UPDATE_URL, HttpMethod.POST, request, User.class);
        return response.getBody();
    }

    @Transactional
    @Override
    public List<UserGroup> getUserGroups() {
        HttpEntity<String> request = new HttpEntity<>(getHttpRequestHeaders());
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<UserGroup[]> response = restTemplate.exchange(DASHBOARD_WEB_USER_GROUPS_URL, HttpMethod.GET, request, UserGroup[].class);
        return Arrays.asList(response.getBody());
    }

    private HttpHeaders getHttpRequestHeaders() {
        String base64Credentials = new String(Base64.encode(DASHBOARD_WEB_CREDENTIAL.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
