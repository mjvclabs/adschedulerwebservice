package com.vclabs.dashboard.core.service;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

/**
 * Created by dperera on 12/09/2018.
 */
public interface WidgetMessageProducer {

    void send(String destination, BasicWidgetDM data);
}
