package com.vclabs.dashboard.core.service.impl;

import com.vclabs.dashboard.core.data.dto.User;
import com.vclabs.dashboard.core.service.UserMessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
/**
 * Created by dperera on 28/09/2018.
 */
@Service
public class UserMessageProducerImpl implements UserMessageProducer {

    @Autowired
    @Qualifier("queueJmsTemplate")
    private JmsTemplate template;

    @Override
    public void sendNewUserMessage(User user) {
        template.convertAndSend("dashboard.user.queue", user);
    }
}
