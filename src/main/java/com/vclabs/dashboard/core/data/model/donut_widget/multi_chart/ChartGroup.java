package com.vclabs.dashboard.core.data.model.donut_widget.multi_chart;

import java.util.List;

/**
 * Created by dperera on 14/12/2018.
 */
public class ChartGroup {

    private String bgColor;

    private long value;

    private String type;

    private List<DetailRow> details;

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DetailRow> getDetails() {
        return details;
    }

    public void setDetails(List<DetailRow> details) {
        this.details = details;
    }
}
