package com.vclabs.dashboard.core.data.model.hbar_widget;

import java.util.List;

/**
 * Created by dperera on 29/11/2018.
 */
public class HBarChartConfig {

    private List<LegendItem> legend;

    public List<LegendItem> getLegend() {
        return legend;
    }

    public void setLegend(List<LegendItem> legend) {
        this.legend = legend;
    }
}
