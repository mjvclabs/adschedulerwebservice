package com.vclabs.dashboard.core.data.model.list_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dperera on 12/09/2018.
 */
public class ListWidgetDM extends BasicWidgetDM {

    private String subTitle;

    private List<ListItem> list;

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public List<ListItem> getList() {
        return list;
    }

    public void setList(List<ListItem> list) {
        this.list = list;
    }

    public void addListItem(ListItem item){
        if(list == null){
            list = new ArrayList<>();
        }
        list.add(item);
    }
}
