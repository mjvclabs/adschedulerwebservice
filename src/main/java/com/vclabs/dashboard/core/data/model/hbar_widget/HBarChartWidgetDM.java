package com.vclabs.dashboard.core.data.model.hbar_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 21/11/2018.
 */
public class HBarChartWidgetDM extends BasicWidgetDM {

    private List<HBarRow> chartData;

    private HBarChartConfig chartConfig;

    public List<HBarRow> getChartData() {
        return chartData;
    }

    public void setChartData(List<HBarRow> chartData) {
        this.chartData = chartData;
    }

    public HBarChartConfig getChartConfig() {
        return chartConfig;
    }

    public void setChartConfig(HBarChartConfig chartConfig) {
        this.chartConfig = chartConfig;
    }
}
