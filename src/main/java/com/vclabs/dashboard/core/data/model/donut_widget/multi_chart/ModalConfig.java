package com.vclabs.dashboard.core.data.model.donut_widget.multi_chart;

import java.util.List;

/**
 * Created by dperera on 14/12/2018.
 */
public class ModalConfig {

    private List<String> tableHeaders;

    private String ddAllOption;

    public List<String> getTableHeaders() {
        return tableHeaders;
    }

    public void setTableHeaders(List<String> tableHeaders) {
        this.tableHeaders = tableHeaders;
    }

    public String getDdAllOption() {
        return ddAllOption;
    }

    public void setDdAllOption(String ddAllOption) {
        this.ddAllOption = ddAllOption;
    }
}
