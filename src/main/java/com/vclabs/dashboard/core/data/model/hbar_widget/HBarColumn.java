package com.vclabs.dashboard.core.data.model.hbar_widget;

/**
 * Created by dperera on 29/11/2018.
 */
public class HBarColumn {

    private String color;

    private long value;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
