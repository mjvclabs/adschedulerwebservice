package com.vclabs.dashboard.core.data.model.hbar_widget;

import java.util.List;

/**
 * Created by dperera on 21/11/2018.
 */
public class HBarRow {

    private long id;

    private String name;

    private List<HBarColumn> hbar;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HBarColumn> getHbar() {
        return hbar;
    }

    public void setHbar(List<HBarColumn> hbar) {
        this.hbar = hbar;
    }
}
