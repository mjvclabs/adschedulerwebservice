package com.vclabs.dashboard.core.data.model.table_widget.pa_widget;

import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;

/**
 * Created by dperera on 06/12/2018.
 */
public class PATableWidgetDM<T> extends TableWidgetDM<T> {

    private PAModalConfig modalConfig;

    public PAModalConfig getModalConfig() {
        return modalConfig;
    }

    public void setModalConfig(PAModalConfig modalConfig) {
        this.modalConfig = modalConfig;
    }
}
