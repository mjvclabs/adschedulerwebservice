package com.vclabs.dashboard.core.data.model.vbar_pie_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 11/01/2019.
 */
public class VBarPieChartWidget extends BasicWidgetDM {

    private List<Group> rowData;

    private List<String> tableHeaders;

    public List<Group> getRowData() {
        return rowData;
    }

    public void setRowData(List<Group> rowData) {
        this.rowData = rowData;
    }

    public List<String> getTableHeaders() {
        return tableHeaders;
    }

    public void setTableHeaders(List<String> tableHeaders) {
        this.tableHeaders = tableHeaders;
    }
}
