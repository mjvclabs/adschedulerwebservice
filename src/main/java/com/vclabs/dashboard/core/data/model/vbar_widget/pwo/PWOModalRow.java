package com.vclabs.dashboard.core.data.model.vbar_widget.pwo;

import com.vclabs.dashboard.core.data.model.common.Url;

/**
 * Created by dperera on 27/11/2018.
 */
public class PWOModalRow {

    private Url c1;

    private Url c2;

    private String c3;

    private String c4;

    public Url getC1() {
        return c1;
    }

    public void setC1(Url c1) {
        this.c1 = c1;
    }

    public Url getC2() {
        return c2;
    }

    public void setC2(Url c2) {
        this.c2 = c2;
    }

    public String getC3() {
        return c3;
    }

    public void setC3(String c3) {
        this.c3 = c3;
    }

    public String getC4() {
        return c4;
    }

    public void setC4(String c4) {
        this.c4 = c4;
    }
}
