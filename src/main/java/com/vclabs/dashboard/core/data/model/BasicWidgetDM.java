package com.vclabs.dashboard.core.data.model;

/**
 * Created by dperera on 12/09/2018.
 */
public abstract class BasicWidgetDM {

    private String widgetId;

    private String title;

    public String getWidgetId() {
        return widgetId;
    }

    public void setWidgetId(String widgetId) {
        this.widgetId = widgetId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
