package com.vclabs.dashboard.core.data.model.vbar_widget.pwo;

import com.vclabs.dashboard.core.data.model.vbar_widget.VBar;

import java.util.List;

/**
 * Created by dperera on 27/11/2018.
 */
public class Group {

    private int id;

    private String groupName;

    private List<VBar> chartData;

    private List<PWOModalRow> modalData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<VBar> getChartData() {
        return chartData;
    }

    public void setChartData(List<VBar> chartData) {
        this.chartData = chartData;
    }

    public List<PWOModalRow> getModalData() {
        return modalData;
    }

    public void setModalData(List<PWOModalRow> modalData) {
        this.modalData = modalData;
    }
}
