package com.vclabs.dashboard.core.data.model.donut_widget.multi_chart;

import java.util.List;

/**
 * Created by dperera on 14/12/2018.
 */
public class DonutChart {

    private long id;

    private String name;

    private List<ChartGroup> groups;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ChartGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<ChartGroup> groups) {
        this.groups = groups;
    }
}
