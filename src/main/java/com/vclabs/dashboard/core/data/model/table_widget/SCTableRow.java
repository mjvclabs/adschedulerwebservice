package com.vclabs.dashboard.core.data.model.table_widget;

/**
 * Created by dperera on 02/01/2019.
 */
public class SCTableRow {

    private String id;

    private String c1;

    private String c2;

    private String bgColor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getC1() {
        return c1;
    }

    public void setC1(String c1) {
        this.c1 = c1;
    }

    public String getC2() {
        return c2;
    }

    public void setC2(String c2) {
        this.c2 = c2;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }
}
