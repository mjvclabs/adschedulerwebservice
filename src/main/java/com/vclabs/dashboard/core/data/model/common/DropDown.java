package com.vclabs.dashboard.core.data.model.common;

/**
 * Created by dperera on 06/12/2018.
 */
public class DropDown {

    private String name;

    private String paramName;

    private String firstOption;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getFirstOption() {
        return firstOption;
    }

    public void setFirstOption(String firstOption) {
        this.firstOption = firstOption;
    }
}
