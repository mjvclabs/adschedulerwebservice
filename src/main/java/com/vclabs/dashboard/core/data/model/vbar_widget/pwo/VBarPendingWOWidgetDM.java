package com.vclabs.dashboard.core.data.model.vbar_widget.pwo;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 27/11/2018.
 */
public class VBarPendingWOWidgetDM extends BasicWidgetDM {

    private List<Group> groups;

    private PWOModalConfig modalConfig;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public PWOModalConfig getModalConfig() {
        return modalConfig;
    }

    public void setModalConfig(PWOModalConfig modalConfig) {
        this.modalConfig = modalConfig;
    }
}
