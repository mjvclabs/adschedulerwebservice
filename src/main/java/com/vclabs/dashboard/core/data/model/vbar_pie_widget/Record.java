package com.vclabs.dashboard.core.data.model.vbar_pie_widget;

/**
 * Created by dperera on 11/01/2019.
 */
public class Record {

    private String name;

    private long amount;

    private String backgroundColor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}
