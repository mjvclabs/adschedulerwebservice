package com.vclabs.dashboard.core.data.model.col_widget;

/**
 * Created by dperera on 18/01/2019.
 */
public class DataRow {

    private long id;

    private String icon;

    private long count;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
