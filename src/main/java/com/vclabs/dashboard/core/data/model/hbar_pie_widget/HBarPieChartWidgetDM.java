package com.vclabs.dashboard.core.data.model.hbar_pie_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 17/01/2019.
 */
public class HBarPieChartWidgetDM extends BasicWidgetDM {

    private List<BarChartData> barChartData;

    private PieChartData pieChartData;

    private String bottomText;

    private String unitText;

    public List<BarChartData> getBarChartData() {
        return barChartData;
    }

    public void setBarChartData(List<BarChartData> barChartData) {
        this.barChartData = barChartData;
    }

    public PieChartData getPieChartData() {
        return pieChartData;
    }

    public void setPieChartData(PieChartData pieChartData) {
        this.pieChartData = pieChartData;
    }

    public String getBottomText() {
        return bottomText;
    }

    public void setBottomText(String bottomText) {
        this.bottomText = bottomText;
    }

    public String getUnitText() {
        return unitText;
    }

    public void setUnitText(String unitText) {
        this.unitText = unitText;
    }
}
