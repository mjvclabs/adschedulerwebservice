package com.vclabs.dashboard.core.data.model.vbar_pie_widget;

import java.util.List;

/**
 * Created by dperera on 11/01/2019.
 */
public class Group {

    private long id;

    private String group;

    private List<Record> records;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
