package com.vclabs.dashboard.core.data.model.vbar_widget.pwo;

import java.util.List;

/**
 * Created by dperera on 28/11/2018.
 */
public class PWOModalConfig {

    private List<String> tableHeaders;

    public List<String> getTableHeaders() {
        return tableHeaders;
    }

    public void setTableHeaders(List<String> tableHeaders) {
        this.tableHeaders = tableHeaders;
    }
}
