package com.vclabs.dashboard.core.data.model.node_widget;

/**
 * Created by dperera on 08/01/2019.
 */
public class NodeDetails {

    private long id;

    private String text;

    private String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
