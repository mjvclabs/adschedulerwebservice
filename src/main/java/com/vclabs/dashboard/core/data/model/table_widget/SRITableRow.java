package com.vclabs.dashboard.core.data.model.table_widget;

import com.vclabs.dashboard.core.data.model.common.Url;

/**
 * Created by dperera on 23/11/2018.
 */
public class SRITableRow {

    private String id;

    private Url c1;

    private String c2;

    private String c3;

    private String c4;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Url getC1() {
        return c1;
    }

    public void setC1(Url c1) {
        this.c1 = c1;
    }

    public String getC2() {
        return c2;
    }

    public void setC2(String c2) {
        this.c2 = c2;
    }

    public String getC3() {
        return c3;
    }

    public void setC3(String c3) {
        this.c3 = c3;
    }

    public String getC4() {
        return c4;
    }

    public void setC4(String c4) {
        this.c4 = c4;
    }
}
