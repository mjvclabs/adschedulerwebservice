package com.vclabs.dashboard.core.data.model.table_widget;

import java.util.List;

/**
 * Created by dperera on 23/11/2018.
 */
public class TableRow {

    private String id;

    private List<String> values;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
