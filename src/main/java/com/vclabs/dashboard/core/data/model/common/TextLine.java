package com.vclabs.dashboard.core.data.model.common;

/**
 * Created by dperera on 14/12/2018.
 */
public class TextLine {

    private String label;

    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
