package com.vclabs.dashboard.core.data.dto;

import java.util.List;
import java.util.Set;

/**
 * Created by dperera on 01/10/2018.
 */
public class User {

    private String username;

    private String password;

    private List<UserGroup> groups;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<UserGroup> groups) {
        this.groups = groups;
    }

}
