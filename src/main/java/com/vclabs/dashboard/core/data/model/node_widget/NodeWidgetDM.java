package com.vclabs.dashboard.core.data.model.node_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.list_widget.ListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dperera on 12/09/2018.
 */
public class NodeWidgetDM extends BasicWidgetDM {

    private List<Node> nodes;

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }
}
