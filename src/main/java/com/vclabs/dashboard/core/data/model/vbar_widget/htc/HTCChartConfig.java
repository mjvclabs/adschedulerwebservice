package com.vclabs.dashboard.core.data.model.vbar_widget.htc;

import com.vclabs.dashboard.core.data.model.common.chart_config.YAxisScaleLabel;

/**
 * Created by dperera on 28/11/2018.
 */
public class HTCChartConfig {

    private YAxisScaleLabel yaxisScaleLabel;

    public YAxisScaleLabel getYaxisScaleLabel() {
        return yaxisScaleLabel;
    }

    public void setYaxisScaleLabel(YAxisScaleLabel yaxisScaleLabel) {
        this.yaxisScaleLabel = yaxisScaleLabel;
    }
}
