package com.vclabs.dashboard.core.data.model.hbar_pie_widget;

/**
 * Created by dperera on 17/01/2019.
 */
public class DataRow {

    private String name;

    private long percentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPercentage() {
        return percentage;
    }

    public void setPercentage(long percentage) {
        this.percentage = percentage;
    }
}
