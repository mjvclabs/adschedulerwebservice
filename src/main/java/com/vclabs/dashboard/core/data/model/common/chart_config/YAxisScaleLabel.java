package com.vclabs.dashboard.core.data.model.common.chart_config;

/**
 * Created by dperera on 28/11/2018.
 */
public class YAxisScaleLabel {

    private String labelString;

    public String getLabelString() {
        return labelString;
    }

    public void setLabelString(String labelString) {
        this.labelString = labelString;
    }
}
