package com.vclabs.dashboard.core.data.model.col_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 18/01/2019.
 */
public class TwoColumnWidgetDM extends BasicWidgetDM {

    private List<DataRow> data;

    public List<DataRow> getData() {
        return data;
    }

    public void setData(List<DataRow> data) {
        this.data = data;
    }
}
