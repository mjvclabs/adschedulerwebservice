package com.vclabs.dashboard.core.data.model.table_widget.pa_widget;

import com.vclabs.dashboard.core.data.model.common.DropDown;

import java.util.List;

/**
 * Created by dperera on 06/12/2018.
 */
public class PAModalConfig {

    private List<DropDown> dropDowns;

    public List<DropDown> getDropDowns() {
        return dropDowns;
    }

    public void setDropDowns(List<DropDown> dropDowns) {
        this.dropDowns = dropDowns;
    }
}
