package com.vclabs.dashboard.core.data.model.node_widget;

import java.util.List;

/**
 * Created by dperera on 08/01/2019.
 */
public class Node {

    private long id;

    private String name;

    private long count;

    private List<NodeDetails> details;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<NodeDetails> getDetails() {
        return details;
    }

    public void setDetails(List<NodeDetails> details) {
        this.details = details;
    }
}
