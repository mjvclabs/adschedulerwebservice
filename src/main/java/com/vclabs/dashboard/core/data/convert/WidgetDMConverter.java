package com.vclabs.dashboard.core.data.convert;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;


/**
 * Created by dperera on 13/09/2018.
 */
public abstract class WidgetDMConverter<T extends BasicWidgetDM> {

    private ConverterName name;

    public abstract void init();

    /**
     * This method should be overridden to prepare data model for the widget
     * @param parameters using the related business logic
     * @param <T>
     * @return
     */
    public abstract <T extends BasicWidgetDM> T convert(Object... parameters);

    public ConverterName getName() {
        return name;
    }

    public void setName(ConverterName name) {
        this.name = name;
    }
}
