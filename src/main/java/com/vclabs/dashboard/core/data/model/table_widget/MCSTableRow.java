package com.vclabs.dashboard.core.data.model.table_widget;

import com.vclabs.dashboard.core.data.model.common.Url;

/**
 * Created by dperera on 23/11/2018.
 */
public class MCSTableRow {

    private String id;

    private String c1;

    private String c2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getC1() {
        return c1;
    }

    public void setC1(String c1) {
        this.c1 = c1;
    }

    public String getC2() {
        return c2;
    }

    public void setC2(String c2) {
        this.c2 = c2;
    }
}
