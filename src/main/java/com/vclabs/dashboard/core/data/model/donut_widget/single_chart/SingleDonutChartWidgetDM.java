package com.vclabs.dashboard.core.data.model.donut_widget.single_chart;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.TextLine;
import com.vclabs.dashboard.core.data.model.donut_widget.multi_chart.DonutChart;

/**
 * Created by dperera on 14/12/2018.
 */
public class SingleDonutChartWidgetDM extends BasicWidgetDM {

    private DonutChart chart;

    private TextLine centerText;

    public DonutChart getChart() {
        return chart;
    }

    public void setChart(DonutChart chart) {
        this.chart = chart;
    }

    public TextLine getCenterText() {
        return centerText;
    }

    public void setCenterText(TextLine centerText) {
        this.centerText = centerText;
    }
}
