package com.vclabs.dashboard.core.data.model.table_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 23/11/2018.
 */
public class TableWidgetDM<T> extends BasicWidgetDM {

    private List<T> rows;

    private List<String> tableHeaders;

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public List<String> getTableHeaders() {
        return tableHeaders;
    }

    public void setTableHeaders(List<String> tableHeaders) {
        this.tableHeaders = tableHeaders;
    }
}
