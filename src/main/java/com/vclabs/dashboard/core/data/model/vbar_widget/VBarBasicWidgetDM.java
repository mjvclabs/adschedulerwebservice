package com.vclabs.dashboard.core.data.model.vbar_widget;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 28/11/2018.
 */
public class VBarBasicWidgetDM extends BasicWidgetDM {

    private List<VBar> chartData;

    public List<VBar> getChartData() {
        return chartData;
    }

    public void setChartData(List<VBar> chartData) {
        this.chartData = chartData;
    }
}
