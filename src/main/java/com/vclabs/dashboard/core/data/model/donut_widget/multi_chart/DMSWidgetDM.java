package com.vclabs.dashboard.core.data.model.donut_widget.multi_chart;

import com.vclabs.dashboard.core.data.model.BasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 14/12/2018.
 */
public class DMSWidgetDM extends BasicWidgetDM {

    private List<DonutChart> charts;

    private ModalConfig modalConfig;

    public List<DonutChart> getCharts() {
        return charts;
    }

    public void setCharts(List<DonutChart> charts) {
        this.charts = charts;
    }

    public ModalConfig getModalConfig() {
        return modalConfig;
    }

    public void setModalConfig(ModalConfig modalConfig) {
        this.modalConfig = modalConfig;
    }
}
