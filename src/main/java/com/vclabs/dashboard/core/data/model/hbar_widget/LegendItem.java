package com.vclabs.dashboard.core.data.model.hbar_widget;

/**
 * Created by dperera on 29/11/2018.
 */
public class LegendItem {

    private String color;

    private String label;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
