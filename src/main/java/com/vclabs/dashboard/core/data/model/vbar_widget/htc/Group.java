package com.vclabs.dashboard.core.data.model.vbar_widget.htc;

import java.util.List;

/**
 * Created by dperera on 27/11/2018.
 */
public class Group {

    private int id;

    private String groupName;

    private List<Record> records;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
