package com.vclabs.dashboard.core.data.model.vbar_widget.htc;

import java.util.List;

/**
 * Created by dperera on 28/11/2018.
 */
public class HTCModalConfig {

    private List<String> tableHeaders;

    private String ddAllOption;

    public List<String> getTableHeaders() {
        return tableHeaders;
    }

    public void setTableHeaders(List<String> tableHeaders) {
        this.tableHeaders = tableHeaders;
    }

    public String getDdAllOption() {
        return ddAllOption;
    }

    public void setDdAllOption(String ddAllOption) {
        this.ddAllOption = ddAllOption;
    }
}
