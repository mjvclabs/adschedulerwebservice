package com.vclabs.dashboard.core.data.model.vbar_widget.htc;

import com.vclabs.dashboard.core.data.model.vbar_widget.VBarBasicWidgetDM;

import java.util.List;

/**
 * Created by dperera on 27/11/2018.
 * This class is used to feed data for High Traffic Channel Widget
 */
public class VBarHTCWidgetDM extends VBarBasicWidgetDM {

    private List<Group> modalData;

    private HTCModalConfig modalConfig;

    private HTCChartConfig chartConfig;

    public List<Group> getModalData() {
        return modalData;
    }

    public void setModalData(List<Group> modalData) {
        this.modalData = modalData;
    }

    public HTCModalConfig getModalConfig() {
        return modalConfig;
    }

    public void setModalConfig(HTCModalConfig modalConfig) {
        this.modalConfig = modalConfig;
    }

    public HTCChartConfig getChartConfig() {
        return chartConfig;
    }

    public void setChartConfig(HTCChartConfig chartConfig) {
        this.chartConfig = chartConfig;
    }
}
