package com.vclabs.nash.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by dperera on 22/08/2017.
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}

