package com.vclabs.nash.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by dperera on 21/08/2017.
 */
@EnableWebSecurity
@Configuration
@Order(1)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private CustomAccessDeniedHandler accessDeniedHandler;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/signin", "/assets-1.0/**", "/userdetaills/login/json", "/userdetaills/logout/json","/dashboard/manual","/dashboard/auto").permitAll()
                .antMatchers("/home", "/").authenticated()
                //start wo module endpoints
                .antMatchers("/workorder").access("hasRole('WorkOrder')")
                .antMatchers("/workorder/create").access("hasRole('NewWorkOrder')")
                .antMatchers("/workorder/update").access("hasRole('UpdateWorkOrder')")

                //end of wo module endpoints

                //start schedule module endpoints
                .antMatchers("/schedule").access("hasRole('ScheduleHome')")
                .antMatchers("/schedule/missedspot-reschedule").access("hasRole('MissedSpotRescheduling')")
                .antMatchers("/schedule/priority-setting").access("hasRole('PrioritySetting')")
                .antMatchers("/schedule/current-schedule").access("hasRole('CurrentSchedule')")
                .antMatchers("/schedule/schedule-history").access("hasRole('ScheduleHistory')")
                //end of schedule module endpoints
                .antMatchers("/fillers/all", "/fillers/tagging").access("hasRole('TagFillers')")

                //start advertisement module endpoints
                .antMatchers("/advertisement-v4").access("hasRole('AllAdvertisement')")
                .antMatchers("/advertisement-v4/new").access("hasRole('AddAdvertisement')")
                .antMatchers("/advertisement-v4/update").access("hasRole('UpdateAdvertisement')")
                .antMatchers("/advertisement-v4").access("hasRole('AllAdvertisement')")
                //end of advertisement module endpoints

                //start billing module endpoints
                .antMatchers("/billing").access("hasRole('BillHome')")
                .antMatchers("/billing/generated-bill").access("hasRole('GeneratedBill')")
                .antMatchers("/billing/edit-details").access("hasRole('BillHome')")
                .antMatchers("/billing/tr-report").access("hasRole('TransmissionReport')")
                .antMatchers("/billing/detail-view").access("hasRole('BillHome')")
                //end of billing module endpoints

                //start user module endpoints
                .antMatchers("/users-v4").access("hasRole('AllUser')")
                .antMatchers("/users-v4/new").access("hasRole('NewUser')")
                .antMatchers("/users-v4/group-management").access("hasRole('UserGroup')")
                .antMatchers("/users-v4/change-profile").authenticated()
                //end of user module endpoints

                //start admin module endpoints
                .antMatchers("/admin/schedule-panel").access("hasRole('AdminSchedulePanel')")
                .antMatchers("/admin/one-day-schedule").access("hasRole('OneDaySchedule')")
                .antMatchers("/admin/channel/list").access("hasRole('Channels')")
                .antMatchers("/admin/channel/update").access("hasRole('UpdateChannel')")
                .antMatchers("/admin/channel/new").access("hasRole('AddChannel')")
                .antMatchers("/admin/client/list").access("hasRole('Clients')")
                .antMatchers("/admin/client/update").access("hasRole('UpdateClient')")
                .antMatchers("/admin/client/new").access("hasRole('AddClient')")
                .antMatchers("/admin/advertisement-category/list").access("hasRole('Categories')")
                .antMatchers("/admin/advertisement-category/update").access("hasRole('UpdateCategory')")
                .antMatchers("/admin/advertisement-category/add").access("hasRole('NewCategory')")
                .antMatchers("/admin/clusters/list").access("hasRole('AllClusters')")
                .antMatchers("/admin/priorities/list").access("hasRole('AllUser')")
                .antMatchers("/admin/servers/list").access("hasRole('ServerControl')")
                .antMatchers("/admin/server/new").access("hasRole('AddServer')")
                .antMatchers("/admin/tax-details/list").access("hasRole('ROLE_TaxDetails')")
                .antMatchers("/admin/product/create").access("hasRole('ROLE_CreateProduct')")
                .antMatchers("/admin/product/update").access("hasRole('ROLE_UpdateProduct')")
                .antMatchers("/admin/product/all").access("hasRole('ROLE_AllProduct')")
                .antMatchers("/admin/playlist-setting").access("hasRole('ROLE_PlaylistSetting')")
                .antMatchers("/admin/playlist-lap-setting").access("hasRole('ROLE_PlaylistLapSetting')")
                .antMatchers("/admin/playlist-view").access("hasRole('ROLE_PlaylistView')")
                //end of admin module endpoints

                //inventory prediction view
                .antMatchers("/inventory/view-inventory-prediction").access("hasRole('ROLE_viewInventoryPrediction')")

                //team management
                .antMatchers("/users-v4/team-management").access("hasRole('ROLE_teamManagement')")
                .antMatchers("/users-v4/team-details").access("hasRole('ROLE_teamDetails')")

                //reporting
                .antMatchers("/reporting/dummy-cut").access("hasRole('ROLE_DummyCut')")
                .antMatchers("/reporting/commercial-availability-hourly").access("hasRole('ROLE_CommercialAvailability')")
                .antMatchers("/reporting/commercial-availability-channelwise").access("hasRole('ROLE_CommercialAvailability')")
                .antMatchers("/reporting/schedule-print").access("hasRole('ROLE_ScheduleReports')")
                .antMatchers("/reporting/schedule-analysis").access("hasRole('ROLE_ScheduleReports')")
                .antMatchers("/reporting/client-revenue-report").access("hasRole('ROLE_RevenueReport')")
                .antMatchers("/reporting/agency-revenue-report").access("hasRole('ROLE_RevenueReport')") 
                .antMatchers("/reporting/sales-report").access("hasRole('ROLE_SalesReport')")
                .antMatchers("/reporting/paymentdue").access("hasRole('ROLE_PaymentDue')")
                .antMatchers("/reporting/material-analysis").access("hasRole('ROLE_Media')")
                .antMatchers("/reporting/media-deletion-history").access("hasRole('ROLE_Media')")
                .antMatchers("/reporting/missed-spot-count").access("hasRole('ROLE_MissedSpotReport')")
                .antMatchers("/reporting/dashboard-auto").access("hasRole('ROLE_DashboardReport')")
                .antMatchers("/reporting/dashboard-manual").access("hasRole('ROLE_DashboardReport')")
                .antMatchers("/reporting/user-wise-report").access("hasRole('ROLE_UserWise')")

                .antMatchers("/servers/**", "/reporting/**").authenticated()

                .and().formLogin()
                .loginPage("/signin")
                .failureUrl("/signin-error")
                .loginProcessingUrl("/sign-in")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/home")
                .and().logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/signin")
                .and()
                .rememberMe()
                .rememberMeCookieName("nash-remember-me")
                .tokenValiditySeconds(24 * 60 * 60)
                .and().csrf().ignoringAntMatchers("/userdetaills/login/json","/userdetaills/logout/json","/dashboard/feed-widget-data")//ignore csrf token for Mosh
                .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    }

    //This sha1 encoder is used for existing passwords of nash db
    @Bean
    public PasswordEncoder passwordEncoder() {
       return new Sha1PasswordEncoder(sha1Encoder());
    }

    @Bean
    public MessageDigestPasswordEncoder sha1Encoder(){
        return new ShaPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

}