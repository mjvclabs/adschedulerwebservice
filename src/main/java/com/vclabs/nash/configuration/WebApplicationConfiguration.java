package com.vclabs.nash.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.CacheControl;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by dperera on 14/08/2017.
 */
@EnableWebMvc
@Configuration
@EnableAsync
@ComponentScan(basePackages = {"com.vclabs.nash", "com.vclabs.dashboard.core"})
@EnableScheduling
public class WebApplicationConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private Environment env;

    @Value("${application.version}")
    private String applicationVersion;

    @Value("${file.advert.previewMediaDirectory}")
    private String mediaServerDirectory;

    @Value("${file.logopath.dashboard}")
    private String logoFileDirectory;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/dashboard/feed-widget-data");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/assets-"+applicationVersion+"/**")
                .addResourceLocations("classpath:/assets/")
                .setCacheControl(CacheControl.maxAge(14, TimeUnit.DAYS));

       // registry.addResourceHandler("/media-files-"+applicationVersion+"/**")
        registry.addResourceHandler("/media-files/**")
                .addResourceLocations("file:" + mediaServerDirectory + File.separator)
                .resourceChain(false)
                .addResolver(new PathResourceResolver());

        registry.addResourceHandler("/channel-logos-"+applicationVersion+"/**")
                .addResourceLocations("file:" + logoFileDirectory + File.separator )
                .resourceChain(false)
                .addResolver(new PathResourceResolver());
    }

    @Bean
    public ITemplateResolver templateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML5");
        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(templateResolver());
        templateEngine.addDialect(new SpringSecurityDialect());
        return templateEngine;
    }

    @Bean
    public ViewResolver viewResolver() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        viewResolver.setOrder(1);
        return viewResolver;
    }

    @Bean(name = "simpleMappingExceptionResolver")
    public SimpleMappingExceptionResolver
    createSimpleMappingExceptionResolver() {
        SimpleMappingExceptionResolver r =
                new SimpleMappingExceptionResolver();
        r.setDefaultErrorView("error-page");
        return r;
    }

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        return new CommonsMultipartResolver();
    }

    @Bean(name = "specificTaskExecutor")
    public TaskExecutor specificTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(6);
        executor.initialize();
        return executor;
    }

    @Bean(name = "playListGenerationTaskExecutor")
    public TaskExecutor playListGenerationTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(8);
        executor.initialize();
        return executor;
    }

    @Bean(name = "dashboardTaskExecutor")
    public TaskExecutor dashboardTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(16);
        executor.initialize();
        return executor;
    }

    @Bean(name = "advertisementTaskExecutor")
    public TaskExecutor advertisementTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(12);
        executor.initialize();
        return executor;
    }

    @Bean(name = "billingServiceExecutor")
    public TaskExecutor billingServiceExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(12);
        executor.initialize();
        return executor;
    }


    @Bean(name = "dailyTaskExecutor")
    public TaskExecutor dailyTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.initialize();
        return executor;
    }

    @Bean(name = "woStatusCheckExecutor")
    public TaskExecutor woStatusCheckExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(6);
        executor.initialize();
        return executor;
    }
}