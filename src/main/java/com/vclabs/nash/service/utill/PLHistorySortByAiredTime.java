package com.vclabs.nash.service.utill;

import com.vclabs.nash.model.entity.PlayList;
import com.vclabs.nash.model.entity.PlayListHistory;

import java.util.Comparator;

/**
 * Created by dperera on 01/01/2020.
 */
public class PLHistorySortByAiredTime implements Comparator<PlayListHistory> {

    @Override
    public int compare(PlayListHistory o1, PlayListHistory o2) {
        return (int)( o1.getActualstarttime().getTime() - o2.getActualstarttime().getTime());
    }
}
