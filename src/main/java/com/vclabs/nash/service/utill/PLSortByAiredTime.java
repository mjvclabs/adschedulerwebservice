package com.vclabs.nash.service.utill;

import com.vclabs.nash.model.entity.PlayList;

import java.util.Comparator;

/**
 * Created by dperera on 01/01/2020.
 */
public class PLSortByAiredTime implements Comparator<PlayList> {

    @Override
    public int compare(PlayList o1, PlayList o2) {
        return (int)( o1.getActualStartTime().getTime() - o2.getActualStartTime().getTime());
    }
}
