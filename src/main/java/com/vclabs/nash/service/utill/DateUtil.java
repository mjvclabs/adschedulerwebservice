package com.vclabs.nash.service.utill;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Nalaka on 2018-09-26.
 */
public class DateUtil {

    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static SimpleDateFormat vsDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static LocalDate toLocalDate(Date date) {
        return date.toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date fromLocalDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date findDateAfter(int days) {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    public static String vsDateFormat(Date date){
        return vsDateFormat.format(date);
    }

    public static Timestamp getStartOfToday() {
        Date today = new Date();
        return new Timestamp(today.getYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);
    }
}
