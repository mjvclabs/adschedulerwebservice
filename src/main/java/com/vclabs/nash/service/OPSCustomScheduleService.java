package com.vclabs.nash.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.OPSResponseData;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.*;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Nalaka on 2019-03-21.
 */
@Service
@Transactional("main")
public class OPSCustomScheduleService {

    @Value("${ops.url.customschedule}")
    private String CUSTOM_SCHEDULE_URL;

    @Value("${file.advert.mediaDirectory}")
    private String mediaDirectory;

    @Autowired
    private OPSResponseDataService oPSResponseDataService;

    @Autowired
    private WorkOrderDAO workOrderDao;

    @Autowired
    private ClientDetailDAO clientDao;

    @Autowired
    private AdvertisementDAO advertisementDao;

    @Autowired
    private WorkOrderService workOrderService;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

    private static final Logger logger = LoggerFactory.getLogger(OPSCustomScheduleService.class);

    public int saveCustomDeal() throws ParseException {

        String plainCredentials = "nashadmin:Welcome1@";
        String base64Credentials = new String(Base64.encode(plainCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<OPSMainSchedule[]> response;

        RestTemplate restTemplate = new RestTemplate();
        //ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("http://localhost:8888/adhub/nash/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);
        //ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("https://192.168.1.99:8443/adhub/nash/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);
        //ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("https://123.231.117.122:8443/adhub/nash/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);
        response = restTemplate.exchange(CUSTOM_SCHEDULE_URL, HttpMethod.GET, request, OPSMainSchedule[].class);

        OPSResponseData opsrd = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            opsrd = oPSResponseDataService.saveCustonScheduleResponse(mapper.writeValueAsString(response.getBody()));
        } catch (JsonProcessingException e) {
            System.out.println("" + e);
        }

        List<OPSMainSchedule> searchList = Arrays.asList(response.getBody());

        for (OPSMainSchedule entry : searchList) {
            WorkOrder woModel = saveOPSWorkOrder(entry.getWorkOrder());
            System.out.print("wo save");
            saveOrderChannelList(getWorkOrderChannelList(entry.getScheduleList()), woModel);
            List<AdvertisementInfo> advertList = getAdvertList(entry.getScheduleList());
            saveCostomDealAdvert(advertList, woModel.getWorkorderid());
            saveCostomDealSchedule(entry.getScheduleList(), advertList, woModel.getWorkorderid());
        }
        if (opsrd != null) {
            opsrd.setStatus(OPSResponseData.Status.SUCCESS);
            oPSResponseDataService.updateResponseData(opsrd);
        }
        return searchList.size();
    }

    public WorkOrder saveOPSWorkOrder(WorkOrderInfo model) throws ParseException {
        SimpleDateFormat dta = new SimpleDateFormat("yyyy-MM-dd");// 00:00:00:000
        Date dt = new Date();
        Date startDate = dta.parse(model.getStartDate());
        Date endDate = dta.parse(model.getEndDate());

        WorkOrder workorder = new WorkOrder();

        workorder.setOrdername(model.getOrderName());
        workorder.setClient(model.getClient());
        workorder.setAgencyclient(model.getAgency());
        workorder.setBillclient(model.getBillClient());
        workorder.setBspotvisibility(model.getVisibility());
        workorder.setComment(model.getComment());

        workorder.setDate(dt);
        workorder.setStartdate(startDate);
        workorder.setEnddate(endDate);

        workorder.setPermissionstatus(0);

        workorder.setSeller(model.getUserName());
        workorder.setWoType(model.getWoType());
        workorder.setScheduleRef(model.getScheduleRef());
        workorder.setCommission(model.getCommission());
        workorder.setTotalBudget(model.getTotalBudget());

        workOrderDao.saveWorkOrder(workorder);

        return workorder;
    }

    public List<OrderChannelInfo> getWorkOrderChannelList(List<ScheduleInfo> scheduleList) {
        HashMap<Integer, HashMap<String, ChannelSpotInfo>> channelSpota = new HashMap();
        for (ScheduleInfo schedule : scheduleList) {
//            hm.put(schedul.getChannelId(), schedul.getAdvertDto().getAdverttype());
            HashMap<String, ChannelSpotInfo> spotList = channelSpota.get(schedule.getChannelId());

            if (spotList != null) {
                ChannelSpotInfo channelSpotModel = spotList.get(schedule.getAdvertDto().getAdverttype());
                if (channelSpotModel != null) {
                    channelSpotModel.setCount(channelSpotModel.getCount() + schedule.getAdvertCount());
                    channelSpotModel.setSpotType(schedule.getAdvertDto().getAdverttype());
                    if (schedule.getAdvertDto().getAdverttype().equals("TVC")) {

                        if (channelSpotModel.getTvcSpotCountList().isEmpty()) {
                            channelSpotModel.getTvcSpotCountList().put(schedule.getAdvertDto().getDuration() + "", schedule.getAdvertCount());
                        } else {
                            setTvcDurationArray(schedule, channelSpotModel);
                        }
                    }
                } else {
                    channelSpotModel = new ChannelSpotInfo();
                    channelSpotModel.setCount(channelSpotModel.getCount() + schedule.getAdvertCount());
                    channelSpotModel.setSpotType(schedule.getAdvertDto().getAdverttype());
                    if (schedule.getAdvertDto().getAdverttype().equals("TVC")) {

                        int count = 0;
                        if (channelSpotModel.getTvcSpotCountList().isEmpty()) {
                            channelSpotModel.getTvcSpotCountList().put(schedule.getAdvertDto().getDuration() + "", schedule.getAdvertCount());
                        } else {
                            setTvcDurationArray(schedule, channelSpotModel);
                        }
                    }
                    spotList.put(schedule.getAdvertDto().getAdverttype(), channelSpotModel);
                }
            } else {
                spotList = new HashMap();
                ChannelSpotInfo channelSpotModel = new ChannelSpotInfo();
                channelSpotModel.setCount(channelSpotModel.getCount() + schedule.getAdvertCount());
                channelSpotModel.setSpotType(schedule.getAdvertDto().getAdverttype());
                if (schedule.getAdvertDto().getAdverttype().equals("TVC")) {
                    int count = 0;
                    if (channelSpotModel.getTvcSpotCountList().isEmpty()) {
                        channelSpotModel.getTvcSpotCountList().put(schedule.getAdvertDto().getDuration() + "", schedule.getAdvertCount());
                    } else {
                        setTvcDurationArray(schedule, channelSpotModel);
                    }
                }
                spotList.put(schedule.getAdvertDto().getAdverttype(), channelSpotModel);
                channelSpota.put(schedule.getChannelId(), spotList);
            }
        }
        List<OrderChannelInfo> orderChannel = new ArrayList<>();
        for (Integer key : channelSpota.keySet()) {
            HashMap<String, ChannelSpotInfo> value = channelSpota.get(key);
            OrderChannelInfo orderModel = new OrderChannelInfo();
            orderModel.setChannelId(key);

            for (String avertType : value.keySet()) {
                ChannelSpotInfo channelSpotModel = value.get(avertType);
                if (channelSpotModel.getSpotType().equals("TVC")) {

                    orderModel.setTvc_numOfSpots(channelSpotModel.getCount());
                    int thirtySecondSpot = 0;
                    int spotDuration = 0;
                    for (String durationKey : channelSpotModel.getTvcSpotCountList().keySet()) {
                        orderModel.getTvcSpotCountList().put(durationKey + "", channelSpotModel.getTvcSpotCountList().get(durationKey));
                        spotDuration += channelSpotModel.getTvcSpotCountList().get(durationKey) * Integer.parseInt(durationKey);
                    }
//                    thirtySecondSpot = (int) Math.ceil(spotDuration / 30);
//                    orderModel.setTvc_numOfSpots(thirtySecondSpot);

                } else if (channelSpotModel.getSpotType().equals("Logo")) {
                    orderModel.setLogo_numOfSpots(channelSpotModel.getCount());
                } else {
                    orderModel.setCrowler_numOfSpots(channelSpotModel.getCount());
                }
            }
            orderChannel.add(orderModel);
        }

        return orderChannel;
    }

    public void setTvcDurationArray(ScheduleInfo schedule, ChannelSpotInfo channelSpotModel) {

        String duration = "" + schedule.getAdvertDto().getDuration();
        int count = channelSpotModel.getTvcSpotCountList().get(duration);
        count += schedule.getAdvertCount();
        channelSpotModel.getTvcSpotCountList().put(schedule.getAdvertDto().getDuration() + "", count);
    }

    public void saveOrderChannelList(List<OrderChannelInfo> list, WorkOrder model) {

        for (OrderChannelInfo orderChannelModel : list) {
            orderChannelModel.setWorkOrderId(model.getWorkorderid());
            workOrderService.setWorkOrderChannelList(orderChannelModel);
        }
    }

    public List<AdvertisementInfo> getAdvertList(List<ScheduleInfo> scheduleList) {
        List<AdvertisementInfo> advertList = new ArrayList<>();

        HashMap<Integer, AdvertisementInfo> advertMap = new HashMap();
        for (ScheduleInfo scheduleInfo : scheduleList) {
            scheduleInfo.getAdvertDto().setAdvertid(scheduleInfo.getAdvertId());
            advertMap.put(scheduleInfo.getAdvertDto().getAdvertid(), scheduleInfo.getAdvertDto());
        }

        for (int key : advertMap.keySet()) {
            AdvertisementInfo advertisementInfo = advertMap.get(key);
            advertisementInfo.setAdverttype(setNashAdvertType(advertisementInfo.getAdverttype()));
            advertList.add(advertisementInfo);
        }
        return advertList;
    }

    public String setNashAdvertType(String type) {
        if (type.equals("TVC")) {
            return "ADVERT";
        } else if (type.equals("Logo")) {
            return "LOGO";
        } else {
            return "CRAWLER";
        }
    }

    public void saveCostomDealAdvert(List<AdvertisementInfo> dataModelList, int workOrderId) {
        try {
            List<OrderAdvertisementInfo> orderAdverts = new ArrayList<>();
            for (AdvertisementInfo dataModel : dataModelList) {
                Advertisement advert = new Advertisement();

                advert.setAdvertname(dataModel.getAdvertname() + "_CS_Advert_ID_" + dataModel.getAdvertid());
                advert.setAdverttype(dataModel.getAdverttype());
                advert.setAdvertpath("");
                advert.setClient(clientDao.getSelectedClient(dataModel.getClient()));
                advert.setCommercialcategory(dataModel.getCommercialcategory());
                advert.setDuration(dataModel.getDuration());
                advert.setLanguage(dataModel.getLanguage());
                advert.setWidth(dataModel.getWidth());
                advert.setHight(dataModel.getHight());
                advert.setXposition(dataModel.getXposition());
                advert.setYposition(dataModel.getYposition());
                advert.setLogoContainerId(dataModel.getLogoContainer());
                advert.setCreateDate(new Date());
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                advert.setExpireDate(dateFormat.parse(dataModel.getSuspendDate()));
                advert.setSuspendDate(dateFormat.parse(dataModel.getSuspendDate()));

                advertisementDao.saveAdvertisement(advert);
                OrderAdvertisementInfo woAdvertModel = new OrderAdvertisementInfo();
                dataModel.setDbAdvertid(advert.getAdvertid());
                woAdvertModel.setAdvertId(advert.getAdvertid());
                woAdvertModel.setWorkOrderId(workOrderId);
                orderAdverts.add(woAdvertModel);

                String stMediaPath = mediaDirectory;
                if (advert.getAdverttype().equals("ADVERT")) {
                    stMediaPath += "/TVC";
                } else if (advert.getAdverttype().equals("LOGO")) {
                    stMediaPath += "/LOGO";
                } else if (advert.getAdverttype().equals("SLIDE")) {
                    stMediaPath += "/SLIDE";
                } else if (advert.getAdverttype().equals("CRAWLER")) {
                    stMediaPath += "/CRAWLER";
                } else if (advert.getAdverttype().equals("V_SHAPE")) {
                    stMediaPath += "/V_SHAPE";
                } else if (advert.getAdverttype().equals("L_SHAPE")) {
                    stMediaPath += "/L_SHAPE";
                } else if (advert.getAdverttype().equals("FILLER")) {
                    stMediaPath += "/FILLER";
                }

                String filePath = stMediaPath + System.getProperty("file.separator") + advert.getAdvertname() + "_" + advert.getAdvertid();
                advert.setAdvertpath(filePath);
                /*later update with minimum impact*/
                if (advert.getAdverttype().equals("LOGO_CONTAINER")) {
                    advert.setAdverttype("LOGO");
                    advert.setAdvertpath("LOGO_CONTAINER");
                    advert.setFileAvailable(true);
                }
                advertisementDao.updateAdvertisement(advert);
            }
            workOrderService.saveOrderAdvertList(workOrderId, orderAdverts);

        } catch (Exception e) {
            logger.debug("Exception in OPSCustomScheduleService in saveCostomDealAdvert() : {}", e.getMessage());
        }
    }

    public void saveCostomDealSchedule(List<ScheduleInfo> scheduleList, List<AdvertisementInfo> advertList, int workOrderId) {

        HashMap<Integer, Integer> advertIdMap = new HashMap<>();
        for (AdvertisementInfo advertisementInfo : advertList) {
            advertIdMap.put(advertisementInfo.getAdvertid(), advertisementInfo.getDbAdvertid());
        }

        for (ScheduleInfo scheduleInfo : scheduleList) {
            scheduleInfo.setWorkOrderId(workOrderId);
            scheduleInfo.setAdvertId(advertIdMap.get(scheduleInfo.getAdvertId()));
        }

        /*sort according to channelId*/
        Collections.sort(scheduleList, new Comparator<ScheduleInfo>() {

            @Override
            public int compare(ScheduleInfo o1, ScheduleInfo o2) {
                return o1.getChannelId() - o2.getChannelId(); //To change body of generated methods, choose Tools | Templates.
            }
        });

        /*sort according to channelId and advertId*/
        Collections.sort(scheduleList, new Comparator<ScheduleInfo>() {

            @Override
            public int compare(ScheduleInfo o1, ScheduleInfo o2) {
                return o1.getAdvertId() - o2.getAdvertId(); //To change body of generated methods, choose Tools | Templates.
            }
        });

        List<ScheduleInfo> tempArry = new ArrayList<>();
        int channelId = scheduleList.get(0).getChannelId();
        int advertId = scheduleList.get(0).getAdvertId();

        for (ScheduleInfo schedulInfo : scheduleList) {

            if (channelId == schedulInfo.getChannelId() && advertId == schedulInfo.getAdvertId()) {
                tempArry.add(schedulInfo);
            } else {
                channelId = schedulInfo.getChannelId();
                advertId = schedulInfo.getAdvertId();
                if (saveSchedule(tempArry)) {
                    tempArry.clear();
                    tempArry.add(schedulInfo);
                }
            }
        }
        saveSchedule(tempArry);
    }

    public Boolean saveSchedule(List<ScheduleInfo> scheduleList) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String stringValue = mapper.writeValueAsString(scheduleList);
            schedulerService.saveWOSchedule(stringValue);
            return true;
        } catch (JsonProcessingException e) {
            logger.debug("Exception in OPSCustomScheduleService in saveSchedule() : {}", e.getMessage());
            return false;
        }
    }
}
