package com.vclabs.nash.service.Mosh;

import com.vclabs.nash.model.entity.MoshUserLogDef;
import com.vclabs.nash.model.process.MoshDetails;

/**
 * Created by Nalaka on 2019-04-08.
 */
public interface MoshUserLogService {

    MoshUserLogDef save(MoshDetails moshDetails);

    MoshUserLogDef update(MoshDetails moshDetails);

    MoshUserLogDef getById(int id);
}
