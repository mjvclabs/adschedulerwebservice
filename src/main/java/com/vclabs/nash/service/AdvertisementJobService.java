package com.vclabs.nash.service;

import com.vclabs.nash.model.entity.Advertisement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional("main")
public class AdvertisementJobService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdvertisementJobService.class);

    @Autowired
    private AdvertisementService advertisementService;

    public void executeExpirationAndSuspendJob() {
        LOGGER.debug("Started advertisement expiration & suspend cron job | (cron = 0 31 23 1/1 * ?) | ( current time : {} )", new Date().toString());
        advertisementExpirationAndSuspend();
        LOGGER.debug("Completed advertisement expiration & suspend cron job | (cron = 0 31 23 1/1 * ?) | ( current time : {} )", new Date().toString());
    }

    private void advertisementExpirationAndSuspend(){
        try {
            Date currentTime = new Date();
            List<Advertisement> allAdverts = advertisementService.getAdvertisementsTobeExpiredOrSuspended(currentTime);
            for (Advertisement advertisement : allAdverts) {

                Date expireDate = advertisement.getExpireDate();
                Date suspendDate = advertisement.getSuspendDate();
                if (expireDate.before(new Date())) {
                    advertisement.setEnabled(0);
                    advertisement.setStatus(1);
                    if (advertisementService.updateAdvertisement(advertisement)) {
                        advertisementService.expireSchedule(advertisement);
                    }
                } else {
                    if (suspendDate.before(new Date())) {
                        advertisementService.suspendAdvert(advertisement);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in AdvertisementService in advertisementExpirationAndSuspend : {}", e.getMessage());
            throw e;
        }
    }

}
