/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.vclabs.nash.model.dao.AttachmentsDAO;
import com.vclabs.nash.model.entity.Attachments;

/**
 *
 * @author Sanira Nanayakkara
 */
@Service
@Transactional("main")
public class AttachmentService {

    @Value("${file.advert.attachment}")
    private String attachmentDirectory;

    @Value("${file.inventory.prediction.configuration_new}")
    private String INVENTORY_PREDICTION_CONFIGURATION_NEW_FILE_PATH;

    @Autowired
    private AttachmentsDAO attachmentsDao;

    private static final Logger logger = LoggerFactory.getLogger(AttachmentService.class);

    public List<Attachments> getAttachmentList(String table, int id) { //tested
        return attachmentsDao.getAttachmentList(table, id);
    }

    public int uploadAttachment(MultipartHttpServletRequest request) throws Exception {

        String stRefTable = "";
        String description = "No descripton";
        int iRefId;
        MultipartFile multiFile = null;
        File fAttachmentsDirectory = null;

        try {
            if (request.getParameter("ref_table") == null || request.getParameter("ref_id") == null) {
                return -1;
            }

            stRefTable = request.getParameter("ref_table");
            iRefId = Integer.parseInt(request.getParameter("ref_id"));
            if (request.getParameter("ref_table") != null) {
                description = request.getParameter("description");
            }

            Iterator<String> itrator = request.getFileNames();
            multiFile = request.getFile(itrator.next());

            fAttachmentsDirectory = new File(attachmentDirectory);
            if (!fAttachmentsDirectory.isDirectory()) {
                fAttachmentsDirectory.mkdirs();
            }

            /// saving the file => file name must be a generated unique one
            String stFileName;
            String stFilePath;
            String stFileType;

            String[] sptFilename = multiFile.getOriginalFilename().split("\\.");
            String stExtension = sptFilename[1].toLowerCase();
            stFileType = stExtension.toUpperCase();
            stFileName = sptFilename[0];

            Attachments pAttachment = new Attachments();
            pAttachment.setName(stFileName);
            pAttachment.setType(stFileType);
            pAttachment.setEnabled(false);
            pAttachment.setReferenceTable(stRefTable);
            pAttachment.setReferenceId(iRefId);
            pAttachment.setDescription(description);
            int iAttachmentId = attachmentsDao.insertAttachment(pAttachment);

            if (stRefTable.equals("csv")) {
                stFilePath = INVENTORY_PREDICTION_CONFIGURATION_NEW_FILE_PATH;
            } else {
                String stWebFileName = stFileName + "_" + String.valueOf(iAttachmentId) + "." + stExtension;
                stFilePath = fAttachmentsDirectory.getAbsolutePath() + System.getProperty("file.separator") + stWebFileName;
            }
            File flWebFilePath = new File(stFilePath);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(flWebFilePath));
            byte[] bytes = multiFile.getBytes();
            stream.write(bytes);
            stream.close();

            pAttachment.setFilePath(stFilePath);
            pAttachment.setEnabled(true);
            attachmentsDao.updateAttachment(pAttachment);

            //////////////////////////////////////////////
            return iAttachmentId;

        } catch (Exception e) {
            logger.debug("Exception in AttachmentService in uploadAttachment() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadAttachment(int id, HttpServletResponse response) {
        try {
            Attachments pAttachment = attachmentsDao.getAttachmentbyId(id);
            File file = new File(pAttachment.getFilePath());
            if (!file.exists()) {
                String errorMessage = "Sorry. The file you are looking for does not exist";
                OutputStream outputStream = response.getOutputStream();
                outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
                outputStream.close();
                return;
            }

//            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
//            if (mimeType == null) {
//                mimeType = "application/octet-stream";
//            }
            //set mime type to default to download
            String mimeType = "application/octet-stream";

            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
            response.setContentLength((int) file.length());
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            FileCopyUtils.copy(inputStream, response.getOutputStream());

        } catch (IOException ex) {
            logger.debug("Exception in AttachmentService in downloadAttachment() : {}", ex.getMessage());
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

    public boolean removeAttachment(int id) {
        Attachments pAttachment = attachmentsDao.getAttachmentbyId(id);
        return attachmentsDao.desableAttachment(pAttachment);
    }
}
