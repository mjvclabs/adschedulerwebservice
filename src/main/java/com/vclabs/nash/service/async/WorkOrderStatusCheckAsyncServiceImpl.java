package com.vclabs.nash.service.async;

import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.WorkOrderStatusCheckService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by dperera on 29/03/2019.
 */
@Service
public class WorkOrderStatusCheckAsyncServiceImpl implements WorkOrderStatusCheckAsyncService{

    @Autowired
    private WorkOrderStatusCheckService workOrderStatusCheckService;

    @Autowired
    private WorkOrderDAO workOrderDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkOrderStatusCheckAsyncServiceImpl.class);

    @Transactional
    @Async("dailyTaskExecutor")
    @Override
    public void processWorkOrders() {
        try {
            LOGGER.debug("Started processWorkOrders cron job | (cron = 0 15 0 1/1 * ?) | ( current time : {} )", new Date().toString());
            List<WorkOrder> allWorkOrderList = workOrderDao.getWorkOrderListForChackStatus();
            for (WorkOrder workOrder : allWorkOrderList) {
                workOrderStatusCheckService.checkAutoStatusAsync(workOrder);
            }
        }catch (Exception e){
            LOGGER.debug("Exception has occurred while executing processWorkOrders cron job, {}", e.getMessage());
        }
    }
}
