package com.vclabs.nash.service.async;

import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.service.SchedulerService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Created by dperera on 23/04/2019.
 */
@Service
public class ScheduleAsyncServiceImpl implements ScheduleAsyncService{

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ScheduleAsyncServiceImpl.class);

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

    @Override
    public int saveScheduleListAsync(List<ScheduleDef> scheduleDefList) throws ExecutionException, InterruptedException {
        Map<Integer, List<ScheduleDef>> scheduleMap = new HashMap<>();
        for(ScheduleDef sd : scheduleDefList){
            Integer channelId =  sd.getChannelid().getChannelid();
            if(!scheduleMap.containsKey(channelId)){
                scheduleMap.put(channelId, new ArrayList<>());
            }
            List<ScheduleDef> list = scheduleMap.get(channelId);
            list.add(sd);
        }

        List<CompletableFuture<Integer>> allFutures = new ArrayList<>();
        for(List<ScheduleDef> list : scheduleMap.values()){
            allFutures.add(schedulerService.saveScheduleGroupAsync(list));
        }

        try{
            CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
            int insertedScheduleCount = 0;
            for(CompletableFuture<Integer> future : allFutures){
                insertedScheduleCount += future.get();
            }
            return insertedScheduleCount;
        }catch (Exception e){
            LOGGER.debug("Exception has occurred while saving schedule group, {}", e.getMessage());
            throw e;
        }
    }
}
