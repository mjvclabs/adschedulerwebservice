package com.vclabs.nash.service;

import com.vclabs.nash.dashboard.dto.ChannelPredictionDto;
import com.vclabs.nash.model.dao.AdvertDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sanduni on 25/01/2019
 */
@Service
public class AdvertChannelService {

    @Autowired
    private AdvertDAO repository;

    public List<ChannelPredictionDto> findChannelPredictionData(){
        return repository.findAll();
    }
}
