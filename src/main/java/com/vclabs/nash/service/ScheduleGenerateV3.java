/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.vclabs.nash.model.entity.*;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Sanira Nanayakkara
 */
public class ScheduleGenerateV3 {

    int i_ChannelId;
    int i_ClusterCount;
    int i_hour;
    Date dt_StartTime;
    Date dt_EndTime;
    private ArrayList<ArrayList<PlayList>> lst_Schedule;
    private List<Advertisement> lst_Fillers;
    List<ScheduleDef> lst_Prioritys;
    List<PriorityDef> lst_PrioritieDefs;
    private int[] ar_FillerCount;
    private ChannelAdvertMapService channelAdvertMapService;
    private PlayListService playListService;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ScheduleGenerateV3.class);

    public ScheduleGenerateV3(int channel_id, int hour, Date start_time, Date end_time, int cluster_count, List<Advertisement> lstFillers,
                              ChannelAdvertMapService channelAdvertMapService, PlayListService playListService) {
        this.i_ChannelId = channel_id;
        this.i_ClusterCount = cluster_count;
        this.i_hour = hour;
        this.dt_StartTime = start_time;
        this.dt_EndTime = end_time;
        this.lst_Schedule = new ArrayList<>();
        for (int i = 0; i < i_ClusterCount; i++) {
            lst_Schedule.add(new ArrayList<>());
        }
        ar_FillerCount = new int[cluster_count];
        this.lst_Fillers = lstFillers;
        this.channelAdvertMapService = channelAdvertMapService;
        this.playListService = playListService;
    }

    public void setPrioritySchedulesList(List<ScheduleDef> lstScheduleList, List<PriorityDef> lstPriorities) {
        this.lst_Prioritys = lstScheduleList;
        this.lst_PrioritieDefs = lstPriorities;
    }

    //assume : schedules rounded to hours - no minuts
    public boolean generatePlayListAdvert(ArrayList<PlayList> lstPlayList) {

        ArrayList<PlayList> lstPriority_00 = new ArrayList<>(); //endtime_first_half_hour
        ArrayList<PlayList> lstPriority_01 = new ArrayList<>(); //endtime_this_hour
        ArrayList<PlayList> lstPriority_02 = new ArrayList<>(); //endtime_next_hour
        ArrayList<PlayList> lstPriority_03 = new ArrayList<>(); //endtime_other_hour
        ArrayList<PlayList> lstPriority_lh = new ArrayList<>(); //starttime_last_half_timbelt

        //set_conflicting cuts
        this.setSchedulesConflicts(lstPlayList);

        //set_adverts_priorities
        for (int iCluster = 0; iCluster < this.i_ClusterCount; iCluster++) {
            ArrayList<PlayList> lst_cluster = lst_Schedule.get(iCluster);
            int iPriorityCount = 0;
            for (PriorityDef item : lst_PrioritieDefs) {
                if (item.getCluster() == iCluster) {
                    iPriorityCount++;
                }
            }

            for (int iPriority = 1; iPriority <= iPriorityCount; iPriority++) {
                for (ScheduleDef pSchedule : lst_Prioritys) {
                    if (pSchedule.getPriority().getCluster() == iCluster && pSchedule.getPriority().getPriority() == iPriority) {
                        for (PlayList item : lstPlayList) {
                            if (item.getSchedule().getScheduleid().equals(pSchedule.getScheduleid())) {
                                item.setClusterPriority(iPriority);
                                lst_cluster.add(item);
                                lstPlayList.remove(item);
                                LOGGER.debug("Priority scheduled. ScheduleId={}, Icuster={}, Priority={} ", item.getSchedule().getScheduleid(), iCluster, iPriority);
                                break;
                            }
                        }
                    }
                }
            }
        }

        long hour_duration = dt_EndTime.getTime() - dt_StartTime.getTime();

        for (PlayList item : lstPlayList) {
            long available_duration = item.getScheduleEndTime().getTime() - dt_StartTime.getTime();
            long valid_duration = dt_EndTime.getTime() - item.getScheduleStartTime().getTime();

            if (valid_duration <= (hour_duration * 7) / 12) { //last 35_min
                lstPriority_lh.add(item);
            } else if (available_duration <= (hour_duration * 7) / 12) { //first 35_min
                lstPriority_00.add(item);
            } else if (available_duration <= hour_duration) {
                lstPriority_01.add(item);
            } else if (available_duration <= (hour_duration * 2)) {
                lstPriority_02.add(item);
            } else {
                lstPriority_03.add(item);
            }
        }
        //dialog req : schedule order must change daily
        Collections.shuffle(lstPriority_00);
        Collections.shuffle(lstPriority_01);
        Collections.shuffle(lstPriority_02);
        Collections.shuffle(lstPriority_03);
        Collections.shuffle(lstPriority_lh);

        //priority list 00 ===========================================================================================
        /**
         * TODO : schedule first half cuts(lstPriority_00) to first half of
         * clusters currently we schedule these cuts to first cluster only.
         */
        if (lstPriority_00.size() > 0) {
            ArrayList<PlayList> lst_cluster = lst_Schedule.get(0);

            PlayList previous_schedule = null;
            if (lst_cluster.size() > 0) {
                previous_schedule = lst_cluster.get(lst_cluster.size() - 1);
            }

            while (lstPriority_00.size() > 0) {
                PlayList tmp_item = null;
                if (previous_schedule == null) {//first_item
                    tmp_item = lstPriority_00.get(0);
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_00.remove(0);

                } else if ((null != (tmp_item = this.getMatchingSchedule(lstPriority_00, previous_schedule)))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_00.remove(tmp_item);

                } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_01, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_01.remove(tmp_item);

                } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_02, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_02.remove(tmp_item);

                } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_03, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_03.remove(tmp_item);

                } else {
                    tmp_item = getNewPlayListObject(previous_schedule);
                    previous_schedule = tmp_item;
                    if (tmp_item == null) {
                        continue;
                    }

                    tmp_item.setScheduleEndTime(previous_schedule.getScheduleEndTime());
                    tmp_item.setActualEndTime(previous_schedule.getActualEndTime());
                    lst_cluster.add(tmp_item);
                    ar_FillerCount[0]++;
                }
            }
        }

        //priority list 01 ===========================================================================================
        /**
         * these schedules aren't scheduled in last cluster becouse there will
         * be less no. of clusters than we expected.
         */
        if (lstPriority_01.size() > 0) {
            int iValidClusterCount = (i_ClusterCount <= 2) ? i_ClusterCount : i_ClusterCount - 1;
            int cluster_advert_count = ((lstPriority_01.size() + lst_Schedule.get(0).size() - ar_FillerCount[0] + 1) / iValidClusterCount);
            LOGGER.debug("Schedules for a cluster : {}", cluster_advert_count);

            for (int i = 0; i < iValidClusterCount; i++) {
                ArrayList<PlayList> lst_cluster = lst_Schedule.get(i);
                int tmp_advert_count = lst_cluster.size() - ar_FillerCount[i];
                PlayList previous_schedule = null;

                if (lst_cluster.size() > 0) {
                    previous_schedule = lst_cluster.get(lst_cluster.size() - 1);
                }
                if (i == (iValidClusterCount - 1)) { // scheduleing last cluster 
                    cluster_advert_count = tmp_advert_count + lstPriority_01.size();
                }
                int indexCounter = 0;
                while ((tmp_advert_count < cluster_advert_count) && (lstPriority_01.size() > 0)) {
                    PlayList tmp_item = null;
                    if (previous_schedule == null) { //first_item
                        tmp_item = lstPriority_01.get(0);
                        lstPriority_01.remove(0);
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        tmp_advert_count++;

                    } else if ((null != (tmp_item = this.getMatchingSchedule(lstPriority_01, previous_schedule)))) {
                        //check lap duration matching
                        if(isValidLapDuration(lst_cluster, tmp_item, lstPriority_01)){
                            lst_cluster.add(tmp_item);
                            previous_schedule = tmp_item;
                            lstPriority_01.remove(tmp_item);
                            tmp_advert_count++;
                            indexCounter = 0;
                        }else if(isSameAdvertPlayLists(lstPriority_01) || indexCounter == lstPriority_01.size()){
                            //if last ad is a filler, it should be removed + check conflicts with zero ads
                            PlayList zpl = getZeroAdvertisement(lst_cluster, i);
                            //update conflicts of remaining pls with zero ads + last added pl
                            lstPriority_01.add(zpl);
                            this.setSchedulesConflicts(lstPriority_01);
                            lst_cluster.add(zpl);
                            lstPriority_01.remove(zpl);
                            previous_schedule = zpl;
                           // tmp_advert_count++;
                            indexCounter = 0;
                        }else {
                            indexCounter++;
                        }
                    } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_02, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_02.remove(tmp_item);

                    } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_03, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_03.remove(tmp_item);

                    } else {
                        tmp_item = getNewPlayListObject(previous_schedule);
                        previous_schedule = tmp_item;
                        if (tmp_item == null) {
                            continue;
                        }

                        lst_cluster.add(tmp_item);
                        ar_FillerCount[i]++;
                    }
                }
            }

            /// if there is a missed advert /// Fail safty
            while (lstPriority_01.size() > 0) {
                PlayList tmp_item = lstPriority_01.get(0);
                lstPriority_01.remove(0);
                lst_Schedule.get(i_ClusterCount - 1).add(tmp_item);
            }
        }

        //priority list lh ===========================================================================================
        if (lstPriority_lh.size() > 0) {
            ArrayList<PlayList> lst_cluster = lst_Schedule.get(i_ClusterCount - 1);

            PlayList previous_schedule = null;
            if (lst_cluster.size() > 0) {
                previous_schedule = lst_cluster.get(lst_cluster.size() - 1);
            }

            while (lstPriority_lh.size() > 0) {
                PlayList tmp_item = null;
                if (previous_schedule == null) {//first_item
                    tmp_item = lstPriority_lh.get(0);
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_lh.remove(0);

                } else if ((null != (tmp_item = this.getMatchingSchedule(lstPriority_lh, previous_schedule)))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_lh.remove(tmp_item);

                } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_02, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_02.remove(tmp_item);

                } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_03, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_03.remove(tmp_item);

                } else {
                    tmp_item = getNewPlayListObject(previous_schedule);
                    previous_schedule = tmp_item;
                    if (tmp_item == null) {
                        continue;
                    }

                    tmp_item.setScheduleStartTime(previous_schedule.getScheduleStartTime());
                    tmp_item.setActualStartTime(previous_schedule.getActualStartTime());
                    lst_cluster.add(tmp_item);
                    ar_FillerCount[0]++;
                }
            }
        }

        //rest of adverts ===========================================================================================
        if (lstPriority_02.size() > 0 || lstPriority_03.size() > 0) {
            int total_scheduled = 0;
            for (int i = 0; i < i_ClusterCount; i++) {
                total_scheduled += lst_Schedule.get(i).size();
                total_scheduled -= ar_FillerCount[i];
            }
            int cluster_advert_count = ((lstPriority_02.size() + lstPriority_03.size() + total_scheduled + 1) / i_ClusterCount);

            for (int i = 0; i < i_ClusterCount; i++) {
                ArrayList<PlayList> lst_cluster = lst_Schedule.get(i);
                int tmp_advert_count = lst_cluster.size() - ar_FillerCount[i];;
                PlayList previous_schedule = null;

                if (lst_cluster.size() > 0) {
                    previous_schedule = lst_cluster.get(lst_cluster.size() - 1);
                }
                if (i == (i_ClusterCount - 1)) {
                    cluster_advert_count = tmp_advert_count + lstPriority_02.size() + lstPriority_03.size();
                }

                while (tmp_advert_count < cluster_advert_count && (lstPriority_02.size() > 0 || lstPriority_03.size() > 0)) {
                    PlayList tmp_item = null;

                    //first_item
                    if (previous_schedule == null) {
                        if (lstPriority_02.size() > 0) {
                            tmp_item = lstPriority_02.get(0);
                            lstPriority_02.remove(0);
                        } else if (lstPriority_03.size() > 0) {
                            tmp_item = lstPriority_03.get(0);
                            lstPriority_03.remove(0);
                        } else {
                            break;
                        }
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        tmp_advert_count++;

                    } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_02, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_02.remove(tmp_item);
                        tmp_advert_count++;

                    } else if (null != (tmp_item = this.getMatchingSchedule(lstPriority_03, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_03.remove(tmp_item);
                        tmp_advert_count++;

                    } else {
                        tmp_item = getNewPlayListObject(previous_schedule);
                        previous_schedule = tmp_item;
                        if (tmp_item == null) {
                            continue;
                        }

                        lst_cluster.add(tmp_item);
                        ar_FillerCount[i]++;
                    }
                }
            }
        }
        return true;
    }

    private PlayList getNewPlayListObject(PlayList previous) {
        PlayList pTmpFiller = getNewPlayListObject();
        pTmpFiller.setScheduleEndTime(previous.getTimeBeltEndTime());
        pTmpFiller.setScheduleStartTime(previous.getTimeBeltStartTime());
        pTmpFiller.setTimeBeltEndTime(previous.getTimeBeltEndTime());
        pTmpFiller.setTimeBeltStartTime(previous.getTimeBeltStartTime());

        return pTmpFiller;
    }

    //filler adverts
    private PlayList getNewPlayListObject() {
        if (lst_Fillers.size() <= 0) {
            return null;
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        int advertIndex = new Random().nextInt(lst_Fillers.size());

        PlayList playList = new PlayList();
        playList.setScheduleEndTime(this.dt_EndTime);
        playList.setScheduleStartTime(this.dt_StartTime);
        playList.setTimeBeltEndTime(this.dt_EndTime);
        playList.setTimeBeltStartTime(this.dt_StartTime);
        playList.setActualEndTime(this.dt_EndTime);
        playList.setActualStartTime(this.dt_StartTime);
        playList.setScheduleHour(this.i_hour);
        playList.setChannel(new ChannelDetails(this.i_ChannelId));
        Advertisement fAdvertisement = new Advertisement(lst_Fillers.get(advertIndex).getAdvertid());
        fAdvertisement.setDuration(lst_Fillers.get(advertIndex).getDuration());
        playList.setAdvert(fAdvertisement);
        playList.setSchedule(new ScheduleDef(1));
        playList.setWorkOrder(new WorkOrder(1));
        try {
            playList.setDate(format.parse(default_format.format(new Date())));
        } catch (ParseException ex) {
            Logger.getLogger(ScheduleGenerateV3.class.getName()).log(Level.SEVERE, null, ex);
        }
        playList.setStatus("0");
        playList.setPlayCluster(-1);
        playList.setPlayOrder(-1);
        playList.setRetryCount(0);
        playList.setComment("Filler");

        return playList;
    }

    public ArrayList<ArrayList<PlayList>> getLst_Schedule() {
        return lst_Schedule;
    }

// Conflicts_Resolving ==========================================================================
    private boolean setSchedulesConflicts(ArrayList<PlayList> lstPlayList) {
        for (PlayList item_1 : lstPlayList) {
            for (PlayList item_2 : lstPlayList) {
                if (this.resolve_SameClientConflicts(item_1, item_2)) {
                    item_1.addConflicting_schedules(item_2.getPlaylistId());
                    LOGGER.debug("[SameClientConflicts] => id :{} <-> {} => Client : {} <-> {} "
                            ,item_1.getPlaylistId(), item_2.getPlaylistId(), item_1.getAdvert().getClient(), item_2.getAdvert().getClient());
                }
                else if (this.resolve_SimilarAdvertConflicts(item_1, item_2)) {
                    item_1.addConflicting_schedules(item_2.getPlaylistId());
                    LOGGER.debug("[SimilarConflicts] => id :{} <-> {} => AdvertName : {} <-> {}"
                            ,item_1.getPlaylistId(), item_2.getPlaylistId(), item_1.getAdvert().getAdvertname(), item_2.getAdvert().getAdvertname());
                }
                else if (this.resolve_CategoryConflicts(item_1, item_2)) {
                    item_1.addConflicting_schedules(item_2.getPlaylistId());
                    LOGGER.debug("[CategoryConflicts] => id :{} <-> {} => Category : {} <-> {}"
                            ,item_1.getPlaylistId(), item_2.getPlaylistId(), item_1.getAdvert().getCommercialcategory(), item_2.getAdvert().getCommercialcategory());
                }
            }
        }
        return true;
    }

    private boolean resolve_SimilarAdvertConflicts(PlayList item_1, PlayList item_2) {
        if (item_1.getAdvert().getAdvertid() == item_2.getAdvert().getAdvertid()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean resolve_CategoryConflicts(PlayList item_1, PlayList item_2) {
        if ((item_1.getAdvert().getCommercialcategory().equals(item_2.getAdvert().getCommercialcategory()))) {
            //if (!((item_1.getAdvert().getClient().equals(item_2.getAdvert().getClient())))) {
            return true;
            //}
        }
        return false;
    }
    
    private boolean resolve_SameClientConflicts(PlayList item_1, PlayList item_2) {
        if (item_1.getAdvert().getClient().equals(item_2.getAdvert().getClient())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isSchedulesConflicting(PlayList playList_01, PlayList playList_02) {
        for (int item : playList_01.getConflicting_schedules()) {
            if (item == playList_02.getPlaylistId()) {
                return true;
            }
        }
        return false;
    }

    private PlayList getMatchingSchedule(ArrayList<PlayList> lstPlayList, PlayList previous_schedule) {
        for (PlayList item : lstPlayList) {
            if (!this.isSchedulesConflicting(previous_schedule, item)) {
                return item;
            }
        }
        return null;
    }

    private boolean isValidLapDuration(ArrayList<PlayList> cluster, PlayList currentList, ArrayList<PlayList> inputList){
        int minLapDuration = 60; //60s
        int lapDuration = getLapDuration(cluster, currentList);
        boolean validLapDuration = lapDuration != -1 ? (lapDuration >= minLapDuration) : true;
        if(!validLapDuration){
            inputList.remove(currentList); //to shift checked playlist to end of the list
            inputList.add(currentList);
        }
        return validLapDuration;
    }

    private int getLapDuration(ArrayList<PlayList> cluster, PlayList currentList){
        boolean hasMatchingAdvert = false;
        int lapDuration = 0;
        for(int i = cluster.size() - 1; i >= 0 ; i--){
            PlayList playList = cluster.get(i);
            if(playList.getAdvert().getAdvertid() == currentList.getAdvert().getAdvertid()){
                hasMatchingAdvert = true;
                break;
            }else {
                lapDuration += playList.getAdvert().getDuration();
            }
        }
        return hasMatchingAdvert ? lapDuration : -1;
    }

    private boolean isSameAdvertPlayLists(ArrayList<PlayList> playLists){
        if(playLists.size() == 0){
            return false;
        }
        Advertisement advertisement = playLists.get(0).getAdvert();
        for(PlayList pl : playLists){
            if(pl.getAdvert().getAdvertid() != advertisement.getAdvertid()){
                return false;
            }
        }
        return true;
    }

    private PlayList getZeroAdvertisement(ArrayList<PlayList> cluster, int j){
        PlayList la = null;  //last advertisement
        PlayList previousFiller = null;
        if(cluster.size() > 0){
            PlayList pa = cluster.get(cluster.size() - 1);
            if(pa.getPlaylistId() == null){
                previousFiller = pa;
            }
            for(int i = cluster.size() - 1; i >= 0; i--){
                la = cluster.get(i);
                if(la.getPlaylistId() != null){
                    break;
                }
            }
        }

        PlayList za = null;
        boolean isFillerNeeded = true;
        for(int i = 0; i <= 5 ; i++){
            za = getNextZeroAdvertisement(); //zero advertisement
            if(!this.resolve_SameClientConflicts(la, za) && !this.resolve_SimilarAdvertConflicts(la, za)
                    && !this.resolve_CategoryConflicts(la, za)){
                isFillerNeeded = false;
                break;
            }
        }

        if(!isFillerNeeded && previousFiller != null){
            cluster.remove(cluster.size() - 1);
            ar_FillerCount[j]--;
        }

        if(isFillerNeeded && previousFiller == null){
            cluster.add(getNewPlayListObject(la));
            ar_FillerCount[j]++;
        }
        return za;
    }

    private PlayList getNextZeroAdvertisement(){
        PlayList pl = channelAdvertMapService.getNextMinDurationPlayList(i_ChannelId);
        if(pl != null){
            pl.setScheduleHour(i_hour);
        }
        return pl;
    }

    public boolean generatePlayListCrawler(ArrayList<PlayList> lstPlayList) {

        ArrayList<PlayList> lstPriority_01 = new ArrayList<>(); //endtime_this_hour
        ArrayList<PlayList> lstPriority_1hf = new ArrayList<>(); //endtime_first_half_hour
        ArrayList<PlayList> lstPriority_2hf = new ArrayList<>(); //starttime_last_half_timbelt

        long lTimeDuration = dt_EndTime.getTime() - dt_StartTime.getTime();

        for (PlayList item : lstPlayList) {
            long available_duration = item.getScheduleEndTime().getTime() - dt_StartTime.getTime();
            long valid_duration = dt_EndTime.getTime() - item.getScheduleStartTime().getTime();

            if (valid_duration <= lTimeDuration / 2) { //last 30_min
                lstPriority_2hf.add(item);
            } else if (available_duration <= lTimeDuration / 2) { //first 30_min
                lstPriority_1hf.add(item);
            } else {
                lstPriority_01.add(item);
            }
        }

        ArrayList<PlayList> retList = new ArrayList<PlayList>();
        if (lstPriority_2hf.size() == 0 && lstPriority_1hf.size() == 0) {
            retList = lstPriority_01;
            retList = setPlayTimeCrawler(retList, lTimeDuration, dt_StartTime, dt_EndTime);
        } else {
            for (PlayList item : lstPriority_01) {
                if (lstPriority_2hf.size() >= lstPriority_1hf.size()) {
                    lstPriority_1hf.add(item);
                } else {
                    lstPriority_2hf.add(item);
                }
            }
            lstPriority_01.clear();
            lstPriority_1hf = setPlayTimeCrawler(lstPriority_1hf, lTimeDuration / 2, dt_StartTime, new Date(dt_StartTime.getTime() + (lTimeDuration / 2)));
            lstPriority_2hf = setPlayTimeCrawler(lstPriority_2hf, lTimeDuration / 2, new Date(dt_StartTime.getTime() + (lTimeDuration / 2)), dt_EndTime);
            for (PlayList item : lstPriority_1hf) {
                retList.add(item);
            }
            for (PlayList item : lstPriority_2hf) {
                retList.add(item);
            }
            lstPriority_1hf.clear();
            lstPriority_2hf.clear();
        }
        lstPlayList = retList;
        return true;
    }

    public ArrayList<PlayList> setPlayTimeCrawler(ArrayList<PlayList> lstPlayList, long lTimeDuration, Date dtStartTime, Date dtEndTime) {
        ArrayList<PlayList> lstReturn = shufflePlayListCrawler(lstPlayList);
        long lIntervalbetweenAdverts = lTimeDuration / (lstReturn.size() + 1);

        for (int i = 1; i <= lstReturn.size(); i++) {
            long lTmpTime = dtStartTime.getTime() + (lIntervalbetweenAdverts * i);
            Date dtPlayTime = new Date();
            dtPlayTime.setTime(lTmpTime);
            lstReturn.get(i - 1).setTimeBeltStartTime(dtPlayTime);
            lstReturn.get(i - 1).setTimeBeltEndTime(dtEndTime);
        }
        return lstReturn;
    }

    public ArrayList<PlayList> shufflePlayListCrawler(ArrayList<PlayList> lstPlayList) {
        if (lstPlayList.size() <= 0) {
            return lstPlayList;
        }

        HashMap<Integer, ArrayList<PlayList>> mapAdverts = new HashMap<Integer, ArrayList<PlayList>>();
        for (PlayList item : lstPlayList) {
            Integer id = item.getAdvert().getAdvertid();
            if (!mapAdverts.containsKey(id)) {
                mapAdverts.put(id, new ArrayList<PlayList>());
            }
            mapAdverts.get(id).add(item);
        }

        ArrayList<PlayList> lstRetList = new ArrayList<PlayList>();
        ArrayList<Integer> keys = new ArrayList<>(mapAdverts.keySet());
        Integer preId = -1;

        ///same advert /////
        if (keys.size() <= 1) {
            return lstPlayList;
        }

        Collections.sort(keys, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return mapAdverts.get(o2).size() - mapAdverts.get(o1).size();
            }
        });

        while (mapAdverts.get(keys.get(0)).size() > 0) {
            if (keys.get(0).equals(preId) && mapAdverts.get(keys.get(1)).size() > 0) {
                PlayList plItem = mapAdverts.get(keys.get(1)).get(0);
                mapAdverts.get(keys.get(1)).remove(plItem);
                lstRetList.add(plItem);
                preId = keys.get(1);
            } else {
                PlayList plItem = mapAdverts.get(keys.get(0)).get(0);
                mapAdverts.get(keys.get(0)).remove(plItem);
                lstRetList.add(plItem);
                preId = keys.get(0);
            }
            Collections.sort(keys, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return mapAdverts.get(o2).size() - mapAdverts.get(o1).size();
                }
            });
        }

        return lstRetList;
    }
}
