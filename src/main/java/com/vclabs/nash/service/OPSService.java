package com.vclabs.nash.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.OPSResponseData;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;


@Service
@Transactional("main")
public class OPSService {

    @Value("${ops.url.hotdeal}")
    private String HOT_DEAL_URL;

    @Autowired
    private WorkOrderDAO workOrderDao;

    @Autowired
    private WorkOrderService workOrderService;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private ClientDetailDAO clientDao;

    @Autowired
    private OPSResponseDataService oPSResponseDataService;

    //@Scheduled(fixedDelay=(1000*60*60*3))

    private static final Logger logger = LoggerFactory.getLogger(OPSService.class);

    public int saveWorkOrder() throws ParseException {

        String plainCredentials = "nashadmin:Welcome1@";
        String base64Credentials = new String(Base64.encode(plainCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> request = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange(HOT_DEAL_URL, HttpMethod.GET, request, OPSMainSchedule[].class);
        //ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("https://192.168.1.99:8443/adhub/nash/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);
        //ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("https://123.231.117.122:8443/adhub/nash/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);

        OPSResponseData opsrd = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            opsrd = oPSResponseDataService.saveHotDealResponse(mapper.writeValueAsString(response.getBody()));
        } catch (JsonProcessingException e) {
            System.out.println("" + e);
        }

        List<OPSMainSchedule> searchList = Arrays.asList(response.getBody());

        for (OPSMainSchedule entry : searchList) {
            WorkOrder woModel = saveOPSWorkOrder(entry.getWorkOrder());
            saveOrderChannelList(entry.getChannelList(), woModel);
            saveAdvertisement(entry.getAdvertList(), woModel.getWorkorderid());
            saveSchedule(entry.getScheduleList(), entry.getAdvertList(), woModel.getWorkorderid());
        }
        return searchList.size();
    }

    public WorkOrder saveOPSWorkOrder(WorkOrderInfo model) throws ParseException {
        SimpleDateFormat dta = new SimpleDateFormat("yyyy-MM-dd");// 00:00:00:000
        Date dt = new Date();
        Date startDate = dta.parse(model.getStartDate());
        Date endDate = dta.parse(model.getEndDate());

        WorkOrder workorder = new WorkOrder();

        workorder.setOrdername(model.getOrderName());
        workorder.setClient(model.getClient());
        workorder.setAgencyclient(model.getAgency());
        workorder.setBillclient(model.getBillClient());
        workorder.setBspotvisibility(model.getVisibility());
        workorder.setComment(model.getComment());

        workorder.setDate(dt);
        workorder.setStartdate(startDate);
        workorder.setEnddate(endDate);

        workorder.setPermissionstatus(0);
        workorder.setAutoStatus(WorkOrder.AutoStatus.Initial);
        workorder.setManualStatus(WorkOrder.ManualStatus.Initial);
        workorder.setSystemTypeype(WorkOrder.SystemType.OPS);

        workorder.setSeller(model.getUserName());
        workorder.setWoType(model.getWoType());
        workorder.setScheduleRef(model.getScheduleRef());
        workorder.setCommission(model.getCommission());
        workorder.setTotalBudget(model.getTotalBudget());

        workOrderDao.saveWorkOrder(workorder);

        return workorder;
    }

    public void saveOrderChannelList(List<OrderChannelInfo> list, WorkOrder model) {

        for (OrderChannelInfo orderChannelModel : list) {
            orderChannelModel.setWorkOrderId(model.getWorkorderid());
            workOrderService.setWorkOrderChannelList(orderChannelModel);
        }
    }

    public void saveAdvertisement(List<AdvertisementInfo> dataModelList, int workOrderId) {

        try {
            List<OrderAdvertisementInfo> orderAdverts = new ArrayList<>();
            for (AdvertisementInfo dataModel : dataModelList) {
                //Advertisement advert = new Advertisement();

                /*advert.setAdvertname(dataModel.getAdvertname());
                advert.setAdverttype(dataModel.getAdverttype());
                advert.setAdvertpath("");
                advert.setClient(clientDao.getSelectedClient(dataModel.getClient()));
                advert.setCommercialcategory(dataModel.getCommercialcategory());
                advert.setDuration(dataModel.getDuration());
                advert.setLanguage(dataModel.getLanguage());
                advert.setWidth(dataModel.getWidth());
                advert.setHight(dataModel.getHight());
                advert.setXposition(dataModel.getXposition());
                advert.setYposition(dataModel.getYposition());
                advert.setLogoContainerId(dataModel.getLogoContainer());
                advert.setCreateDate(new Date());
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                advert.setExpireDate(dateFormat.parse(dataModel.getExpireDate()));
                advert.setSuspendDate(dateFormat.parse(dataModel.getSuspendDate()));

                advertisementDao.saveAdvertisement(advert);
                OrderAdvertisementInfo woAdvertModel = new OrderAdvertisementInfo();
                dataModel.setAdvertid(advert.getAdvertid());*/
                OrderAdvertisementInfo woAdvertModel = new OrderAdvertisementInfo();
                woAdvertModel.setAdvertId(dataModel.getAdvertid());
                woAdvertModel.setWorkOrderId(workOrderId);
                orderAdverts.add(woAdvertModel);

                /*String stMediaPath = MediaDirectory;
                if (advert.getAdverttype().equals("ADVERT")) {
                    stMediaPath += "/TVC";
                } else if (advert.getAdverttype().equals("LOGO")) {
                    stMediaPath += "/LOGO";
                } else if (advert.getAdverttype().equals("SLIDE")) {
                    stMediaPath += "/SLIDE";
                } else if (advert.getAdverttype().equals("CRAWLER")) {
                    stMediaPath += "/CRAWLER";
                } else if (advert.getAdverttype().equals("V_SHAPE")) {
                    stMediaPath += "/V_SHAPE";
                } else if (advert.getAdverttype().equals("L_SHAPE")) {
                    stMediaPath += "/L_SHAPE";
                } else if (advert.getAdverttype().equals("FILLER")) {
                    stMediaPath += "/FILLER";
                }

                String filePath = stMediaPath + System.getProperty("file.separator") + advert.getAdvertname() + "_" + String.valueOf(advert.getAdvertid());
                advert.setAdvertpath(filePath);*///+"\\"+dataModel.getAdvertname()+".mpg");

                /// later update with minimum impact
                /*if (advert.getAdverttype().equals("LOGO_CONTAINER")) {
                    advert.setAdverttype("LOGO");
                    advert.setAdvertpath("LOGO_CONTAINER");
                    advert.setFileAvailable(true);
                }
                advertisementDao.updateAdvertisement(advert);*/
            }
            workOrderService.saveOrderAdvertList(workOrderId, orderAdverts);

        } catch (Exception e) {
            logger.debug("Exception in OPSService in saveAdvertisement() : {}", e.getMessage());
        }
    }

    public void saveSchedule(List<ScheduleInfo> scheduleList, List<AdvertisementInfo> dataModelList, int workOrderId) {
        if (!dataModelList.isEmpty()) {
            //sort according to channelId
            /*Collections.sort(schedulList, new Comparator<ScheduleDef>() {
             @Override
             public int compare(ScheduleDef o1, ScheduleDef o2) {
             return o1.getWorkorderid().getWorkorderid() - o2.getWorkorderid().getWorkorderid();
             }
             });*/
            Collections.sort(scheduleList, new Comparator<ScheduleInfo>() {

                @Override
                public int compare(ScheduleInfo o1, ScheduleInfo o2) {
                    return o1.getChannelId() - o2.getChannelId(); //To change body of generated methods, choose Tools | Templates.
                }
            });
            List<ScheduleInfo> tempArry = new ArrayList<>();

            int numOfAdvert = dataModelList.size();
            int advertCount = 0;

            //set advert
            for (ScheduleInfo schedulInfo : scheduleList) {
                if (advertCount >= numOfAdvert) {
                    advertCount = 0;
                }
                schedulInfo.setWorkOrderId(workOrderId);
                schedulInfo.setAdvertId(dataModelList.get(advertCount).getAdvertid());
                advertCount++;
            }

            //sort according to channelId and advertId
            //Collections.sort(scheduleList);
            Collections.sort(scheduleList, new Comparator<ScheduleInfo>() {

                @Override
                public int compare(ScheduleInfo o1, ScheduleInfo o2) {
                    return o1.getAdvertId() - o2.getAdvertId(); //To change body of generated methods, choose Tools | Templates.
                }
            });
            int channelId = scheduleList.get(0).getChannelId();
            int advertId = scheduleList.get(0).getAdvertId();

            for (ScheduleInfo schedulInfo : scheduleList) {

                if (channelId == schedulInfo.getChannelId() && advertId == schedulInfo.getAdvertId()) {
                    tempArry.add(schedulInfo);
                } else {
                    channelId = schedulInfo.getChannelId();
                    advertId = schedulInfo.getAdvertId();
                    if (saveSchedule(tempArry)) {
                        tempArry.clear();
                        tempArry.add(schedulInfo);
                    }
                }
            }
            saveSchedule(tempArry);
        }
    }

    public Boolean saveSchedule(List<ScheduleInfo> scheduleList) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String stringValue = mapper.writeValueAsString(scheduleList);
            schedulerService.saveWOSchedule(stringValue);
            return true;
        } catch (JsonProcessingException e) {
            logger.debug("Exception in OPSService in saveSchedule() : {}", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}
