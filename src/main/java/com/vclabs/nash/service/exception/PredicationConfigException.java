package com.vclabs.nash.service.exception;

/**
 * Created by dperera on 18/10/2018.
 */
public class PredicationConfigException extends NashException {

    public PredicationConfigException(String message) {
        super(message);
    }

    public PredicationConfigException(Throwable cause) {
        super(cause);
    }
}
