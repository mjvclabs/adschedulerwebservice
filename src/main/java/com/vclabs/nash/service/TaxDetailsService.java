package com.vclabs.nash.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.TaxDAO;
import com.vclabs.nash.model.entity.TaxDef;

/**
 * @author Sanira Nanayakkara
 */

@Service
@Transactional("main")
public class TaxDetailsService {

    @Autowired
    private TaxDAO taxDetailsDao;
    
    public List<TaxDef> getAllTaxDetails() {
        return taxDetailsDao.getAllTaxDetails();
    }
    
    public boolean updateTaxDetails(List<TaxDef> modelList){
        for (TaxDef model : modelList) {
            taxDetailsDao.updateTaxDetails(model);
        }
        return true;
    }
}
