package com.vclabs.nash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.ZeroAdsSettingDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ZeroAdsSetting;
import com.vclabs.nash.model.process.ZeroAdsSettingInfo;
import com.vclabs.nash.model.view.ManualChannelAdvertMapView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-18.
 */
@Service
@Transactional("main")
public class ZeroAdsSettingService {

    @Autowired
    private ZeroAdsSettingDAO zeroAdsSettingDao;

    @Lazy
    @Autowired
    private ChannelAdvertMapService channelAdvertMapService;

    public ZeroAdsSetting setSettinglAsAutoPlayList(int channelId) {

        ChannelDetails channelDetail = new ChannelDetails();
        channelDetail.setChannelid(channelId);

        ZeroAdsSetting zeroAdsSetting = new ZeroAdsSetting();
        zeroAdsSetting.setChannelId(channelDetail);
        zeroAdsSetting.setpListType(ZeroAdsSetting.PlayListType.AUTOPLAYLIST);
        zeroAdsSettingDao.save(zeroAdsSetting);

        return zeroAdsSetting;
    }

    public ZeroAdsSetting setSettinglAsManualPlayList(int channelId) {

        ChannelDetails channelDetail = new ChannelDetails();
        channelDetail.setChannelid(channelId);

        ZeroAdsSetting zeroAdsSetting = new ZeroAdsSetting();
        zeroAdsSetting.setChannelId(channelDetail);
        zeroAdsSetting.setpListType(ZeroAdsSetting.PlayListType.MANUALPLAYLIST);
        zeroAdsSettingDao.save(zeroAdsSetting);

        return zeroAdsSetting;
    }

    public ZeroAdsSetting getSelectedChannelSetting(int channelId) {
        return zeroAdsSettingDao.findByChannelId(channelId);
    }

    public List<ZeroAdsSetting> getZeroAdsSetting() {
        return zeroAdsSettingDao.findAll();
    }

    public Boolean saveZeroAdsSetting(String data) throws IOException {

        Boolean settingStatus = true;
        List<ZeroAdsSettingInfo> settingList = null;
        ObjectMapper mapper = new ObjectMapper();
        settingList = mapper.readValue(data, new TypeReference<List<ZeroAdsSettingInfo>>() {
        });

        for (ZeroAdsSettingInfo zeroAdsSettingInfo : settingList) {
            ZeroAdsSetting zeroAdsSetting = zeroAdsSettingDao.findByChannelId(zeroAdsSettingInfo.getChannelId());
            if (!zeroAdsSettingInfo.getpListType().equals(zeroAdsSetting.getpListType().toString())) {
                channelAdvertMapService.clearPalyLis(zeroAdsSetting.getChannelId().getChannelid());
            }
            zeroAdsSetting.setpListType(ZeroAdsSetting.PlayListType.valueOf(zeroAdsSettingInfo.getpListType()));
            if (zeroAdsSetting.getpListType() == ZeroAdsSetting.PlayListType.MANUALPLAYLIST) {
                List<ManualChannelAdvertMapView> manualPlayList = channelAdvertMapService.getZeroAdsManualPlayList(zeroAdsSetting.getChannelId().getChannelid());
//                if (manualPlayList.isEmpty()) {
//                    settingStatus = false;
//                    break;
//                }
            }
            zeroAdsSettingDao.update(zeroAdsSetting);
        }
        return settingStatus;
    }
}
