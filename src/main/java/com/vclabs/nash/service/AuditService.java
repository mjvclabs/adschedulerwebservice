/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.vclabs.nash.model.dao.GeneralAuditDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.entity.GeneralAudit;
import com.vclabs.nash.model.entity.ScheduleDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vclabs.nash.model.dao.WorkOrderAuditDAO;
import com.vclabs.nash.model.entity.WorkOrderAudit;
import com.vclabs.nash.model.view.WorkOrderAuditView;
import com.vclabs.nash.model.view.reporting.SpotAuditView;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class AuditService {

    @Autowired
    private WorkOrderAuditDAO workOrderAuditDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    private SchedulerDAO schedulerDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    public List<WorkOrderAuditView> getWorkOrderAudit(int workOrderId) {
        try {
            List<WorkOrderAudit> auditList = workOrderAuditDao.getSelectedAudit(workOrderId);
            List<WorkOrderAuditView> auditViewList = new ArrayList<>();
            for (WorkOrderAudit workOrderAudit: auditList) {
                WorkOrderAuditView viewModel = new WorkOrderAuditView();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
                viewModel.setDate(df.format(workOrderAudit.getDateAndTime()));
                viewModel.setOldData(workOrderAudit.getOldData());
                viewModel.setNewData(workOrderAudit.getNewData());
                viewModel.setUser(workOrderAudit.getUser());
                if (workOrderAudit.getTablename().equals("work_order")) {
                    viewModel.setWorkOrderTableField(workOrderAudit.getField());
                } else if (workOrderAudit.getTablename().equals("work_order_channel_list")) {
                    viewModel.setWorkOrderChannelListTableField(workOrderAudit.getField());
                }
                auditViewList.add(viewModel);
            }
            return auditViewList;

        } catch (Exception e) {
            LOGGER.debug("Exception in AudioService while trying to get Work Order Audit List: ", e.getMessage());
            throw e;
        }
    }

    public List<SpotAuditView> getScheduleAudit(int workOrderId) {
        ArrayList<String> scheduleIdArray = new ArrayList<String>();
        List<ScheduleDef> scheduleList = schedulerDao.getAllScheduleInWorkOrder(workOrderId);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Map<String, List<GeneralAudit>> map_auditLog = new TreeMap<>();

        for (ScheduleDef scheduleDef : scheduleList) {
            scheduleIdArray.add("" + scheduleDef.getScheduleid());
        }

        List<GeneralAudit> auditList = new ArrayList<>();

        if(scheduleIdArray.size()>0){
            auditList = generalAuditDao.getSelectedSchedulesRecodes(scheduleIdArray);
        }

        for (GeneralAudit generalAudit : auditList) {

            List<GeneralAudit> temList = map_auditLog.get(df.format(generalAudit.getDateAndTime()));
            if (temList == null) {
                temList = new ArrayList<>();
                temList.add(generalAudit);
                map_auditLog.put(df.format(generalAudit.getDateAndTime()), temList);
            } else {
                temList.add(generalAudit);
            }
        }

        List<SpotAuditView> audiViewList = new ArrayList<>();

        // using for-each loop for iteration over Map.entrySet()
        for (Map.Entry<String, List<GeneralAudit>> entry : map_auditLog.entrySet()) {
            SpotAuditView spotAuditView = new SpotAuditView();
            spotAuditView.setTime(entry.getKey());
            spotAuditView.setSpotCount(entry.getValue().size());
            List<GeneralAudit> slectedAuditList = entry.getValue();
            int scheduleCount = 0;
            int playedCount = 0;
            int invalidCount = 0;
            int removeCount = 0;
            int stopAndReschedule = 0;
            int startTimeChange = 0;
            int endTimeChange = 0;
            int missedSpot=0;
            int replaseAdvertcount = 0;
            int replaseAdvertCommentCount=0;
            int channelChangeCount = 0;
            int priorityCount = 0;
            for (GeneralAudit generalAudit : slectedAuditList) {
                if (generalAudit.getField().equals("Status")) {
                    if (generalAudit.getOldData().equals("0") && generalAudit.getNewData().equals("1")) {
                        scheduleCount++;
                        spotAuditView.setAction(scheduleCount + " change to scheduled statuus");
                        spotAuditView.setSpotCount(scheduleCount);
                    } else if (generalAudit.getOldData().equals("1") && generalAudit.getNewData().equals("2")) {
//                        playedCount++;
//                        spotAuditView.setAction(playedCount + " change to Aired statuus");
//                        spotAuditView.setSpotCount(playedCount);
                    } else if (generalAudit.getOldData().equals("0") && generalAudit.getNewData().equals("8")) {
//                        invalidCount++;
//                        spotAuditView.setAction(invalidCount + " change to Invalid statuus");
//                        spotAuditView.setSpotCount(invalidCount);
                    } else if (generalAudit.getOldData().equals("0") && generalAudit.getNewData().equals("6")) {
                        removeCount++;
                        spotAuditView.setAction(removeCount + " change to remove statuus");
                        spotAuditView.setSpotCount(removeCount);
                    } else if (generalAudit.getOldData().equals("2") && generalAudit.getNewData().equals("1")) {
//                        stopAndReschedule++;
//                        spotAuditView.setAction(" Stop and retry");
//                        spotAuditView.setSpotCount(stopAndReschedule);
                    }else if (generalAudit.getOldData().equals("1") && generalAudit.getNewData().equals("5")){
                        missedSpot++;
                        spotAuditView.setAction(" Change to missed status");
                        spotAuditView.setSpotCount(missedSpot);
                        //change to missed status
                    }else if (generalAudit.getOldData().equals("5") && generalAudit.getNewData().equals("1")){
                      //Auto reschedule to next hour
                    }else if (generalAudit.getOldData().equals("5") && generalAudit.getNewData().equals("2")){
                        //Auto play missed spot
                    }else if(generalAudit.getOldData().equals("0 by nash")){
                        //set audit by nash
                    }
                    else {
                        String action = spotAuditView.getAction();
                        if (action != null) {
                            action = action + " - " + generalAudit.getOldData() + " - " + generalAudit.getNewData();
                            spotAuditView.setAction(action);
                        }
                    }
                } else if (generalAudit.getField().equals("Schedule_start_time")) {
                    startTimeChange++;
                    spotAuditView.setAction("Start time change from " + generalAudit.getOldData() + " to " + generalAudit.getNewData());
//                    spotAuditView.setSpotCount(startTimeChange);

                } else if (generalAudit.getField().equals("Schedule_end_time")) {
//                    if (!spotAuditView.getAction().equals("")) {
//                        audiViewList.add(spotAuditView);
//                        spotAuditView = new SpotAuditView();
//                        spotAuditView.setTime(entry.getKey());
//                    }Comment
//                    endTimeChange++;
                    spotAuditView.setAction("End time change from " + generalAudit.getOldData() + " to " + generalAudit.getNewData());
//                    spotAuditView.setSpotCount(endTimeChange);
                } else if (generalAudit.getField().equals("Advert_id")) {
                    replaseAdvertcount++;
                    spotAuditView.setAction("Replace advertisement");
                    spotAuditView.setSpotCount(replaseAdvertcount);
                } else if(generalAudit.getField().equals("Comment")){
                    if(generalAudit.getNewData().endsWith("repalced for")){
                        replaseAdvertCommentCount++;
                        spotAuditView.setAction("Replace advertisement");
                        spotAuditView.setSpotCount(replaseAdvertCommentCount);
                    }

                } else if(generalAudit.getField().equals("created_date")) {

                }else if(generalAudit.getField().equals("Channel_id")) {
                    channelChangeCount++;
                    spotAuditView.setAction("Change channel");
                    spotAuditView.setSpotCount(channelChangeCount);
                }else if(generalAudit.getField().equals("Priority_id")){
                    priorityCount++;
                    spotAuditView.setAction("Set Priority");
                    spotAuditView.setSpotCount(priorityCount);
                } else if(generalAudit.getField().equals("Actual_start_time")||generalAudit.getField().equals("Actual_end_time")) {
                    //Actual time set by system
                }
                else {
                    String action = spotAuditView.getAction();
                    action = action + " - " + generalAudit.getOldData() + " - " + generalAudit.getNewData() + "_Not stattus";
                    spotAuditView.setAction(action);
                }
            }
            if(spotAuditView.getAction()!=null) {
                audiViewList.add(spotAuditView);
            }
        }
        return audiViewList;
    }

}