/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service;

import com.vclabs.nash.dashboard.service.*;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dao.WorkOrderChannelListDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.dto.ScheduleAdvertDto;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TVCDurationSpotCount;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import com.vclabs.nash.model.view.WorkOrderChannelScheduleView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 *
 * @author hp
 */
@Service
@Transactional("main")
public class WorkOrderStatusCheckService {

    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private WorkOrderChannelListDAO workOrderChannelListDao;
    @Autowired
    private WorkOrderService workOrderService;
//    @Autowired
//    private NewWorkOrderWidgetService newWorkOrderWidgetService;
//    @Autowired
//    private MyToDoListWidgetService myToDoListWidgetService;
//    @Autowired
//    MyTeamToDoListWidgetService myTeamToDoListWidgetService;
//    @Autowired
//    private DummyCutWidgetService dummyCutWidgetService;
//
//    @Autowired
//    private PendingWorkOrderWidgetService pendingWorkOrderWidgetService;
//    @Autowired
//    private PartiallyEnteredWorkOrderWidgetService partiallyEnteredWorkOrderWidgetService;

    private static final Logger logger = LoggerFactory.getLogger(WorkOrderStatusCheckService.class);

   // @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Async("woStatusCheckExecutor")
    public void checkAutoStatusAsync(WorkOrder workOrder){
        this.checkAutoStatus(workOrder);
        logger.debug("Checked status of work-order : {}", workOrder.getWorkorderid());
    }

    public Boolean checkWorkOrderStatus(WorkOrder workOrder) {
        return checkAutoStatus(workOrder);
    }

    public Boolean checkAutoStatus(WorkOrder workOrder) {

        try {
            WorkOrderChannelScheduleView model = this.getScheduleAvailableSpotCount(workOrder.getWorkorderid(), -999);
            List<ScheduleAdvertDto> scheduleList = schedulerDao.getScheduleList(workOrder.getWorkorderid());
            int totalTVCDuration = 0;
            int totalTVCAiredDuration = 0;
            int totalTVCCount = 0;
            int totalTVCAiredCount = 0;

            int totalLOGOCount = 0;
            int totalLOGOAiredCount = 0;

            int totalCRAWLERCount = 0;
            int totalCRAWLERAiredCount = 0;

            int totalVSHAPECount = 0;
            int totalVSHAPEAiredCount = 0;

            int totalLSHAPECount = 0;
            int totalLSHAPEAiredCount = 0;

            for (ScheduleAdvertDto schedule : scheduleList) {

                switch (schedule.getAdvertType()) {
                    case "ADVERT":
                        totalTVCDuration += schedule.getDuration();
                        totalTVCCount = totalTVCDuration / 30;
                        if ((totalTVCDuration % 30) != 0) {
                            totalTVCCount++;
                        }
                        if (schedule.getStatus().equals("2")) {
                            totalTVCAiredDuration += schedule.getDuration();
                            totalTVCAiredCount = totalTVCAiredDuration / 30;
                            if ((totalTVCAiredDuration % 30) != 0) {
                                totalTVCAiredCount++;
                            }
                        }
                        break;

                    case "LOGO":
                        totalLOGOCount++;
                        if (schedule.getStatus().equals("2")) {
                            totalLOGOAiredCount++;
                        }
                        break;

                    case "CRAWLER":
                        totalCRAWLERCount++;
                        if (schedule.getStatus().equals("2")) {
                            totalCRAWLERAiredCount++;
                        }
                        break;

                    case "V_SHAPE":
                        totalVSHAPECount++;
                        if (schedule.getStatus().equals("2")) {
                            totalVSHAPEAiredCount++;
                        }
                        break;

                    case "L_SHAPE":
                        totalLSHAPECount++;
                        if (schedule.getStatus().equals("2")) {
                            totalLSHAPEAiredCount++;
                        }
                        break;

                    default:
                        break;
                }

            }

            if (workOrder.getAutoStatus() == WorkOrder.AutoStatus.Initial &&(totalTVCCount > 0 || totalLOGOCount > 0 || totalCRAWLERCount > 0 || totalVSHAPECount > 0 || totalLSHAPECount > 0)) {
                workOrder.setAutoStatus(WorkOrder.AutoStatus.IncompleteSchedule);
            }

            if (totalTVCDuration == model.getTotalTVCDuration() && totalLOGOCount == model.getLogoSpots() && totalCRAWLERCount == model.getCrowlerSpots() && totalVSHAPECount == model.getV_sqeezeSpots() && totalLSHAPECount == model.getL_sqeezeSpots()) {
                workOrder.setAutoStatus(WorkOrder.AutoStatus.CompleteSchedule);
            }

            if (totalTVCAiredDuration == model.getTotalTVCDuration() && totalLOGOAiredCount == model.getLogoSpots() && totalCRAWLERAiredCount == model.getCrowlerSpots() && totalVSHAPEAiredCount == model.getV_sqeezeSpots() && totalLSHAPEAiredCount == model.getL_sqeezeSpots()) {
                workOrder.setAutoStatus(WorkOrder.AutoStatus.CompleteAiring);
            }
            if(workOrder.getAutoStatus() == WorkOrder.AutoStatus.CompleteSchedule && (totalTVCDuration != model.getTotalTVCDuration() ||
                    totalLOGOCount != model.getLogoSpots() || totalCRAWLERCount != model.getCrowlerSpots() || totalVSHAPECount != model.getV_sqeezeSpots() ||
                    totalLSHAPECount != model.getL_sqeezeSpots())){
                workOrder.setAutoStatus(WorkOrder.AutoStatus.IncompleteSchedule);
            }
            if (workOrderService.setAuditForWorkOrder(workOrder)) {
                workOrderDao.upadteWorkOrder(workOrder);
                /*=============DashBoard================
                newWorkOrderWidgetService.setNewWorkOrderData();
                myToDoListWidgetService.setMyToDoListData();
                myTeamToDoListWidgetService.setMyTeamToDoListData();
                dummyCutWidgetService.setDummyCutWidget();
                pendingWorkOrderWidgetService.setPendingWorkOrderData();
                partiallyEnteredWorkOrderWidgetService.setPartiallyEnteredWOData();
                /*=============DashBoard================*/
            }
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderStatusCheckService in checkAutoStatus()");
            return false;
        }
    }

    /// refactored 
    public WorkOrderChannelScheduleView getScheduleAvailableSpotCount(int workOrderId, int channelId) {
        try {
            List<WorkOrderChannelList> woChannelList;
            if (channelId != -999) {
                woChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId, channelId);
            } else {
                woChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId);
            }

            Set<WorkOrderChannelList> woChannelSet = new HashSet();
            //remove duplicate
            for (WorkOrderChannelList item : woChannelList) {
                woChannelSet.add(item);
            }

            WorkOrderChannelScheduleView viewModel = new WorkOrderChannelScheduleView();
            long iTotalTVCDuration = 0;
            for (WorkOrderChannelList _orderChannelModel : woChannelSet) {
                /// initial paid + bonus spot counts
                viewModel.setTvcfspot(_orderChannelModel.getNumbertofspot() + _orderChannelModel.getBonusspot());
                viewModel.setLogoSpots(_orderChannelModel.getLogospots() + _orderChannelModel.getLogobspots());
                viewModel.setCrowlerSpots(_orderChannelModel.getCrowlerspots() + _orderChannelModel.getCrowlerbspots());
                viewModel.setV_sqeezeSpots(_orderChannelModel.getVsqeezspots() + _orderChannelModel.getVsqeezebspots());
                viewModel.setL_sqeezeSpots(_orderChannelModel.getLsqeezespots() + _orderChannelModel.getLsqeezebspots());

                /// tvc_breakdown                 
                HashMap<Integer, Integer> tvcBreackdownMap = viewModel.getTvcBreackdownMap();
                for (TVCDurationSpotCount spot : _orderChannelModel.getTvcSpots()) {
                    if (tvcBreackdownMap.containsKey(spot.getDuration())) {
                        tvcBreackdownMap.put(spot.getDuration(), tvcBreackdownMap.get(spot.getDuration()) + spot.getSpotCount());
                    } else {
                        tvcBreackdownMap.put(spot.getDuration(), spot.getSpotCount());
                    }
                    iTotalTVCDuration += (spot.getDuration() * spot.getSpotCount());
                }
            }
            viewModel.setTotalTVCDuration(iTotalTVCDuration);

            WorkOrderChannelList lstScheduled = null;
            if (channelId != -999) {
                lstScheduled = schedulerDao.getWorkOrderAvailableSpotCount(workOrderId, channelId);
            } else {
                lstScheduled = schedulerDao.getWorkOrderAvailableSpotCount(workOrderId);
            }
            viewModel.setAvailableLogoSpots(lstScheduled.getLogospots());
            viewModel.setAvailableCrowlerSpots(lstScheduled.getCrowlerspots());
            viewModel.setAvailableV_SqeezeSpots(lstScheduled.getVsqeezspots());
            viewModel.setAvailableL_SqeezeSpots(lstScheduled.getLsqeezespots());

            /// tvc_breakdown 
            long iAvailableTVCDuration = viewModel.getTotalTVCDuration();
            HashMap<Integer, Integer> tvcBreackdownMapAvailable = viewModel.getTvcBreackdownMapAvailable();
            for (int item : viewModel.getTvcBreackdownMap().keySet()) {
                tvcBreackdownMapAvailable.put(item, viewModel.getTvcBreackdownMap().get(item));
            }
            for (TVCDurationSpotCount item : lstScheduled.getTvcSpots()) {
                if (!viewModel.getTvcBreackdownMap().containsKey(item.getDuration())) {
                    viewModel.getTvcBreackdownMap().put(item.getDuration(), 0);
                }
                tvcBreackdownMapAvailable.put(item.getDuration(), viewModel.getTvcBreackdownMap().get(item.getDuration()) - item.getSpotCount());
                iAvailableTVCDuration -= (item.getDuration() * item.getSpotCount());
            }
            viewModel.setAvailableTVCDuration(iAvailableTVCDuration);
            viewModel.setAvailableTvcSpots((int) (iAvailableTVCDuration / 30)); /// updated to get 30sec spot count
            return viewModel;

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderStatusCheckService in getScheduleAvailableSpotCount()");
            return null;
        }
    }
}
