package com.vclabs.nash.service.cronjobs;

import com.vclabs.nash.service.*;
import com.vclabs.nash.service.async.WorkOrderStatusCheckAsyncService;
import com.vclabs.nash.service.utill.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by dperera on 27/03/2019.
 */
@Component
public class CronJobExecutor {

    @Lazy
    @Autowired
    private WorkOrderStatusCheckService workOrderStatusCheckService;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

    @Lazy
    @Autowired
    private ChannelAdvertMapService channelAdvertMapService;

    @Lazy
    @Autowired
    private AdvertisementService advertisementService;

    @Lazy
    @Autowired
    private PlayListService playListService;

    @Lazy
    @Autowired
    private WorkOrderStatusCheckAsyncService workOrderStatusCheckAsyncService;

    @Lazy
    @Autowired
    private SpotSpreadService spotSpreadService;

    @Autowired
    private AdvertisementJobService advertisementJobService;

    @Scheduled(cron = "0 15 0 1/1 * ?")
    public void checkWorkOrder() {
        //workOrderStatusCheckService.checkWorkOrderAsync();
        workOrderStatusCheckAsyncService.processWorkOrders();
    }

    @Scheduled(cron = "0 41 23 1/1 * ?")
    public void clearAndCreatePlayList(){
        schedulerService.clearAndCreatePlayListAsync();
    }

    @Scheduled(cron = "0 40 23 * * *")
    public void clearAllChannelAsync(){
        channelAdvertMapService.clearAllChannelAsync();
    }

    @Scheduled(cron = "0 31 23 1/1 * ?")
    public void advertisementExpirationAndSuspendAsync(){
        advertisementJobService.executeExpirationAndSuspendJob();
    }

    @Scheduled(cron = "0 0 0/1 1/1 * ?")
    public void autoScheduleGenerateCallerAsync(){
        playListService.autoScheduleGenerateCallerAsync();
    }

    /* @Scheduled(cron = "0 40 0/1 1/1 * ?")
    public void runPartiallyProcessedFutureSchedulesForToday(){
        Date currentTime = new Date();
        Date date = DateUtil.fromLocalDate(DateUtil.toLocalDate(currentTime)); //start of day
        Time time =  new Time(currentTime.getTime());
        spotSpreadService.runPartiallyProcessedFutureSchedules(date, time);
        spotSpreadService.regenerateNotCreatedPlayLists(date);
    }*/

    // Commented this method for DB hang issue
//    @Scheduled(cron = "0 10 6/4 1/1 * ?")
//    public void runPartiallyProcessedFutureSchedulesForFutureDates(){
//        Date date = DateUtil.fromLocalDate(DateUtil.toLocalDate(new Date()));
//        spotSpreadService.runPartiallyProcessedFutureSchedules(date);
//    }

}
