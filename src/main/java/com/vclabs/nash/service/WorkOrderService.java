/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;

import com.vclabs.nash.dashboard.dto.MyCompletedSchedulesDto;
import com.vclabs.nash.dashboard.dto.MyRecordedRevenueDto;
import com.vclabs.nash.dashboard.dto.PartiallyEnteredWODto;
import com.vclabs.nash.dashboard.dto.PendingWOCountByAutoStatusDto;
import com.vclabs.nash.dashboard.dto.PendingWOCountByManualStatusDto;
import com.vclabs.nash.dashboard.dto.RevisedWODto;
import com.vclabs.nash.dashboard.dto.SchedulesReadyForInvoicingDto;
import com.vclabs.nash.model.process.dto.WODropDownDataDto;
import com.vclabs.nash.dashboard.service.*;
import com.vclabs.nash.dashboard.dto.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vclabs.nash.model.dao.ChannelDetailDAO;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.OrderAdvertListDAO;
import com.vclabs.nash.model.dao.RemainingSpotBankDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dao.WorkOrderAuditDAO;
import com.vclabs.nash.model.dao.WorkOrderChannelListDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ClientDetails;
import com.vclabs.nash.model.entity.OrderAdvertList;
import com.vclabs.nash.model.entity.RemainingSpotBank;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TVCDurationSpotCount;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.entity.WorkOrderAudit;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import com.vclabs.nash.model.process.OrderAdvertisementInfo;
import com.vclabs.nash.model.process.OrderChannelInfo;
import com.vclabs.nash.model.process.FiltersModel;
import com.vclabs.nash.model.process.WorkOrderInfo;
import com.vclabs.nash.model.view.OrderAdvertListView;
import com.vclabs.nash.model.view.ScheduleHomeView;
import com.vclabs.nash.model.view.WorkOrderChannelListView;
import com.vclabs.nash.model.view.WorkOrderUpdateView;
import com.vclabs.nash.model.view.WorkOrderView;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class WorkOrderService {

    static final Logger logger = LoggerFactory.getLogger(WorkOrderService.class);

    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private WorkOrderChannelListDAO workOrderChannelListDao;
    @Autowired
    private WorkOrderAuditDAO workOrderAuditDao;
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private ChannelDetailDAO channelDetailDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;
    @Autowired
    private OrderAdvertListDAO orderAdvertListDao;
    @Autowired
    private RemainingSpotBankDAO remainingSpotBankDao;
//    @Autowired
//    private MyToDoListWidgetService myToDoListWidgetService;
//    @Autowired
//    MyTeamToDoListWidgetService myTeamToDoListWidgetService;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

//    @Autowired
//    private PendingWorkOrderWidgetService pendingWorkOrderWidgetService;
//
//    @Autowired
//    private NewWorkOrderWidgetService newWorkOrderWidgetService;
//
//    @Autowired
//    private RevisedWorkOrderMaximizeWidgetService revisedWorkOrderMaximizeWidgetService;
//
//    @Autowired
//    private PartiallyEnteredWorkOrderWidgetService partiallyEnteredWorkOrderWidgetService;

    public List<WorkOrderView> allWorkOrderList() { //tested
        try {
            List<WorkOrder> order = workOrderDao.getAllWorkOrderList();
            List<WorkOrderView> orderView = new ArrayList<>();
            for (WorkOrder order1 : order) {
                if (order1.getWorkorderid() != 1) {
                    WorkOrderView model = new WorkOrderView();
                    model.setWorkorderid(order1.getWorkorderid());
                    model.setClient(order1.getClient());
                    model.setOrdername(order1.getOrdername());
                    model.setSeller(order1.getSeller());
                    model.setDate(order1.getDate());
                    model.setStartdate(order1.getStartdate());
                    model.setEnddate(order1.getEnddate());
                    model.setPermissionstatus(order1.getPermissionstatus());
                    model.setChannelCount(workOrderChannelListDao.getSelectedOrderChannelCount(model.getWorkorderid()));
                    model.setAdvertCount(orderAdvertListDao.getSelectedOrderAdvertCount(model.getWorkorderid()));
                    orderView.add(model);
                }
            }
            Collections.sort(orderView);
            return orderView;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, allWorkOrderList : {}", e.getMessage());
            throw e;
        }
    }

    public List<WorkOrderView> WorkOrederList() {

        try {
            List<WorkOrder> order = workOrderDao.getAllWorkOrderList();
            List<WorkOrderView> orderView = new ArrayList<>();
            for (WorkOrder order1 : order) {
                if (order1.getWorkorderid() != 1) {
                    WorkOrderView model = new WorkOrderView();
                    model.setWorkorderid(order1.getWorkorderid());
                    model.setClient(order1.getClient());
                    model.setOrdername(order1.getOrdername());
                    model.setSeller(order1.getSeller());
                    model.setDate(order1.getDate());
                    model.setStartdate(order1.getStartdate());
                    model.setEnddate(order1.getEnddate());
                    model.setPermissionstatus(order1.getPermissionstatus());
                    model.setChannelCount(workOrderChannelListDao.getSelectedOrderChannelCount(model.getWorkorderid()));
                    model.setAdvertCount(orderAdvertListDao.getSelectedOrderAdvertCount(model.getWorkorderid()));
                    orderView.add(model);
                }
            }
            Collections.sort(orderView);
            return orderView;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, WorkOrederList method : {}", e.getMessage());
            throw e;
        }

    }

    public List<WODropDownDataDto> getWODropDownData(){
        List<WODropDownDataDto> woData = new ArrayList<>();
        try{
            woData = workOrderDao.getWODropDownData();
        }catch (Exception e){
            logger.debug("Exception in WorkOrderService: ", e.getMessage());
        }
        return woData;
    }

    public List<ScheduleHomeView> WorkOrederList_userName(String userName) {
        List<WorkOrder> order = workOrderDao.getWorkOrederList(userName);
        List<ScheduleHomeView> orderView = new ArrayList<>();
        for (WorkOrder order1 : order) {
            ScheduleHomeView model = new ScheduleHomeView();
            model.setWorkorderid(order1.getWorkorderid());
            model.setWorkOrderName(order1.getOrdername());
            model.setScheduleStartDate(order1.getStartdate());
            model.setScheduleEndDate(order1.getEnddate());
            model.setAvailableAdvert(getAvailbleSpotsCount(order1.getWorkorderid(), order1.getBspotvisibility()));
            model.setAirSpots(schedulerDao.getSelectedWorkOrderAirSpot(order1.getWorkorderid()));
            model.setPermissionstatus(order1.getPermissionstatus());
            model.setBillingStatus(order1.getBillingstatus());
            orderView.add(model);
        }
        Collections.sort(orderView);
        return orderView;
    }

    public int setWorkOrder(String dateS, String dateE, String orderName, String clientName, String seller, String visibility, String comment, String exdata, String channelList, String advertList) throws Exception {
        try {
            ObjectMapper m = new ObjectMapper();
            WorkOrderInfo dataModel = m.readValue(exdata, WorkOrderInfo.class);

            SimpleDateFormat dta = new SimpleDateFormat("yyyy-MM-dd");// 00:00:00:000
            Date dt = new Date();
            Date startDate = dta.parse(dateS);
            Date endDate = dta.parse(dateE);
            String[] allClient = clientName.split("-");

            WorkOrder workorder = new WorkOrder();

            workorder.setOrdername(orderName);
            workorder.setClient(Integer.parseInt(allClient[0]));
            workorder.setAgencyclient(Integer.parseInt(allClient[1]));
            workorder.setBillclient(Integer.parseInt(allClient[2]));
            workorder.setSeller(seller);
            workorder.setBspotvisibility(Integer.parseInt(visibility));
            workorder.setComment(comment);

            workorder.setDate(dt);
            workorder.setStartdate(startDate);
            workorder.setEnddate(endDate);
            workorder.setLastModifyDate(dt);

            workorder.setPermissionstatus(0);
            workorder.setAutoStatus(WorkOrder.AutoStatus.Initial);
            workorder.setManualStatus(WorkOrder.ManualStatus.Initial);
            String[] sptAdvertIds = advertList.replace("[", "").replace("]", "").split(",");
            workorder.setSystemTypeype(WorkOrder.SystemType.NASH);

            workorder.setSeller(dataModel.getUserName());
            workorder.setWoType(dataModel.getWoType());
            workorder.setScheduleRef(dataModel.getScheduleRef());
            workorder.setCommission(dataModel.getCommission());
            workorder.setTotalBudget(dataModel.getTotalBudget());
            workorder.setRevenueMonth(dataModel.getRevenueMonth());

            if (workOrderDao.saveWorkOrder(workorder) != -1) {

                /*=============DashBoard================
                myToDoListWidgetService.setMyToDoListData();
                myTeamToDoListWidgetService.setMyTeamToDoListData();
                pendingWorkOrderWidgetService.setPendingWorkOrderData();
                newWorkOrderWidgetService.setNewWorkOrderData();
                revisedWorkOrderMaximizeWidgetService.setRevisedWorkOrderData();
                partiallyEnteredWorkOrderWidgetService.setPartiallyEnteredWOData();
                /*=============DashBoard================*/

                WorkOrderAudit auditModel = getAuditModelForWorkOrder(workorder);
                auditModel.setField("Create Uesr");
                String createUser = SecurityContextHolder.getContext().getAuthentication().getName();
                auditModel.setOldData(createUser);
                auditModel.setNewData(createUser);

                workOrderAuditDao.saveWorkOrderAudit(auditModel);

                List<OrderAdvertisementInfo> lstOrderAdverts = new ArrayList<>();
                for (String id : sptAdvertIds) {
                    OrderAdvertisementInfo pInfo = new OrderAdvertisementInfo();
                    try {
                        pInfo.setAdvertId(Integer.parseInt(id));
                    } catch (Exception e) {
                        continue;
                    }
                    pInfo.setWorkOrderId(workorder.getWorkorderid());
                    lstOrderAdverts.add(pInfo);
                }
                if (createChannelList(channelList, workorder.getWorkorderid()) && (this.saveOrderAdvertList(workorder.getWorkorderid(), lstOrderAdverts) > 0)) {
                    return workorder.getWorkorderid();
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } catch (ParseException e) {
            logger.debug("ParseException in WorkOrderService, setWorkOrder method : {}", e.getMessage());
            return -1;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, setWorkOrder method : {}", e.getMessage());
            throw e;
        }
    }

    public Boolean createChannelList(String channelList, int workOrderId) throws IOException {
        try {
            if (!channelList.equals("")) {

                ObjectMapper m = new ObjectMapper();
                List<OrderChannelInfo> orderChannels = m.readValue(channelList, new TypeReference<List<OrderChannelInfo>>() {
                });

                for (OrderChannelInfo model : orderChannels) {
                    model.setWorkOrderId(workOrderId);
                    if (!setWorkOrderChannelList(model)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, createChannelList method : {}", e.getMessage());
            throw e;
        }
    }

    @Transactional
    public Boolean updateWorkOrder(String workOrderData, String channelData, String advertList) throws Exception {

        try {
            ObjectMapper m = new ObjectMapper();
            WorkOrderInfo dataModel = m.readValue(workOrderData, WorkOrderInfo.class);

            List<OrderChannelInfo> dataChannels = m.readValue(channelData, new TypeReference<List<OrderChannelInfo>>() {
            });

            WorkOrder updateModel = workOrderDao.getSelectedWorkOrder(dataModel.getWorkOrderId());

            List<WorkOrderChannelList> updateChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(dataModel.getWorkOrderId());

            if (!updateModel.getOrdername().equals(dataModel.getOrderName())) {
                updateModel.setOrdername(dataModel.getOrderName());
            }
            SimpleDateFormat dta = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = dta.parse(dataModel.getStartDate());
            if (updateModel.getStartdate() != startDate) {
                updateModel.setStartdate(startDate);
                dataModel.setStartDate(startDate.toString());
            }
            Date endDate = dta.parse(dataModel.getEndDate());
            if (updateModel.getEnddate() != endDate) {
                updateModel.setEnddate(endDate);
                dataModel.setEndDate(endDate.toString());
            }
            if (!updateModel.getSeller().equals("SU")) {
                if (!updateModel.getSeller().equals(dataModel.getUserName())) {
                    updateModel.setSeller(dataModel.getUserName());
                }
            }
            if (!updateModel.getComment().equals(dataModel.getComment())) {
                updateModel.setComment(dataModel.getComment());
            }
            if (updateModel.getBspotvisibility() != dataModel.getVisibility()) {
                updateModel.setBspotvisibility(dataModel.getVisibility());
            }
            if (updateModel.getClient() != dataModel.getClient()) {
                updateModel.setClient(dataModel.getClient());
            }
            if (updateModel.getAgencyclient() != dataModel.getAgency()) {
                updateModel.setAgencyclient(dataModel.getAgency());
            }
            if (updateModel.getBillclient() != dataModel.getBillClient()) {
                updateModel.setBillclient(dataModel.getBillClient());
            }
            if (!updateModel.getWoType().equals(dataModel.getWoType())) {
                updateModel.setWoType(dataModel.getWoType());
            }
            if (!updateModel.getScheduleRef().equals(dataModel.getScheduleRef())) {
                updateModel.setScheduleRef(dataModel.getScheduleRef());
            }
            if (updateModel.getTotalBudget() != dataModel.getTotalBudget()) {
                updateModel.setTotalBudget(dataModel.getTotalBudget());
            }
            if (updateModel.getCommission() != dataModel.getCommission()) {
                updateModel.setCommission(dataModel.getCommission());
            }
            if (!updateModel.getRevenueMonth().equals(dataModel.getRevenueMonth())) {
                updateModel.setRevenueMonth(dataModel.getRevenueMonth());
            }

            if (!updateModel.getManualStatus().name().equals(dataModel.getManualStatus())) {
                if (WorkOrder.ManualStatus.Initial.name().equals(dataModel.getManualStatus())) {
                    updateModel.setManualStatus(WorkOrder.ManualStatus.Initial);
                } else if (WorkOrder.ManualStatus.Hold.name().equals(dataModel.getManualStatus())) {
                    /*============According to the dialog requirement this status change only for suspend button ============
                    updateModel.setManualStatus(WorkOrder.ManualStatus.Hold);
                    holdWorkOrder(updateModel.getWorkorderid());*/
                } else if (WorkOrder.ManualStatus.Revised.name().equals(dataModel.getManualStatus())) {
                    updateModel.setManualStatus(WorkOrder.ManualStatus.Revised);
                } else if (WorkOrder.ManualStatus.PaymentDone.name().equals(dataModel.getManualStatus())) {
                    updateModel.setManualStatus(WorkOrder.ManualStatus.PaymentDone);
                }
            }
            updateModel.setLastModifyDate(new Date());
            if (setAuditForWorkOrder(updateModel)) {
                workOrderDao.upadteWorkOrder(updateModel);

                /*=============DashBoard================
                newWorkOrderWidgetService.setNewWorkOrderData();
                revisedWorkOrderMaximizeWidgetService.setRevisedWorkOrderData();
                partiallyEnteredWorkOrderWidgetService.setPartiallyEnteredWOData();
                pendingWorkOrderWidgetService.setPendingWorkOrderData();
                myToDoListWidgetService.setMyToDoListData();
                myTeamToDoListWidgetService.setMyTeamToDoListData();
                /*=============DashBoard================*/
            } else {
                return false;
            }

            /// update advertlist //////////////////////////////////////////////////////////////////
            String[] sptAdvertIds = advertList.replace("[", "").replace("]", "").split(",");
            List<OrderAdvertisementInfo> lstOrderAdverts = new ArrayList<>();
            for (String id : sptAdvertIds) {
                OrderAdvertisementInfo pInfo = new OrderAdvertisementInfo();
                try {
                    pInfo.setAdvertId(Integer.parseInt(id));
                } catch (Exception e) {
                    continue;
                }
                pInfo.setWorkOrderId(updateModel.getWorkorderid());
                lstOrderAdverts.add(pInfo);
            }
            this.saveOrderAdvertList(updateModel.getWorkorderid(), lstOrderAdverts);

            Boolean updated = false;
            for (int i = 0; i < updateChannelList.size(); i++) {
                updated = false;
                WorkOrderChannelList channelModel = updateChannelList.get(i);
                for (int j = 0; j < dataChannels.size(); j++) {
                    OrderChannelInfo newDataModel = dataChannels.get(j);
                    newDataModel.setAverage();
                    if (channelModel.getChannelid().getChannelid() == newDataModel.getChannelId()) {

                        if (newDataModel.getTvc_numOfSpots() != -1) {
                            channelModel.setNumbertofspot(newDataModel.getTvc_numOfSpots());
                        }
                        if (newDataModel.getTvc_bonusSpots() != -1) {
                            channelModel.setBonusspot(newDataModel.getTvc_bonusSpots());
                        }
                        if (newDataModel.getTvc_package_value() != -1) {
                            channelModel.setTvcpackagevalue(newDataModel.getTvc_package_value());
                        }

                        if (newDataModel.getLogo_numOfSpots() != channelModel.getLogospots()) {
                            channelModel.setLogospots(newDataModel.getLogo_numOfSpots());
                        }
                        if (newDataModel.getLogo_bonusSpots() != channelModel.getLogobspots()) {
                            channelModel.setLogobspots(newDataModel.getLogo_bonusSpots());
                        }
                        if (newDataModel.getLogo_package_value() != channelModel.getLogopackagevalue()) {
                            channelModel.setLogopackagevalue(newDataModel.getLogo_package_value());
                        }

                        if (newDataModel.getCrowler_numOfSpots() != channelModel.getCrowlerspots()) {
                            channelModel.setCrowlerspots(newDataModel.getCrowler_numOfSpots());
                        }
                        if (newDataModel.getCrowler_bonusSpots() != channelModel.getCrowlerbspots()) {
                            channelModel.setCrowlerbspots(newDataModel.getCrowler_bonusSpots());
                        }
                        if (newDataModel.getCrowler_package_value() != channelModel.getCrowlerpackagevalue()) {
                            channelModel.setCrowlerpackagevalue(newDataModel.getCrowler_package_value());
                        }

                        if (newDataModel.getV_sqeeze_numOfSpots() != channelModel.getVsqeezspots()) {
                            channelModel.setVsqeezspots(newDataModel.getV_sqeeze_numOfSpots());
                        }
                        if (newDataModel.getV_sqeeze_bonusSpots() != channelModel.getVsqeezebspots()) {
                            channelModel.setVsqeezebspots(newDataModel.getV_sqeeze_bonusSpots());
                        }
                        if (newDataModel.getV_sqeeze_package_value() != channelModel.getVsqeezpackagevalue()) {
                            channelModel.setVsqeezpackagevalue(newDataModel.getV_sqeeze_package_value());
                        }

                        if (newDataModel.getL_sqeeze_numOfSpots() != channelModel.getLsqeezespots()) {
                            channelModel.setLsqeezespots(newDataModel.getL_sqeeze_numOfSpots());
                        }
                        if (newDataModel.getL_sqeeze_bonusSpots() != channelModel.getLsqeezebspots()) {
                            channelModel.setLsqeezebspots(newDataModel.getL_sqeeze_bonusSpots());
                        }
                        if (newDataModel.getL_sqeeze_package_value() != channelModel.getLsqeezpackagevalue()) {
                            channelModel.setLsqeezpackagevalue(newDataModel.getL_sqeeze_package_value());
                        }

                        if (newDataModel.getPackage_value() != channelModel.getPackagevalue()) {
                            channelModel.setPackagevalue(newDataModel.getPackage_value());
                        }
                        if (newDataModel.getAve_package_value() != channelModel.getAvaragepackagevalue()) {
                            channelModel.setAvaragepackagevalue(newDataModel.getAve_package_value());
                        }

                        channelModel.setWorkorderid(updateModel);
                        channelModel.setChannelid(channelDetailDao.getChannel(newDataModel.getChannelId()));
//                        if(setAuditForWorkOrderChannelList(channelModel)){
//                            if(workOrderChannelListDao.updateWorkOrderChannelList(channelModel)){
//                                updateChannelList.remove(channelModel);
//                                dataChannels.remove(newDataModel);
//                                i--;
//                            }
//                        }

                        //tvcbreakdown //////////////////
                        List<TVCDurationSpotCount> currentSpotCount = channelModel.getTvcSpots();
                        HashMap<String, Integer> mapBonusSpots = newDataModel.getTvcBonusSpotCountList();
                        HashMap<String, Integer> mapNonBonusSpots = newDataModel.getTvcSpotCountList();

                        for (TVCDurationSpotCount spot : currentSpotCount) {
                            String key = spot.getDuration().toString();
                            if (spot.getIsBonus()) {
                                if (mapBonusSpots.containsKey(key)) {
                                    if (!mapBonusSpots.get(key).equals(spot.getSpotCount())) {
                                        /*schedule budget update balance*/
                                        int notBonusSpot = 0;
                                        for (TVCDurationSpotCount currentSpotCount1 : currentSpotCount) {
                                            if (spot.getDuration() == currentSpotCount1.getDuration() && !currentSpotCount1.getIsBonus()) {
                                                notBonusSpot = currentSpotCount1.getSpotCount();
                                            }
                                        }
                                        int scheduledSpot = checkScheduledSpot(dataModel.getWorkOrderId(), spot.getDuration(), spot.getWoChannel().getChannelid().getChannelid(), 0, spot.getSpotCount());
                                        int bonusSpot = scheduledSpot - notBonusSpot;

                                        if (spot.getSpotCount() > mapBonusSpots.get(key)) {
                                            if (bonusSpot < mapBonusSpots.get(key)) {
                                                spot.setSpotCount(mapBonusSpots.get(key));
                                                workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                            } else {
                                                spot.setSpotCount(bonusSpot);
                                                workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                            }
                                        } else {
                                            spot.setSpotCount(mapBonusSpots.get(key));
                                            workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                        }
                                    }
                                    mapBonusSpots.remove(key);
                                } else {
                                    int notBonusSpot = 0;
                                    for (TVCDurationSpotCount currentSpotCount1 : currentSpotCount) {
                                        if (spot.getDuration() == currentSpotCount1.getDuration() && !currentSpotCount1.getIsBonus()) {
                                            notBonusSpot = currentSpotCount1.getSpotCount();
                                        }
                                    }
                                    int scheduledSpot = checkScheduledSpot(dataModel.getWorkOrderId(), spot.getDuration(), spot.getWoChannel().getChannelid().getChannelid(), 0, spot.getSpotCount());
                                    int bonusSpot = scheduledSpot - notBonusSpot;
                                    if (bonusSpot <= 0) {
                                        workOrderChannelListDao.deleteTVCDurationSpotCount(spot);
                                    } else {
                                        spot.setSpotCount(bonusSpot);
                                        workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                    }

                                }
                            } else {
                                if (mapNonBonusSpots.containsKey(key)) {
                                    if (!mapNonBonusSpots.get(key).equals(spot.getSpotCount())) {
                                        if (spot.getSpotCount() > mapNonBonusSpots.get(key)) {
                                            int scheduledCount = checkScheduledSpot(dataModel.getWorkOrderId(), spot.getDuration(), spot.getWoChannel().getChannelid().getChannelid(), mapNonBonusSpots.get(key), spot.getSpotCount());
                                            if (scheduledCount < mapNonBonusSpots.get(key)) {
                                                spot.setSpotCount(mapNonBonusSpots.get(key));
                                                workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                            } else {
                                                if (scheduledCount < spot.getSpotCount()) {
                                                    spot.setSpotCount(scheduledCount);
                                                    workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                                }
                                            }
                                        } else {
                                            spot.setSpotCount(mapNonBonusSpots.get(key));
                                            workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                        }
                                    }
                                    mapNonBonusSpots.remove(key);
                                } else {

                                    int schedulSpot = checkScheduledSpot(dataModel.getWorkOrderId(), spot.getDuration(), spot.getWoChannel().getChannelid().getChannelid(), 0, spot.getSpotCount());

                                    if (schedulSpot == 0) {
                                        workOrderChannelListDao.deleteTVCDurationSpotCount(spot);
                                    } else {
                                        spot.setSpotCount(schedulSpot);
                                        workOrderChannelListDao.updateTVCDurationSpotCount(spot);
                                    }
                                }
                            }
                        }
                        for (Map.Entry<String, Integer> entry : newDataModel.getTvcBonusSpotCountList().entrySet()) {
                            int duration = Integer.parseInt(entry.getKey());
                            int count = entry.getValue();

                            TVCDurationSpotCount pTvcSpotCount = new TVCDurationSpotCount();
                            pTvcSpotCount.setIsBonus(true);
                            pTvcSpotCount.setDuration(duration);
                            pTvcSpotCount.setSpotCount(count);
                            pTvcSpotCount.setWoChannel(channelModel);

                            workOrderChannelListDao.saveTVCDurationSpotCount(pTvcSpotCount);
                        }
                        for (Map.Entry<String, Integer> entry : newDataModel.getTvcSpotCountList().entrySet()) {
                            int duration = Integer.parseInt(entry.getKey());
                            int count = entry.getValue();

                            TVCDurationSpotCount pTvcSpotCount = new TVCDurationSpotCount();
                            pTvcSpotCount.setIsBonus(false);
                            pTvcSpotCount.setDuration(duration);
                            pTvcSpotCount.setSpotCount(count);
                            pTvcSpotCount.setWoChannel(channelModel);

                            workOrderChannelListDao.saveTVCDurationSpotCount(pTvcSpotCount);
                        }

                        dataChannels.remove(newDataModel);
                        j--;

                        updated = true;
                    }
                }
                if (setAuditForWorkOrderChannelList(channelModel) && updated && workOrderChannelListDao.updateWorkOrderChannelList(channelModel)) {
                    updateChannelList.remove(channelModel);
                    i--;
                }
            }

            for (int i = 0; i < updateChannelList.size(); i++) {
                workOrderChannelListDao.deleteWorkOrderChannelList(updateChannelList.get(i));
            }
            for (int i = 0; i < dataChannels.size(); i++) {
                dataChannels.get(i).setWorkOrderId(updateModel.getWorkorderid());
                setWorkOrderChannelList(dataChannels.get(i));
            }

            return true;

        } catch (ParseException | IOException e) {
            logger.debug("ParseException in WorkOrderService, updateWorkOrder method : {}", e.getMessage());
            return false;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, updateWorkOrder method : {}", e.getMessage());
            throw e;
        }
    }

    //Remaove additional spot from schedule table
    public int checkScheduledSpot(int workOrderID, int duation, int channelID, int newSpotCount, int oldSpotCount) {
        try {
            // get all scheduled advertisemenrt according to duration and channel
            List<ScheduleDef> remainingSpotList = schedulerDao.getAllSpotOrderByDate(workOrderID, duation, channelID);

            /* if (!remainingSpotList.isEmpty() && remainingSpotList.size() > newSpotCount) {

             //for the scheduled spot count
             int scheduledSpot = 0;

             //Scheduled advertisement are counted
             for (int i = 0; i < remainingSpotList.size(); i++) {
             if (!remainingSpotList.get(i).getStatus().equals("0") && !remainingSpotList.get(i).getStatus().equals("7")) {
             remainingSpotList.remove(remainingSpotList.get(i));
             i--;
             scheduledSpot++;
             }
             }

             if (scheduledSpot <= newSpotCount) {
             newSpotCount = newSpotCount - scheduledSpot;

             //Remove aditional scheduled advert
             for (int i = newSpotCount; i < remainingSpotList.size(); i++) {

             remainingSpotList.get(i).setStatus("6");
             remainingSpotList.get(i).setComment("Update spot count in workOrder");
             schedulerDao.updateSchedule(remainingSpotList.get(i));
             }
             return true;
             } else {
             return false;
             }
             } else {
             return true;
             }*/
            return remainingSpotList.size();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, checkScheduledSpot method : {}", e.getMessage());
            throw e;
        }
    }

    //Workorder persimmission status will be updated
    public Boolean updateWorkOrderPermissionstatus(int workOrderId, int permissionstatus) {

        try {

            WorkOrder model = workOrderDao.getSelectedWorkOrder(workOrderId);
            model.setPermissionstatus(permissionstatus);

            return workOrderDao.upadteWorkOrder(model);

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, updateWorkOrderPermissionstatus method : {}", e.getMessage());
            throw e;
        }
    }

    public Boolean deleteWorkOrder(int workOrderId) {
        try {

            WorkOrder model = workOrderDao.getSelectedWorkOrder(workOrderId);

            return workOrderDao.deleteWorkOrder(model);

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, deleteWorkOrder method : {}", e.getMessage());
            throw e;
        }
    }

    public List<WorkOrderUpdateView> getSelectedWorkOrder(int workOrderId) { //tested
        try {
            WorkOrder order = workOrderDao.getSelectedWorkOrder(workOrderId);
            List<WorkOrderUpdateView> orderUpdateView = new ArrayList<>();
            WorkOrderUpdateView model = new WorkOrderUpdateView();
            model.setWorkOrderId(order.getWorkorderid());

            model.setAgency("" + order.getAgencyclient());
            model.setClientName("" + order.getClient());
            model.setBillingClient("" + order.getBillclient());

            if (order.getAgencyclient() == 0) {
                model.setAgencyByName("No agency");
            } else {
                model.setAgencyByName(clientDetailDao.getSelectedClient(order.getAgencyclient()).getClientname());
            }
            model.setClientByName(clientDetailDao.getSelectedClient(order.getClient()).getClientname());
            model.setBillingClientByName(clientDetailDao.getSelectedClient(order.getBillclient()).getClientname());

            model.setProductName(order.getOrdername());
            model.setSeller(order.getSeller());
            model.setStartdate(order.getStartdate());
            model.setEnddate(order.getEnddate());
            model.setComment(order.getComment());
            model.setB_spots_visibility(order.getBspotvisibility());
            model.setChannelList(getSelectedOrderChannelList(model.getWorkOrderId()));
            model.setCommission(order.getCommission());
            model.setTotalbudget(order.getTotalBudget());
            model.setWoTyep(order.getWoType());
            model.setScheduleRef(order.getScheduleRef());
            model.setBillStatus(order.getBillingstatus());
            model.setManualStatus(order.getManualStatus().name());
            model.setWorkOrderStatus(order.getPermissionstatus());
            if (order.getRevenueMonth() != null) {
                model.setRevenueMonth(order.getRevenueMonth());
            }

            List<ScheduleDef> scheduleList = schedulerDao.getScheduleListOrderByASC(workOrderId);
            if (!scheduleList.isEmpty()) {
                model.setRevenueLine(scheduleList.get(0).getRevenueLine());
            }

            orderUpdateView.add(model);

            return orderUpdateView;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, getSelectedWorkOrder method : {}", e.getMessage());
            throw e;
        }
    }

    //active spot status change as hold status(9)
    public boolean holdWorkOrder(int workOrderID) throws Exception {
        try {
            int remainSeconds = 0;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            List<ScheduleDef> remainingSpotList = schedulerDao.getRamainingSpot(workOrderID, toDay);
            for (int i = 0; i < remainingSpotList.size(); i++) {
                remainingSpotList.get(i).setStatus("6");
                remainingSpotList.get(i).setComment(remainingSpotList.get(i).getComment() + " Work order Hold");
                remainSeconds += remainingSpotList.get(i).getAdvertid().getDuration();
                schedulerDao.updateSchedule(remainingSpotList.get(i));
            }
            WorkOrder model = workOrderDao.getSelectedWorkOrder(workOrderID);
            model.setPermissionstatus(4);
            model.setComment("Hold work order");
            model.setManualStatus(WorkOrder.ManualStatus.Hold);
            setAuditForWorkOrder(model);
            workOrderDao.upadteWorkOrder(model);
            Advertisement advertModel = new Advertisement();
            advertModel.setAdvertid(1);
            RemainingSpotBank remainmModel = new RemainingSpotBank(remainSeconds, "Work Order Hold", model, clientDetailDao.getSelectedClient(model.getBillclient()), advertModel);
            remainingSpotBankDao.saveORUpdateRemainingSpot(remainmModel);
            schedulerService.setPlayPermissionForWorkOrder(workOrderID,toDay);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, holdWorkOrder method : {}", e.getMessage());
            throw e;
        }
    }

    //WorkOrderChannelList method
    //Selected workOrder's channels are returnde
    public List<WorkOrderChannelListView> getSelectedOrderChannelList(int workorderId) { //tested
        //We have to give workOrderID
        List<WorkOrderChannelList> dataList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workorderId);
        List<WorkOrderChannelListView> channelList = new ArrayList<>();

        for (WorkOrderChannelList dataList1 : dataList) {
            WorkOrderChannelListView model = new WorkOrderChannelListView();
            model.setOrderedchannellistid(dataList1.getOrderedchannellistid());
            model.setWorkorderid(dataList1.getWorkorderid().getWorkorderid());
            model.setChannelid(dataList1.getChannelid().getChannelid());
            model.setChannelname(dataList1.getChannelid().getChannelname());

            model.setNumbertofspot(dataList1.getNumbertofspot());
            model.setBonusSpots(dataList1.getBonusspot());
            model.setTvcPackage(dataList1.getTvcpackagevalue());

            model.setLogoSpots(dataList1.getLogospots());
            model.setLogo_B_spots(dataList1.getLogobspots());
            model.setLogoPackage(dataList1.getLogopackagevalue());

            model.setCrowlerSpots(dataList1.getCrowlerspots());
            model.setCrowler_B_Spots(dataList1.getCrowlerbspots());
            model.setCrowlerPackage(dataList1.getCrowlerpackagevalue());

            model.setV_sqeezeSpots(dataList1.getVsqeezspots());
            model.setV_sqeeze_B_Spots(dataList1.getVsqeezebspots());
            model.setvSqeezePackage(dataList1.getVsqeezpackagevalue());

            model.setL_sqeezeSpots(dataList1.getLsqeezespots());
            model.setL_sqeeze_B_Spots(dataList1.getLsqeezebspots());
            model.setlSqeezePackage(dataList1.getLsqeezpackagevalue());

            model.setPackageValue(dataList1.getPackagevalue());
            model.setAveragePackageValue(dataList1.getAvaragepackagevalue());

            for (TVCDurationSpotCount spotCount : dataList1.getTvcSpots()) {
                if (spotCount.getIsBonus()) {
                    model.getTvcBonusSpotCountList().put(spotCount.getDuration().toString(), spotCount.getSpotCount());
                } else {
                    model.getTvcSpotCountList().put(spotCount.getDuration().toString(), spotCount.getSpotCount());
                }
            }
            channelList.add(model);
        }

        return channelList;
    }

    public Boolean setWorkOrderChannelList(OrderChannelInfo modelInfo) {

        try {
            modelInfo.setAverage();
            WorkOrderChannelList orderChannelList = new WorkOrderChannelList();

            WorkOrder wro = new WorkOrder();
            wro.setWorkorderid(modelInfo.getWorkOrderId());

            ChannelDetails chdt = new ChannelDetails();
            chdt.setChannelid(modelInfo.getChannelId());

            orderChannelList.setNumbertofspot(modelInfo.getTvc_numOfSpots());
            orderChannelList.setBonusspot(modelInfo.getTvc_bonusSpots());
            orderChannelList.setTvcpackagevalue(modelInfo.getTvc_package_value());

            orderChannelList.setLogospots(modelInfo.getLogo_numOfSpots());
            orderChannelList.setLogobspots(modelInfo.getLogo_bonusSpots());
            orderChannelList.setLogopackagevalue(modelInfo.getLogo_package_value());

            orderChannelList.setCrowlerspots(modelInfo.getCrowler_numOfSpots());
            orderChannelList.setCrowlerbspots(modelInfo.getCrowler_bonusSpots());
            orderChannelList.setCrowlerpackagevalue(modelInfo.getCrowler_package_value());

            orderChannelList.setVsqeezspots(modelInfo.getV_sqeeze_numOfSpots());
            orderChannelList.setVsqeezebspots(modelInfo.getV_sqeeze_bonusSpots());
            orderChannelList.setVsqeezpackagevalue(modelInfo.getV_sqeeze_package_value());

            orderChannelList.setLsqeezespots(modelInfo.getL_sqeeze_numOfSpots());
            orderChannelList.setLsqeezebspots(modelInfo.getL_sqeeze_bonusSpots());
            orderChannelList.setLsqeezpackagevalue(modelInfo.getL_sqeeze_package_value());

            orderChannelList.setPackagevalue(modelInfo.getPackage_value());
            orderChannelList.setAvaragepackagevalue(modelInfo.getAve_package_value());

            orderChannelList.setChannelid(chdt);
            orderChannelList.setWorkorderid(wro);

            for (Map.Entry<String, Integer> entry : modelInfo.getTvcBonusSpotCountList().entrySet()) {
                int duration = Integer.parseInt(entry.getKey());
                int count = entry.getValue();

                TVCDurationSpotCount pTvcSpotCount = new TVCDurationSpotCount();
                pTvcSpotCount.setIsBonus(true);
                pTvcSpotCount.setDuration(duration);
                pTvcSpotCount.setSpotCount(count);
                pTvcSpotCount.setWoChannel(orderChannelList);

                orderChannelList.addTvcSpot(pTvcSpotCount);
            }
            for (Map.Entry<String, Integer> entry : modelInfo.getTvcSpotCountList().entrySet()) {
                int duration = Integer.parseInt(entry.getKey());
                int count = entry.getValue();

                TVCDurationSpotCount pTvcSpotCount = new TVCDurationSpotCount();
                pTvcSpotCount.setIsBonus(false);
                pTvcSpotCount.setDuration(duration);
                pTvcSpotCount.setSpotCount(count);
                pTvcSpotCount.setWoChannel(orderChannelList);

                orderChannelList.addTvcSpot(pTvcSpotCount);
            }

            if (workOrderChannelListDao.setWorkOrderChannelList(orderChannelList)) {
                for (TVCDurationSpotCount spotCount : orderChannelList.getTvcSpots()) {
                    workOrderChannelListDao.saveTVCDurationSpotCount(spotCount);
                }
            }
            return true;

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, setWorkOrderChannelList method: {}", e.getMessage());
            throw e;
        }
    }

    public Boolean updateWorkOrderChannelList(int orderedchannellistid, int workOrderId, int channelId, int numberOfSpot, int bonusSpots) {
        try {

            WorkOrderChannelList orderChannelList = new WorkOrderChannelList();

            ChannelDetails chdt = channelDetailDao.getChannel(channelId);

            WorkOrder wro = workOrderDao.getSelectedWorkOrder(workOrderId);

            orderChannelList.setOrderedchannellistid(orderedchannellistid);
            orderChannelList.setNumbertofspot(numberOfSpot);
            orderChannelList.setBonusspot(bonusSpots);
            orderChannelList.setChannelid(chdt);
            orderChannelList.setWorkorderid(wro);

            return workOrderChannelListDao.updateWorkOrderChannelList(orderChannelList);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, updateWorkOrderChannelList method: {}", e.getMessage());
            throw e;
        }
    }

    public Boolean deleteWorkOrderChannelList(int orderedchannellistId) {

        try {

            WorkOrderChannelList model = workOrderChannelListDao.getSelectedOrderedChannelListDetail(orderedchannellistId);

            return workOrderChannelListDao.deleteWorkOrderChannelList(model);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, deleteWorkOrderChannelList method: {}", e.getMessage());
            throw e;
        }
    }

    /*
     *This is a timer. It will call after every 24 hour
     */
    public void timer() {
        workOrderExpiration();
    }

    public void workOrderExpiration() {
        try {
            List<WorkOrder> order = workOrderDao.getActiveWorkOrederList();
            for (WorkOrder model : order) {

                Date endDate = model.getEnddate();

                Calendar c = Calendar.getInstance();
                c.setTime(endDate);
                c.add(Calendar.DATE, 1);
                endDate = c.getTime();

                if (endDate.before(new Date())) {
                    model.setPermissionstatus(3);
                    workOrderDao.upadteWorkOrder(model);
                }
            }
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, workOrderExpiration method : {}", e.getMessage());
            throw e;
        }
    }
    //OrderAdvertList method

    public List<OrderAdvertListView> getOrderAdvertList() {

        List<OrderAdvertListView> advertList = new ArrayList<>();
        try {
            List<OrderAdvertList> dataList = orderAdvertListDao.getAllOrderAdvertList();

            for (OrderAdvertList dataList1 : dataList) {
                OrderAdvertListView model = new OrderAdvertListView();
                model.setOrderadvertlistid(dataList1.getOrderadvertlistid());
                model.setAdvertid(dataList1.getAdvertid().getAdvertid());
                model.setWorkorderid(dataList1.getWorkorderid().getWorkorderid());
                advertList.add(model);
            }

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, getOrderAdvertList method: {}", e.getMessage());
            throw e;
        }

        return advertList;
    }

    public List<OrderAdvertListView> getSelectedOrderAdvertList(int workOrderId) {

        List<OrderAdvertListView> advertList = new ArrayList<>();
        try {
            List<OrderAdvertList> dataList = orderAdvertListDao.getOrderAdvertList(workOrderId);

            for (OrderAdvertList dataList1 : dataList) {
                OrderAdvertListView model = new OrderAdvertListView();
                model.setOrderadvertlistid(dataList1.getOrderadvertlistid());
                model.setAdvertid(dataList1.getAdvertid().getAdvertid());
                model.setWorkorderid(dataList1.getWorkorderid().getWorkorderid());
                advertList.add(model);
            }

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, getSelectedOrderAdvertList method : {}", e.getMessage());
            throw e;
        }

        return advertList;
    }

    public int saveOrderAdvertList(String workOrderAdvertList) {
        try {
            ObjectMapper m = new ObjectMapper();
            List<OrderAdvertisementInfo> orderAdverts = m.readValue(workOrderAdvertList, new TypeReference<List<OrderAdvertisementInfo>>() {
            });
            return this.saveOrderAdvertList(orderAdverts.get(0).getWorkOrderId(), orderAdverts);
        } catch (IOException e) {
            logger.debug("Exception in WorkOrderService, saveOrderAdvertList method : {}", e.getMessage());
            return 2;
        }
    }

    public int saveOrderAdvertList(int workOrderId, List<OrderAdvertisementInfo> orderAdverts) {

        try {
            //Get existing advertisement list
            List<OrderAdvertList> saveOrderAdvertList = orderAdvertListDao.getOrderAdvertList(workOrderId);

            for (int i = 0; i < orderAdverts.size(); i++) {
                if (orderAdverts.get(i).getAdvertId() == 0) {
                    orderAdverts.remove(orderAdverts.get(i));
                }
            }

            Boolean existing = false;

            //Remove same advert from saveOrderAdvertList and orderAdverts
            for (int i = 0; i < saveOrderAdvertList.size(); i++) {
                existing = false;
                for (int j = 0; j < orderAdverts.size(); j++) {
                    if (orderAdverts.get(j).getAdvertId() == saveOrderAdvertList.get(i).getAdvertid().getAdvertid()) {
                        orderAdverts.remove(orderAdverts.get(j));
                        j--;
                        existing = true;
                    }
                }
                if (existing) {
                    saveOrderAdvertList.remove(saveOrderAdvertList.get(i));
                    i--;
                }
            }

            //New adverts are saved
            for (OrderAdvertisementInfo model : orderAdverts) {

                WorkOrder workorder = new WorkOrder();
                workorder.setWorkorderid(model.getWorkOrderId());

                Advertisement adver = new Advertisement();
                adver.setAdvertid(model.getAdvertId());

                OrderAdvertList orderAdvert = new OrderAdvertList();
                orderAdvert.setWorkorderid(workorder);
                orderAdvert.setAdvertid(adver);

                orderAdvertListDao.setOrderAdvertList(orderAdvert);
            }
            int num = 1;

            // Delete not scheduled advertisement
            for (int i = 0; i < saveOrderAdvertList.size(); i++) {

                if (schedulerDao.getScheduleListFromAdvertAndWorkOrde(saveOrderAdvertList.get(i).getWorkorderid().getWorkorderid(), saveOrderAdvertList.get(i).getAdvertid().getAdvertid()).isEmpty()) {
                    orderAdvertListDao.deleteOrderAdvertList(saveOrderAdvertList.get(i));
                } else {
                    num = 3;
                }
            }
            return num;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, saveOrderAdvertList method : {}", e.getMessage());
            throw e;
        }
    }

    public Boolean updateOrderAdvertList(int orderadvertlistId, int workorderid, int advertid) {

        try {
            WorkOrder workorder = new WorkOrder();
            workorder.setWorkorderid(workorderid);

            Advertisement adver = new Advertisement();
            adver.setAdvertid(advertid);

            OrderAdvertList orderAdvert = new OrderAdvertList();
            orderAdvert.setOrderadvertlistid(orderadvertlistId);
            orderAdvert.setWorkorderid(workorder);
            orderAdvert.setAdvertid(adver);

            return orderAdvertListDao.upadteOrderAdvertList(orderAdvert);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService, updateOrderAdvertList method: {}", e.getMessage());
            throw e;
        }
    }

    public Boolean deleteOrderAdvertList(int orderadvertlistid) {

        try {

            OrderAdvertList model = orderAdvertListDao.getSelectedOrderAdvertList(orderadvertlistid);
            model.setOrderadvertlistid(orderadvertlistid);

            return orderAdvertListDao.deleteOrderAdvertList(model);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService: {}", e.getMessage());
            throw e;
        }
    }

    public List<ClientDetails> getClientDetails() { //tested
        return clientDetailDao.getAllClientOrderByName();
    }

    public int getAvailbleSpotsCount(int workOrderId, int bVisibility) {
        int count = 0;
        try {
            List<WorkOrderChannelList> dataList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId);
            for (WorkOrderChannelList model : dataList) {
                if (bVisibility == 1) {
                    count += model.getNumbertofspot() + model.getBonusspot();
                } else {
                    count += model.getNumbertofspot();
                }
            }
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService: {}", e.getMessage());
            throw e;
        }
        return count;
    }

    public WorkOrderAudit getAuditModelForWorkOrder(WorkOrder model) {

        WorkOrderAudit auditModel = new WorkOrderAudit();
        auditModel.setRecordId("" + model.getWorkorderid());
        auditModel.setWorkorderid(model);
        auditModel.setDateAndTime(new Date());
        auditModel.setTablename("work_order");
        // auditModel.setUser(user);

        return auditModel;
    }

    public WorkOrderAudit getAuditModelForWorkOrderChannel(WorkOrderChannelList model) {

        WorkOrderAudit auditModel = new WorkOrderAudit();
        auditModel.setRecordId("" + model.getOrderedchannellistid());
        auditModel.setWorkorderid(model.getWorkorderid());
        auditModel.setDateAndTime(new Date());
        auditModel.setTablename("work_order_channel_list");
        //auditModel.setUser(user);

        return auditModel;
    }

    public Boolean setAuditForWorkOrder(WorkOrder model) {
        try {
            WorkOrder dataModel = workOrderDao.getSelectedWorkOrder(model.getWorkorderid());

            for (Field field : (WorkOrder.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null) {
                    writeAuditData(field, model, dataModel);
                }
            }
            return true;
        } catch (NullPointerException e) {
            logger.debug("NullPointerException in WorkOrderService: {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService: {}", e.getMessage());
            throw e;
        }
        return false;
    }

    public void writeAuditData(Field field, WorkOrder newModel, WorkOrder oldModel) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            if (field.getAnnotation(Column.class) != null && !field.get(newModel).equals(field.get(oldModel)) && !field.getAnnotation(Column.class).name().equals("lastModifyDate")) {

                String fieldName = field.getName();
                WorkOrderAudit auditModel = getAuditModelForWorkOrder(newModel);
                auditModel.setField(field.getAnnotation(Column.class).name());
                if (fieldName.equals("enddate") || fieldName.equals("startdate")) {
                    auditModel.setOldData(df.format(field.get(oldModel)));
                    auditModel.setNewData(df.format(field.get(newModel)));
                } else {
                    auditModel.setOldData(field.get(oldModel).toString());
                    auditModel.setNewData(field.get(newModel).toString());
                }

                workOrderAuditDao.saveWorkOrderAudit(auditModel);
            }
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService writeAuditData method: {}", e.getMessage());
        }
    }

    public Boolean setAuditForWorkOrderChannelList(WorkOrderChannelList model) {
        try {
            WorkOrderChannelList dataModel = workOrderChannelListDao.getSelectedOrderedChannelListDetail(model.getOrderedchannellistid());

            for (Field field : (WorkOrderChannelList.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null && (!field.get(model).equals(field.get(dataModel)))) {

                    WorkOrderAudit auditModel = getAuditModelForWorkOrderChannel(model);
                    auditModel.setField(field.getAnnotation(Column.class).name());
                    auditModel.setOldData(field.get(dataModel).toString());
                    auditModel.setNewData(field.get(model).toString());

                    workOrderAuditDao.saveWorkOrderAudit(auditModel);
                }
            }
            return true;
        } catch (IllegalAccessException e) {
            logger.debug("Exception in WorkOrderService: {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderService: {}", e.getMessage());
            throw e;
        }
        return false;
    }

    //If there is exist schedule ref, It will return true
    public Boolean checkExistScheduleRef(String refe) {
        List<WorkOrder> exsitList = workOrderDao.getIsExistingWorkOrder(refe);
        return !exsitList.isEmpty();

    }

    public Boolean checkExistScheduleRefForUpadte(String refe, int workOrderId) {
        WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(workOrderId);
        if (workOrder.getScheduleRef().equals(refe)) {
            return false;
        } else {
            return checkExistScheduleRef(refe);
        }
    }

    public Page<WorkOrderView> getWorkOrders(Map<String, String> filterRequest) {

        String sortString = filterRequest.get("sort");
        int page = Integer.parseInt(filterRequest.get("page"));
        int size = Integer.parseInt(filterRequest.get("size"));
        String woIDS = filterRequest.get("woid");
        int woID = 0;
        if (!woIDS.equals("")) {
            woID = Integer.parseInt(woIDS);
        }

        FiltersModel filter = new FiltersModel();
        filter.setStatus(filterRequest.get("status"));
        filter.setModifyDateS(filterRequest.get("modifyDate"));
        filter.setEnd(filterRequest.get("end"));
        filter.setStart(filterRequest.get("start"));
        filter.setMe(filterRequest.get("me"));
        filter.setScheduleRef(filterRequest.get("scheduleRef"));
        filter.setProduct(filterRequest.get("product"));
        filter.setClient(filterRequest.get("client"));
        filter.setAgent(filterRequest.get("agent"));

        Pageable pageable;
        //String agent, String client, String product, String scheduleRef, String me, String start, String end,String modifyDate, String status)
        if (sortString != null) {
            String[] sortParams = sortString.split(",");
            Sort sort = new Sort(Sort.Direction.fromString(sortParams[1]), sortParams[0]);
            pageable = new PageRequest(page, size, sort);
        } else {
            pageable = new PageRequest(page, size);
        }

        int totalWorkOrders = workOrderDao.getAllWorkOrderCount(woID, filter);
        List<WorkOrder> workOrderList = workOrderDao.getLimitedWorkOrderList(pageable.getPageSize(), pageable.getOffset(), woID, filter);
        List<WorkOrderView> orderView = new ArrayList<>();
        for (WorkOrder workOrder : workOrderList) {
            if (workOrder.getWorkorderid() != 1 && workOrder.getWorkorderid() != 0) {
                WorkOrderView model = new WorkOrderView();
                model.setWorkorderid(workOrder.getWorkorderid());
                model.setAgent(workOrder.getAgencyclient());
                model.setClient(workOrder.getClient());
                model.setOrdername(workOrder.getOrdername());
                model.setScheduleRef(workOrder.getScheduleRef());
                model.setSeller(workOrder.getSeller());
                model.setDate(workOrder.getDate());
                model.setStartdate(workOrder.getStartdate());
                model.setEnddate(workOrder.getEnddate());
                model.setLastModifydate(workOrder.getLastModifyDate());
                model.setStatus(workOrder.getAutoStatus().name() + "/" + workOrder.getManualStatus().name());
                model.setPermissionstatus(workOrder.getPermissionstatus());

                List<WorkOrderChannelList> updateChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrder.getWorkorderid());
                int channelCount = 0;
                int totalSpotCount = 0;
                double totalBudget = 0;
                for (WorkOrderChannelList workOrderChannelList : updateChannelList) {
                    channelCount++;
                    totalBudget += workOrderChannelList.getPackagevalue();
                    totalSpotCount += workOrderChannelList.getNumbertofspot();
                    totalSpotCount += workOrderChannelList.getBonusspot();

                    totalSpotCount += workOrderChannelList.getLogospots();
                    totalSpotCount += workOrderChannelList.getLogobspots();

                    totalSpotCount += workOrderChannelList.getCrowlerspots();
                    totalSpotCount += workOrderChannelList.getCrowlerbspots();

                    totalSpotCount += workOrderChannelList.getVsqeezspots();
                    totalSpotCount += workOrderChannelList.getVsqeezebspots();

                    totalSpotCount += workOrderChannelList.getLsqeezespots();
                    totalSpotCount += workOrderChannelList.getLogobspots();
                }
                model.setSpotCount(schedulerDao.getSelectedWorkOrderValidSpot(workOrder.getWorkorderid()) + " / " + totalSpotCount);
                model.setChannelCount(channelCount);
                model.setAdvertCount(orderAdvertListDao.getSelectedOrderAdvertCount(model.getWorkorderid()));
                if (Math.round(totalBudget * 100) == Math.round(workOrder.getTotalBudget() * 100)) {
                    model.setNotMatchWithBudget(Boolean.FALSE);
                }
                orderView.add(model);
            }
        }
        Collections.sort(orderView);

        return new PageImpl<>(orderView, pageable, totalWorkOrders);
    }

    public Page<WorkOrderView> getWorkOrdersForSchedule(Map<String, String> filterRequest) { //tested

        String sortString = filterRequest.get("sort");
        int page = Integer.parseInt(filterRequest.get("page"));
        int size = Integer.parseInt(filterRequest.get("size"));
        String woIDS = filterRequest.get("woid");
        int woID = 0;
        if (!woIDS.equals("")) {
            woID = Integer.parseInt(woIDS);
        }

        FiltersModel filter = new FiltersModel();
        filter.setStatus(filterRequest.get("status"));
        filter.setModifyDateS(filterRequest.get("modifyDate"));
        filter.setEnd(filterRequest.get("end"));
        filter.setStart(filterRequest.get("start"));
        filter.setMe(filterRequest.get("me"));
        filter.setScheduleRef(filterRequest.get("scheduleRef"));
        filter.setProduct(filterRequest.get("product"));
        filter.setClient(filterRequest.get("client"));
        filter.setAgent(filterRequest.get("agent"));

        Pageable pageable;
        //String agent, String client, String product, String scheduleRef, String me, String start, String end,String modifyDate, String status)
        if (sortString != null) {
            String[] sortParams = sortString.split(",");
            Sort sort = new Sort(Sort.Direction.fromString(sortParams[1]), sortParams[0]);
            pageable = new PageRequest(page, size, sort);
        } else {
            pageable = new PageRequest(page, size);
        }

        int totalWorkOrders = workOrderDao.getAllWorkOrderCountForSchedule(woID, filter);
        List<WorkOrder> workOrderList = workOrderDao.getLimitedWorkOrderListForSchedule(pageable.getPageSize(), pageable.getOffset(), woID, filter);
        List<WorkOrderView> orderView = new ArrayList<>();
        for (WorkOrder workOrder : workOrderList) {
            if (workOrder.getWorkorderid() != 1 && workOrder.getWorkorderid() != 0) {
                WorkOrderView model = new WorkOrderView();
                model.setWorkorderid(workOrder.getWorkorderid());
                model.setAgent(workOrder.getAgencyclient());
                model.setClient(workOrder.getClient());
                model.setOrdername(workOrder.getOrdername());
                model.setScheduleRef(workOrder.getScheduleRef());
                model.setSeller(workOrder.getSeller());
                model.setDate(workOrder.getDate());
                model.setStartdate(workOrder.getStartdate());
                model.setEnddate(workOrder.getEnddate());
                model.setLastModifydate(workOrder.getLastModifyDate());
                model.setStatus(workOrder.getAutoStatus().name() + "/" + workOrder.getManualStatus().name());
                model.setPermissionstatus(workOrder.getPermissionstatus());

                List<WorkOrderChannelList> updateChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrder.getWorkorderid());
                int channelCount = 0;
                double totalBudget = 0;
                for (WorkOrderChannelList workOrderChannelList : updateChannelList) {
                    channelCount++;
                    totalBudget += workOrderChannelList.getPackagevalue();
                }
                model.setSpotCount(getWorkOrderSpotCountAndAiredCount(workOrder.getWorkorderid()));
                model.setChannelCount(channelCount);
                model.setAdvertCount(orderAdvertListDao.getSelectedOrderAdvertCount(model.getWorkorderid()));
                if (Math.round(totalBudget * 100) == Math.round(workOrder.getTotalBudget() * 100)) {
                    model.setNotMatchWithBudget(Boolean.FALSE);
                }
                orderView.add(model);
            }
        }
        Collections.sort(orderView);
        return new PageImpl<>(orderView, pageable, totalWorkOrders);
    }

    public String getWorkOrderSpotCountAndAiredCount(int workOrderId) {
        int spotCount = 0;
        int airedCount = 0;
        List<ScheduleDef> schedulList = schedulerDao.getScheduleListOrderByASC(workOrderId);
        for (ScheduleDef scheduleDef : schedulList) {
            spotCount++;
            if (scheduleDef.getStatus().equals("2")) {
                airedCount++;
            }
        }
        return "" + spotCount + "/" + airedCount;
    }

    //tested
    public Page<WorkOrderView> getWorkOrdersForBilling(Map<String, String> filterRequest) {

        String sortString = filterRequest.get("sort");
        int page = Integer.parseInt(filterRequest.get("page"));
        int size = Integer.parseInt(filterRequest.get("size"));

        String woIDS = filterRequest.get("woid");
        FiltersModel filter = new FiltersModel();
        if (!woIDS.equals("")) {
            filter.setWorkOrderId(Integer.valueOf(woIDS));
        }
        filter.setAgent(filterRequest.get("agent"));
        filter.setClient(filterRequest.get("client"));
        filter.setProduct(filterRequest.get("product"));
        filter.setMe(filterRequest.get("me"));
        filter.setWoType(filterRequest.get("woType"));
        filter.setInvoiceType(filterRequest.get("invoiceType"));
        filter.setScheduleRef(filterRequest.get("scheduleRef"));
        filter.setRevMonth(filterRequest.get("revMonth"));
        filter.setEnd(filterRequest.get("end"));
        filter.setStatus(filterRequest.get("status"));

        Pageable pageable;
        if (sortString != null) {
            String[] sortParams = sortString.split(",");
            Sort sort = new Sort(Sort.Direction.fromString(sortParams[1]), sortParams[0]);
            pageable = new PageRequest(page, size, sort);
        } else {
            pageable = new PageRequest(page, size);
        }

        List<WorkOrder> workOrderList = workOrderDao.getLimitedWorkOrderListForBilling(pageable.getPageSize(), pageable.getOffset(), filter);
        int totalCount = workOrderDao.getAllWorkOrderCountForBilling(filter);
        List<WorkOrderView> orderView = new ArrayList<>();
        for (WorkOrder workOrder : workOrderList) {
            if (workOrder.getWorkorderid() != 1 && workOrder.getWorkorderid() != 0) {
                WorkOrderView model = new WorkOrderView();
                model.setWorkorderid(workOrder.getWorkorderid());
                model.setAgent(workOrder.getAgencyclient());
                model.setClient(workOrder.getClient());
                model.setOrdername(workOrder.getOrdername());
                model.setScheduleRef(workOrder.getScheduleRef());
                model.setSeller(workOrder.getSeller());
                model.setDate(workOrder.getDate());
                model.setStartdate(workOrder.getStartdate());
                model.setEnddate(workOrder.getEnddate());
                model.setLastModifydate(workOrder.getLastModifyDate());
                model.setStatus(workOrder.getAutoStatus().getTextValue() + "/" + workOrder.getManualStatus().getTextValue());
                model.setPermissionstatus(workOrder.getPermissionstatus());
                model.setWoType(workOrder.getWoType());
                model.setRenMonth(workOrder.getRevenueMonth());
                model.setAmount(workOrder.getTotalBudget());
                model.setCommission("" + workOrder.getCommission() + "%");
                model.setBillingStatus(workOrder.getBillingstatus());
                if (workOrder.getAgencyclient() == 0) {
                    model.setCommission(model.getCommission() + " Direct");
                } else {
                    model.setCommission(model.getCommission() + " Agency");
                }
                String spots = getWorkOrderSpotCountAndAiredCount(workOrder.getWorkorderid());
                model.setSpotCount(spots.split("/")[0]);
                model.setAired(spots.split("/")[1]);
                model.setAutoStatus(workOrder.getAutoStatus().ordinal());
                orderView.add(model);
            }
        }
        Collections.sort(orderView);

        return new PageImpl<>(orderView, pageable, totalCount);
    }

    /*----------------------------------Dashboard core---------------------------*/

    public List<WorkOrder> findPenddingWorkOrderByUser(String user){
      return  workOrderDao.findPenddingWorkOrderByUser(user);
    }

    public List<WorkOrder> findPenddingWorkOrderByRevenueMonth(String month){
        return  workOrderDao.findPenddingWorkOrderByRevenueMonth(month);
    }

    public List<PendingWOCountByAutoStatusDto> findAllPendingWOAutoStatusCount(){
        return workOrderDao.findAllPendingWOAutoStatusCount();
    }

    public List<PendingWOCountByManualStatusDto> findAllPendingWOManualStatusCount(){
        return workOrderDao.findAllPendingWOManualStatusCount();
    }

    public List<WorkOrder> findCompletedSchedulesWorkOrderByUser(String user) {
        return workOrderDao.findCompletedSchedulesWorkOrderByUser(user);
    }

    public List<MyCompletedSchedulesDto> findMyCompletedSchedules(){
        return workOrderDao.findMyCompletedSchedules();
    }

    public List<WorkOrder> findNewWorkOrders(String seller) {
        return workOrderDao.findNewWorkOrders(seller);
    }

    public List<String> findNewWorkOrderSellers() {
        return workOrderDao.findNewWorkOrdersSellers();
    }

    public List<WorkOrder> findRevisedWorkOrders() {
        return workOrderDao.findRevisedWorkOrders();
    }

    /*-----For Revise WorkOrder Maximize widget----*/
    public List<RevisedWODto> createRevisedWOList() {
        List<RevisedWODto> revisedWODtos = new ArrayList<>();
        List<WorkOrder> revisedWOs = findRevisedWorkOrders();

        for (WorkOrder revisedWo : revisedWOs){
            RevisedWODto revisedWODto = new RevisedWODto();
            revisedWODto.setWoId(revisedWo.getWorkorderid());
            revisedWODto.setClient(clientDetailDao.getSelectedClient(revisedWo.getClient()).getClientname());
            revisedWODto.setSpots(schedulerDao.getSelectedScheduleCount(revisedWo.getWorkorderid()));
            revisedWODtos.add(revisedWODto);
        }
        return revisedWODtos;
    }

    /****************For Schedules Ready for Invoicing Maximize Widget***********/
    public List<SchedulesReadyForInvoicingDto> findSchedulesReadyForInvoicing(){
        return workOrderDao.findSchedulesReadyForInvoicing();
    }

    /****************For Partially Entered WO widget*****************************/
    public List<PartiallyEnteredWODto> findPartiallyEnteredWOs(){
        List<PartiallyEnteredWODto> partiallyEnteredWODtos = new ArrayList<>();
        List<WorkOrder> partiallyEnteredWOs = workOrderDao.findPartiallyEnteredWorkOrders();
        for (WorkOrder workOrder : partiallyEnteredWOs){
            PartiallyEnteredWODto partiallyEnteredWODto = new PartiallyEnteredWODto();

            partiallyEnteredWODto.setWoId(workOrder.getWorkorderid());
            partiallyEnteredWODto.setClient(clientDetailDao.getSelectedClient(workOrder.getClient()).getClientname());
            partiallyEnteredWODto.setSpots(schedulerDao.getSelectedScheduleCount(workOrder.getWorkorderid()));
            partiallyEnteredWODto.setSeller(workOrder.getSeller());
            partiallyEnteredWODtos.add(partiallyEnteredWODto);
        }
        return partiallyEnteredWODtos;
    }

    /************For My Team Sales Revenue widget*****************/
    public double getTotalRevenueForMonthBySeller(String month, String seller){
        return workOrderDao.getTotalRevenueForMonthBySeller(month, seller);
    }

    /************For My Recorded Revenue widget*****************/
    public List<MyRecordedRevenueDto> getMyRecordedRevenue(String userName){
        return workOrderDao.findMyRecordedRevenue(userName);
    }

    public List<String> findAllSellers(){
        return workOrderDao.findAllSellers();
    }

    public List<SalesRevenueDTO> findSelecteUserAllWorkOrder(String user){
        return workOrderDao.findSelecteUserAllWorkOrder(user);
    }

    /************For RevenueLine widget*****************/
    public List<RevenueLineDbDTO> findAllWorkOrderForRevenueLine() {
        return workOrderDao.findAllWorkOrderForRevenueLine();
    }

    public WorkOrder resumingWorkOrder(int workOrderId) {

        WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(workOrderId);
        WorkOrder newWorkOrde = new WorkOrder();
        newWorkOrde.setOrdername(workOrder.getOrdername());
        newWorkOrde.setClient(workOrder.getClient());
        newWorkOrde.setSeller(workOrder.getSeller());
        newWorkOrde.setDate(new Date());
        newWorkOrde.setStartdate(workOrder.getStartdate());
        newWorkOrde.setEnddate(workOrder.getEnddate());
        newWorkOrde.setPermissionstatus(0);
        newWorkOrde.setBspotvisibility(workOrder.getBspotvisibility());
        newWorkOrde.setComment("Resuming workOrder (WorkOrder Id = )" + workOrder.getWorkorderid());
        newWorkOrde.setAgencyclient(workOrder.getAgencyclient());
        newWorkOrde.setBillclient(workOrder.getBillclient());
        newWorkOrde.setBillingstatus(workOrder.getBillingstatus());
        newWorkOrde.setWoType(workOrder.getWoType());
        newWorkOrde.setScheduleRef(workOrder.getScheduleRef() + "resuming");
        newWorkOrde.setTotalBudget(workOrder.getTotalBudget());
        newWorkOrde.setCommission(workOrder.getCommission());
        newWorkOrde.setRevenueMonth(workOrder.getRevenueMonth());
        newWorkOrde.setAutoStatus(WorkOrder.AutoStatus.Initial);
        newWorkOrde.setManualStatus(WorkOrder.ManualStatus.Initial);
        newWorkOrde.setSystemTypeype(WorkOrder.SystemType.NASH);
        workOrderDao.saveWorkOrder(newWorkOrde);

        List<WorkOrderChannelList> oldChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId);
        List<OrderAdvertList> oldAdvertList = orderAdvertListDao.getOrderAdvertList(workOrderId);

        for (WorkOrderChannelList workOrderChannelList : oldChannelList) {
            WorkOrderChannelList newWorkOrderChannelList = new WorkOrderChannelList();
            newWorkOrderChannelList.setNumbertofspot(workOrderChannelList.getNumbertofspot());
            newWorkOrderChannelList.setBonusspot(workOrderChannelList.getBonusspot());
            newWorkOrderChannelList.setTvcpackagevalue(workOrderChannelList.getTvcpackagevalue());

            newWorkOrderChannelList.setLogospots(workOrderChannelList.getLogospots());
            newWorkOrderChannelList.setLogobspots(workOrderChannelList.getLogobspots());
            newWorkOrderChannelList.setLogopackagevalue(workOrderChannelList.getLogopackagevalue());

            newWorkOrderChannelList.setCrowlerspots(workOrderChannelList.getCrowlerspots());
            newWorkOrderChannelList.setCrowlerbspots(workOrderChannelList.getCrowlerbspots());
            newWorkOrderChannelList.setCrowlerpackagevalue(workOrderChannelList.getCrowlerpackagevalue());

            newWorkOrderChannelList.setVsqeezspots(workOrderChannelList.getVsqeezspots());
            newWorkOrderChannelList.setVsqeezebspots(workOrderChannelList.getVsqeezebspots());
            newWorkOrderChannelList.setVsqeezpackagevalue(workOrderChannelList.getVsqeezpackagevalue());

            newWorkOrderChannelList.setLsqeezespots(workOrderChannelList.getLsqeezespots());
            newWorkOrderChannelList.setLsqeezebspots(workOrderChannelList.getLsqeezebspots());
            newWorkOrderChannelList.setLsqeezpackagevalue(workOrderChannelList.getLsqeezpackagevalue());

            newWorkOrderChannelList.setPackagevalue(workOrderChannelList.getPackagevalue());
            newWorkOrderChannelList.setAvaragepackagevalue(workOrderChannelList.getAvaragepackagevalue());
            newWorkOrderChannelList.setChannelid(workOrderChannelList.getChannelid());
            newWorkOrderChannelList.setWorkorderid(newWorkOrde);

            List<TVCDurationSpotCount> tvcDurationList = workOrderChannelList.getTvcSpots();

            for (TVCDurationSpotCount tvcDurationSpotCount : tvcDurationList) {
                TVCDurationSpotCount newTVCDuration = new TVCDurationSpotCount();
                newTVCDuration.setIsBonus(tvcDurationSpotCount.getIsBonus());
                newTVCDuration.setDuration(tvcDurationSpotCount.getDuration());
                newTVCDuration.setSpotCount(tvcDurationSpotCount.getSpotCount());
                newTVCDuration.setWoChannel(newWorkOrderChannelList);

                newWorkOrderChannelList.addTvcSpot(newTVCDuration);
            }

            if (workOrderChannelListDao.setWorkOrderChannelList(newWorkOrderChannelList)) {
                for (TVCDurationSpotCount spotCount : newWorkOrderChannelList.getTvcSpots()) {
                    workOrderChannelListDao.saveTVCDurationSpotCount(spotCount);
                }
            }
        }


        for (OrderAdvertList orderAdvertList : oldAdvertList) {

            OrderAdvertList orderAdvertList1 = new OrderAdvertList();
            orderAdvertList1.setAdvertid(orderAdvertList.getAdvertid());
            orderAdvertList1.setWorkorderid(newWorkOrde);

            orderAdvertListDao.setOrderAdvertList(orderAdvertList1);
        }
        schedulerService.resumeSuspendWorkOrderSpot(newWorkOrde,workOrderId);
        return newWorkOrde;
    }

    public List<Integer> findZeroBudgetWorkOrders(){
        return workOrderDao.findZeroBudgetWorkOrders();
    }
}
