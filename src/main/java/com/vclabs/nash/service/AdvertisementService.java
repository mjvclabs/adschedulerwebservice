/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import com.vclabs.nash.dashboard.dto.DummyCutListDto;
import com.vclabs.nash.dashboard.dto.IngestStatusWidgetDto;
import com.vclabs.nash.dashboard.dto.PriorityAdvertisementWidgetDto;
import com.vclabs.nash.dashboard.service.DummyCutWidgetService;
import com.vclabs.nash.dashboard.service.IngestStatusWidgetService;

import com.vclabs.nash.model.dao.*;
import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.model.entity.product.CodeMapping;
import com.vclabs.nash.model.process.dto.AdvertisementDropDownDataDto;
import com.vclabs.nash.model.view.GeneralAuditView;
import com.vclabs.nash.model.view.Message;
import com.vclabs.nash.service.exception.NashException;
import com.vclabs.nash.service.playlist_preview.vo.DailySchedulePreview;
import com.vclabs.nash.service.product.CodeMappingService;
import com.vclabs.nash.service.utill.FFMpegUtility;
import com.vclabs.nash.service.vo.PlaylistInputData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;

import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;

import com.vclabs.nash.model.dao.Advertisement.AdvertCategoryDAO;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.process.AdvertisementInfo;
import com.vclabs.nash.model.process.VideoConverter;
import com.vclabs.nash.model.view.AdvertisementDefView;
import com.vclabs.nash.model.process.AdvertFilter;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class AdvertisementService extends DateFormat {

    @Value("${file.advert.mediaDirectory}")
    private String mediaDirectory;

    @Value("${file.advert.mediaServerDirectory}")
    private String mediaServerDirectory;

    @Value("${file.advert.previewMediaDirectory}")
    private String previewMediaDirectory;

    @Autowired
    private AdvertisementDAO advertisementDao;
    @Autowired
    private ClientDetailDAO clientDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    private AdvertCategoryDAO advertCategoryDao;
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private RemainingSpotBankDAO remainingSpotBankDao;
    @Autowired
    private OrderAdvertListDAO orderAdvertListDAO;

    @Lazy
    @Autowired
    private ChannelAdvertMapService channelAdvertMapService;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

//    @Lazy
//    @Autowired
//    private DummyCutWidgetService dummyCutWidgetService;

//    @Lazy
//    @Autowired
//    private IngestStatusWidgetService ingestStatusWidgetService;

    @Autowired
    private FFMpegUtility ffMpegUtility;

    @Autowired
    private CodeMappingService codeMappingService;

    private static final Logger logger = LoggerFactory.getLogger(AdvertisementService.class);
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

//    @Async("dailyTaskExecutor")
//    public void advertisementExpirationAndSuspendAsync() {
//        logger.debug("Started advertisement expiration & suspend cron job | (cron = 0 40 23 1/1 * ?) | ( current time : {} )", new Date().toString());
//        advertisementExpirationAndSuspend();
//        logger.debug("Completed advertisement expiration & suspend cron job | (cron = 0 40 23 1/1 * ?) | ( current time : {} )", new Date().toString());
//    }
//
//    @Transactional
//    public void advertisementExpirationAndSuspend(){
//
//        try {
//            //List<Advertisement> allAdvert = getAllAdvertisement();
//            Date currentTime = new Date();
//            List<Advertisement> allAdvert = getAdvertisementsTobeExpiredOrSuspended(currentTime);
//            for (int i = 0; i < allAdvert.size(); i++) {
//                Date expireDate = allAdvert.get(i).getExpireDate();
//                Date suspendDate = allAdvert.get(i).getSuspendDate();
//                if (expireDate.before(new Date())) {
//                    allAdvert.get(i).setEnabled(0);
//                    allAdvert.get(i).setStatus(1);
//                    if (advertisementDao.updateAdvertisement(allAdvert.get(i))) {
//                        expireSchedule(allAdvert.get(i));
//                    }
//                } else {
//                    if (suspendDate.before(new Date())) {
//                        //suspendAdvert(allAdvert.get(i).getAdvertid());
//                        suspendAdvert(allAdvert.get(i));
//                    }
//                }
//            }
//        } catch (Exception e) {
//            logger.debug("Exception in AdvertisementService in advertisementExpirationAndSuspend : {}", e.getMessage());
//            throw e;
//        }
//    }

    public Boolean updateAdvertisement(Advertisement advertisement) {
        return advertisementDao.updateAdvertisement(advertisement);
    }

    public void expireSchedule(Advertisement expireAdvert) {
        try {

            List<ScheduleDef> expireScheduleList = schedulerDao.getScheduleListFromAdvertAndNotScheduleAndMissed(expireAdvert.getAdvertid());
            for (int j = 0; j < expireScheduleList.size(); j++) {
                expireScheduleList.get(j).setStatus("9");
                expireScheduleList.get(j).setComment("Advert expire");
                schedulerDao.updateSchedule(expireScheduleList.get(j));
            }
            //Remove Zeroads from list
            channelAdvertMapService.removeSuspendOrExpireAdverFromZeroAds(expireAdvert.getAdvertid());
            logger.debug("Successfully expired advertisement: {}", expireAdvert.getAdvertid());
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in expireSchedule : {}", e.getMessage());
        }
    }

    @Transactional
    public Advertisement getSelectedAdvertisement(int advertisementId) {
        Advertisement advertisement = advertisementDao.getSelectedAdvertisement(advertisementId);
        List<OrderAdvertList> orderAdvertLists = orderAdvertListDAO.getWorkOrderIdList(advertisementId);
        if (orderAdvertLists.isEmpty()) {
            advertisement.setCanClientChange(Boolean.TRUE);
        } else {
            advertisement.setCanClientChange(Boolean.FALSE);
        }
        return advertisement;
    }

    public List<Advertisement> getAdvertisementAllList() {
        List<Advertisement> advertList = advertisementDao.getAdvertisementAll();
        for (int i = 0; i < advertList.size(); i++) {

            ScheduleDef model = schedulerDao.getMaximumSpotInAdvert(advertList.get(i).getAdvertid());
            if (model == null) {
                advertList.get(i).setCanDelete(Boolean.TRUE);
            } else {
                Calendar c = Calendar.getInstance();
                c.setTime(model.getDate());
                c.add(Calendar.DATE, 1);
                if (c.getTime().before(new Date())) {
                    advertList.get(i).setCanDelete(Boolean.TRUE);
                } else {
                    advertList.get(i).setCanDelete(Boolean.FALSE);
                }
            }
        }
        return advertList;
    }

    public List<AdvertisementDropDownDataDto> getAdvertisementAllListOrderByName() {
        return advertisementDao.getAdvertisementAllOrderByName();
    }

    public List<Advertisement> getAdvertisementAllListValid() {
        return advertisementDao.getAdvertisementAllValid();
    }

    public List<Advertisement> getAllAdvertisement(){
        return advertisementDao.getAdvertisementAll();
    }

    public List<Advertisement> getAdvertisementsTobeExpiredOrSuspended(Date date) {
        return advertisementDao.getAdvertisementsTobeExpiredOrSuspended(date);
    }

    public List<Advertisement> getAdvertisementFilterList(String fData) { //tested
        try {
            String[] dataList = fData.split(",");

            String advertId = "-111";
            if (dataList.length == 4) {
                advertId = dataList[3];
                if (advertId == null || advertId.equals("")) {
                    advertId = "-111";
                } else {
                    try {
                        Integer.parseInt(advertId);
                    } catch (Exception e) {
                        advertId = "-111";
                    }
                }
            }

            List<Advertisement> advertList = advertisementDao.getFilterAdvertisements(dataList[0], dataList[1], dataList[2], advertId);
            for (int i = 0; i < advertList.size(); i++) {

                ScheduleDef model = schedulerDao.getMaximumSpotInAdvert(advertList.get(i).getAdvertid());

                if (model == null) {
                    advertList.get(i).setCanDelete(Boolean.TRUE);
                } else {
                    Calendar c = Calendar.getInstance();
                    c.setTime(model.getDate());
                    c.add(Calendar.DATE, 1);
                    if (c.getTime().before(new Date())) {
                        advertList.get(i).setCanDelete(Boolean.TRUE);
                    } else {
                        advertList.get(i).setCanDelete(Boolean.FALSE);
                    }
                }
            }
            return advertList;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in getAdvertisementFilterList : {}", e.getMessage());
            throw e;
        }
    }

    public List<Advertisement> getAdvertisementForTheZeroManualList(String fData) { //tested
        try {
            String[] dataList = fData.split(",");

            String advertId = "-111";
            if (dataList.length == 4) {
                advertId = dataList[3];
                if (advertId == null || advertId.equals("")) {
                    advertId = "-111";
                } else {
                    try {
                        Integer.parseInt(advertId);
                    } catch (Exception e) {
                        advertId = "-111";
                    }
                }
            }

            List<Advertisement> advertList = advertisementDao.getFilterAdvertisements(dataList[0], "ADVERT", dataList[2], advertId);
            return advertList;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in getAdvertisementForTheZeroManualList : {}", e.getMessage());
            throw e;
        }
    }

    public List<AdvertisementDefView> getClientAdvertisementList(int clieanId) {

        List<AdvertisementDefView> adverList = new ArrayList<>();
        List<Advertisement> tempList = advertisementDao.getAdvertisementList(clieanId);
        try {
            for (Advertisement temp : tempList) {
                AdvertisementDefView model = new AdvertisementDefView();
                model.setAdvertId(temp.getAdvertid());
                model.setAdvertName(temp.getAdvertname());
                model.setDuration(temp.getDuration());
                model.setType(temp.getAdverttype());
                adverList.add(model);
            }
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in getClientAdvertisementList : {}", e.getMessage());
            throw e;
        }
        return adverList;
    }

    public List<AdvertisementDefView> getClientAdvertisementListValid(int clieanId) {

        List<AdvertisementDefView> adverList = new ArrayList<>();
        List<Advertisement> tempList = advertisementDao.getAdvertisementListValid(clieanId);
        try {
            for (Advertisement temp : tempList) {
                AdvertisementDefView model = new AdvertisementDefView();
                model.setAdvertId(temp.getAdvertid());
                model.setAdvertName(temp.getAdvertname());
                model.setDuration(temp.getDuration());
                adverList.add(model);
            }
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in getClientAdvertisementListValid : {}", e.getMessage());
        }
        return adverList;
    }

    public List<Advertisement> getClientAdvertisement(int clieanIdOne, int clieanIdTwo) { //tested
        return advertisementDao.getClientAdvertisement(clieanIdOne, clieanIdTwo);
    }

    public Integer saveAdvertisement(String advertData) {

        try {
            ObjectMapper m = new ObjectMapper();
            AdvertisementInfo dataModel = m.readValue(advertData, AdvertisementInfo.class);

            Advertisement advert = new Advertisement();

            advert.setAdvertname(dataModel.getAdvertname());
            advert.setAdverttype(dataModel.getAdverttype());
            advert.setAdvertpath("");
            advert.setClient(clientDao.getSelectedClient(dataModel.getClient()));
            advert.setCommercialcategory(dataModel.getCommercialcategory());
            advert.setDuration(dataModel.getDuration());
            advert.setLanguage(dataModel.getLanguage());
            advert.setVedioType(dataModel.getVedioType());
            advert.setWidth(dataModel.getWidth());
            advert.setHight(dataModel.getHight());
            advert.setXposition(dataModel.getXposition());
            advert.setYposition(dataModel.getYposition());
            advert.setLogoContainerId(dataModel.getLogoContainer());
            advert.setCreateDate(new Date());
            advert.setExpireDate(dateFormat.parse(dataModel.getExpireDate()));
            advert.setSuspendDate(dateFormat.parse(dataModel.getSuspendDate()));
            advert.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            long cmId = dataModel.getCodeMapping();
            if(cmId != -1){
                advert.setCodeMapping(codeMappingService.findById(cmId));
            }
            advertisementDao.saveAdvertisement(advert);

            String stMediaPath = mediaDirectory + File.separator;
            if (advert.getAdverttype().equals("ADVERT")) {
                stMediaPath += "TVC";
            } else if (advert.getAdverttype().equals("LOGO")) {
                stMediaPath += "LOGO";
            } else if (advert.getAdverttype().equals("SLIDE")) {
                stMediaPath += "SLIDE";
            } else if (advert.getAdverttype().equals("CRAWLER")) {
                stMediaPath += "CRAWLER";
            } else if (advert.getAdverttype().equals("V_SHAPE")) {
                stMediaPath += "V_SHAPE";
            } else if (advert.getAdverttype().equals("L_SHAPE")) {
                stMediaPath += "L_SHAPE";
            } else if (advert.getAdverttype().equals("FILLER")) {
                stMediaPath += "FILLER";
            }

            String filePath = stMediaPath + File.separator + advert.getAdvertname() + "_" + advert.getAdvertid();
            advert.setAdvertpath(filePath);

            /// later update with minimum impact
            if (advert.getAdverttype().equals("LOGO_CONTAINER")) {
                advert.setAdverttype("LOGO");
                advert.setAdvertpath("LOGO_CONTAINER");
                advert.setFileAvailable(true);
            }
            advertisementDao.updateAdvertisement(advert);
            /*=============DashBoard================
            dummyCutWidgetService.setDummyCutWidget();
            ingestStatusWidgetService.setIngestStatusWidgetData();
             =============DashBoard================*/

            GeneralAudit auditModel = new GeneralAudit("advertisement_def",  advert.getAdvertid());
            auditModel.setField("user");
            auditModel.setOldData(advert.getUser());
            auditModel.setNewData(advert.getUser());

            generalAuditDao.saveGeneralAudit(auditModel);
            return advert.getAdvertid();
        } catch (Exception e) {
            return -1;
        }
    }

    /// depricated //////////// old no_use 
    public Boolean updateAdvertisement(String advertDetails) {

        try {
            ObjectMapper m = new ObjectMapper();
            AdvertisementInfo dataModel = m.readValue(advertDetails, AdvertisementInfo.class);

            Advertisement advert = advertisementDao.getSelectedAdvertisement(dataModel.getAdvertid());

            advert.setAdvertid(dataModel.getAdvertid());
            advert.setAdvertname(dataModel.getAdvertname());
            advert.setAdverttype(dataModel.getAdverttype());
            String filePath = mediaDirectory + System.getProperty("file.separator") + advert.getAdvertname() + "_" + advert.getAdvertid()
                    + (advert.getAdverttype().equals("ADVERT") || advert.getAdverttype().equals("FILLER") ? ".mpg" : ".swf");
            advert.setAdvertpath(filePath);
            ClientDetails data = clientDao.getSelectedClient(dataModel.getClient());
            advert.setClient(new ClientDetails(data.getClientid()));
            advert.setCommercialcategory(dataModel.getCommercialcategory());
            advert.setDuration(dataModel.getDuration());
            advert.setLanguage(dataModel.getLanguage());
            advert.setWidth(dataModel.getWidth());
            advert.setHight(dataModel.getHight());
            advert.setXposition(dataModel.getXposition());
            advert.setYposition(dataModel.getYposition());
            advert.setLogoContainerId(dataModel.getLogoContainer());
            advert.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));

            if (setAuditForAdvertisement(advert)) {
                /*=============DashBoard================
                dummyCutWidgetService.setDummyCutWidget();
                ingestStatusWidgetService.setIngestStatusWidgetData();
                =============DashBoard================*/
                return advertisementDao.updateAdvertisement(advert);
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public Boolean updateAdvertisementMetaData(String advertDetails) {

        try {
            ObjectMapper m = new ObjectMapper();
            AdvertisementInfo dataModel = m.readValue(advertDetails, AdvertisementInfo.class);

            Advertisement advert = advertisementDao.getSelectedAdvertisement(dataModel.getAdvertid());

            advert.setAdvertid(dataModel.getAdvertid());
            advert.setAdvertname(dataModel.getAdvertname());
            advert.setAdverttype(dataModel.getAdverttype());

            ClientDetails data = clientDao.getSelectedClient(dataModel.getClient());
            advert.setClient(new ClientDetails(data.getClientid(),data.getClientname()));
            advert.setCommercialcategory(dataModel.getCommercialcategory());
            advert.setDuration(dataModel.getDuration());
            advert.setLanguage(dataModel.getLanguage());
            advert.setVedioType(dataModel.getVedioType());
            advert.setWidth(dataModel.getWidth());
            advert.setHight(dataModel.getHight());
            advert.setXposition(dataModel.getXposition());
            advert.setYposition(dataModel.getYposition());
            advert.setLogoContainerId(dataModel.getLogoContainer());
            advert.setExpireDate(dateFormat.parse(dataModel.getExpireDate()));
            advert.setSuspendDate(dateFormat.parse(dataModel.getSuspendDate()));
            advert.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            long cmId = dataModel.getCodeMapping();
            if(cmId != -1){
                advert.setCodeMapping(codeMappingService.findById(cmId));
            }
            if (setAuditForAdvertisement(advert)) {
                /*=============DashBoard================
                dummyCutWidgetService.setDummyCutWidget();
                ingestStatusWidgetService.setIngestStatusWidgetData();
                =============DashBoard================*/
                return advertisementDao.updateAdvertisement(advert);
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public Boolean deleteAdvertisement(int advertisementId) {

        try {

            Advertisement model = advertisementDao.getSelectedAdvertisement(advertisementId);
            model.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            return advertisementDao.deleteAdvertisement(model);

        } catch (Exception e) {
            return false;
        }
    }

    public Boolean deleteAdvertisementByStatus(int advertisementId, HttpServletRequest request) {
        try {
            ScheduleDef model = schedulerDao.getMaximumSpotInAdvert(advertisementId);
            if (model.getDate().before(new Date())) {
                return deleteAdvert(advertisementId, request);
            } else {
                return false;
            }

        } catch (Exception e) {
            return deleteAdvert(advertisementId, request);
        }
    }

    @Transactional
    public Boolean deleteAdvert(int advertisementId, HttpServletRequest request) {
        try {
            Advertisement model = advertisementDao.getSelectedAdvertisement(advertisementId);
            model.setEnabled(0);
            model.setStatus(3);
            model.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            setAuditForAdvertisement(model);
            return advertisementDao.updateAdvertisement(model);
        } catch (Exception e) {
            return false;
        }
    }

    private String getPreviewMediaFilePath(Advertisement pAdvert, String oFileExtension){
        String mediaFilePath = pAdvert.getAdvertpath().replace(mediaDirectory, mediaServerDirectory);
        File mediaFile = new File(mediaFilePath);
        String mediaFileName = mediaFile.getName().split("\\.")[0];
        String pMediaPath = previewMediaDirectory + File.separator + mediaFileName;
        if (oFileExtension.equals("mpg")) {
            pMediaPath = pMediaPath + ".mp4";
        } else if (oFileExtension.equals("swf")) {
            pMediaPath = pMediaPath + ".swf";
        } else if (oFileExtension.equals("jpg")) {
            pMediaPath = pMediaPath + ".jpg";
        } else if (oFileExtension.equals("png")) {
            pMediaPath = pMediaPath + ".png";
        }
        return pMediaPath;
    }

    @Transactional(rollbackFor = NashException.class )
    public Advertisement fileUploader(MultipartHttpServletRequest request) throws NashException {

        Iterator<String> itrator = request.getFileNames();
        MultipartFile multiFile = request.getFile(itrator.next());
        int iAdvertId = Integer.parseInt(request.getParameter("advert_id"));
        Advertisement pAdvert = advertisementDao.getSelectedAdvertisement(iAdvertId);
        String stMediaPath = pAdvert.getAdvertpath().replace(mediaDirectory, mediaServerDirectory);

        String fileExtension = multiFile.getOriginalFilename().split("\\.")[1].toLowerCase();
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String dateTimeSuffix = dateTimeFormat.format(new Date());

        // saving the file => file name must be a generated unique one
        if (!pAdvert.isFileAvailable()) {
            stMediaPath = stMediaPath + "_" + dateTimeSuffix + "." + fileExtension;
        } else {
            stMediaPath = (stMediaPath.substring(0, stMediaPath.indexOf("_"))) + "_" + pAdvert.getAdvertid() + "_" + dateTimeSuffix + "." + fileExtension;
        }

        try (InputStream inputStream = multiFile.getInputStream();) {
            Files.copy(inputStream, Paths.get(stMediaPath));
        }catch (Exception e){
            throw new NashException(e);
        }

        //need to remove old media file & preview file
        boolean hasMediaFile = pAdvert.isFileAvailable();
        String oldMediaPath = hasMediaFile ? pAdvert.getAdvertpath().replace(mediaDirectory, mediaServerDirectory) : "";
        String  oldPMediaPath = hasMediaFile ? getPreviewMediaFilePath(pAdvert, fileExtension) : "";

        String path = stMediaPath.replace(mediaServerDirectory, mediaDirectory);
        pAdvert.setAdvertpath(path);
        try {
            File mediaFilePath = new File(stMediaPath);
            String pMediaPath = getPreviewMediaFilePath(pAdvert, fileExtension);
            if (fileExtension.equals("mpg")) {
                /*if (!VideoConverter.convert(stMediaPath, pMediaPath)) {
                    if( mediaFilePath.delete()) {
                        logger.info("Delete media file path: {}", stMediaPath);
                    }
                    throw new IOException("Error has occurred while converting the media file.");
                }*/
                if (!ffMpegUtility.covertFromMpegToMp4(stMediaPath, pMediaPath)) {
                    if( mediaFilePath.delete()) {
                        logger.info("Delete media file path: {}", stMediaPath);
                    }
                    throw new IOException("Error has occurred while converting the media file.");
                }
            } else if (fileExtension.equals("swf")) {
                Files.copy(new File(stMediaPath).toPath(), new File(pMediaPath).toPath(), REPLACE_EXISTING);
            } else if (fileExtension.equals("jpg")) {
                Files.copy(new File(stMediaPath).toPath(), new File(pMediaPath).toPath(), REPLACE_EXISTING);
            } else if (fileExtension.equals("png")) {
                Files.copy(new File(stMediaPath).toPath(), new File(pMediaPath).toPath(), REPLACE_EXISTING);
            } else {
                throw new NashException("Unsupported file format for scheduling.");
            }

            pAdvert.setFileAvailable(true);
            pAdvert.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            advertisementDao.updateAdvertisement(pAdvert);
            if(hasMediaFile){ //for existing advertisements
                File oldMediaFile = new File(oldMediaPath);
                if(oldMediaFile.delete()){
                    logger.info("Delete media file path: {} ", oldMediaPath);
                }
                File oldPMediaFile = new File(oldPMediaPath);
                if(oldPMediaFile.delete()){
                    logger.info("Delete media file path: {} ", oldPMediaPath);
                }
            }
            /*=============DashBoard================
            dummyCutWidgetService.setDummyCutWidget();
            ingestStatusWidgetService.setIngestStatusWidgetData();
            =============DashBoard================*/
            return pAdvert;
        }  catch (Exception e) {
            throw new NashException(e);
        }
    }

    public List<AdvertCategories> getAllAdvertCategory() {
        return advertCategoryDao.getAllAdvertCategories();
    }

    public Message saveAdvertisementCategory(String category) {
        Message message = new Message();
        try {
            if (checkAdvertisementCategoryName(category)) {
                AdvertCategories model = new AdvertCategories();
                model.setCategory(category);

                if (advertCategoryDao.saveAdvertCategories(model)) {
                    message.setFlag(true);
                    message.setMessage("Successfully save");
                    return message;
                } else {
                    message.setFlag(false);
                    message.setMessage("Not save category");
                    return message;
                }
            } else {
                message.setFlag(false);
                message.setMessage("Existing category");
                return message;
            }
        } catch (Exception e) {
            message.setFlag(false);
            message.setMessage("Error");
            return message;
        }
    }

    public Boolean updateAdvertisementCategory(int id, String category) {
        try {
            AdvertCategories model = new AdvertCategories();
            model.setAdvertCategoryId(id);
            model.setCategory(category);

            return advertCategoryDao.updateAdvertCategories(model);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkAdvertisementCategoryName(String category) {
        return advertCategoryDao.getAdvertCategories(category).size() == 0 ? true : false;
    }

    //User can suspend advertisement
    public Boolean suspendAdvert(Advertisement advert) {
        try {
            int remainSeconds = 0;
            logger.info("Advertisement Suspend obj[]-first -> Advert Id : {} | User : {}", advert.getAdvertid(), advert.getUser());
            //Get valid scheduled spots
            List<ScheduleDef> remainingSpotList = schedulerDao.getScheduledSpotByAdvertID(advert.getAdvertid());
            logger.info("Advertisement Suspend obj[]-second -> Advert Id : {}", advert.getAdvertid());
            //Calculate remaining spot total duration and That scheduled spot update as suspend spots
            for (int i = 0; i < remainingSpotList.size(); i++) {
                remainingSpotList.get(i).setStatus("9");
                remainingSpotList.get(i).setComment("Advertisement Suspend");
                remainSeconds += remainingSpotList.get(i).getAdvertid().getDuration();
                logger.info("Before update schedule obj[] -> Advert Id : {}", advert.getAdvertid());
                schedulerDao.updateSchedule(remainingSpotList.get(i));
            }
            logger.info("Advertisement Suspend obj[]-third -> Advert Id : {}", advert.getAdvertid());
            //Generate play list after the remove scheduled spot
            //Note: this is commented 22/02/2021 to reduce expiration job duration
            //schedulerService.PlaylistGenerateForScheduledSpot(remainingSpotList);

            //Create workOrder dume model for remaining table
            WorkOrder model = new WorkOrder();
            model.setWorkorderid(1);

            //Selected advertisement update as suspend advertisemant
            Advertisement advertModel = advert;
            advertModel.setEnabled(0);
            advertModel.setStatus(2);
            advertModel.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            logger.info("Advertisement Suspend obj[]-fourth -> Advert Id : {}", advert.getAdvertid());
            if (advertisementDao.updateAdvertisement(advertModel)) {
                logger.info("Before update advertisement obj[] -> Advert Id : {}",advertModel.getAdvertid());
                //Save audit data
                setAuditForAdvertisement(advertModel);

            }
            logger.info("Advertisement Suspend obj[]-fifth -> Advert Id : {}", advert.getAdvertid());
            //Remove Zeroads from list
            channelAdvertMapService.removeSuspendOrExpireAdverFromZeroAds(advertModel.getAdvertid());
            logger.info("Advertisement Suspend obj[]-sixth -> Advert Id : {}", advert.getAdvertid());
            //Save remaining total duration
            RemainingSpotBank remainmModel = new RemainingSpotBank(remainSeconds, "Advertisement suspend", model, advertModel.getClient(), advertModel);
            RemainingSpotBank updateModel = remainingSpotBankDao.getSetectedRemainingSpot(1, advert.getAdvertid());
            if (updateModel != null) {
                remainmModel.setRemainingId(updateModel.getRemainingId());
                remainmModel.setRemainingSpot(remainSeconds);
            }
            remainingSpotBankDao.saveORUpdateRemainingSpot(remainmModel);
            logger.debug("Successfully suspended advertisement: {}", advert.getAdvertid());
            return true;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in suspendAdvert : {}", e.getMessage());
            throw e;
        }
    }

    //User can suspend advertisement
    public Boolean suspendAdvert(int advertID) {
        try {
            int remainSeconds = 0;
            logger.info("Advertisement Suspend - first -> Advert Id : {}", advertID);

            //Get valid scheduled spots
            List<ScheduleDef> remainingSpotList = schedulerDao.getScheduledSpotByAdvertID(advertID);
            logger.info("Advertisement Suspend - second -> Advert Id : {}", advertID);
            //Calculate remaining spot total duration and That scheduled spot update as suspend spots
            List<ScheduleDef> updatedList = new ArrayList<>();
            for (int i = 0; i < remainingSpotList.size(); i++) {
                ScheduleDef scheduleDef = remainingSpotList.get(i);
                if(scheduleDef.getStatus().equalsIgnoreCase("1") || scheduleDef.getStatus().equalsIgnoreCase("2") ) {
                    continue;
                }
                scheduleDef.setStatus("9");
                scheduleDef.setComment("Advertisement Suspend");
                remainSeconds += scheduleDef.getAdvertid().getDuration();
                updatedList.add(scheduleDef);
                logger.info("Before update schedule -> Advert Id : {}", advertID);
            }
            logger.info("Advertisement Suspend - third -> Advert Id : {}", advertID);
            schedulerDao.updateSchedule(updatedList);
            logger.info("Advertisement Suspend - fourth -> Advert Id : {}", advertID);
            //Generate play list after the remove scheduled spot
            long t1 = System.currentTimeMillis();
            if(remainingSpotList.size() != 0) {
                generatePlayListForScheduledSpots(remainingSpotList);
            }
                long t2 = System.currentTimeMillis();
            System.out.printf(">>>Advert service suspend "+ (t2 - t1));
            //Create workOrder dume model for remaining table
            WorkOrder model = new WorkOrder();
            model.setWorkorderid(1);
            logger.info("Advertisement Suspend - fifth -> Advert Id : {}", advertID);
            //Selected advertisement update as suspend advertisemant
            Advertisement advertModel = advertisementDao.getSelectedAdvertisement(advertID);
            advertModel.setEnabled(0);
            advertModel.setStatus(2);
            advertModel.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            logger.info("Before update advertisement -> Advert Id : {} | User : {}",advertModel.getAdvertid(), advertModel.getUser());
            if ( setAuditForAdvertisement(advertModel)) {
                logger.info("Before update advertisement -> Advert Id : {}",advertModel.getAdvertid());
                //Save audit data
                advertisementDao.updateAdvertisement(advertModel);


            }
            //Remove Zeroads from list
            logger.info("Advertisement Suspend - sixth -> Advert Id : {}", advertID);
            channelAdvertMapService.removeSuspendOrExpireAdverFromZeroAds(advertModel.getAdvertid());
            //Save remaining total duration 
            RemainingSpotBank remainmModel = new RemainingSpotBank(remainSeconds, "Advertisement suspend", model, advertModel.getClient(), advertModel);
            RemainingSpotBank updateModel = remainingSpotBankDao.getSetectedRemainingSpot(1, advertID);
            if (updateModel != null) {
                remainmModel.setRemainingId(updateModel.getRemainingId());
                remainmModel.setRemainingSpot(remainSeconds);
            }
            remainingSpotBankDao.saveORUpdateRemainingSpot(remainmModel);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in suspendAdvert : {}", e.getMessage());
            throw e;
        }
    }

    //User can resume advertisement
    public Boolean resumeAdvert(int advertID) throws Exception {
        try {
            int suspentSeconds = 0;

            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            //Get suspend spot
            List<ScheduleDef> suspentSpotList = schedulerDao.getSuspendByAdvertID(advertID);

            //Calculate expire spot total duration.
            //Valid spot Active.
            for (int i = 0; i < suspentSpotList.size(); i++) {

                Date date = suspentSpotList.get(i).getDate();

                if (date.toString().equals(dateFormat.format(new Date()))) {
                    Date time = suspentSpotList.get(i).getActualstarttime();

                    if (time.before(timeFormat.parse(timeFormat.format(new Date())))) {

                        suspentSpotList.remove(suspentSpotList.get(i));
                        i--;

                    } else {

                        suspentSpotList.get(i).setStatus("0");
                        suspentSpotList.get(i).setComment("Active Suspend advertisement");
                       //schedulerDao.updateSchedule(suspentSpotList.get(i));
                    }
                } else {
                    if (date.before(new Date())) {
                        suspentSeconds = 0;
                        suspentSpotList.get(i).setStatus("5");
                        suspentSpotList.get(i).setComment("Active Suspend advertisement as misseed");
                       //schedulerDao.updateSchedule(suspentSpotList.get(i));
                        suspentSpotList.remove(suspentSpotList.get(i));
                        i--;
                    } else {

                        suspentSpotList.get(i).setStatus("0");
                        suspentSpotList.get(i).setComment("Active Suspend advertisement");
                        //schedulerDao.updateSchedule(suspentSpotList.get(i));
                    }
                }
            }
            schedulerDao.updateSchedule(suspentSpotList);
            //Generate play list after the remove scheduled spot
            if(suspentSpotList.size() != 0) {
                generatePlayListForScheduledSpots(suspentSpotList);
            }
                //Selected advertisement update as suspend advertisemant
            Advertisement advertModel = advertisementDao.getSelectedAdvertisement(advertID);
            advertModel.setEnabled(1);
            advertModel.setStatus(1);
            advertModel.setLastModifydate(getDataYYYMMDD_dash_HHmmss_colan(getDataYYYMMDD_dash_HHmmss_colan(new Date())));
            if (setAuditForAdvertisement(advertModel)) {

                //Save audit data
                //setAuditForAdvertisement(advertModel);
                advertisementDao.updateAdvertisement(advertModel);
            }

            RemainingSpotBank updateModel = remainingSpotBankDao.getSetectedRemainingSpot(1, advertID);
            if(updateModel != null) {
                updateModel.setRemainingSpot(suspentSeconds);
                remainingSpotBankDao.saveORUpdateRemainingSpot(updateModel);
            }
            return true;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementService in resumeAdvert : {}", e.getMessage());
            throw e;
        }
    }

    public Boolean generatePlayListForScheduledSpots(List<ScheduleDef> scheduleDefs)  {
        Map<Integer, List<PlaylistInputData>> scheduleDefGroups = new HashMap<>();
        for(ScheduleDef sd: scheduleDefs) {
            int channelId = sd.getChannelid().getChannelid();
            Date date = sd.getDate();
            Date startTime = sd.getSchedulestarttime();
            Date endTime = sd.getScheduleendtime();
            List<PlaylistInputData> playlistInputDataList = scheduleDefGroups.get(channelId);
            if(playlistInputDataList == null) {
                playlistInputDataList = new ArrayList<>();
                scheduleDefGroups.put(channelId, playlistInputDataList);
            }

            PlaylistInputData playlistInputData = null;
            if (!playlistInputDataList.isEmpty()) {
                for (PlaylistInputData pld : playlistInputDataList) {
                    if (pld.getChannelId() == channelId && pld.getDate().getTime() == date.getTime()) {
                        playlistInputData = pld;
                        break;
                    }
                }
            }
            if (playlistInputData != null) {
                if (playlistInputData.getStartTime().getTime() > startTime.getTime()) {
                    playlistInputData.setStartTime(startTime);
                }
                if (playlistInputData.getEndTime().getTime() < endTime.getTime()) {
                    playlistInputData.setEndTime(endTime);
                }
            } else {
                playlistInputDataList.add(new PlaylistInputData(channelId, date, startTime, endTime));
            }
        }
        List<CompletableFuture<Boolean>> allFutures = new ArrayList<>();
        for(Map.Entry<Integer, List<PlaylistInputData>> entry: scheduleDefGroups.entrySet()) {
            for(PlaylistInputData pd: entry.getValue()) {
                allFutures.add(schedulerService.setDailyScheduleAsync(pd.getChannelId(), pd.getDate(), pd.getDate(), pd.getStartTime()));
            }
        }
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());
        try {
            schedulerService.setPlayPermissionForWorkOrder(-102, dateFormat.parse(date));
        } catch (ParseException e) {
            logger.debug("Exception has occurred while generating playlist by the schedule spots: {}", e.getMessage());
            return false;
        }
        return true;
    }

    public Boolean setAuditForAdvertisement(Advertisement model) {
        try {
            Advertisement dataModel = advertisementDao.getAdvertisementForAudit(model.getAdvertid());
            for (Field field : (Advertisement.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null && (!field.get(model).equals(field.get(dataModel)))) {

                    GeneralAudit auditModel = new GeneralAudit("advertisement_def", model.getAdvertid());
                    auditModel.setField(field.getAnnotation(Column.class).name());
                    auditModel.setOldData(field.get(dataModel).toString());
                    auditModel.setNewData(field.get(model).toString());

                    generalAuditDao.saveGeneralAudit(auditModel);
                }
            }

            int oldClientId = dataModel.getClient().getClientid();
            int newClientId = model.getClient().getClientid();

            if (oldClientId != newClientId) {
                GeneralAudit auditModel = new GeneralAudit("advertisement_def", model.getAdvertid());
                auditModel.setField("Client");
                auditModel.setOldData(dataModel.getClient().getClientid() + "-" + dataModel.getClient().getClientname());
                auditModel.setNewData(model.getClient().getClientid() + "-" + model.getClient().getClientname());

                generalAuditDao.saveGeneralAudit(auditModel);
            }
            return true;
        } catch (IllegalAccessException e) {
            logger.debug("Exception in AdvertisementService in getMissedSpot: {}", e.getMessage());
        }
        return false;
    }

    public Page<Advertisement> getAllAdvertList(Map<String, String> filterRequest) {

        String sortString = filterRequest.get("sort");
        int page = Integer.parseInt(filterRequest.get("page"));
        int size = Integer.parseInt(filterRequest.get("size"));

        AdvertFilter advertFilter = new AdvertFilter();
        advertFilter.setAdvertId(filterRequest.get("adverId"));
        if (advertFilter.getAdvertId().trim().equals("")) {
            advertFilter.setAdvertId("-111");
        }
        advertFilter.setAdvertName(filterRequest.get("adverName"));
        if (advertFilter.getAdvertName().trim().equals("")) {
            advertFilter.setAdvertName("-111");
        }
        advertFilter.setAdvertCategory(filterRequest.get("advertCategory"));
        if (advertFilter.getAdvertCategory().trim().equals("")) {
            advertFilter.setAdvertCategory("-111");
        }
        advertFilter.setClient(filterRequest.get("client_id"));
        if (advertFilter.getClient().trim().equals("")) {
            advertFilter.setClient("-111");
        }

        Pageable pageable;

        if (sortString != null) {
            String[] sortParams = sortString.split(",");
            Sort sort = new Sort(Sort.Direction.fromString(sortParams[1]), sortParams[0]);
            pageable = new PageRequest(page, size, sort);
        } else {
            pageable = new PageRequest(page, size);
        }

        List<Advertisement> advertList = advertisementDao.getLimitedAdvert(pageable.getPageSize(), pageable.getOffset(), advertFilter);
        int advertCount = advertisementDao.getLimitedAdvertCount(advertFilter);
        for (Advertisement advertisement: advertList) {

            ScheduleDef model = schedulerDao.getMaximumSpotInAdvert(advertisement.getAdvertid());
            if (model == null) {
                advertisement.setCanDelete(Boolean.TRUE);
            } else {
                Calendar c = Calendar.getInstance();
                c.setTime(model.getDate());
                c.add(Calendar.DATE, 1);
                if (c.getTime().before(new Date())) {
                    advertisement.setCanDelete(Boolean.TRUE);
                } else {
                    advertisement.setCanDelete(Boolean.FALSE);
                }
            }
        }

        return new PageImpl<>(advertList, pageable, advertCount);
    }

    public List<GeneralAuditView> getAdvertHistory(int advertId) {
        List<GeneralAuditView> viewList = new ArrayList<>();
        List<GeneralAudit> auditDataList = generalAuditDao.getSelectedData("advertisement_def", advertId);

        for (GeneralAudit generalAudit : auditDataList) {
            GeneralAuditView generalAuditView = new GeneralAuditView();
            generalAuditView.setLogTime(generalAudit.getDateAndTime());
            String field = getAdvertFieldName(generalAudit.getField());
            generalAuditView.setField(field);
            if (field.equals("Status")) {
                generalAuditView.setOldData(getAdvertStatus(generalAudit.getOldData()));
                generalAuditView.setNewData(getAdvertStatus(generalAudit.getNewData()));
            } else {
                generalAuditView.setOldData(generalAudit.getOldData());
                generalAuditView.setNewData(generalAudit.getNewData());
            }
            generalAuditView.setUser(generalAudit.getUser());
            viewList.add(generalAuditView);
        }
        return viewList;

    }

    public String getAdvertFieldName(String field) {
        String name = "";

        switch (field) {
            case "Advert_name":
                name = "Advertisement Name";
                break;

            case "Advert_path":
                name = "Advertisement path";

                break;
            case "Advert_type":
                name = "Advertisement type";
                break;

            case "Commercial_category":
                name = "Commercial category";
                break;

            case "vedioType":
                name = "Video Type";
                break;

            case "LogoContainer_Id":
                name = "LogoContainer Id";
                break;

            case "File_Available":
                name = "File Available";
                break;

            case "create_date":
                name = "Create Date";
                break;

            case "expire_date":
                name = "Expire Date";
                break;

            case "suspend_date":
                name = "Suspend Date";
                break;

            case "user":
                name = "User";
                break;

            case "enabled":
                name = "Enabled";
                break;
            case "status":
                name = "Status";
                break;

            default:
                name = field;
                break;
        }

        return name;
    }

    public String getAdvertStatus(String status) {
        String stringStatus = "";

        switch (status) {
            case "1":
                stringStatus = "Activate";
                break;

            case "2":
                stringStatus = "Hold";

                break;
            case "3":
                stringStatus = "Deleted";
                break;

            case "0":
                stringStatus = "Commercial category";
                break;

            default:
                stringStatus = status;
                break;
        }

        return stringStatus;
    }

    /**********************methods for dashboard*******************************/

    public List<Advertisement> findDummyCutsForDashboard(){
        return advertisementDao.getDummyCutsForDashboard();
    }

    public Integer getSpotCountOfDummyCuts(Integer advertId){
        return advertisementDao.getSpotCountOfDummyCutsForDashboard(advertId);
    }

    public List<DummyCutListDto> createDummyCutListForDashboardWidget(){
        List<DummyCutListDto> dummyCutDetailList = new ArrayList<>();
        List<Advertisement> dummyCuts = findDummyCutsForDashboard();

        for (Advertisement dummyCut : dummyCuts){
            DummyCutListDto dummyCutList = new DummyCutListDto();
            dummyCutList.setCutId(dummyCut.getAdvertid());
            dummyCutList.setClient(dummyCut.getClient());
            dummyCutList.setCreateDate(dummyCut.getCreateDate());
            dummyCutList.setExpireDate(dummyCut.getExpireDate());
            dummyCutList.setSpots(getSpotCountOfDummyCuts(dummyCut.getAdvertid()));
            dummyCutList.setUser(dummyCut.getUser());
            dummyCutDetailList.add(dummyCutList);
        }

        return dummyCutDetailList;
    }

    public List<IngestStatusWidgetDto> findAllAdvertisementsForIngestStatusWidget(){
        return advertisementDao.findAllAdvertsForIngestStatusWidget();
    }

    public List<PriorityAdvertisementWidgetDto> findAdvertsForPriorityAdvertisementWidget(){
        return advertisementDao.findPriorityAdvertisementsDetails();
    }

    public Advertisement getAdvertisementWithMinDuration(List<Integer> adIds){
        return advertisementDao.getAdvertisementWithMinDuration(adIds);
    }
}