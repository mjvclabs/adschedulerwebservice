/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.ChannelDetailDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.view.DashboadChannelView;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class DashboardService extends DateFormat {

    @Autowired
    private ChannelDetailDAO channelDetailsDao;
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private DashboardAsyncService dashboardAsyncService;

    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardService.class);

    /*
     *
     *
     */
    public List<DashboadChannelView> getManualORAutoChannelList(int type, Date date) throws Exception {
        List<DashboadChannelView> channelViewList = new ArrayList<>();
        List<ChannelDetails> channelList = new ArrayList<>();
        // date = new Date();

        try {
            checkChannelUpdate(type);
            if (type == 1) {
                channelList = channelDetailsDao.getAllManualChannelList();
            } else {
                channelList = channelDetailsDao.getAllAutoChannelList();
            }
            List<CompletableFuture<DashboadChannelView>> allFutures = new ArrayList<>();
            for (ChannelDetails model : channelList) {
                allFutures.add(dashboardAsyncService.prepareDashboardDataByChannelAsync(model, date));
            }
            CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
            for(CompletableFuture<DashboadChannelView> result: allFutures) {
                DashboadChannelView dashboadChannelView = result.get();
                if(dashboadChannelView != null) {
                    channelViewList.add(dashboadChannelView);
                }
            }
            return channelViewList;
        } catch (Exception e) {
            LOGGER.debug("Exception in DashboardService while trying to getManualORAutoChannelList: {}", e.getMessage());
            throw e;
        }
    }

    public Boolean checkChannelUpdate(int type) {
        try {
            List<ChannelDetails> channelList = new ArrayList<>();
            if (type == 1) {
                channelList = channelDetailsDao.getAllManualChannelList();
            } else {
                channelList = channelDetailsDao.getAllAutoChannelList();
            }

            for (ChannelDetails model : channelList) {
                Calendar update_T = Calendar.getInstance();
                Calendar current_T = Calendar.getInstance();

                update_T.setTime(model.getLastUpdate());
                Date updatedTime = update_T.getTime();
                current_T.setTime(new Date());
                Date currentTime = current_T.getTime();

                long difference = currentTime.getTime() - updatedTime.getTime();
                if ((difference / 60000) >= 60) {
                    Date date = getDataYYYMMDD_dash_HHmmss_colan("2111-01-01 01:00:00.000");
                    model.setLastUpdate(date);
                    channelDetailsDao.updateChannel(model);
                }
            }
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in DashboardService while trying to checkChannelUpdate: {}", e.getMessage());
            return false;
        }
    }
}
