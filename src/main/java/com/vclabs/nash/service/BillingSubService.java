package com.vclabs.nash.service;

import com.vclabs.nash.controller.dto.WorkOrderBillRequest;
import com.vclabs.nash.model.dao.InvoiceDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.InvoiceDef;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.view.InvoiceView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
@Transactional("main")
public class BillingSubService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BillingSubService.class);

    @Value("${file.woBillTempDirectory}")
    private String tempWOBillDirectory;

    @Value("${file.woBillZipDirectory}")
    private String zipWOBillDirectory;

    @Autowired
    private BillingService billingService;

    @Autowired
    private WorkOrderDAO workOrderDAO;

    @Autowired
    private InvoiceDAO invoiceDao;

    public Path saveInvoice(WorkOrderBillRequest billRequest, HttpServletRequest request) throws IOException {
        Path billTempDirectoryPath = createDirectory();
        List<CompletableFuture<Boolean>> allFutures = new ArrayList<>();
        for (Integer woId : billRequest.getWoIds()) {
            WorkOrder workOrder = workOrderDAO.getSelectedWorkOrder(woId);
            if (workOrder.getBillingstatus() != 0) { //no need to regenerate bills once they are processed
                continue;
            }
            billingService.saveInvoice(workOrder, request, billRequest.isFullBill(), billTempDirectoryPath);
           // allFutures.add(billingService.saveInvoiceAsync(workOrder, request, billRequest.isFullBill(), billTempDirectoryPath));
        }
        //CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
        compressToZip(billTempDirectoryPath.toAbsolutePath().toString(), this.zipWOBillDirectory);
        FileSystemUtils.deleteRecursively(billTempDirectoryPath.toFile());
        return Paths.get(this.zipWOBillDirectory + File.separator + billTempDirectoryPath.getFileName() + ".zip");
    }

    private Path createDirectory() throws IOException {
        String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String randomNumber = new BigDecimal(new Random().nextDouble())
                .multiply(new BigDecimal(1000000000)).toBigInteger().toString();
        String directoryPath = time + "_" + randomNumber;
        Path path = Paths.get(tempWOBillDirectory + File.separator + directoryPath);
        Files.createDirectory(path);
        return path;
    }

    private String compressToZip(String folderPath, String zipLocation) {
        File directory = new File(folderPath);
        String zipFileLocation = zipLocation + File.separator + directory.getName() + ".zip";
        try (FileOutputStream fos = new FileOutputStream(zipFileLocation); ZipOutputStream zos = new ZipOutputStream(fos)){
            String[] srcFiles = directory.list();
            byte[] buffer = new byte[1024];
            if(srcFiles.length != 0 ){
                for (int i = 0; i < srcFiles.length; i++) {
                    File srcFile = new File(folderPath + File.separator + srcFiles[i]);
                    try(FileInputStream fis = new FileInputStream(srcFile)) {
                        zos.putNextEntry(new ZipEntry(srcFile.getName()));
                        int length;
                        while ((length = fis.read(buffer)) > 0) {
                            zos.write(buffer, 0, length);
                        }
                        zos.closeEntry();
                        fos.flush();
                    }
                }
            }
        }catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return zipFileLocation;
    }

    public ResponseEntity<ByteArrayResource> downloadFile(String fileName) throws IOException {
        Path zipFilePath = Paths.get(this.zipWOBillDirectory + File.separator + fileName);
        byte[] data = Files.readAllBytes(zipFilePath);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=" + zipFilePath.getFileName())
                .contentLength(data.length)
                .body(resource);
    }

    public List<InvoiceView> getAllInvoices() {
        List<InvoiceView> invoiceList = new ArrayList();
        List<CompletableFuture<Boolean>> allFutures = new ArrayList<>();
        List<InvoiceDef> dataList = invoiceDao.getAllInvoice();
        for (InvoiceDef model : dataList) {
            allFutures.add(billingService.addInvoiceAsync(invoiceList, model));
        }
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
        return invoiceList;
    }
}
