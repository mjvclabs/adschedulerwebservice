package com.vclabs.nash.service.playlist_preview;

import com.vclabs.nash.model.dao.TimeBeltDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.playlist_preview.PlayListTemp;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.service.ChannelDetailService;
import com.vclabs.nash.service.playlist_preview.vo.AdvertRow;
import com.vclabs.nash.service.playlist_preview.vo.DailySchedulePreview;
import com.vclabs.nash.service.playlist_preview.vo.TimeBeltGroup;
import com.vclabs.nash.service.utill.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dperera on 05/11/2019.
 */
@Service
@Transactional("main")
public class PlayListPreviewService {

    @Autowired
    private TimeBeltDAO timeBeltDao;

    @Autowired
    private PlayListTempService playListTempService;

    @Autowired
    private ChannelDetailService channelDetailService;

    @Transactional
    public List<DailySchedulePreview> generatePlayListPreview(int seqNum, int channelId, Date startDate, Date endDate, Date startTime, Date endTime){
        playListTempService.generatePlayList(seqNum, channelId,  startDate,  endDate,  startTime,  endTime);
        List<TimeBelts> timeBelts = timeBeltDao.getTimeBelts(channelId, startTime, endTime);
        for (LocalDate date = DateUtil.toLocalDate(startDate); date.isBefore(DateUtil.toLocalDate(endDate)) || date.isEqual(DateUtil.toLocalDate(endDate)); date = date.plusDays(1)) {
            for(TimeBelts timeBelt : timeBelts){
                playListTempService.generatePlayListBackEnd(channelId, DateUtil.fromLocalDate(date), timeBelt,  timeBelt.getHour(), seqNum);
            }
        }

        List<DailySchedulePreview> dailySchedulePreviews = new ArrayList<>();
        for (LocalDate date = DateUtil.toLocalDate(startDate); date.isBefore(DateUtil.toLocalDate(endDate)) || date.isEqual(DateUtil.toLocalDate(endDate)); date = date.plusDays(1)) {
            Date currentDate = DateUtil.fromLocalDate(date);
            DailySchedulePreview dailySchedulePreview = new DailySchedulePreview(currentDate);
            List<PlayListTemp> dailyScheduleList = playListTempService.getSchedulesByDate(channelId, currentDate, seqNum);
            for(PlayListTemp plt : dailyScheduleList){
                TimeBeltGroup group = dailySchedulePreview.getGroup(plt.getScheduleHour());
                AdvertRow ar = new AdvertRow();
                Advertisement ad = plt.getAdvert();
                ar.setChannel(plt.getChannel().getChannelname());
                ar.setAdvertName(ad.getAdvertname());
                ar.setClient(ad.getClient().getClientname());
                ar.setScheduleTime(plt.getSchedule().getSchedulestarttime().toString() + "-" + plt.getSchedule().getScheduleendtime().toString());
                ar.setDuration(ad.getDuration());
                ar.setCluster(plt.getPlayCluster().toString());
                ar.setType(ad.getAdverttype());
                group.addAdvertRow(ar);
            }
            dailySchedulePreviews.add(dailySchedulePreview);
        }
        return dailySchedulePreviews;
    }

}
