package com.vclabs.nash.service.playlist_preview;

import com.vclabs.nash.service.playlist_preview.vo.DailySchedulePreview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created by dperera on 08/11/2019.
 */
@Service
@Transactional("main")
public class PlayListPreviewAsyncService {

    @Autowired
    private PlayListPreviewService playListPreviewService;

    @Async("specificTaskExecutor")
    public CompletableFuture<List<DailySchedulePreview>> generatePlayListPreview(int seqNum, int channelId, Date startDate, Date endDate, Date startTime, Date endTime) {
        CompletableFuture<List<DailySchedulePreview>> completableFuture = new CompletableFuture<>();
        try{
            completableFuture.complete(playListPreviewService.generatePlayListPreview(seqNum,  channelId,  startDate,  endDate,  startTime,  endTime));
        }catch (Exception e){
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

}
