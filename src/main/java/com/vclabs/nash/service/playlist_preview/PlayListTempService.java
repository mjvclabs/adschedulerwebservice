package com.vclabs.nash.service.playlist_preview;

import com.vclabs.nash.model.dao.*;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.model.entity.playlist_preview.PlayListTemp;
import com.vclabs.nash.service.*;
import com.vclabs.nash.service.utill.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dperera on 22/10/2019.
 */
@Service
@Transactional("main")
public class PlayListTempService {

    @Autowired
    private PlaylistTempDAO playlistTempDAO;

    @Autowired
    private PlaylistTempSequenceDAO playlistTempSequenceDAO;

    @Autowired
    private TimeBeltDAO timeBeltDao;

    @Autowired
    private SchedulerDAO schedulerDao;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private AdvertisementDAO advertisementDao;

    @Autowired
    private PriorityDefDAO priorityDefDao;

    @Autowired
    private ChannelDetailService channelDetailService;

    @Autowired
    private ChannelAdvertMapService channelAdvertMapService;

    @Autowired
    private AdvertSettingService advertSettingService;

    @Autowired
    private PlayListService playListService;

    @Autowired
    private WOChannelSummaryService woChannelSummaryService;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void generatePlayList(int seqNum, int channelId, Date startDate, Date endDate, Date startTime, Date endTime){
        List<TimeBelts> timeBelts = timeBeltDao.getTimeBelts(channelId, startTime, endTime);
        for (LocalDate date = DateUtil.toLocalDate(startDate); date.isBefore(DateUtil.toLocalDate(endDate)) || date.isEqual(DateUtil.toLocalDate(endDate)); date = date.plusDays(1)) {
            createTempPlayList(DateUtil.fromLocalDate(date), timeBelts, seqNum);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean generatePlayListBackEnd(int channelId, Date date,  TimeBelts timeBelt, int hour, int sequenceNum){

        boolean bFlag = false;
        //ScheduleGenerate generator = new ScheduleGenerate(channelId, hour, timeBelt.getStartTime(), timeBelt.getEndTime(), timeBelt.getClusterCount(),
          //      advertisementDao.getFillerAdvertisements());

        ScheduleGenerateV2 generator = new ScheduleGenerateV2(channelId, hour, timeBelt.getStartTime(), timeBelt.getEndTime(), timeBelt.getClusterCount(),
                advertisementDao.getFillerAdvertisements(), channelAdvertMapService, playListService, advertSettingService);
        generator.setWoChannelSummaryService(woChannelSummaryService);
        List<ScheduleDef> lstScheduleList = schedulerDao.getSchedulePrioritybyTimeBelt(timeBelt.getTimeBeltId(), date);
        List<PriorityDef> lstPriorities = priorityDefDao.getPriorityListbyTimeBelt(timeBelt.getTimeBeltId());
        generator.setPrioritySchedulesList(lstScheduleList, lstPriorities);

        //generate advert play list
        ArrayList<PlayList> play_list_adverts = getAdvertPlayList(channelId, date, timeBelt.getStartTime(), timeBelt.getEndTime(), hour, sequenceNum);
        bFlag = generator.generatePlayListAdvert(play_list_adverts, 0);
        if (!bFlag) {
            return false;
        }
        ArrayList<ArrayList<PlayList>> lst_scheduled_adverts = generator.getLst_Schedule();
        for (int i = 0; i < lst_scheduled_adverts.size(); i++) {
            ArrayList<PlayList> lst_cluster = lst_scheduled_adverts.get(i);
            for (int j = 0; j < lst_cluster.size(); j++) {
                PlayList playList = lst_cluster.get(j);
                playList.setDate(date);
                updatePlayListTemp(playList, i, j, sequenceNum);
            }
        }

        //generate logo play list
        ArrayList<PlayList> play_list_logos = getPlayListLogo(channelId, date, timeBelt.getStartTime(), timeBelt.getEndTime(), hour, sequenceNum);
        for (PlayList playList : play_list_logos) {
            PlayListTemp playListTemp = getPlayList(playList.getPlaylistId());
            playListTemp.setStatus("1");
            playListTemp.setComment("schedule_generated");
            playlistTempDAO.save(playListTemp);
        }

        //generate crawler play list
        ArrayList<PlayList> play_list_crawlers = getPlayListCrawler(channelId, date, timeBelt.getStartTime(), timeBelt.getEndTime(), hour, sequenceNum);
        ChannelDetails pChannel = channelDetailService.getChannel(channelId);
        if (!pChannel.isIsManual()) {
            bFlag = generator.generatePlayListCrawler(play_list_crawlers);
            if (!bFlag) {
                return false;
            }
        } else {
            play_list_crawlers = generator.shufflePlayListCrawler(play_list_crawlers);
        }
        int iOrder = 0;
        for (PlayList playList : play_list_crawlers) {
            PlayListTemp playListTemp = getPlayList(playList.getPlaylistId());
            playListTemp.setStatus("1");
            playListTemp.setPlayOrder(iOrder++);
            playListTemp.setComment("schedule_generated");
            playlistTempDAO.save(playListTemp);
        }
        return true;
    }

    private void updatePlayListTemp(PlayList playList, int i, int j,  int sequenceNum){
        PlayListTemp playListTemp;
        if(playList.getPlaylistId() != null && playList.getLable() != PlayList.Lable.ZERO){
            playListTemp = getPlayList(playList.getPlaylistId());
        }else { //set filler properties
            playListTemp = new PlayListTemp();
            playListTemp.setScheduleEndTime(playList.getScheduleEndTime());
            playListTemp.setScheduleStartTime(playList.getScheduleStartTime());
            playListTemp.setTimeBeltEndTime(playList.getTimeBeltEndTime());
            playListTemp.setTimeBeltStartTime(playList.getTimeBeltStartTime());
            playListTemp.setActualEndTime(playList.getActualEndTime());
            playListTemp.setActualStartTime(playList.getActualStartTime());
            playListTemp.setScheduleHour(playList.getScheduleHour());
            playListTemp.setChannel(playList.getChannel());
            playListTemp.setAdvert(playList.getAdvert());
            playListTemp.setSchedule(playList.getSchedule());
            playListTemp.setWorkOrder(playList.getWorkOrder());
            playListTemp.setRetryCount(0);
            playListTemp.setDate(playList.getDate());
            playListTemp.setComment(playList.getComment());
            playListTemp.setSequenceNumber(sequenceNum);
        }
        playListTemp.setPlayCluster(i);
        playListTemp.setPlayOrder(j);
        playListTemp.setStatus("1");
        playListTemp.setComment("schedule_generated");
        playlistTempDAO.save(playListTemp);

        if(playList.getLable() == PlayList.Lable.ZERO){
            playListService.deletePlayList(playList.getPlaylistId()); //delete zero ad which are used in the playlist preview
        }

    }

    public PlayListTemp getPlayList(int playListId) {
        return playlistTempDAO.getPlayList(playListId);
    }

    private void createTempPlayList(Date date, List<TimeBelts> timeBelts, int sequenceNum){
        List<TimeSlotScheduleMap> selectedScheduleList =  schedulerService.findByDateAndTimeBelts(date, timeBelts);
        for (TimeSlotScheduleMap item : selectedScheduleList) {
            ScheduleDef dataModel = item.getScheduleid();
            PlayListTemp model = new PlayListTemp();
            model.setWorkOrder(dataModel.getWorkorderid());
            model.setAdvert(dataModel.getAdvertid());
            model.setDate(dataModel.getDate());
            model.setChannel(dataModel.getChannelid());
            model.setScheduleStartTime(dataModel.getSchedulestarttime());
            model.setScheduleEndTime(dataModel.getScheduleendtime());
            model.setSchedule(dataModel);
            model.addConflicting_schedules(1);

            model.setStatus(dataModel.getStatus());
            model.setComment("Status :" + model.getStatus() + " Time :" + new Date().toString());
            model.setPlayCluster(-1);
            model.setPlayOrder(-1);
            model.setActualEndTime(item.getTimeBelt().getEndTime());
            model.setActualStartTime(item.getTimeBelt().getStartTime());
            model.setScheduleHour(item.getTimeBelt().getHour());
            model.setTimeBeltEndTime(item.getTimeBelt().getEndTime());
            model.setTimeBeltStartTime(item.getTimeBelt().getStartTime());
            model.setSequenceNumber(sequenceNum);
            playlistTempDAO.save(model);
        }
    }

    public ArrayList<PlayList> getAdvertPlayList(int channelId, Date date, Date startTime, Date endTime, int hour, int sequenceNum){
        return convertToPlayList(playlistTempDAO.getAdvertPlayList(channelId, date, startTime, endTime, hour, sequenceNum));
    }

    public ArrayList<PlayList> getPlayListLogo(int channelId, Date date, Date startTime, Date endTime, int hour, int sequenceNum){
        return convertToPlayList(playlistTempDAO.getAdvertPlayList(channelId, date, startTime, endTime, hour, sequenceNum));
    }

    public ArrayList<PlayList> getPlayListCrawler(int channelId, Date date, Date startTime, Date endTime, int hour, int sequenceNum){
        return convertToPlayList(playlistTempDAO.getPlayListCrawler(channelId, date, startTime, endTime, hour, sequenceNum));
    }

    private ArrayList<PlayList> convertToPlayList(List<PlayListTemp> playListTemps){
        ArrayList<PlayList> playLists = new ArrayList<>();
        for(PlayListTemp tempItem : playListTemps){
            PlayList model = new PlayList();
            model.setPlaylistId(tempItem.getId());
            model.setWorkOrder(tempItem.getWorkOrder());
            model.setAdvert(tempItem.getAdvert());
            model.setDate(tempItem.getDate());
            model.setChannel(tempItem.getChannel());
            model.setScheduleStartTime(tempItem.getScheduleStartTime());
            model.setScheduleEndTime(tempItem.getScheduleEndTime());
            model.setSchedule(tempItem.getSchedule());
            model.addConflicting_schedules(1);

            model.setStatus(tempItem.getStatus());
            model.setComment("Status :" + model.getStatus() + " Time :" + new Date().toString());
            model.setPlayCluster(-1);
            model.setPlayOrder(-1);
            model.setActualEndTime(tempItem.getActualEndTime());
            model.setActualStartTime(tempItem.getActualStartTime());
            model.setScheduleHour(tempItem.getScheduleHour());
            model.setTimeBeltEndTime(tempItem.getTimeBeltEndTime());
            model.setTimeBeltStartTime(tempItem.getTimeBeltStartTime());
            playLists.add(model);
        }
        return playLists;
    }

    @Transactional
    public List<PlayListTemp> getSchedulesByDate(int channelId, Date date, int sequenceNumber){
        return playlistTempDAO.getSchedulesByDate(channelId, date, sequenceNumber);
    }
}