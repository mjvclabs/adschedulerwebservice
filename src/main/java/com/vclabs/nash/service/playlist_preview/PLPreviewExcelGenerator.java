package com.vclabs.nash.service.playlist_preview;

import com.vclabs.nash.model.process.excel.Common;
import com.vclabs.nash.service.PlayListService;
import com.vclabs.nash.service.playlist_preview.vo.AdvertRow;
import com.vclabs.nash.service.playlist_preview.vo.DailySchedulePreview;
import com.vclabs.nash.service.playlist_preview.vo.TimeBeltGroup;
import com.vclabs.nash.service.utill.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.apache.poi.ss.usermodel.CellStyle.ALIGN_CENTER;
import static org.apache.poi.ss.usermodel.CellStyle.BORDER_THIN;

/**
 * Created by dperera on 07/11/2019.
 */
@Component
public class PLPreviewExcelGenerator extends Common {

    private static final Logger LOGGER = LoggerFactory.getLogger(PLPreviewExcelGenerator.class);

    private final String WEB_DIRECTORY = "preview";
    private final String FILE_NAME_PREFIX = "playlist_preview_";

    private HSSFWorkbook workBook;
    private CellStyle tableHeaderStyle;
    private CellStyle bodyCellStyle;
    private CellStyle bodyCellStyle2; //vertical center style

    public void generateSchedulePreviewExcel(HttpServletRequest request, Map<String, List<DailySchedulePreview>> sm, int seqNum ){

        this.workBook = new HSSFWorkbook();
        this.tableHeaderStyle = tableHeaderStyle(workBook);
        this.bodyCellStyle = setDataRowCellStyle(workBook);
        this.bodyCellStyle2 = setDataRowCellStyle2(workBook);

        for(Map.Entry<String, List<DailySchedulePreview>> entry : sm.entrySet()){
            createSheet(entry.getKey(), entry.getValue());
        }

        String oFileName = FILE_NAME_PREFIX + seqNum + ".xls" ;
        String oFilePath =  getFilePath(request, WEB_DIRECTORY, oFileName, true);

        try {
            File file = new File(oFilePath);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            workBook.write(fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void createSheet(String channelName, List<DailySchedulePreview> dailySchedulePreviews){

        HSSFSheet sheet =  this.workBook.createSheet(channelName);
        HSSFRow headerRow = sheet.createRow((short) 0);

        String[] headers = { "Date", "Time Belt", "Channel", "Advert Name", "Client", "Scheduled Time", "Duration(Sec)", "Cluster", "Type" };
        for(int i = 0; i < headers.length; i++){
            String labelName = headers[i];
            Cell cell =  headerRow.createCell(i);
            cell.setCellValue(labelName);
            cell.setCellStyle(tableHeaderStyle);
        }

        int rowCount = 1;
        for(DailySchedulePreview dsp : dailySchedulePreviews){
            boolean isNewDate = true;
            HSSFRow row = sheet.createRow((short) rowCount);
            if(dsp.getRecordsCount() > 0){
                sheet.addMergedRegion(new CellRangeAddress(rowCount,(rowCount + dsp.getRecordsCount() - 1),0,0));
            }
            rowCount++;
            Cell cell = row.createCell(0); // col 0
            cell.setCellValue(DateUtil.vsDateFormat(dsp.getDate()));
            cell.setCellStyle(bodyCellStyle2);

            for(int i = 0; i < dsp.getTimeBeltGroups().size(); i++){
                TimeBeltGroup group = dsp.getTimeBeltGroups().get(i);
                int fRow = rowCount;
                if(isNewDate){
                    isNewDate = false;
                    fRow = rowCount - 1;
                }
                int lRow = rowCount + group.getAdvertRows().size() - 1;
                sheet.addMergedRegion(new CellRangeAddress(fRow, lRow,1,1));
                if(i != 0){
                    row = sheet.createRow((short) rowCount);
                    rowCount++;
                }
                cell = row.createCell(1);
                cell.setCellValue(group.getTimeBelt());   //col 1
                cell.setCellStyle(bodyCellStyle2);

                for(int j = 0; j < group.getAdvertRows().size(); j++){
                    AdvertRow ar = group.getAdvertRows().get(j);
                    if(j != 0){
                        row = sheet.createRow((short) rowCount);
                        rowCount++;
                    }
                    cell = row.createCell(2);
                    cell.setCellValue(ar.getChannel());         //col 2
                    cell.setCellStyle(bodyCellStyle);

                    cell = row.createCell(3);
                    cell.setCellValue(ar.getAdvertName());      //col 3
                    cell.setCellStyle(bodyCellStyle);

                    cell = row.createCell(4);
                    cell.setCellValue(ar.getClient());          //col 4
                    cell.setCellStyle(bodyCellStyle);

                    cell = row.createCell(5);
                    cell.setCellValue(ar.getScheduleTime());    //col 5
                    cell.setCellStyle(bodyCellStyle);

                    cell = row.createCell(6);
                    cell.setCellValue(ar.getDuration());        //col 6
                    cell.setCellStyle(bodyCellStyle);

                    cell = row.createCell(7);
                    cell.setCellValue(ar.getCluster());         //col 7
                    cell.setCellStyle(bodyCellStyle);

                    cell = row.createCell(8);
                    cell.setCellValue(ar.getType());            //col 8
                    cell.setCellStyle(bodyCellStyle);
                }
            }

        }
    }

    public CellStyle setDataRowCellStyle2(HSSFWorkbook workBook) {
        CellStyle style;
        Font monthFont = workBook.createFont();
        monthFont.setFontHeightInPoints((short) 11);
        monthFont.setColor(IndexedColors.WHITE.getIndex());
        style = workBook.createCellStyle();
        style.setAlignment(ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setWrapText(true);
        style.setBorderRight(BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    public void downloadSchedulePreviewFile(HttpServletResponse response, HttpServletRequest request, int seqNum){
        String oFileName = FILE_NAME_PREFIX + seqNum + ".xls" ;
        String oFilePath =  getFilePath(request, WEB_DIRECTORY, oFileName, false);
        downloadFile(response, oFilePath);

    }
}
