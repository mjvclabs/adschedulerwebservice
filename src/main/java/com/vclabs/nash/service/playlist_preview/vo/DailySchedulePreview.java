package com.vclabs.nash.service.playlist_preview.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dperera on 06/11/2019.
 */
public class DailySchedulePreview {

    private Date date;

    private List<TimeBeltGroup> timeBeltGroups;

    private int recordsCount;

    public DailySchedulePreview(Date date) {
        this.date = date;
        this.timeBeltGroups = new ArrayList<>();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<TimeBeltGroup> getTimeBeltGroups() {
        return timeBeltGroups;
    }

    public void setTimeBeltGroups(List<TimeBeltGroup> timeBeltGroups) {
        this.timeBeltGroups = timeBeltGroups;
    }

    public TimeBeltGroup getGroup(int scheduleHour){
        for(TimeBeltGroup group : timeBeltGroups){
            if(group.getScheduleHour() == scheduleHour){
                return group;
            }
        }
        TimeBeltGroup group = new TimeBeltGroup(scheduleHour);
        timeBeltGroups.add(group);
        return group;
    }

    public int getRecordsCount() {
        for(TimeBeltGroup group : timeBeltGroups){
            this.recordsCount += group.getAdvertRows().size();
        }
        return recordsCount;
    }
}
