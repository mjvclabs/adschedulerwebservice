package com.vclabs.nash.service.playlist_preview.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dperera on 06/11/2019.
 */
public class TimeBeltGroup {

    private int scheduleHour;

    private String timeBelt;

    private List<AdvertRow> advertRows;

    public TimeBeltGroup(int scheduleHour) {
        this.scheduleHour = scheduleHour;
        this.advertRows = new ArrayList<>();
        setTimeBelt();
    }

    private void setTimeBelt(){
        this.timeBelt = scheduleHour + ":00 - " + (scheduleHour + 1) +  ":00" ;
    }

    public int getScheduleHour() {
        return scheduleHour;
    }

    public void setScheduleHour(int scheduleHour) {
        this.scheduleHour = scheduleHour;
    }

    public String getTimeBelt() {
        return timeBelt;
    }

    public void setTimeBelt(String timeBelt) {
        this.timeBelt = timeBelt;
    }

    public List<AdvertRow> getAdvertRows() {
        return advertRows;
    }

    public void setAdvertRows(List<AdvertRow> advertRows) {
        this.advertRows = advertRows;
    }

    public void addAdvertRow(AdvertRow ar) {
        this.advertRows.add(ar);
    }
}
