package com.vclabs.nash.service.playlist_preview;

import com.vclabs.nash.model.dao.PlaylistTempSequenceDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.PlaylistTempSequence;
import com.vclabs.nash.model.process.excel.Common;
import com.vclabs.nash.service.ChannelDetailService;
import com.vclabs.nash.service.playlist_preview.vo.DailySchedulePreview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Created by dperera on 08/11/2019.
 */
@Service
@Transactional("main")
public class PlayListPreviewHandler extends Common{

    @Autowired
    private PlaylistTempSequenceDAO playlistTempSequenceDAO;

    @Autowired
    private ChannelDetailService channelDetailService;

    @Autowired
    private PlayListPreviewAsyncService playListTempAsyncService;

    @Autowired
    private PLPreviewExcelGenerator plPreviewExcelGenerator;

    @Transactional
    public int generateDailySchedulesForPreview(HttpServletRequest request, List<Integer> channelIds, Date startDate, Date endDate, Date startTime, Date endTime ) throws ExecutionException, InterruptedException {
        PlaylistTempSequence plts = playlistTempSequenceDAO.save(new PlaylistTempSequence());
        Map<String, List<DailySchedulePreview>> sm = new HashMap<>();
        List<CompletableFuture< List<DailySchedulePreview>>> allFutures = new ArrayList<>();
        List<String> channels = new ArrayList<>();

        for(int cid : channelIds){
            CompletableFuture<List<DailySchedulePreview>> future = playListTempAsyncService.generatePlayListPreview(plts.getId(), cid, startDate, endDate, startTime, endTime);
            allFutures.add(future);
            ChannelDetails cd = channelDetailService.getChannel(cid);
            channels.add(cd.getChannelname());
        }
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
        for(int i = 0; i < channels.size(); i++){
            String channelName = channels.get(i);
            CompletableFuture<List<DailySchedulePreview>> future = allFutures.get(i);
            sm.put(channelName, future.get());
        }
        plPreviewExcelGenerator.generateSchedulePreviewExcel(request, sm, plts.getId());
        return plts.getId();
    }

    public void downloadSchedulePreviewFile(HttpServletResponse response, HttpServletRequest request, int seqNum){
        plPreviewExcelGenerator.downloadSchedulePreviewFile(response,  request,  seqNum);
    }
}
