package com.vclabs.nash.service.vo;

import java.util.Date;

public class PlaylistInputData {

    private Integer channelId;

    private Date date;

    private Date startTime;

    private Date endTime;

    public PlaylistInputData(Integer channelId, Date date, Date startTime, Date endTime) {
        this.channelId = channelId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
