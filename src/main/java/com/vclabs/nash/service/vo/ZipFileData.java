package com.vclabs.nash.service.vo;

public class ZipFileData {

    private String fileName;

    public ZipFileData(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
