package com.vclabs.nash.service.vo;

import com.vclabs.nash.model.entity.PlayList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dperera on 27/09/2019.
 */
public class ScheduleGenerateInput {

    private List<PlayList> lstCluster;

    private List<PlayList> lstPriority;

    private List<PlayList> zbCluster;

    private PlayList tmpItem;

    private PlayList previousSchedule;

    private  int tmpAdvertCount;

    private int indexCounter;

    private int i;

    public ScheduleGenerateInput(List<PlayList> lstCluster, List<PlayList> lstPriority, PlayList tmpItem,
                                 PlayList previousSchedule, int tmpAdvertCount, int indexCounter, int i, List<PlayList> zbCluster) {
        this.lstCluster = lstCluster;
        this.lstPriority = lstPriority;
        this.tmpItem = tmpItem;
        this.previousSchedule = previousSchedule;
        this.tmpAdvertCount = tmpAdvertCount;
        this.indexCounter = indexCounter;
        this.i = i;
        this.zbCluster = zbCluster;
    }

    public List<PlayList> getLstCluster() {
        return lstCluster;
    }

    public List<PlayList> getLstPriority() {
        return lstPriority;
    }

    public PlayList getTmpItem() {
        return tmpItem;
    }

    public PlayList getPreviousSchedule() {
        return previousSchedule;
    }

    public int getTmpAdvertCount() {
        return tmpAdvertCount;
    }

    public int getIndexCounter() {
        return indexCounter;
    }

    public int getI() {
        return i;
    }

    public void setLstCluster(List<PlayList> lstCluster) {
        this.lstCluster = lstCluster;
    }

    public void setLstPriority(List<PlayList> lstPriority) {
        this.lstPriority = lstPriority;
    }

    public void setTmpItem(PlayList tmpItem) {
        this.tmpItem = tmpItem;
    }

    public void setPreviousSchedule(PlayList previousSchedule) {
        this.previousSchedule = previousSchedule;
    }

    public void setTmpAdvertCount(int tmpAdvertCount) {
        this.tmpAdvertCount = tmpAdvertCount;
    }

    public void setIndexCounter(int indexCounter) {
        this.indexCounter = indexCounter;
    }

    public void setI(int i) {
        this.i = i;
    }

    public List<PlayList> getZbCluster() {
        return zbCluster;
    }

    public void setZbCluster(ArrayList<PlayList> zbCluster) {
        this.zbCluster = zbCluster;
    }
}
