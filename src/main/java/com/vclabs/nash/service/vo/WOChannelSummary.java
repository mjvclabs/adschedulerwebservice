package com.vclabs.nash.service.vo;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by dperera on 29/04/2020.
 *
 * select sum(ad.duration) as consumedDuration from schedule_def sd left join advertisement_def ad on sd.Advert_id = ad.Advert_id
 where sd.Channel_id = 2 and sd.Work_order_id = 2521 and sd.Status = 2 and ad.Advert_type = 'ADVERT'
 */
public class WOChannelSummary {

    private int workOrderId;

    private double totalBudget = 0;

    private int channelId;

    private int consumedDuration;

    private int paidDuration;

    private boolean isZeroBudget;

    private int bonusDuration;

    public WOChannelSummary(int workOrderId, BigDecimal consumedDuration) {
        this.workOrderId = workOrderId;
        this.consumedDuration = consumedDuration.intValue();
    }

    public WOChannelSummary(int channelId, int workOrderId, BigInteger paidDuration, BigInteger bonusDuration) {
        this.channelId = channelId;
        this.workOrderId = workOrderId;
        this.paidDuration = paidDuration.intValue();
        this.bonusDuration = bonusDuration.intValue();
    }


    public WOChannelSummary(double totalBudget, int channelId, int workOrderId, BigInteger paidDuration, BigInteger bonusDuration) {
        this.totalBudget = totalBudget;
        this.channelId = channelId;
        this.workOrderId = workOrderId;
        this.paidDuration = paidDuration.intValue();
        this.bonusDuration = bonusDuration.intValue();
    }


    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getConsumedDuration() {
        return consumedDuration;
    }

    public void setConsumedDuration(int consumedDuration) {
        this.consumedDuration = consumedDuration;
    }

    public int getPaidDuration() {
        return paidDuration;
    }

    public void setPaidDuration(int paidDuration) {
        this.paidDuration = paidDuration;
    }

    public int getBonusDuration() {
        return bonusDuration;
    }

    public void setBonusDuration(int bonusDuration) {
        this.bonusDuration = bonusDuration;
    }

    public boolean isZeroBudget() {
        return isZeroBudget;
    }

    public void setZeroBudget(boolean zeroBudget) {
        isZeroBudget = zeroBudget;
    }

    public double getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(double totalBudget) {
        this.totalBudget = totalBudget;
    }
}
