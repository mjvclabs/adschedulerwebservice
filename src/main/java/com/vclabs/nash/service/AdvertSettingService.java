package com.vclabs.nash.service;

import com.vclabs.nash.model.dao.Advertisement.AdvertSettingDAO;
import com.vclabs.nash.model.entity.AdvertSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dperera on 16/09/2019.
 */
@Service
@Transactional("main")
public class AdvertSettingService {

    @Autowired
    private AdvertSettingDAO advertSettingDAO;

    public int finMaxAllowedSlots(int cid, int duration){
        AdvertSetting setting = advertSettingDAO.finMaxAllowedSlots(cid, duration);
        return setting != null ? setting.getMaxSlots() : -1;
    }

    public int findMinLapDuration(int cid, int duration){
        AdvertSetting setting = advertSettingDAO.findLapDuration(cid, duration);
        return setting != null ? setting.getLap() : -1;
    }

    public AdvertSetting save(AdvertSetting advertSetting){
        if(!advertSettingDAO.hasIntersection(advertSetting)){
            return advertSettingDAO.save(advertSetting);
        }
        return null;
    }

    public List<AdvertSetting> finByChannelId(int cid,  AdvertSetting.Type type){
        return advertSettingDAO.finByChannelId(cid, type);
    }

    public Boolean delete(AdvertSetting advertSetting){
        return advertSettingDAO.delete(advertSetting);
    }

    public int getMaxLapDuration(int channelId) {
        return advertSettingDAO.getMaxLapDuration(channelId);
    }
}
