/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

import static org.hibernate.internal.util.io.StreamCopier.BUFFER_SIZE;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.DocumentException;
import com.vclabs.nash.model.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javax.persistence.Column;

import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ClientDetails;
import com.vclabs.nash.model.entity.GeneralAudit;
import com.vclabs.nash.model.entity.InvoiceData;
import com.vclabs.nash.model.entity.InvoiceDef;
import com.vclabs.nash.model.entity.InvoiceSpotRecord;
import com.vclabs.nash.model.entity.InvoiceTaxDetails;
import com.vclabs.nash.model.entity.SapData;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TaxDef;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import com.vclabs.nash.model.process.InvoiceDataInfo;
import com.vclabs.nash.model.process.InvoiceInfo;
import com.vclabs.nash.model.process.PrintInvoicePro;
import com.vclabs.nash.model.process.SAPChannelSpotInfo;
import com.vclabs.nash.model.process.SAPData;
import com.vclabs.nash.model.process.TRDetails;
import com.vclabs.nash.model.view.BillingView;
import com.vclabs.nash.model.view.InvoiceView;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class BillingService extends DateFormat {

    @Autowired
    private WorkOrderChannelListDAO workOrderChannelListDao;
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private WorkOrderService workOrderService;
    @Autowired
    private TaxDAO taxDao;
    @Autowired
    private InvoiceDAO invoiceDao;
    @Autowired
    private InvoiceSpotRecordDAO invoiceSpotRecordDao;
    @Autowired
    private InvoiceTaxDetailsDAO invoiceTaxDetailsDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    private ChannelDetailDAO channelDetailDao;
    @Autowired
    UserDetailsDAO userDetailsDao;
    @Autowired
    InvoiceDataDAO invoiceDataDao;

    @Value("${file.sap.copy.path}")
    private String SAP_FILE_COPY_FILE_PATH;

    @Value("${file.sap.path}")
    private String SAP_FILE_PATH;

    private static final Logger logger = LoggerFactory.getLogger(BillingService.class);


    public List<BillingView> getBillingDetails(int workOrderId) {

        List<WorkOrderChannelList> orderChannelList = null;
        orderChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId);

        List<ScheduleDef> scheduleList = null;
        scheduleList = schedulerDao.getAirSpot(workOrderId);

        List<BillingView> billViewList = new ArrayList<>();

        for (WorkOrderChannelList orderList : orderChannelList) {

            BillingView model = new BillingView();
            model.setChannelId(orderList.getChannelid().getChannelid());
            model.setChannelName(orderList.getChannelid().getShortname());

            model.setTvcSpots(orderList.getNumbertofspot());
            model.setTvcPackageAmount(orderList.getTvcpackagevalue());
            model.setTvcPackageAvarage();

            model.setCrowlerSpots(orderList.getCrowlerspots());
            model.setCrowlerPackageAmount(orderList.getCrowlerpackagevalue());
            model.setCrowlerPackageAvarage();

            model.setLogoSpots(orderList.getLogospots());
            model.setLogoSpotsPackageAmount(orderList.getLogopackagevalue());
            model.setLogoSpotsPackageAvarage();

            model.setL_sqeezeSpots(orderList.getLsqeezespots());
            model.setL_sqeezePackageAmount(orderList.getLsqeezpackagevalue());
            model.setL_sqeezePackageAvarage();

            model.setV_sqeezeSpots(orderList.getVsqeezspots());
            model.setV_sqeezePackageAmount(orderList.getVsqeezpackagevalue());
            model.setV_sqeezePackageAvarage();

            billViewList.add(model);
        }

        for (ScheduleDef schedule : scheduleList) {

            if (schedule.getStatus().equals("2")) {

                for (BillingView billing : billViewList) {
                    if (schedule.getChannelid().getChannelid() == billing.getChannelId()) {
                        switch (schedule.getAdvertid().getAdverttype()) {
                            case "ADVERT":
                                billing.addTVC(schedule.getAdvertid().getDuration());
                                break;

                            case "LOGO":
                                billing.addLOGO();
                                break;

                            case "CRAWLER":
                                billing.addCROWLER();
                                break;

                            case "V_SHAPE":
                                billing.addV_SQEEZE();
                                break;

                            case "L_SHAPE":
                                billing.addL_SQEEZE();
                                break;
                        }
                        billing.setLogoAirPackageAmount();
                        billing.setTvcAirPackageAmount();
                        billing.setCrowlerAirPackageAmount();
                        billing.setL_sqeezeAirPackageAmount();
                        billing.setV_sqeezeAirPackageAmount();
                        billing.setTotalAmount();
                        break;
                    }
                }
            }

        }
        return billViewList;
    }

    public List<TaxDef> getAllTaxDetails() {
        return taxDao.getAllTaxDetails();
    }

    public TaxDef getAgencyCommission() {
        return taxDao.getSelectedTAx(5);
    }

    public int saveInvoice(String invoiceData, HttpServletRequest request) throws Exception {
        try {
            ObjectMapper m = new ObjectMapper();
            InvoiceInfo dataModel = m.readValue(invoiceData, InvoiceInfo.class);

            WorkOrder workorder = workOrderDao.getSelectedWorkOrder(dataModel.getWorkOrderId());
            if (workorder.getBillingstatus() != 0) {
                return 0;
            }
            return saveInvoice(workorder, dataModel, request, null, false);
        } catch (Exception e) {
            logger.debug("Exception in BillingService in saveInvoice() : {}", e.getMessage());
            throw e;
        }
    }

    @Async("billingServiceExecutor")
    public CompletableFuture<Boolean> saveInvoiceAsync(WorkOrder workOrder, HttpServletRequest request, boolean isFullBill, Path billTempDirectoryPath) {
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        try{
            completableFuture.complete(saveInvoice(workOrder, request, isFullBill, billTempDirectoryPath));
        }catch (Exception e){
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }
    // For Jira issue NRM-819
    boolean isScheduledBill = true;
    // End

    public Boolean saveInvoice(WorkOrder workOrder, HttpServletRequest request, boolean isFullBill, Path billTempDirectoryPath) {
        //Following logic rules are implemented similar to the front end logic rules

        // For Jira issue NRM-819
        isScheduledBill = isFullBill;
        // End

        int woPermissionStatus = workOrder.getPermissionstatus();
        int selectedInvoiceStatus = 0;
        if (woPermissionStatus == 0 || woPermissionStatus == 1) {
            selectedInvoiceStatus = 2;
        } else if (woPermissionStatus == 2) {
            selectedInvoiceStatus = 3;
        }
        //reused same logic which are used in the front end side
        int invoiceType = isFullBill ? (selectedInvoiceStatus + 1) : (selectedInvoiceStatus + 2);
        List<BillingView> billingViews = getBillingDetails(workOrder.getWorkorderid());
        double totalAmount = 0;
        // For Jira issue NRM-819
        if (!isScheduledBill){
            for (BillingView bv : billingViews) {
                totalAmount += bv.getTvcAirPackageAmount();
                totalAmount += bv.getLogoAirPackageAmount();
                totalAmount += bv.getCrowlerAirPackageAmount();
                totalAmount += bv.getV_sqeezeAirPackageAmount();
                totalAmount += bv.getL_sqeezeAirPackageAmount();
            }
        }
        // End
        else {
            for (BillingView bv : billingViews) {
                totalAmount += bv.getTvcPackageAmount();
                totalAmount += bv.getLogoSpotsPackageAmount();
                totalAmount += bv.getCrowlerPackageAmount();
                totalAmount += bv.getV_sqeezePackageAmount();
                totalAmount += bv.getL_sqeezePackageAmount();
            }
        }
        InvoiceInfo dataModel = new InvoiceInfo();
        dataModel.setWorkOrderId(workOrder.getWorkorderid());
        dataModel.setInvoiceType(invoiceType);
        dataModel.setTotalAmount(totalAmount);
        dataModel.setAdjustment(0); //default value is 0
        dataModel.setCommissionRate(workOrder.getCommission());
        try {
            String filePath = billTempDirectoryPath.toAbsolutePath().toString() + File.separator + workOrder.getWorkorderid() + ".pdf";
            saveInvoice(workOrder, dataModel, request, filePath, isFullBill);
        } catch (IOException | DocumentException e) {
            logger.error(e.getMessage());
        }
        return true;
    }

    private int saveInvoice(WorkOrder workorder, InvoiceInfo dataModel, HttpServletRequest request, String filePath, boolean isFullBill) throws IOException, DocumentException {
        InvoiceDef model = new InvoiceDef();
        model.setBillingclient("" + workorder.getBillclient());
        model.setClient("" + workorder.getClient());
        model.setCommissionclient("" + workorder.getAgencyclient());
        model.setCommissionrate(dataModel.getCommissionRate());
        model.setDate(new Date());
        model.setInvoicetype(dataModel.getInvoiceType());
        model.setTotalamount("" + dataModel.getTotalAmount());
        model.setAdjustAmount("" + dataModel.getAdjustment());
        model.setPrintstatus(0);
        model.setWorkorderid(workorder);
        model.setWorkOrdPeriod(workorder.getStartdate().toString() + " - " + workorder.getEnddate().toString());
        model.setSapFilePath("Not_generate");
        workorder.setBillingstatus(1);
        workorder.setPermissionstatus(3);
        workorder.setAutoStatus(WorkOrder.AutoStatus.CompleteInvoicing);

        if (workOrderService.setAuditForWorkOrder(workorder)) {
            workOrderDao.upadteWorkOrder(workorder);
        }
        TRDetails trModel = new TRDetails();
        ClientDetails billingClient = clientDetailDao.getSelectedClient(workorder.getBillclient());

        trModel.setRegNO(billingClient.getVat_no());
        trModel.setAdvertiser("");
        trModel.setMarketingEx(userDetailsDao.getUserDetails(workorder.getSeller()).getName());
        if (workorder.getAgencyclient() != 0) {
            ClientDetails asgencyClient = clientDetailDao.getSelectedClient(workorder.getAgencyclient());
            trModel.setAgency(asgencyClient.getClientname());
        }

        if (invoiceDao.saveInvoice(model)) {
            if (saveInvoiceContentDetails(model)) {
                if (saveInvoiceTaxDetails(model)) {
                    generateSAPFile(model, request);
                    // return false;
                    new PDFGenerator().transmissionReportPDFGenerator_V2(schedulerDao.allChannelsNotAllWorkOrders(workorder.getWorkorderid(), workorder.getStartdate(), workorder.getEnddate()), trModel, request);
                    if(filePath == null) {
                        new PDFGenerator().billPDFGenerator(request, getPrintInvoicePro(model));
                    } else {
                        new PDFGenerator().billPDFGenerator(request, getPrintInvoicePro(model), filePath);
                    }
                    return model.getInvoiceid();
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public Boolean saveInvoiceContentDetails(InvoiceDef model) {
        try {
            List<BillingView> dataList = getBillingDetails(model.getWorkorderid().getWorkorderid());
            for (BillingView bill : dataList) {
                if (bill.getTvcSpots() > 0) {
                    InvoiceSpotRecord spotRecord = new InvoiceSpotRecord();
                    spotRecord.setAirSpot(bill.getTvcAir());
                    spotRecord.setAllSpots(bill.getTvcSpots());
                    ChannelDetails channel = channelDetailDao.getChannel(bill.getChannelId());
                    spotRecord.setChannelId(channel);
                    spotRecord.setContentType("TVC");
                    if (!isScheduledBill){   // For Jira issue NRM-819
                        spotRecord.setPackageAmount("" + bill.getTvcAirPackageAmount());
                    } else {
                        spotRecord.setPackageAmount("" + bill.getTvcPackageAmount());
                    }
                    spotRecord.setInvoiceId(model);

                    invoiceSpotRecordDao.saveInvoiceSpotRecord(spotRecord);

                }
                if (bill.getCrowlerSpots() >  0) {
                    InvoiceSpotRecord spotRecord = new InvoiceSpotRecord();
                    spotRecord.setAirSpot(bill.getCrowlerAir());
                    spotRecord.setAllSpots(bill.getCrowlerSpots());
                    ChannelDetails channel = channelDetailDao.getChannel(bill.getChannelId());
                    spotRecord.setChannelId(channel);
                    spotRecord.setContentType("Crowler");
                    if (!isScheduledBill){   //For Jira issue NRM-819
                        spotRecord.setPackageAmount("" + bill.getCrowlerAirPackageAmount());
                    } else {
                        spotRecord.setPackageAmount("" + bill.getCrowlerPackageAmount());
                    }
                    spotRecord.setInvoiceId(model);

                    invoiceSpotRecordDao.saveInvoiceSpotRecord(spotRecord);

                }
                if (bill.getLogoSpots() > 0) {
                    InvoiceSpotRecord spotRecord = new InvoiceSpotRecord();
                    spotRecord.setAirSpot(bill.getLogoAir());
                    spotRecord.setAllSpots(bill.getLogoSpots());
                    ChannelDetails channel = channelDetailDao.getChannel(bill.getChannelId());
                    spotRecord.setChannelId(channel);
                    spotRecord.setContentType("Logo");
                    if (!isScheduledBill){   //For Jira issue NRM-819
                        spotRecord.setPackageAmount("" + bill.getLogoAirPackageAmount());
                    } else {
                        spotRecord.setPackageAmount("" + bill.getLogoSpotsPackageAmount());
                    }
                    spotRecord.setInvoiceId(model);

                    invoiceSpotRecordDao.saveInvoiceSpotRecord(spotRecord);

                }
                if (bill.getL_sqeezeSpots() > 0) {
                    InvoiceSpotRecord spotRecord = new InvoiceSpotRecord();
                    spotRecord.setAirSpot(bill.getL_sqeeze_Air());
                    spotRecord.setAllSpots(bill.getL_sqeezeSpots());
                    ChannelDetails channel = channelDetailDao.getChannel(bill.getChannelId());
                    spotRecord.setChannelId(channel);
                    spotRecord.setContentType("L_Sqeeze");
                    if (!isScheduledBill){   //For Jira issue NRM-819
                        spotRecord.setPackageAmount("" + bill.getL_sqeezeAirPackageAmount());
                    } else {
                        spotRecord.setPackageAmount("" + bill.getL_sqeezePackageAmount());
                    }
                    spotRecord.setInvoiceId(model);

                    invoiceSpotRecordDao.saveInvoiceSpotRecord(spotRecord);

                }
                if (bill.getV_sqeezeSpots() > 0) {
                    InvoiceSpotRecord spotRecord = new InvoiceSpotRecord();
                    spotRecord.setAirSpot(bill.getV_sqeeze_Air());
                    spotRecord.setAllSpots(bill.getV_sqeezeSpots());
                    ChannelDetails channel = channelDetailDao.getChannel(bill.getChannelId());
                    spotRecord.setChannelId(channel);
                    spotRecord.setContentType("V_Sqeeze");
                    if (!isScheduledBill){   //For Jira issue NRM-819
                        spotRecord.setPackageAmount("" + bill.getV_sqeezeAirPackageAmount());
                    } else {
                        spotRecord.setPackageAmount("" + bill.getV_sqeezePackageAmount());
                    }
                    spotRecord.setInvoiceId(model);

                    invoiceSpotRecordDao.saveInvoiceSpotRecord(spotRecord);
                }
            }
            return true;
        } catch (Exception e) {
            logger.debug("Exception in BillingService in saveInvoiceContentDetails() : {}", e.getMessage());
            return false;
        }
    }

    public Boolean saveInvoiceTaxDetails(InvoiceDef model) {
        try {
            List<TaxDef> taxList = getAllTaxDetails();
            double tatalAmount = Double.parseDouble(model.getTotalamount());
            double ajustAmount = Double.parseDouble(model.getAdjustAmount());
            int commissionRate = model.getCommissionrate();
            double taxaddedAmount = tatalAmount - (((tatalAmount + ajustAmount) * commissionRate) / 100);
            double nbtAmount = 0;
            DecimalFormat df = new DecimalFormat("#0.00");
            df.setRoundingMode(RoundingMode.DOWN);
            double totalAmount = Double.parseDouble(df.format(taxaddedAmount));

            for (TaxDef datamodel : taxList) {
                InvoiceTaxDetails taxDetails = new InvoiceTaxDetails();
                taxDetails.setTaxCategory(datamodel.getTaxcategory());
                taxDetails.setTaxRate(datamodel.getRate());
                double d = 0;
                if (taxDetails.getTaxCategory().equals("NBT")) {
                    nbtAmount = (taxaddedAmount * datamodel.getRate()) / 100;
                }
                if (taxDetails.getTaxCategory().equals("VAT")) {
                    d = ((taxaddedAmount + nbtAmount) * datamodel.getRate()) / 100;
                    totalAmount = totalAmount + Double.parseDouble(df.format(d));
                } else {
                    d = (taxaddedAmount * datamodel.getRate()) / 100;
                    totalAmount = totalAmount + Double.parseDouble(df.format(d));
                }
                taxDetails.setTax("" + d);
                taxDetails.setInvoiceId(model);

                invoiceTaxDetailsDao.saveInvoiceTaxDetails(taxDetails);
            }
            InvoiceTaxDetails taxDetails = new InvoiceTaxDetails();
            taxDetails.setTaxCategory("Total Payable");
            taxDetails.setTaxRate(0);
            taxDetails.setTax("" + totalAmount);
            taxDetails.setInvoiceId(model);
            invoiceTaxDetailsDao.saveInvoiceTaxDetails(taxDetails);

            return true;
        } catch (Exception e) {
            logger.debug("Exception in BillingService in saveInvoiceTaxDetails() : {}", e.getMessage());
            return false;
        }
    }

    public Boolean getSelectedInvoiceAndPrint(HttpServletRequest request, int invoiceID) {
        try {
            InvoiceDef invoice = invoiceDao.getSelectedInvoice(invoiceID);
            int status = invoice.getPrintstatus() + 1;
            invoice.setPrintstatus(status);
            if (new PDFGenerator().billPDFGenerator(request, getPrintInvoicePro(invoice))) {
                if (setAuditForInvoice(invoice)) {
                    return invoiceDao.updateInvoice(invoice);
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } catch (Exception e) {
            logger.debug("Exception in BillingService in getSelectedInvoiceAndPrint() : {}", e.getMessage());
            throw e;
        }
    }

    public List<InvoiceView> getAllInvoice() {
        try {
            List<InvoiceView> invoiceList = new ArrayList();

            List<InvoiceDef> dataList = invoiceDao.getAllInvoice();

            for (InvoiceDef model : dataList) {
                InvoiceView invoice = new InvoiceView();
                String billClient = "";
                String client = "";
                try {
                    billClient = clientDetailDao.getSelectedClient(Integer.parseInt(model.getBillingclient())).getClientname();
                    client = clientDetailDao.getSelectedClient(Integer.parseInt(model.getClient())).getClientname();
                } catch (Exception e) {
                    logger.debug("Exception in BillingService in getAllInvoice() : {}", e.getMessage());
                }

                invoice.setBillingclient(billClient);
                invoice.setClient(client);
                invoice.setCommissionRate(model.getCommissionrate());
                invoice.setCommissionclient(model.getCommissionclient());
                invoice.setInvoiceType(model.getInvoicetype());
                invoice.setInvoiceid(model.getInvoiceid());
                invoice.setTotalAmount(Double.parseDouble(model.getTotalamount()));
                invoice.setPrintStatus(model.getPrintstatus());
                invoice.setWorkOrderId(model.getWorkorderid().getWorkorderid());

                invoiceList.add(invoice);
            }

            return invoiceList;
        } catch (Exception e) {
            logger.debug("Exception in BillingService in getAllInvoice() : {}", e.getMessage());
            throw e;
        }
    }

    @Async("billingServiceExecutor")
    public CompletableFuture<Boolean> addInvoiceAsync(List<InvoiceView> invoiceList, InvoiceDef model) {
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        try{
            completableFuture.complete(addInvoice(invoiceList,model));
        }catch (Exception e){
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

    private Boolean addInvoice(List<InvoiceView> invoiceList, InvoiceDef model) {
        InvoiceView invoice = new InvoiceView();
        String billClient = "";
        String client = "";
        try {
            billClient = clientDetailDao.getSelectedClient(Integer.parseInt(model.getBillingclient())).getClientname();
            client = clientDetailDao.getSelectedClient(Integer.parseInt(model.getClient())).getClientname();
        } catch (Exception e) {
            logger.debug("Exception in BillingService in getAllInvoice() : {}", e.getMessage());
        }
        invoice.setBillingclient(billClient);
        invoice.setClient(client);
        invoice.setCommissionRate(model.getCommissionrate());
        invoice.setCommissionclient(model.getCommissionclient());
        invoice.setInvoiceType(model.getInvoicetype());
        invoice.setInvoiceid(model.getInvoiceid());
        invoice.setTotalAmount(Double.parseDouble(model.getTotalamount()));
        invoice.setPrintStatus(model.getPrintstatus());
        invoice.setWorkOrderId(model.getWorkorderid().getWorkorderid());
        invoiceList.add(invoice);
        return true;
    }

    public PrintInvoicePro getPrintInvoicePro(InvoiceDef model) {
        try {

            PrintInvoicePro invoicePro = new PrintInvoicePro();
            invoicePro.setInvoiceid(model.getInvoiceid());
            invoicePro.setInvoiceType(model.getInvoicetype());
            invoicePro.setWorkOrderId(model.getWorkorderid().getWorkorderid());
            invoicePro.setCommissionRate(model.getCommissionrate());
            double total = Double.parseDouble(model.getTotalamount()) + Double.parseDouble(model.getAdjustAmount());
            invoicePro.setTotalAmount(total);
            invoicePro.setPrintStatus(model.getPrintstatus());
            invoicePro.setWorkOrderPeriod(model.getWorkOrdPeriod());

            invoicePro.setDate(getDateYYYMMDD_Dot(model.getDate()));

            ClientDetails endClient = clientDetailDao.getSelectedClient(model.getClient());
            String endClienAddress = endClient.getClientname() + "," + endClient.getAddress();
            endClienAddress = endClienAddress.replace(",", "\n");

            ClientDetails billingClient = clientDetailDao.getSelectedClient(model.getBillingclient());
            String billingClienAddress = billingClient.getClientname() + "," + billingClient.getAddress();
            billingClienAddress = billingClienAddress.replace(",", "\n");

            invoicePro.setBillingclient(model.getBillingclient());
            invoicePro.setBillingclientAddress(billingClienAddress);

            invoicePro.setClient(model.getClient());
            invoicePro.setClientAddress(endClienAddress);
            invoicePro.setVatRegNum(clientDetailDao.getSelectedClient(model.getBillingclient()).getClientid().toString());

            //invoicePro.setCommissionclient(model.getWorkorderid());
            invoicePro.setSpotRecord(invoiceSpotRecordDao.getSelectedInvoiceSpotRecord(model.getInvoiceid()));
            invoicePro.setTaxDetails(invoiceTaxDetailsDao.getSelectedInvoiceTaxDetails(model.getInvoiceid()));

            String channelName = "";
            for (int i = 0; i < invoicePro.getSpotRecord().size(); i++) {
                if (!channelName.equals(invoicePro.getSpotRecord().get(i).getChannelId().getChannelname())) {
                    channelName = invoicePro.getSpotRecord().get(i).getChannelId().getChannelname();
                    if (i != 0) {
                        invoicePro.setChannelNames(invoicePro.getChannelNames() + "," + channelName);
                    } else {
                        invoicePro.setChannelNames(channelName);
                    }
                }
            }

            return invoicePro;
        } catch (Exception e) {
            logger.debug("Exception in BillingService in getPrintInvoicePro() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadBillPDF(HttpServletRequest request, HttpServletResponse response) {
        try {
            String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
            String fullPath = webServerPath + "/BillPDF/Bill.pdf";

            File downloadFile = new File(fullPath);
            FileInputStream inputStream = new FileInputStream(downloadFile);

            String mimeType = "application/octet-stream";

            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outStream.close();

        } catch (IOException e) {
            logger.debug("Exception in BillingService in downloadBillPDF() : {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in BillingService in downloadBillPDF() : {}", e.getMessage());
            throw e;
        }

    }

    public Boolean setAuditForInvoice(InvoiceDef model) {
        try {
            InvoiceDef dataModel = invoiceDao.getSelectedInvoice(model.getInvoiceid());
            for (Field field : (InvoiceDef.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null && (!field.get(model).equals(field.get(dataModel)))) {
                    GeneralAudit auditModel = new GeneralAudit("invoice_def", model.getInvoiceid());
                    auditModel.setField(field.getAnnotation(Column.class).name());
                    auditModel.setOldData(field.get(dataModel).toString());
                    auditModel.setNewData(field.get(model).toString());

                    generalAuditDao.saveGeneralAudit(auditModel);
                }
            }
        } catch (IllegalAccessException e) {
            logger.debug("Exception in BillingService in setAuditForInvoice() : {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in BillingService in setAuditForInvoice() : {}", e.getMessage());
            throw e;
        }
        return true;
    }

    public List<SAPData> generateSAPFile(InvoiceDef dataModel, HttpServletRequest request) {
        List<SAPData> sapDateList;
        sapDateList = new ArrayList<SAPData>();
        try {
            List<SAPChannelSpotInfo> channelDataList = new ArrayList<>();
            List<InvoiceSpotRecord> spotRecordList = invoiceSpotRecordDao.getSelectedInvoiceSpotRecord(dataModel.getInvoiceid());
            List<InvoiceTaxDetails> taxDetails = invoiceTaxDetailsDao.getSelectedInvoiceTaxDetails(dataModel.getInvoiceid());

            double NBTrate = 0.0;
            for (InvoiceTaxDetails model : taxDetails) {
                if (model.getTaxCategory().equals("NBT")) {
                    NBTrate = model.getTaxRate();
                }
            }

            for (int i = 0; i < spotRecordList.size(); i++) {

                if (channelDataList.isEmpty()) {
                    SAPChannelSpotInfo channelModel = new SAPChannelSpotInfo();
                    channelModel.setChannelName(spotRecordList.get(i).getChannelId().getChannelname());
                    channelModel.addTotalAmount(Double.parseDouble(spotRecordList.get(i).getPackageAmount()));
                    channelModel.addTotalAiredAmount(Double.parseDouble(spotRecordList.get(i).getPackageAmount()), spotRecordList.get(i).getAllSpots(), spotRecordList.get(i).getAirSpot());
                    channelDataList.add(channelModel);
                } else {
                    Boolean updated = true;
                    for (int j = 0; j < channelDataList.size(); j++) {
                        if (spotRecordList.get(i).getChannelId().getChannelname().equals(channelDataList.get(j).getChannelName())) {
                            channelDataList.get(j).addTotalAmount(Double.parseDouble(spotRecordList.get(i).getPackageAmount()));
                            channelDataList.get(j).addTotalAiredAmount(Double.parseDouble(spotRecordList.get(i).getPackageAmount()), spotRecordList.get(i).getAllSpots(), spotRecordList.get(i).getAirSpot());
                            updated = false;
                        }
                    }

                    if (updated) {
                        SAPChannelSpotInfo channelModel = new SAPChannelSpotInfo();
                        channelModel.setChannelName(spotRecordList.get(i).getChannelId().getChannelname());
                        channelModel.addTotalAmount(Double.parseDouble(spotRecordList.get(i).getPackageAmount()));
                        channelModel.addTotalAiredAmount(Double.parseDouble(spotRecordList.get(i).getPackageAmount()), spotRecordList.get(i).getAllSpots(), spotRecordList.get(i).getAirSpot());
                        channelDataList.add(channelModel);
                    }
                }
            }

            String idLength = "000000000000413";
            for (int i = 0; i < channelDataList.size(); i++) {

                SAPData sapModel = new SAPData();

                String id = dataModel.getWorkorderid().getWorkorderid().toString();
                for (int j = id.length(); j < idLength.length(); j++) {
                    id = "0" + id;
                }
                id = "DTV-VCL-" + id;
                sapModel.setSalesOrderID(id);
                //ClientDetails endClient = clientDetailDao.getSelectedClient(dataModel.getClient());
                //String clientName = workOrder.getAgencyclient() ? workOrder.getClient()
                ClientDetails billingClient = clientDetailDao.getSelectedClient(dataModel.getBillingclient());
                ClientDetails agencyClient = clientDetailDao.getSelectedClient(dataModel.getWorkorderid().getAgencyclient());
                int invoiceType = getInvoiceType(agencyClient.getClientid(), dataModel.getCommissionrate(), billingClient.getClientid());
                if (agencyClient.getClientid() == 0) {
                    agencyClient.setAddress("");
                    agencyClient.setClientname("");
                }
                sapModel.setInvoiceType("" + invoiceType);
                sapModel.setContactPerson_attn("");
                sapModel.setEndUser(billingClient.getCx_cade());
                sapModel.setMaterialDescription("Commercial Advertising_" + channelDataList.get(i).getChannelName());
                DecimalFormat df = new DecimalFormat("#.00");
                if (dataModel.getInvoicetype() == 1 || dataModel.getInvoicetype() == 3 || dataModel.getInvoicetype() == 4) {
                    sapModel.setConditionType_ZCTR(df.format(channelDataList.get(i).getChannelTotalAmount()));
                }
                if (dataModel.getInvoicetype() == 2 || dataModel.getInvoicetype() == 5) {
                    sapModel.setConditionType_ZCTR(df.format(channelDataList.get(i).getChannelAiredTotalAmount()));
                }

                sapModel.setConditionType_Z001("" + dataModel.getCommissionrate());
                sapModel.setConditionType_ZNBT("" + NBTrate);
                sapModel.setBillingDateFrom(getDateDDMMYYY_slash(dataModel.getWorkorderid().getStartdate()));
                sapModel.setBillingDateTo(getDateDDMMYYY_slash(dataModel.getWorkorderid().getEnddate()));
                sapModel.setAgencyName(agencyClient.getClientname());
                sapModel.setAgencyAddress(agencyClient.getAddress());
                sapModel.setChannel(channelDataList.get(i).getChannelName());
                sapModel.setProductORBrand(dataModel.getWorkorderid().getOrdername() + "-Ref:" + dataModel.getWorkorderid().getScheduleRef());
                ClientDetails sapClient = clientDetailDao.getSelectedClient(dataModel.getWorkorderid().getClient());
                sapModel.setClientName(sapClient.getClientname());
                sapModel.setMediaAgentName(userDetailsDao.getUserDetails(dataModel.getWorkorderid().getSeller()).getName());
                sapDateList.add(sapModel);
            }

            SAPFileWriter(sapDateList, dataModel, request);
            return sapDateList;
        } catch (Exception e) {
            logger.debug("Exception in BillingService in generateSAPFile() : {}", e.getMessage());
            return sapDateList;
        }
    }

    public int getInvoiceType(int agencyID, int commistion, int billingclient) {

        if (agencyID == 0 && commistion == 0) {
            return 1;
        } else if (agencyID == billingclient && commistion == 15) {
            return 2;
        } else if (agencyID != billingclient && billingclient != 0 && commistion == 15) {
            return 3;
        } else if (agencyID != 0 && commistion == 0) {
            return 4;
        } else {
            return 0;
        }
    }

    public void SAPFileWriter(List<SAPData> dataList, InvoiceDef dataModel, HttpServletRequest request) throws Exception {
        BufferedWriter writer = null;
        try {
            //file path
            String fileName = getSAPFileName();
            String copyPath = SAP_FILE_COPY_FILE_PATH + fileName + ".txt";
            String filePath = getSapPath(request) + File.separator + fileName + ".txt";
            File logFile = new File(filePath);
            writer = new BufferedWriter(new FileWriter(logFile));
            for (int i = 0; i < dataList.size(); i++) {
                writer.write(dataList.get(i).getSAPdata());
                if (i != (dataList.size() - 1)) {
                    writer.newLine();
                }
            }
            // Close the writer regardless of what happens...
            writer.close();
            Path FROM = Paths.get(filePath);
            Path TO = Paths.get(copyPath);
            Files.copy(FROM, TO);

            dataModel.setSapFilePath(filePath);
            invoiceDao.updateInvoice(dataModel);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Exception in BillingService in SAPFileWriter() : {}", e.getMessage());
            throw e;
        }

    }

    public String getSapPath(HttpServletRequest request) {

        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + File.separator + "ROOT";
        String webDirectory = File.separator + "SAP";
        File web_directory = new File(webServerPath + webDirectory);
        if (!web_directory.isDirectory()) {
            web_directory.mkdirs();
        }

        String sapPath = SAP_FILE_PATH;
        return sapPath;
    }

    public String getSAPFileName() {
        String name = "DTV_ADVERTISING_INCOME_VCL_";
        String date = getDateYYYMMDD(new Date());
        SapData sapdata = invoiceDao.getSapData();
        if (sapdata.getFileNameDate().equals(date)) {
            sapdata.setCount(sapdata.getCount() + 1);
            String count = "" + sapdata.getCount();
            for (int j = count.length(); j < 3; j++) {
                count = "0" + count;
            }
            name += sapdata.getFileNameDate() + "_" + count;
            invoiceDao.updateSapData(sapdata);
        } else {
            sapdata.setFileNameDate(date);
            sapdata.setCount(1);
            String count = "" + sapdata.getCount();
            for (int j = count.length(); j < 3; j++) {
                count = "0" + count;
            }
            name += sapdata.getFileNameDate() + "_" + count;
            invoiceDao.updateSapData(sapdata);
        }

        return name;
    }

    public void downloadSAPFile(HttpServletResponse response, int invoiceID) {
        try {
            String fullPath = invoiceDao.getSelectedInvoice(invoiceID).getSapFilePath();
            File downloadFile = new File(fullPath);
            String absolutePath = SAP_FILE_PATH + downloadFile.getName();

            downloadFile = new File(absolutePath);
            FileInputStream inputStream = new FileInputStream(downloadFile);

            String mimeType = "application/octet-stream";

            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outStream.close();

        } catch (IOException e) {
            logger.debug("Exception in BillingService in downloadSAPFile() : {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in BillingService in downloadSAPFile() : {}", e.getMessage());
            throw e;
        }

    }

    public InvoiceData getInvoiceData(int invoiceId) { //tested
        InvoiceData invoiceData = invoiceDataDao.getInvoiceData(invoiceId);
        return invoiceData == null ? new InvoiceData() : invoiceData;
    }

    public InvoiceData saveInvoiceDetails(String invoiceData) throws IOException {
        try {

            ObjectMapper m = new ObjectMapper();
            InvoiceDataInfo dataModel = m.readValue(invoiceData, InvoiceDataInfo.class);
            InvoiceData invoiceModel = null;
            if (dataModel.getInvoiceDataID() == 0) {
                invoiceModel = new InvoiceData();
            } else {
                invoiceModel = invoiceDataDao.getSelectedInvoiceData(dataModel.getInvoiceDataID());
            }
            invoiceModel.setInvoiceDate(dataModel.getInvoiceDate());

            InvoiceDef invoiceID = new InvoiceDef();
            invoiceID.setInvoiceid(dataModel.getInvoiceID());

            invoiceModel.setInvoiceId(invoiceID);
            invoiceModel.setInvoiceNo(dataModel.getInvoiceNo());
            invoiceModel.setHandOverDate_ME(dataModel.getHandOverDate_ME());
            invoiceModel.setHandOverDate_Agency(dataModel.getHandOverDate_Agency());
            invoiceModel.setPayment_Date(dataModel.getPayment_Date());
            invoiceModel.setPaidRef(dataModel.getPaidRef());
            invoiceModel.setPaidAmount(dataModel.getPaidAmount());
            invoiceModel.setDifference(dataModel.getDifference());
            invoiceModel.setClearingDocNo(dataModel.getClearingDocNo());
            return invoiceDataDao.updateInvoiceData(invoiceModel);
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        }

    }

    public void updateSapFilePath() {
        List<InvoiceDef> allInvoice = invoiceDao.getAllInvoice();
        for (InvoiceDef invoiceDef : allInvoice) {
            String previusFilePath = invoiceDef.getSapFilePath();
            String[] filePathArry = previusFilePath.split("/");
            String fileName = filePathArry[filePathArry.length - 1];
            String newPath = SAP_FILE_PATH + fileName;
            invoiceDef.setSapFilePath(newPath);
            invoiceDao.saveInvoice(invoiceDef);
        }
    }
}
