package com.vclabs.nash.service.product;

import com.vclabs.nash.model.dao.product.CodeMappingDAO;
import com.vclabs.nash.model.entity.product.Brand;
import com.vclabs.nash.model.entity.product.CodeMapping;
import com.vclabs.nash.model.entity.product.Product;
import com.vclabs.nash.model.view.product.ProductView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dperera on 30/08/2019.
 */
@Service
@Transactional("main")
public class CodeMappingService {

    @Autowired
    private CodeMappingDAO codeMappingDAO;

    @Autowired
    private ProductService productService;

    @Autowired
    private BrandService brandService;

    @Transactional
    public CodeMapping save(CodeMapping cm) {
        Product product = cm.getProduct();
        Brand brand = cm.getBrand();
        if (product.getName() != null && brand != null && !StringUtils.isEmpty(cm.getCode())) {
            Product existingProduct = productService.findByName(product.getName());
            if (existingProduct == null) {
                productService.save(product);
                cm.setProduct(productService.findByName(product.getName()));
            } else {
                cm.setProduct(existingProduct);
            }
            if (brand.getId() == null) {
                brandService.save(brand);
            }
            return codeMappingDAO.save(cm);
        }
        return null;
    }

    @Transactional
    public Page<ProductView> findAllCodeMapping(Map<String, String> filterRequest) {

        String sortString = filterRequest.get("sort");
        int page = Integer.parseInt(filterRequest.get("page"));
        int size = Integer.parseInt(filterRequest.get("size"));

        String productId = filterRequest.get("productId");
        String productName = filterRequest.get("productName");
        String clientId = filterRequest.get("client_id");

        Pageable pageable;

        if (sortString != null) {
            String[] sortParams = sortString.split(",");
            Sort sort = new Sort(Sort.Direction.fromString(sortParams[1]), sortParams[0]);
            pageable = new PageRequest(page, size, sort);
        } else {
            pageable = new PageRequest(page, size);
        }

        List<CodeMapping> codeMappingList = codeMappingDAO.findAllCodeMapping(pageable.getPageSize(), pageable.getOffset(), clientId, productName, productId);
        int productCount = codeMappingDAO.findAllCodeMappingCount(clientId, productName, productId);
        List<ProductView> productViewList = new ArrayList<>();
        for (CodeMapping codeMapping : codeMappingList) {
            ProductView productView = new ProductView();
            productView.setMapId(codeMapping.getId().intValue());
            productView.setClientId(codeMapping.getClient().getClientid());
            productView.setClientName(codeMapping.getClient().getClientname());
            productView.setBrandName(codeMapping.getBrand().getName());
            productView.setCode(codeMapping.getCode());
            productView.setProductName(codeMapping.getProduct().getName());
            productViewList.add(productView);
        }

        return new PageImpl<>(productViewList, pageable, productCount);
    }

    public List<CodeMapping> findByClientId(long clientId) {
        return codeMappingDAO.findByClientId(clientId);
    }

    public CodeMapping findById(long id) {
        return codeMappingDAO.findById(id);
    }

    public Boolean vailateCocde(String code) {
        return codeMappingDAO.findByCode(code).size() == 0 ? true : false;
    }

}
