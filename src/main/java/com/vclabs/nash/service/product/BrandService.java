package com.vclabs.nash.service.product;

import com.vclabs.nash.model.dao.product.BrandDAO;
import com.vclabs.nash.model.entity.product.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dperera on 29/08/2019.
 */
@Service
@Transactional("main")
public class BrandService {

    @Autowired
    private BrandDAO brandDAO;

    @Transactional
    public List<Brand> findAll(){
        return brandDAO.findAll();
    }

    @Transactional
    public void save(Brand brand){
        brandDAO.save(brand);
    }

    public Brand findByName(String name){
        return brandDAO.findByName(name);
    }

}
