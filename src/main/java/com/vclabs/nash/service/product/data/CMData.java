package com.vclabs.nash.service.product.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dperera on 29/11/2019.
 */
public class CMData {

    @JsonProperty("CLIENT_ID")
    private String clientId;

    @JsonProperty("COMPANY")
    private String company;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("BRAND")
    private String brand;

    @JsonProperty("CODE")
    private String code;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
