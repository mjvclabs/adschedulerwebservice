package com.vclabs.nash.service.product;

import com.vclabs.nash.model.dao.product.ProductDAO;
import com.vclabs.nash.model.entity.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dperera on 29/08/2019.
 */
@Service
@Transactional("main")
public class ProductService {

    @Autowired
    private ProductDAO productDAO;

    @Transactional
    public void save(Product product){
        productDAO.save(product);
    }

    public Product findByName(String name){
        return productDAO.findByName(name);
    }

    public List<Product> findAll(){
        return productDAO.findAll();
    }
}
