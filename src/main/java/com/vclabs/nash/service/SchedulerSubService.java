package com.vclabs.nash.service;

import com.vclabs.nash.model.view.WorkOrderScheduleView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class SchedulerSubService {

    @Autowired
    private SchedulerService schedulerService;

    public WorkOrderScheduleView getWOScheduleViewData(int woID, int channelId, Boolean withUtilizeData) {
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();
        List<CompletableFuture<Boolean>> allFutures = new ArrayList<>();
        allFutures.add(schedulerService.getWOScheduleViewDataF1(retObject, woID, channelId));
        if(withUtilizeData) {
            allFutures.add(schedulerService.getWOScheduleViewDataF2(retObject, woID));
        }
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
        return retObject;
    }
}
