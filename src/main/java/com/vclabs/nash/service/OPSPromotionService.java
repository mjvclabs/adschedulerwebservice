package com.vclabs.nash.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.OPSResponseData;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.*;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Nalaka on 2018-09-25.
 */
@Service
@Transactional("main")
public class OPSPromotionService {

    @Autowired
    private OPSResponseDataService oPSResponseDataService;
    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private WorkOrderService workOrderService;

    @Lazy
    @Autowired
    private SchedulerService schedulerService;

    public int savePromotion() throws ParseException {

        String plainCredentials = "nashadmin:Welcome1@";
        String base64Credentials = new String(Base64.encode(plainCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> request = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("http://localhost:8080/nash/promotion/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);
        //ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("https://192.168.1.99:8443/adhub/nash/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);
        //ResponseEntity<OPSMainSchedule[]> response = restTemplate.exchange("https://123.231.117.122:8443/adhub/nash/export-workorders", HttpMethod.GET, request, OPSMainSchedule[].class);
        OPSResponseData opsrd = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            opsrd = oPSResponseDataService.savePromotionResponse(mapper.writeValueAsString(response.getBody()));
        } catch (JsonProcessingException e) {
            System.out.println("" + e);
        }

        List<OPSMainSchedule> searchList = Arrays.asList(response.getBody());

        for (OPSMainSchedule entry : searchList) {
            WorkOrder woModel = saveOPSWorkOrder(entry.getWorkOrder());
            saveOrderChannelList(entry.getChannelList(), woModel);
            saveAdvertisement(entry.getAdvertList(), woModel.getWorkorderid());
            setAdvertToSpot(entry.getAdvertList(), entry.getScheduleList());
            savePromotionSchedule(entry.getScheduleList(), woModel.getWorkorderid());
        }
        if (opsrd != null) {
            opsrd.setStatus(OPSResponseData.Status.SUCCESS);
            oPSResponseDataService.updateResponseData(opsrd);
        }
        return searchList.size();
    }

    public WorkOrder saveOPSWorkOrder(WorkOrderInfo model) throws ParseException {
        SimpleDateFormat dta = new SimpleDateFormat("yyyy-MM-dd");// 00:00:00:000
        Date dt = new Date();
        Date startDate = dta.parse(model.getStartDate());
        Date endDate = dta.parse(model.getEndDate());

        WorkOrder workorder = new WorkOrder();

        workorder.setOrdername(model.getOrderName());
        workorder.setClient(model.getClient());
        workorder.setAgencyclient(model.getAgency());
        workorder.setBillclient(model.getBillClient());
        workorder.setBspotvisibility(model.getVisibility());
        workorder.setComment(model.getComment());

        workorder.setDate(dt);
        workorder.setStartdate(startDate);
        workorder.setEnddate(endDate);

        workorder.setPermissionstatus(0);

        workorder.setSeller(model.getUserName());
        workorder.setWoType(model.getWoType());
        workorder.setScheduleRef(model.getScheduleRef());
        workorder.setCommission(model.getCommission());
        workorder.setTotalBudget(model.getTotalBudget());

        workOrderDao.saveWorkOrder(workorder);

        return workorder;
    }

    public void saveOrderChannelList(List<OrderChannelInfo> list, WorkOrder model) {

        for (OrderChannelInfo orderChannelModel : list) {
            orderChannelModel.setWorkOrderId(model.getWorkorderid());
            workOrderService.setWorkOrderChannelList(orderChannelModel);
        }
    }

    public void saveAdvertisement(List<AdvertisementInfo> dataModelList, int workOrderId) {
        try {
            List<OrderAdvertisementInfo> orderAdverts = new ArrayList<>();
            for (AdvertisementInfo dataModel : dataModelList) {

                OrderAdvertisementInfo woAdvertModel = new OrderAdvertisementInfo();
                woAdvertModel.setAdvertId(dataModel.getAdvertid());
                woAdvertModel.setWorkOrderId(workOrderId);
                orderAdverts.add(woAdvertModel);

            }
            workOrderService.saveOrderAdvertList(workOrderId, orderAdverts);

        } catch (Exception e) {
            System.err.println("[error] => " + e.getClass() + " : " + e.getStackTrace()[0].getMethodName() + " - " + e.getMessage());
        }
    }

    public void setAdvertToSpot(List<AdvertisementInfo> advertList, List<ScheduleInfo> scheduleList) {

        //Sort according to duration
//        Collections.sort(scheduleList, new Comparator<ScheduleInfo>() {
//
//            @Override
//            public int compare(ScheduleInfo o1, ScheduleInfo o2) {
//                return o1.getAdvertDto().getDuration() - o2.getAdvertDto().getDuration();
//            }
//        });
//        HashMap<Integer, List<AdvertisementInfo>> tvcAdvertMap = new HashMap<Integer, List<AdvertisementInfo>>();
//        List<AdvertisementInfo> logAdvertList = new ArrayList<>();
//        List<AdvertisementInfo> crawlerAdvertList = new ArrayList<>();
//
//        for (AdvertisementInfo advertisementInfo : advertList) {
//            String adverType = advertisementInfo.getAdverttype();
//            switch (adverType) {
//                case "TVC":
//                    List<AdvertisementInfo> tempArray = tvcAdvertMap.get(advertisementInfo.getDuration());
//                    if (tempArray != null) {
//                        tempArray.add(advertisementInfo);
//                    } else {
//                        tempArray = new ArrayList<>();
//                        tempArray.add(advertisementInfo);
//                        tvcAdvertMap.put(advertisementInfo.getDuration(), tempArray);
//                    }
//                    break;
//                case "LOGO":
//                    logAdvertList.add(advertisementInfo);
//                    break;
//                default:
//                    crawlerAdvertList.add(advertisementInfo);
//                    break;
//            }
//        }
        int advertArryListSize = 0;
//        int selectedTvcListSize = 0;
//        int tvcDuration = 0;
//        int logoListSize = 0;
//        int crawlerListSize = 0;

        for (ScheduleInfo scheduleInfo : scheduleList) {

            scheduleInfo.setAdvertId(advertList.get(advertArryListSize).getAdvertid());
            advertArryListSize++;
            if (advertArryListSize == advertList.size()) {
                advertArryListSize = 0;
            }
//            switch (scheduleInfo.getAdvertDto().getAdverttype()) {
//                case "TVC":
//                    if (!tvcAdvertMap.isEmpty()) {
//                        List<AdvertisementInfo> selectedTvcAdvertList = tvcAdvertMap.get(scheduleInfo.getAdvertDto().getDuration());
//                        if (!selectedTvcAdvertList.isEmpty()) {
//                            if (tvcDuration == scheduleInfo.getAdvertDto().getDuration()) {
//                                scheduleInfo.setAdvertId(selectedTvcAdvertList.get(selectedTvcListSize).getAdvertid());
//                                selectedTvcListSize++;
//                                if (selectedTvcListSize == selectedTvcAdvertList.size()) {
//                                    selectedTvcListSize = 0;
//                                }
//                            } else {
//                                selectedTvcListSize = 0;
//                                tvcDuration = scheduleInfo.getAdvertDto().getDuration();
//                                scheduleInfo.setAdvertId(selectedTvcAdvertList.get(selectedTvcListSize).getAdvertid());
//                                selectedTvcListSize++;
//                                if (selectedTvcListSize == selectedTvcAdvertList.size()) {
//                                    selectedTvcListSize = 0;
//                                }
//                            }
//                        }
//                    }
//                    break;
//                case "LOGO":
//                    if (!logAdvertList.isEmpty()) {
//                        scheduleInfo.setAdvertId(logAdvertList.get(logoListSize).getAdvertid());
//                        logoListSize++;
//                        if (logoListSize == logAdvertList.size()) {
//                            logoListSize = 0;
//                        }
//                    }
//                    break;
//                case " CRAWLER":
//                    if (!crawlerAdvertList.isEmpty()) {
//                        scheduleInfo.setAdvertId(crawlerAdvertList.get(crawlerListSize).getAdvertid());
//                        crawlerListSize++;
//                        if (crawlerListSize == crawlerAdvertList.size()) {
//                            crawlerListSize = 0;
//                        }
//                    }
//                    break;
//                default:
//                    break;
//            }
        }
    }

    public void savePromotionSchedule(List<ScheduleInfo> scheduleList, int workOrderId) {
        //sort according to channelId
        Collections.sort(scheduleList, new Comparator<ScheduleInfo>() {

            @Override
            public int compare(ScheduleInfo o1, ScheduleInfo o2) {
                return o1.getChannelId() - o2.getChannelId(); //To change body of generated methods, choose Tools | Templates.
            }
        });

        //sort according to channelId and advertId
        //Collections.sort(scheduleList);
        Collections.sort(scheduleList, new Comparator<ScheduleInfo>() {

            @Override
            public int compare(ScheduleInfo o1, ScheduleInfo o2) {
                return o1.getAdvertId() - o2.getAdvertId(); //To change body of generated methods, choose Tools | Templates.
            }
        });

        List<ScheduleInfo> tempArry = new ArrayList<>();
        int channelId = scheduleList.get(0).getChannelId();
        int advertId = scheduleList.get(0).getAdvertId();

        for (ScheduleInfo schedulInfo : scheduleList) {
            schedulInfo.setWorkOrderId(workOrderId);
            if (channelId == schedulInfo.getChannelId() && advertId == schedulInfo.getAdvertId()) {
                tempArry.add(schedulInfo);
            } else {
                channelId = schedulInfo.getChannelId();
                advertId = schedulInfo.getAdvertId();
                if (saveSchedule(tempArry)) {
                    tempArry.clear();
                    tempArry.add(schedulInfo);
                }
            }
        }
        saveSchedule(tempArry);
    }

    public Boolean saveSchedule(List<ScheduleInfo> scheduleList) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String stringValue = mapper.writeValueAsString(scheduleList);
            schedulerService.saveWOSchedule(stringValue);
            return true;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return false;
        }
    }
}
