/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.itextpdf.text.Chunk;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.math.RoundingMode;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.process.PrintInvoicePro;
import com.vclabs.nash.model.process.TRDetails;

/**
 *
 * @author Administrator
 */
@Repository
public class PDFGenerator {

    private static final Logger logger = LoggerFactory.getLogger(PDFGenerator.class);

    public boolean transmissionReportPDFGenerator(List<ScheduleDef> scheduleDefList, HttpServletRequest request) {
        // String webServerPath = new File(context.getServletContext().getRealPath("/")).getParent() + "\\ROOT";
        Document document = new Document(PageSize.A4, 25, 25, 70, 70);
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(getTransmissionPath(request)));
            document.open();
            try {
                //Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
                //document.add(img);
                document.add(getHeding("Transmission Report"));
                document.add(getHeding("                      "));
                PdfPTable table = new PdfPTable(25);
                //table.setWidthPercentage(288 / 5.23f);
                //table.setWidths(new int[]{2, 1, 1});
                //table.setTotalWidth(325);
                table.setWidthPercentage(100);

                PdfPCell cell;
                // we add a cell with colspan 3
                cell = getCellColspan2();
                cell.addElement(this.getTableHeading("Num#"));
                table.addCell(cell);

                cell = getCellColspan2();
                cell.addElement(this.getTableHeading("Client"));
                table.addCell(cell);

                cell = getCellColspan3();
                cell.addElement(this.getTableHeading("Channel"));
                table.addCell(cell);

                cell = getCellColspan2();
                cell.addElement(this.getTableHeading("work Order"));
                table.addCell(cell);

                cell = getCellColspan4();
                cell.addElement(this.getTableHeading("Advertiesment Name"));
                table.addCell(cell);

                cell = getCellColspan3();
                cell.addElement(this.getTableHeading("Date"));
                table.addCell(cell);

                cell = getCellColspan4();
                cell.addElement(this.getTableHeading("Shedule Time"));
                table.addCell(cell);

                cell = getCellColspan3();
                cell.addElement(this.getTableHeading("Actual Time"));
                table.addCell(cell);

                cell = getCellColspan2();
                cell.addElement(this.getTableHeading("Status"));
                table.addCell(cell);

                for (int i = 0; i < scheduleDefList.size(); i++) {
                    int num = i + 1;
                    cell = getCellColspan2();
                    cell.addElement(this.getTableData("" + num));
                    table.addCell(cell);

                    cell = getCellColspan2();
                    cell.addElement(this.getTableData(scheduleDefList.get(i).getAdvertid().getClient().getClientname()));
                    table.addCell(cell);

                    cell = getCellColspan3();
                    cell.addElement(this.getTableData(scheduleDefList.get(i).getChannelid().getChannelname()));
                    table.addCell(cell);

                    cell = getCellColspan2();
                    cell.addElement(this.getTableData(Integer.toString(scheduleDefList.get(i).getWorkorderid().getWorkorderid())));
                    table.addCell(cell);

                    cell = getCellColspan4();
                    cell.addElement(this.getTableData(scheduleDefList.get(i).getAdvertid().getAdvertname()));
                    table.addCell(cell);

                    cell = getCellColspan3();
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    String stringDate = format.format(scheduleDefList.get(i).getDate());
                    cell.addElement(this.getTableData(stringDate));
                    table.addCell(cell);

                    cell = getCellColspan4();
                    format = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                    String stringStartTime = format.format(scheduleDefList.get(i).getSchedulestarttime());
                    String stringEndTime = format.format(scheduleDefList.get(i).getScheduleendtime());
                    cell.addElement(this.getTableData(stringStartTime + " - " + stringEndTime));
                    //cell.addElement(this.getTableData(scheduleDefList.get(i).getSchedulestarttime()));
                    table.addCell(cell);

                    cell = getCellColspan3();
                    format = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                    String stringActualTime = "";

                    try {
                        stringActualTime = format.format(scheduleDefList.get(i).getActualstarttime());
                    } catch (Exception ex) {
                        stringActualTime = "";
                        logger.debug("Exception in PDFGenerator in transmissionReportPDFGenerator() : {}", ex.getMessage());
                    }

                    String status = "";
                    if (scheduleDefList.get(i).getStatusbyName().equals("Played")) {
                        status = "Aired";
                    } else {
                        stringActualTime = "";
                    }

                    //String stringEndTime = format.format(scheduleDefList.get(i).getScheduleendtime());
                    cell.addElement(this.getTableData(stringActualTime));
                    //cell.addElement(this.getTableData("Actual Time"));
                    table.addCell(cell);

                    cell = getCellColspan2();
//                            int intgerStatus = Integer.parseInt(scheduleDefList.get(i).getStatus());
//                            String stringStatus="";
//                            if(intgerStatus == 1)
//                            {
//				stringStatus ="Ready";		
//                            }
//                            else if(intgerStatus == 2)
//                            {
//				stringStatus ="full";
//                            }
//                            else if(intgerStatus == 3)
//                            {
//				stringStatus ="Not played";
//                            }
//                            else if(intgerStatus == 4)
//                            {
//				stringStatus ="half";
//                            }

                    cell.addElement(this.getTableData(status));
                    table.addCell(cell);
                }
                //table.completeRow();
                // now we add a cell with rowspan 2
                cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
                cell.setRowspan(2);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
                cell.setRowspan(2);
                table.addCell(cell);
                // we add the four remaining cells with addCell()
//                        table.addCell("rowggg");
//                        table.addCell("rogggggggggggggg");
//                        table.addCell("ro66666666666");
//                        table.addCell("3wwwwwwwwwwwwww");
//                        table.addCell("jjjjjjjjjjjj");
//                        table.addCell("roggggggggggggggg");
//                        table.addCell("rohhhhhhhhhhhhh");
//                        table.addCell("rbbbbbbbbbbbbbbbb");
//                        table.addCell("rbbbbbbbbbbbbbbbb");
                document.add(table);
                document.close();
            } catch (Exception ex) {
                logger.debug("Exception in PDFGenerator in transmissionReportPDFGenerator() : {}", ex.getMessage());
                return false;
            }

        } catch (Exception ex) {
            logger.debug("Exception in PDFGenerator in transmissionReportPDFGenerator() : {}", ex.getMessage());
        }

        return true;
    }

    public Boolean transmissionReportPDFGenerator_V2(List<ScheduleDef> scheduleDefList, TRDetails model, HttpServletRequest request) throws IOException, DocumentException {

        try {
            if (!scheduleDefList.isEmpty()) {
                model.setChannel(scheduleDefList.get(0).getChannelid().getChannelname());
            }
            Document document = new Document(PageSize.A4, 25, 25, 25, 10);

            PdfWriter.getInstance(document, new FileOutputStream(getTransmissionPath(request)));
            document.open();
            document.add(new Chunk(""));

            createHeader(document);

            PdfPTable tblForm = getTRDetails(document, model);

            document.add(tblForm);

            PdfPTable tblData = this.createDataTable();
            tblData.addCell(this.createTableHeaderCell("Date", 2));
            tblData.addCell(this.createTableHeaderCell("Aired Time", 2));
            tblData.addCell(this.createTableHeaderCell("Product Name", 6));
            tblData.addCell(this.createTableHeaderCell("Dur.", 1));

            int count = 0;
            int spotCount = 0;
            Boolean addede = true;
            int arraySize = scheduleDefList.size();
            for (ScheduleDef item : scheduleDefList) {
                addede = true;

                if (!model.getChannel().equals(item.getChannelid().getChannelname())) {
                    model.setChannel(item.getChannelid().getChannelname());
                    arraySize = arraySize - count;
                    document.add(tblData);

                    Paragraph text_2 = new Paragraph();
                    text_2.setAlignment(Element.ALIGN_CENTER);
                    text_2.setFont(new Font(Font.FontFamily.COURIER, 9));
                    text_2.add("Total No of Spots:" + spotCount);
                    document.add(text_2);

                    tblData.deleteBodyRows();
                    if (arraySize > 0) {
                        document.newPage();
                        createHeader(document);
                        document.add(getTRDetails(document, model));
                        tblData.addCell(this.createTableHeaderCell("Date", 2));
                        tblData.addCell(this.createTableHeaderCell("Aired Time", 2));
                        tblData.addCell(this.createTableHeaderCell("Product Name", 6));
                        tblData.addCell(this.createTableHeaderCell("Dur.", 1));
                    }
                    count = 0;
                    spotCount = 0;
                }
                count++;
                spotCount++;
                DateFormat dtFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                tblData.addCell(this.createTableCell(dtFormat.format(item.getDate()), 2));

                dtFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                if (item.getStatus().equals("2")) {
                    tblData.addCell(this.createTableCell(dtFormat.format(item.getActualstarttime()), 2));
                } else {
                    tblData.addCell(this.createTableCell("", 2));
                }
                tblData.addCell(this.createTableCell(item.getAdvertid().getAdvertname(), 6));
                tblData.addCell(this.createTableCell("" + item.getAdvertid().getDuration(), 1));
                if (count == 35) {

                    arraySize = arraySize - count;
                    addede = false;
                    document.add(tblData);

                    Paragraph text_2 = new Paragraph();
                    text_2.setAlignment(Element.ALIGN_CENTER);
                    text_2.setFont(new Font(Font.FontFamily.COURIER, 9));
                    //text_2.add("Total No of Spots:" + count);
                    document.add(text_2);

                    tblData.deleteBodyRows();
                    if (arraySize > 0) {
                        document.newPage();
                        createHeader(document);
                        document.add(getTRDetails(document, model));
                        tblData.addCell(this.createTableHeaderCell("Date", 2));
                        tblData.addCell(this.createTableHeaderCell("Aired Time", 2));
                        tblData.addCell(this.createTableHeaderCell("Product Name", 6));
                        tblData.addCell(this.createTableHeaderCell("Dur.", 1));
                    }
                    count = 0;
                }
            }
            if (addede) {

                document.add(tblData);
                Paragraph text_2 = new Paragraph();
                text_2.setAlignment(Element.ALIGN_CENTER);
                text_2.setFont(new Font(Font.FontFamily.COURIER, 9));
                text_2.add("Total No of Spots:" + spotCount);
                document.add(text_2);
            }
            document.close();
            return true;
        } catch (Exception e) {
            logger.debug("Exception in PDFGenerator in transmissionReportPDFGenerator_V2() : {}", e.getMessage());
            return false;
        }
    }

    public PdfPTable getTRDetails(Document document, TRDetails model) throws DocumentException {
        PdfPTable tblForm = createDetailsForm(document);
        tblForm.addCell(this.createFormCell("Reg. No:"));
        tblForm.addCell(this.createFormCell(model.getRegNO()));
        tblForm.addCell(this.createFormCell("Product:"));
        tblForm.addCell(this.createFormCell(model.getProduct()));
        tblForm.addCell(this.createFormCell("Schedule Ref:"));
        tblForm.addCell(this.createFormCell(model.getScheduleRef()));
        tblForm.addCell(this.createFormCell("Advertiser:"));
        tblForm.addCell(this.createFormCell(model.getAdvertiser()));
        tblForm.addCell(this.createFormCell("Channel:"));
        tblForm.addCell(this.createFormCell(model.getChannel()));
        tblForm.addCell(this.createFormCell("Agency/Agent:"));
        tblForm.addCell(this.createFormCell(model.getAgency()));
        tblForm.addCell(this.createFormCell("Marketing Ex.:"));
        tblForm.addCell(this.createFormCell(model.getMarketingEx()));

        return tblForm;
    }

    private void createHeader(Document doc) throws IOException, DocumentException {
        Paragraph text_1 = new Paragraph();
        text_1.setAlignment(Element.ALIGN_CENTER);
        text_1.setFont(new Font(Font.FontFamily.COURIER, 13));
        text_1.add("Dialog Television (Pvt) Ltd.");

        Paragraph text_2 = new Paragraph();
        text_2.setAlignment(Element.ALIGN_CENTER);
        text_2.setFont(new Font(Font.FontFamily.COURIER, 9));
        text_2.add("475 Union Place, Colombo 2");

        Paragraph text_3 = new Paragraph();
        text_3.setAlignment(Element.ALIGN_CENTER);
        text_3.setFont(new Font(Font.FontFamily.COURIER, 12));
        text_3.add("Transmission Certificate");

        doc.add(text_1);
        doc.add(text_2);
        text_3.setSpacingBefore(15.0f);
        text_3.setSpacingAfter(15.0f);
        doc.add(text_3);
    }

    private PdfPTable createDetailsForm(Document doc) throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.setWidths(new float[]{1, 4});
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        return table;
    }

    private PdfPCell createFormCell(String cData) {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(0);
        cell.setPadding(0.00f);

        Paragraph text = new Paragraph();
        //text.setLeading(12);
        text.setAlignment(Element.ALIGN_LEFT);
        text.setFont(new Font(Font.FontFamily.COURIER, 10));
        text.add(cData);

        cell.addElement(text);
        return cell;
    }

    private PdfPTable createDataTable() {
        PdfPTable table = new PdfPTable(11);
        table.setWidthPercentage(100.0f);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setSpacingBefore(18.0f);
        return table;
    }

    private PdfPCell createTableHeaderCell(String sText, int iColspan) {
        PdfPCell cell = new PdfPCell();
        cell.setBorderWidthRight(0);
        cell.setBorderWidthLeft(0);
        cell.setColspan(iColspan);
        cell.setPadding(0.00f);
        cell.setPaddingBottom(5.0f);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        cell.addElement(this.getTableData_V2(sText));
        return cell;
    }

    private PdfPCell createTableCell(String sText, int iColspan) {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(0);
        cell.setColspan(iColspan);
        cell.setPadding(0.00f);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);

        cell.addElement(this.getTableData(sText));
        return cell;
    }

    private Paragraph getTableData_V2(String sText) {
        Paragraph text = new Paragraph();
        text.setAlignment(Element.ALIGN_LEFT);
        text.setFont(new Font(Font.FontFamily.COURIER, 10));
        text.add(sText);
        return text;
    }

    private Paragraph getHeding(String headingString) {
        Font font1 = new Font(Font.FontFamily.HELVETICA, 21);
        Paragraph HeadinPara = new Paragraph();
        HeadinPara.setAlignment(Element.ALIGN_CENTER);
        HeadinPara.setFont(font1);
        HeadinPara.add(headingString);
        return HeadinPara;
    }

    private Paragraph getTableHeading(String headingString) {
        Font font1 = new Font(Font.FontFamily.HELVETICA, 11);
        Paragraph HeadinPara = new Paragraph();
        //float linehight = 0.5;
        HeadinPara.setLeading(12);
        HeadinPara.setAlignment(Element.ALIGN_CENTER);
        HeadinPara.setFont(font1);
        HeadinPara.add(headingString);
        return HeadinPara;
    }

    private Paragraph getTableData(String headingString) {
        Font font1 = new Font(Font.FontFamily.COURIER, 9);
        Paragraph HeadinPara = new Paragraph();
        HeadinPara.setAlignment(Element.ALIGN_LEFT);
        HeadinPara.setFont(font1);
        HeadinPara.add(headingString);
        return HeadinPara;
    }

    private PdfPCell getCellColspan1() {
        //getCellColspan1();
        PdfPCell cell = new PdfPCell();
        cell.setPaddingBottom(5);
        cell.setBorderWidth(0.25f);
        cell.setColspan(1);
        return cell;
    }

    private PdfPCell getCellColspan2() {
        PdfPCell cell = new PdfPCell();
        cell.setPaddingBottom(5);
        cell.setBorderWidth(0.25f);
        cell.setColspan(2);
        return cell;
    }

    private PdfPCell getCellColspan3() {
        PdfPCell cell = new PdfPCell();
        cell.setPaddingBottom(5);
        cell.setBorderWidth(0.25f);
        cell.setColspan(3);
        return cell;
    }

    private PdfPCell getCellColspan4() {
        PdfPCell cell = new PdfPCell();
        cell.setPaddingBottom(5);
        cell.setBorderWidth(0.25f);
        cell.setColspan(4);
        return cell;
    }

    public String getTransmissionPath(HttpServletRequest request) {

        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
        String webDirectory = "/PDF";
        File web_directory = new File(webServerPath + webDirectory);
        if (!web_directory.isDirectory()) {
            web_directory.mkdirs();
        }

        String filePath = "/TransmissionReport.pdf";
        File pdf_file_path = new File(webServerPath + webDirectory + filePath);

        if (pdf_file_path.isFile()) {
            pdf_file_path.delete();
        }

        return pdf_file_path.toString();
    }

    public Boolean billPDFGenerator(HttpServletRequest request, PrintInvoicePro printData) {
        return billPDFGenerator(request, printData, getBillPath(request));
    }

    public Boolean billPDFGenerator(HttpServletRequest request, PrintInvoicePro printData, String filePath) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
            String today = dateFormat.format(new Date());
            Font normalFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
            Font boltTableHeading = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);

            String custome = "Splendor media(pvt) Ltd" + " \n " + "30/51C Longdon Place " + "\n" + " Colombo 07";
            String Channel = "Channel :AXN,Zee Cafe \n Product/Brand : Pathum Vimana \n Contract No:";
            String phrase1 = "We hereby certify that this invoice is true and correct";
            String phrase2 = "Cheques should be drawn in favour of 'Dialog Television (Private) Limited' \n "
                    + "with the invoice number qouted on the reverse of the cheque. 45 days credit period \n"
                    + " is applicable for the payment from thedate of invoice";
            String phrase3 = "This is a computer generated invoice. No signature is required and is deemed as official and final";
            String phrase4 = "This is a ";
            String phrase5 = "copy of original bill.";

            Document bill = new Document(PageSize.A4, 25, 25, 25, 25);
            PdfWriter billWriter = PdfWriter.getInstance(bill, new FileOutputStream(filePath));
            bill.open();
            bill.add(getHeding("Tax invoice"));

            PdfPTable BillDetails = getTable(2);
            BillDetails.setSpacingBefore(10);
            BillDetails.setWidthPercentage(100);

            PdfPCell cellOne = new PdfPCell();

            cellOne.addElement(new Phrase("Dialog Televission (Pvt) Ltd ", normalFont));
            cellOne.addElement(new Phrase("No 475, Union Place", normalFont));
            cellOne.addElement(new Phrase("Colombo 02", normalFont));
            cellOne.addElement(new Phrase("Sri Lanka", normalFont));
            cellOne.addElement(new Phrase("Telephone   :+94(0) 777679679", normalFont));
            cellOne.addElement(new Phrase("Fax              :+94(0) 777678697", normalFont));
            cellOne.addElement(new Phrase("Fax              :+94(0) 117678697", normalFont));
            cellOne.addElement(new Phrase("Email           :service@dialog.lk", normalFont));
            //cellOne.addElement(new Phrase("VAT Reg No:114363006-7000", normalFont));
            //cellOne.setRowspan(1);
            cellOne.setBorder(Rectangle.NO_BORDER);
            cellOne.setPadding(0);
            cellOne.setHorizontalAlignment(0);
            BillDetails.addCell(cellOne);

            PdfPCell cellTwo = new PdfPCell();
            cellTwo.addElement(new Phrase("Invoice No      :" + printData.getInvoiceid(), normalFont));
            cellTwo.addElement(new Phrase("Date                :" + today, normalFont));
            cellTwo.addElement(new Phrase("Billing Period   :" + printData.getWorkOrderPeriod(), normalFont));
            cellTwo.addElement(new Phrase("VAT Reg No  :" + printData.getVatRegNum(), normalFont));
            cellTwo.addElement(new Phrase("Currency         :LKR", normalFont));
            //cellTwo.setRowspan(1);
            cellTwo.setBorder(Rectangle.NO_BORDER);
            cellTwo.setPadding(0);
            cellTwo.setHorizontalAlignment(0);
            BillDetails.addCell(cellTwo);

            PdfPTable customerDetails = getTable(2);
            customerDetails.setSpacingBefore(10);
            customerDetails.setWidthPercentage(100);

            PdfPCell cellTree = new PdfPCell();
            cellTree.addElement(new Phrase("Customer Details", boltTableHeading));
            cellTree.setBorder(Rectangle.TOP);
            cellTree.setPadding(0);
            cellTree.setHorizontalAlignment(0);
            customerDetails.addCell(cellTree);

            PdfPCell cellFour = new PdfPCell();
            cellFour.addElement(new Phrase("End Customer Details:", boltTableHeading));
            cellFour.setBorder(Rectangle.TOP);
            cellFour.setPadding(0);
            cellFour.setHorizontalAlignment(0);
            customerDetails.addCell(cellFour);

            PdfPCell customer = getCell_p_0_HA_Left_NoBORDER(getPhrase_Normal(printData.getBillingclientAddress(), 10));
            customerDetails.addCell(customer);

            PdfPCell endCustomer = getCell_p_0_HA_Left_NoBORDER(getPhrase_Normal(printData.getClientAddress(), 10));
            customerDetails.addCell(endCustomer);

            PdfPCell filler = getCell_p_0_HA_Left_NoBORDER(new Phrase());
            customerDetails.addCell(filler);

            PdfPCell filler1 = getCell_p_0_HA_Left_NoBORDER(new Phrase());
            customerDetails.addCell(filler1);

            PdfPCell filler2 = getCell_p_0_HA_Left_NoBORDER(new Phrase());
            customerDetails.addCell(filler2);

            PdfPCell channelDetails = getCell_p_0_HA_Left_NoBORDER(getPhrase_Normal("Channel :" + printData.getChannelNames(), 10));
            customerDetails.addCell(channelDetails);

            PdfPTable detailBill = getTable(5);
            detailBill.setSpacingBefore(10);
            detailBill.setSpacingAfter(10);
            detailBill.setWidthPercentage(100);

            PdfPCell channelName = getCell_p_2_HA_Center_WithBORDER(getPhrase_Bold("Channel Name", 10));
            detailBill.addCell(channelName);

            PdfPCell contentType = getCell_p_2_HA_Center_WithBORDER(getPhrase_Bold("Content Type", 10));
            detailBill.addCell(contentType);

            PdfPCell advertCount = getCell_p_2_HA_Center_WithBORDER(getPhrase_Bold("Advert count", 10));
            detailBill.addCell(advertCount);

            PdfPCell rate = getCell_p_2_HA_Center_WithBORDER(getPhrase_Bold("Rate", 10));
            detailBill.addCell(rate);

            PdfPCell tatolAmount = getCell_p_2_HA_Center_WithBORDER(getPhrase_Bold("Tatol amount", 10));
            detailBill.addCell(tatolAmount);

            DecimalFormat df = new DecimalFormat("#0.00");
            df.setRoundingMode(RoundingMode.DOWN);
            for (int i = 0; i < printData.getSpotRecord().size(); i++) {

                PdfPCell cell = getCell_p_2_HA_Center_WithBORDER(getPhrase_Normal(printData.getSpotRecord().get(i).getChannelId().getShortname(), 10));
                detailBill.addCell(cell);

                PdfPCell cell1 = getCell_p_2_HA_Center_WithBORDER(getPhrase_Normal(printData.getSpotRecord().get(i).getContentType(), 10));
                detailBill.addCell(cell1);

                if (printData.getInvoiceType() != 2) {
                    if (printData.getInvoiceType() != 5) {
                        PdfPCell cell2 = getCell_p_2_HA_Center_WithBORDER(getPhrase_Normal("" + printData.getSpotRecord().get(i).getAllSpots(), 10));
                        detailBill.addCell(cell2);
                    } else {
                        PdfPCell cell2 = getCell_p_2_HA_Center_WithBORDER(getPhrase_Normal("" + printData.getSpotRecord().get(i).getAirSpot(), 10));
                        detailBill.addCell(cell2);
                    }
                } else {
                    PdfPCell cell2 = getCell_p_2_HA_Center_WithBORDER(getPhrase_Normal("" + printData.getSpotRecord().get(i).getAirSpot(), 10));
                    detailBill.addCell(cell2);
                }

                PdfPCell cell3 = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(df.format(Double.parseDouble(printData.getSpotRecord().get(i).getPackageAmount()) / printData.getSpotRecord().get(i).getAllSpots())), 10));
                detailBill.addCell(cell3);

                if (printData.getInvoiceType() != 2) {
                    if (printData.getInvoiceType() != 5) {
                        PdfPCell cell4 = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(df.format(Double.parseDouble(printData.getSpotRecord().get(i).getPackageAmount()))), 10));
                        detailBill.addCell(cell4);
                    } else {
                        double airTotalAmount = printData.getSpotRecord().get(i).getAirSpot() * (Double.parseDouble(printData.getSpotRecord().get(i).getPackageAmount()) / printData.getSpotRecord().get(i).getAllSpots());
                        PdfPCell cell4 = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(df.format(airTotalAmount)), 10));
                        detailBill.addCell(cell4);
                    }
                } else {
                    double airTotalAmount = printData.getSpotRecord().get(i).getAirSpot() * (Double.parseDouble(printData.getSpotRecord().get(i).getPackageAmount()) / printData.getSpotRecord().get(i).getAllSpots());
                    PdfPCell cell4 = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(df.format(airTotalAmount)), 10));
                    detailBill.addCell(cell4);
                }
            }
            PdfPCell invoicedata = getCell_p_0_HA_Left_NoBORDER(new Phrase());
            detailBill.addCell(invoicedata);
            detailBill.addCell(invoicedata);

            invoicedata = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Sub Total", 10));
            invoicedata.setColspan(2);
            detailBill.addCell(invoicedata);

            invoicedata = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(df.format(printData.getTotalAmount())), 10));
            detailBill.addCell(invoicedata);

            invoicedata = getCell_p_0_HA_Left_NoBORDER(new Phrase());
            detailBill.addCell(invoicedata);
            detailBill.addCell(invoicedata);

            invoicedata = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Agency Commision " + printData.getCommissionRate() + "%", 10));
            invoicedata.setColspan(2);
            detailBill.addCell(invoicedata);

            double commision = (printData.getTotalAmount() * printData.getCommissionRate()) / 100;

            invoicedata = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(df.format(commision)), 10));
            detailBill.addCell(invoicedata);

            invoicedata = getCell_p_0_HA_Left_NoBORDER(new Phrase());
            detailBill.addCell(invoicedata);
            detailBill.addCell(invoicedata);

            invoicedata = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Net amount ", 10));
            invoicedata.setColspan(2);
            detailBill.addCell(invoicedata);

            invoicedata = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(df.format(printData.getTotalAmount() - commision)), 10));
            detailBill.addCell(invoicedata);

            Double total_amount = printData.getTotalAmount() - commision;

            for (int i = 0; i < printData.getTaxDetails().size(); i++) {

                PdfPCell cell = getCell_p_0_HA_Left_NoBORDER(new Phrase());
                detailBill.addCell(cell);
                detailBill.addCell(cell);
                if (printData.getTaxDetails().get(i).getTaxCategory().equals("Total Payable")) {
                    cell = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold(printData.getTaxDetails().get(i).getTaxCategory(), 10));
                } else {
                    cell = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold(printData.getTaxDetails().get(i).getTaxCategory() + " " + printData.getTaxDetails().get(i).getTaxRate() + "%", 10));
                }
                cell.setColspan(2);
                detailBill.addCell(cell);

                double value = Double.parseDouble(printData.getTaxDetails().get(i).getTax());
                String st = df.format(value);
                cell = getCell_p_2_HA_Right_WithBORDER(getPhrase_Normal(addCommas(st), 10));
                detailBill.addCell(cell);
            }
            PdfPTable info = getTable(3);
            info.setSpacingBefore(10);
            info.setSpacingAfter(10);
            info.setWidthPercentage(100);

            PdfPCell cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Beneficiary", 10));
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Normal("Dialog Television(Pvt)Ltd", 10));
            cell1.setColspan(2);
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Bank", 10));
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Normal("Standard charterd Bank", 10));
            cell1.setColspan(2);
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Bank Address", 10));
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Normal("No 32, York Street, Colombo 01", 10));
            cell1.setColspan(2);
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Swift Code", 10));
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Normal("SCBLLKLX", 10));
            cell1.setColspan(2);
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Bold("Bank Account", 10));
            info.addCell(cell1);

            cell1 = getCell_p_2_HA_Left_WithBORDER(getPhrase_Normal("01-3667707-01", 10));
            cell1.setColspan(2);
            info.addCell(cell1);

            bill.add(BillDetails);
            bill.add(customerDetails);
            bill.add(detailBill);
            bill.add(getPhrase_Normal(phrase1, 10));
            bill.add(info);
            Paragraph para = getParagraph_Normal(phrase2, 10);
            para.setSpacingAfter(10);
            bill.add(para);
            bill.add(getPhrase_Normal(phrase3, 8));
            Paragraph para1 = getParagraph_Normal("  ", 10);
            para1.setSpacingAfter(2);
            bill.add(para1);

            if (printData.getPrintStatus() == 0) {
                bill.add(getPhrase_Normal(phrase4 + " original bill.", 8));
            } else {
                bill.add(getPhrase_Normal(phrase4 + " " + printData.getPrintStatus() + " " + phrase5, 8));
            }

            bill.close();

            return true;
        } catch (Exception e) {
            logger.debug("Exception in PDFGenerator in billPDFGenerator() : {}", e.getMessage());
            return false;
        }
    }

    public String addCommas(String value) {
        if (value.length() <= 3) {
            return value;
        } else {
            String[] num = value.split("\\.");
            String[] n = num[0].split("");
            String number = "";
            int count = 0;
            for (int i = n.length - 1; i >= 0; i--) {
                if (count == 3) {
                    number = n[i] + "," + number;
                    count = 0;
                } else {
                    count++;
                    number = n[i] + number;
                }
            }
            number = number + "." + num[1];
            return number;
        }
    }

    public String getBillPath(HttpServletRequest request) {

        String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
        String webDirectory = "/BillPDF";
        File web_directory = new File(webServerPath + webDirectory);
        if (!web_directory.isDirectory()) {
            web_directory.mkdirs();
        }

        String filePath = "/Bill.pdf";
        File pdf_file_path = new File(webServerPath + webDirectory + filePath);
        String billPath = webServerPath + webDirectory + filePath;

        if (pdf_file_path.isFile()) {
            pdf_file_path.delete();
        }
        return billPath;
    }

    public PdfPCell getCell_p_0_HA_Left_NoBORDER(Phrase data) {

        PdfPCell cell = new PdfPCell();
        cell.addElement(data);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(0);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        return cell;
    }

    public PdfPCell getCell_p_0_HA_Left_WithBORDER(Phrase data) {

        PdfPCell cell = new PdfPCell();
        cell.addElement(data);
        cell.setPadding(0);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        return cell;
    }

    public PdfPCell getCell_p_0_HA_Center_WithBORDER(Phrase data) {

        PdfPCell cell = new PdfPCell();
        cell.setPadding(0);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_MIDDLE);
        cell.addElement(data);

        return cell;
    }

    public PdfPCell getCell_p_2_HA_Left_WithBORDER(Phrase data) {

        PdfPCell cell = new PdfPCell();
        cell.addElement(data);
        cell.setPadding(2);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);

        return cell;
    }

    public PdfPCell getCell_p_2_HA_Right_WithBORDER(Phrase data) {

        PdfPCell cell = new PdfPCell(data);
        cell.setPadding(2);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setVerticalAlignment(Element.ALIGN_BOTTOM);

        return cell;
    }

    public PdfPCell getCell_p_2_HA_Center_WithBORDER(Phrase data) {

        PdfPCell cell = new PdfPCell();
        cell.setPadding(2);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_MIDDLE);
        cell.addElement(data);

        return cell;
    }

    public Phrase getPhrase_Normal(String data, int fontSize) {

        Font normalFont = new Font(Font.FontFamily.HELVETICA, fontSize, Font.NORMAL);
        Phrase phrase = new Phrase(data, normalFont);
        return phrase;
    }

    public Phrase getPhrase_Bold(String data, int fontSize) {

        Font normalFont = new Font(Font.FontFamily.HELVETICA, fontSize, Font.BOLD);
        Phrase phrase = new Phrase(data, normalFont);
        return phrase;
    }

    public Paragraph getParagraph_Normal(String data, int fontSize) {
        Font normalFont = new Font(Font.FontFamily.HELVETICA, fontSize, Font.NORMAL);
        Paragraph paragraph = new Paragraph(data, normalFont);
        return paragraph;
    }

    public PdfPTable getTable(int columCount) {
        return new PdfPTable(columCount);
    }
}
