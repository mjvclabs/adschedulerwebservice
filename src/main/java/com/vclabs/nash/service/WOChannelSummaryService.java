package com.vclabs.nash.service;

import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.vo.WOChannelSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dperera on 30/04/2020.
 */
@Service
public class WOChannelSummaryService {

    @Autowired
    private SchedulerDAO schedulerDAO;

    @Autowired
    private WorkOrderDAO workOrderDAO;

    @Transactional
    public List<WOChannelSummary> getWorkOrderChannelSummaries(int channelId, List<Integer> woIds){
        List<WOChannelSummary> summaryList = workOrderDAO.getWorkOrderChannelSummary(channelId, woIds);
        List<WOChannelSummary> scheduleSummaryList = schedulerDAO.getWorkOrderChannelSummaries(channelId, woIds);
        for(WOChannelSummary summary : summaryList){
            for(WOChannelSummary cs : scheduleSummaryList) {
                if(summary.getWorkOrderId() == cs.getWorkOrderId()){
                    summary.setConsumedDuration(cs.getConsumedDuration());
                    break;
                }
            }
            WorkOrder workOrder = workOrderDAO.getSelectedWorkOrder(summary.getWorkOrderId());
            boolean isZeroBudget = !(workOrder.getTotalBudget() > 0);
            summary.setZeroBudget(isZeroBudget);
        }
        return summaryList;
    }
}
