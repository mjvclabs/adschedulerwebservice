/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.vclabs.dashboard.core.exception.DashboardCoreException;
import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.model.dao.*;
import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.model.process.MoshDetails;
import com.vclabs.nash.model.view.MoshMsg;
import com.vclabs.nash.service.Mosh.MoshUserLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import com.vclabs.nash.model.process.UserGroupInfo;
import com.vclabs.nash.model.process.UserInfo;
import com.vclabs.nash.model.view.UserGroupView;
import com.vclabs.nash.model.view.UserView;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author user
 */
@Service
@Transactional
public class UserDetailsService {

    @Autowired
    private UserDetailsDAO userdetails;
    @Autowired
    private PageNameDAO pageNameDao;
    @Autowired
    private UserGroupDAO userGroupDao;
    @Autowired
    private AuthorityDAO authorityDao;
    @Autowired
    private UrlPageMapDAO urlPageMapDao;
//    @Autowired
//    private DashboardManager dashboardManager;
    @Autowired
    private MoshUserLogService moshUserLogService;

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsService.class);

    public MoshMsg getLoginPermission(String username, String passWord,MoshDetails moshDetails) {

        MoshMsg msg=new MoshMsg();
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA");
        } catch (NoSuchAlgorithmException ex) {
            logger.debug("Exception in UserDetailsService in getLoginPermission() : {}", ex.getMessage());
        }

        md.update(passWord.getBytes());
        byte byteData[] = md.digest();

        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        passWord = hexString.toString();

        List<SecurityUsersEntity> userList = userdetails.getUserDetailsList(username, passWord);
        if (userList.isEmpty()) {
            msg.setId(-1);
            msg.setStatus(false);
            logger.debug("Completed Mosh login request processing. [ user = {} ] ", username);
            return msg;
        } else if (userList.size() == 1) {
            msg.setId(moshUserLogService.save(moshDetails).getId());
            msg.setStatus(true);
            logger.debug("Completed Mosh login request processing. [ user = {} ] ", username);
            return msg;
        } else {
            msg.setId(-1);
            msg.setStatus(false);
            logger.debug("Completed Mosh login request processing. [ user = {} ] ", username);
            return msg;
        }
    }

    public void moshUserLogout(MoshDetails moshDetails) {
        moshUserLogService.update(moshDetails);
    }

    public Boolean setUserDetails(String userName, String details, String dashboardGroups) throws IOException {

        if (getUservalidation(userName)) {
            ObjectMapper mapper = new ObjectMapper();
            UserInfo useData;
            useData = mapper.readValue(details, UserInfo.class);
            SecurityUsersEntity user = new SecurityUsersEntity();
            user.setUsername(userName);
            user.setPassword(useData.getPassword());
            user.setAddress(useData.getAddress());
            user.setContact(useData.getContact());
            user.setEmail(useData.getEmail());
            user.setName(useData.getName());
            user.setEmployeeId(useData.getEmployee_id());
            user.setChecker(0);
            user.setUsergroup(useData.getUserGroup());
            user.setCreateWorkOrder(Short.parseShort(useData.getWoCreatePremission()));
            user.setUsername(userName);
            List<UserGroup> userGroupList = userGroupDao.getSelectedUserGroup(useData.getUserGroup());
            userdetails.setUserDetails(user);
            for (int i = 0; i < userGroupList.size(); i++) {
                SecurityAuthoritiesEntity auth = new SecurityAuthoritiesEntity(user, "ROLE_" + userGroupList.get(i).getPageid().getPageName());
                authorityDao.saveAuthority(auth);
            }
            if (user.getCreateWorkOrder() == 1) {
                addCreateWorkOrde(user);
            }
            saveUserToDashboard(userName, userdetails.getUserDetails(userName).getPassword(), dashboardGroups);
            return true;
        } else {
            return false;
        }

    }

    public Boolean getUservalidation(String username) {
        List<SecurityUsersEntity> userList = userdetails.getUserValidation(username);
        if (userList.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public int updateUser(String oldUserName, String newUserName, String oldPassword, String newPassword, String group) {
        if ((userdetails.getUserDetailsList(oldUserName, oldPassword)).size() == 1) {
            SecurityUsersEntity userDetails = new SecurityUsersEntity();
            userDetails.setPassword(newPassword);
            userDetails.setUsername(newUserName);
            userdetails.updateUserDetails(userDetails);

            return 1;
        } else {
            return 0;
        }
    }

    public List<SecurityUsersEntity> getAllUsers() {
        List<SecurityUsersEntity> userList = userdetails.getAllUser();
        return userList;
    }

    public List<UserView> getAllUser() throws Exception {

        List<SecurityUsersEntity> userList = userdetails.getAllUser();
        List<UserView> viewList = new ArrayList<>();

        for (int i = 0; i < userList.size(); i++) {
            Boolean woCreate = false;
            if (userList.get(i).getCreateWorkOrder() == 1) {
                woCreate = true;
            }
            viewList.add(new UserView(userList.get(i).getUsername(), userList.get(i).getName(), userList.get(i).getEmployeeId(), userList.get(i).getEmail(), userList.get(i).getAddress(), userList.get(i).getContact(), userList.get(i).getEnabled(), userList.get(i).getUsergroup(), woCreate));
        }

        return viewList;
    }

    public List<UserView> getMEUser() throws Exception { //tested

        List<SecurityUsersEntity> userList = userdetails.getUserList("");
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getUsername().equals("SU")) {
                userList.remove(userList.get(i));
                break;
            }
        }
        List<UserView> viewList = new ArrayList<>();

        for (int i = 0; i < userList.size(); i++) {
            Boolean woCreate = false;
            if (userList.get(i).getCreateWorkOrder() == 1) {
                woCreate = true;
            }
            viewList.add(new UserView(userList.get(i).getUsername(), userList.get(i).getName(), userList.get(i).getEmployeeId(), userList.get(i).getEmail(), userList.get(i).getAddress(), userList.get(i).getContact(), userList.get(i).getEnabled(), userList.get(i).getUsergroup(), woCreate));
        }

        return viewList;
    }

    public UserView getSelectedUser(String userName) {
        try {
            SecurityUsersEntity user = userdetails.getUserDetails(userName);
            Boolean woCreate = false;
            if (user.getCreateWorkOrder() == 1) {
                woCreate = true;
            }
            return new UserView(user.getUsername(), user.getName(), user.getEmployeeId(), user.getEmail(), user.getAddress(), user.getContact(), user.getEnabled(), user.getUsergroup(), woCreate);
        } catch (Exception e) {
            return null;
        }

    }

    public int getCheckar(String userName) {

        SecurityUsersEntity user = userdetails.getUserDetails(userName);

        return user.getChecker();

    }

    public Boolean setUserStatus(String userName, int status) {

        SecurityUsersEntity user = userdetails.getUserDetails(userName);
        user.setEnabled((short) status);
        userdetails.updateUserDetails(user);

        return true;
    }

    public Boolean updateUserInfo(String userName, String userInfo, String dashboardGroups) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        UserInfo useData;
        useData = mapper.readValue(userInfo, UserInfo.class);

        SecurityUsersEntity olduser = userdetails.getUserDetails(userName);

        SecurityUsersEntity newUser = new SecurityUsersEntity();
        newUser.setPassword(useData.getPassword());

        if (useData.getUserGroup().equals("Not_admin")) {
            if (olduser.getPassword().equals(newUser.getPassword())) {
                olduser.setAddress(useData.getAddress());
                olduser.setContact(useData.getContact());
                olduser.setEmail(useData.getEmail());
                olduser.setName(useData.getName());
                olduser.setEmployeeId(useData.getEmployee_id());
                int check = olduser.getChecker();
                check++;
                olduser.setChecker(check);

                if (!useData.getNewpassword().equals("")) {
                    olduser.setPassword(useData.getNewpassword());
                }
                userdetails.updateUserDetails(olduser);
            } else {
                return false;
            }
        } else {
            olduser.setAddress(useData.getAddress());
            olduser.setContact(useData.getContact());
            olduser.setEmail(useData.getEmail());
            olduser.setName(useData.getName());
            olduser.setEmployeeId(useData.getEmployee_id());
            olduser.setCreateWorkOrder(Short.parseShort(useData.getWoCreatePremission()));

            if (!useData.getNewpassword().equals("")) {
                olduser.setPassword(useData.getNewpassword());
            }
            int check = olduser.getChecker();
            check++;
            olduser.setChecker(check);
            if (!useData.getUserGroup().equals("Not_admin")) {
                if (!olduser.getUsergroup().equals(useData.getUserGroup())) {
                    olduser.setUsergroup(useData.getUserGroup());

                    List<SecurityAuthoritiesEntity> athorityList = authorityDao.getAuthority(userName);
                    for (int i = 0; i < athorityList.size(); i++) {
                        authorityDao.deleteAuthority(athorityList.get(i));
                    }

                    List<UserGroup> userGroupList = userGroupDao.getSelectedUserGroup(useData.getUserGroup());
                    for (int i = 0; i < userGroupList.size(); i++) {
                        SecurityAuthoritiesEntity auth = new SecurityAuthoritiesEntity(olduser, "ROLE_" + userGroupList.get(i).getPageid().getPageName());
                        authorityDao.saveAuthority(auth);
                    }
                }
//                if(!olduser.getSecurityAuthoritiesEntityCollection().equals(authorityDao.getAuthority(userName))){
//
//                }
            }

            if (olduser.getCreateWorkOrder() == 1) {
                addCreateWorkOrde(olduser);
            } else {
                removeCreateWorkOrde(olduser);
            }

            userdetails.updateUserDetails(olduser);
        }
        com.vclabs.dashboard.core.data.dto.User user = new com.vclabs.dashboard.core.data.dto.User();
        user.setPassword(olduser.getPassword());
        user.setUsername(olduser.getUsername());
/*================Dashboard=====================*/
//        try {
//            dashboardManager.updateUserPassword(user);
//            user.setGroups(setUserGroups(dashboardGroups));
//            dashboardManager.update(user);
//        } catch (Exception e) {
//
//        }
        /*================Dashboard=====================*/
        return true;
    }

    public List<PageName> getAllPage() throws Exception {
        return pageNameDao.getAllPages();
    }

    public List<UserGroupView> getAllUserGroup() throws Exception {

        List<UserGroupView> userGroupList = new ArrayList<>();
        List<UserGroup> userList = userGroupDao.getAllUserGroup();

        for (int i = 0; i < userList.size(); i++) {
            UserGroupView model = new UserGroupView();
            model.setGroupname(userList.get(i).getUserGroupName());
            model.setPageId(userList.get(i).getPageid().getPageId());
            model.setPagename(userList.get(i).getPageid().getPageName());
            model.setReadAcc(userList.get(i).getReadAcc());
            model.setWriteAcc(userList.get(i).getWriteAcc());

            userGroupList.add(model);
        }

        return userGroupList;
    }

    public Boolean saveUserGroup(String userGroupData) throws Exception {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<UserGroupInfo> newuserGroupList = mapper.readValue(userGroupData, new TypeReference<List<UserGroupInfo>>() {
            });
            List<UserGroup> userList = userGroupDao.getAllUserGroup();
            List<String> updatedGroupName = new ArrayList<>();

            for (int i = 0; i < newuserGroupList.size(); i++) {

                for (int j = 0; j < userList.size(); j++) {
                    if (newuserGroupList.get(i).getGroupname().equals(userList.get(j).getUserGroupName())
                            && newuserGroupList.get(i).getPageId() == userList.get(j).getPageid().getPageId()) {
                        userList.get(j).setReadAcc(newuserGroupList.get(i).getReadAcc());
                        userList.get(j).setWriteAcc(newuserGroupList.get(i).getWriteAcc());

                        userGroupDao.updateUserGroup(userList.get(j));
                        userList.remove(userList.get(j));
                        j--;
                        newuserGroupList.remove(newuserGroupList.get(i));
                        i--;
                        break;
                    }
                }

            }

            Boolean addORDelete = false;
            String groupName = "";
            for (int i = 0; i < newuserGroupList.size(); i++) {

                UserGroup model = new UserGroup();
                model.setUserGroupName(newuserGroupList.get(i).getGroupname());
                model.setPageid(new PageName(newuserGroupList.get(i).getPageId(), newuserGroupList.get(i).getPagename()));
                model.setReadAcc(newuserGroupList.get(i).getReadAcc());
                model.setWriteAcc(newuserGroupList.get(i).getWriteAcc());

                userGroupDao.saveUserGroup(model);

                addORDelete = true;

                if (!groupName.equals(model.getUserGroupName())) {
                    groupName = model.getUserGroupName();
                    updatedGroupName.add(groupName);
                }

            }

            for (int i = 0; i < userList.size(); i++) {
                userGroupDao.deleteUserGroup(userList.get(i));
                addORDelete = true;
                if (!groupName.equals(userList.get(i).getUserGroupName())) {
                    groupName = userList.get(i).getUserGroupName();
                    updatedGroupName.add(groupName);
                }
            }
            if (addORDelete) {
                for (int i = 0; i < updatedGroupName.size(); i++) {
                    checkUsers(updatedGroupName.get(i));
                }
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public Boolean checkUsers(String userGroup) {
        try {
            /**/
            List<SecurityUsersEntity> userList = userdetails.getUserListForCheck(userGroup);
            for (int i = 0; i < userList.size(); i++) {
                List<UserGroup> userGroupList = userGroupDao.getSelectedUserGroup(userGroup);
                List<SecurityAuthoritiesEntity> athorityList = authorityDao.getAuthority(userList.get(i).getUsername());
                for (int j = 0; j < athorityList.size(); j++) {
                    for (int k = 0; k < userGroupList.size(); k++) {
                        if (athorityList.get(j).getAuthority().equals("ROLE_" + userGroupList.get(k).getPageid().getPageName())) {
                            athorityList.remove(athorityList.get(j));
                            j--;
                            userGroupList.remove(userGroupList.get(k));
                            k--;
                            break;
                        }
                    }
                }
                for (int j = 0; j < athorityList.size(); j++) {
                    authorityDao.deleteAuthority(athorityList.get(j));
                }

                for (int j = 0; j < userGroupList.size(); j++) {
                    SecurityAuthoritiesEntity auth = new SecurityAuthoritiesEntity(userList.get(i), "ROLE_" + userGroupList.get(j).getPageid().getPageName());
                    authorityDao.saveAuthority(auth);
                }

            }

            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public UserView getLoginUser() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserView loginUser = getSelectedUser(user.getUsername());
        return loginUser;
    }

    public void addCreateWorkOrde(SecurityUsersEntity user) {

        Boolean haveAthority = false;
        List<SecurityAuthoritiesEntity> authoritiList = authorityDao.getAuthority(user.getUsername());
        for (SecurityAuthoritiesEntity authority : authoritiList) {
            if (authority.getAuthority().equals("ROLE_NewWorkOrder") || authority.getAuthority().equals("ROLE_UpdateWorkOrder")) {
                haveAthority = true;
            }
        }

        if (!haveAthority) {
            SecurityAuthoritiesEntity newWorkOrder = new SecurityAuthoritiesEntity(user, "ROLE_NewWorkOrder");
            SecurityAuthoritiesEntity updateWorkOrder = new SecurityAuthoritiesEntity(user, "ROLE_UpdateWorkOrder");
            authorityDao.saveAuthority(newWorkOrder);
            authorityDao.saveAuthority(updateWorkOrder);
        }
    }

    public void removeCreateWorkOrde(SecurityUsersEntity user) {
        List<SecurityAuthoritiesEntity> authoritiList = authorityDao.getAuthority(user.getUsername());
        for (SecurityAuthoritiesEntity authority : authoritiList) {
            if (authority.getAuthority().equals("ROLE_NewWorkOrder") || authority.getAuthority().equals("ROLE_UpdateWorkOrder")) {
                authorityDao.deleteAuthority(authority);
            }
        }
    }

    public void saveUserToDashboard(String userName, String password, String dashboardGroups) {
        com.vclabs.dashboard.core.data.dto.User user = new com.vclabs.dashboard.core.data.dto.User();
        List<com.vclabs.dashboard.core.data.dto.UserGroup> dashboardUserGroupList = setUserGroups(dashboardGroups);

        user.setUsername(userName);
        user.setPassword(password);
        user.setGroups(dashboardUserGroupList);
        /*================Dashboard=====================*/
//        try {
//            dashboardManager.saveUser(user);
//        } catch (DashboardCoreException e) {
//            logger.debug("Execption occurred while trying to save User to Dashboard : UserDetailService/saveUserToDashboard : {}", e.getMessage());
//        }
        /*================Dashboard=====================*/
    }

    private List<com.vclabs.dashboard.core.data.dto.UserGroup> setUserGroups(String dashboardGroups){
        List<String> userGroups = Arrays.asList(dashboardGroups.replace("'", "").split("\\s*,\\s*"));
        List<com.vclabs.dashboard.core.data.dto.UserGroup> dashboardUserGroupList = new ArrayList<>();

        for (String userGroup : userGroups) {
            if(!userGroup.equals("")){
                com.vclabs.dashboard.core.data.dto.UserGroup dashboardusergroup = new com.vclabs.dashboard.core.data.dto.UserGroup();
                dashboardusergroup.setId(Long.valueOf(userGroup));
                dashboardUserGroupList.add(dashboardusergroup);
            }
        }
        return dashboardUserGroupList;
    }

    public List<UserView> getTeamlessUser() {

        List<SecurityUsersEntity> userList = userdetails.getTeamlessUser();
        List<UserView> viewList = new ArrayList();
        for (int i = 0; i < userList.size(); i++) {
            viewList.add(new UserView(userList.get(i).getUsername(), userList.get(i).getName(), userList.get(i).getEmployeeId(), userList.get(i).getEmail(), userList.get(i).getAddress(), userList.get(i).getContact(), userList.get(i).getEnabled(), userList.get(i).getUsergroup(),false));
        }
        return viewList;
    }
}
