/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.service.vo.WOChannelSummary;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Sanira Nanayakkara
 */
public class ScheduleGenerateV2 {

    int i_ChannelId;
    int i_ClusterCount;
    int i_hour;
    int i_previousFillerId =0;
    Date dt_StartTime;
    Date dt_EndTime;
    private ArrayList<ArrayList<PlayList>> lst_Schedule;
    private List<Advertisement> lst_Fillers;
    List<ScheduleDef> lst_Prioritys;
    List<PriorityDef> lst_PrioritieDefs;
    private int[] ar_FillerCount;
    private ChannelAdvertMapService channelAdvertMapService;
    private PlayListService playListService;
    private List<PlayList> zeroBudgetPlayLists;
    private AdvertSettingService advertSettingService;
    private WOChannelSummaryService woChannelSummaryService;
    private int[] zbPLCount; //zero budget playlist count
    private int[] zaPLCount; //zero add play list count

    private List<Integer> genZeroAds; //generated zero adverts
    private List<Integer> conZeroAds; //consumed zero adverts
    private List<WOChannelSummary> woChannelSummaries;
    private boolean regenerateRequest;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ScheduleGenerateV2.class);

    public ScheduleGenerateV2(int channel_id, int hour, Date start_time, Date end_time, int cluster_count, List<Advertisement> lstFillers,
                              ChannelAdvertMapService channelAdvertMapService, PlayListService playListService, AdvertSettingService advertSettingService) {
        this.i_ChannelId = channel_id;
        this.i_ClusterCount = cluster_count;
        this.i_hour = hour;
        this.dt_StartTime = start_time;
        this.dt_EndTime = end_time;
        this.lst_Schedule = new ArrayList<>();
        for (int i = 0; i < i_ClusterCount; i++) {
            lst_Schedule.add(new ArrayList<>());
        }
        ar_FillerCount = new int[cluster_count];
        zbPLCount = new int[cluster_count];
        zaPLCount = new int[cluster_count];
        this.lst_Fillers = lstFillers;
        this.channelAdvertMapService = channelAdvertMapService;
        this.playListService = playListService;
        this.advertSettingService = advertSettingService;
        setZeroBudgetPlayLists();
        genZeroAds = new ArrayList<>();
        conZeroAds = new ArrayList<>();
        regenerateRequest = false;
    }

    public void setRegenerateRequest(boolean regenerateRequest) {
        this.regenerateRequest = regenerateRequest;
    }

    public void updateLstSchedule(int clusterIndex, List<PlayList> playLists){
        for(int i = 0; i < i_ClusterCount; i++){
            if(clusterIndex == i && playLists != null && !playLists.isEmpty()){
                ArrayList<PlayList> cluster = lst_Schedule.get(i);
                cluster.addAll(playLists);
                break;
            }
        }
    }

    private void setZeroBudgetPlayLists(){
        this.zeroBudgetPlayLists = playListService.getZeroBudgetPlayLists(i_ChannelId, dt_StartTime, dt_EndTime, i_hour);
    }

    public void setPrioritySchedulesList(List<ScheduleDef> lstScheduleList, List<PriorityDef> lstPriorities) {
        this.lst_Prioritys = lstScheduleList;
        this.lst_PrioritieDefs = lstPriorities;
    }

    private boolean isZeroBudgetPlayList(PlayList pl){
        if(zeroBudgetPlayLists == null || zeroBudgetPlayLists.size() == 0){
            return false;
        }
        for(PlayList zbPL : zeroBudgetPlayLists){
            if(zbPL.getPlaylistId().equals(pl.getPlaylistId())){
                return true;
            }
        }
        return false;
    }

    private void updateConsumedDuration(PlayList playList) {

        int woId = 0;
        int spentTime = 0;
        if(playList != null) {
            woId = playList.getWorkOrder().getWorkorderid();
            spentTime = playList.getAdvert().getDuration();
        }
//        int woId = playList.getWorkOrder().getWorkorderid();
//        int spentTime = playList.getAdvert().getDuration();

        WOChannelSummary woChannelSummary = null;
        for (WOChannelSummary wo : woChannelSummaries) {
            if (wo.getWorkOrderId() == woId) {
                woChannelSummary = wo;
                break;
            }
        }


        if(woChannelSummary == null || woChannelSummary.getBonusDuration() == 0){
            return;
        }

        woChannelSummary.setConsumedDuration(woChannelSummary.getConsumedDuration() + spentTime);
        if(woChannelSummary.getConsumedDuration() >= woChannelSummary.getPaidDuration()){
            reprocessZbLists(woId, lstPriority_00, zb_lstPriority_00);
            reprocessZbLists(woId, lstPriority_01, zb_lstPriority_01);
            reprocessZbLists(woId, lstPriority_02, zb_lstPriority_02);
            reprocessZbLists(woId, lstPriority_03, zb_lstPriority_03);
            reprocessZbLists(woId, lstPriority_lh, zb_lstPriority_lh);
            reprocessZbLists(woId, lstPriority_last, zb_lstPriority_last);
        }
    }

    private void reprocessZbListsBeforeScheduling(){
        if(woChannelSummaries == null){
            return;
        }
        for(WOChannelSummary wo : woChannelSummaries){
            int woId = wo.getWorkOrderId();
            if(wo.isZeroBudget() || wo.getConsumedDuration() >= wo.getPaidDuration()){
                reprocessZbLists(woId, lstPriority_00, zb_lstPriority_00);
                reprocessZbLists(woId, lstPriority_01, zb_lstPriority_01);
                reprocessZbLists(woId, lstPriority_02, zb_lstPriority_02);
                reprocessZbLists(woId, lstPriority_03, zb_lstPriority_03);
                reprocessZbLists(woId, lstPriority_lh, zb_lstPriority_lh);
                reprocessZbLists(woId, lstPriority_last, zb_lstPriority_last);
            }
        }
    }

    private void reprocessZbLists(int woId, ArrayList<PlayList> priorityList, ArrayList<PlayList> zbList){
        Iterator<PlayList> prIterator = priorityList.iterator();
        while (prIterator.hasNext()) {
            PlayList playList = prIterator.next();
            if(playList.getWorkOrder().getWorkorderid().intValue() == woId){
                zbList.add(playList);
                prIterator.remove();
            }
        }
    }

    public void setWoChannelSummaryService(WOChannelSummaryService woChannelSummaryService) {
        this.woChannelSummaryService = woChannelSummaryService;
    }

    private void setWoChannelSummaries(ArrayList<PlayList> playLists){
        Set<Integer> woIds = new HashSet<>();
        for(PlayList playList: playLists){
            woIds.add(playList.getWorkOrder().getWorkorderid());
        }
        woChannelSummaries = woChannelSummaryService.getWorkOrderChannelSummaries(i_ChannelId, new ArrayList<>(woIds));
    }

    private ArrayList<PlayList> lstPriority_00, lstPriority_01, lstPriority_02, lstPriority_03, lstPriority_lh, lstPriority_last ;
    private ArrayList<PlayList> zb_lstPriority_00, zb_lstPriority_01, zb_lstPriority_02, zb_lstPriority_03, zb_lstPriority_lh, zb_lstPriority_last;

    //assume : schedules rounded to hours - no minuts
    public boolean generatePlayListAdvert(ArrayList<PlayList> lstPlayList, int nextClusterIndex) {
        if(!lstPlayList.isEmpty()){
            setWoChannelSummaries(lstPlayList);
        }
        lstPriority_00 = new ArrayList<>(); //endtime_first_half_hour
        lstPriority_01 = new ArrayList<>(); //endtime_this_hour
        lstPriority_02 = new ArrayList<>(); //endtime_next_hour
        lstPriority_03 = new ArrayList<>(); //endtime_other_hour
        lstPriority_lh = new ArrayList<>(); //starttime_last_half_timbelt
        lstPriority_last = new ArrayList<>(); //Inactive adverts received on reshuffle request

        //zero budget play lists
        zb_lstPriority_00 = new ArrayList<>(); //endtime_first_half_hour
        zb_lstPriority_01 = new ArrayList<>(); //endtime_this_hour
        zb_lstPriority_02 = new ArrayList<>(); //endtime_next_hour
        zb_lstPriority_03 = new ArrayList<>(); //endtime_other_hour
        zb_lstPriority_lh = new ArrayList<>(); //starttime_last_half_timbelt
        zb_lstPriority_last = new ArrayList<>(); //Inactive adverts received on reshuffle request

        //set_conflicting cuts
        this.setSchedulesConflicts(lstPlayList);

        //set_adverts_priorities
        for (int iCluster = 0; iCluster < this.i_ClusterCount; iCluster++) {
            ArrayList<PlayList> lst_cluster = lst_Schedule.get(iCluster);
            int iPriorityCount = 0;
            for (PriorityDef item : lst_PrioritieDefs) {
                if (item.getCluster() == iCluster) {
                    iPriorityCount++;
                }
            }

            for (int iPriority = 1; iPriority <= iPriorityCount; iPriority++) {
                for (ScheduleDef pSchedule : lst_Prioritys) {
                    if (pSchedule.getPriority().getCluster() == iCluster && pSchedule.getPriority().getPriority() == iPriority) {
                        for (PlayList item : lstPlayList) {
                            if (item.getSchedule().getScheduleid().equals(pSchedule.getScheduleid())) {
                                item.setClusterPriority(iPriority);
                                lst_cluster.add(item);
                                lstPlayList.remove(item);
                                LOGGER.debug("Priority scheduled. ScheduleId={}, Icuster={}, Priority={} ", item.getSchedule().getScheduleid(), iCluster, iPriority);
                                break;
                            }
                        }
                    }
                }
            }
        }

        long hour_duration = dt_EndTime.getTime() - dt_StartTime.getTime();

        for (PlayList item : lstPlayList) {
            long available_duration = item.getScheduleEndTime().getTime() - dt_StartTime.getTime();
            long valid_duration = dt_EndTime.getTime() - item.getScheduleStartTime().getTime();

            boolean isSHAdvertsValid = true;
            if(nextClusterIndex == 2) {
                Date currentTime = new Date();
                int elapsedTime = (currentTime.getMinutes() * 60) + currentTime.getSeconds();
                System.out.println("elapsedTime"+elapsedTime);
                if (elapsedTime < (30 * 60)) {
                    isSHAdvertsValid = false;
                }
            }

            if (valid_duration <= (hour_duration * 7) / 12) { //last 35_min
                if(isZeroBudgetPlayList(item)){
                    if(isSHAdvertsValid){
                        zb_lstPriority_lh.add(item);
                    }else {
                        zb_lstPriority_last.add(item);
                    }
                }else {
                    if(isSHAdvertsValid) {
                        lstPriority_lh.add(item);
                    }else {
                        lstPriority_last.add(item);
                    }
                }
            } else if (available_duration <= (hour_duration * 7) / 12) { //first 35_min
                if(isZeroBudgetPlayList(item)){
                    zb_lstPriority_00.add(item);
                }else {
                    lstPriority_00.add(item);
                }
            } else if (available_duration <= hour_duration) {
                if(isZeroBudgetPlayList(item)){
                    zb_lstPriority_01.add(item);
                }else {
                    lstPriority_01.add(item);
                }
            } else if (available_duration <= (hour_duration * 2)) {
                if(isZeroBudgetPlayList(item)){
                    zb_lstPriority_02.add(item);
                }else {
                    lstPriority_02.add(item);
                }
            } else {
                if(isZeroBudgetPlayList(item)){
                    zb_lstPriority_03.add(item);
                }else {
                    lstPriority_03.add(item);
                }
            }
        }
        reprocessZbListsBeforeScheduling();
        //dialog req : schedule order must change daily
        Collections.shuffle(lstPriority_00);
        Collections.shuffle(lstPriority_01);
        Collections.shuffle(lstPriority_02);
        Collections.shuffle(lstPriority_03);
        Collections.shuffle(lstPriority_lh);

        Collections.shuffle(zb_lstPriority_00);
        Collections.shuffle(zb_lstPriority_01);
        Collections.shuffle(zb_lstPriority_02);
        Collections.shuffle(zb_lstPriority_03);
        Collections.shuffle(zb_lstPriority_lh);

        List<List<PlayList>> zbLists = new ArrayList<>();

        if(!isFirstHalf() && lstPriority_lh.size() > 0) {
            lstPriority_00.addAll(lstPriority_lh);
            lstPriority_lh.clear();
        }

        //this is to fix: When regenerate call to 2nd cluster in 1st half, 2nd half ads mixing with 1st half ads. 20201006
        if(this.i_ClusterCount - 1 == nextClusterIndex && isFirstHalf()) {
            if(!lstPriority_lh.isEmpty()) {
                zb_lstPriority_last.addAll(lstPriority_lh);
                lstPriority_lh.clear();
            }
            if(!zb_lstPriority_lh.isEmpty()) {
                zb_lstPriority_last.addAll(zb_lstPriority_lh);
                zb_lstPriority_lh.clear();
            }
        }

        //priority list 00 ===========================================================================================
        if (lstPriority_00.size() > 0) { // || zb_lstPriority_00.size() > 0
            ArrayList<PlayList> lst_cluster = lst_Schedule.get(nextClusterIndex);

            PlayList previous_schedule = null;
            if (lst_cluster.size() > 0) {
                PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
                System.out.println(tempPlayList);
//                if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                    previous_schedule = tempPlayList;
//                }
                if(tempPlayList != null && tempPlayList.getStatus() != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                    previous_schedule = tempPlayList;
                }
            }

            //set zero budget list
            zbLists.add(zb_lstPriority_00);
            zbLists.add(zb_lstPriority_01);
            zbLists.add(zb_lstPriority_02);
            zbLists.add(zb_lstPriority_03);
            int advertCount = lstPriority_00.size();
            if(!isFirstHalf() && i_ClusterCount > nextClusterIndex) {
                advertCount =  lstPriority_00.size() / (i_ClusterCount - nextClusterIndex);
            }
            while (advertCount > 0) { //while (lstPriority_00.size() > 0 || zb_lstPriority_00.size() > 0) {
                if(!isFirstHalf() && lstPriority_01.isEmpty() && lstPriority_01.isEmpty() && lstPriority_02.isEmpty() && lstPriority_03.isEmpty()){
                    if(!zb_lstPriority_lh.isEmpty()) {
                        lstPriority_01.addAll(zb_lstPriority_lh);
                        zb_lstPriority_lh.clear();
                    }
                }
                PlayList tmp_item = null;
                if (previous_schedule == null) {//first_item
                    tmp_item = lstPriority_00.get(0);
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_00.remove(0);
                    advertCount--;
                    updateConsumedDuration(tmp_item);
                } else if ((null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_00, previous_schedule)))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_00.remove(tmp_item);
                    advertCount--;
                    updateConsumedDuration(tmp_item);
                } else if (null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_01, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_01.remove(tmp_item);
                    updateConsumedDuration(tmp_item);
                } else if (null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_02, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_02.remove(tmp_item);
                    updateConsumedDuration(tmp_item);
                } else if (null != (tmp_item = this.findMatching03PlayList(lst_cluster, lstPriority_03, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_03.remove(tmp_item);
                    updateConsumedDuration(tmp_item);
                } else if(null != (tmp_item = getMatchedZeroBudgetPlayList(lst_cluster, 0, zbLists))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    // For Jira issue NRM-807 - 21/10/2021
                    for (int j=0; j < zbLists.size(); j++){
                        if (zbLists.get(j).size() != 0) {
                            zbLists.get(j).remove(tmp_item);
                        }
                    }
                    //End
                    zbPLCount[0]++;
                    updateConsumedDuration(tmp_item);
                } else if(previous_schedule.getPlaylistId() == null && (null != (tmp_item = getZeroAdvertisement(lst_cluster, 0)))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[0]++;
                } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule)))  {
                    previous_schedule = tmp_item;
                    tmp_item.setScheduleEndTime(previous_schedule.getScheduleEndTime());
                    tmp_item.setActualEndTime(previous_schedule.getActualEndTime());
                    lst_cluster.add(tmp_item);
                    ar_FillerCount[0]++;
                }  else {
                    tmp_item = getZeroAdvertisement(lst_cluster, 0);
                    if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                        tmp_item = getNewPlayListObject(previous_schedule);
                    }
                    previous_schedule = tmp_item;
                    if(tmp_item == null){
                        continue;
                    }
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[0]++;
                }
            }
        }
        reprocessZbListsBeforeScheduling();
        if(lstPriority_00.size() > 0) {
            lstPriority_01.addAll(lstPriority_00);
            lstPriority_00.clear();
        }
        //status: lstPriority_00.size = 0

        //priority list 01 ===========================================================================================
        /**
         * these schedules aren't scheduled in last cluster because there will
         * be less no. of clusters than we expected.
         */
        if (lstPriority_01.size() > 0) { //if (lstPriority_01.size() > 0 || zb_lstPriority_01.size() > 0)

            Collections.shuffle(lstPriority_01);
            Collections.shuffle(lstPriority_02);
            Collections.shuffle(lstPriority_03);
            Collections.shuffle(zb_lstPriority_01);
            Collections.shuffle(zb_lstPriority_02);
            Collections.shuffle(zb_lstPriority_03);
            //set zero budget list
            zbLists.clear();
            if(isFirstHalf()) {
                zbLists.add(zb_lstPriority_00);
            } else {
                zbLists.add(zb_lstPriority_lh);
            }
            zbLists.add(zb_lstPriority_00);
            zbLists.add(zb_lstPriority_01);
            if(isFirstHalf()) {
                zbLists.add(zb_lstPriority_lh);
            }
            zbLists.add(zb_lstPriority_02);
            zbLists.add(zb_lstPriority_03);

            //int iValidClusterCount = (i_ClusterCount <= 2) ? i_ClusterCount : i_ClusterCount - 1;
            //int eligibleClusterCount = iValidClusterCount - nextClusterIndex;
            int iValidClusterCount =  i_ClusterCount - nextClusterIndex;
           /* if(iValidClusterCount == nextClusterIndex){
                eligibleClusterCount = 1;
            }*/
            int cluster_advert_count = ((lstPriority_01.size() + lst_Schedule.get(0).size() - ar_FillerCount[0] - zaPLCount[0] + 1) / iValidClusterCount);
            LOGGER.debug("Schedules for a cluster : {}", cluster_advert_count);

            for (int i = 0; i < i_ClusterCount; i++) {
                if(nextClusterIndex > i){
                    continue;
                }
                ArrayList<PlayList> lst_cluster = lst_Schedule.get(i);
                int tmp_advert_count = lst_cluster.size() - ar_FillerCount[i] - zaPLCount[i];
                PlayList previous_schedule = null;

                if (lst_cluster.size() > 0) {
                    PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
                    if(tempPlayList != null && tempPlayList.getStatus() != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                        previous_schedule = tempPlayList;
                    }
//                    if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                        previous_schedule = tempPlayList;
//                    }
                }
                if (i == (i_ClusterCount - 1)) { // scheduleing last cluster
                    cluster_advert_count = tmp_advert_count + lstPriority_01.size();
                }
                //remove zero budget ads
                while ((tmp_advert_count <= cluster_advert_count) && lstPriority_01.size() > 0) {
                    PlayList tmp_item = null;
                    if (previous_schedule == null) { //first_item
                        tmp_item = lstPriority_01.get(0);
                        lstPriority_01.remove(0);
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    } else if ((null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_01, previous_schedule)))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_01.remove(tmp_item);
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    } else if (null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_02, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_02.remove(tmp_item);
                        updateConsumedDuration(tmp_item);
                    } else if (null != (tmp_item = this.findMatching03PlayList(lst_cluster, lstPriority_03, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_03.remove(tmp_item);
                        updateConsumedDuration(tmp_item);
                    } else if(i == 0 && zb_lstPriority_00.isEmpty() && null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, i, zb_lstPriority_00))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807
                        for (int j=0; j < zbLists.size(); j++){ //added this loop for remove zero budget item
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        // End
                        zbPLCount[i]++;
                        updateConsumedDuration(tmp_item);
                    } else if(null != (tmp_item = getMatchedZeroBudgetPlayList(lst_cluster, i, zbLists))){
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        zbPLCount[i]++;
                        updateConsumedDuration(tmp_item);
                    } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule))){
                        previous_schedule = tmp_item;
                        lst_cluster.add(tmp_item);
                        ar_FillerCount[i]++;
                    } else {
                        tmp_item = getZeroAdvertisement(lst_cluster, i);
                        if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                            tmp_item = getNewPlayListObject(previous_schedule);
                        }
                        previous_schedule = tmp_item;
                        if(tmp_item == null){
                            continue;
                        }
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        conZeroAds.add(tmp_item.getPlaylistId());
                        zaPLCount[i]++;
                    }
                }
            }
        }
        //status: lstPriority_00.size() = 0, lstPriority_01.size() = 0
        //If there is remaining lstPriority_01 for play list regenerate request, they should be moved to last cluster
        if(lstPriority_01.size() > 0 && isFirstHalf() || lstPriority_lh.size() == 0){
            lstPriority_lh.addAll(lstPriority_01);
            lstPriority_01.clear();
        }

        if(i_ClusterCount == 1 ) {
            if(isFirstHalf()){
                lstPriority_last.addAll(lstPriority_lh);
            } else {
                zb_lstPriority_lh.addAll(lstPriority_lh);
            }
            lstPriority_lh.clear();
        }

        //priority list lh ===========================================================================================
        if (lstPriority_lh.size() > 0) { //   if (lstPriority_lh.size() > 0 || zb_lstPriority_lh.size() > 0)

            Collections.shuffle(lstPriority_lh);
            Collections.shuffle(lstPriority_02);
            Collections.shuffle(lstPriority_03);
            Collections.shuffle(zb_lstPriority_lh);
            Collections.shuffle(zb_lstPriority_01);
            Collections.shuffle(zb_lstPriority_02);
            Collections.shuffle(zb_lstPriority_03);
            //set zero budget list
            zbLists.clear();
            zbLists.add(zb_lstPriority_lh);
            zbLists.add(zb_lstPriority_01);
            zbLists.add(zb_lstPriority_02);
            zbLists.add(zb_lstPriority_03);

            ArrayList<PlayList> lst_cluster = lst_Schedule.get(i_ClusterCount - 1);

            PlayList previous_schedule = null;
            if (lst_cluster.size() > 0) {
                PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
//                if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                    previous_schedule = tempPlayList;
//                }
                if(tempPlayList != null && tempPlayList.getStatus() != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                    previous_schedule = tempPlayList;
                }
            }
            while (lstPriority_lh.size() > 0) {
                PlayList tmp_item = null;
                if (previous_schedule == null) {//first_item
                    tmp_item = lstPriority_lh.get(0);
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_lh.remove(0);
                    updateConsumedDuration(tmp_item);
                } else if ((null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_lh, previous_schedule)))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_lh.remove(tmp_item);
                    updateConsumedDuration(tmp_item);
                } else if ((null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_01, previous_schedule)))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_01.remove(tmp_item);
                    updateConsumedDuration(tmp_item);
                } else if (null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_02, previous_schedule))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_02.remove(tmp_item);
                    updateConsumedDuration(tmp_item);
                } else if (null != (tmp_item = findMatching03PlayList(lst_cluster, lstPriority_03, previous_schedule) )) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    lstPriority_03.remove(tmp_item);
                    updateConsumedDuration(tmp_item);
                } else if(null != (tmp_item = getMatchedZeroBudgetPlayList(lst_cluster, i_ClusterCount - 1, zbLists))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    // For Jira issue NRM-807 - 21/10/2021
                    for (int j=0; j < zbLists.size(); j++){
                        if (zbLists.get(j).size() != 0) {
                            zbLists.get(j).remove(tmp_item);
                        }
                    }
                    //End
                    zbPLCount[i_ClusterCount - 1]++;
                    updateConsumedDuration(tmp_item);
                } else if(previous_schedule.getPlaylistId() == null && (null != (tmp_item = getZeroAdvertisement(lst_cluster, i_ClusterCount - 1)))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[i_ClusterCount - 1]++;
                } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule))){
                    previous_schedule = tmp_item;
                    tmp_item.setScheduleStartTime(previous_schedule.getScheduleStartTime());
                    tmp_item.setActualStartTime(previous_schedule.getActualStartTime());
                    lst_cluster.add(tmp_item);
                    ar_FillerCount[i_ClusterCount - 1]++;
                } else {
                    tmp_item = getZeroAdvertisement(lst_cluster, i_ClusterCount - 1);
                    if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                        tmp_item = getNewPlayListObject(previous_schedule);
                    }
                    previous_schedule = tmp_item;
                    if(tmp_item == null){
                        continue;
                    }
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[i_ClusterCount - 1]++;
                }
                if(lstPriority_lh.size() == 0 && lstPriority_01.size() > 0) {
                    lstPriority_lh.addAll(lstPriority_01);
                    lstPriority_01.clear();
                }
            }
        }
        //status: lstPriority_00.size() = 0, lstPriority_01.size() = 0, lstPriority_1h.size() = 0

        if(!lstPriority_last.isEmpty() && nextClusterIndex == 0){
            if(!isFirstHalf()) {
                lstPriority_03.addAll(lstPriority_last);
            } else {
                zb_lstPriority_lh.addAll(lstPriority_last);
            }
            lstPriority_last.clear();
        }
        /*if(!zb_lstPriority_last.isEmpty() && nextClusterIndex == 0){
            zb_lstPriority_03.addAll(zb_lstPriority_last);
            zb_lstPriority_last.clear();
        }*/

        //rest of paid adverts ===========================================================================================
        if (lstPriority_02.size() > 0 || lstPriority_03.size() > 0 ) {
            Collections.shuffle(lstPriority_02);
            Collections.shuffle(lstPriority_03);
            if(isFirstHalf()) {
                zbLists.add(zb_lstPriority_00);
            } else {
                zbLists.add(zb_lstPriority_lh);
            }
            Collections.shuffle(zb_lstPriority_01);
            if(isFirstHalf()) {
                zbLists.add(zb_lstPriority_lh);
            }
            Collections.shuffle(zb_lstPriority_02);
            Collections.shuffle(zb_lstPriority_03);
            //set zero budget list
            zbLists.clear();
            zbLists.add(zb_lstPriority_01);
            zbLists.add(zb_lstPriority_02);
            zbLists.add(zb_lstPriority_03);

            int total_scheduled = 0;
            for (int i = 0; i < i_ClusterCount; i++) {
                total_scheduled += lst_Schedule.get(i).size();
                total_scheduled -= ar_FillerCount[i];
                total_scheduled -= zaPLCount[i];
            }
            int cluster_advert_count = ((zb_lstPriority_00.size() + zb_lstPriority_01.size() + zb_lstPriority_02.size() + zb_lstPriority_03.size() + zb_lstPriority_lh.size() + zb_lstPriority_last.size() + lstPriority_02.size() + lstPriority_03.size() + total_scheduled + 1) / (i_ClusterCount - nextClusterIndex));

            for (int i = 0; i < i_ClusterCount; i++) {
                if(nextClusterIndex > i){
                    continue;
                }
                ArrayList<PlayList> lst_cluster = lst_Schedule.get(i);
                int tmp_advert_count = lst_cluster.size() - ar_FillerCount[i] -  zaPLCount[i];
                PlayList previous_schedule = null;

                if (lst_cluster.size() > 0) {
                    PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
//                    if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                        previous_schedule = tempPlayList;
//                    }
                    if(tempPlayList != null && tempPlayList.getStatus() != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                        previous_schedule = tempPlayList;
                    }
                }
                boolean isLastCluster = i == (i_ClusterCount - 1);
                if (isLastCluster) {
                    cluster_advert_count = tmp_advert_count + lstPriority_02.size() + lstPriority_03.size() + 1;
                }

                while (tmp_advert_count < cluster_advert_count && (lstPriority_02.size() > 0 || lstPriority_03.size() > 0 )) {
                    PlayList tmp_item = null;
                    //first_item
                    if (previous_schedule == null) {
                        if (lstPriority_02.size() > 0) {
                            tmp_item = lstPriority_02.get(0);
                            lstPriority_02.remove(0);
                        } else if (lstPriority_03.size() > 0) {
                            tmp_item = findMatching03PlayList(lst_cluster, lstPriority_03, null);//first item priority 03
                            lstPriority_03.remove(tmp_item);
                        }
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    } if (null != (tmp_item = this.getMatchingSchedule(lst_cluster, lstPriority_02, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_02.remove(tmp_item);
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    } else if (null != (tmp_item = findMatching03PlayList(lst_cluster, lstPriority_03, previous_schedule))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        lstPriority_03.remove(tmp_item);
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    }  else if(i == 0 && zb_lstPriority_00.isEmpty() && null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, i, zb_lstPriority_00))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807 - 21/10/2021
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        zbPLCount[i]++;
                        updateConsumedDuration(tmp_item);
                    }  else if(isLastCluster && zb_lstPriority_00.isEmpty() && null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, i, zb_lstPriority_lh))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        zbPLCount[i]++;
                        updateConsumedDuration(tmp_item);
                    } else if(null != (tmp_item = getMatchedZeroBudgetPlayList(lst_cluster, i, zbLists))){
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        zbPLCount[i]++;
                        updateConsumedDuration(tmp_item);
                    } else if(previous_schedule.getPlaylistId() == null && (null != (tmp_item = getZeroAdvertisement(lst_cluster, i)))){
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        conZeroAds.add(tmp_item.getPlaylistId());
                    } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule))){
                        tmp_item.setScheduleStartTime(previous_schedule.getScheduleStartTime());
                        tmp_item.setActualStartTime(previous_schedule.getActualStartTime());
                        previous_schedule = tmp_item;
                        lst_cluster.add(tmp_item);
                        ar_FillerCount[i]++;
                    } else {
                        tmp_item = getZeroAdvertisement(lst_cluster, i);
                        if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                            tmp_item = getNewPlayListObject(previous_schedule);
                        }
                        previous_schedule = tmp_item;
                        if(tmp_item == null){
                            continue;
                        }
                        lst_cluster.add(tmp_item);
                        conZeroAds.add(tmp_item.getPlaylistId());
                    }
                }
            }
        }
        //status: lstPriority_00.size() = 0, lstPriority_1h.size() = 0, lstPriority_01.size() = 0, lstPriority_02.size() = 0, lstPriority_03.size() = 0

        //Start to schedule remaining zero budget advertisements
        if(isFirstHalf() && !zb_lstPriority_01.isEmpty() && !zb_lstPriority_lh.isEmpty() && nextClusterIndex == 1 && i_ClusterCount == 2) {
            zb_lstPriority_00.addAll(zb_lstPriority_01);
            zb_lstPriority_01.clear();
        }

        if(!isFirstHalf() && zb_lstPriority_lh.size() > 0) {
            zb_lstPriority_00.addAll(zb_lstPriority_lh);
            zb_lstPriority_lh.clear();
        }
        //remaining zero budget adverts of the first cluster should be scheduled at the end of first cluster
        if(zb_lstPriority_00.size() > 0){
            int advertCount = zb_lstPriority_00.size();
            if(!isFirstHalf() && i_ClusterCount > nextClusterIndex) {
                advertCount =  zb_lstPriority_00.size() / (i_ClusterCount - nextClusterIndex);
            }
            zbLists.clear();
            Collections.shuffle(zb_lstPriority_00);
            Collections.shuffle(zb_lstPriority_01);
            Collections.shuffle(zb_lstPriority_02);
            Collections.shuffle(zb_lstPriority_03);
            zbLists.add(zb_lstPriority_01);
            zbLists.add(zb_lstPriority_02);
            zbLists.add(zb_lstPriority_03);
            ArrayList<PlayList> lst_cluster = lst_Schedule.get(nextClusterIndex);
            int cIndex = 0;
            if(isFirstHalf() && nextClusterIndex == 1 && i_ClusterCount == 2) {
                lst_cluster = lst_Schedule.get(1);
                cIndex = 1;
            }
            PlayList previous_schedule = null;
            if (lst_cluster.size() > 0) {
                PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
                if(tempPlayList != null && tempPlayList.getStatus() !=null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                    previous_schedule = tempPlayList;
                }
//                if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                    previous_schedule = tempPlayList;
//                }
            }
            while (advertCount > 0) {
                PlayList tmp_item = null;
                if (previous_schedule == null) {//first_item
                    tmp_item = zb_lstPriority_00.get(0);
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    zb_lstPriority_00.remove(0);
                    advertCount--;
                    updateConsumedDuration(tmp_item);
                }  else if(null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, cIndex, zb_lstPriority_00))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    // For Jira issue NRM-807 - 21/10/2021
                    for (int j=0; j < zbLists.size(); j++){
                        if (zbLists.get(j).size() != 0) {
                            zbLists.get(j).remove(tmp_item);
                        }
                    }
                    //End
                    zbPLCount[cIndex]++;
                    advertCount--;
                    updateConsumedDuration(tmp_item);
                } else if(null != (tmp_item = getMatchedZeroBudgetPlayList(lst_cluster, cIndex, zbLists))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    // For Jira issue NRM-807 - 21/10/2021
                    for (int j=0; j < zbLists.size(); j++){
                        if (zbLists.get(j).size() != 0) {
                            zbLists.get(j).remove(tmp_item);
                        }
                    }
                    //End
                    zbPLCount[cIndex]++;
                    updateConsumedDuration(tmp_item);
                } else if(previous_schedule.getPlaylistId() == null && (null != (tmp_item = getZeroAdvertisement(lst_cluster, cIndex)))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[cIndex]++;
                } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule))) {
                    tmp_item.setScheduleEndTime(previous_schedule.getScheduleEndTime());
                    tmp_item.setActualEndTime(previous_schedule.getActualEndTime());
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    ar_FillerCount[cIndex]++;
                } else {
                    tmp_item = getZeroAdvertisement(lst_cluster, cIndex);
                    if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                        tmp_item = getNewPlayListObject(previous_schedule);
                    }
                    previous_schedule = tmp_item;
                    if(tmp_item == null){
                        continue;
                    }
                    lst_cluster.add(tmp_item);
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[cIndex]++;
                }
            }
        }
        //status: lstPriority_00.size() = 0, lstPriority_1h.size() = 0, lstPriority_01.size() = 0, lstPriority_02.size() = 0, lstPriority_03.size() = 0
        //status zb = zb_lstPriority_00.size() = 0
        if(zb_lstPriority_00.size() > 0) {
            zb_lstPriority_01.addAll(zb_lstPriority_00);
            zb_lstPriority_00.clear();
        }
        if (zb_lstPriority_01.size() > 0 && isFirstHalf()) {

            Collections.shuffle(zb_lstPriority_01);
            Collections.shuffle(zb_lstPriority_02);
            Collections.shuffle(zb_lstPriority_03);
            zbLists.clear();
            zbLists.add(zb_lstPriority_02);
            zbLists.add(zb_lstPriority_03);

            int iValidClusterCount = (i_ClusterCount <= 2) ? i_ClusterCount : i_ClusterCount - 1;
            int eligibleClusterCount = iValidClusterCount - nextClusterIndex;
            if(iValidClusterCount == nextClusterIndex){
                eligibleClusterCount = 1;
            }
            int total_scheduled = 0;
            for (int i = 0; i < i_ClusterCount; i++) {
                total_scheduled += lst_Schedule.get(i).size();
                total_scheduled -= ar_FillerCount[i];
                total_scheduled -= zaPLCount[i];
            }
            int cluster_advert_count = ((zb_lstPriority_01.size() + total_scheduled - ar_FillerCount[0] - zaPLCount[0] + 1) * 75) / (100 * eligibleClusterCount);
            LOGGER.debug("Schedules for a cluster : {}", cluster_advert_count);

            for (int i = 0; i <= iValidClusterCount; i++) {
                if(nextClusterIndex > i || lst_Schedule.size() == 0 || lst_Schedule.size() <= i ){
                    continue;
                }
                ArrayList<PlayList> lst_cluster = lst_Schedule.get(i);
                int tmp_advert_count = lst_cluster.size() - ar_FillerCount[i] - zaPLCount[i];
                PlayList previous_schedule = null;

                if (lst_cluster.size() > 0) {
                    PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
//                    if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                        previous_schedule = tempPlayList;
//                    }
                    if(tempPlayList != null && tempPlayList.getStatus() != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                        previous_schedule = tempPlayList;
                    }
                }
                if (i == iValidClusterCount - 1) { // scheduleing last cluster
                    cluster_advert_count = tmp_advert_count + zb_lstPriority_01.size();
                }
                //remove zero budget ads
                while ((tmp_advert_count < cluster_advert_count) && zb_lstPriority_01.size() > 0) {
                    PlayList tmp_item = null;
                    if (previous_schedule == null) { //first_item
                        tmp_item = zb_lstPriority_01.get(0);
                        zb_lstPriority_01.remove(0);
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    } else if(null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, i, zb_lstPriority_01))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807 - 21/10/2021
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        zbPLCount[0]++;
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    } else if(null != (tmp_item = getMatchedZeroBudgetPlayList(lst_cluster, i, zbLists))){
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        zbPLCount[0]++;
                        updateConsumedDuration(tmp_item);
                    } else if(previous_schedule.getPlaylistId() == null && (null != (tmp_item = getZeroAdvertisement(lst_cluster, i)))){
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        conZeroAds.add(tmp_item.getPlaylistId());
                        zaPLCount[i]++;
                    } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule))){
                        tmp_item.setScheduleStartTime(previous_schedule.getScheduleStartTime());
                        tmp_item.setActualStartTime(previous_schedule.getActualStartTime());
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        ar_FillerCount[i]++;
                    } else {
                        tmp_item = getZeroAdvertisement(lst_cluster, i);
                        if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                            tmp_item = getNewPlayListObject(previous_schedule);
                        }
                        previous_schedule = tmp_item;
                        if(tmp_item == null){
                            continue;
                        }
                        lst_cluster.add(tmp_item);
                        conZeroAds.add(tmp_item.getPlaylistId());
                        zaPLCount[i]++;
                    }
                }
            }
        }
        //status: lstPriority_00.size() = 0, lstPriority_1h.size() = 0, lstPriority_01.size() = 0, lstPriority_02.size() = 0, lstPriority_03.size() = 0
        //status zb = zb_lstPriority_00.size() = 0, zb_lstPriority_01 = 0
        //If there is remaining zb_lstPriority_01 for play list regenerate request, they should be moved to last cluster
        if(zb_lstPriority_01.size() > 0 && isFirstHalf() || zb_lstPriority_lh.size() == 0){
            zb_lstPriority_lh.addAll(zb_lstPriority_01);
            zb_lstPriority_01.clear();
        }

        if (zb_lstPriority_lh.size() > 0) { //   if (lstPriority_lh.size() > 0 || zb_lstPriority_lh.size() > 0)

            Collections.shuffle(zb_lstPriority_lh);
            Collections.shuffle(zb_lstPriority_02);
            Collections.shuffle(zb_lstPriority_03);
            //set zero budget list
            zbLists.clear();
            zbLists.add(zb_lstPriority_02);
            zbLists.add(zb_lstPriority_03);
            int clusterIndex = i_ClusterCount - 1;
            ArrayList<PlayList> lst_cluster = lst_Schedule.get(clusterIndex);
            PlayList previous_schedule = null;
            if (lst_cluster.size() > 0) {
                PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
//                if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                    previous_schedule = tempPlayList;
//                }
                if(tempPlayList != null && tempPlayList.getStatus() != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                    previous_schedule = tempPlayList;
                }
            }
            while (zb_lstPriority_lh.size() > 0) {
                PlayList tmp_item = null;
                if (previous_schedule == null) {//first_item
                    tmp_item = zb_lstPriority_lh.get(0);
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    zb_lstPriority_lh.remove(0);
                    updateConsumedDuration(tmp_item);
                } else if(null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, clusterIndex, zb_lstPriority_lh))) {
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    // For Jira issue NRM-807 - 21/10/2021
                    for (int j=0; j < zbLists.size(); j++){
                        if (zbLists.get(j).size() != 0) {
                            zbLists.get(j).remove(tmp_item);
                        }
                    }
                    //End
                    zbPLCount[clusterIndex]++;
                    updateConsumedDuration(tmp_item);
                } else if(null != (tmp_item = getMatchedZeroBudgetPlayList(lst_cluster, clusterIndex, zbLists))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    // For Jira issue NRM-807
                    for (int j=0; j < zbLists.size(); j++){
                        if (zbLists.get(j).size() != 0) {
                            zbLists.get(j).remove(tmp_item);
                        }
                    }
                    //End
                    zbPLCount[clusterIndex]++;
                    updateConsumedDuration(tmp_item);
                } else if(previous_schedule.getPlaylistId() == null && (null != (tmp_item = getZeroAdvertisement(lst_cluster, clusterIndex)))){
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[clusterIndex]++;
                } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule))){
                    tmp_item.setScheduleStartTime(previous_schedule.getScheduleStartTime());
                    tmp_item.setActualStartTime(previous_schedule.getActualStartTime());
                    lst_cluster.add(tmp_item);
                    previous_schedule = tmp_item;
                    ar_FillerCount[i_ClusterCount - 1]++;
                }  else {
                    tmp_item = getZeroAdvertisement(lst_cluster, clusterIndex);
                    if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                        tmp_item = getNewPlayListObject(previous_schedule);
                    }
                    previous_schedule = tmp_item;
                    if(tmp_item == null){
                        continue;
                    }
                    lst_cluster.add(tmp_item);
                    conZeroAds.add(tmp_item.getPlaylistId());
                    zaPLCount[clusterIndex]++;
                }

                if(zb_lstPriority_lh.size() == 0 && zb_lstPriority_01.size() > 0) {
                    zb_lstPriority_lh.addAll(zb_lstPriority_01);
                    zb_lstPriority_01.clear();
                }
            }
        }
        //status: lstPriority_00.size() = 0, lstPriority_1h.size() = 0, lstPriority_01.size() = 0, lstPriority_02.size() = 0, lstPriority_03.size() = 0
        //status zb = zb_lstPriority_00.size() = 0, zb_lstPriority_01.size() = 0, zb_lstPriority_lh.size() = 0

        if(!lstPriority_last.isEmpty() && !isFirstHalf()){
            zb_lstPriority_02.addAll(lstPriority_last);
            lstPriority_last.clear();
        }

        if(!zb_lstPriority_last.isEmpty() && !isFirstHalf()){
            zb_lstPriority_03.addAll(zb_lstPriority_last);
            zb_lstPriority_last.clear();
        }

        /*if(!zb_lstPriority_last.isEmpty()) {
            lstPriority_last.addAll(zb_lstPriority_last);
            zb_lstPriority_last.clear();
        }*/

        if(zb_lstPriority_02.isEmpty() && zb_lstPriority_03.isEmpty()) {
            zb_lstPriority_02.addAll(zb_lstPriority_last);
            zb_lstPriority_last.clear();
        }

        if(zb_lstPriority_02.isEmpty() && zb_lstPriority_03.isEmpty()) {
            zb_lstPriority_02.addAll(lstPriority_last);
            lstPriority_last.clear();
        }

        //rest of zero budget  adverts ===========================================================================================
        if (zb_lstPriority_02.size() > 0 || zb_lstPriority_03.size() > 0 ) {
            Collections.shuffle(zb_lstPriority_02);
            Collections.shuffle(zb_lstPriority_03);

            int total_scheduled = 0;
            for (int i = 0; i < i_ClusterCount; i++) {
                total_scheduled += lst_Schedule.get(i).size();
                total_scheduled -= ar_FillerCount[i];
                total_scheduled -= zaPLCount[i];
            }
            int cluster_advert_count = ((zb_lstPriority_02.size() + zb_lstPriority_03.size() + lstPriority_last.size() + total_scheduled + 1) / (i_ClusterCount - nextClusterIndex));

            for (int i = 0; i < i_ClusterCount; i++) {
                if(nextClusterIndex > i){
                    continue;
                }
                ArrayList<PlayList> lst_cluster = lst_Schedule.get(i);
                int tmp_advert_count = lst_cluster.size() - ar_FillerCount[i] -  zaPLCount[i];
                PlayList previous_schedule = null;

                if (lst_cluster.size() > 0) {
                    PlayList tempPlayList = lst_cluster.get(lst_cluster.size() - 1);
//                    if(tempPlayList != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
//                        previous_schedule = tempPlayList;
//                    }
                    if(tempPlayList != null && tempPlayList.getStatus() != null && !tempPlayList.getStatus().equalsIgnoreCase("2" )) {
                        previous_schedule = tempPlayList;
                    }
                }
                boolean isLastCluster = i == (i_ClusterCount - 1);
                if (isLastCluster) {
                    cluster_advert_count = tmp_advert_count + zb_lstPriority_02.size() + zb_lstPriority_03.size() + 1;
                }

                if(zb_lstPriority_02.isEmpty() && zb_lstPriority_03.isEmpty() && !lstPriority_last.isEmpty()) {
                    zb_lstPriority_02.addAll(lstPriority_last);
                    lstPriority_last.clear();
                }

                while (zb_lstPriority_02.size() > 0 || zb_lstPriority_03.size() > 0) {
                    PlayList tmp_item = null;
                    //first_item
                    if (previous_schedule == null) {
                        if (zb_lstPriority_02.size() > 0) {
                            tmp_item = zb_lstPriority_02.get(0);
                            zb_lstPriority_02.remove(0);
                        } else if (zb_lstPriority_03.size() > 0) {
                            tmp_item = findMatching03PlayList(lst_cluster, zb_lstPriority_03, previous_schedule);
                            zb_lstPriority_03.remove(tmp_item);
                        }
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        tmp_advert_count++;
                        updateConsumedDuration(tmp_item);
                    }  else if(null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, i, zb_lstPriority_02))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807 - 21/10/2021
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        tmp_advert_count++;
                        zbPLCount[i]++;
                        updateConsumedDuration(tmp_item);
                    }  else if(null != (tmp_item = findMatchedZeroBudgetPlayList(lst_cluster, i, zb_lstPriority_03))) {
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        // For Jira issue NRM-807 - 21/10/2021
                        for (int j=0; j < zbLists.size(); j++){
                            if (zbLists.get(j).size() != 0) {
                                zbLists.get(j).remove(tmp_item);
                            }
                        }
                        //End
                        tmp_advert_count++;
                        zbPLCount[i]++;
                        updateConsumedDuration(tmp_item);
                    } else if(previous_schedule.getPlaylistId() == null && (null != (tmp_item = getZeroAdvertisement(lst_cluster, i)))){
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        conZeroAds.add(tmp_item.getPlaylistId());
                    } else if(previous_schedule.getLable() != PlayList.Lable.FILLER && null != (tmp_item = getNewPlayListObject(previous_schedule))) {
                        tmp_item.setScheduleStartTime(previous_schedule.getScheduleStartTime());
                        tmp_item.setActualStartTime(previous_schedule.getActualStartTime());
                        lst_cluster.add(tmp_item);
                        previous_schedule = tmp_item;
                        ar_FillerCount[i]++;
                    } else {
                        tmp_item = getZeroAdvertisement(lst_cluster, i);
                        if(tmp_item == null) { //when zero ads feature disable channel, filers not in use to gap filling
                            tmp_item = getNewPlayListObject(previous_schedule);
                        }
                        previous_schedule = tmp_item;
                        if(tmp_item == null){
                            continue;
                        }
                        lst_cluster.add(tmp_item);
                        conZeroAds.add(tmp_item.getPlaylistId());
                    }
                    if(zb_lstPriority_02.isEmpty() && zb_lstPriority_03.isEmpty()) {
                        zb_lstPriority_02.addAll(lstPriority_last);
                        zb_lstPriority_02.addAll(zb_lstPriority_last);
                        lstPriority_last.clear();
                        zb_lstPriority_last.clear();
                    }
                }
            }
        }
        //status: lstPriority_00.size() = 0, lstPriority_1h.size() = 0, lstPriority_01.size() = 0, lstPriority_02.size() = 0, lstPriority_03.size() = 0
        //status zb = zb_lstPriority_00.size() = 0, zb_lstPriority_01.size() = 0, zb_lstPriority_lh.size() = 0, zb_lstPriority_02.size() = 0, zb_lstPriority_03.size() = 0
        //End of scheduling remaining zero budget advertisements

        //clear not used zero ads
        for(int gid : genZeroAds){
            boolean isContain = false;
            for (int cid : conZeroAds){
                if(gid == cid){
                    isContain = true;
                    break;
                }
            }
            if(!isContain){
                PlayList playList = playListService.getSelectedPlayList(gid);
                if(!playList.getStatus().equalsIgnoreCase("2")) {
                    playListService.deletePlayList(gid);
                }
            }
        }
        return true;
    }

    private PlayList findMatching03PlayList(List<PlayList> currentCluster, List<PlayList> cluster03, PlayList previousSchedule) {
        Collections.sort(cluster03, new Comparator<PlayList>() {
            @Override
            public int compare(PlayList o1, PlayList o2) {
//                return o1.getScheduleEndTime().getTime() > o2.getScheduleEndTime().getTime() ? 1: -1;
                // For Jira issue NRM-803
                if (o1.getScheduleEndTime().getTime() > o2.getScheduleEndTime().getTime()){
                    return 1;
                }
                else if (o1.getScheduleEndTime().getTime() == o2.getScheduleEndTime().getTime()){
                    return 0;
                }
                else {
                    return -1;
                }
                // End
            }
        });

        if(!cluster03.isEmpty()) {
            if(currentCluster.size() == 0) {
                PlayList item = cluster03.get(0);
                item.setC3(true);
                return item;
            }
            boolean hasC3Advert = false;
            for(PlayList playList: currentCluster) {
                if(playList.isC3()) {
                    hasC3Advert = true;
                    break;
                }
            }
            if(!hasC3Advert) {
                for (PlayList item : cluster03) {
                    if (!this.isSchedulesConflicting(previousSchedule, item) && isValidLapDuration(currentCluster, item)) {
                        item.setC3(true);
                        return item;
                    }
                }
            } else {
                Collections.shuffle(cluster03);
                PlayList item = getMatchingSchedule(currentCluster, cluster03, previousSchedule);
//                item.setC3(true);
//                return item;
                if(item != null){
                    item.setC3(true);
                    return item;
                }
            }
        }
        return null;
    }

    private boolean isFirstHalf() {
        Date currentTime = new Date();
        int elapsedTime = (currentTime.getMinutes() * 60) + currentTime.getSeconds();
        return elapsedTime < 30 * 60;
    }

    private PlayList findMatchedZeroBudgetPlayList(List<PlayList> cluster, int j, List<PlayList> zbCluster){
        PlayList playList = getZeroBudgetPlayList(cluster, j, zbCluster);
        if(playList != null){
            return playList;
        }
        return null;
    }

    private PlayList getMatchedZeroBudgetPlayList(List<PlayList> cluster, int j,  List<List<PlayList>> zbClusters){
        int zbClusterIndex = 0;
        for(List<PlayList> zbCluster : zbClusters){
            if(zbClusterIndex == zbClusters.size() - 1) {
                PlayList previousFiller = !cluster.isEmpty() ? cluster.get(cluster.size() - 1) : null;
                return findMatching03PlayList(cluster, zbCluster, previousFiller);
            }
            PlayList playList = getZeroBudgetPlayList(cluster, j, zbCluster);
            if(playList != null){
                return playList;
            }
            zbClusterIndex++;
        }
        return null;
    }

    private List<PlayList> filterZeroBudgetPlayLists(List<PlayList> playLists){
        List<PlayList> zeroBudgetList = new ArrayList<>();
        Iterator<PlayList> iterator = playLists.iterator();
        while (iterator.hasNext()){
            PlayList pl = iterator.next();
            if(isZeroBudgetPlayList(pl)){
                zeroBudgetList.add(pl);
                iterator.remove();
            }
        }
        return zeroBudgetList;
    }

    private PlayList getNewPlayListObject(PlayList previous) {
        PlayList pTmpFiller = getNewPlayListObject();
        if(pTmpFiller == null){
            return null;
        }
        pTmpFiller.setScheduleEndTime(this.dt_EndTime);
        pTmpFiller.setScheduleStartTime(previous.getTimeBeltStartTime());
        pTmpFiller.setTimeBeltEndTime(previous.getTimeBeltEndTime());
        pTmpFiller.setTimeBeltStartTime(previous.getTimeBeltStartTime());

        return pTmpFiller;
    }

    //filler adverts
    private PlayList getNewPlayListObject() {
        if (lst_Fillers.size() <= 0) {
            return null;
        }

        Collections.shuffle(lst_Fillers);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

        int advertIndex = 0;//= new Random().nextInt(lst_Fillers.size());
        for (int i = 0; i < lst_Fillers.size(); i++) {
            if (lst_Fillers.get(i).getAdvertid() != i_previousFillerId) {
                i_previousFillerId = lst_Fillers.get(i).getAdvertid();
                advertIndex = i;
                break;
            }
        }

        PlayList playList = new PlayList();
        playList.setScheduleEndTime(this.dt_EndTime);
        playList.setScheduleStartTime(this.dt_StartTime);
        playList.setTimeBeltEndTime(this.dt_EndTime);
        playList.setTimeBeltStartTime(this.dt_StartTime);
        playList.setActualEndTime(this.dt_EndTime);
        playList.setActualStartTime(this.dt_StartTime);
        playList.setScheduleHour(this.i_hour);
        playList.setChannel(new ChannelDetails(this.i_ChannelId));
        Advertisement fAdvertisement = new Advertisement(lst_Fillers.get(advertIndex).getAdvertid());
        fAdvertisement.setDuration(lst_Fillers.get(advertIndex).getDuration());
        playList.setAdvert(fAdvertisement);
        playList.setSchedule(new ScheduleDef(1));
        playList.setWorkOrder(new WorkOrder(1));
        try {
            playList.setDate(format.parse(default_format.format(new Date())));
        } catch (ParseException ex) {
            Logger.getLogger(ScheduleGenerateV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        playList.setStatus("0");
        playList.setPlayCluster(-1);
        playList.setPlayOrder(-1);
        playList.setRetryCount(0);
        playList.setComment("Filler");
        playList.setLable(PlayList.Lable.FILLER);

        return playList;
    }

    public ArrayList<ArrayList<PlayList>> getLst_Schedule() {
        for(ArrayList<PlayList> cluster: lst_Schedule) {
            Iterator<PlayList> iterator = cluster.iterator();
            while (iterator.hasNext()){
                if(iterator.next() == null){
                    iterator.remove();
                }
            }
        }
        return lst_Schedule;
    }

// Conflicts_Resolving ==========================================================================
    private boolean setSchedulesConflicts(List<PlayList> lstPlayList) {
        for (PlayList item_1 : lstPlayList) {
            for (PlayList item_2 : lstPlayList) {
                if (this.resolve_SameClientConflicts(item_1, item_2)) {
                    item_1.addConflicting_schedules(item_2.getPlaylistId());
                    LOGGER.debug("[SameClientConflicts] => id :{} <-> {} => Client : {} <-> {} "
                            ,item_1.getPlaylistId(), item_2.getPlaylistId(), item_1.getAdvert().getClient(), item_2.getAdvert().getClient());
                }
                else if (this.resolve_SimilarAdvertConflicts(item_1, item_2)) {
                    item_1.addConflicting_schedules(item_2.getPlaylistId());
                    LOGGER.debug("[SimilarConflicts] => id :{} <-> {} => AdvertName : {} <-> {}"
                            ,item_1.getPlaylistId(), item_2.getPlaylistId(), item_1.getAdvert().getAdvertname(), item_2.getAdvert().getAdvertname());
                }
                else if (this.resolve_CategoryConflicts(item_1, item_2)) {
                    item_1.addConflicting_schedules(item_2.getPlaylistId());
                    LOGGER.debug("[CategoryConflicts] => id :{} <-> {} => Category : {} <-> {}"
                            ,item_1.getPlaylistId(), item_2.getPlaylistId(), item_1.getAdvert().getCommercialcategory(), item_2.getAdvert().getCommercialcategory());
                }
            }
        }
        return true;
    }

    private boolean resolve_SimilarAdvertConflicts(PlayList item_1, PlayList item_2) {
        if (item_1.getAdvert().getAdvertid() == item_2.getAdvert().getAdvertid()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean resolve_CategoryConflicts(PlayList item_1, PlayList item_2) {
        if ((item_1.getAdvert().getCommercialcategory().equals(item_2.getAdvert().getCommercialcategory()))) {
            //if (!((item_1.getAdvert().getClient().equals(item_2.getAdvert().getClient())))) {
            return true;
            //}
        }
        return false;
    }
    
    private boolean resolve_SameClientConflicts(PlayList item_1, PlayList item_2) {
        if (item_1.getAdvert().getClient().equals(item_2.getAdvert().getClient())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isSchedulesConflicting(PlayList playList_01, PlayList playList_02) {
        if (playList_01 != null) { //For Jira issue NRM-830
            for (int item : playList_01.getConflicting_schedules()) {
                if (item == playList_02.getPlaylistId()) {
                    return true;
                }
            }
        }
        return false;
    }

    private PlayList getMatchingSchedule(List<PlayList> lst_cluster, List<PlayList> lstPlayList, PlayList previous_schedule){

        for (PlayList item : lstPlayList) {
            if (!this.isSchedulesConflicting(previous_schedule, item) && isValidLapDuration(lst_cluster, item)) {
                return item;
            }
        }
        return null;
    }

    private boolean isValidLapDuration(List<PlayList> cluster, PlayList pl, List<PlayList> inputList){
        int minLapDuration = advertSettingService.findMinLapDuration(pl.getChannel().getChannelid(), pl.getAdvert().getDuration());
        int lapDuration = getLapDuration(cluster, pl);
        boolean validLapDuration = lapDuration != -1 ? (lapDuration >= minLapDuration) : true;
        if(!validLapDuration){
            inputList.remove(pl); //to shift checked playlist to end of the list
            inputList.add(pl);
        }
        return validLapDuration;
    }

    private boolean isValidLapDuration(List<PlayList> cluster, PlayList pl){
        int minLapDuration = advertSettingService.findMinLapDuration(pl.getChannel().getChannelid(), pl.getAdvert().getDuration());
        int lapDuration = getLapDuration(cluster, pl);
        return lapDuration != -1 ? (lapDuration >= minLapDuration) : true;
    }

    private int getLapDuration(List<PlayList> cluster, PlayList currentList){
        boolean hasMatchingAdvert = false;
        int lapDuration = 0;
        try {
            if(currentList.getPlaylistId() != null) {
                for (int i = cluster.size() - 1; i >= 0; i--) {
                    PlayList playList = cluster.get(i);
                    if (playList == null) {
                        continue;
                    }
                    if (playList.getAdvert().getAdvertid() == currentList.getAdvert().getAdvertid()) {
                        hasMatchingAdvert = true;
                        break;
                    } else {
                        lapDuration += playList.getAdvert().getDuration();
                    }
                }
            }
        }catch (Exception e){
            System.out.printf(e.getMessage());
        }

        return hasMatchingAdvert ? lapDuration : -1;
    }

    private boolean isSameAdvertPlayLists(List<PlayList> playLists){
        if(playLists.size() == 0){
            return false;
        }
        Advertisement advertisement = playLists.get(0).getAdvert();
        for(PlayList pl : playLists){
            if(pl.getAdvert().getAdvertid() != advertisement.getAdvertid()){
                return false;
            }
        }
        return true;
    }

    private PlayList getZeroBudgetPlayList(List<PlayList> cluster, int j, List<PlayList> zbCluster){
        if(zbCluster == null || zbCluster.size() == 0){
            return null;
        }

        PlayList la = null;  //last advertisement
        PlayList previousFiller = null;
        //find previous filler and previous advert
        if(cluster.size() > 0){
            PlayList pa = cluster.get(cluster.size() - 1);
            if(pa.getPlaylistId() == null){
                previousFiller = pa;
            }
            for(int i = cluster.size() - 1; i >= 0; i--){
                la = cluster.get(i);
                if(la.getPlaylistId() != null){
                    break;
                }
            }
        }

        PlayList result = null;
        boolean isFillerNeeded = true;

        Iterator<PlayList> iterator;
        if(previousFiller == null){
            iterator = zbCluster.iterator();
            while (iterator.hasNext()){
                PlayList zb = iterator.next();
                boolean noConflicts = !this.resolve_SameClientConflicts(la, zb) && !this.resolve_SimilarAdvertConflicts(la, zb)
                        && !this.resolve_CategoryConflicts(la, zb);
                if(isValidLapDuration(cluster, zb) && noConflicts){
                    result = zb;
                    isFillerNeeded = false;
                    iterator.remove();
                    break;
                }
            }
        }else {

            iterator = zbCluster.iterator();
            while (iterator.hasNext()){
                PlayList zb = iterator.next();
                boolean noConflicts = !this.resolve_SameClientConflicts(la, zb) && !this.resolve_SimilarAdvertConflicts(la, zb)
                        && !this.resolve_CategoryConflicts(la, zb);
                if(isValidLapDuration(cluster.subList(0, cluster.size() - 2 ), zb) && noConflicts){
                    result = zb;
                    isFillerNeeded = false;
                    iterator.remove();
                    break;
                }
            }

            if(result == null){
                iterator = zbCluster.iterator();
                while (iterator.hasNext()){
                    PlayList zb = iterator.next();
                    if(isValidLapDuration(cluster, zb)){
                        result = zb;
                        iterator.remove();
                        break;
                    }
                }
            }

        }

        if(result != null && !isFillerNeeded && previousFiller != null){
            cluster.remove(cluster.size() - 1);
            ar_FillerCount[j]--;
        }

        if(result != null && isFillerNeeded && previousFiller == null){
            cluster.add(getNewPlayListObject(la));
            ar_FillerCount[j]++;
        }
        return result;
    }

    private PlayList getZeroAdvertisement(List<PlayList> cluster, int j){
        PlayList la = null;  //last advertisement
        PlayList previousFiller = null;
        //find previous filler and previous advert
        if(cluster.size() > 0){
            PlayList pa = cluster.get(cluster.size() - 1);
            if(pa.getPlaylistId() == null){
                previousFiller = pa;
            }
            for(int i = cluster.size() - 1; i >= 0; i--){
                la = cluster.get(i);
                if(la.getPlaylistId() != null){
                    break;
                }
            }
        }

        PlayList za = getNextZeroAdvertisement();
        if(za == null){
            return null;
        }

        boolean isFillerNeeded = true;
        for(int i = 0; i < 5 ; i++){
            if(!this.resolve_SameClientConflicts(la, za) && !this.resolve_SimilarAdvertConflicts(la, za)
                    && !this.resolve_CategoryConflicts(la, za) && isValidLapDuration(cluster, za)){
                isFillerNeeded = false;
                break;
            }else {
                //playListService.deletePlayList(za.getPlaylistId());
            }
            za = getNextZeroAdvertisement(); //zero advertisement
        }

        if(!isFillerNeeded && previousFiller != null){
            cluster.remove(cluster.size() - 1);
            ar_FillerCount[j]--;
        }

        if(isFillerNeeded && previousFiller == null && za != null){
            PlayList filler = getNewPlayListObject(la);
            if(filler != null) {
                cluster.add(filler);
                ar_FillerCount[j]++;
            }
        }
      //  za.setScheduleEndTime(la.getScheduleEndTime());
        za.setScheduleEndTime(this.dt_EndTime);
        za.setScheduleStartTime(this.dt_StartTime);
        za.setTimeBeltEndTime(this.dt_EndTime);
        za.setTimeBeltStartTime(this.dt_StartTime);
        za.setActualEndTime(la.getActualEndTime());
        za.setActualStartTime(la.getActualStartTime());
        za.setLable(PlayList.Lable.ZERO);

        return za;
    }

    private PlayList getNextZeroAdvertisement(){
        PlayList pl = channelAdvertMapService.getNextMinDurationPlayList(i_ChannelId);
        if(pl != null){
            pl.setScheduleHour(i_hour);
            genZeroAds.add(pl.getPlaylistId());
        }
        return pl;
    }

    public boolean generatePlayListCrawler(ArrayList<PlayList> lstPlayList) {

        ArrayList<PlayList> lstPriority_01 = new ArrayList<>(); //endtime_this_hour
        ArrayList<PlayList> lstPriority_1hf = new ArrayList<>(); //endtime_first_half_hour
        ArrayList<PlayList> lstPriority_2hf = new ArrayList<>(); //starttime_last_half_timbelt

        long lTimeDuration = dt_EndTime.getTime() - dt_StartTime.getTime();

        for (PlayList item : lstPlayList) {
            long available_duration = item.getScheduleEndTime().getTime() - dt_StartTime.getTime();
            long valid_duration = dt_EndTime.getTime() - item.getScheduleStartTime().getTime();

            if (valid_duration <= lTimeDuration / 2) { //last 30_min
                lstPriority_2hf.add(item);
            } else if (available_duration <= lTimeDuration / 2) { //first 30_min
                lstPriority_1hf.add(item);
            } else {
                lstPriority_01.add(item);
            }
        }

        ArrayList<PlayList> retList = new ArrayList<PlayList>();
        if (lstPriority_2hf.size() == 0 && lstPriority_1hf.size() == 0) {
            retList = lstPriority_01;
            retList = setPlayTimeCrawler(retList, lTimeDuration, dt_StartTime, dt_EndTime);
        } else {
            for (PlayList item : lstPriority_01) {
                if (lstPriority_2hf.size() >= lstPriority_1hf.size()) {
                    lstPriority_1hf.add(item);
                } else {
                    lstPriority_2hf.add(item);
                }
            }
            lstPriority_01.clear();
            lstPriority_1hf = setPlayTimeCrawler(lstPriority_1hf, lTimeDuration / 2, dt_StartTime, new Date(dt_StartTime.getTime() + (lTimeDuration / 2)));
            lstPriority_2hf = setPlayTimeCrawler(lstPriority_2hf, lTimeDuration / 2, new Date(dt_StartTime.getTime() + (lTimeDuration / 2)), dt_EndTime);
            for (PlayList item : lstPriority_1hf) {
                retList.add(item);
            }
            for (PlayList item : lstPriority_2hf) {
                retList.add(item);
            }
            lstPriority_1hf.clear();
            lstPriority_2hf.clear();
        }
        lstPlayList = retList;
        return true;
    }

    public ArrayList<PlayList> setPlayTimeCrawler(ArrayList<PlayList> lstPlayList, long lTimeDuration, Date dtStartTime, Date dtEndTime) {
        ArrayList<PlayList> lstReturn = shufflePlayListCrawler(lstPlayList);
        long lIntervalbetweenAdverts = lTimeDuration / (lstReturn.size() + 1);

        for (int i = 1; i <= lstReturn.size(); i++) {
            long lTmpTime = dtStartTime.getTime() + (lIntervalbetweenAdverts * i);
            Date dtPlayTime = new Date();
            dtPlayTime.setTime(lTmpTime);
            lstReturn.get(i - 1).setTimeBeltStartTime(dtPlayTime);
            lstReturn.get(i - 1).setTimeBeltEndTime(dtEndTime);
        }
        return lstReturn;
    }

    public ArrayList<PlayList> shufflePlayListCrawler(ArrayList<PlayList> lstPlayList) {
        if (lstPlayList.size() <= 0) {
            return lstPlayList;
        }

        HashMap<Integer, ArrayList<PlayList>> mapAdverts = new HashMap<Integer, ArrayList<PlayList>>();
        for (PlayList item : lstPlayList) {
            Integer id = item.getAdvert().getAdvertid();
            if (!mapAdverts.containsKey(id)) {
                mapAdverts.put(id, new ArrayList<PlayList>());
            }
            mapAdverts.get(id).add(item);
        }

        ArrayList<PlayList> lstRetList = new ArrayList<PlayList>();
        ArrayList<Integer> keys = new ArrayList<>(mapAdverts.keySet());
        Integer preId = -1;

        ///same advert /////
        if (keys.size() <= 1) {
            return lstPlayList;
        }

        Collections.sort(keys, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return mapAdverts.get(o2).size() - mapAdverts.get(o1).size();
            }
        });

        while (mapAdverts.get(keys.get(0)).size() > 0) {
            if (keys.get(0).equals(preId) && mapAdverts.get(keys.get(1)).size() > 0) {
                PlayList plItem = mapAdverts.get(keys.get(1)).get(0);
                mapAdverts.get(keys.get(1)).remove(plItem);
                lstRetList.add(plItem);
                preId = keys.get(1);
            } else {
                PlayList plItem = mapAdverts.get(keys.get(0)).get(0);
                mapAdverts.get(keys.get(0)).remove(plItem);
                lstRetList.add(plItem);
                preId = keys.get(0);
            }
            Collections.sort(keys, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return mapAdverts.get(o2).size() - mapAdverts.get(o1).size();
                }
            });
        }

        return lstRetList;
    }
}
