package com.vclabs.nash.service;

import com.vclabs.nash.model.dao.PlayHistoryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author chathuranga on 11/25/2020
 */
@Service
@Transactional("main")
public class PlayListHistoryService {

    @Autowired
    private PlayHistoryDAO playHistoryDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayListHistoryService.class);

    @Scheduled(cron = "0 30 1 * * ?")
    @Transactional
    public void removeDuplicatesByScheduleId() {
        playHistoryDAO.removeDuplicatesByScheduleId();
    }
}
