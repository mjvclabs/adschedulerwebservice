package com.vclabs.nash.service.teams;

import com.vclabs.nash.dashboard.dto.MyTeamToDoListDto;
import com.vclabs.nash.model.entity.teams.TeamDetail;

import java.util.List;

/**
 * Created by Sanduni on 03/01/2019
 */
public interface TeamDetailService {

    TeamDetail save(TeamDetail teamDetail);

    TeamDetail update(TeamDetail teamDetail);

    TeamDetail findById(Integer id);

    List<TeamDetail> findAllTeams();

    Boolean checkTeamName(String teamName);

    List<String> findAllTeamMembers(Integer teamId);

    List<String> findAllTeamAdmins(Integer teamId);

    Boolean deleteTeamMember(Integer teamId, String username);

    Boolean deleteTeamAdmin(Integer teamId, String username);

    List<MyTeamToDoListDto> getTeamMembersWODetail();

    List<TeamDetail> findAllTeamDetailsForTeamSalesRevenue();

}
