/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;

import com.vclabs.nash.model.dao.LogoContainerDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.LogoContainer;
import com.vclabs.nash.model.entity.LogoContainerMap;

/**
 * @author Sanira Nanayakkara
 */
@Service
@Transactional("main")
public class LogoContainerService {

    @Autowired
    private LogoContainerDAO logoContainerDao;
    @Autowired
    private AdvertisementDAO advertisementDao;

    public ArrayList<LogoContainer> getLogoContainerList() {
        return logoContainerDao.getLogoContainerList();
    }

    public Boolean updateLogoContainer(LogoContainer model) {
        return logoContainerDao.updateLogoContainer(model);
    }

    public Boolean insertLogoContainer(LogoContainer model) {
        return logoContainerDao.insertLogoContainer(model);
    }
    
    public List<Advertisement> getLogoList(int id){
        List<LogoContainerMap> logoMapList = logoContainerDao.getLogoListbyId(id);
        List<Advertisement> retList = new ArrayList<>();
        for(LogoContainerMap map : logoMapList)
            retList.add(map.getLogo());
        return retList;
    }
            
    public void insertLogoList(int id, List<Integer> lstLogos){
        if(!(advertisementDao.getSelectedAdvertisement(id).getAdvertpath().equals("LOGO_CONTAINER"))) return;
        
        List<LogoContainerMap> logoMapList = logoContainerDao.getLogoListbyId(id);
        for(LogoContainerMap map : logoMapList){
            if(lstLogos.contains(map.getLogo().getAdvertid())){
                lstLogos.remove(map.getLogo().getAdvertid());
            }else{
                logoContainerDao.removeLogoContainerMap(map);
            }
        }
        for(Integer item : lstLogos){
            LogoContainerMap logoConMap = new LogoContainerMap();
            logoConMap.setLogoContainerId(id);
            
            Advertisement logo = advertisementDao.getSelectedAdvertisement(item);
            if(!(logo.getAdverttype().equals("LOGO")) || logo.getAdvertpath().equals("LOGO_CONTAINER")) continue;
            
            logoConMap.setLogo(logo);
            logoContainerDao.insertLogoContainerMap(logoConMap);
        }            
    }
}
