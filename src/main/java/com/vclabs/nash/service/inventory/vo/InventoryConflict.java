package com.vclabs.nash.service.inventory.vo;

import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.service.utill.MathUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-26.
 */
public class InventoryConflict {

    private Date inventoryDate;

    private List<InventoryConflictRow> rows = new ArrayList<>();

    public InventoryConflict(Date date){
        this.inventoryDate = date;
    }

    public Date getInventoryDate() {
        return inventoryDate;
    }

    public void setInventoryDate(Date inventoryDate) {
        this.inventoryDate = inventoryDate;
    }

    public List<InventoryConflictRow> getRows() {
        return rows;
    }

    public void setRows(List<InventoryConflictRow> rows) {
        this.rows = rows;
    }

    public void addTVCRow(InventoryRow iRow, double consumedAmount, double requestedAmount, double newAmount){
        addRow(iRow, InventoryConflictRow.Type.TVC, iRow.getWebTvcDuration(), consumedAmount, MathUtil.round(requestedAmount,2),newAmount);
    }

    public void addLogoRow(InventoryRow iRow, int consumedAmount, int requestedAmount, int newAmount){
        addRow(iRow, InventoryConflictRow.Type.LOGO, iRow.getLogoSpots(), consumedAmount,requestedAmount,newAmount);
    }

    public void addCrawlerRow(InventoryRow iRow, int consumedAmount, int requestedAmount, int newAmount){
        addRow(iRow, InventoryConflictRow.Type.Crawler, iRow.getCrawlerSpots(), consumedAmount,requestedAmount,newAmount);
    }

    public void addLCrawlerRow(InventoryRow iRow, int consumedAmount, int requestedAmount, int newAmount){
        addRow(iRow, InventoryConflictRow.Type.LCrawler, iRow.getCrawlerSpots(), consumedAmount,requestedAmount,newAmount);
    }

    private void addRow(InventoryRow iRow, InventoryConflictRow.Type type, Number oldAmount, Number consumedAmount, Number requestedAmount, Number newAmount){
        InventoryConflictRow inventoryConflictRow = new InventoryConflictRow();
        inventoryConflictRow.setDate(inventoryDate);
        inventoryConflictRow.setType(type);
        inventoryConflictRow.setConsumedAmount(consumedAmount);
        inventoryConflictRow.setRequestedAmount(requestedAmount);
        inventoryConflictRow.setNewAmount(newAmount);
        inventoryConflictRow.setInventoryRow(iRow);
        inventoryConflictRow.setOldAmount(oldAmount);
        rows.add(inventoryConflictRow);
    }
}
