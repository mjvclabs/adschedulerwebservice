package com.vclabs.nash.service.inventory.impl;

import com.vclabs.nash.model.dao.inventory.InventoryConsumptionDAO;
import com.vclabs.nash.model.entity.inventory.InventoryConsumption;
import com.vclabs.nash.service.inventory.InventoryConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Sanduni on 02/10/2018
 */
@Service
public class InventoryConsumptionServiceImpl implements InventoryConsumptionService{

    @Autowired
    private InventoryConsumptionDAO repository;

    @Override
    public InventoryConsumption findByInventoryRowId(long inventoryRowId) {
        return repository.findByInventoryRowId(inventoryRowId);
    }

    @Override
    public InventoryConsumption save(InventoryConsumption ic){
        InventoryConsumption existIC =  findByInventoryRowId(ic.getInventoryRow().getId());
        if(existIC == null){
            return repository.saveAndFlush(ic);
        }
        return ic;
    }
}
