package com.vclabs.nash.service.inventory.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.inventory.InventoryDAO;
import com.vclabs.nash.model.dao.inventory.InventoryRowDAO;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryConsumption;
import com.vclabs.nash.model.entity.inventory.InventoryPriceRange;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.service.inventory.InventoryConsumptionService;
import com.vclabs.nash.service.inventory.InventoryService;
import com.vclabs.nash.service.inventory.vo.InventoryConflict;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-26.
 */
@Service
public class InventoryServiceImpl implements InventoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryServiceImpl.class);

    @Autowired
    private InventoryDAO repository;

    @Autowired
    private InventoryRowDAO inventoryRowRepository;

    @Autowired
    private InventoryConsumptionService inventoryConsumptionService;

    @Value(value = "classpath:inventory-price-ranges.json")
    private Resource inventoryRanges;

    @Override
    @Transactional
    public Inventory save(Inventory inventory) {
        repository.save(inventory);
        for(InventoryRow row : inventory.getInventoryRows()){
            row.setInventory(inventory);
            repository.save(row);
            for(InventoryPriceRange pr : row.getPriceRanges()){
                pr.setInventoryRow(row);
                repository.save(pr);
            }
        }
        inventory = repository.save(inventory);
        LOGGER.debug("Successfully saved/updated inventory id = {} of channel  = {}", inventory.getId(), inventory.getChannel());
        return inventory;
    }

    @Override
    @Transactional
    public InventoryRow save(InventoryRow inventoryRow) {
        return repository.save(inventoryRow);
    }

    @Override
    @Transactional
    public Inventory saveOrUpdate(Inventory inventory, InventoryConflict... conflict) {
        Inventory oldInventory = findByChannelAndDate(inventory.getChannel().getChannelid(), inventory.getDate());
        if(oldInventory == null){
            return save(inventory);
        }
        oldInventory = merge(oldInventory, inventory, conflict[0]);
        return oldInventory;
    }

    @Override
    public Inventory generateInitialInventory(Date date) {
        Inventory inventory = new Inventory();
        inventory.setDate(date);
        List<InventoryRow> rows = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            InventoryRow row = new InventoryRow();
            row.setFromTime(new Time( i, 0,0));
            row.setToTime(new Time( i+1, 0,0));
            setPriceRanges(row);
            row.setInventory(inventory);
            rows.add(row);
        }
        inventory.setInventoryRows(rows);
        return inventory;
    }

    @Override
    public Inventory generateInitialInventory() {
        return generateInitialInventory(new Date(0));
    }

    @Override
    public Inventory findByChannel(int channelId) {
        return repository.findByChannelId(channelId);
    }

    @Override
    public Inventory findByChannelAndDate(int channelId, Date date) {
        return repository.findByChannelIdAndDate(channelId, date);
    }

    @Override
    public boolean isExistInventory(Long channelId, Date date) {
        return false;
    }

    @Override
    @Transactional
    public InventoryRow findByChannelAndDateAndTime(Integer channelId, Date date, Time fromTime, Time toTime) {
        return inventoryRowRepository.findByChannelAndDateAndTimeBelt(channelId,date,fromTime,toTime);
    }

    @Override
    public List<Date> findInventoryExistDates(Integer channelId, Date fromDate, Date toDate) {
        return repository.findInventoryExistDates(channelId, fromDate, toDate);
    }

    private void setPriceRanges(InventoryRow inventoryRow){
        List<InventoryPriceRange> priceRanges =  inventoryRow.getPriceRanges();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<InventoryPriceRange> ranges =  objectMapper.readValue(inventoryRanges.getInputStream(),
                    new TypeReference<List<InventoryPriceRange>>(){});
            for(InventoryPriceRange priceRange : ranges){
                priceRange.setInventoryRow(inventoryRow);
                priceRanges.add(priceRange);
            }
        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
        }
    }

    private Inventory merge(Inventory oldInventory, Inventory newInventory, InventoryConflict conflict) {
        List<InventoryRow> oldRows = oldInventory.getInventoryRows();
        List<InventoryRow> newRows = newInventory.getInventoryRows();
        for (int i = 0; i < 24; i++) {
            InventoryRow oldRow = oldRows.get(i);
            InventoryRow newRow = newRows.get(i);
            oldRow.setClusters(newRow.getClusters());

            oldRow.setLogoPrice(newRow.getLogoPrice());
            oldRow.setCrawlerPrice(newRow.getCrawlerPrice());
            oldRow.setlCrawlerPrice(newRow.getlCrawlerPrice());

            updateInventoryRow(oldRow, newRow, conflict);

            List<InventoryPriceRange> oldRowPriceRanges = oldRow.getPriceRanges();
            List<InventoryPriceRange> newRowPriceRanges = newRow.getPriceRanges();
            for(int j = 0; j < oldRowPriceRanges.size(); j++){
                InventoryPriceRange oldPriceRange = oldRowPriceRanges.get(j);
                InventoryPriceRange newPriceRange = newRowPriceRanges.get(j);
                oldPriceRange.setPrice(newPriceRange.getPrice());
            }
        }
        newInventory = save(oldInventory);
        return newInventory;
    }

    private void updateInventoryRow(InventoryRow oldRow, InventoryRow newRow, InventoryConflict conflict){

        InventoryConsumption ic = inventoryConsumptionService.findByInventoryRowId(oldRow.getId());
        if(ic == null){
            oldRow.setTotalTvcDuration(newRow.getTotalTvcDuration());
            oldRow.setWebTvcDuration(newRow.getWebTvcDuration());
            oldRow.setLogoSpots(newRow.getLogoSpots());
            oldRow.setWebLogoSpots(newRow.getWebLogoSpots());
            oldRow.setCrawlerSpots(newRow.getCrawlerSpots());
            oldRow.setWebCrawlerSpots(newRow.getWebCrawlerSpots());
            oldRow.setLCrawlerSpots(newRow.getLCrawlerSpots());
            oldRow.setWebLCrawlerSpots(newRow.getWebLCrawlerSpots());
            return;
        }

        double webTvcDuration = newRow.getWebTvcDuration();
        double totalTvcDuration = newRow.getTotalTvcDuration();
        if(ic.getSpentTime() > webTvcDuration){
            webTvcDuration = ic.getSpentTime();
            if(totalTvcDuration < webTvcDuration){
                totalTvcDuration = webTvcDuration;
            }
            conflict.addTVCRow(oldRow, webTvcDuration, newRow.getWebTvcDuration(), webTvcDuration);
        }
        oldRow.setWebTvcDuration(webTvcDuration);
        oldRow.setTotalTvcDuration(totalTvcDuration);

        int logoSpots = newRow.getWebLogoSpots();
        int totalLogoSpots = newRow.getLogoSpots();
        if(ic.getSpentLogoCount() > logoSpots){
            logoSpots = ic.getSpentLogoCount();
            if(totalLogoSpots < logoSpots){
                totalLogoSpots = logoSpots;
            }
            conflict.addLogoRow(oldRow, logoSpots, newRow.getWebLogoSpots(), logoSpots);
        }
        oldRow.setLogoSpots(totalLogoSpots);
        oldRow.setWebLogoSpots(logoSpots);

        int crawlerSpots = newRow.getWebCrawlerSpots();
        int totalCrawlerSpots = newRow.getCrawlerSpots();
        if(ic.getSpentCrawlerCount() > crawlerSpots){
            crawlerSpots = ic.getSpentCrawlerCount();
            if(totalCrawlerSpots < crawlerSpots){
                totalCrawlerSpots = crawlerSpots;
            }
            conflict.addCrawlerRow(oldRow, crawlerSpots, newRow.getWebCrawlerSpots(), crawlerSpots);
        }
        oldRow.setCrawlerSpots(totalCrawlerSpots);
        oldRow.setWebCrawlerSpots(crawlerSpots);

        //update remaining inventory balance of consumptions
        double remainingTime = 0;
        if(oldRow.getWebTvcDuration() >  ic.getSpentTime()){
            remainingTime = oldRow.getWebTvcDuration() - ic.getSpentTime();
        }
        ic.setRemainingTime(remainingTime);

        int remainingLogoCount = 0;
        if(oldRow.getWebLogoSpots() > ic.getSpentLogoCount()){
            remainingLogoCount = oldRow.getWebLogoSpots() - ic.getSpentLogoCount();
        }
        ic.setRemainingLogoCount(remainingLogoCount);

        int remainingCrawlers = 0;
        if(oldRow.getWebCrawlerSpots() >  ic.getSpentCrawlerCount()){
            remainingCrawlers = oldRow.getWebCrawlerSpots() - ic.getSpentCrawlerCount();
        }
        ic.setRemainingCrawlerCount(remainingCrawlers);

        inventoryConsumptionService.save(ic);
    }
}
