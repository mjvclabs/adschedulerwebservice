package com.vclabs.nash.service.inventory;

import com.vclabs.nash.dashboard.dto.ChannelUtillizationDto;
import com.vclabs.nash.model.entity.inventory.InventoryConsumption;
import com.vclabs.nash.model.entity.inventory.NashInventoryConsumption;

import java.util.List;

/**
 * Created by Nalaka on 2018-10-04.
 */
public interface NashInventoryConsumptionService {

    NashInventoryConsumption findByInventoryRowId(long inventoryRowId);

    NashInventoryConsumption save(NashInventoryConsumption ic);

    List<NashInventoryConsumption> findHighestInventoryUtilizedChannels();

    List<ChannelUtillizationDto> findChannelUtillizationDetails();
}
