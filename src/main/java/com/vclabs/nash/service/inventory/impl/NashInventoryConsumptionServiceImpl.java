package com.vclabs.nash.service.inventory.impl;

import com.vclabs.nash.dashboard.dto.ChannelUtillizationDto;
import com.vclabs.nash.model.dao.inventory.NashInventoryConsumptionDAO;
import com.vclabs.nash.model.entity.inventory.NashInventoryConsumption;
import com.vclabs.nash.service.inventory.NashInventoryConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2018-10-04.
 */
@Service
public class NashInventoryConsumptionServiceImpl implements NashInventoryConsumptionService {

    @Autowired
    private NashInventoryConsumptionDAO nashInventoryConsumptionDAO;

    @Override
    public NashInventoryConsumption findByInventoryRowId(long inventoryRowId) {
        return nashInventoryConsumptionDAO.findByInventoryRowId(inventoryRowId);
    }

    @Override
    public NashInventoryConsumption save(NashInventoryConsumption ic) {
        return nashInventoryConsumptionDAO.save(ic);
    }

    @Override
    @Transactional
    public List<NashInventoryConsumption> findHighestInventoryUtilizedChannels() {
        return nashInventoryConsumptionDAO.findHighestInventoryUtilizedChannels();
    }

    @Override
    @Transactional
    public List<ChannelUtillizationDto> findChannelUtillizationDetails() {
        List<NashInventoryConsumption> inventoryConsumptions = findHighestInventoryUtilizedChannels();
        List<ChannelUtillizationDto> channelUtillizationDtoList = new ArrayList<>();

        for (NashInventoryConsumption inventoryConsumption : inventoryConsumptions){
            Double totalTime = inventoryConsumption.getSpentTime() + inventoryConsumption.getRemainingTime();
            ChannelUtillizationDto channelUtillizationDto = new ChannelUtillizationDto();
            channelUtillizationDto.setChannel(inventoryConsumption.getChannel());
            channelUtillizationDto.setRemainingTimePercentage(calculatePercentage(inventoryConsumption.getRemainingTime(), totalTime).intValue());
            channelUtillizationDto.setSpentTimePercentage(calculatePercentage(inventoryConsumption.getSpentTime(), totalTime).intValue());
            channelUtillizationDtoList.add(channelUtillizationDto);
        }
        return channelUtillizationDtoList;
    }

    private Double calculatePercentage(Double percentageAmount, Double totalTime){
        return percentageAmount*100/totalTime;
    }

}
