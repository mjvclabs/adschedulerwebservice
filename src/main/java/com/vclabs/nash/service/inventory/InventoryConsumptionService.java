package com.vclabs.nash.service.inventory;

import com.vclabs.nash.model.entity.inventory.InventoryConsumption;

/**
 * Created by Sanduni on 02/10/2018
 */
public interface InventoryConsumptionService {

    InventoryConsumption findByInventoryRowId(long inventoryRowId);

    InventoryConsumption save(InventoryConsumption ic);
}
