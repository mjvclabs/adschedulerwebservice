/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.ClientDetails;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.excel.ClientOrAgencyExcel;
import com.vclabs.nash.model.view.reporting.ClientOrAgencyList;
import com.vclabs.nash.model.view.reporting.ClientOrAgencyValue;

/**
 *
 * @author Nalaka
 * @since 26-02-2018
 */
@Service
@Transactional("main")
public class ClientAndAgencyRevenueService {

    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;
    @Autowired
    private ClientOrAgencyExcel clientOrAgencyExcel;

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAndAgencyRevenueService.class);

    public Map<Integer, ClientOrAgencyList> getClientOrAgencyRevenue(List<Integer> clientIDs, int year, String month, Boolean isClient) {

        List<WorkOrder> workOrderList = workOrderDao.getSelectedClientWorkOrder(0, clientIDs);
        filterRevenueYear(year, workOrderList);
        Map<Integer, ClientOrAgencyList> clientMap = new TreeMap<Integer, ClientOrAgencyList>();
        //int lastClientKey = -111;//clientOrAgencyList.setGrandTotal();
        for (WorkOrder workOrder : workOrderList) {
            ClientDetails clientDetail = clientDetailDao.getSelectedClient(workOrder.getClient());
            if (isClient && clientDetail.getClienttype().equals("Client")) {
                if (month.equals("All") || month.equals(workOrder.getRevenueMonth())) {
                    ClientOrAgencyList clientOrAgency = clientMap.get(workOrder.getClient());
                    if (clientOrAgency == null) {
                        clientOrAgency = new ClientOrAgencyList();
                        clientOrAgency.getClientAgencyList().put(getMonthName(workOrder.getRevenueMonth()), getClientOrAgencyModel(workOrder));
                        clientOrAgency.setClientOrAgencyName(clientDetail.getClientname());
                        clientMap.put(workOrder.getClient(), clientOrAgency);
                    } else {
                        ClientOrAgencyValue clientOrAgencyValue = clientOrAgency.getClientAgencyList().get(getMonthName(workOrder.getRevenueMonth()));
                        if (clientOrAgencyValue == null) {
                            clientOrAgency.getClientAgencyList().put(getMonthName(workOrder.getRevenueMonth()), getClientOrAgencyModel(workOrder));
                        } else {
                            clientOrAgencyValue.addValue(workOrder.getTotalBudget());
                        }
                    }
                }
            } else {
                if (clientDetail.getClienttype().equals("Agency")) {
                    if (month.equals("All") || month.equals(workOrder.getRevenueMonth())) {
                        ClientOrAgencyList clientOrAgency = clientMap.get(workOrder.getClient());
                        if (clientOrAgency == null) {
                            clientOrAgency = new ClientOrAgencyList();
                            clientOrAgency.getClientAgencyList().put(getMonthName(workOrder.getRevenueMonth()), getClientOrAgencyModel(workOrder));
                            clientOrAgency.setClientOrAgencyName(clientDetail.getClientname());
                            clientMap.put(workOrder.getClient(), clientOrAgency);
                        } else {
                            ClientOrAgencyValue clientOrAgencyValue = clientOrAgency.getClientAgencyList().get(getMonthName(workOrder.getRevenueMonth()));
                            if (clientOrAgencyValue == null) {
                                clientOrAgency.getClientAgencyList().put(getMonthName(workOrder.getRevenueMonth()), getClientOrAgencyModel(workOrder));
                            } else {
                                clientOrAgencyValue.addValue(workOrder.getTotalBudget());
                            }
                        }
                    }
                }
            }
        }
        if (month.equals("All")) {
            fillMonth(clientMap);
        }
        calculateGrandTotal(clientMap);
        return clientMap;
    }

    public ClientOrAgencyValue getClientOrAgencyModel(WorkOrder workOrder) {

        ClientOrAgencyValue clientOrAgencyValue = new ClientOrAgencyValue();
        clientOrAgencyValue.setClientOrAgencyName("" + workOrder.getClient());
        clientOrAgencyValue.setMonthName(workOrder.getRevenueMonth());
        clientOrAgencyValue.addValue(workOrder.getTotalBudget());

        return clientOrAgencyValue;

    }

    public void filterRevenueYear(int year, List<WorkOrder> workOrderList) {

        for (int i = 0; i < workOrderList.size(); i++) {
//            int revenueMonth = getMonthName(workOrderList.get(i).getRevenueMonth());
//            if(revenueMonth!=0) {
//                revenueMonth--;
//            }
            //if (year != getRevenueYear(revenueMonth, workOrderList.get(i))) {
            if(!getYear(year,workOrderList.get(i))){
                workOrderList.remove(workOrderList.get(i));
                i--;
            }
        }

    }

    public Boolean getYear(int year, WorkOrder workOrder) {

        Map<Integer, Integer> yearMonthMap = new TreeMap<Integer, Integer>();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(workOrder.getStartdate());

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(workOrder.getEnddate());

        int startDateYear = calendar.get(Calendar.YEAR);
        int endDateYear = endCalendar.get(Calendar.YEAR);

        if (startDateYear == endDateYear) {
            if (year == startDateYear) {
                return true;
            } else {
                return false;
            }
        } else {
            int revenueMonth = getMonthName(workOrder.getRevenueMonth());
            if (getRevenueYear(revenueMonth, workOrder) == year) {
                return true;
            } else {
                return false;
            }
        }

    }

    public int getRevenueYear(int month, WorkOrder workOrder) {
        Map<Integer, Integer> yearMonthMap = new TreeMap<Integer, Integer>();

        int revenueMonth = getMonthName(workOrder.getRevenueMonth());

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(workOrder.getStartdate());

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(workOrder.getEnddate());

        while (calendar.before(endCalendar)) {
            int woYear = calendar.get(Calendar.YEAR);
            int woMonth = calendar.get(Calendar.MONTH);
            calendar.add(Calendar.MONTH, 1);

            yearMonthMap.put(woMonth, woYear);
        }
        int year = 0;
        try {
            year = yearMonthMap.get(month);
        } catch (NullPointerException e) {
            LOGGER.debug("Exception in ClientAndAgencyRevenueService while trying to get revenue year: {}", e.getMessage());
        }
        return year;
    }

    public void fillMonth(Map<Integer, ClientOrAgencyList> clientMap) {
        String[] monthArry = {"NotSetMonth", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        for (Entry<Integer, ClientOrAgencyList> entry : clientMap.entrySet()) {
            ClientOrAgencyList clientOrAgencyList = entry.getValue();
            for (String month : monthArry) {
                ClientOrAgencyValue clientOrAgencyValue = clientOrAgencyList.getClientAgencyList().get(getMonthName(month));
                if (clientOrAgencyValue == null) {
                    WorkOrder workOrder = new WorkOrder();
                    workOrder.setClient(entry.getKey());
                    workOrder.setRevenueMonth(month);

                    clientOrAgencyList.getClientAgencyList().put(getMonthName(month), getClientOrAgencyModel(workOrder));
                }
            }
        }
    }

    public int getMonthName(String month) {
        int monthNumber = 0;
        switch (month) {
            case "January":
                monthNumber = 1;
                break;

            case "February":
                monthNumber = 2;
                break;

            case "March":
                monthNumber = 3;
                break;

            case "April":
                monthNumber = 4;
                break;

            case "May":
                monthNumber = 5;
                break;

            case "June":
                monthNumber = 6;
                break;
            case "July":
                monthNumber = 7;
                break;

            case "August":
                monthNumber = 8;
                break;

            case "September":
                monthNumber = 9;
                break;

            case "October":
                monthNumber = 10;
                break;

            case "November":
                monthNumber = 11;
                break;

            case "December":
                monthNumber = 12;
                break;
            default:
                monthNumber = 0;
                break;
        }

        return monthNumber;
    }

    public void calculateGrandTotal(Map<Integer, ClientOrAgencyList> clientMap) {
        List<Integer> monthArray = new ArrayList<>();
        if (!clientMap.isEmpty()) {
            ClientOrAgencyList clientOrAgencyList = clientMap.entrySet().iterator().next().getValue();
            for (Entry<Integer, ClientOrAgencyValue> entry : clientOrAgencyList.getClientAgencyList().entrySet()) {
                monthArray.add(entry.getKey());
            }
        }

        ClientOrAgencyList clientOrAgencyList = new ClientOrAgencyList();
        clientOrAgencyList.setClientOrAgencyName("Grand Total");
        Boolean calculateAllMonthGrandTotal = Boolean.TRUE;
        for (int month : monthArray) {
            ClientOrAgencyValue clientOrAgencyValue = new ClientOrAgencyValue();
            for (Entry<Integer, ClientOrAgencyList> entry : clientMap.entrySet()) {
                clientOrAgencyValue.addValue(entry.getValue().getClientAgencyList().get(month).getValue());
                clientOrAgencyValue.setMonth(month);
                if (calculateAllMonthGrandTotal) {
                    entry.getValue().setGrandTotal();
                    clientOrAgencyList.addGrendTotal(entry.getValue().getGrandTotal());
                }
            }
            clientOrAgencyList.getClientAgencyList().put(month, clientOrAgencyValue);
            calculateAllMonthGrandTotal = Boolean.FALSE;
        }
        clientMap.put(100000, clientOrAgencyList);
    }

    public Map<Integer, Integer> getYears() {
        List<WorkOrder> workOrderList = workOrderDao.getSelectedClientWorkOrder(0, new ArrayList<>());
        Map<Integer, Integer> yearMap = new TreeMap<Integer, Integer>();
        for (WorkOrder workOrder : workOrderList) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(workOrder.getStartdate());
            int year = cal.get(Calendar.YEAR);
            yearMap.put(year, year);

            cal.setTime(workOrder.getEnddate());
            year = cal.get(Calendar.YEAR);
            yearMap.put(year, year);
        }
        return yearMap;
    }

    public Boolean generateClientRevenueExcel(HttpServletRequest request, List<Integer> clientIDs, int year, String month) throws Exception {
        try {
            clientOrAgencyExcel.generateClientORAgencyRevenueExcel(getClientOrAgencyRevenue(clientIDs, year, month, true), request, true);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in ClientAndAgencyRevenueService in generateClientRevenueExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadClientRevenueExcell(HttpServletResponse response, HttpServletRequest request) {
        clientOrAgencyExcel.downloadClientRevenueExcell(response, request);
    }

    public Boolean generateAgencyRevenueExcel(HttpServletRequest request, List<Integer> clientIDs, int year, String month) throws Exception {
        try {
            clientOrAgencyExcel.generateClientORAgencyRevenueExcel(getClientOrAgencyRevenue(clientIDs, year, month, false), request, false);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in ClientAndAgencyRevenueService in generateAgencyRevenueExcel() : {}", e.getMessage());
            throw e;
        }
    }

    public void downloadAgencyRevenueExcell(HttpServletResponse response, HttpServletRequest request) {
        clientOrAgencyExcel.downloadAgencyRevenueExcell(response, request);
    }
}
