/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.service.report;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.dao.GeneralAuditDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.GeneralAudit;
import com.vclabs.nash.model.process.excel.MediaDeletionHistoryExcel;
import com.vclabs.nash.model.view.reporting.MediaDeletionView;
import com.vclabs.nash.service.DateFormat;

/**
 *
 * @author hp
 */
@Service
@Transactional("main")
public class MediaDeleteService extends DateFormat {
    
    @Autowired
    private AdvertisementDAO advertisementDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    MediaDeletionHistoryExcel mediaDeletionHistoryExcel;

    private static final Logger logger = LoggerFactory.getLogger(MediaDeleteService.class);
    
    public List<MediaDeletionView> getDeleteMediaFile() {
        
        List<MediaDeletionView> mediaFile = new ArrayList<>();
        List<Advertisement> advertList = advertisementDao.getDeletedAdvertList();
        List<GeneralAudit> generalAuditList = generalAuditDao.getAdvertDeleteDetails();
        
        for (Advertisement advertisement : advertList) {
            MediaDeletionView mediaModel = new MediaDeletionView();
            mediaModel.setAdvertID(advertisement.getAdvertid());
            mediaModel.setAdvertName(advertisement.getAdvertname());
            if (advertisement.getLastModifydate() != null) {
                mediaModel.setDeleteDate(getDataYYYMMDD_dash_HHmmss_colan(advertisement.getLastModifydate()));
            }

            for(GeneralAudit generalAudit1:generalAuditList){
                if(generalAudit1.getRecordId().equals(""+advertisement.getAdvertid())){
                    mediaModel.setDeleteUser(generalAudit1.getUser());
                    generalAuditList.remove(generalAudit1);
                    break;
                }
            }
            mediaFile.add(mediaModel);
        }
        
        return mediaFile;
    }
    
    public Boolean generateMediaDeletionHistoryReportExcel(HttpServletRequest request) throws Exception {
        try {
            mediaDeletionHistoryExcel.generateMediaDeletionHistoryReport(getDeleteMediaFile(), request);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in MediaDeleteService in generateMediaDeletionHistoryReportExcel() : {}", e.getMessage());
            throw e;
        }
    }
    
    public void downloadMediaDeletionHistoryExcel(HttpServletResponse response, HttpServletRequest request) {
        mediaDeletionHistoryExcel.downloadMediaDeletionHistoryExcell(response, request);
    }
}
