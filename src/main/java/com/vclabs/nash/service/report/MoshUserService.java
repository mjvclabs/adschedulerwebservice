package com.vclabs.nash.service.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.MoshUserLogDefDAO;
import com.vclabs.nash.model.entity.MoshUserLogDef;
import com.vclabs.nash.model.process.UserWiseInfo;
import com.vclabs.nash.model.process.excel.UserWiseExcel;
import com.vclabs.nash.model.view.reporting.UserWiseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2019-05-15.
 */
@Service
@Transactional("main")
public class MoshUserService {

    private static final Logger logger = LoggerFactory.getLogger(MoshUserService.class);

    @Autowired
    private MoshUserLogDefDAO moshUserLogDefDAO;

    @Autowired
    UserWiseExcel userWiseExcel;

    private SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    private SimpleDateFormat endDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    public List<UserWiseView> getUserLogdata(String filterData) {

        UserWiseInfo msData = new UserWiseInfo();
        try {
            ObjectMapper mapper = new ObjectMapper();
            msData = mapper.readValue(filterData, UserWiseInfo.class);
        } catch (IOException e) {
            logger.debug("IOException in MoshUserService in getUserLogdata: {}", e.getMessage());
        }

        ArrayList<String> userList = new ArrayList<>();
        ArrayList<Integer> channelList = new ArrayList<>();
        String[] userListString = msData.getUsers().split("=");
        String[] channelListString = msData.getChannelIds().split("=");

        if (userListString.length != 0) {
            for (String user : userListString) {
                if (!user.equals("") && !user.equals("All")) {
                    userList.add(user);
                }
            }
        }

        if (channelListString.length != 0) {
            for (String channelId : channelListString) {
                if (!channelId.equals("") &&!channelId.equals("-111")) {
                    channelList.add(Integer.parseInt(channelId));
                }
            }
        }

        List<UserWiseView> userWiseViewList = new ArrayList<>();
        try {
            Date std = startDateFormat.parse(msData.getStartDate()+" 00:00:00");
            Date end = endDateFormat.parse(msData.getEndDate()+ " 23:59:59");
            List<MoshUserLogDef> userLogDefList = moshUserLogDefDAO.getselectedLogs(channelList, std, end, userList);
            for (MoshUserLogDef moshUserLogDef : userLogDefList) {

                UserWiseView userWiseView = new UserWiseView();
                userWiseView.setChannelName(moshUserLogDef.getChannelDetails().getChannelname());
                userWiseView.setIpAddress(moshUserLogDef.getIpAddress());
                userWiseView.setUserName(moshUserLogDef.getUser());
                userWiseView.setLoginDate(dateFormat.format(moshUserLogDef.getLoginTime()));
                userWiseView.setLoginTime(timeFormat.format(moshUserLogDef.getLoginTime()));
                if(moshUserLogDef.getLogoutTime()!=null) {
                    userWiseView.setLogoutDate(dateFormat.format(moshUserLogDef.getLogoutTime()));
                    userWiseView.setLogoutTime(timeFormat.format(moshUserLogDef.getLogoutTime()));
                    userWiseView.setSessionDuration(getTimeDiff(moshUserLogDef));
                }
                userWiseViewList.add(userWiseView);
            }
        } catch (ParseException e) {
            logger.debug("Exception in MoshUserService in getUserLogdata: {}", e.getMessage());
        }

        return userWiseViewList;
    }

    private String getTimeDiff(MoshUserLogDef moshUserLogDef) {

        Long diff = moshUserLogDef.getLogoutTime().getTime() - moshUserLogDef.getLoginTime().getTime();

        Long diffSeconds = diff / 1000 % 60;
        Long diffMinutes = diff / (60 * 1000) % 60;
        Long diffHours = diff / (60 * 60 * 1000) % 24;
        Long diffDays = diff / (24 * 60 * 60 * 1000);

        String timeDiff = (diffDays.toString().length() == 1 ? ("0" + diffDays) : diffDays.toString()) +
                " " + (diffHours.toString().length() == 1 ? ("0" + diffHours) : diffHours.toString()) +
                ":" + (diffMinutes.toString().length() == 1 ? ("0" + diffMinutes) : diffMinutes.toString()) +
                ":" + (diffSeconds.toString().length() == 1 ? ("0" + diffSeconds) : diffSeconds.toString());

        return timeDiff;
    }

    public Boolean generateUsrWiseReportExcel(HttpServletRequest request, String filterData) {
        try {
            userWiseExcel.generateUserWiseReport(getUserLogdata(filterData), request);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in MissedSpotCountService in generateMissedSpotCountReportExcel: {}", e.getMessage());
            return false;
        }
    }

    public void downloadUserWiseExcel(HttpServletResponse response, HttpServletRequest request) {
        logger.info("downloadUserWiseExcel");
       userWiseExcel.downloadUserWiseReportExcell(response, request);
    }


}
