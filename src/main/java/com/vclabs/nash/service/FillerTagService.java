package com.vclabs.nash.service;

import com.vclabs.nash.model.dao.FillerTagDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.FillerTag;
import com.vclabs.nash.model.process.AdvertFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Nalaka on 2020-01-03.
 */
@Service
public class FillerTagService {

    @Autowired
    private FillerTagDAO fillerTagDAO;

    @Transactional
    public void saveFillerTag(int advertId, List<FillerTag> tags){
        List<FillerTag> oldTags = getByAdvertId(advertId);
        if(oldTags.isEmpty()){
            for(FillerTag fillerTag: tags){
                fillerTagDAO.saveFillerTag(fillerTag);
            }
        }else {
            Iterator<FillerTag> nIterator =  tags.iterator();
            while (nIterator.hasNext()){
                FillerTag fillerTag  = nIterator.next();
                Iterator<FillerTag> oIterator =  oldTags.iterator();
                while (oIterator.hasNext()){
                    if(fillerTag.getChannelDetails().getChannelid() == oIterator.next().getChannelDetails().getChannelid()){
                        oIterator.remove();
                        nIterator.remove();
                        break;
                    }
                }
            }
            for(FillerTag fillerTag: tags){
                fillerTagDAO.saveFillerTag(fillerTag);
            }
            if(!oldTags.isEmpty()){
                List<Long> deletedTagIds = oldTags.stream().map(t -> t.getId()).collect(Collectors.toList());
                fillerTagDAO.delete(deletedTagIds);
            }
        }
    }

    @Transactional
    public List<FillerTag> getByAdvertId(long advertId){
        return fillerTagDAO.getByAdvertId(advertId);
    }

    @Transactional
    public Page<Advertisement> getFillers(Map<String, String> filterRequest) {
        int page = Integer.parseInt(filterRequest.get("page"));
        int size = Integer.parseInt(filterRequest.get("size"));
        Pageable pageable = new PageRequest(page, size);

        AdvertFilter advertFilter = new AdvertFilter();
        advertFilter.setAdvertId(filterRequest.get("advertId"));
        if (advertFilter.getAdvertId().trim().length() == 0) {
            advertFilter.setAdvertId("-111");
        }
        advertFilter.setAdvertName(filterRequest.get("advertName"));
        if (advertFilter.getAdvertName().trim().length() == 0) {
            advertFilter.setAdvertName("-111");
        }
        advertFilter.setClient(filterRequest.get("clientId"));
        advertFilter.setChannelId(Integer.parseInt(filterRequest.get("channelId")));
        List<Advertisement> advertList = fillerTagDAO.getFillers(advertFilter, size, page * size);
        int numOfMatches = fillerTagDAO.getFillerCount(advertFilter);
        return new PageImpl<>(advertList, pageable, numOfMatches);
    }

    @Transactional
    public List<Advertisement> getFillersByChannelId(int channelId){
        return fillerTagDAO.getFillersByChannelId(channelId);
    }
}
