package com.vclabs.nash.service;

import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.view.DashboadChannelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional("main")
public class DashboardAsyncService {

    @Autowired
    private SchedulerDAO schedulerDao;

    @Async("dashboardTaskExecutor")
    public CompletableFuture<DashboadChannelView> prepareDashboardDataByChannelAsync(ChannelDetails model, Date date) {
        CompletableFuture<DashboadChannelView> completableFuture = new CompletableFuture<>();
        try{
            completableFuture.complete(prepareDashboardDataByChannel(model, date));
        }catch (Exception e){
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

    private DashboadChannelView prepareDashboardDataByChannel(ChannelDetails model, Date date) {
        DateFormat dateFormat = new DateFormat();
        Date today = dateFormat.getDataYYYMMDD_dash(dateFormat.getDataYYYMMDD_dash(new Date()));
        List<ScheduleDef> scheduleList = schedulerDao.getTodaySchedule(model.getChannelid(), date);
        for (int i = 0; i < scheduleList.size(); i++) {
            if (scheduleList.get(i).getStatus().equals("6")
                    || scheduleList.get(i).getStatus().equals("9")
                    || scheduleList.get(i).getStatus().equals("8")
                    || (!scheduleList.get(i).getAdvertid().isFileAvailable())) {
                scheduleList.remove(scheduleList.get(i));

                i--;
            }
        }
        if (!scheduleList.isEmpty()) {
            DashboadChannelView dashBoadModel = new DashboadChannelView();
            dashBoadModel.setChannelID(model.getChannelid());
            dashBoadModel.setChannelName(model.getShortname());
            dashBoadModel.setIsManual(Boolean.TRUE);
            dashBoadModel.setLogoPath(model.getChannelLogoPath());
            if (model.getServerDetails() == null) {
                dashBoadModel.setRunServer(Boolean.FALSE);
            } else {
                if (model.getServerDetails().getStatus() == 1) {
                    dashBoadModel.setRunServer(Boolean.TRUE);
                } else {
                    dashBoadModel.setRunServer(Boolean.FALSE);
                }
            }
            if (dateFormat.getDataYYYMMDD_dash(today).equals(dateFormat.getDataYYYMMDD_dash(model.getLastUpdate()))) {
                dashBoadModel.setUpdated(Boolean.TRUE);
            } else {
                dashBoadModel.setUpdated(Boolean.FALSE);
            }

            return dashBoadModel;
        }
        return null;
    }
}
