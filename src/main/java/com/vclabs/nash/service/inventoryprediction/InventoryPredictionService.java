package com.vclabs.nash.service.inventoryprediction;

import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryMonthlyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryWeeklyPrediction;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
public interface InventoryPredictionService {

    void saveDailyInventoryPredictions(String filePath);

    void saveWeeklyInventoryPredictions(String filePath);

    void saveMonthlyInventoryPredictions(String filePath);

    InventoryDailyPrediction findDailyPredictionById(long id);

    List<InventoryDailyPrediction> findAllDailyPredictions();

    List<InventoryDailyPrediction> findLatestDailyPredictions(Date startingDate, Date lastDate);

    List<InventoryWeeklyPrediction>findLatestWeeklyPredictions(Date startingDate, Date lastDate);

    List<InventoryMonthlyPrediction>findLatestMonthlyPredictions(Date startingDate, Date lastDate);

    InventoryDailyPrediction save(InventoryDailyPrediction inventoryDailyPrediction);

    InventoryWeeklyPrediction save(InventoryWeeklyPrediction inventoryWeeklyPrediction);

    InventoryMonthlyPrediction save(InventoryMonthlyPrediction inventoryMonthlyPrediction);

    List<InventoryDailyPrediction> getLatestDailyInventory();

    List<InventoryWeeklyPrediction> getLatestWeeklyInventory();

    List<InventoryMonthlyPrediction> getLatestMonthlyInventory();

    void updateDailyPredictionResultMeta();

    void updateWeeklyPredictionResultMeta();

    void updateMonthlylyPredictionResultMeta();

}
