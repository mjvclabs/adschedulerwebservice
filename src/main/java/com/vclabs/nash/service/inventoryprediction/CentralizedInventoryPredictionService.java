package com.vclabs.nash.service.inventoryprediction;

import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;

import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 11/10/2018
 */
public interface CentralizedInventoryPredictionService {

    void syncPredicteDataWithCentralizedInventory();

    CentralizedInventoryPrediction save(CentralizedInventoryPrediction centralizedInventoryPrediction);

    CentralizedInventoryPrediction findPrediction(Date dateAndTime, ChannelDetails channelDetails);

    void saveDailyPredictionsInCentralizedInventory();

    CentralizedInventoryPrediction overrideByDailyPrediction(InventoryDailyPrediction prediction);

    void overrideByWeeklyPrediction();

    void overrideByMonthlyPrediction();

    List<CentralizedInventoryPrediction> findUpdatedInventory();

    List<CentralizedInventoryPrediction> findByDateAndChannel(Date date, Integer channelId);
}
