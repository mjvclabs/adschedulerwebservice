package com.vclabs.nash.service.inventoryprediction.cornjob;

import com.vclabs.nash.service.inventoryprediction.CentralizedInventoryPredictionService;
import com.vclabs.nash.service.inventoryprediction.InventoryPredictionFileService;
import com.vclabs.nash.service.inventoryprediction.InventoryPredictionService;
import com.vclabs.nash.service.inventoryprediction.InventorySyncService;
import com.vclabs.nash.service.utill.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Nalaka on 2018-10-18.
 */
@Component
public class InventoryPredictionScheduler {

    @Autowired
    private InventoryPredictionFileService inventoryPredictionFileService;
    @Autowired
    private InventoryPredictionService inventoryPredictionService;
    @Autowired
    private CentralizedInventoryPredictionService centralizedInventoryPredictionService;
    @Autowired
    private InventorySyncService inventorySyncService;

    private static final Logger logger = LoggerFactory.getLogger(InventoryPredictionScheduler.class);

   // @Scheduled(cron = "0 0/20 0-23 * * *")
    public void predictedDataSyncWithCentralizeInventory(){
        logger.debug("Start copy prediction file. date {}", DateUtil.toLocalDate(new Date()));
        inventoryPredictionFileService.copyPredictionFiles();
        logger.debug("Successfully copy prediction file. date {}", DateUtil.toLocalDate(new Date()));

        logger.debug("Start read all prediction file. date {}", DateUtil.toLocalDate(new Date()));
        inventoryPredictionFileService.readAllInventoryFile();
        logger.debug("Successfully read all prediction file. date {}", DateUtil.toLocalDate(new Date()));

        logger.debug("Start to predicted data sync with CentralizeInventory. date {}", DateUtil.toLocalDate(new Date()));
        centralizedInventoryPredictionService.syncPredicteDataWithCentralizedInventory();
        logger.debug("Successfully data sync with CentralizeInventory. date {}", DateUtil.toLocalDate(new Date()));

        logger.debug("Start to CentralizeInventory data sync with system inventory. date {}", DateUtil.toLocalDate(new Date()));
        inventorySyncService.syncPredictionInventoryWithSystemInventory();
        logger.debug("Successfully CentralizeInventory data sync with system inventory. date {}", DateUtil.toLocalDate(new Date()));
    }
}
