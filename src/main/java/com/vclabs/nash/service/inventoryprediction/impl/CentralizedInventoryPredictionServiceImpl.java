package com.vclabs.nash.service.inventoryprediction.impl;

import com.vclabs.nash.model.dao.inventoryprediction.CentralizedInventoryPredictionDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryMonthlyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryWeeklyPrediction;
import com.vclabs.nash.service.inventoryprediction.CentralizedInventoryPredictionService;
import com.vclabs.nash.service.inventoryprediction.InventoryPredictionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction.PredictionType.DAILY_INVENTORY;
import static com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction.PredictionType.MONTHLY_INVENTORY;
import static com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction.PredictionType.WEEKLY_INVENTORY;

/**
 * Created by Sanduni on 11/10/2018
 */
@Service
public class CentralizedInventoryPredictionServiceImpl implements CentralizedInventoryPredictionService {

    @Autowired
    private CentralizedInventoryPredictionDAO centralizedInventoryPredictionDAO;

    @Autowired
    private InventoryPredictionService inventoryPredictionService;

    @Override
    @Transactional
    public void syncPredicteDataWithCentralizedInventory() {
        overrideByMonthlyPrediction();
        overrideByWeeklyPrediction();
        saveDailyPredictionsInCentralizedInventory();
    }

    @Override
    public CentralizedInventoryPrediction save(CentralizedInventoryPrediction centralizedInventoryPrediction) {
        return centralizedInventoryPredictionDAO.save(centralizedInventoryPrediction);
    }

    @Override
    @Transactional
    public CentralizedInventoryPrediction findPrediction(Date dateAndTime, ChannelDetails channelDetails) {
        return centralizedInventoryPredictionDAO.findCentralizedInventoryPrediction(dateAndTime, channelDetails.getChannelid());
    }

    @Override
    @Transactional
    public void saveDailyPredictionsInCentralizedInventory(){
        List<InventoryDailyPrediction> dailyPredictions = inventoryPredictionService.getLatestDailyInventory();
        for (InventoryDailyPrediction inventoryDailyPrediction : dailyPredictions) {
            overrideByDailyPrediction(inventoryDailyPrediction);
        }
        inventoryPredictionService.updateDailyPredictionResultMeta();
    }

    @Override
    @Transactional
    public CentralizedInventoryPrediction overrideByDailyPrediction(InventoryDailyPrediction dailyPrediction) {
        CentralizedInventoryPrediction cip = findPrediction(dailyPrediction.getDateAndTime(), dailyPrediction.getChannelId());
        if (cip == null) {
            cip = new CentralizedInventoryPrediction();
        }
        setCentralizedInventoryByDailyPredictions(dailyPrediction, cip);
        centralizedInventoryPredictionDAO.save(cip);
        return cip;
    }

    @Transactional
    @Override
    public void overrideByWeeklyPrediction() {
        List<InventoryWeeklyPrediction> weeklyPredictionList = inventoryPredictionService.getLatestWeeklyInventory();
        for (InventoryWeeklyPrediction inventoryWeeklyPrediction : weeklyPredictionList) {
            CentralizedInventoryPrediction centralizedInventoryPrediction = centralizedInventoryPredictionDAO.findCentralizedInventoryPrediction(inventoryWeeklyPrediction.getDateAndTime(), inventoryWeeklyPrediction.getChannelId().getChannelid());
            if(centralizedInventoryPrediction==null){
                saveNewCentralizedInventoryPredictionByWeeklyPrediction(inventoryWeeklyPrediction);
            }else{
                centralizedInventoryPrediction.setPredictionType(WEEKLY_INVENTORY);
                centralizedInventoryPrediction.setInventory(inventoryWeeklyPrediction.getInventory());
                centralizedInventoryPrediction.setReference(inventoryWeeklyPrediction.getId());
                centralizedInventoryPrediction.setHasUpdated(Boolean.TRUE);
                centralizedInventoryPredictionDAO.save(centralizedInventoryPrediction);
            }
        }
        inventoryPredictionService.updateWeeklyPredictionResultMeta();
    }

    @Override
    public void overrideByMonthlyPrediction() {
        List<InventoryMonthlyPrediction> monthlyPredictionList = inventoryPredictionService.getLatestMonthlyInventory();
        for (InventoryMonthlyPrediction inventoryMonthlyPrediction : monthlyPredictionList) {
            CentralizedInventoryPrediction centralizedInventoryPrediction = centralizedInventoryPredictionDAO.findCentralizedInventoryPrediction(inventoryMonthlyPrediction.getDateAndTime(), inventoryMonthlyPrediction.getChannelId().getChannelid());
            if(centralizedInventoryPrediction==null){
                saveNewCentralizedInventoryPredictionByMonthlyPrediction(inventoryMonthlyPrediction);
            }else{
                centralizedInventoryPrediction.setPredictionType(MONTHLY_INVENTORY);
                centralizedInventoryPrediction.setInventory(inventoryMonthlyPrediction.getInventory());
                centralizedInventoryPrediction.setReference(inventoryMonthlyPrediction.getId());
                centralizedInventoryPrediction.setHasUpdated(Boolean.TRUE);
                centralizedInventoryPredictionDAO.save(centralizedInventoryPrediction);
            }
        }
        inventoryPredictionService.updateMonthlylyPredictionResultMeta();
    }

    @Transactional
    @Override
    public List<CentralizedInventoryPrediction> findUpdatedInventory() {
        return centralizedInventoryPredictionDAO.findUpdatedInventory();
    }

    @Transactional
    @Override
    public List<CentralizedInventoryPrediction> findByDateAndChannel(Date date, Integer channelId){
        return centralizedInventoryPredictionDAO.findByDateAndChannel(date, channelId);
    }

    private CentralizedInventoryPrediction saveNewCentralizedInventoryPredictionByWeeklyPrediction(InventoryWeeklyPrediction weeklyPrediction) {
        CentralizedInventoryPrediction cip = new CentralizedInventoryPrediction();
        cip.setDateAndTime(weeklyPrediction.getDateAndTime());
        cip.setDate(weeklyPrediction.getDate());
        cip.setFromTime(weeklyPrediction.getFromTime());
        cip.setToTime(weeklyPrediction.getToTime());
        cip.setInventory(weeklyPrediction.getInventory());
        cip.setChannelDetails(weeklyPrediction.getChannelId());
        cip.setPredictionType(WEEKLY_INVENTORY);
        cip.setReference(weeklyPrediction.getId());
        cip.setHasUpdated(Boolean.TRUE);
        centralizedInventoryPredictionDAO.save(cip);
        return cip;
    }

    private CentralizedInventoryPrediction setCentralizedInventoryByDailyPredictions(InventoryDailyPrediction dailyPrediction, CentralizedInventoryPrediction centralizedPrediction){
        centralizedPrediction.setPredictionType(DAILY_INVENTORY);
        centralizedPrediction.setDate(dailyPrediction.getDate());
        centralizedPrediction.setDateAndTime(dailyPrediction.getDateAndTime());
        centralizedPrediction.setFromTime(dailyPrediction.getFromTime());
        centralizedPrediction.setToTime(dailyPrediction.getToTime());
        centralizedPrediction.setChannelDetails(dailyPrediction.getChannelId());
        centralizedPrediction.setInventory(dailyPrediction.getInventory());
        centralizedPrediction.setReference(dailyPrediction.getId());
        centralizedPrediction.setHasUpdated(Boolean.TRUE);
        return centralizedPrediction;
    }

    private CentralizedInventoryPrediction saveNewCentralizedInventoryPredictionByMonthlyPrediction(InventoryMonthlyPrediction monthlyPrediction) {
        CentralizedInventoryPrediction cip = new CentralizedInventoryPrediction();
        cip.setDateAndTime(monthlyPrediction.getDateAndTime());
        cip.setDate(monthlyPrediction.getDate());
        cip.setFromTime(monthlyPrediction.getFromTime());
        cip.setToTime(monthlyPrediction.getToTime());
        cip.setInventory(monthlyPrediction.getInventory());
        cip.setChannelDetails(monthlyPrediction.getChannelId());
        cip.setPredictionType(MONTHLY_INVENTORY);
        cip.setReference(monthlyPrediction.getId());
        cip.setHasUpdated(Boolean.TRUE);
        centralizedInventoryPredictionDAO.save(cip);
        return cip;
    }
}
