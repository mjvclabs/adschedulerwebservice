package com.vclabs.nash.service.inventoryprediction.impl;

import com.vclabs.nash.controller.inventory.InventoryWebService;
import com.vclabs.nash.controller.inventory.dto.InventoryContainer;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryConsumption;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.model.entity.inventory.NashInventoryConsumption;
import com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction;
import com.vclabs.nash.service.inventory.InventoryConsumptionService;
import com.vclabs.nash.service.inventory.InventoryService;
import com.vclabs.nash.service.inventory.NashInventoryConsumptionService;
import com.vclabs.nash.service.inventoryprediction.CentralizedInventoryPredictionService;
import com.vclabs.nash.service.inventoryprediction.InventorySyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-10-15.
 */

@Service
public class InventorySyncServiceImpl implements InventorySyncService {

    @Autowired
    private CentralizedInventoryPredictionService centralizedInventoryPredictionService;
    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private InventoryWebService inventoryWebService;
    @Autowired
    private InventoryConsumptionService inventoryConsumptionService;
    @Autowired
    private NashInventoryConsumptionService nashInventoryConsumptionService;

    @Override
    @Transactional
    public void syncPredictionInventoryWithSystemInventory() {
        List<CentralizedInventoryPrediction> predictionList = centralizedInventoryPredictionService.findUpdatedInventory();
        for (CentralizedInventoryPrediction centralizedInventoryPrediction : predictionList) {
            if (dateTimeValidation(centralizedInventoryPrediction.getDateAndTime())) {
                InventoryRow inventoryRow = inventoryService.findByChannelAndDateAndTime(centralizedInventoryPrediction.getChannelDetails().getChannelid(), centralizedInventoryPrediction.getDate(), centralizedInventoryPrediction.getFromTime(), centralizedInventoryPrediction.getToTime());
                if (inventoryRow != null) {
                    setUpdatedInventory(inventoryRow,centralizedInventoryPrediction.getInventory());
                    inventoryService.save(inventoryRow);
                } else {
                    InventoryContainer inventoryContainer = inventoryWebService.loadByChannelAndDate(centralizedInventoryPrediction.getChannelDetails().getChannelid(), centralizedInventoryPrediction.getDate());
                    Inventory inventory = inventoryWebService.createInventory(inventoryContainer.getInventory(), centralizedInventoryPrediction.getDate());
                    setWebSpotCalculation(inventory);
                    inventoryService.save(inventory);
                    inventoryRow = inventoryService.findByChannelAndDateAndTime(centralizedInventoryPrediction.getChannelDetails().getChannelid(), centralizedInventoryPrediction.getDate(), centralizedInventoryPrediction.getFromTime(), centralizedInventoryPrediction.getToTime());
                    inventoryRow.setTotalTvcDuration(centralizedInventoryPrediction.getInventory());
                    inventoryService.save(inventoryRow);
                }
            }
            centralizedInventoryPrediction.setHasUpdated(Boolean.FALSE);
            centralizedInventoryPredictionService.save(centralizedInventoryPrediction);
        }
    }

    private Boolean dateTimeValidation(Date date) {
        return new Date().getTime() < date.getTime();
    }

    private void setWebSpotCalculation(Inventory inventory) {
        List<InventoryRow> inventoryRows = inventory.getInventoryRows();
        for (InventoryRow inventoryRow : inventoryRows) {
            inventoryRow.setWebTvcDurationPercentage(50.0);
            if (inventoryRow.getCrawlerSpots() != 0) {
                inventoryRow.setWebCrawlerSpots(inventoryRow.getCrawlerSpots() / 2);
            }
            if (inventoryRow.getLCrawlerSpots() != 0) {
                inventoryRow.setWebLCrawlerSpots(inventoryRow.getLCrawlerSpots() / 2);
            }
            if (inventoryRow.getLogoSpots() != 0) {
                inventoryRow.setWebLogoSpots(inventoryRow.getLogoSpots() / 2);
            }
        }

    }

    private void setUpdatedInventory(InventoryRow inventoryRowId,double newInventory) {
        InventoryConsumption inventoryConsumption = inventoryConsumptionService.findByInventoryRowId(inventoryRowId.getId());
        NashInventoryConsumption nashInventoryConsumption = nashInventoryConsumptionService.findByInventoryRowId(inventoryRowId.getId());
        double nashSpendSpot = 0;
        double opsSpendSpot = 0;
        //get ops spent spot
        if (inventoryConsumption != null) {
            opsSpendSpot = inventoryConsumption.getSpentTime();
        }
        //get nash spent spot
        if (nashInventoryConsumption != null) {
            nashSpendSpot = nashInventoryConsumption.getSpentTime();
        }
        double webInventoryPercentage = inventoryRowId.getWebTvcDurationPercentage();
        int webInventorySpot = (int) (newInventory * (webInventoryPercentage / 100));
        if (nashSpendSpot < webInventorySpot && opsSpendSpot < (newInventory - webInventorySpot)) {
            inventoryRowId.setWebTvcDuration(webInventorySpot);
            inventoryRowId.setTotalTvcDuration(newInventory);
            if (inventoryConsumption != null) {
                inventoryConsumption.setRemainingTime(webInventorySpot - opsSpendSpot);
                inventoryConsumptionService.save(inventoryConsumption);
            }
            if(nashInventoryConsumption != null) {
                nashInventoryConsumption.setRemainingTime((newInventory - webInventorySpot) - nashSpendSpot);
                nashInventoryConsumptionService.save(nashInventoryConsumption);
            }
        }
    }
}
