package com.vclabs.nash.service.inventoryprediction;

/**
 * Created by Nalaka on 2018-10-18.
 */
public interface InventoryPredictionFileService {

    void copyPredictionFiles();

    void readAllInventoryFile();
}
