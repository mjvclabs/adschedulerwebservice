/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.model.dao.ChannelDetailDAO;
import com.vclabs.nash.model.dao.GeneralAuditDAO;
import com.vclabs.nash.model.dao.PriorityDefDAO;
import com.vclabs.nash.model.dao.TimeBeltDAO;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.GeneralAudit;
import com.vclabs.nash.model.entity.PriorityDef;
import com.vclabs.nash.model.entity.ServerDetails;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.model.process.ChannelInfo;
import com.vclabs.nash.model.process.ChannelProperties;
import com.vclabs.nash.model.process.dto.ChannelDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.criteria.CriteriaBuilder;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class ChannelDetailService {

    @Autowired
    private ChannelDetailDAO channelDetailsDao;
    @Autowired
    private TimeBeltDAO timeBeltDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    private PriorityDefDAO priorityDefDao;

    @Autowired
    private ModelMapper modelMapper;

    private static final Logger logger = LoggerFactory.getLogger(ChannelDetailService.class);

    public List<ChannelDetails> AllChannelList() {
        List<ChannelDetails> channelDetails = channelDetailsDao.getAllChannelList();
        Collections.sort(channelDetails);
        return channelDetails;
    }

    public List<ChannelDetails> ChannelList() {
        List<ChannelDetails> channelDetails = channelDetailsDao.getChannelList();
        Collections.sort(channelDetails);
        return channelDetails;
    }

    public List<ChannelDetails> ChannelListOrderByName() { //tested
        List<ChannelDetails> channelDetails = channelDetailsDao.getChannelListOrderByName();
        return channelDetails;
    }

    public ChannelDetails getChannel(int _channelID) {
        return channelDetailsDao.getChannel(_channelID);
    }

    public int saveAndUpdateChannel(String newModel) {
        try {
            ObjectMapper m = new ObjectMapper();
            ChannelInfo dataModel = m.readValue(newModel, ChannelInfo.class);
            ChannelDetails model;

            if (dataModel.getChannelId() != 0) {
                model = channelDetailsDao.getChannel(dataModel.getChannelId());
                if (model == null) {
                    return -1;
                }
                model.setChannelname(dataModel.getLongName());
                model.setShortname(dataModel.getShortName());
                model.setVideoformat(dataModel.getvFormat());
                model.setRemarks(dataModel.getRemarks());

                model.setIsManual(dataModel.isIsManual());
                model.setManualDelay(dataModel.getManualDelay());
                model.setOpsChannelId(dataModel.getOpsId());
                model.setLogoPosition(dataModel.getLogoPosition());

                if (channelDetailsDao.updateChannel(model) && setAuditForChannelDetails(model)) {
                    return -2;
                }
                return -1;

            } else {
                model = new ChannelDetails();
                model.setChannelname(dataModel.getLongName());
                model.setShortname(dataModel.getShortName());
                model.setVideoformat(dataModel.getvFormat());
                model.setRemarks(dataModel.getRemarks());

                model.setIsManual(dataModel.isIsManual());
                model.setManualDelay(dataModel.getManualDelay());
                model.setOpsChannelId(0);
                model.setLogoPosition(dataModel.getLogoPosition());

                model.setLastUpdate(new Date());
                model.setChannelLogoPath("No_path");

                if (channelDetailsDao.saveChannel(model) != -1) {
                    List<TimeBelts> timeBelt = timeBeltDao.getTimeBelts(1);
                    for (TimeBelts time_b : timeBelt) {
                        TimeBelts timemodel = new TimeBelts();
                        timemodel.setChannelID(model.getChannelid());
                        timemodel.setHour(time_b.getHour());
                        timemodel.setPeriod(0);
                        timemodel.setStartTime(time_b.getStartTime());
                        timemodel.setEndTime(time_b.getEndTime());
                        timemodel.setClusterCount(time_b.getClusterCount());
                        timemodel.setIsScheduleGenerated(Boolean.FALSE);
                        timemodel.setScheduleGeneratedTime(time_b.getScheduleGeneratedTime());
                        timemodel.setTotalScheduleTime(time_b.getTotalScheduleTime());
                        timeBeltDao.saveTimeBelt(timemodel);
                        for (int j = 0; j < time_b.getClusterCount(); j++) {
                            for (int k = 1; k <= 3; k++) {
                                if (timemodel.getTimeBeltId() != null) {
                                    PriorityDef prioriry = new PriorityDef();
                                    prioriry.setCluster(j);
                                    prioriry.setPriority(k);
                                    prioriry.setTimebeltId(timemodel);
                                    priorityDefDao.savePriorityDef(prioriry);
                                }
                            }
                        }
                    }

                    return model.getChannelid();
                } else {
                    return -1;
                }
            }
        } catch (Exception e) {
            logger.debug("Exception in ChannelDetailService in saveAndUpdateChannel() : {}", e.getMessage());
            return -1;
        }

    }

    public boolean updateChannelProperties(String newData) {
        try {
            ObjectMapper m = new ObjectMapper();
            ChannelProperties dataModel = m.readValue(newData, ChannelProperties.class);

            if (dataModel.getChannelId() != 0) {
                ChannelDetails modelChannel = channelDetailsDao.getChannel(dataModel.getChannelId());
                modelChannel.setIsManual(dataModel.isIsManual());
                modelChannel.setManualDelay(dataModel.getManualDelay());
                modelChannel.setServerDetails(new ServerDetails(dataModel.getServerId()));

                return channelDetailsDao.updateChannel(modelChannel);
            }
        } catch (Exception e) {
            logger.debug("Exception in ChannelDetailService in updateChannelProperties() : {}", e.getMessage());
            return false;
        }
        return false;
    }

    public Boolean setChannelTimeBelt(int channelId) {
        try {
            List<TimeBelts> timeBelt = timeBeltDao.getTimeBelts(1);
            for (TimeBelts time_b : timeBelt) {
                TimeBelts timemodel = new TimeBelts();
                timemodel.setChannelID(channelId);
                timemodel.setHour(time_b.getHour());
                timemodel.setPeriod(time_b.getPeriod());
                timemodel.setStartTime(time_b.getStartTime());
                timemodel.setEndTime(time_b.getEndTime());
                timemodel.setClusterCount(time_b.getClusterCount());
                timemodel.setIsScheduleGenerated(Boolean.FALSE);
                timemodel.setScheduleGeneratedTime(time_b.getScheduleGeneratedTime());
                timemodel.setTotalScheduleTime(time_b.getTotalScheduleTime());
                timeBeltDao.saveTimeBelt(timemodel);
            }
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ChannelDetailService in setChannelTimeBelt() : {}", e.getMessage());
            return true;
        }
    }

    public Boolean setAuditForChannelDetails(ChannelDetails model) {
        try {
            ChannelDetails dataModel = channelDetailsDao.getChannel(model.getChannelid());

            for (Field field : (ChannelDetails.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null && (!field.get(model).equals(field.get(dataModel)))) {
                    GeneralAudit auditModel = new GeneralAudit("channel_detail", model.getChannelid());
                    auditModel.setField(field.getAnnotation(Column.class).name());
                    auditModel.setOldData(field.get(dataModel).toString());
                    auditModel.setNewData(field.get(model).toString());

                    generalAuditDao.saveGeneralAudit(auditModel);
                }
            }
            return true;
        } catch (IllegalAccessException e) {
            logger.debug("Exception in ChannelDetailService in setAuditForChannelDetails() : {}", e.getMessage());
        } catch (Exception e) {
            logger.debug("Exception in ChannelDetailService in setAuditForChannelDetails() : {}", e.getMessage());
            throw e;
        }
        return false;
    }

    public boolean disableChannel(int channelID) {
        try {
            ChannelDetails model = channelDetailsDao.getChannel(channelID);
            model.setEnabled(false);

            if (setAuditForChannelDetails(model) && channelDetailsDao.updateChannel(model)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.debug("Exception in ChannelDetailService in disableChannel() : {}", e.getMessage());
            return false;
        }
    }

    public boolean enableChannel(int channelID) {
        try {
            ChannelDetails model = channelDetailsDao.getChannel(channelID);
            model.setEnabled(true);

            if (setAuditForChannelDetails(model) && channelDetailsDao.updateChannel(model)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.debug("Exception in ChannelDetailService in enableChannel() : {}", e.getMessage());
            return false;
        }
    }

    public List<ChannelDetails> getAllAutoChannels() {
        return channelDetailsDao.getAllAutoChannelList();
    }

    public Integer createNewChannel(String channelName){
        ChannelDetails channel = new ChannelDetails();
        channel.setChannelname(channelName);
        channel.setIsManual(false);
        channel.setLastUpdate(new Date());
        channel.setManualDelay(16);
        channel.setChannelLogoPath("img/Logo/" + channelName + ".png");
        channel.setLogoPosition("0x0");
        channel.setEnabled(true);
        channel.setRemarks("No");
        channel.setShortname(channelName);
        channel.setVideoformat("SD");
        channelDetailsDao.save(channel);
        return channel.getChannelid();
    }

    public List<ChannelDto> getAllChannels(){
        List<ChannelDetails> channelDetails = channelDetailsDao.getAllChannelList();
        List<ChannelDto> dtoList = new ArrayList<>();
        Collections.sort(channelDetails);
        for (ChannelDetails channel : channelDetails){
            ChannelDto channelDto = modelMapper.map(channel, ChannelDto.class);
            dtoList.add(channelDto);
        }
        return dtoList;
    }

    public List<ChannelDetails> getAllEnabledChannelList(){
        return channelDetailsDao.getAllEnabledChannelList();
    }
}
