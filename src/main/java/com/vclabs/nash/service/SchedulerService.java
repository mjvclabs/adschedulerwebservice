/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.nio.file.Path;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.hibernate.internal.util.io.StreamCopier.BUFFER_SIZE;

import com.vclabs.nash.dashboard.dto.DailyMissedSpotsDto;
import com.vclabs.nash.dashboard.dto.HightTrafficChannelsWidgetDto;
import com.vclabs.nash.dashboard.service.ChannelUtillizationWidgetService;
import com.vclabs.nash.model.dto.MissedSpotMapDto;
import com.vclabs.nash.model.dto.RescheduleTimeBelt;
import com.vclabs.nash.model.dto.TimeSlotScheduleMapDto;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.model.entity.inventory.NashInventoryConsumption;
import com.vclabs.nash.service.async.ScheduleAsyncService;
import com.vclabs.nash.service.inventory.InventoryService;
import com.vclabs.nash.service.inventory.NashInventoryConsumptionService;

import com.vclabs.nash.service.playlist_preview.vo.DailySchedulePreview;
import com.vclabs.nash.service.utill.PLHistorySortByAiredTime;
import com.vclabs.nash.service.utill.PLSortByAiredTime;
import com.vclabs.nash.service.vo.PlaylistInputData;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.vclabs.nash.model.dao.Advertisement.AdvertisementDAO;
import com.vclabs.nash.model.dao.ChannelDetailDAO;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.dao.GeneralAuditDAO;
import com.vclabs.nash.model.dao.OrderAdvertListDAO;
import com.vclabs.nash.model.dao.PlayHistoryDAO;
import com.vclabs.nash.model.dao.PlayListScheduleDAO;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dao.TimeBeltDAO;
import com.vclabs.nash.model.dao.WorkOrderChannelListDAO;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.GeneralAudit;
import com.vclabs.nash.model.entity.OrderAdvertList;
import com.vclabs.nash.model.entity.PlayList;
import com.vclabs.nash.model.entity.PlayListHistory;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.TVCDurationSpotCount;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.model.entity.TimeSlot;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import com.vclabs.nash.model.process.CopySchedulesInfo;
import com.vclabs.nash.model.process.ExcelGenerater;
import com.vclabs.nash.model.process.MissedSpotItemModel;
import com.vclabs.nash.model.process.OneDayScheduleInfo;
import com.vclabs.nash.model.process.RemainingSpotModel;
import com.vclabs.nash.model.process.ReplaceAdvertData;
import com.vclabs.nash.model.process.ScheduleAdminViewInfo;
import com.vclabs.nash.model.process.ScheduleInfo;
import com.vclabs.nash.model.process.ScheduleItemModel;
import com.vclabs.nash.model.process.ScheduleModelPro;
import com.vclabs.nash.model.process.SchedulePatternInfo;
import com.vclabs.nash.model.process.WorkOrderSpot;
import com.vclabs.nash.model.process.PriorityMap;
import com.vclabs.nash.model.view.AdvertisementDefView;
import com.vclabs.nash.model.view.Message;
import com.vclabs.nash.model.view.MissedSpotView;
import com.vclabs.nash.model.view.OnedayScheduleView;
import com.vclabs.nash.model.view.PreScheduleInsertView;
import com.vclabs.nash.model.view.PreScheduleReplaceView;
import com.vclabs.nash.model.view.RemainingSpotView;
import com.vclabs.nash.model.view.ScheduleAdminView;
import com.vclabs.nash.model.view.ScheduleAdvertView;
import com.vclabs.nash.model.view.ScheduleUpdateView;
import com.vclabs.nash.model.view.ScheduleWebView;
import com.vclabs.nash.model.view.TimeBeltUtilization;
import com.vclabs.nash.model.view.TimeBeltUtilizationItem;
import com.vclabs.nash.model.view.WorkOrderChannelScheduleView;
import com.vclabs.nash.model.view.WorkOrderScheduleView;

/**
 *
 * @author user
 */
@Service
@Transactional("main")
public class SchedulerService extends DateFormat {

    private static final org.slf4j.Logger logger4g = LoggerFactory.getLogger(SchedulerService.class);
    @Autowired
    private SchedulerDAO schedulerDao;
    @Autowired
    private WorkOrderDAO workOrderDao;
    @Autowired
    private PlayListScheduleDAO playListScheduleDao;
    @Autowired
    private WorkOrderChannelListDAO workOrderChannelListDao;
    @Autowired
    private TimeBeltDAO timeBeltDao;
    @Autowired
    private GeneralAuditDAO generalAuditDao;
    @Autowired
    private ChannelDetailDAO channelDetailDAO;
    @Autowired
    private PlayHistoryDAO playHistoryDao;
    @Autowired
    private AdvertisementDAO advertisementDao;
    @Autowired
    private OrderAdvertListDAO orderAdvertListDao;
    @Autowired
    private ClientDetailDAO clientDetailDao;
    @Autowired
    private WorkOrderService workOrderService;
    @Autowired
    private SpotSpreadService spotSpreadService;
    @Autowired
    private WorkOrderStatusCheckService workOrderStatusCheckService;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private NashInventoryConsumptionService nashInventoryConsumptionService;
    @Autowired
    private ChannelUtillizationWidgetService channelUtillizationWidgetService;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SchedulerService.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Lazy
    @Autowired
    private ScheduleAsyncService scheduleAsyncService;

    @Async("dailyTaskExecutor")
    public void clearAndCreatePlayListAsync() {
        clearAndCreatePlayList();
    }

    @Transactional
    public void clearAndCreatePlayList() {
        Date today = new Date();
        logger4g.debug("Started clear & create play list cron job | (cron = 0 43 23 1/1 * ?) | ( current time : {} )", today.toString());
        if (clearPlaylist()) {
            playListGenerateCaller(today);
            List<ChannelDetails> allChannel = channelDetailDAO.getChannelList();
            for (int i = 0; i < allChannel.size(); i++) {
                refreshTimeBelt(allChannel.get(i).getChannelid());
            }
        }
        logger4g.debug("Completed clear & create play list cron job | (cron = 0 43 23 1/1 * ?) | ( current time : {} )", new Date().toString());
    }

    public List<ScheduleWebView> getScheduleList() {

        List<ScheduleWebView> schedulerList = new ArrayList<>();

        try {
            List<ScheduleDef> datalist = schedulerDao.getScheduleList();

            for (ScheduleDef datalist1 : datalist) {
                ScheduleWebView model = new ScheduleWebView();
                model.setScheduleid(datalist1.getScheduleid());
                model.setChannelid(datalist1.getChannelid().getChannelid());
                model.setAdvertid(datalist1.getAdvertid().getAdvertid());
                model.setWorkorderid(datalist1.getWorkorderid().getWorkorderid());
                model.setDate(datalist1.getDate());
                model.setSchedulestarttime(datalist1.getSchedulestarttime());
                model.setScheduleendtime(datalist1.getScheduleendtime());
                model.setStatus(datalist1.getStatus());
                schedulerList.add(model);
            }

        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to get Schedule List: ", e.getMessage());
        }
        return schedulerList;
    }

    public List<ScheduleUpdateView> getScheduleListOrderByASC(int workOrderId) { //tested

        List<ScheduleUpdateView> schedulerList = new ArrayList<>();
        ScheduleModelPro temModel = new ScheduleModelPro();
        ScheduleUpdateView updateModel = new ScheduleUpdateView();

        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date toDay = dateFormat.parse(dateFormat.format(new Date()));

            List<ScheduleDef> datalist = schedulerDao.getScheduleListOrderByASC(workOrderId);

            for (ScheduleDef dataModel : datalist) {

                if (temModel.getChannelId() == dataModel.getChannelid().getChannelid()) {
                    if ((temModel.getScheduledStartDate().compareTo(dataModel.getSchedulestarttime()) == 0) && (temModel.getScheduledEndDate().compareTo(dataModel.getScheduleendtime()) == 0)) {
                        if (temModel.getAdvertId() == dataModel.getAdvertid().getAdvertid()) {
                            if ((temModel.getInitiateDate()).compareTo(dataModel.getDate()) == 0) {

                                for (ScheduleItemModel scheduleItem : updateModel.getScheduleItem()) {
                                    if (scheduleItem.getDate().equals(dataModel.getDate().toString())) {

                                        if (toDay.after(dataModel.getDate())) {
                                            scheduleItem.setExpire(1);
                                        }
                                        scheduleItem.addOne();
                                        scheduleItem.getScheduleid().add(dataModel.getScheduleid());
                                        if(dataModel.getPriority() != null){
                                            scheduleItem.getPriorityIds().add(new PriorityMap(dataModel.getScheduleid(),dataModel.getPriority().getPriorityId()));
                                        }else{
                                            scheduleItem.getPriorityIds().add(new PriorityMap(dataModel.getScheduleid(),0));
                                        }
                                    }
                                }
                            } else {
                                temModel.setInitiateDate(dataModel.getDate());
                                for (ScheduleItemModel scheduleItem : updateModel.getScheduleItem()) {
                                    if (scheduleItem.getDate().equals(dataModel.getDate().toString())) {
                                        scheduleItem.addOne();
                                        scheduleItem.getScheduleid().add(dataModel.getScheduleid());
                                        if(dataModel.getPriority() != null){
                                            scheduleItem.getPriorityIds().add(new PriorityMap(dataModel.getScheduleid(),dataModel.getPriority().getPriorityId()));
                                        }else{
                                            scheduleItem.getPriorityIds().add(new PriorityMap(dataModel.getScheduleid(),0));
                                        }
                                    }
                                }
                            }
                        } else {
                            schedulerList.add(updateModel);
                            temModel = createSchedulePro(dataModel);
                            updateModel = createScheduleUpdateView(dataModel);
                        }
                    } else {
                        schedulerList.add(updateModel);
                        temModel = createSchedulePro(dataModel);
                        updateModel = createScheduleUpdateView(dataModel);
                    }

                } else {
                    if (updateModel.getChannelid() != -99) {
                        schedulerList.add(updateModel);
                    }
                    temModel = createSchedulePro(dataModel);
                    updateModel = createScheduleUpdateView(dataModel);
                }
            }
//            if(!updateModel.getAdvertCount().isEmpty()){
            schedulerList.add(updateModel);
//            }

        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to getScheduleListOrderByASC: ", e.getMessage());
        }
        return schedulerList;
    }

    //tested
    /// get data for create and update  current schedule page ////////////////refactored
    public WorkOrderScheduleView getWOScheduleViewData(int workOrderId, int selectedChannelId, Boolean withUtolizData) {

       // long time1 = System.currentTimeMillis();
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();

        List<ScheduleUpdateView> lstSchedule = this.getScheduleListOrderByASC(workOrderId);
        WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(workOrderId, selectedChannelId);
     //   long time2 = System.currentTimeMillis();

    //    System.currentTimeMillis();
        List<TimeBeltUtilization> lstUtilization = null;
        if (withUtolizData) {
            lstUtilization = this.getScheduleSlotUtilization(workOrderId);
        }

        retObject.setTimebeltSchedules(lstSchedule);
        retObject.setWoChannelSpotsCount(woChannelSpotsCount);
        retObject.setWoTimeBeltUtilization(lstUtilization);
      //  long time3 = System.currentTimeMillis();

//        LOGGER.debug("Time Belt Utilization diff: {} ms", (time3 - time2));
//        LOGGER.debug("All view data time diff: {} ms", (time3 - time1));
        return retObject;
    }

    //This function will be used to parallel processing when requesting schedule view
    @Async("billingServiceExecutor")
    public CompletableFuture<Boolean> getWOScheduleViewDataF1(WorkOrderScheduleView retObject, int workOrderId, int selectedChannelId) {
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        try{
            completableFuture.complete(getWOScheduleViewDataSF1(retObject, workOrderId, selectedChannelId));
        }catch (Exception e){
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

    //This function will be used to parallel processing when requesting schedule view
    @Async("billingServiceExecutor")
    public CompletableFuture<Boolean> getWOScheduleViewDataF2(WorkOrderScheduleView retObject, int workOrderId) {
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        try{
            completableFuture.complete(getWOScheduleViewDataSF2(retObject, workOrderId));
        }catch (Exception e){
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

    private boolean getWOScheduleViewDataSF1(WorkOrderScheduleView retObject, int workOrderId, int selectedChannelId) {
        WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(workOrderId, selectedChannelId);
        List<ScheduleUpdateView> lstSchedule = this.getScheduleListOrderByASC(workOrderId);
        retObject.setWoChannelSpotsCount(woChannelSpotsCount);
        retObject.setTimebeltSchedules(lstSchedule);
        return true;
    }

    private boolean getWOScheduleViewDataSF2(WorkOrderScheduleView retObject, int workOrderId) {
        List<TimeBeltUtilization> lstUtilization = this.getScheduleSlotUtilization(workOrderId);
        retObject.setWoTimeBeltUtilization(lstUtilization);
        return true;
    }

    @Deprecated
    public List<MissedSpotView> getMissedSpot(String fromDate, String toDate, String channelIds, int advertId, int[] clientIDs, String hour) {

//        try {
//            String[] channelIdArry = channelIds.split("_");
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
//            Date fromDay = dateFormat.parse(fromDate);
//            Date toDay = dateFormat.parse(toDate);
//            long start = System.currentTimeMillis();
//
//            List<ScheduleDef> scheduleList = schedulerDao.getMissedSpot(fromDay, toDay, -111, advertId);
//            long finish = System.currentTimeMillis();
//            long timeElapsed = finish - start;
//            System.out.println("Query time:"+timeElapsed);
//            for (int j = 0; j < scheduleList.size(); j++) {
//                if (scheduleList.get(j).getStatus().equals("9") && scheduleList.get(j).getComment().equals("Advert expire")) {
//                    scheduleList.remove(scheduleList.get(j));
//                    j--;
//                } else {
//                    if (channelIdArry.length != 0 && !channelIdArry[1].equals("-111")) {
//                        Boolean invalidSpot = true;
//                        for (int i = 1; i < channelIdArry.length; i++) {
//                            if (scheduleList.get(j).getChannelid().getChannelid().toString().equals(channelIdArry[i])) {
//                                invalidSpot = false;
//                            }
//                        }
//                        if (invalidSpot) {
//                            scheduleList.remove(scheduleList.get(j));
//                            j--;
//                        }
//                    }
//
//                }
//            }
//            for (int j = 0; j < scheduleList.size(); j++) {
//                if (clientIDs.length != 0 && clientIDs[0] != -999) {
//                    Boolean invalidSpot = true;
//                    for (int i = 0; i < clientIDs.length; i++) {
//                        if (scheduleList.get(j).getWorkorderid().getClient() == clientIDs[i]) {
//                            invalidSpot = false;
//                        }
//                    }
//                    if (invalidSpot) {
//                        scheduleList.remove(scheduleList.get(j));
//                        j--;
//                    }
//                }
//            }
//            long finish2 = System.currentTimeMillis();
//            System.out.println("Remove 9 Status:"+(finish2-finish));
//            List<MissedSpotView> viewList = new ArrayList<>();
//            MissedSpotView model = new MissedSpotView();
//
//            for (int i = 0; i < scheduleList.size(); i++) {
//                String timeBelt = scheduleList.get(i).getSchedulestarttime().toString() + "-" + scheduleList.get(i).getScheduleendtime().toString();
//                String startTime = scheduleList.get(i).getActualstarttime().toString();
//                startTime = startTime.substring(0, 2);
//                if (hour.equals("all") || hour.equals(startTime)) {
//                    TimeBelts currnentBelt = timeBeltDao.getTimeBelt(scheduleList.get(i).getChannelid().getChannelid(), Integer.parseInt(startTime), 0);
//                    String actualTime = timeFormat.format(currnentBelt.getStartTime()) + "-" + timeFormat.format(currnentBelt.getEndTime());
//                    if (timeBelt.equals(model.getTimeBelt())) {
//
//                        MissedSpotItemModel item = new MissedSpotItemModel();
//                        item.setScheduleId(scheduleList.get(i).getScheduleid());
//                        item.setActualTimeBelt(actualTime);
//                        item.setSchedulDate(scheduleList.get(i).getDate().toString());
//                        item.setStatus(scheduleList.get(i).getStatus());
//                        item.setSchedulStartTime(scheduleList.get(i).getSchedulestarttime().toString());
//                        item.setSchedulEndTime(scheduleList.get(i).getScheduleendtime().toString());
//
//                        item.setWorkOrderId(scheduleList.get(i).getWorkorderid().getWorkorderid());
//                        item.setWorkOrderName(scheduleList.get(i).getWorkorderid().getOrdername());
//                        item.setWorkOrderEndDate(dateFormat.format(scheduleList.get(i).getWorkorderid().getEnddate()));
//                        item.setScheduleEndDate(scheduleList.get(i).getWorkorderid().getEnddate());
//                        ClientDetails client = clientDetailDao.getSelectedClient(scheduleList.get(i).getWorkorderid().getClient());
//                        item.setClient(client.getClientname());
//                        item.setClientId(client.getClientid());
//                        if (scheduleList.get(i).getPriority() != null) {
//                            item.setPriority("C" + scheduleList.get(i).getPriority().getCluster() + " / P" + scheduleList.get(i).getPriority().getPriority());
//                        }
//                        item.setAdvertNme(scheduleList.get(i).getAdvertid().getAdvertname());
//                        item.setDuratuion(scheduleList.get(i).getAdvertid().getDuration());
//                        item.setAdvertId(scheduleList.get(i).getAdvertid().getAdvertid());
//                        item.setAdvertType(scheduleList.get(i).getAdvertid().getAdverttype());
//
//                        item.setChannelName(scheduleList.get(i).getChannelid().getShortname());
//                        item.setChannelId(scheduleList.get(i).getChannelid().getChannelid());
//
//                        model.getItemList().add(item);
//                    } else {
//                        if (!model.getTimeBelt().equals("No_time")) {
//                            viewList.add(model);
//                        }
//                        model = new MissedSpotView();
//                        model.setTimeBelt(timeBelt);
//
//                        MissedSpotItemModel item = new MissedSpotItemModel();
//                        item.setScheduleId(scheduleList.get(i).getScheduleid());
//                        item.setActualTimeBelt(actualTime);
//                        item.setSchedulDate(scheduleList.get(i).getDate().toString());
//                        item.setStatus(scheduleList.get(i).getStatus());
//                        item.setSchedulStartTime(scheduleList.get(i).getSchedulestarttime().toString());
//                        item.setSchedulEndTime(scheduleList.get(i).getScheduleendtime().toString());
//
//                        item.setScheduleEndDate(scheduleList.get(i).getWorkorderid().getEnddate());
//                        item.setWorkOrderId(scheduleList.get(i).getWorkorderid().getWorkorderid());
//                        item.setWorkOrderName(scheduleList.get(i).getWorkorderid().getOrdername());
//                        item.setWorkOrderEndDate(dateFormat.format(scheduleList.get(i).getWorkorderid().getEnddate()));
//                        ClientDetails client = clientDetailDao.getSelectedClient(scheduleList.get(i).getWorkorderid().getClient());
//                        item.setClient(client.getClientname());
//                        item.setClientId(client.getClientid());
//                        if (scheduleList.get(i).getPriority() != null) {
//                            item.setPriority("C" + scheduleList.get(i).getPriority().getCluster() + " / P" + scheduleList.get(i).getPriority().getPriority());
//                        }
//
//                        item.setAdvertNme(scheduleList.get(i).getAdvertid().getAdvertname());
//                        item.setDuratuion(scheduleList.get(i).getAdvertid().getDuration());
//                        item.setAdvertId(scheduleList.get(i).getAdvertid().getAdvertid());
//                        item.setAdvertType(scheduleList.get(i).getAdvertid().getAdverttype());
//
//                        item.setChannelName(scheduleList.get(i).getChannelid().getShortname());
//                        item.setChannelId(scheduleList.get(i).getChannelid().getChannelid());
//
//                        model.getItemList().add(item);
//                    }
//                }
//
//            }
//            if (!model.getTimeBelt().equals("No_time")) {
//                viewList.add(model);
//            }
//            long finish3 = System.currentTimeMillis();
//            System.out.println("View data proccess: "+(finish3-finish2));
            return getMissedSpot_v2(fromDate,toDate, channelIds, advertId,clientIDs, hour);
//            return viewList;
//        } catch (Exception e) {
//            LOGGER.debug("Exception in SchedulerService in getMissedSpot: ", e.getMessage());
//            return null;
//        }
    }

    @Transactional
    public List<MissedSpotView> getMissedSpot_v2(String fromDate, String toDate, String channelIds, int advertId, int[] clientIDs, String hour) {

        String[] channelIdArry = channelIds.split("_");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        Date fromDay = new Date();
        Date toDay = new Date();
        try {
            fromDay = dateFormat.parse(fromDate);
            toDay = dateFormat.parse(toDate);
        } catch (ParseException e) {

        }
        long start = System.currentTimeMillis();

        List<MissedSpotMapDto> scheduleList = schedulerDao.getMissedSpotBySQL(fromDay, toDay, -111, advertId);
        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("Query time:" + timeElapsed);
        for (int j = 0; j < scheduleList.size(); j++) {
            if(scheduleList.get(j).getStatus().equals("11") &&missedViewSpotValidation(scheduleList.get(j))) {
                scheduleList.remove(scheduleList.get(j));
                j--;
            }else if (scheduleList.get(j).getStatus().equals("9") && scheduleList.get(j).getComment().equals("Advert expire")) {
                scheduleList.remove(scheduleList.get(j));
                j--;
            } else {
                if (channelIdArry.length != 0 && !channelIdArry[1].equals("-111")) {
                    Boolean invalidSpot = true;
                    for (int i = 1; i < channelIdArry.length; i++) {
                        if ((scheduleList.get(j).getChannelId() + "").equals(channelIdArry[i])) {
                            invalidSpot = false;
                        }
                    }
                    if (invalidSpot) {
                        scheduleList.remove(scheduleList.get(j));
                        j--;
                    }
                }

            }
        }
        if (clientIDs.length != 0 && clientIDs[0] != -999) {
            for (int j = 0; j < scheduleList.size(); j++) {

                Boolean invalidSpot = true;
                for (int i = 0; i < clientIDs.length; i++) {
                    if (scheduleList.get(j).getClientId() == clientIDs[i]) {
                        invalidSpot = false;
                    }
                }
                if (invalidSpot) {
                    scheduleList.remove(scheduleList.get(j));
                    j--;
                }
            }
        }
        long finish2 = System.currentTimeMillis();
        System.out.println("Remove 9 Status:" + (finish2 - finish));
        List<MissedSpotView> viewList = new ArrayList<>();
        MissedSpotView model = new MissedSpotView();

        try {
            for (int i = 0; i < scheduleList.size(); i++) {
                String timeBelt = scheduleList.get(i).getScheduleStartTime().toString() + "-" + scheduleList.get(i).getSchedulEndTime().toString();
                String startTime = scheduleList.get(i).getScheduleActualStartTime().toString();
                startTime = startTime.substring(0, 2);
                if (hour.equals("all") || hour.equals(startTime)) {
                    String actualTime = timeFormat.format(scheduleList.get(i).getActualStartTime()) + "-" + timeFormat.format(scheduleList.get(i).getActualEndTime());
                    if (timeBelt.equals(model.getTimeBelt())) {
                        model.getItemList().add(setMissedSpotItemData(scheduleList.get(i), actualTime));
                    } else {
                        if (!model.getTimeBelt().equals("No_time")) {
                            viewList.add(model);
                        }
                        model = new MissedSpotView();
                        model.setTimeBelt(timeBelt);
                        model.getItemList().add(setMissedSpotItemData(scheduleList.get(i), actualTime));
                    }
                }

            }
        } catch (Exception e) {

        }
        if (!model.getTimeBelt().equals("No_time")) {
            viewList.add(model);
        }
        long finish3 = System.currentTimeMillis();
        System.out.println("View data proccess: " + (finish3 - finish2));
        return viewList;
    }

    //Validate spot date and time
    public Boolean missedViewSpotValidation(MissedSpotMapDto model) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            Date currentTime = timeFormat.parse(timeFormat.format(new Date()));
            Date date = dateFormat.parse(dateFormat.format(model.getDate()));

           /* if (toDay.before(date)) {
                return false;
            } else */
            if (toDay.equals(date)) {
                String timeString = timeFormat.format(model.getScheduleStartTime());
                timeString = timeString.replace("30", "00");
                Date time = timeFormat.parse(timeString);
                if (currentTime.before(time)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in scheduleSpotValidation: ", e.getMessage());
            return false;
        }
    }

    private MissedSpotItemModel setMissedSpotItemData(MissedSpotMapDto missedSpotmap, String actualTime){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        MissedSpotItemModel item = new MissedSpotItemModel();
        item.setScheduleId(missedSpotmap.getScheduleId());
        item.setActualTimeBelt(actualTime);
        item.setSchedulDate(missedSpotmap.getDate().toString());
        item.setStatus(missedSpotmap.getStatus());
        item.setSchedulStartTime(missedSpotmap.getScheduleStartTime().toString());
        item.setSchedulEndTime(missedSpotmap.getSchedulEndTime().toString());

        item.setWorkOrderId(missedSpotmap.getWorkOrderId());
        item.setWorkOrderName(missedSpotmap.getWorkOrderName());
        item.setWorkOrderEndDate(dateFormat.format(missedSpotmap.getWorkOrderEndDate()));
        item.setScheduleEndDate(missedSpotmap.getWorkOrderEndDate());
        item.setClient(missedSpotmap.getClientName());
        item.setClientId(missedSpotmap.getClientId());
        if (missedSpotmap.getPriorityId() != -1) {
            item.setPriority("C" + missedSpotmap.getPriorityCluster() + " / P" + missedSpotmap.getPriorityNumber());
        }
        item.setAdvertNme(missedSpotmap.getAdvertName());
        item.setDuratuion(missedSpotmap.getDuration());
        item.setAdvertId(missedSpotmap.getAdvertId());
        item.setAdvertType(missedSpotmap.getAdvertType());

        item.setChannelName(missedSpotmap.getChannelName());
        item.setChannelId(missedSpotmap.getChannelId());

        return item;
    }

    public List<TimeSlot> getTimeSlotList() {
        return schedulerDao.getTimeSlot();
    }

    public List<AdvertisementDefView> getSelectedAdvert(int workOrderId, String advertType) { //tested
        List<AdvertisementDefView> viewAdvertList = new ArrayList<>();
        try {

            List<OrderAdvertList> advertList = orderAdvertListDao.getOrderAdvertList(workOrderId);

            if (!advertType.equals("All")) {
                switch (advertType) {
                    case "TVC":
                        for (OrderAdvertList model : advertList) {

                            if (model.getAdvertid().getAdverttype().equals("ADVERT")) {
                                AdvertisementDefView advertModel = new AdvertisementDefView();
                                advertModel.setAdvertId(model.getAdvertid().getAdvertid());
                                advertModel.setAdvertName(model.getAdvertid().getAdvertname());
                                advertModel.setDuration(model.getAdvertid().getDuration());
                                advertModel.setLanguage(model.getAdvertid().getLanguage());
                                advertModel.setType(model.getAdvertid().getAdverttype());
                                advertModel.setEnabled(model.getAdvertid().getEnabled());
                                advertModel.setLastModifydate(model.getAdvertid().getLastModifydate());
                                advertModel.setIsFileAvailable(model.getAdvertid().isFileAvailable());
                                advertModel.setStatus(model.getAdvertid().getStatus());
                                viewAdvertList.add(advertModel);
                            }
                        }
                        break;

                    case "LOGO":
                        for (OrderAdvertList model : advertList) {

                            if (model.getAdvertid().getAdverttype().equals("LOGO")) {
                                AdvertisementDefView advertModel = new AdvertisementDefView();
                                advertModel.setAdvertId(model.getAdvertid().getAdvertid());
                                advertModel.setAdvertName(model.getAdvertid().getAdvertname());
                                advertModel.setDuration(model.getAdvertid().getDuration());
                                advertModel.setLanguage(model.getAdvertid().getLanguage());
                                advertModel.setType(model.getAdvertid().getAdverttype());
                                advertModel.setEnabled(model.getAdvertid().getEnabled());
                                advertModel.setLastModifydate(model.getAdvertid().getLastModifydate());
                                advertModel.setIsFileAvailable(model.getAdvertid().isFileAvailable());
                                advertModel.setStatus(model.getAdvertid().getStatus());
                                viewAdvertList.add(advertModel);
                            }
                        }
                        break;

                    case "CRAWLER":
                        for (OrderAdvertList model : advertList) {

                            if (model.getAdvertid().getAdverttype().equals("CRAWLER")) {
                                AdvertisementDefView advertModel = new AdvertisementDefView();
                                advertModel.setAdvertId(model.getAdvertid().getAdvertid());
                                advertModel.setAdvertName(model.getAdvertid().getAdvertname());
                                advertModel.setDuration(model.getAdvertid().getDuration());
                                advertModel.setLanguage(model.getAdvertid().getLanguage());
                                advertModel.setType(model.getAdvertid().getAdverttype());
                                advertModel.setEnabled(model.getAdvertid().getEnabled());
                                advertModel.setLastModifydate(model.getAdvertid().getLastModifydate());
                                advertModel.setIsFileAvailable(model.getAdvertid().isFileAvailable());
                                advertModel.setStatus(model.getAdvertid().getStatus());
                                viewAdvertList.add(advertModel);
                            }
                        }
                        break;

                    case "V_SHAPE":
                        for (OrderAdvertList model : advertList) {

                            if (model.getAdvertid().getAdverttype().equals("V_SHAPE")) {
                                AdvertisementDefView advertModel = new AdvertisementDefView();
                                advertModel.setAdvertId(model.getAdvertid().getAdvertid());
                                advertModel.setAdvertName(model.getAdvertid().getAdvertname());
                                advertModel.setDuration(model.getAdvertid().getDuration());
                                advertModel.setLanguage(model.getAdvertid().getLanguage());
                                advertModel.setType(model.getAdvertid().getAdverttype());
                                advertModel.setEnabled(model.getAdvertid().getEnabled());
                                advertModel.setLastModifydate(model.getAdvertid().getLastModifydate());
                                advertModel.setIsFileAvailable(model.getAdvertid().isFileAvailable());
                                advertModel.setStatus(model.getAdvertid().getStatus());
                                viewAdvertList.add(advertModel);
                            }
                        }
                        break;

                    case "L_SHAPE":
                        for (OrderAdvertList model : advertList) {

                            if (model.getAdvertid().getAdverttype().equals("L_SHAPE")) {
                                AdvertisementDefView advertModel = new AdvertisementDefView();
                                advertModel.setAdvertId(model.getAdvertid().getAdvertid());
                                advertModel.setAdvertName(model.getAdvertid().getAdvertname());
                                advertModel.setDuration(model.getAdvertid().getDuration());
                                advertModel.setLanguage(model.getAdvertid().getLanguage());
                                advertModel.setType(model.getAdvertid().getAdverttype());
                                advertModel.setEnabled(model.getAdvertid().getEnabled());
                                advertModel.setLastModifydate(model.getAdvertid().getLastModifydate());
                                advertModel.setIsFileAvailable(model.getAdvertid().isFileAvailable());
                                advertModel.setStatus(model.getAdvertid().getStatus());
                                viewAdvertList.add(advertModel);
                            }
                        }
                        break;
                }
            } else {
                for (OrderAdvertList model : advertList) {
                    AdvertisementDefView advertModel = new AdvertisementDefView();
                    advertModel.setAdvertId(model.getAdvertid().getAdvertid());
                    advertModel.setAdvertName(model.getAdvertid().getAdvertname());
                    advertModel.setDuration(model.getAdvertid().getDuration());
                    advertModel.setLanguage(model.getAdvertid().getLanguage());
                    advertModel.setType(model.getAdvertid().getAdverttype());
                    advertModel.setEnabled(model.getAdvertid().getEnabled());
                    advertModel.setLastModifydate(model.getAdvertid().getLastModifydate());
                    advertModel.setIsFileAvailable(model.getAdvertid().isFileAvailable());
                    advertModel.setStatus(model.getAdvertid().getStatus());
                    viewAdvertList.add(advertModel);
                }
            }

        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to getSelectedAdvert: ", e.getMessage());
        }

        for (AdvertisementDefView advertisementDefView : viewAdvertList) {
            List<ScheduleDef> scheduleDefList = schedulerDao.getSelectedAdvertSpotByWorkored(workOrderId, advertisementDefView.getAdvertId());
            if (scheduleDefList.isEmpty()) {
                advertisementDefView.setCanDelete(Boolean.TRUE);
            }
        }
        return viewAdvertList;
    }

    public int getSelectedScheduleCount(int workOrderId) {
        return schedulerDao.getSelectedScheduleCount(workOrderId);
    }

    public int getSelectedChannelSlotCount(int workOrderId, int channelId) {
        return schedulerDao.getSelectedChannelSlotCount(workOrderId, channelId);
    }

    public RemainingSpotView getWorkOrderRemainSpotAsSeconds(int workOrderId, int channelId) throws Exception {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date toDay = dateFormat.parse(dateFormat.format(new Date()));

            RemainingSpotView viewModel = new RemainingSpotView();

            List<ScheduleDef> validSchedul = schedulerDao.getRamainingSpot_v2(workOrderId, toDay);
            for (int j = 0; j < validSchedul.size(); j++) {
                viewModel.addTotalAdvertCount(validSchedul.get(j).getChannelid().getChannelid(), validSchedul.get(j).getAdvertid().getDuration());
            }

            for (int i = 0; i < validSchedul.size(); i++) {

                if (validSchedul.get(i).getStatus().equals("0") || validSchedul.get(i).getStatus().equals("7")) {
                    if (validSchedul.get(i).getChannelid().getChannelid() == channelId) {
                        viewModel.addDuration(validSchedul.get(i).getAdvertid().getDuration());
                    }
                } else {

                    if (viewModel.getRemainList().isEmpty()) {
                        RemainingSpotModel model = new RemainingSpotModel();
                        model.setAdvertDuration(validSchedul.get(i).getAdvertid().getDuration());
                        model.setChannelID(validSchedul.get(i).getChannelid().getChannelid());
                        model.addOne();
                        viewModel.getRemainList().add(model);
                    } else {
                        Boolean newRecod = true;
                        for (int j = 0; j < viewModel.getRemainList().size(); j++) {
                            if ((validSchedul.get(i).getAdvertid().getDuration() == viewModel.getRemainList().get(j).getAdvertDuration()) && (validSchedul.get(i).getChannelid().getChannelid() == viewModel.getRemainList().get(j).getChannelID())) {
                                viewModel.getRemainList().get(j).addOne();
                                newRecod = false;
                            }
                        }
                        if (newRecod) {
                            RemainingSpotModel model = new RemainingSpotModel();
                            model.setAdvertDuration(validSchedul.get(i).getAdvertid().getDuration());
                            model.setChannelID(validSchedul.get(i).getChannelid().getChannelid());
                            model.addOne();
                            viewModel.getRemainList().add(model);
                        }
                    }
                }
            }
            return viewModel;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to getWorkOrderRemainSpotAsSeconds: ", e.getMessage());
            throw e;
        }
    }

    public List<ScheduleAdminView> getFinalAllSchedule(String filterData) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ScheduleAdminViewInfo dataModel = mapper.readValue(filterData, ScheduleAdminViewInfo.class
            );

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date sDate = dateFormat.parse(dataModel.getStartDate());
            Date eDate = dateFormat.parse(dataModel.getEndDate());

            List<ScheduleAdminView> finalList = new ArrayList<>();
            List<TimeSlotScheduleMap> lstTimeBeltMap = schedulerDao.getFinalAllSchedule(dataModel);
            Collections.sort(lstTimeBeltMap);

            int tmpTimeBetlId = -1;
            ScheduleAdminView viewModel = new ScheduleAdminView();

            for (TimeSlotScheduleMap tempModel : lstTimeBeltMap) {
                if (tmpTimeBetlId != tempModel.getTimeBelt().getTimeBeltId()) {
                    viewModel = createScheduleAdminView(tempModel, sDate, eDate);
                    tmpTimeBetlId = tempModel.getTimeBelt().getTimeBeltId();
                    finalList.add(viewModel);
                }

                String tempDate = new SimpleDateFormat("yyyy-MM-dd").format(tempModel.getDate());
                for (ScheduleItemModel item : viewModel.getScheduleItem()) {
                    if (tempDate.equals(item.getDate())) {
                        item.addOne();
                        if (tempModel.getScheduleid().getStatus().equals("2")) {
                            item.addAired();
                        }
                        item.addUtilizedAdvertTimeinSeconds(tempModel.getScheduleid().getAdvertid().getDuration());
                    }
                }
            }
            return finalList;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to getFinalAllSchedule: ", e.getMessage());
            return null;
        }
    }

    public ScheduleDef getScheduleDefPriority(ScheduleDef scheduleDef) {
        scheduleDef.setPriority(schedulerDao.getSelectedSchedule(scheduleDef.getScheduleid()).getPriority());
        return  scheduleDef;
    }

    public ScheduleDef getScheduleDefModel(ScheduleInfo model) {
        try {
            //24 hour date format HH, 12 hour date format hh
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse("1970-01-01 00:00:00.000");

            ChannelDetails channel = new ChannelDetails();
            channel.setChannelid(model.getChannelId());

            WorkOrder workorder = workOrderDao.getSelectedWorkOrder(model.getWorkOrderId());

            Advertisement advert = new Advertisement();
            advert.setAdvertid(model.getAdvertId());

            ScheduleDef schedule = new ScheduleDef();


            schedule.setChannelid(channel);
            schedule.setWorkorderid(workorder);
            schedule.setAdvertid(advert);
            schedule.setDate(dateFormat.parse(model.getSchedulDate()));
            schedule.setSchedulestarttime(timeFormat.parse(model.getScheduledStartDate()));
            schedule.setScheduleendtime(timeFormat.parse(model.getScheduledEndDate()));
            schedule.setActualstarttime(date);
            schedule.setActualendtime(date);
            schedule.setSequencenum(0);
            schedule.setStatus("0");
            schedule.setComment(model.getComment());
            schedule.setProgram(model.getProgram());
            schedule.setRevenueLine(model.getRevenueLine());
            schedule.setMonth(model.getMonth());

            checkChannelUpdate(schedule.getChannelid().getChannelid(), schedule.getDate());

            return schedule;

        } catch (ExceptionInInitializerError | ParseException e) {
            LOGGER.debug("Exception in SchedulerService while trying to getScheduleDefModel: ", e.getMessage());
            return null;
        }
    }

    public void checkChannelUpdate(int channelID, Date day) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String today = dateFormat.format(new Date());
        String scheduleday = dateFormat.format(day);
        if (today.equals(scheduleday)) {
            ChannelDetails model = channelDetailDAO.getChannel(channelID);
            try {
                today = dateFormat2.format(new Date());
                model.setLastUpdate(dateFormat2.parse(today));
                channelDetailDAO.updateChannel(model);
            } catch (Exception e) {
                LOGGER.debug("Exception in SchedulerService in checkChannelUpdate: ", e.getMessage());
            }
        }

    }

    public ScheduleDef getSelectedSchedule() {
        int scheduleId = 2;
        return schedulerDao.getSelectedSchedule(scheduleId);
    }

    public List<ScheduleWebView> getselectedSchedul(int workOrderId, String sDate) {
        try {
            List<ScheduleWebView> modelList = new ArrayList<>();
            List<ScheduleDef> temp = new ArrayList<>();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            temp = schedulerDao.selectedScheduleFromDate(workOrderId, dateFormat.parse(sDate));

            for (ScheduleDef model : temp) {

                ScheduleWebView scheView = new ScheduleWebView();
                scheView.setScheduleid(model.getScheduleid());
                scheView.setAdvertid(model.getAdvertid().getAdvertid());
                scheView.setAdvertName(model.getAdvertid().getAdvertname());
                scheView.setChannelid(model.getChannelid().getChannelid());
                scheView.setChannelName(model.getChannelid().getChannelname());
                scheView.setDate(model.getDate());
                scheView.setScheduleendtime(model.getScheduleendtime());
                scheView.setSchedulestarttime(model.getSchedulestarttime());
                scheView.setWorkorderid(model.getWorkorderid().getWorkorderid());

                modelList.add(scheView);

            }

            return modelList;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in getselectedSchedul: ", e.getMessage());
            return null;
        }
    }

    public List<ScheduleAdvertView> getSelectedTimeSlotAdvert(String selectdate, String filterData) {
        try {
            String[] stringData;
            stringData = selectdate.split("_");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(stringData[0]);

            ObjectMapper mapper = new ObjectMapper();
            ScheduleAdminViewInfo filter = mapper.readValue(filterData, ScheduleAdminViewInfo.class
            );

            List<TimeSlotScheduleMap> dataList;
            dataList = schedulerDao.getSelectedDateSchedule(date, Integer.parseInt(stringData[1]), filter);

            List<ScheduleAdvertView> advertViewList = new ArrayList<>();

            for (TimeSlotScheduleMap dataModel : dataList) {
                ScheduleAdvertView adverViewModel = new ScheduleAdvertView();
                adverViewModel.setWorkOrderId(dataModel.getScheduleid().getWorkorderid().getWorkorderid().toString());
                adverViewModel.setChannelName(dataModel.getChannelid().getChannelname());
                adverViewModel.setAdvertname(dataModel.getScheduleid().getAdvertid().getAdvertname());
                adverViewModel.setDuration(dataModel.getScheduleid().getAdvertid().getDuration());
                adverViewModel.setPriority(dataModel.getScheduleid().getSequencenum());
                adverViewModel.setStatus(dataModel.getScheduleid().getStatusbyName());
                adverViewModel.setStartAndEndTime(dataModel.getScheduleid().getSchedulestarttime().toString() + "-" + dataModel.getScheduleid().getScheduleendtime().toString());
                if ((!dataModel.getScheduleid().getActualstarttime().toString().equals("00:00:00")) && (!dataModel.getScheduleid().getActualendtime().toString().equals("00:00:00"))) {
                    adverViewModel.setPalyTime(dataModel.getScheduleid().getActualstarttime().toString() + "-" + dataModel.getScheduleid().getActualendtime().toString());
                }
                adverViewModel.setClient(dataModel.getScheduleid().getAdvertid().getClient().getClientname());

                advertViewList.add(adverViewModel);
            }
            return advertViewList;

        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to getSelectedTimeSlotAdvert: ", e.getMessage());
            return null;
        }
    }

    @Async("specificTaskExecutor")
    public CompletableFuture<List<OnedayScheduleView>> getScheduleOneDayAsync(OneDayScheduleInfo scheduleInfo) {
        CompletableFuture<List<OnedayScheduleView>> completableFuture = new CompletableFuture<>();
        try {
            completableFuture.complete(getOneDaySchedules(scheduleInfo));
        } catch (Exception e) {
            LOGGER.debug("Exception in getScheduleOneDayAsync: ", e.getMessage());
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

    public List<OnedayScheduleView> getOneDaySchedules(OneDayScheduleInfo scheduleInfo) {
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy/MM/dd");
        List<OnedayScheduleView> retList = new ArrayList<>();
        HashMap<Integer, OnedayScheduleView> mapTimebelt = new HashMap<Integer, OnedayScheduleView>();
        if (scheduleInfo.getStartHour() < scheduleInfo.getEndHour()) {
            scheduleInfo.setEndHour(scheduleInfo.getEndHour() - 1);
        }

        ///get original timebelt schedule map objects to identify shifted schedules(initial timebelt changed) after dynamic shceduling
        List<TimeSlotScheduleMap> tmpList = schedulerDao.getOneDaySchedules(scheduleInfo);
        HashMap<Integer, TimeSlotScheduleMap> mapTimebeltSchedules = new HashMap<>();
        for (TimeSlotScheduleMap item : tmpList) {
            mapTimebeltSchedules.put(item.getScheduleid().getScheduleid(), item);
        }

        try {
            if (dtFormat.format(new Date()).equals(dtFormat.format(scheduleInfo.getDate()))) { // today
                List<PlayList> resultList = playListScheduleDao.getOneDaySchedules(scheduleInfo);

                for (PlayList item : resultList) {
                    int iTimeBeltId = item.getScheduleHour();
                    if (!mapTimebelt.containsKey(iTimeBeltId)) {
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
                        OnedayScheduleView tmpView = new OnedayScheduleView(iTimeBeltId,
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 10) ? "0" : "") + iTimeBeltId + ":00:00"),
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 9) ? "0" : "") + (iTimeBeltId + 1) + ":00:00"));
                        mapTimebelt.put(iTimeBeltId, tmpView);
                    }
                    ScheduleAdvertView model = new ScheduleAdvertView(
                            item.getWorkOrder().getOrdername(),
                            item.getChannel().getChannelname(),
                            item.getAdvert().getAdvertname(),
                            item.getAdvert().getDuration(),
                            item.getAdvert().getAdverttype(),
                            0,
                            item.getStatusbyName());
                    model.setStartAndEndTime(item.getSchedule().getSchedulestarttime().toString() + "-" + item.getSchedule().getScheduleendtime().toString());
                    if (item.getSchedule().getAdvertid().getEnabled() == 0 && item.getSchedule().getAdvertid().getStatus() == 1) {
                        model.setStatus("Expired");
                    }

                    TimeSlotScheduleMap tmpScheduleMap = mapTimebeltSchedules.get(item.getSchedule().getScheduleid());
                    TimeBelts initialTimeBelt = (tmpScheduleMap != null) ? tmpScheduleMap.getTimeBelt() : null;
                    if (initialTimeBelt != null && initialTimeBelt.getHour() != ((int) item.getScheduleHour())) {
                        model.setInitialStartEndTime(initialTimeBelt.getStartTime().toString() + "-" + initialTimeBelt.getEndTime().toString());
                    }

                    model.setClusterNum(item.getPlayCluster());
                    model.setIsFileAvailable(item.getAdvert().isFileAvailable());
                    model.setClient(item.getAdvert().getClient().getClientname());
                    if (!getTimeHHMMSS(item.getActualStartTime()).equals("00:00:00")) {
                        model.setPalyTime(getTimeHHMMSS(item.getActualStartTime()) + "-" + getTimeHHMMSS(item.getActualEndTime()));
                    }
                    mapTimebelt.get(iTimeBeltId).addScheduleItems(model);
                }

            } else if (dtFormat.parse(dtFormat.format(new Date())).after(dtFormat.parse((dtFormat.format(scheduleInfo.getDate()))))) {
                List<PlayListHistory> resultList = playHistoryDao.getOneSchedule(scheduleInfo);

                for (PlayListHistory item : resultList) {
                    int iTimeBeltId = item.getSchedulehour();
                    if (!mapTimebelt.containsKey(iTimeBeltId)) {
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
                        OnedayScheduleView tmpView = new OnedayScheduleView(iTimeBeltId,
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 10) ? "0" : "") + iTimeBeltId + ":00:00"),
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 9) ? "0" : "") + (iTimeBeltId + 1) + ":00:00"));
                        mapTimebelt.put(iTimeBeltId, tmpView);
                    }
                    Advertisement advet = advertisementDao.getSelectedAdvertisement(item.getAdvertid());
                    ScheduleAdvertView model = new ScheduleAdvertView(
                            workOrderDao.getSelectedWorkOrder(item.getWorkorderid()).getOrdername(),
                            channelDetailDAO.getChannel(item.getChannelid()).getChannelname(),
                            advet.getAdvertname(),
                            advet.getDuration(),
                            advet.getAdverttype(),
                            0,
                            item.getStatusbyName());
                    model.setStartAndEndTime(item.getScheduleid().getSchedulestarttime().toString() + "-" + item.getScheduleid().getScheduleendtime().toString());

                    TimeSlotScheduleMap tmpScheduleMap = mapTimebeltSchedules.get(item.getScheduleid().getScheduleid());
                    TimeBelts initialTimeBelt = (tmpScheduleMap != null) ? tmpScheduleMap.getTimeBelt() : null;
                    if (initialTimeBelt != null && initialTimeBelt.getHour() != (item.getSchedulehour())) {
                        model.setInitialStartEndTime(initialTimeBelt.getStartTime().toString() + "-" + initialTimeBelt.getEndTime().toString());
                    }
                    model.setClusterNum(item.getPlaycluster());
                    model.setIsFileAvailable(advet.isFileAvailable());
                    model.setClient(advet.getClient().getClientname());
                    model.setPalyTime(getTimeHHMMSS(item.getActualstarttime()) + "-" + getTimeHHMMSS(item.getActualendtime()));
                    mapTimebelt.get(iTimeBeltId).addScheduleItems(model);
                }

            } else {
                List<TimeSlotScheduleMap> resultList = schedulerDao.getOneDaySchedules(scheduleInfo);

                for (TimeSlotScheduleMap item : resultList) {
                    int iTimeBeltId = item.getTimeBelt().getHour();
                    if (!mapTimebelt.containsKey(iTimeBeltId)) {
                        OnedayScheduleView tmpView = new OnedayScheduleView(iTimeBeltId, item.getTimeBelt().getStartTime(), item.getTimeBelt().getEndTime());
                        mapTimebelt.put(iTimeBeltId, tmpView);
                    }
                    ScheduleAdvertView model = new ScheduleAdvertView(
                            "" + item.getWorkOrderId(),
                            item.getChannelid().getChannelname(),
                            item.getScheduleid().getAdvertid().getAdvertname(),
                            item.getScheduleid().getAdvertid().getDuration(),
                            item.getScheduleid().getAdvertid().getAdverttype(),
                            0,
                            item.getScheduleid().getStatusbyName());
                    model.setStartAndEndTime(item.getScheduleid().getSchedulestarttime().toString() + "-" + item.getScheduleid().getScheduleendtime().toString());
                    model.setIsFileAvailable(item.getScheduleid().getAdvertid().isFileAvailable());
                    model.setClient(item.getScheduleid().getAdvertid().getClient().getClientname());
                    if (!getTimeHHMMSS(item.getScheduleid().getActualstarttime()).equals("00:00:00")) {
                        model.setPalyTime(getTimeHHMMSS(item.getScheduleid().getActualstarttime()) + "-" + getTimeHHMMSS(item.getScheduleid().getActualendtime()));
                    }
                    ///expired advertisement
                    if (item.getScheduleid().getAdvertid().getEnabled() == 0 && item.getScheduleid().getAdvertid().getStatus() == 1) {
                        model.setStatus("Expired");
                    }
                    mapTimebelt.get(iTimeBeltId).addScheduleItems(model);
                }
            }

            for (int key : mapTimebelt.keySet()) {
                OnedayScheduleView tmpItem = mapTimebelt.get(key);
                tmpItem.orderScheduleItems();
                tmpItem.setChannelId(scheduleInfo.getChannelId());
                retList.add(tmpItem);
            }
            Collections.sort(retList);
            return retList;
        } catch (Exception ex) {
            Logger.getLogger(SchedulerService.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<OnedayScheduleView> getOneDaySchedulesView(OneDayScheduleInfo scheduleInfo) {
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy/MM/dd");
        List<OnedayScheduleView> retList = new ArrayList<>();
        HashMap<Integer, OnedayScheduleView> mapTimebelt = new HashMap<Integer, OnedayScheduleView>();
        if (scheduleInfo.getStartHour() < scheduleInfo.getEndHour()) {
            scheduleInfo.setEndHour(scheduleInfo.getEndHour() - 1);
        }

        ///get original timebelt schedule map objects to identify shifted schedules(initial timebelt changed) after dynamic shceduling
        List<TimeSlotScheduleMap> tmpList = schedulerDao.getOneDaySchedules(scheduleInfo);
        HashMap<Integer, TimeSlotScheduleMap> mapTimebeltSchedules = new HashMap<>();
        for (TimeSlotScheduleMap item : tmpList) {
            mapTimebeltSchedules.put(item.getScheduleid().getScheduleid(), item);
        }

        try {
            if (dtFormat.format(new Date()).equals(dtFormat.format(scheduleInfo.getDate()))) { // today
                List<PlayList> resultList = playListScheduleDao.getOneDaySchedulesView(scheduleInfo);
                resultList = updateZeroAdPlayListOrder(resultList);

                for (PlayList item : resultList) {
                    int iTimeBeltId = item.getScheduleHour();
                    if (!mapTimebelt.containsKey(iTimeBeltId)) {
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
                        OnedayScheduleView tmpView = new OnedayScheduleView(iTimeBeltId,
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 10) ? "0" : "") + iTimeBeltId + ":00:00"),
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 9) ? "0" : "") + (iTimeBeltId + 1) + ":00:00"));
                        mapTimebelt.put(iTimeBeltId, tmpView);
                    }
                    ScheduleAdvertView model = new ScheduleAdvertView(
                            item.getWorkOrder().getOrdername(),
                            item.getChannel().getChannelname(),
                            item.getAdvert().getAdvertname(),
                            item.getAdvert().getDuration(),
                            item.getAdvert().getAdverttype(),
                            0,
                            item.getStatusbyName());
                    model.setStartAndEndTime(item.getSchedule().getSchedulestarttime().toString() + "-" + item.getSchedule().getScheduleendtime().toString());
                    if (item.getSchedule().getAdvertid().getEnabled() == 0 && item.getSchedule().getAdvertid().getStatus() == 1) {
                        model.setStatus("Expired");
                    }

                    TimeSlotScheduleMap tmpScheduleMap = mapTimebeltSchedules.get(item.getSchedule().getScheduleid());
                    TimeBelts initialTimeBelt = (tmpScheduleMap != null) ? tmpScheduleMap.getTimeBelt() : null;
                    if (initialTimeBelt != null && initialTimeBelt.getHour() != ((int) item.getScheduleHour())) {
                        model.setInitialStartEndTime(initialTimeBelt.getStartTime().toString() + "-" + initialTimeBelt.getEndTime().toString());
                    }

                    model.setClusterNum(item.getPlayCluster());
                    model.setPlayOrder(item.getPlayOrder());
                    model.setIsFileAvailable(item.getAdvert().isFileAvailable());
                    model.setClient(item.getAdvert().getClient().getClientname());
                    if (!getTimeHHMMSS(item.getActualStartTime()).equals("00:00:00")) {
                        model.setPalyTime(getTimeHHMMSS(item.getActualStartTime()) + "-" + getTimeHHMMSS(item.getActualEndTime()));
                    }
                    mapTimebelt.get(iTimeBeltId).addScheduleItems(model);
                }

            } else if (dtFormat.parse(dtFormat.format(new Date())).after(dtFormat.parse((dtFormat.format(scheduleInfo.getDate()))))) {
                List<PlayListHistory> resultList = playHistoryDao.getOneSchedule(scheduleInfo);
                updateZeroAdPlayListHistoryOrder(resultList);
                for (PlayListHistory item : resultList) {
                    int iTimeBeltId = item.getSchedulehour();
                    if (!mapTimebelt.containsKey(iTimeBeltId)) {
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
                        OnedayScheduleView tmpView = new OnedayScheduleView(iTimeBeltId,
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 10) ? "0" : "") + iTimeBeltId + ":00:00"),
                                dtFmt.parse("1970-01-01 " + ((iTimeBeltId < 9) ? "0" : "") + (iTimeBeltId + 1) + ":00:00"));
                        mapTimebelt.put(iTimeBeltId, tmpView);
                    }
                    Advertisement advet = advertisementDao.getSelectedAdvertisement(item.getAdvertid());
                    ScheduleAdvertView model = new ScheduleAdvertView(
                            workOrderDao.getSelectedWorkOrder(item.getWorkorderid()).getOrdername(),
                            channelDetailDAO.getChannel(item.getChannelid()).getChannelname(),
                            advet.getAdvertname(),
                            advet.getDuration(),
                            advet.getAdverttype(),
                            0,
                            item.getStatusbyName());
                    model.setStartAndEndTime(item.getScheduleid().getSchedulestarttime().toString() + "-" + item.getScheduleid().getScheduleendtime().toString());

                    TimeSlotScheduleMap tmpScheduleMap = mapTimebeltSchedules.get(item.getScheduleid().getScheduleid());
                    TimeBelts initialTimeBelt = (tmpScheduleMap != null) ? tmpScheduleMap.getTimeBelt() : null;
                    if (initialTimeBelt != null && initialTimeBelt.getHour() != (item.getSchedulehour())) {
                        model.setInitialStartEndTime(initialTimeBelt.getStartTime().toString() + "-" + initialTimeBelt.getEndTime().toString());
                    }
                    model.setClusterNum(item.getPlaycluster());
                    model.setIsFileAvailable(advet.isFileAvailable());
                    model.setClient(advet.getClient().getClientname());
                    model.setPalyTime(getTimeHHMMSS(item.getActualstarttime()) + "-" + getTimeHHMMSS(item.getActualendtime()));
                    mapTimebelt.get(iTimeBeltId).addScheduleItems(model);
                }

            } else {
                List<TimeSlotScheduleMap> resultList = schedulerDao.getOneDaySchedules(scheduleInfo);

                for (TimeSlotScheduleMap item : resultList) {
                    int iTimeBeltId = item.getTimeBelt().getHour();
                    if (!mapTimebelt.containsKey(iTimeBeltId)) {
                        OnedayScheduleView tmpView = new OnedayScheduleView(iTimeBeltId, item.getTimeBelt().getStartTime(), item.getTimeBelt().getEndTime());
                        mapTimebelt.put(iTimeBeltId, tmpView);
                    }
                    ScheduleAdvertView model = new ScheduleAdvertView(
                            "" + item.getWorkOrderId(),
                            item.getChannelid().getChannelname(),
                            item.getScheduleid().getAdvertid().getAdvertname(),
                            item.getScheduleid().getAdvertid().getDuration(),
                            item.getScheduleid().getAdvertid().getAdverttype(),
                            0,
                            item.getScheduleid().getStatusbyName());
                    model.setStartAndEndTime(item.getScheduleid().getSchedulestarttime().toString() + "-" + item.getScheduleid().getScheduleendtime().toString());
                    model.setIsFileAvailable(item.getScheduleid().getAdvertid().isFileAvailable());
                    model.setClient(item.getScheduleid().getAdvertid().getClient().getClientname());
                    if (!getTimeHHMMSS(item.getScheduleid().getActualstarttime()).equals("00:00:00")) {
                        model.setPalyTime(getTimeHHMMSS(item.getScheduleid().getActualstarttime()) + "-" + getTimeHHMMSS(item.getScheduleid().getActualendtime()));
                    }
                    ///expired advertisement
                    if (item.getScheduleid().getAdvertid().getEnabled() == 0 && item.getScheduleid().getAdvertid().getStatus() == 1) {
                        model.setStatus("Expired");
                    }
                    mapTimebelt.get(iTimeBeltId).addScheduleItems(model);
                }
            }

            for (int key : mapTimebelt.keySet()) {
                OnedayScheduleView tmpItem = mapTimebelt.get(key);
                //tmpItem.orderScheduleItems();
                retList.add(tmpItem);
            }
            Collections.sort(retList);
            return retList;
        } catch (Exception ex) {
            Logger.getLogger(SchedulerService.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private List<PlayList> updateZeroAdPlayListOrder(List<PlayList> resultList){
        int currentHour = new Date().getHours(); //current hour index
        List<PlayList> oldHoursList = new ArrayList<>();
        List<PlayList> currentHourList = new ArrayList<>();
        List<PlayList> futureHoursList = new ArrayList<>();

        Iterator<PlayList> playListIterator = resultList.iterator();
        while (playListIterator.hasNext()){
            PlayList playList = playListIterator.next();
            if(playList.getScheduleHour() < currentHour ||
                    (playList.getScheduleHour() == currentHour &&
                            (!playList.getStatus().equalsIgnoreCase("1")))){
                oldHoursList.add(playList);
            }else {
                futureHoursList.add(playList);
            }
        }

        //sort zero ads (directly requested from mosh based on aired time)
        List<PlayList> dZeroAds = new ArrayList<>();
        Iterator<PlayList> iterator = oldHoursList.iterator();
        while (iterator.hasNext()){
            PlayList pl = iterator.next();
            if(pl.getLable() == PlayList.Lable.ZERO && pl.getPlayCluster() == -1 && (pl.getStatus().equalsIgnoreCase("2") || pl.getStatus().equalsIgnoreCase("5")) ){
                iterator.remove();
                dZeroAds.add(pl);
            }
        }
        Comparator<PlayList> comparator = new PLSortByAiredTime();
        Collections.sort(oldHoursList, comparator);
        Collections.sort(dZeroAds, comparator);
        for(PlayList za : dZeroAds){
            boolean isAdded = false;
            for(int i = 0; i < oldHoursList.size(); i++){
                PlayList next = oldHoursList.get(i);
                if(za.getActualStartTime().getTime() <= next.getActualStartTime().getTime()){
                    isAdded = true;
                    oldHoursList.add(i, za);
                    break;
                }
            }

            if(!isAdded){
                oldHoursList.add(za);
            }
        }
        oldHoursList.addAll(futureHoursList);
        return oldHoursList;
    }

    private void updateZeroAdPlayListHistoryOrder(List<PlayListHistory> resultList){
        //sort zero ads (directly requested from mosh based on aired time)
        List<PlayListHistory> dZeroAds = new ArrayList<>();
        Iterator<PlayListHistory> iterator = resultList.iterator();
        while (iterator.hasNext()){
            PlayListHistory pl = iterator.next();
            if(pl.getLabel() == PlayList.Lable.ZERO && pl.getPlaycluster() == -1 && (pl.getStatus().equalsIgnoreCase("2") || pl.getStatus().equalsIgnoreCase("5")) ){
                iterator.remove();
                dZeroAds.add(pl);
            }
        }
        Comparator<PlayListHistory> comparator = new PLHistorySortByAiredTime();
        Collections.sort(resultList, comparator);
        Collections.sort(dZeroAds, comparator);
        for(PlayListHistory za : dZeroAds){
            boolean isAdded = false;
            for(int i = 0; i < resultList.size(); i++){
                PlayListHistory next = resultList.get(i);
                if(za.getActualstarttime().getTime() <= next.getActualstarttime().getTime()){
                    resultList.add(i, za);
                    isAdded = true;
                    break;
                }
            }

            if(!isAdded){
                resultList.add(za);
            }
        }

    }

    /// refactored 
    public WorkOrderChannelScheduleView getScheduleAvailableSpotCount(int workOrderId, int channelId) { //tested
        try {
            List<WorkOrderChannelList> woChannelList;
            if (channelId != -999) {
                woChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId, channelId);
            } else {
                woChannelList = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId);
            }

            Set<WorkOrderChannelList> woChannelSet = new HashSet();
            //remove duplicate
            for (WorkOrderChannelList item : woChannelList) {
                woChannelSet.add(item);
            }

            WorkOrderChannelScheduleView viewModel = new WorkOrderChannelScheduleView();
            long iTotalTVCDuration = 0;
            for (WorkOrderChannelList _orderChannelModel : woChannelSet) {
                /// initial paid + bonus spot counts
                viewModel.setTvcfspot(_orderChannelModel.getNumbertofspot() + _orderChannelModel.getBonusspot());
                viewModel.setLogoSpots(_orderChannelModel.getLogospots() + _orderChannelModel.getLogobspots());
                viewModel.setCrowlerSpots(_orderChannelModel.getCrowlerspots() + _orderChannelModel.getCrowlerbspots());
                viewModel.setV_sqeezeSpots(_orderChannelModel.getVsqeezspots() + _orderChannelModel.getVsqeezebspots());
                viewModel.setL_sqeezeSpots(_orderChannelModel.getLsqeezespots() + _orderChannelModel.getLsqeezebspots());

                /// tvc_breakdown                 
                HashMap<Integer, Integer> tvcBreackdownMap = viewModel.getTvcBreackdownMap();
                for (TVCDurationSpotCount spot : _orderChannelModel.getTvcSpots()) {
                    if (tvcBreackdownMap.containsKey(spot.getDuration())) {
                        tvcBreackdownMap.put(spot.getDuration(), tvcBreackdownMap.get(spot.getDuration()) + spot.getSpotCount());
                    } else {
                        tvcBreackdownMap.put(spot.getDuration(), spot.getSpotCount());
                    }
                    iTotalTVCDuration += (spot.getDuration() * spot.getSpotCount());
                }
            }
            viewModel.setTotalTVCDuration(iTotalTVCDuration);

            WorkOrderChannelList lstScheduled = null;
            if (channelId != -999) {
                lstScheduled = schedulerDao.getWorkOrderAvailableSpotCount(workOrderId, channelId);
            } else {
                lstScheduled = schedulerDao.getWorkOrderAvailableSpotCount(workOrderId);
            }
            //viewModel.setAvailableTvcSpots(lstScheduled.getNumbertofspot());
            viewModel.setAvailableLogoSpots(lstScheduled.getLogospots());
            viewModel.setAvailableCrowlerSpots(lstScheduled.getCrowlerspots());
            viewModel.setAvailableV_SqeezeSpots(lstScheduled.getVsqeezspots());
            viewModel.setAvailableL_SqeezeSpots(lstScheduled.getLsqeezespots());

            /// tvc_breakdown 
            long iAvailableTVCDuration = viewModel.getTotalTVCDuration();
            HashMap<Integer, Integer> tvcBreackdownMapAvailable = viewModel.getTvcBreackdownMapAvailable();
            for (int item : viewModel.getTvcBreackdownMap().keySet()) {
                tvcBreackdownMapAvailable.put(item, viewModel.getTvcBreackdownMap().get(item));
            }
            for (TVCDurationSpotCount item : lstScheduled.getTvcSpots()) {
                if (!viewModel.getTvcBreackdownMap().containsKey(item.getDuration())) {
                    viewModel.getTvcBreackdownMap().put(item.getDuration(), 0);
                }
                tvcBreackdownMapAvailable.put(item.getDuration(), viewModel.getTvcBreackdownMap().get(item.getDuration()) - item.getSpotCount());
                iAvailableTVCDuration -= (item.getDuration() * item.getSpotCount());
            }
            viewModel.setAvailableTVCDuration(iAvailableTVCDuration);
            viewModel.setAvailableTvcSpots((int) (iAvailableTVCDuration / 30)); /// updated to get 30sec spot count
            return viewModel;

        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to getScheduleAvailableSpotCount: ", e.getMessage());
            return null;
        }
    }

    public WorkOrderChannelScheduleView getScheduleAirSpotCount(int workOrderId) {
        try {
            List<WorkOrderChannelList> orderChannelLists;
            WorkOrder workOrder;
            orderChannelLists = workOrderChannelListDao.getSelectedWorkOrderSpotCount(workOrderId);

            workOrder = workOrderDao.getSelectedWorkOrder(workOrderId);
            int custemorVisibility = workOrder.getBspotvisibility();
            WorkOrderChannelScheduleView viewModel = new WorkOrderChannelScheduleView();

            for (WorkOrderChannelList _orderChannelModel : orderChannelLists) {
                viewModel.setTvcfspot(_orderChannelModel.getNumbertofspot());
                viewModel.setLogoSpots(_orderChannelModel.getLogospots());
                viewModel.setCrowlerSpots(_orderChannelModel.getCrowlerspots());
                viewModel.setV_sqeezeSpots(_orderChannelModel.getVsqeezspots());
                viewModel.setL_sqeezeSpots(_orderChannelModel.getLsqeezespots());
                if (custemorVisibility == 1) {
                    viewModel.setTvcfspot(_orderChannelModel.getBonusspot());
                    viewModel.setLogoSpots(_orderChannelModel.getLogobspots());
                    viewModel.setCrowlerSpots(_orderChannelModel.getCrowlerbspots());
                    viewModel.setV_sqeezeSpots(_orderChannelModel.getVsqeezebspots());
                    viewModel.setL_sqeezeSpots(_orderChannelModel.getLsqeezebspots());
                }
            }

            List<ScheduleDef> scheduledList = schedulerDao.getScheduleListOrderByASC(workOrderId);
            for (ScheduleDef model : scheduledList) {
                switch (model.getAdvertid().getAdverttype()) {
                    case "ADVERT":
                        if (model.getStatus().equals("2")) {
                            viewModel.setNumbertOfAirSpot(1);
                        }
                        break;
                    case "LOGO":
                        if (model.getStatus().equals("2")) {
                            viewModel.setAirLogoSpots(1);
                        }
                        break;
                    case "CRAWLER":
                        if (model.getStatus().equals("2")) {
                            viewModel.setAirCrowlerSpots(1);
                        }
                        break;
                    case "V_SHAPE":
                        if (model.getStatus().equals("2")) {
                            viewModel.setAirV_sqeezeSpots(1);
                        }
                        break;
                    case "L_SHAPE":
                        if (model.getStatus().equals("2")) {
                            viewModel.setAirL_sqeezeSpots(1);
                        }
                        break;
                    default:
                        break;
                }
            }

            return viewModel;

        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to getScheduleAirSpotCount: ", e.getMessage());
            return null;
        }
    }

    public ScheduleUpdateView createScheduleUpdateView(ScheduleDef dataModel) throws ParseException {

        ScheduleUpdateView model = new ScheduleUpdateView();

        model.setChannelid(dataModel.getChannelid().getChannelid());
        model.setChannelName(dataModel.getChannelid().getChannelname());

        model.setAdvertid(dataModel.getAdvertid().getAdvertid());
        model.setAdvertName(dataModel.getAdvertid().getAdvertname());
        model.setAdvertDuration(dataModel.getAdvertid().getDuration());
        model.setAdverLang(dataModel.getAdvertid().getLanguage());
        model.setAdverCategory(dataModel.getAdvertid().getAdverttype());
        if ((!dataModel.getAdvertid().isFileAvailable()) && (dataModel.getAdvertid().getStatus() == 1)) {
            model.setIsDummyCut(Boolean.TRUE);
        }
        if ((dataModel.getAdvertid().getEnabled() == 0) && (dataModel.getAdvertid().getStatus() == 2)) {
            model.setIsSuspendCut(Boolean.TRUE);
        }

        model.setWorkorderid(dataModel.getWorkorderid().getWorkorderid());

        model.setSchedulestarttime(dataModel.getSchedulestarttime());
        model.setScheduleendtime(dataModel.getScheduleendtime());
        model.setProgram(dataModel.getProgram());
        model.setRevenueline(dataModel.getRevenueLine());
        model.setMonth(dataModel.getMonth());

        String startDay = dataModel.getWorkorderid().getStartdate().toString();
        String endDay = dataModel.getWorkorderid().getEnddate().toString();
        int i = 0;
        if (endDay.equals(startDay)) {
            ScheduleItemModel itemModel = new ScheduleItemModel();
            startDay = addDay(dataModel.getWorkorderid().getStartdate(), i);
            itemModel.setDate(startDay);

            if (startDay.equals(dataModel.getDate().toString())) {
                itemModel.addOne();
                itemModel.getScheduleid().add(dataModel.getScheduleid());
            }
            model.getScheduleItem().add(itemModel);
        }
        while (!endDay.equals(startDay)) {

            ScheduleItemModel itemModel = new ScheduleItemModel();
            startDay = addDay(dataModel.getWorkorderid().getStartdate(), i);
            itemModel.setDate(startDay);

            if (startDay.equals(dataModel.getDate().toString())) {
                itemModel.addOne();
                itemModel.getScheduleid().add(dataModel.getScheduleid());
                if(dataModel.getPriority() != null){
                    itemModel.getPriorityIds().add(new PriorityMap(dataModel.getScheduleid(),dataModel.getPriority().getPriorityId()));
                }else{
                    itemModel.getPriorityIds().add(new PriorityMap(dataModel.getScheduleid(),0));
                }
            }
            model.getScheduleItem().add(itemModel);
            i++;
        }

        return model;
    }

    public String addDay(Date parsedDate, int count) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(parsedDate);
        cal.add(Calendar.DATE, count);
        return dateFormat.format(cal.getTime());
    }

    public ScheduleModelPro createSchedulePro(ScheduleDef dataModel) {
        ScheduleModelPro model = new ScheduleModelPro();
        model.setChannelId(dataModel.getChannelid().getChannelid());
        model.setScheduleId(dataModel.getScheduleid());
        model.setInitiateDate(dataModel.getDate());
        model.setScheduledEndDate(dataModel.getScheduleendtime());
        model.setScheduledStartDate(dataModel.getSchedulestarttime());
        model.setAdvertId(dataModel.getAdvertid().getAdvertid());

        return model;

    }

    public ScheduleAdminView createScheduleAdminView(TimeSlotScheduleMap dataModel, Date startDate, Date endDate) {
        ScheduleAdminView model = new ScheduleAdminView();
        model.setTimeBeltId(dataModel.getTimeBelt().getTimeBeltId());
        model.setSchedulestarttime(dataModel.getTimeBelt().getStartTime());
        model.setScheduleendtime(dataModel.getTimeBelt().getEndTime());

        String startDay = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
        String endDay = new SimpleDateFormat("yyyy-MM-dd").format(endDate);

        int i = 0;
        while (!endDay.equals(startDay)) {
            ScheduleItemModel itemModel = new ScheduleItemModel();
            startDay = this.addDay(startDate, i);
            itemModel.setDate(startDay);
            itemModel.setTotalAdvertTime(new Date(dataModel.getTimeBelt().getTotalScheduleTime() * 1000));
            model.getScheduleItem().add(itemModel);
            i++;
        }

        return model;
    }

    @Transactional
    public WorkOrderScheduleView setWorkOrderStatusAdnSetSchedule(String schedulData) {
        try {
            ObjectMapper m = new ObjectMapper();
            List<ScheduleInfo> schedulList;
            schedulList = m.readValue(schedulData, new TypeReference<List<ScheduleInfo>>() {
            });
            WorkOrder model = workOrderDao.getSelectedWorkOrder(schedulList.get(0).getWorkOrderId());
            model.setPermissionstatus(2);
            if (workOrderDao.upadteWorkOrder(model)) {
                WorkOrderScheduleView wsView = saveWOSchedule(schedulData);
                return wsView;
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to setWorkOrderStatusAndSetSchedule: ", e.getMessage());
            return null;
        }
        return null;
    }

    public void checkAndUpdateWO(int woID) {
        WorkOrder model = workOrderDao.getSelectedWorkOrder(woID);
        if (model.getWorkorderid() == 0) {
            model.setPermissionstatus(2);
            //channelUtillizationWidgetService.setChannelUtilizationData();
            workOrderDao.upadteWorkOrder(model);
        }
    }

    public Boolean setRescheduleSpot(String schedulData, int scheduleId) {
        try {
            ObjectMapper m = new ObjectMapper();
            List<ScheduleInfo> schedulList;
            schedulList = m.readValue(schedulData, new TypeReference<List<ScheduleInfo>>() {
            });

            SimpleDateFormat dta = new SimpleDateFormat("yyyy-MM-dd");// 00:00:00:000
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(scheduleId);
            if (schedulModel.getAdvertid().getEnabled() == 1) {
                schedulModel.setStatus("7");
                schedulModel.setComment("Rescheduled");
                schedulModel.setSchedulestarttime(timeFormat.parse(schedulList.get(0).getScheduledStartDate()));
                schedulModel.setScheduleendtime(timeFormat.parse(schedulList.get(0).getScheduledEndDate()));
                schedulModel.setDate(dta.parse(schedulList.get(0).getSchedulDate()));
                if (schedulModel.getWorkorderid().getEnddate().before(dta.parse(schedulList.get(0).getSchedulDate()))) {
                    LOGGER.debug("setRescheduleSpot : invalid data :{} {} ", schedulModel.getWorkorderid().getEnddate(), schedulList.get(0).getSchedulDate());
                    return false;
                }

                if (scheduleSpotValidation(schedulModel)) {
                    if (schedulerDao.updateSchedule(schedulModel)) {
                        //if (setSchedule(schedulData)) {
                        setDailySchedule(schedulModel.getChannelid().getChannelid(), schedulModel.getDate(), schedulModel.getDate(), schedulModel.getSchedulestarttime());
                        if (dta.format(new Date()).equals(schedulList.get(0).getSchedulDate())) {

                            setPlayPermissionForWorkOrder(-102, dta.parse(schedulList.get(0).getSchedulDate()));

                        }
                        return true;
                        //} else {
                        //    return false;
                        //}
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService while trying to setRescheduleSpot: ", e.getMessage());
            return false;
        }
    }

    @Transactional
    public WorkOrderScheduleView setRescheduleSpot_V2(String schedulData, String scheduleId) {
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();
        retObject.setRescheduleStatus(true);
        Message msg = new Message();
        try {
            ObjectMapper m = new ObjectMapper();
            List<ScheduleInfo> schedulList;
            schedulList = m.readValue(schedulData, new TypeReference<List<ScheduleInfo>>() {
            });

            String[] scheduleIdList = scheduleId.split("_");

            SimpleDateFormat dta = new SimpleDateFormat("yyyy-MM-dd");// 00:00:00:000
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            List<ScheduleDef> temSchedu = new ArrayList<>();

            List<WorkOrderSpot> spotAvailability = checkSpotWorkOrderSpotAvailability(scheduleIdList);

            for (int i = 0; i < scheduleIdList.length; i++) {
                ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(Integer.parseInt(scheduleIdList[i]));
                if (schedulModel.getAdvertid().getEnabled() == 1 || (schedulModel.getAdvertid().getEnabled() == 0 && schedulModel.getAdvertid().getStatus() == 2)) {
                    schedulModel.setStatus("7");
                    schedulModel.setComment("Rescheduled");
                    schedulModel.setSchedulestarttime(timeFormat.parse(schedulList.get(0).getScheduledStartDate()));
                    schedulModel.setScheduleendtime(timeFormat.parse(schedulList.get(0).getScheduledEndDate()));
                    schedulModel.setDate(dta.parse(schedulList.get(0).getSchedulDate()));
                    ChannelDetails channelModel = new ChannelDetails();
                    channelModel.setChannelid(schedulList.get(0).getChannelId());
                    schedulModel.setChannelid(channelModel);
                    if (schedulModel.getWorkorderid().getEnddate().before(dta.parse(schedulList.get(0).getSchedulDate()))) {
                        LOGGER.debug("setRescheduleSpot : invalid data : {} {} ", schedulModel.getWorkorderid().getEnddate(), schedulList.get(0).getSchedulDate());
                        msg.setFlag(Boolean.FALSE);
                        msg.setMessage("Invalid data. (Check work order end date)");

                        retObject.setScheduleTempList(temSchedu);
                        retObject.setCurrentScheduleList(temSchedu);
                        retObject.setRemoveScheduleList(new ArrayList<>());
                        retObject.setMessage(" Schedules Inserted.");
                        retObject.setMessageDto(msg);
                        return retObject;
                    }

                    // if (schedulerDao.updateSchedule(schedulModel)) {
                    temSchedu.add(schedulModel);
                    //}
                }
            }
            Boolean noChannel = true;
            for (int i = 0; i < temSchedu.size(); i++) {
                for (int j = 0; j < spotAvailability.size(); j++) {
                    if (temSchedu.get(i).getWorkorderid().getWorkorderid() == spotAvailability.get(j).getWorkOrderId()) {
                        for (int k = 0; k < spotAvailability.get(j).getValidateList().size(); k++) {
                            if (temSchedu.get(i).getChannelid().getChannelid() == spotAvailability.get(j).getValidateList().get(k).getChannelID() && temSchedu.get(i).getAdvertid().getDuration() == spotAvailability.get(j).getValidateList().get(k).getAdvertDuration()) {
                                spotAvailability.get(j).getValidateList().get(k).setCheckSpot();
                                noChannel = false;
                            }
                        }

                        if (noChannel) {
                            msg.setFlag(Boolean.FALSE);
                            msg.setMessage("Requeseted channel not in work order");

                            retObject.setScheduleTempList(temSchedu);
                            retObject.setCurrentScheduleList(temSchedu);
                            retObject.setRemoveScheduleList(new ArrayList<>());
                            retObject.setMessage(" Schedules Inserted.");
                            retObject.setMessageDto(msg);
                            return retObject;//"Please add channel " + temSchedu.get(i).getChannelid().getChannelname() + " and " + temSchedu.get(i).getAdvertid().getDuration() + " spots";
                        }
                    }
                }
            }

            for (int i = 0; i < spotAvailability.size(); i++) {
                for (int j = 0; j < spotAvailability.get(i).getValidateList().size(); j++) {
                    int check = spotAvailability.get(i).getValidateList().get(j).getCheckSpot();
                    if (check != 0) {
                        int advertDuration = spotAvailability.get(i).getValidateList().get(j).getAdvertDuration();
                        long available = spotAvailability.get(i).getValidateList().get(j).getTotalAvailableDuration();
                        int requested = spotAvailability.get(i).getValidateList().get(j).getRequestedSpot();
                        if (((check * advertDuration) - (requested * advertDuration)) > available) {
                            msg.setFlag(Boolean.FALSE);
                            msg.setMessage("Not available spot");

                            retObject.setScheduleTempList(temSchedu);
                            retObject.setCurrentScheduleList(temSchedu);
                            retObject.setRemoveScheduleList(new ArrayList<>());
                            retObject.setMessage(" Schedules Inserted.");
                            retObject.setMessageDto(msg);
                            return retObject;
                        }
                    }
                }
            }

            for (int i = 0; i < temSchedu.size(); i++) {
                if (scheduleSpotValidation(temSchedu.get(i))) {
                    if (schedulerDao.updateSchedule(temSchedu.get(i))) {

                    }
                } else {
                    temSchedu.remove(temSchedu.get(i));
                    i--;
                }
            }

            if (!temSchedu.isEmpty()) {

                retObject.setScheduleTempList(temSchedu);
                retObject.setCurrentScheduleList(temSchedu);
                retObject.setRemoveScheduleList(new ArrayList<>());
                retObject.setMessage(" Schedules Inserted.");

//                Collections.sort(temSchedu);
//                ScheduleDef minSchedule = temSchedu.get(0);
//                setDailySchedule(minSchedule.getChannelid().getChannelid(), minSchedule.getDate(), temSchedu.get(temSchedu.size() - 1).getDate(), minSchedule.getSchedulestarttime());
//                setPlayPermissionForWorkOrder(-102, new Date());

//                if (dta.format(new Date()).equals(schedulList.get(0).getSchedulDate())) {
//                    setPlayPermissionForWorkOrder(-102, dta.parse(schedulList.get(0).getSchedulDate()));
//                }
            }
            msg.setFlag(Boolean.TRUE);
            msg.setMessage(temSchedu.size() + " spots are rescheduled.");
            retObject.setMessageDto(msg);
            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in setRescheduleSpot_V2 ", e.getMessage());
            msg.setFlag(Boolean.FALSE);
            msg.setMessage("Error");
            retObject.setScheduleTempList(new ArrayList<>());
            retObject.setCurrentScheduleList(new ArrayList<>());
            retObject.setRemoveScheduleList(new ArrayList<>());
            retObject.setMessageDto(msg);

            return retObject;
        }
    }

    @Transactional
    public WorkOrderScheduleView setReschedulSoptToToday(String scheduleIds) {
        Message msg = new Message();
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();
        retObject.setRescheduleStatus(true);
        int invalidCount = 0;
        try {
            List<ScheduleDef> schedulTempList = new ArrayList<>();
            String[] stringIds = scheduleIds.split("_");
            for (int i = 0; i < stringIds.length; i++) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date toDay = dateFormat.parse(dateFormat.format(new Date()));

                ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(Integer.parseInt(stringIds[i]));
                if (schedulModel.getWorkorderid().getEnddate().before(toDay)) {
                    invalidCount++;
                    // msg.setFlag(Boolean.FALSE);
                    //msg.setMessage("Invalid (Check work order end date)");
                    //return msg;
                } else {
                    if (schedulModel.getAdvertid().getEnabled() == 1 || (schedulModel.getAdvertid().getEnabled() == 0 && schedulModel.getAdvertid().getStatus() == 2)) {
                        schedulModel.setStatus("7");
                        schedulModel.setComment("Rescheduled");
                        schedulModel.setDate(toDay);

                        if (scheduleSpotValidation(schedulModel)) {
                            if (schedulerDao.updateSchedule(schedulModel)) {
                                //if (schedulerDao.saveSchedule(newModel)) {
                                schedulTempList.add(schedulModel);
                                //}
                            }
                        }
                    }
                }
            }
//            if (PlaylistGenerateForScheduledSpot(schedulTempList)) {
                msg.setFlag(Boolean.TRUE);
                msg.setMessage(schedulTempList.size() + " spots are rescheduled to today.");
                if (invalidCount != 0) {
                    msg.setMessage(msg.getMessage() + " " + invalidCount + " spots invalid (Check work order end date)");
                }
//            } else {
//                msg.setFlag(Boolean.FALSE);
//                msg.setMessage("Try again");
//            }
            retObject.setScheduleTempList(schedulTempList);
            retObject.setCurrentScheduleList(schedulTempList);
            retObject.setRemoveScheduleList(new ArrayList<>());
            retObject.setMessageDto(msg);
            retObject.setMessage(" Schedules Inserted.");
            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in setReschedulSoptToToday ", e.getMessage());
            msg.setFlag(Boolean.FALSE);
            msg.setMessage("Error");

            retObject.setScheduleTempList(new ArrayList<>());
            retObject.setCurrentScheduleList(new ArrayList<>());
            retObject.setRemoveScheduleList(new ArrayList<>());
            retObject.setMessageDto(msg);

            return retObject;
        }
    }

    public Message releseDummySpot(String dummyIDList) {
        try {
            List<ScheduleDef> schedulTempList = new ArrayList<>();
            String[] stringIds = dummyIDList.split("_");
            for (int i = 0; i < stringIds.length; i++) {

                ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(Integer.parseInt(stringIds[i]));
                if (schedulModel.getAdvertid().getEnabled() == 1 && schedulModel.getStatus().equals("8")) {
                    schedulModel.setStatus("6");
                    schedulModel.setComment("Release dummy cut");

                    if (schedulerDao.updateSchedule(schedulModel)) {
                        //if (schedulerDao.saveSchedule(newModel)) {
                        schedulTempList.add(schedulModel);
                        //}
                    }

                }
            }
            Message msg = new Message();
            msg.setFlag(Boolean.TRUE);
            if (!schedulTempList.isEmpty()) {
                msg.setMessage(schedulTempList.size() + " dummy cuts are released.");
            } else {
                msg.setMessage("Select atleast one dummy cut to delete");
            }
            return msg;//PlaylistGenerateForScheduledSpot(schedulTempList);
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in releseDummySpot ", e.getMessage());
            Message msg = new Message();
            msg.setFlag(Boolean.FALSE);
            msg.setMessage("Error");
            return msg;
        }
    }

    public List<WorkOrderSpot> checkSpotWorkOrderSpotAvailability(String[] stringIds) {

        List<ScheduleDef> schedulList = new ArrayList<>();
        List<WorkOrderSpot> soptList = new ArrayList<>();

        //get selected schedule
        for (int i = 0; i < stringIds.length; i++) {
            schedulList.add(schedulerDao.getSelectedSchedule(Integer.parseInt(stringIds[i])));
        }

        if (!schedulList.isEmpty()) {
            //Sort schedul list according to workOrder id;
            Collections.sort(schedulList, new Comparator<ScheduleDef>() {
                @Override
                public int compare(ScheduleDef o1, ScheduleDef o2) {
                    return o1.getWorkorderid().getWorkorderid() - o2.getWorkorderid().getWorkorderid();
                }
            });

            int woID = -1;
            int count = 0;
            for (int j = 0; j < schedulList.size(); j++) {
                if (woID != schedulList.get(j).getWorkorderid().getWorkorderid()) {

                    //Create WO spot model
                    WorkOrderSpot model = new WorkOrderSpot();
                    model.setWorkOrderId(schedulList.get(j).getWorkorderid().getWorkorderid());
                    count++;

                    woID = schedulList.get(j).getWorkorderid().getWorkorderid();
                    //Get selected WO channel list
                    List<WorkOrderChannelList> workOrderchannelSpotCount = workOrderChannelListDao.getSelectedWorkOrderSpotCount(woID);

                    for (int k = 0; k < workOrderchannelSpotCount.size(); k++) {
                        WorkOrderChannelScheduleView channelModel = getScheduleAvailableSpotCount(woID, workOrderchannelSpotCount.get(k).getChannelid().getChannelid());
                        model.setChannelAvailabeSpot(channelModel, workOrderchannelSpotCount.get(k));
                    }
                    model.setChannelSpotCount(schedulList.get(j));
                    soptList.add(model);

                } else {
                    soptList.get(count - 1).setChannelSpotCount(schedulList.get(j));
                }
            }

        }
        return soptList;
    }

    //Validate spot date and time
    public Boolean scheduleSpotValidation(ScheduleDef model) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            Date currentTime = timeFormat.parse(timeFormat.format(new Date()));
            Date date = dateFormat.parse(dateFormat.format(model.getDate()));

            if (toDay.after(date)) {
                return false;
            } else if (toDay.equals(date)) {
                String timeString = timeFormat.format(model.getSchedulestarttime());
                timeString = timeString.replace("30", "00");
                Date time = timeFormat.parse(timeString);
                if (currentTime.after(time)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in scheduleSpotValidation: ", e.getMessage());
            return false;
        }
    }

    // @Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackFor=Exception.class)
    public boolean setSchedule(String schedulData) {
        try {

            ObjectMapper m = new ObjectMapper();
            List<ScheduleInfo> schedulList;
            List<ScheduleDef> schedulTempList = new ArrayList<>();
            schedulList = m.readValue(schedulData, new TypeReference<List<ScheduleInfo>>() {
            });

            for (ScheduleInfo model : schedulList) {
                for (int i = 0; i < model.getAdvertCount(); i++) {

                    ScheduleDef schedule = getScheduleDefModel(model);

                    if (schedulerDao.saveSchedule(schedule)) {
                        schedulTempList.add(schedule);
                        // setSchedule(timeFormat.parse(model.getScheduledStartDate()),timeFormat.parse(model.getScheduledEndDate()),channel,schedule,dateFormat.parse(model.getSchedulDate()));
                    }
                }
            }
            Collections.sort(schedulTempList);
            ScheduleDef minSchedule = schedulerDao.getSelectedSchedule(schedulTempList.get(0).getScheduleid());

            if (minSchedule.getAdvertid().getAdverttype().equals("ADVERT") || minSchedule.getAdvertid().getAdverttype().equals("LOGO")) {
                setDailySchedule(minSchedule.getChannelid().getChannelid(), minSchedule.getDate(), schedulTempList.get(schedulTempList.size() - 1).getDate(), minSchedule.getSchedulestarttime());
            } else {
                CGSpreading(schedulTempList, minSchedule.getChannelid().getChannelid(), minSchedule.getSchedulestarttime(), minSchedule.getScheduleendtime());
            }
            //workOrderStatusCheckService.checkWorkOrder();
            //setDailySchedule(minSchedule.getChannelid().getChannelid(), minSchedule.getDate(), schedulTempList.get(schedulTempList.size() - 1).getDate(), minSchedule.getSchedulestarttime());
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in setSchedule ", e.getMessage());
            return false;
        }
    }

    public int getAvailableSpotCount(ScheduleInfo scheduleInfo) {
        return getAvailableSpotCount(scheduleInfo.getWorkOrderId(), scheduleInfo.getChannelId(), scheduleInfo.getAdvertId());
    }

    public int getAvailableSpotCount(int woId, int channelId, int advertId) {
        WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(woId, channelId);
        Advertisement advert = advertisementDao.getSelectedAdvertisement(advertId);

        int count = 0;
        switch (advert.getAdverttype()) {
            case "ADVERT":
                count = (int) (woChannelSpotsCount.getAvailableTVCDuration() / (advert.getDuration()));
                break;
            case "LOGO":
                count = woChannelSpotsCount.getAvailableLogoSpots();
                break;
            case "CRAWLER":
                count = woChannelSpotsCount.getAvailableCrowlerSpots();
                break;
            case "V_SHAPE":
                count = woChannelSpotsCount.getAvailableV_SqeezeSpots();
                break;
            case "L_SHAPE":
                count = woChannelSpotsCount.getAvailableL_SqeezeSpots();
                break;
            default:
                break;
        }
        return count;
    }

    // save schedule data and update frontend ////////////////refactored
    @Transactional
    public WorkOrderScheduleView saveWOSchedule(String schedulData) {
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();
        int iInsertedCount = 0;

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            ObjectMapper m = new ObjectMapper();
            List<ScheduleInfo> schedulList;
            List<ScheduleDef> schedulTempList = new ArrayList<>();
            List<ScheduleDef> currentschedulList = new ArrayList<>();
            schedulList = m.readValue(schedulData, new TypeReference<List<ScheduleInfo>>() {
            });

            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            Date currentTime = timeFormat.parse(timeFormat.format(new Date()));

            /*Filter expired schedule*/
            for (int i = 0; i < schedulList.size(); i++) {
                Date date = dateFormat.parse(schedulList.get(i).getSchedulDate());

                if (toDay.after(date)) {
                    schedulList.remove(schedulList.get(i));
                    i--;
                } else if (toDay.equals(date)) {
                    String timeString = schedulList.get(i).getScheduledStartDate();
                    //timeString = timeString.replace("30", "00");
                    Date time = timeFormat.parse(timeString);
                    if (currentTime.after(time)) {
                        schedulList.remove(schedulList.get(i));
                        i--;
                    }
                }
            }

            int iAvailableSpotCount = 0;
            if (!schedulList.isEmpty()) {
                iAvailableSpotCount = this.getAvailableSpotCount(schedulList.get(0));
                currentschedulList = schedulerDao.getScheduleListOrderByASC(schedulList.get(0).getWorkOrderId());
                if (currentschedulList == null) {
                    currentschedulList = new ArrayList<>();
                }
            }

            for (ScheduleInfo model : schedulList) {
                for (int i = 0; i < model.getAdvertCount(); i++) {

                    ScheduleDef schedule = getScheduleDefModel(model);

                    if (iAvailableSpotCount > 0 && schedulerDao.saveSchedule(schedule)) {
                        schedulTempList.add(schedule);
                        iAvailableSpotCount--;
                        iInsertedCount++;
                        // setSchedule(timeFormat.parse(model.getScheduledStartDate()),timeFormat.parse(model.getScheduledEndDate()),channel,schedule,dateFormat.parse(model.getSchedulDate()));
                    } else if (iAvailableSpotCount <= 0) {
                        LOGGER.debug("[insert] : AvailableSpotCount = 0");
                    }
                }
            }

            Message msg = new Message();
            msg.setFlag(false);
            if(!schedulTempList.isEmpty()){
                msg.setFlag(true);
                msg.setMessage("Success");
                retObject.setMessageDto(msg);
            }

            retObject.setScheduleTempList(schedulTempList);
            retObject.setCurrentScheduleList(currentschedulList);
            retObject.setRemoveScheduleList(new ArrayList<>());
            retObject.setMessage(iInsertedCount + " Schedules Inserted.");
            //channelUtillizationWidgetService.setChannelUtilizationData();
            return retObject;
        } catch (IOException e) {
            LOGGER.debug("Exception in SchedulerService in saveWOSchedule ", e.getMessage());
            return null;
        } catch(ParseException e){
            LOGGER.debug("Exception in SchedulerService in saveWOSchedule ", e.getMessage());
            return null;
        }catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in saveWOSchedule ", e.getMessage());
            throw e;
        }
    }

    //All spots save and update are done //deprecated
    public Boolean saveAndUpdateSchedule(String schedulData) {
        try {
            //JSON array convert to ScheduleInfo array
            ObjectMapper m = new ObjectMapper();
            List<ScheduleInfo> schedulList;
            List<ScheduleDef> schedulTempList = new ArrayList<>();
            schedulList = m.readValue(schedulData, new TypeReference<List<ScheduleInfo>>() {
            });

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            Date currentTime = timeFormat.parse(timeFormat.format(new Date()));

            /*Filter expired schedule*/
            for (int i = 0; i < schedulList.size(); i++) {
                Date date = dateFormat.parse(schedulList.get(i).getSchedulDate());

                if (toDay.after(date)) {
                    schedulList.remove(schedulList.get(i));
                    i--;
                } else if (toDay.equals(date)) {
                    String timeString = schedulList.get(i).getScheduledStartDate();
                    //timeString = timeString.replace("30", "00");
                    Date time = timeFormat.parse(timeString);
                    if (currentTime.after(time)) {
                        schedulList.remove(schedulList.get(i));
                        i--;
                    }
                }
            }

            for (ScheduleInfo model : schedulList) {
                /*Remove schedule*/
                if (model.getAdvertCount() == -999) {
                    List<Integer> items = model.getScheduleIds();
                    for (int i = 0; i < items.size(); i++) {
                        ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(items.get(i));
                        schedulModel.setStatus("6");
                        schedulerDao.updateSchedule(schedulModel);
                        checkChannelUpdate(schedulModel.getChannelid().getChannelid(), schedulModel.getDate());
                        schedulTempList.add(schedulModel);
                    }

                } else {
                    List<Integer> scheduleIds = model.getScheduleIds();

                    if (scheduleIds.size() == 1 && scheduleIds.get(0).equals("0")) {
                        /*Save new schedule*/
                        for (int i = 0; i < model.getAdvertCount(); i++) {
                            ScheduleDef schedulModel = getScheduleDefModel(model);
                            schedulerDao.saveSchedule(schedulModel);
                            schedulTempList.add(schedulModel);
                        }
                    } else if (scheduleIds.size() > model.getAdvertCount()) {
                        int removeCount = scheduleIds.size() - model.getAdvertCount();
                        for (int i = 0; i < scheduleIds.size(); i++) {

                            if (i < removeCount) {
                                /*Remove schedule*/
                                ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(scheduleIds.get(i));
                                schedulModel.setStatus("6");
                                schedulerDao.updateSchedule(schedulModel);
                                checkChannelUpdate(schedulModel.getChannelid().getChannelid(), schedulModel.getDate());
                                schedulTempList.add(schedulModel);
                            } else {
                                ScheduleDef schedulModel = getScheduleDefModel(model);
                                schedulModel.setScheduleid(scheduleIds.get(i));
                                schedulerDao.updateSchedule(schedulModel);
                                schedulTempList.add(schedulModel);
                            }

                        }

                    } else if (scheduleIds.size() < model.getAdvertCount()) {
                        for (int i = 0; i < model.getAdvertCount(); i++) {

                            if (i < scheduleIds.size()) {
                                ScheduleDef schedulModel = getScheduleDefModel(model);
                                schedulModel.setScheduleid(scheduleIds.get(i));
                                schedulerDao.updateSchedule(schedulModel);
                                schedulTempList.add(schedulModel);

                            } else {
                                ScheduleDef schedulModel = getScheduleDefModel(model);
                                schedulerDao.saveSchedule(schedulModel);
                                schedulTempList.add(schedulModel);
                            }
                        }
                    } else if (scheduleIds.size() == model.getAdvertCount()) {
                        for (int i = 0; i < model.getAdvertCount(); i++) {
                            ScheduleDef schedulModel = getScheduleDefModel(model);
                            schedulModel.setScheduleid(scheduleIds.get(i));
                            schedulerDao.updateSchedule(schedulModel);
                            schedulTempList.add(schedulModel);
                        }
                    }
                }
            }
            if (!schedulTempList.isEmpty()) {
                Collections.sort(schedulTempList);
                ScheduleDef minSchedule = schedulTempList.get(0);
                setDailySchedule(minSchedule.getChannelid().getChannelid(), minSchedule.getDate(), schedulTempList.get(schedulTempList.size() - 1).getDate(), minSchedule.getSchedulestarttime());

                WorkOrder workOrder = minSchedule.getWorkorderid();
                if (workOrder.getAutoStatus() == WorkOrder.AutoStatus.Initial) {
                    workOrder.setAutoStatus(WorkOrder.AutoStatus.IncompleteSchedule);
                    if (workOrderService.setAuditForWorkOrder(workOrder)) {
                        workOrderDao.upadteWorkOrder(workOrder);
                    }
                }
            }
            return true;
        } catch (IOException | ParseException e) {
            LOGGER.debug("Exception in SchedulerService in saveAndUpdateSchedule ", e.getMessage());
            return false;
        }
    }

    public Integer checkProrityScheduleCount(String scheduleIdsData){
        Integer priorityListCount = 0;
        try {
            ObjectMapper m = new ObjectMapper();
            List<Integer> scheduleIds = m.readValue(scheduleIdsData, new TypeReference<List<Integer>>() {
            });
            List<ScheduleDef> scheduleDefList = schedulerDao.getAllSchedulesByIds(scheduleIds);
            for(ScheduleDef scheduleDef : scheduleDefList){
                if(scheduleDef.getPriority() != null){
                    priorityListCount++;
                }
            }
        }catch (Exception e){
            LOGGER.debug("Exception in checkProrityScheduleCount ", e.getMessage());
        }
        return priorityListCount;
    }

    // update schedule and update frontend ////////////////refactored
    public WorkOrderScheduleView updateWOSchedule(String schedulData) {

        WorkOrderScheduleView retObject = new WorkOrderScheduleView();
        int iInsertedCount = 0;
        int iRemovedCount = 0;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            Date currentTime = timeFormat.parse(timeFormat.format(new Date()));

            Calendar cal = Calendar.getInstance();
            cal.setTime(currentTime);
            int currentMinit = cal.get(Calendar.MINUTE);
            if(currentMinit<30){
                cal.add(Calendar.MINUTE, (31-currentMinit));
                currentTime= cal.getTime();
            }

            List<ScheduleDef> schedulTempList = new ArrayList<>();
            List<ScheduleDef> currentScheduleList = new ArrayList<>();
            List<ScheduleDef> removeScheduleList = new ArrayList<>();

            //JSON array convert to ScheduleInfo array
            ObjectMapper m = new ObjectMapper();
            List<ScheduleInfo> schedulList;
            schedulList = m.readValue(schedulData, new TypeReference<List<ScheduleInfo>>() {
            });

            int iAvailableSpotCount = 0;
            if (!schedulList.isEmpty()) {
                Collection<ScheduleInfo> schedules = schedulList.stream().collect(Collectors.toMap(ScheduleInfo::getChannelId, Function.identity(), (sch1, sch2) -> sch1)).values();
                for (ScheduleInfo item : schedules) {
                    iAvailableSpotCount += this.getAvailableSpotCount(item);
                }
//                iAvailableSpotCount += this.getAvailableSpotCount(schedulList.get(0));
                currentScheduleList = schedulerDao.getScheduleListOrderByASC(schedulList.get(0).getWorkOrderId());
            }

            /*Filter expired schedule*/
            for (int i = 0; i < schedulList.size(); i++) {
                Date date = dateFormat.parse(schedulList.get(i).getSchedulDate());

                if (toDay.after(date)) {
                    schedulList.remove(schedulList.get(i));
                    i--;
                } else if (toDay.equals(date)) {
                    String timeString = schedulList.get(i).getScheduledStartDate();
                    //timeString = timeString.replace("30", "00");
                    Date time = timeFormat.parse(timeString);
                    List<Integer> scheduleIds = schedulList.get(i).getScheduleIds();
                    if (currentTime.after(time)) {
                        schedulList.remove(schedulList.get(i));
                        i--;
                    }else {
                        if (scheduleIds.size() != 0) {
                            ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(scheduleIds.get(0));
                            if(currentTime.after(schedulModel.getSchedulestarttime())){
                                schedulList.remove(schedulList.get(i));
                                i--;
                            }
                        }
                    }
                }
            }

            for (ScheduleInfo model : schedulList) {
                /*Remove schedule*/
                if (model.getAdvertCount() == -999) {
                    List<Integer> items = model.getScheduleIds();
                    for (int i = 0; i < items.size(); i++) {
                        ScheduleDef schedulModel = schedulerDao.getSelectedSchedule(items.get(i));
                        schedulModel.setStatus("6");
                        schedulModel.setComment("[update]removed all by user");
                        schedulerDao.updateSchedule(schedulModel);
                        removeScheduleList.add(schedulModel);
                        schedulTempList.add(schedulModel);

                        iAvailableSpotCount++;
                        iRemovedCount++;
                    }
                } else {
                    List<Integer> scheduleIds = model.getScheduleIds();
                    if (scheduleIds.size() == 1 && scheduleIds.get(0).equals("0")) {
                        /*Save new schedule*/
                        for (int i = 0; i < model.getAdvertCount(); i++) {
                            ScheduleDef schedulModel = getScheduleDefModel(model);
                            schedulModel.setComment("[update]inserted by user");
                            if (iAvailableSpotCount > 0 && schedulerDao.saveSchedule(schedulModel)) {
                                schedulTempList.add(schedulModel);
                                iAvailableSpotCount--;
                                iInsertedCount++;
                            }
                        }
                    } else if (scheduleIds.size() > model.getAdvertCount()) {
                        int removeCount = scheduleIds.size() - model.getAdvertCount();
                        List<Integer> tempSchIdList = scheduleIds;
                        List<Integer> tempSchIdListMain = scheduleIds;
                        Collections.sort(tempSchIdList);
                        Collections.sort(tempSchIdListMain);
                        int removedCount = 0;

                        for (int i = 0; i < tempSchIdListMain.size(); i++) {
                            ScheduleDef schedulModel1 = schedulerDao.getSelectedSchedule(tempSchIdListMain.get(i));
                            if (schedulModel1.getPriority() == null) {
                                if(removedCount < removeCount){
                                    schedulModel1.setStatus("6");
                                    schedulModel1.setComment("[update]removed by user");
                                    schedulerDao.updateSchedule(schedulModel1);
                                    removeScheduleList.add(schedulModel1);
                                    schedulTempList.add(schedulModel1);
                                    iAvailableSpotCount++;
                                    iRemovedCount++;
                                    removedCount++;
                                }else{
                                    ScheduleDef schedulModel = getScheduleDefModel(model);
                                    schedulModel.setScheduleid(scheduleIds.get(i));
                                    schedulModel.setComment("[update]updated by user");
                                    schedulModel = getScheduleDefPriority(schedulModel);
                                    schedulerDao.updateSchedule(schedulModel);
                                    schedulTempList.add(schedulModel);
                                }
                            } else {
                                ScheduleDef schedulModel = getScheduleDefModel(model);
                                schedulModel.setScheduleid(scheduleIds.get(i));
                                schedulModel.setComment("[update]updated by user");
                                schedulModel = getScheduleDefPriority(schedulModel);
                                schedulerDao.updateSchedule(schedulModel);
                                schedulTempList.add(schedulModel);
                            }
                        }

                    } else if (scheduleIds.size() < model.getAdvertCount()) {
                        for (int i = 0; i < model.getAdvertCount(); i++) {
                            if (i < scheduleIds.size()) {
                                ScheduleDef schedulModel = getScheduleDefModel(model);
                                schedulModel.setScheduleid(scheduleIds.get(i));
                                schedulModel.setComment("[update]updated by user");
                                schedulModel = getScheduleDefPriority(schedulModel);
                                schedulerDao.updateSchedule(schedulModel);
                                schedulTempList.add(schedulModel);

                            } else {
                                ScheduleDef schedulModel = getScheduleDefModel(model);
                                schedulModel.setComment("[update]inserted by user");
                                if (iAvailableSpotCount > 0 && schedulerDao.saveSchedule(schedulModel)) {
                                    schedulTempList.add(schedulModel);
                                    iAvailableSpotCount--;
                                    iInsertedCount++;
                                }
                            }
                        }
                    } else if (scheduleIds.size() == model.getAdvertCount()) {
                        for (int i = 0; i < model.getAdvertCount(); i++) {
                            ScheduleDef schedulModel = getScheduleDefModel(model);
                            schedulModel.setScheduleid(scheduleIds.get(i));
                            schedulModel.setComment("[update]updated by user");
                            schedulModel = getScheduleDefPriority(schedulModel);
                            schedulerDao.updateSchedule(schedulModel);
                            schedulTempList.add(schedulModel);

                        }
                    }
                }
            }
            retObject.setScheduleTempList(schedulTempList);
            retObject.setCurrentScheduleList(currentScheduleList);
            retObject.setRemoveScheduleList(removeScheduleList);
            return retObject;
        } catch (IOException | ParseException e) {
            LOGGER.debug("Exception in SchedulerService in updateWOSchedule {}", e.getMessage());
        }
        return null;
    }

    public void afterUpdateWOSchedule(WorkOrderScheduleView retObject) {
        try {
            List<ScheduleDef> scheduleTempList = retObject.getScheduleTempList();
            List<ScheduleDef> currentScheduleList = retObject.getCurrentScheduleList();
            List<ScheduleDef> removeScheduleList = retObject.getRemoveScheduleList();
            if (!scheduleTempList.isEmpty()) {
                Collections.sort(scheduleTempList);
                ScheduleDef minSchedule = scheduleTempList.get(0);
                spotSpreadService.recreateDailySchedule(scheduleTempList, currentScheduleList, userDetailsService.getLoginUser().getUserName(), removeScheduleList, retObject.getRescheduleStatus());
                workOrderStatusCheckService.checkWorkOrderStatus(minSchedule.getWorkorderid());
            }
        }catch (Exception e){
            LOGGER.debug("Exception has occurred after spreading spots for work order - {} | exception: {}",retObject.getWorkOrderId(), e.getMessage());
        }
    }

    public Boolean deleteSchedule(int scheduleId) {
        try {

            ScheduleDef model = schedulerDao.getSelectedSchedule(scheduleId);

            return schedulerDao.deleteSchedule(model);
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in deleteSchedule ", e.getMessage());
        }
        return true;
    }

    public Boolean CGSpreading(List<ScheduleDef> schedulList, int iChannelID, Date dtScheduleStartDate, Date dtScheduleEndDate) {
        if (schedulList.isEmpty()) {
            return false;
        }

        List<TimeBelts> lstTimes = timeBeltDao.getTimeBelts(iChannelID, dtScheduleStartDate, dtScheduleEndDate);

        if (lstTimes.isEmpty()) {
            return false;
        }

        Collections.shuffle(schedulList);

        int k = 0;
        for (ScheduleDef schedule_item : schedulList) {
            TimeSlotScheduleMap mapModel = new TimeSlotScheduleMap();
            mapModel.setChannelid(schedule_item.getChannelid());
            mapModel.setScheduleid(schedule_item);
            mapModel.setDate(schedule_item.getDate());
            mapModel.setTimeBelt(lstTimes.get(k));
            mapModel.setWorkOrderId(schedule_item.getWorkorderid().getWorkorderid());
            schedulerDao.saveTimeSlotMap(mapModel);

            k++;
            if (k >= lstTimes.size()) {
                k = 0;
            }
        }
        return true;
    }

    @Async("advertisementTaskExecutor")
    public CompletableFuture<Boolean> setDailyScheduleAsync(int channelId, Date date, Date scheduleEndTime, Date scheduleStartTime) {
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        try{
            completableFuture.complete(setDailySchedule(channelId, date, scheduleEndTime, scheduleStartTime));
        }catch (Exception e){
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

    public Boolean setDailySchedule(int iChannelID, Date dtScheduleStartDate, Date dtScheduleEndDate, Date dtSchedulStartTime) {

        Date dtStartDate = new Date();
        Date dtStartTime = new Date();

        Calendar calDate = Calendar.getInstance();
        calDate.setTime(dtStartDate);
        calDate.set(Calendar.HOUR_OF_DAY, 0);
        calDate.set(Calendar.MINUTE, 0);
        calDate.set(Calendar.SECOND, 0);
        calDate.set(Calendar.MILLISECOND, 0);
        dtStartDate = calDate.getTime();

        Calendar calTime = Calendar.getInstance();
        calTime.setTime(dtStartTime);
        calTime.set(Calendar.YEAR, 1970);
        calTime.set(Calendar.MONTH, 0);
        calTime.set(Calendar.DAY_OF_MONTH, 1);
        calTime.set(Calendar.MINUTE, 0);
        calTime.set(Calendar.SECOND, 0);
        calTime.set(Calendar.MILLISECOND, 0);

        if (dtStartDate.before(dtScheduleStartDate)) {
            dtStartDate.setTime(dtScheduleStartDate.getTime());
            calTime.set(Calendar.HOUR_OF_DAY, 0);
            dtStartTime = calTime.getTime();
        } else {
            calTime.add(Calendar.HOUR_OF_DAY, 1);
            dtStartTime = calTime.getTime();
        }

        Date dtEndDate = new Date();
        dtEndDate.setTime(dtScheduleEndDate.getTime());

        try {
            List<List<ScheduleDef>> lstDailySchedules = schedulerDao.getAllSchedulesBetweenDates(iChannelID, dtStartDate, dtEndDate, dtStartTime, "All");
            List<TimeBelts> timeBelts = timeBeltDao.getTimeBelts(iChannelID);

            for (List<ScheduleDef> lstDaySchedule : lstDailySchedules) {
                for (TimeBelts tBelt : timeBelts) {
                    tBelt.setUtilizedScheduleTime(0);
                }

                Collections.sort(lstDaySchedule);
                Collections.sort(lstDaySchedule,
                        (a, b) -> (int) ((a.getScheduleendtime().getTime() - a.getScheduleendtime().getTime())
                                - (b.getScheduleendtime().getTime() - b.getScheduleendtime().getTime())));

                for (ScheduleDef schedule : lstDaySchedule) {
                    if (schedule.getAdvertid().getAdverttype().equals("LOGO")) {
                        TimeSlotScheduleMap mapModel = new TimeSlotScheduleMap();
                        mapModel.setChannelid(schedule.getChannelid());
                        mapModel.setScheduleid(schedule);
                        mapModel.setDate(schedule.getDate());
                        mapModel.setTimeBelt(timeBeltDao.getTimeBelt(iChannelID,
                                schedule.getSchedulestarttime().getHours(), 0));
                        mapModel.setWorkOrderId(schedule.getWorkorderid().getWorkorderid());
                        schedulerDao.saveTimeSlotMap(mapModel);

                        continue;
                    }

                    Collections.sort(timeBelts);
                    for (TimeBelts tBelt : timeBelts) {
                        if (((schedule.getSchedulestarttime().equals(tBelt.getStartTime())
                                || schedule.getSchedulestarttime().before(tBelt.getStartTime()))
                                && schedule.getScheduleendtime().after(tBelt.getStartTime()))
                                || (schedule.getSchedulestarttime().after(tBelt.getStartTime())
                                && schedule.getSchedulestarttime().before(tBelt.getEndTime()))) {

                            TimeSlotScheduleMap mapModel = new TimeSlotScheduleMap();
                            mapModel.setChannelid(schedule.getChannelid());
                            mapModel.setScheduleid(schedule);
                            mapModel.setDate(schedule.getDate());
                            mapModel.setTimeBelt(tBelt);
                            mapModel.setWorkOrderId(schedule.getWorkorderid().getWorkorderid());

                            schedulerDao.saveTimeSlotMap(mapModel);

                            tBelt.addUtilizedScheduleTime(schedule.getAdvertid().getDuration());
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in setDailySchedule: ", e.getMessage());
            throw e;
        }

        return true;
    }

    public List<RescheduleTimeBelt> getPartiallyProcessedSchedules(Date date){
        return schedulerDao.getPartiallyProcessedSchedules(date);
    }

    public List<RescheduleTimeBelt> getPartiallyProcessedSchedules(Date date, Time time){
        return schedulerDao.getPartiallyProcessedSchedules(date, time);
    }

    public Boolean createTimeSlot(int workOrderId) {

        try {
            List<ScheduleDef> schedulTempList = schedulerDao.getScheduleListOrderByASC(workOrderId);
            ScheduleDef minSchedule = Collections.min(schedulTempList);
            setDailySchedule(minSchedule.getChannelid().getChannelid(), minSchedule.getDate(), Collections.max(schedulTempList).getDate(), minSchedule.getSchedulestarttime());
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in createTimeSlot: ", e.getMessage());
            return false;
        }
    }

    public Boolean playListGenerateCaller(Date today) {
        try {
            logger4g.debug("Started playListGenerateCaller method execution | ( current time : {} )", new Date().toString());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date nextDay = dateFormat.parse(addDay(today, 1));
            logger4g.debug("Invoking setPlayPermissionForWorkOrder | ( wid : {} ) | ( next day : {} ) | ( current time : {} )",
                    -102, nextDay, new Date().toString());
            //setPlayPermissionForWorkOrder(-102, nextDay);
            setPlayPermissionForWorkOrderV2(-102, nextDay);
            logger4g.debug("Completed playListGenerateCaller method execution | ( current time : {} )", new Date().toString());
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in playListGenerateCaller: ", e.getMessage());
            return false;
        }
    }

    public Boolean setPlayPermissionForWorkOrderV2(int workOrderId, Date today) {
        try {
            List<TimeSlotScheduleMap> slectedScheduleList;
            List<PlayList> playList;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            if (workOrderId != -102) {
                slectedScheduleList = schedulerDao.getTimeBeltMapFromDate(workOrderId, today);
                playList = playListScheduleDao.getPlayList(workOrderId);
            } else {
                slectedScheduleList = schedulerDao.getTimeBeltMapFromDate(today);
                logger4g.info("Scheduled spot count: " + slectedScheduleList.size());
                playList = playListScheduleDao.getPlayList(dateFormat.parse(dateFormat.format(today)));
                logger4g.info("playList spot count: " + playList.size());
            }

            for (int i = 0; i < slectedScheduleList.size(); i++) {
                processPlayLit(playList, slectedScheduleList, slectedScheduleList.get(i));
            }

            for (TimeSlotScheduleMap item : slectedScheduleList) {

                ScheduleDef dataModel = item.getScheduleid();
                logger4g.info("SchedulelId: " + dataModel.getScheduleid());
                PlayList model = new PlayList();
                model.setWorkOrder(dataModel.getWorkorderid());
                model.setAdvert(dataModel.getAdvertid());
                model.setDate(dataModel.getDate());
                model.setChannel(dataModel.getChannelid());
                model.setScheduleStartTime(dataModel.getSchedulestarttime());
                model.setScheduleEndTime(dataModel.getScheduleendtime());
                model.setSchedule(dataModel);

                model.addConflicting_schedules(1);

                model.setStatus(dataModel.getStatus());

                model.setComment("Status :" + model.getStatus() + " Time :" + new Date().toString());
                model.setPlayCluster(-1);
                model.setPlayOrder(-1);
                model.setActualEndTime(item.getTimeBelt().getEndTime());
                model.setActualStartTime(item.getTimeBelt().getStartTime());
                model.setScheduleHour(item.getTimeBelt().getHour());
                model.setTimeBeltEndTime(item.getTimeBelt().getEndTime());
                model.setTimeBeltStartTime(item.getTimeBelt().getStartTime());

                playListScheduleDao.saveAndUpdatePlayList(model);
            }
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in setPlayPermissionForWorkOrder: ", e.getMessage());
            return false;
        }
    }

    @Transactional
    public Boolean setPlayPermissionForWorkOrder(int workOrderId, Date today) {
        try {
            List<TimeSlotScheduleMap> slectedScheduleList;
            List<PlayList> playList;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            if (workOrderId != -102) {
                slectedScheduleList = schedulerDao.getTimeBeltMapFromDate(workOrderId, today);
                playList = playListScheduleDao.getPlayList(workOrderId);
            } else {
                slectedScheduleList = schedulerDao.getTimeBeltMapFromDate(today);
                logger4g.info("Scheduled spot count: " + slectedScheduleList.size());
                playList = playListScheduleDao.getPlayList(dateFormat.parse(dateFormat.format(today)));
                logger4g.info("playList spot count: " + playList.size());
            }

            for (int i = 0; i < slectedScheduleList.size(); i++) {
                for (int j = 0; j < playList.size(); j++) {
                    int playListScheduleID = playList.get(j).getSchedule().getScheduleid();
                    int scheduleID = slectedScheduleList.get(i).getScheduleid().getScheduleid();

                    int scheduleHour = slectedScheduleList.get(i).getTimeBelt().getHour();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    int hours = cal.get(Calendar.HOUR_OF_DAY);

                    if (playListScheduleID == scheduleID) {
                        if ((playList.get(j).getStatus().equals("0") || playList.get(j).getStatus().equals("9") || playList.get(j).getStatus().equals("7")) && scheduleHour > hours) {

                            playList.get(j).setWorkOrder(slectedScheduleList.get(i).getScheduleid().getWorkorderid());
                            playList.get(j).setAdvert(slectedScheduleList.get(i).getScheduleid().getAdvertid());
                            playList.get(j).setDate(slectedScheduleList.get(i).getDate());
                            playList.get(j).setChannel(slectedScheduleList.get(i).getChannelid());
                            playList.get(j).setScheduleStartTime(slectedScheduleList.get(i).getScheduleid().getSchedulestarttime());
                            playList.get(j).setScheduleEndTime(slectedScheduleList.get(i).getScheduleid().getScheduleendtime());
                            playList.get(j).setSchedule(slectedScheduleList.get(i).getScheduleid());
                            playList.get(j).setScheduleHour(slectedScheduleList.get(i).getTimeBelt().getHour());
                            playList.get(j).setStatus(slectedScheduleList.get(i).getScheduleid().getStatus());

                        }

                        if (playListScheduleDao.saveAndUpdatePlayList(playList.get(j))) {
                            if (slectedScheduleList.remove(slectedScheduleList.get(i))) {
                                i--;
                                break;
                            }
                        }
                    }
                }
            }

            for (TimeSlotScheduleMap item : slectedScheduleList) {

                ScheduleDef dataModel = item.getScheduleid();
                logger4g.info("SchedulelId: " + dataModel.getScheduleid());
                PlayList model = new PlayList();
                model.setWorkOrder(dataModel.getWorkorderid());
                model.setAdvert(dataModel.getAdvertid());
                model.setDate(dataModel.getDate());
                model.setChannel(dataModel.getChannelid());
                model.setScheduleStartTime(dataModel.getSchedulestarttime());
                model.setScheduleEndTime(dataModel.getScheduleendtime());
                model.setSchedule(dataModel);

                model.addConflicting_schedules(1);

                model.setStatus(dataModel.getStatus());

                model.setComment("Status :" + model.getStatus() + " Time :" + new Date().toString());
                model.setPlayCluster(-1);
                model.setPlayOrder(-1);
                model.setActualEndTime(item.getTimeBelt().getEndTime());
                model.setActualStartTime(item.getTimeBelt().getStartTime());
                model.setScheduleHour(item.getTimeBelt().getHour());
                model.setTimeBeltEndTime(item.getTimeBelt().getEndTime());
                model.setTimeBeltStartTime(item.getTimeBelt().getStartTime());

                playListScheduleDao.saveAndUpdatePlayList(model);
            }
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in setPlayPermissionForWorkOrder: ", e.getMessage());
            return false;
        }
    }

/*    public Boolean clearPlaylist() {
        logger4g.debug("Started clearPlaylist method execution | ( current time : {} )", new Date().toString());
        try {
            List<PlayList> playList;
            playList = playListScheduleDao.getPlayList();
            for (PlayList model : playList) {
                PlayListHistory historyModel = new PlayListHistory();
                historyModel.setPlaylistid(model.getPlaylistId());
                historyModel.setChannelid(model.getChannel().getChannelid());
                historyModel.setWorkorderid(model.getWorkOrder().getWorkorderid());
                historyModel.setAdvertid(model.getAdvert().getAdvertid());
                historyModel.setScheduleid(model.getSchedule());
                historyModel.setSchedulehour(model.getScheduleHour());
                historyModel.setSchedulestarttime(model.getScheduleStartTime());
                historyModel.setScheduleendtime(model.getScheduleEndTime());
                historyModel.setTimeBeltstarttime(model.getTimeBeltStartTime());
                historyModel.setTimeBeltendtime(model.getTimeBeltEndTime());
                historyModel.setActualstarttime(model.getActualStartTime());
                historyModel.setActualendtime(model.getActualEndTime());
                historyModel.setStatus(model.getStatus());
                historyModel.setPlaycluster(model.getPlayCluster());
                historyModel.setPlayorder(model.getPlayOrder());
                historyModel.setComment(model.getComment());
                historyModel.setDate(model.getDate());
                logger4g.info("Save " + historyModel.toString());
                if (playListScheduleDao.savePlayListHistory(historyModel)) {
                    logger4g.info("Delete " + model.toString());
                    playListScheduleDao.deletePlayList(model);
                }
            }
            logger4g.debug("Completed clearPlaylist method execution | ( current time : {} )", new Date().toString());
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in clearPlaylist: ", e.getMessage());
            return false;
        }
    }*/

    public Boolean clearPlaylist() {
        logger4g.debug("Started clearPlaylist method execution | ( current time : {} )", new Date().toString());
        try {
            int plCount = playListScheduleDao.getPlayListCount();
            int offset = 0;
            final int limit = 4000;
            while (offset < plCount) {
                List<PlayList> playLists = playListScheduleDao.getPlayLists(limit, 0);
                if(!playLists.isEmpty()){
                    clearPlayList(playLists);
                }
                offset += limit ;
            }
            logger4g.debug("Completed clearPlaylist method execution | ( current time : {} )", new Date().toString());
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in clearPlaylist: ", e.getMessage());
            return false;
        }
    }

    public Boolean clearPlayList(List<PlayList> playList) {
        List<PlayListHistory> playListHistoryList = new ArrayList<>();
        List<Integer> playListIds = new ArrayList<>();
        for (PlayList model : playList) {
            PlayListHistory historyModel = new PlayListHistory();
            historyModel.setPlaylistid(model.getPlaylistId());
            historyModel.setChannelid(model.getChannel().getChannelid());
            historyModel.setWorkorderid(model.getWorkOrder().getWorkorderid());
            historyModel.setAdvertid(model.getAdvert().getAdvertid());
            historyModel.setScheduleid(model.getSchedule());
            historyModel.setSchedulehour(model.getScheduleHour());
            historyModel.setSchedulestarttime(model.getScheduleStartTime());
            historyModel.setScheduleendtime(model.getScheduleEndTime());
            historyModel.setTimeBeltstarttime(model.getTimeBeltStartTime());
            historyModel.setTimeBeltendtime(model.getTimeBeltEndTime());
            historyModel.setActualstarttime(model.getActualStartTime());
            historyModel.setActualendtime(model.getActualEndTime());
            historyModel.setStatus(model.getStatus());
            historyModel.setPlaycluster(model.getPlayCluster());
            historyModel.setPlayorder(model.getPlayOrder());
            historyModel.setComment(model.getComment());
            historyModel.setLabel(model.getLable());
            historyModel.setDate(model.getDate());
            playListHistoryList.add(historyModel);
            playListIds.add(model.getPlaylistId());
        }
        if(playListScheduleDao.savePlayListHistory(playListHistoryList)){
            playListScheduleDao.deletePlayLists(playListIds);
        }
        return true;
    }

    public Boolean refreshTimeBelt(int channelId) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

            ArrayList<TimeBelts> timeBelts = (ArrayList<TimeBelts>) timeBeltDao.getTimeBelts(channelId);
            for (TimeBelts item : timeBelts) {
                try {
                    item.setIsScheduleGenerated(false);
                    item.setScheduleGeneratedTime(format.parse(dateFormat.format(new Date())));
                    timeBeltDao.updateTimeBelt(item);

                } catch (ParseException ex) {
                    Logger.getLogger(PlayListService.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in refreshTimeBelt: ", e.getMessage());
            return false;
        }
    }

    public List<String> getAvailableAdvertSpot(int workOrderId, int advertId) throws ParseException {
        //Get scheduled original spot according to WO and AdvertId
        List<String> msg = new ArrayList<>();
        List<ScheduleDef> scheduleList = schedulerDao.getAvailabilitySelectedAdvert(workOrderId, advertId);
        msg.add("" + scheduleList.size());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        //Select valid spot for the replace
        for (int i = 0; i < scheduleList.size(); i++) {

            if (scheduleList.get(i).getComment().equals("Replace another Advert") || !(scheduleList.get(i).getStatus().equals("0") || scheduleList.get(i).getStatus().equals("7"))) {
                scheduleList.remove(scheduleList.get(i));
                i--;
            } else {
                Date dt = scheduleList.get(i).getDate();
                Date tm = scheduleList.get(i).getSchedulestarttime();
                if (dt.toString().equals(dateFormat.format(new Date()))) {
                    if (tm.before(timeFormat.parse(timeFormat.format(new Date())))) {
                        scheduleList.remove(scheduleList.get(i));
                        i--;
                    }
                } else {
                    if (dt.before(dateFormat.parse(dateFormat.format(new Date())))) {
                        scheduleList.remove(scheduleList.get(i));
                        i--;
                    }
                }
            }
        }
        msg.add("" + scheduleList.size());

        return msg;
    }

    public Boolean PlaylistGenerateForScheduledSpot(List<ScheduleDef> schedulTempList) {
        try {
            if (!schedulTempList.isEmpty()) {

                //Sort schedul list according to channel id;
                Collections.sort(schedulTempList, new Comparator<ScheduleDef>() {
                    @Override
                    public int compare(ScheduleDef o1, ScheduleDef o2) {
                        return o1.getChannelid().getChannelid() - o2.getChannelid().getChannelid();
                    }
                });

                List<ScheduleDef> tempList = new ArrayList<>();

                //Assign channel id;
                int channelId = schedulTempList.get(0).getChannelid().getChannelid();
                for (int i = 0; i < schedulTempList.size(); i++) {
                    if (channelId == schedulTempList.get(i).getChannelid().getChannelid()) {

                        //Same channel's spot add to tempList
                        tempList.add(schedulTempList.get(i));
                    } else {
                        //Change channelId
                        channelId = schedulTempList.get(i).getChannelid().getChannelid();

                        //Sort schedul list according to scheduled date;
                        Collections.sort(tempList);

                        //Gererate schedule
                        setDailySchedule(tempList.get(0).getChannelid().getChannelid(), tempList.get(0).getDate(), tempList.get(tempList.size() - 1).getDate(), tempList.get(0).getSchedulestarttime());

                        //TempList clear aand add new channel object to tempList
                        tempList.clear();
                        tempList.add(schedulTempList.get(i));
                    }
                }

                Collections.sort(tempList);
                ScheduleDef minSchedule = tempList.get(0);
                setDailySchedule(minSchedule.getChannelid().getChannelid(), minSchedule.getDate(), tempList.get(tempList.size() - 1).getDate(), minSchedule.getSchedulestarttime());

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateFormat.format(new Date());

                Collections.sort(schedulTempList);
                setPlayPermissionForWorkOrder(-102, dateFormat.parse(date));
            }
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in PlaylistGenerateForScheduledSpot: ", e.getMessage());
            return false;
        }
    }

    public Boolean setAuditForSchedule(ScheduleDef model) {
        try {
            ScheduleDef dataModel = schedulerDao.getSelectedSchedule(model.getScheduleid());

            for (Field field : (ScheduleDef.class).getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getAnnotation(Column.class) != null && (!field.get(model).equals(field.get(dataModel)))) {
                    GeneralAudit auditModel = new GeneralAudit("schedule_def", model.getScheduleid());

                    auditModel.setField(field.getAnnotation(Column.class).name());
                    auditModel.setOldData(field.get(dataModel).toString());
                    auditModel.setNewData(field.get(model).toString());

                    generalAuditDao.saveGeneralAudit(auditModel);
                }
            }

            return true;
        } catch (IllegalAccessException e) {
            LOGGER.debug("Exception in SchedulerService in setAuditForSchedule: ", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in setAuditForSchedule: ", e.getMessage());
            throw e;
        }

        return false;
    }

    //Task_144
    public List<TimeBeltUtilization> getScheduleSlotUtilization(int iWorkOrderId) { //tested
        try {
//            List<TimeBeltUtilization> lstReturn = new ArrayList<>();
//            WorkOrder pWorkOrder = workOrderDao.getSelectedWorkOrder(iWorkOrderId);
//            ScheduleAdminViewInfo dataModel = new ScheduleAdminViewInfo(iWorkOrderId, -111, -111, pWorkOrder.getStartdate().toString(), pWorkOrder.getEnddate().toString());
//
//            List<TimeSlotScheduleMap> lstTimeBeltMap = schedulerDao.getFinalAllSchedule(dataModel);
//            Collections.sort(lstTimeBeltMap);
//
//            int tmpTimeBetlId = -1;
//            TimeBeltUtilization viewModel = null;
//
//            for (TimeSlotScheduleMap tempModel : lstTimeBeltMap) {
//                if (tmpTimeBetlId != tempModel.getTimeBelt().getTimeBeltId()) {
//                    viewModel = createTimeBeltUtilizationView(tempModel, pWorkOrder.getStartdate(), pWorkOrder.getEnddate());
//                    tmpTimeBetlId = tempModel.getTimeBelt().getTimeBeltId();
//                    lstReturn.add(viewModel);
//                }
//
//                String temp_date = new SimpleDateFormat("yyyy-MM-dd").format(tempModel.getDate());
//                for (TimeBeltUtilizationItem item : viewModel.getUtilizationItems()) {
//                    if (temp_date.equals(item.getDate())) {
//                        item.addOne();
//                        item.addUtilizedAdvertTimeinSeconds(tempModel.getScheduleid().getAdvertid().getDuration());
//                    }
//                }
//            }
//            return lstReturn;
            //return setUtilization(iWorkOrderId);
            return setUtilizationV2(iWorkOrderId);
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in getScheduleSlotUtilization: ", e.getMessage());
            throw e;
        }
    }

    public List<TimeBeltUtilization> setUtilizationV2(int iWorkOrderId) {

        List<TimeBeltUtilization> lstReturn = new ArrayList<>();
        WorkOrder pWorkOrder = workOrderDao.getSelectedWorkOrder(iWorkOrderId);
        ScheduleAdminViewInfo dataModel = new ScheduleAdminViewInfo(iWorkOrderId, -111, -111, pWorkOrder.getStartdate().toString(), pWorkOrder.getEnddate().toString());

//        change in inventory
//                String tempDate = new SimpleDateFormat("yyyy-MM-dd").format(tempModel.getDate());
//                for (TimeBeltUtilizationItem item : viewModel.getUtilizationItems()) {
//                    if (tempDate.equals(item.getDate())) {
//                        item.addOne();
//                        item.addUtilizedAdvertTimeinSeconds(tempModel.getScheduleid().getAdvertid().getDuration());
//                    }
//                }

        List<TimeSlotScheduleMapDto> tssList = schedulerDao.getFinalAllScheduleV2(dataModel);
        Collections.sort(tssList);

        int tmpTimeBetlId = -1;
        TimeBeltUtilization viewModel = null;

        if (!tssList.isEmpty()) {
            tmpTimeBetlId = tssList.get(0).getTimeBeltId();
        }

        for (int i = 1; i < tssList.size(); i++) {
            if (tssList.get(i).getTimeBeltId() == tmpTimeBetlId) {
                tssList.remove(i);
                i--;
            } else {
                tmpTimeBetlId = tssList.get(i).getTimeBeltId();
            }
        }

        tmpTimeBetlId = -1;
        List<Integer> scheduleIds = new ArrayList<>();
        for (TimeSlotScheduleMapDto scheduleMap : tssList) {
            scheduleIds.add(scheduleMap.getScheduleId());
        }
        if (!scheduleIds.isEmpty()) {
            List<TimeSlotScheduleMap> timeSlotScheduleMaps = schedulerDao.getSchedulesByIds(scheduleIds);
            for (TimeSlotScheduleMap tempModel : timeSlotScheduleMaps) {
                if (tmpTimeBetlId != tempModel.getTimeBelt().getTimeBeltId()) {
                    viewModel = createTimeBeltUtilizationView(tempModel, pWorkOrder.getStartdate(), pWorkOrder.getEnddate());
                    tmpTimeBetlId = tempModel.getTimeBelt().getTimeBeltId();
                    lstReturn.add(viewModel);
                }
            }
        }
        return lstReturn;
    }

    @Deprecated //this method is replaced with the method setUtilizationV2
    public List<TimeBeltUtilization> setUtilization(int iWorkOrderId) {

        List<TimeBeltUtilization> lstReturn = new ArrayList<>();
        WorkOrder pWorkOrder = workOrderDao.getSelectedWorkOrder(iWorkOrderId);
        ScheduleAdminViewInfo dataModel = new ScheduleAdminViewInfo(iWorkOrderId, -111, -111, pWorkOrder.getStartdate().toString(), pWorkOrder.getEnddate().toString());

//        change in inventory
//                String tempDate = new SimpleDateFormat("yyyy-MM-dd").format(tempModel.getDate());
//                for (TimeBeltUtilizationItem item : viewModel.getUtilizationItems()) {
//                    if (tempDate.equals(item.getDate())) {
//                        item.addOne();
//                        item.addUtilizedAdvertTimeinSeconds(tempModel.getScheduleid().getAdvertid().getDuration());
//                    }
//                }
        List<TimeSlotScheduleMap> lstTimeBeltMap = schedulerDao.getFinalAllSchedule(dataModel);
        Collections.sort(lstTimeBeltMap);

        int tmpTimeBetlId = -1;
        TimeBeltUtilization viewModel = null;

        if (!lstTimeBeltMap.isEmpty()) {
            tmpTimeBetlId = lstTimeBeltMap.get(0).getTimeBelt().getTimeBeltId();
        }

        for (int i = 1; i < lstTimeBeltMap.size(); i++) {
            if (lstTimeBeltMap.get(i).getTimeBelt().getTimeBeltId() == tmpTimeBetlId) {
                lstTimeBeltMap.remove(i);
                i--;
            } else {
                tmpTimeBetlId = lstTimeBeltMap.get(i).getTimeBelt().getTimeBeltId();
            }
        }

        tmpTimeBetlId = -1;
        for (TimeSlotScheduleMap tempModel : lstTimeBeltMap) {
            if (tmpTimeBetlId != tempModel.getTimeBelt().getTimeBeltId()) {
                viewModel = createTimeBeltUtilizationView(tempModel, pWorkOrder.getStartdate(), pWorkOrder.getEnddate());
                tmpTimeBetlId = tempModel.getTimeBelt().getTimeBeltId();
                lstReturn.add(viewModel);
            }
        }

        return lstReturn;
    }

    public TimeBeltUtilization createTimeBeltUtilizationView(TimeSlotScheduleMap dataModel, Date startDate, Date endDate) {

        TimeBeltUtilization model = new TimeBeltUtilization();
        model.setTimeBeltId(dataModel.getTimeBelt().getTimeBeltId());

        String startDay = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
        String endDay = new SimpleDateFormat("yyyy-MM-dd").format(endDate);

        InventoryRow inventoryRow = null;
        NashInventoryConsumption nashInventoryConsumption = null;

        Time fromTime = new Time(dataModel.getTimeBelt().getStartTime().getHours(), dataModel.getTimeBelt().getStartTime().getMinutes(), 0);
        Time toTime = new Time(dataModel.getTimeBelt().getEndTime().getHours(), dataModel.getTimeBelt().getStartTime().getMinutes(), 0);
        int i = 0;

        WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(dataModel.getWorkOrderId());
        ////////////////////====================new inventory system============================//////////////////////////////
        if (workOrder.getInventoryType() == WorkOrder.InventoryType.NEW) {
           /* while (!endDay.equals(startDay)) {

                startDay = this.addDay(startDate, i);
                try {
                    inventoryRow = inventoryService.findByChannelAndDateAndTime(dataModel.getChannelid().getChannelid(), new SimpleDateFormat("yyyy-MM-dd").parse(startDay), fromTime, toTime);
                    nashInventoryConsumption = nashInventoryConsumptionService.findByInventoryRowId(inventoryRow.getId());
                } catch (ParseException e) {
                    LOGGER.debug("Exception in SchedulerService in createTimeBeltUtilizationView: ", e.getMessage());
                }
                TimeBeltUtilizationItem itemModel = new TimeBeltUtilizationItem();
                itemModel.setDate(startDay);
                if (inventoryRow != null && nashInventoryConsumption != null) {
                    long totalDuration = (long) (inventoryRow.getTotalTvcDuration() - inventoryRow.getWebTvcDuration());
                    itemModel.setTlTime(new Date(totalDuration * 1000));
                    itemModel.addUtilizedAdvertTimeinSeconds((long) nashInventoryConsumption.getSpentTime());
                } else {
                    itemModel.setTlTime(new Date(dataModel.getTimeBelt().getTotalScheduleTime() * 1000));
                }
                model.getUtilizationItems().add(itemModel);
                i++;
            }*/
        } else {
            ////////////////////====================old inventory system============================//////////////////////////////

            while (!endDay.equals(startDay)) {
                TimeBeltUtilizationItem itemModel = new TimeBeltUtilizationItem();
                startDay = this.addDay(startDate, i);
                itemModel.setDate(startDay);
                itemModel.setTlTime(new Date(dataModel.getTimeBelt().getTotalScheduleTime() * 1000));
                model.getUtilizationItems().add(itemModel);
                i++;
            }
        }
        return model;
    }

    public Boolean generateExcwl(HttpServletRequest request, String fromDate, String toDate, String channelIDs, int advertID, int[] clientIDs, String hour) throws Exception {
        return new ExcelGenerater().writeMissedSpotInExcel(getMissedSpot(fromDate, toDate, channelIDs, advertID, clientIDs, hour), request);
    }

    public void downloadMissedSpotExcel(HttpServletRequest request, HttpServletResponse response) {
        try {

            String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
            String fullPath = webServerPath + "/Excell/MissedSpotExcell.xls";

            File downloadFile = new File(fullPath);
            FileInputStream inputStream = new FileInputStream(downloadFile);

            String mimeType = "application/octet-stream";

            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outStream.close();

        } catch (IOException e) {
            LOGGER.debug("Exception in SchedulerService in downloadMissedSpotExcel: ", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in downloadMissedSpotExcel: ", e.getMessage());
            throw e;
        }
    }

    public PreScheduleInsertView getPreScheduleInsertView(int iWOId, int iChnnelID, int iAdvertId) {
        Advertisement advert = advertisementDao.getSelectedAdvertisement(iAdvertId);
        WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(iWOId, iChnnelID);

        PreScheduleInsertView schedInsertView = new PreScheduleInsertView();
        schedInsertView.setChannelId(iChnnelID);
        schedInsertView.setAdvertDuration(advert.getDuration());

        if (advert.getAdverttype().equals("ADVERT")) {
            Integer iInitialTvcBreackdown = woChannelSpotsCount.getTvcBreackdownMap().get(advert.getDuration());
            if (iInitialTvcBreackdown == null) {
                iInitialTvcBreackdown = 0;
            }
            Integer iAvailableTvcBreackdown = woChannelSpotsCount.getTvcBreackdownMapAvailable().get(advert.getDuration());
            if (iAvailableTvcBreackdown == null || iAvailableTvcBreackdown < 0) {
                iAvailableTvcBreackdown = 0;
            }
            schedInsertView.setInitialSpotCount(iInitialTvcBreackdown);
            schedInsertView.setAvailableSpotCount(iAvailableTvcBreackdown);
            schedInsertView.setInitialScheduleTime(woChannelSpotsCount.getTotalTVCDuration());
            schedInsertView.setAvailableScheduleTime(woChannelSpotsCount.getAvailableTVCDuration());

            int iAvailableSpotCount = (int) (woChannelSpotsCount.getAvailableTVCDuration() / (advert.getDuration()));
            schedInsertView.setValidSlotCount(iAvailableSpotCount);

        } else {
            if (advert.getAdverttype().equals("LOGO")) {
                schedInsertView.setInitialSpotCount(woChannelSpotsCount.getLogoSpots());
                schedInsertView.setAvailableSpotCount(woChannelSpotsCount.getAvailableLogoSpots());

            } else if (advert.getAdverttype().equals("CRAWLER")) {
                schedInsertView.setInitialSpotCount(woChannelSpotsCount.getCrowlerSpots());
                schedInsertView.setAvailableSpotCount(woChannelSpotsCount.getAvailableCrowlerSpots());

            } else if (advert.getAdverttype().equals("V_SHAPE")) {
                schedInsertView.setInitialSpotCount(woChannelSpotsCount.getV_sqeezeSpots());
                schedInsertView.setAvailableSpotCount(woChannelSpotsCount.getAvailableV_SqeezeSpots());

            } else if (advert.getAdverttype().equals("L_SHAPE")) {
                schedInsertView.setInitialSpotCount(woChannelSpotsCount.getL_sqeezeSpots());
                schedInsertView.setAvailableSpotCount(woChannelSpotsCount.getAvailableL_SqeezeSpots());
            }
            schedInsertView.setInitialScheduleTime(0);
            schedInsertView.setAvailableScheduleTime(0);
            schedInsertView.setValidSlotCount(schedInsertView.getAvailableSpotCount());
        }
        schedInsertView.setNewSlotCount(0);
        return schedInsertView;
    }

    /// valid slot count for insertion
    public List<PreScheduleInsertView> prePatternInsert(String schedulData) throws Exception {
        HashMap<Integer, PreScheduleInsertView> mapSchedInsert = new HashMap<>();

        try {
            ObjectMapper m = new ObjectMapper();
            SchedulePatternInfo schedulPattern = m.readValue(schedulData, SchedulePatternInfo.class);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

            ///// validate start end dates ///////////////////////////////////////////
            Date dtStartDate = dateFormat.parse(schedulPattern.getScheduleStartDate());
            Date dtEndDate = dateFormat.parse(schedulPattern.getScheduleEndDate());
            Date dtStartTime = timeFormat.parse(schedulPattern.getScheduleStartTime());
            Date dtEndTime = timeFormat.parse(schedulPattern.getScheduleEndTime());
            Date dtCurrentDateTime = new Date();

            WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(schedulPattern.getWorkOrderId());
            checkAndUpdateWO(schedulPattern.getWorkOrderId());
            if (dtStartDate.before(dtCurrentDateTime)) {
                Calendar calDate = Calendar.getInstance();
                calDate.setTime(dateFormat.parse(dateFormat.format(dtCurrentDateTime)));
                if (dtCurrentDateTime.getHours() >= dtStartTime.getHours()) {
                    calDate.add(Calendar.DAY_OF_MONTH, 1);
                }
                dtStartDate = calDate.getTime();
            }

            if (workOrder.getEnddate().before(dtEndDate)) {
                Calendar calDate = Calendar.getInstance();
                calDate.setTime(workOrder.getEnddate());
                dtEndDate = calDate.getTime();
            }
            //// iterate through start end dates //////////////////////////////////////////
            Calendar clStartDate = Calendar.getInstance();
            clStartDate.setTime(dtStartDate);
            Calendar clEndDate = Calendar.getInstance();
            clEndDate.setTime(dtEndDate);

            HashSet<Integer> hsDays = new HashSet<>(schedulPattern.getLstDays());
            HashMap<Integer, Integer> mapInsertedSpotCount = new HashMap<>();
            Advertisement advert = advertisementDao.getSelectedAdvertisement(schedulPattern.getAdvertId());

            for (int id : schedulPattern.getLstChannelIds()) {
                ChannelDetails channel = channelDetailDAO.getChannel(id);
                PreScheduleInsertView schedInsertView = getPreScheduleInsertView(schedulPattern.getWorkOrderId(), id, schedulPattern.getAdvertId());
                schedInsertView.setChannelName(channel.getChannelname());
                mapSchedInsert.put(id, schedInsertView);

                mapInsertedSpotCount.put(id, 0);
            }

            for (Calendar clDate = clStartDate; !clStartDate.after(clEndDate); clStartDate.add(Calendar.DATE, 1)) {
                int dayOfWeek = clDate.get(Calendar.DAY_OF_WEEK);
                if (hsDays.contains(dayOfWeek)) {
                    for (int channelId : schedulPattern.getLstChannelIds()) {
                        mapInsertedSpotCount.put(channelId, mapInsertedSpotCount.get(channelId) + schedulPattern.getSlotCount());
                    }
                }
            }

            for (int id : schedulPattern.getLstChannelIds()) {
                PreScheduleInsertView schedInsertView = mapSchedInsert.get(id);
                schedInsertView.setNewSlotCount(mapInsertedSpotCount.get(id));
                if (schedInsertView.getValidSlotCount() > schedInsertView.getNewSlotCount()) {
                    schedInsertView.setValidSlotCount(schedInsertView.getNewSlotCount());
                }
            }

            return new ArrayList(mapSchedInsert.values());
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in prePatternInsert: ", e.getMessage());
            throw e;
        }
    }

    // save schedule pattern and update frontend 
    public WorkOrderScheduleView saveSchedulePattern(String schedulData) throws Exception {
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();

        int iInsertedCount = 0;
        try {
            ObjectMapper m = new ObjectMapper();
            SchedulePatternInfo schedulPattern = m.readValue(schedulData, SchedulePatternInfo.class);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

            ///// validate start end dates ///////////////////////////////////////////
            Date dtStartDate = dateFormat.parse(schedulPattern.getScheduleStartDate());
            Date dtEndDate = dateFormat.parse(schedulPattern.getScheduleEndDate());
            Date dtStartTime = timeFormat.parse(schedulPattern.getScheduleStartTime());
            Date dtEndTime = timeFormat.parse(schedulPattern.getScheduleEndTime());
            Date dtCurrentDateTime = new Date();

            WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(schedulPattern.getWorkOrderId());
            checkAndUpdateWO(schedulPattern.getWorkOrderId());
            if (dtStartDate.before(dtCurrentDateTime)) {
                Calendar calDate = Calendar.getInstance();
                calDate.setTime(dateFormat.parse(dateFormat.format(dtCurrentDateTime)));
                if (dtCurrentDateTime.getHours() >= dtStartTime.getHours()) {
                    calDate.add(Calendar.DAY_OF_MONTH, 1);
                }
                dtStartDate = calDate.getTime();
            }

            if (workOrder.getEnddate().before(dtEndDate)) {
                Calendar calDate = Calendar.getInstance();
                calDate.setTime(workOrder.getEnddate());
                dtEndDate = calDate.getTime();
            }

            //// iterate through start end dates //////////////////////////////////////////
            Calendar clStartDate = Calendar.getInstance();
            clStartDate.setTime(dtStartDate);
            Calendar clEndDate = Calendar.getInstance();
            clEndDate.setTime(dtEndDate);

            HashSet<Integer> hsDays = new HashSet<>(schedulPattern.getLstDays());
            HashMap<Integer, ChannelDetails> mapChannels = new HashMap<>();
            HashMap<Integer, Integer> mapAvailableSpotCount = new HashMap<>();
            Advertisement advert = advertisementDao.getSelectedAdvertisement(schedulPattern.getAdvertId());

            for (int id : schedulPattern.getLstChannelIds()) {
                ChannelDetails channel = channelDetailDAO.getChannel(id);
                mapChannels.put(id, channel);
                //// get available spots for particular advert duration ///////////////////////
                int iAvailableSpotCount = this.getAvailableSpotCount(schedulPattern.getWorkOrderId(), id, schedulPattern.getAdvertId());
                mapAvailableSpotCount.put(id, iAvailableSpotCount);
            }
            Date defultDate = dateFormat.parse("1970-01-01 00:00:00.000");


            for (Calendar clDate = clStartDate; !clStartDate.after(clEndDate); clStartDate.add(Calendar.DATE, 1)) {
                int dayOfWeek = clDate.get(Calendar.DAY_OF_WEEK);
                if (hsDays.contains(dayOfWeek)) {
                    for (int channelId : schedulPattern.getLstChannelIds()) {
                        for (int i = 0; i < schedulPattern.getSlotCount(); i++) {

                            ScheduleDef scheduleEntity = new ScheduleDef();
                            scheduleEntity.setChannelid(mapChannels.get(channelId));
                            scheduleEntity.setWorkorderid(workOrder);
                            scheduleEntity.setAdvertid(advert);
                            scheduleEntity.setDate(clDate.getTime());
                            scheduleEntity.setSchedulestarttime(dtStartTime);
                            scheduleEntity.setScheduleendtime(dtEndTime);
                            scheduleEntity.setActualstarttime(defultDate);
                            scheduleEntity.setActualendtime(defultDate);
                            scheduleEntity.setSequencenum(0);
                            scheduleEntity.setStatus("0");
                            scheduleEntity.setComment("pattern insert");
                            scheduleEntity.setProgram("");
                            scheduleEntity.setRevenueLine(schedulPattern.getRevenueLine());
                            scheduleEntity.setMonth(schedulPattern.getRevenueMonth());

                            int iAvailableSpotCount = mapAvailableSpotCount.get(channelId);
                            if (iAvailableSpotCount > 0 && schedulerDao.saveSchedule(scheduleEntity)) {
                                mapAvailableSpotCount.put(channelId, iAvailableSpotCount - 1);
                                iInsertedCount++;
                            } else if (iAvailableSpotCount <= 0) {
                                LOGGER.debug("[insert] : AvailableSpotCount = 0 channel : {}", channelId);
                            }
                        }
                    }
                }
            }
            retObject = this.getWOScheduleViewData(schedulPattern.getWorkOrderId(), -999, true);
            retObject.setMessage(iInsertedCount + " Schedules Inserted.");
            retObject.setLstChannelIds(schedulPattern.getLstChannelIds());
            retObject.setDtStartDate(dtStartDate);
            retObject.setDtEndDate(dtEndDate);
            retObject.setDtStartTime(dtStartTime);
            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in saveSchedulePattern: ", e.getMessage());
            throw e;
        }
    }

    public List<PreScheduleReplaceView> preAdvertisementsReplace(String replaceData) throws Exception {
        List<PreScheduleReplaceView> retList = new ArrayList<PreScheduleReplaceView>();

        try {
            ObjectMapper mapper = new ObjectMapper();
            ReplaceAdvertData replacePattern = mapper.readValue(replaceData, ReplaceAdvertData.class);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            ///// validate start end dates ///////////////////////////////////////////
            Date dtStartDate = dateFormat.parse(replacePattern.getScheduleStartDate());
            Date dtEndDate = dateFormat.parse(replacePattern.getScheduleEndDate());
            Date dtStartTime = timeFormat.parse("00:00:00");
            Date dtCurrentDateTime = new Date();

            WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(replacePattern.getWorkOrderID());
            if (dtStartDate.before(dtCurrentDateTime)) {
                Calendar calDate = Calendar.getInstance();
                calDate.setTime(dateFormat.parse(dateFormat.format(dtCurrentDateTime)));
                dtStartDate = calDate.getTime();

                Calendar calTime = Calendar.getInstance();
                calTime.setTime(dtStartTime);
                calTime.set(Calendar.HOUR_OF_DAY, dtCurrentDateTime.getHours() + 1);
                dtStartTime = calTime.getTime();
            }

            ///replce schedules ////////////////////////
            Advertisement advertOriginal = advertisementDao.getSelectedAdvertisement(replacePattern.getOriginalAdvertID());
            List<Advertisement> advertReplace = new ArrayList<>();
            HashSet<Integer> setSelectedSchedules = new HashSet<>();

            for (int iAdvertID : replacePattern.getReplaceAdvertID()) {
                Advertisement advert = advertisementDao.getSelectedAdvertisement(iAdvertID);
                if (advert.getAdverttype().equals(advertOriginal.getAdverttype())) {
                    advertReplace.add(advert);
                }
            }
            if (replacePattern.isIsSelected()) {
                setSelectedSchedules = new HashSet<>(replacePattern.getSelectedSchedules());
            }

            ////////////////////////////////////////////////////////////////////requaredDuration
            if (advertOriginal.getAdverttype().equals("ADVERT")) {
                for (int iChannelID : replacePattern.getReplaceChannelID()) {
                    int iAdvertIndex = 0;
                    long requaredDuration = 0;
                    long replaceAdverDuration = 0;
                    long availableDuration = 0;
                    long durationChangeforReplace = 0;
                    boolean isReplaceRuninig = true;
                    int replacesTotal = 0;
                    int replacesValid = 0;

                    List<List<ScheduleDef>> lstDailySchedules = schedulerDao.getAllSchedulesBetweenDates(iChannelID, dtStartDate, dtEndDate, dtStartTime, replacePattern.getOriginalAdvertID());
                    WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(replacePattern.getWorkOrderID(), iChannelID);
                    availableDuration = woChannelSpotsCount.getAvailableTVCDuration();

                    /// replacing //////////////////////////////////////////
                    if (replacePattern.getReplaceMode() == 2 && !advertReplace.isEmpty()) { ///horizontal
                        isReplaceRuninig = true;

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                if ((replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid()))) {
                                    continue; /// not a selected schedule
                                }

                                requaredDuration += (advertReplace.get(iAdvertIndex).getDuration() - advertOriginal.getDuration());
                                replaceAdverDuration += advertReplace.get(iAdvertIndex).getDuration();
                                replacesTotal++;

                                if (!isReplaceRuninig) {
                                    break;
                                }
                                if ((availableDuration < requaredDuration)) {
                                    isReplaceRuninig = false;
                                    break;
                                }
                                replacesValid++;
                                iAdvertIndex = (++iAdvertIndex) % advertReplace.size();
                            }
                        }

                    } else if (replacePattern.getReplaceMode() == 1 && !advertReplace.isEmpty()) { // vertical
                        durationChangeforReplace -= advertOriginal.getDuration();
                        for (Advertisement advert : advertReplace) {
                            durationChangeforReplace += advert.getDuration();
                        }

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                if (replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid())) {
                                    continue; /// not a selected schedule
                                }

                                requaredDuration += durationChangeforReplace;
                                replaceAdverDuration += durationChangeforReplace;
                                replacesTotal++;

                                if (!isReplaceRuninig) {
                                    break;
                                }
                                if (availableDuration < requaredDuration) {
                                    isReplaceRuninig = false;
                                    break;
                                }
                                replacesValid++;
                            }
                        }
                    }

                    ChannelDetails channelD = channelDetailDAO.getChannel(iChannelID);
                    PreScheduleReplaceView retObj = new PreScheduleReplaceView();
                    retObj.setChannelId(iChannelID);
                    retObj.setChannelName(channelD.getChannelname());
                    retObj.setRequaredDuration(replaceAdverDuration);//requaredDuration
                    retObj.setAvailableDuration(availableDuration);
                    retObj.setTotalReplaceCount(replacesTotal);
                    retObj.setValidReplaceCount(replacesValid);

                    retList.add(retObj);
                }
            } else {
                for (int iChannelID : replacePattern.getReplaceChannelID()) {
                    int iAdvertIndex = 0;
                    int availableSpotCount = 0;
                    boolean isReplaceRuninig = true;
                    int replacesTotal = 0;
                    int replacesValid = 0;

                    List<List<ScheduleDef>> lstDailySchedules = schedulerDao.getAllSchedulesBetweenDates(iChannelID, dtStartDate, dtEndDate, dtStartTime, replacePattern.getOriginalAdvertID());
                    WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(replacePattern.getWorkOrderID(), iChannelID);
                    availableSpotCount = this.getAvailableSpotCount(replacePattern.getWorkOrderID(), iChannelID, replacePattern.getOriginalAdvertID());

                    /// replacing //////////////////////////////////////////
                    if (replacePattern.getReplaceMode() == 2 && !advertReplace.isEmpty()) { ///horizontal
                        isReplaceRuninig = true;

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                if ((replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid()))) {
                                    continue; /// not a selected schedule
                                }
                                replacesTotal++;
                                // Comment these lines for Jira issue NRM-808
//                                if (!isReplaceRuninig) {
//                                    break;
//                                }
                                if ((availableSpotCount <= 0)) {
                                    isReplaceRuninig = false;
                                    // Comment this line for Jira issue NRM-808
//                                    break;
                                }
                                replacesValid++;
                                iAdvertIndex = (++iAdvertIndex) % advertReplace.size();
                            }
                        }

                    } else if (replacePattern.getReplaceMode() == 1 && !advertReplace.isEmpty()) { // vertical

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                if (replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid())) {
                                    continue; /// not a selected schedule
                                }
                                replacesTotal++;
                                // Comment these lines for Jira issue NRM-808
//                                if (!isReplaceRuninig) {
//                                    break;
//                                }
                                if (availableSpotCount <= 0) {
                                    isReplaceRuninig = false;
                                    // Comment this line for Jira issue NRM-808
//                                    break;
                                }
                                replacesValid++;
                                availableSpotCount++;
                                availableSpotCount -= advertReplace.size();
                            }
                        }
                    }

                    ChannelDetails channelD = channelDetailDAO.getChannel(iChannelID);
                    PreScheduleReplaceView retObj = new PreScheduleReplaceView();
                    retObj.setChannelId(iChannelID);
                    retObj.setChannelName(channelD.getChannelname());
                    retObj.setRequaredDuration(0);
                    retObj.setAvailableDuration(0);
                    retObj.setTotalReplaceCount(replacesTotal);
                    retObj.setValidReplaceCount(replacesValid);

                    retList.add(retObj);
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in preAdvertisementsReplace: ", e.getMessage());
            throw e;
        }
        return retList;
    }

    /// advert replace refactored
    public WorkOrderScheduleView advertisementsReplace(String replaceData) throws Exception {
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();

        try {
            ObjectMapper mapper = new ObjectMapper();
            ReplaceAdvertData replacePattern = mapper.readValue(replaceData, ReplaceAdvertData.class);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            ///// validate start end dates ///////////////////////////////////////////
            Date dtStartDate = dateFormat.parse(replacePattern.getScheduleStartDate());
            Date dtEndDate = dateFormat.parse(replacePattern.getScheduleEndDate());
            Date dtStartTime = timeFormat.parse("00:00:00");
            Date dtCurrentDateTime = new Date();

            WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(replacePattern.getWorkOrderID());
            if (dtStartDate.before(dtCurrentDateTime)) {
                Calendar calDate = Calendar.getInstance();
                calDate.setTime(dateFormat.parse(dateFormat.format(dtCurrentDateTime)));
                dtStartDate = calDate.getTime();

                Calendar calTime = Calendar.getInstance();
                calTime.setTime(dtStartTime);
                calTime.set(Calendar.HOUR_OF_DAY, dtCurrentDateTime.getHours() + 1);
                dtStartTime = calTime.getTime();
            }
            ///replce schedules ////////////////////////
            int iUpdatedScheduleCount = 0;
            int iInsertedScheduleCount = 0;
            int iRemovedScheduleCount = 0;

            Advertisement advertOriginal = advertisementDao.getSelectedAdvertisement(replacePattern.getOriginalAdvertID());
            List<Advertisement> advertReplace = new ArrayList<>();
            HashSet<Integer> setSelectedSchedules = new HashSet<>();

            String advertName = "";
            int advertisementId = 0;
            for (int iAdvertID : replacePattern.getReplaceAdvertID()) {
                Advertisement advert = advertisementDao.getSelectedAdvertisement(iAdvertID);
                advertName = advert.getAdvertname();
                advertisementId = iAdvertID;
                if (advert.getAdverttype().equals(advertOriginal.getAdverttype())) {
                    advertReplace.add(advert);
                }
            }
            if (replacePattern.isIsSelected()) {
                setSelectedSchedules = new HashSet<>(replacePattern.getSelectedSchedules());
            }

            ////////////////////////////////////////////////////////////////////
            if (advertOriginal.getAdverttype().equals("ADVERT")) {
                for (int iChannelID : replacePattern.getReplaceChannelID()) {
                    int iAdvertIndex = 0;
                    long availableDuration = 0;
                    long durationChangeforReplace = 0;
                    boolean isReplaceRuninig = true;

                    List<List<ScheduleDef>> lstDailySchedules = schedulerDao.getAllSchedulesBetweenDates(iChannelID, dtStartDate, dtEndDate, dtStartTime, replacePattern.getOriginalAdvertID());
                    WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(replacePattern.getWorkOrderID(), iChannelID);
                    availableDuration = woChannelSpotsCount.getAvailableTVCDuration();

                    /// replacing //////////////////////////////////////////
                    if (replacePattern.getReplaceMode() == 2 && !advertReplace.isEmpty()) { ///horizontal
                        String comment = "repalced for " + advertOriginal.getAdvertname();
                        isReplaceRuninig = true;

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                //Status of scheduled or aired adverts should not be changed
                                if(schedule.getStatus().equalsIgnoreCase("1") && schedule.getStatus().equalsIgnoreCase("2") ) {
                                    continue;
                                }
                                if (replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid())) {
                                    continue; /// not a selected schedule
                                }
                                if (!isReplaceRuninig) {
                                    break;
                                }

                                schedule.setAdvertid(advertReplace.get(iAdvertIndex));
                                schedule.setComment(comment);
                                if (advertReplace.get(iAdvertIndex).getStatus() == 1) {
                                    schedule.setStatus("0");
                                }
                                durationChangeforReplace = advertReplace.get(iAdvertIndex).getDuration() - advertOriginal.getDuration();
                                if (availableDuration < durationChangeforReplace) {
                                    isReplaceRuninig = false;
                                    break;
                                }
                                if (schedulerDao.updateSchedule(schedule)) {
                                    iUpdatedScheduleCount++;
                                    availableDuration -= durationChangeforReplace;
                                    iAdvertIndex = (++iAdvertIndex) % advertReplace.size();
                                }

                            }
                        }

                    } else if (replacePattern.getReplaceMode() == 1 && !advertReplace.isEmpty()) { // vertical
                        String commentOld = "[v] repalced by ";
                        String commentNew = "repalced for " + advertOriginal.getAdvertname();
                        durationChangeforReplace -= advertOriginal.getDuration();
                        for (Advertisement advert : advertReplace) {
                            commentOld += (advert.getAdvertname() + " - ");
                            durationChangeforReplace += advert.getDuration();
                        }

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                if (replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid())) {
                                    continue; /// not a selected schedule
                                }

                                if (!isReplaceRuninig) {
                                    break;
                                }
                                if (availableDuration < durationChangeforReplace) {
                                    break;
                                }
                                /// remove curent schedule //////////////////////
                                schedule.setStatus("6");
                                schedule.setComment(commentOld);

                                if (schedulerDao.updateSchedule(schedule)) {
                                    iRemovedScheduleCount++;
                                    availableDuration += schedule.getAdvertid().getDuration();

                                    /// insert new adverts for freed slot
                                    for (Advertisement advert : advertReplace) {
                                        ScheduleDef scheduleEntity = new ScheduleDef();
                                        scheduleEntity.setChannelid(schedule.getChannelid());
                                        scheduleEntity.setWorkorderid(schedule.getWorkorderid());
                                        scheduleEntity.setAdvertid(advert);
                                        scheduleEntity.setDate(schedule.getDate());
                                        scheduleEntity.setSchedulestarttime(schedule.getSchedulestarttime());
                                        scheduleEntity.setScheduleendtime(schedule.getScheduleendtime());
                                        scheduleEntity.setActualstarttime(schedule.getActualstarttime());
                                        scheduleEntity.setActualendtime(schedule.getActualendtime());
                                        scheduleEntity.setSequencenum(schedule.getSequencenum());
                                        scheduleEntity.setStatus("0");
                                        scheduleEntity.setComment(commentNew);
                                        scheduleEntity.setProgram(schedule.getProgram());
                                        scheduleEntity.setRevenueLine(schedule.getRevenueLine());
                                        scheduleEntity.setMonth(schedule.getMonth());

                                        if (schedulerDao.saveSchedule(scheduleEntity)) {
                                            iInsertedScheduleCount++;
                                            availableDuration -= advert.getDuration();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                for (int iChannelID : replacePattern.getReplaceChannelID()) {
                    int iAdvertIndex = 0;
                    int availableSpotCount = 0;
                    boolean isReplaceRuninig = true;

                    List<List<ScheduleDef>> lstDailySchedules = schedulerDao.getAllSchedulesBetweenDates(iChannelID, dtStartDate, dtEndDate, dtStartTime, replacePattern.getOriginalAdvertID());
                    WorkOrderChannelScheduleView woChannelSpotsCount = this.getScheduleAvailableSpotCount(replacePattern.getWorkOrderID(), iChannelID);
                    availableSpotCount = this.getAvailableSpotCount(replacePattern.getWorkOrderID(), iChannelID, replacePattern.getOriginalAdvertID());

                    /// replacing //////////////////////////////////////////
                    if (replacePattern.getReplaceMode() == 2 && !advertReplace.isEmpty()) { ///horizontal
                        String comment = "repalced for " + advertOriginal.getAdvertname();
                        isReplaceRuninig = true;

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                if (replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid())) {
                                    continue; /// not a selected schedule
                                }
//                                if (!isReplaceRuninig) { // For Jira issue NRM-808
//                                    break;
//                                }

                                schedule.setAdvertid(advertReplace.get(iAdvertIndex));
                                schedule.setComment(comment);
                                if (availableSpotCount <= 0) {
                                    isReplaceRuninig = false;
//                                    break;                // For Jira issue NRM-808
                                }
                                if (schedulerDao.updateSchedule(schedule)) {
                                    iUpdatedScheduleCount++;
                                    iAdvertIndex = (++iAdvertIndex) % advertReplace.size();
                                }
                            }
                        }

                    } else if (replacePattern.getReplaceMode() == 1 && !advertReplace.isEmpty()) { // vertical
                        String commentOld = "[v] repalced by ";
                        String commentNew = "repalced for " + advertOriginal.getAdvertname();

                        for (List<ScheduleDef> dailySchedules : lstDailySchedules) {
                            for (ScheduleDef schedule : dailySchedules) {
                                if (replacePattern.isIsSelected() && !setSelectedSchedules.contains(schedule.getScheduleid())) {
                                    continue; /// not a selected schedule
                                }

                                if (!isReplaceRuninig) {
                                    break;
                                }
                                if (availableSpotCount < advertReplace.size()) {
                                    break;
                                }
                                /// remove curent schedule //////////////////////
                                schedule.setStatus("6");
                                schedule.setComment(commentOld);

                                if (schedulerDao.updateSchedule(schedule)) {
                                    iRemovedScheduleCount++;
                                    availableSpotCount++;

                                    /// insert new adverts for freed slot
                                    for (Advertisement advert : advertReplace) {
                                        ScheduleDef scheduleEntity = new ScheduleDef();
                                        scheduleEntity.setChannelid(schedule.getChannelid());
                                        scheduleEntity.setWorkorderid(schedule.getWorkorderid());
                                        scheduleEntity.setAdvertid(advert);
                                        scheduleEntity.setDate(schedule.getDate());
                                        scheduleEntity.setSchedulestarttime(schedule.getSchedulestarttime());
                                        scheduleEntity.setScheduleendtime(schedule.getScheduleendtime());
                                        scheduleEntity.setActualstarttime(schedule.getActualstarttime());
                                        scheduleEntity.setActualendtime(schedule.getActualendtime());
                                        scheduleEntity.setSequencenum(schedule.getSequencenum());
                                        scheduleEntity.setStatus("0");
                                        scheduleEntity.setComment(commentNew);
                                        scheduleEntity.setProgram(schedule.getProgram());
                                        scheduleEntity.setRevenueLine(schedule.getRevenueLine());
                                        scheduleEntity.setMonth(schedule.getMonth());

                                        if (schedulerDao.saveSchedule(scheduleEntity)) {
                                            iInsertedScheduleCount++;
                                            availableSpotCount--;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            retObject = this.getWOScheduleViewData(replacePattern.getWorkOrderID(), -999, true);
            retObject.setMessage(iRemovedScheduleCount + " Schedules Removed - " + iUpdatedScheduleCount + " Schedules Updated - " + iInsertedScheduleCount + " Schedules Inserted.");
            retObject.setLstChannelIds(replacePattern.getReplaceChannelID());
            retObject.setDtStartDate(dtStartDate);
            retObject.setDtEndDate(dtEndDate);
            retObject.setDtStartTime(dtStartTime);

            for(int i=0; i<replacePattern.getSelectedSchedules().size(); i++) {
                for(int j=0; j<retObject.getTimebeltSchedules().size(); j++){
//                    if(retObject.getTimebeltSchedules().get(j).getAdvertid() == replacePattern.getOriginalAdvertID()){
//                        retObject.getTimebeltSchedules().get(j).setAdvertid(advertisementId);
//                        retObject.getTimebeltSchedules().get(j).setAdvertName(advertName);
//                    }

                    // For Jira issue NRM-820
                    ScheduleUpdateView timeBeltSchedule = retObject.getTimebeltSchedules().get(j);
                    for(int k=0; k < timeBeltSchedule.getScheduleItem().size(); k++) {
                        ScheduleItemModel scheduleItem = timeBeltSchedule.getScheduleItem().get(k);
                        for(int z=0; z<scheduleItem.getScheduleid().size(); z++) {
                            if (scheduleItem.getScheduleid().get(z).intValue() == replacePattern.getSelectedSchedules().get(i).intValue()) {
                                retObject.getTimebeltSchedules().get(j).setAdvertid(advertisementId);
                                retObject.getTimebeltSchedules().get(j).setAdvertName(advertName);
                            }
                        }
                    }
                    // End
                }
            }

            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in advertisementsReplace: ", e.getMessage());
            throw e;
        }
    }

    public void recreateDailyScheduleForPatternFillAndReplaceAdvert(WorkOrderScheduleView retObject) {
        spotSpreadService.recreateDailyScheduleForPatternFillAndReplaceAdvert(retObject.getLstChannelIds(),
                retObject.getDtStartDate(), retObject.getDtEndDate(), retObject.getDtStartTime(),retObject.getRescheduleStatus());

    }

    // pre copy schedule rows data and update frontend 
    public List<PreScheduleInsertView> preCopyScheduleRows(String schedulData) throws Exception {
        List<PreScheduleInsertView> retObject = new ArrayList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            CopySchedulesInfo scheduleData = mapper.readValue(schedulData, CopySchedulesInfo.class);
            WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(scheduleData.getWorkOrderID());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            Date dtCurrentDateTime = new Date();
            Date dtStartDate = dateFormat.parse(dateFormat.format(dtCurrentDateTime));
            Date dtEndDate = dateFormat.parse(dateFormat.format(workOrder.getEnddate()));
            Date dtStartTime = timeFormat.parse("00:00:00");
            System.out.println("dtStartTime"+dtStartTime);

//            Calendar calTime = Calendar.getInstance();
//            calTime.setTime(dtStartTime);
//            calTime.set(Calendar.HOUR_OF_DAY, dtCurrentDateTime.getHours() + 1);
//            Date startTime = calTime.getTime();
//            System.out.println("dtStartTime"+dtStartTime);


//                calTime.set(Calendar.HOUR_OF_DAY, today.getHours() + 1);
//                today = calTime.getTime();
//                Date toDay = dateFormat.parse(dateFormat.format(new Date()));
//                if (toDay.after(schedule.getDate()) && (scheduleData.getStartTime().equals("")) && (today.after(schedule.getSchedulestarttime()))){
//                    continue;
//                }


            ///copy schedules ////////////////////////
            HashMap<Integer, HashMap<Integer, Integer>> mapChannels = new HashMap<>();
            HashMap<Integer, ScheduleDef> mapAllSchedules = new HashMap<>();
            ChannelDetails channel = null;
            Advertisement advert = null;
            HashMap<Integer, Integer> insertedCount = new HashMap<>();
            HashMap<Integer, Integer> totalCount = new HashMap<>();

            if (scheduleData.getChannelID() != -1) {
                channel = channelDetailDAO.getChannel(scheduleData.getChannelID());
            }
            if (scheduleData.getAdvertID() != -1) {
                advert = advertisementDao.getSelectedAdvertisement(scheduleData.getAdvertID());
            }
            for (ScheduleDef schedule : schedulerDao.getAllSchedulesBetweenDates(scheduleData.getWorkOrderID(), dtStartDate, dtEndDate, dtStartTime)) {
                mapAllSchedules.put(schedule.getScheduleid(), schedule);
            }
            ////////////////////////////////////////////////////////////////////
            for (int iSheduleId : scheduleData.getSelectedSchedules()) {
                if (!mapAllSchedules.containsKey(iSheduleId)) {
                    continue;
                }
                ScheduleDef schedule = mapAllSchedules.get(iSheduleId);

//                Date today = new Date();
//                Calendar calTime = Calendar.getInstance();
//                calTime.set(Calendar.HOUR_OF_DAY, today.getHours() + 1);
//                today = calTime.getTime();
//                Date toDay = dateFormat.parse(dateFormat.format(new Date()));
//                if (toDay.equals(schedule.getDate()) && (scheduleData.getStartTime().equals("")) && (scheduleData.getEndTime().equals("")) && (today.after(schedule.getSchedulestarttime()))){
//                    continue;
//                }

                /// copping //////////////////////////////////////////
                ScheduleDef scheduleEntity = new ScheduleDef();
                scheduleEntity.setChannelid(schedule.getChannelid());
                scheduleEntity.setWorkorderid(schedule.getWorkorderid());
                scheduleEntity.setAdvertid(schedule.getAdvertid());

                if (channel != null) {
                    scheduleEntity.setChannelid(channel);
                }
                if (advert != null) {
                    scheduleEntity.setAdvertid(advert);
                }

                if (!mapChannels.containsKey(scheduleEntity.getChannelid().getChannelid())) {
                    mapChannels.put(scheduleEntity.getChannelid().getChannelid(), new HashMap());
                    insertedCount.put(scheduleEntity.getChannelid().getChannelid(), 0);
                    totalCount.put(scheduleEntity.getChannelid().getChannelid(), 0);
                }

                HashMap<Integer, Integer> mapAdvert = mapChannels.get(scheduleEntity.getChannelid().getChannelid());
                if (!mapAdvert.containsKey(scheduleEntity.getAdvertid().getAdvertid())) {
                    int avl = this.getAvailableSpotCount(scheduleData.getWorkOrderID(), scheduleEntity.getChannelid().getChannelid(), scheduleEntity.getAdvertid().getAdvertid());
                    mapAdvert.put(scheduleEntity.getAdvertid().getAdvertid(), avl);
                }

                int iAvailableSpotCount = mapAdvert.get(scheduleEntity.getAdvertid().getAdvertid());
                if (iAvailableSpotCount > 0) {
                    mapAdvert.put(scheduleEntity.getAdvertid().getAdvertid(), iAvailableSpotCount - 1);
                    insertedCount.put(scheduleEntity.getChannelid().getChannelid(), insertedCount.get(scheduleEntity.getChannelid().getChannelid()) + 1);
                }
                totalCount.put(scheduleEntity.getChannelid().getChannelid(), totalCount.get(scheduleEntity.getChannelid().getChannelid()) + 1);
            }

            for (int id : mapChannels.keySet()) {
                WorkOrderChannelScheduleView wocView = this.getScheduleAvailableSpotCount(scheduleData.getWorkOrderID(), id);

                PreScheduleInsertView schedInsertView = new PreScheduleInsertView();
                if (channel == null) {
                    schedInsertView.setChannelId(-1);
                    schedInsertView.setChannelName("Default");
                } else {
                    schedInsertView.setChannelId(channel.getChannelid());
                    schedInsertView.setChannelName(channel.getChannelname());
                }
                if (advert == null) {
                    schedInsertView.setAdvertName("Default");
                } else {
                    schedInsertView.setAdvertName(advert.getAdvertname());
                }
                schedInsertView.setInitialScheduleTime(wocView.getTotalTVCDuration());
                schedInsertView.setAvailableScheduleTime(wocView.getAvailableTVCDuration());
                schedInsertView.setValidSlotCount(insertedCount.get(id));
                schedInsertView.setNewSlotCount(totalCount.get(id));

                retObject.add(schedInsertView);
            }

            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in preCopyScheduleRows: ", e.getMessage());
            throw e;
        }
    }

    // copy schedule rows data and update frontend
    @Transactional
    public WorkOrderScheduleView copyScheduleRows(String schedulData) throws Exception {
        WorkOrderScheduleView retObject = new WorkOrderScheduleView();

        try {
            ObjectMapper mapper = new ObjectMapper();
            CopySchedulesInfo scheduleData = mapper.readValue(schedulData, CopySchedulesInfo.class);
            WorkOrder workOrder = workOrderDao.getSelectedWorkOrder(scheduleData.getWorkOrderID());
            LOGGER.debug("Started copy schedule rows for the workorder : {}", workOrder.getWorkorderid());
            retObject.setWorkOrderId(scheduleData.getWorkOrderID());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            Date currentTime = timeFormat.parse(timeFormat.format(new Date()));

            Date dtStartDate;
            Date dtStartTime;
            if (workOrder.getStartdate().getTime() > System.currentTimeMillis()) {
                dtStartDate = dateFormat.parse(dateFormat.format(workOrder.getStartdate()));
                // dtStartTime =  timeFormat.parse(scheduleData.getStartTime() + ":00");
                dtStartTime = timeFormat.parse("00:00:00");
            } else {
                Date dtCurrentDateTime = new Date();
                dtStartDate = dateFormat.parse(dateFormat.format(dtCurrentDateTime));
                dtStartTime = timeFormat.parse("00:00:00");
//                Calendar calTime = Calendar.getInstance();
//                calTime.setTime(dtStartTime);
//                calTime.set(Calendar.HOUR_OF_DAY, dtCurrentDateTime.getHours() + 1);
//                dtStartTime = calTime.getTime();
            }
            Date dtEndDate = dateFormat.parse(dateFormat.format(workOrder.getEnddate()));


            ///copy schedules ////////////////////////
            //int iInsertedScheduleCount = 0;

            //HashMap<Integer, Integer> mapAvailableSpotCount = new HashMap<>();
            HashSet<Integer> setChannel = new HashSet<>();
            HashMap<Integer, ScheduleDef> mapAllSchedules = new HashMap<>();
            ChannelDetails channel = null;
            Advertisement advert = null;
            Date dtSchedStartTime = null;
            Date dtSchedEndTime = null;
            String revMonth = null;
            String revLine = null;

            if (scheduleData.getChannelID() != -1) {
                channel = channelDetailDAO.getChannel(scheduleData.getChannelID());
            }
            if (scheduleData.getAdvertID() != -1) {
                advert = advertisementDao.getSelectedAdvertisement(scheduleData.getAdvertID());
            }
            if (!scheduleData.getRevenueMonth().equals("")) {
                revMonth = scheduleData.getRevenueMonth();
            }
            if (!scheduleData.getRevenueLine().equals("")) {
                revLine = scheduleData.getRevenueLine();
            }
            if (!scheduleData.getStartTime().equals("")) {
                dtSchedStartTime = timeFormat.parse(scheduleData.getStartTime() + ":00");
            }
            if (!scheduleData.getEndTime().equals("")) {
                dtSchedEndTime = timeFormat.parse(scheduleData.getEndTime() + ":00");
            }

            for (ScheduleDef schedule : schedulerDao.getAllSchedulesByIds(scheduleData.getSelectedSchedules())) {
                mapAllSchedules.put(schedule.getScheduleid(), schedule);
            }

            ////////////////////////////////////////////////////////////////////
            List<ScheduleDef> scheduleDefList = new ArrayList<>();
            int scheduleCount = 0;
            for (int iSheduleId : scheduleData.getSelectedSchedules()) {
                scheduleCount++;
                ScheduleDef schedule = mapAllSchedules.get(iSheduleId);
                String timeString = schedule.getSchedulestarttime().toString();
                timeString = timeString.replace("30", "00");

                if (toDay.after(schedule.getDate())) {
                    //remove expire spot
                } //else if ((toDay.equals(schedule.getDate()) && currentTime.after(timeFormat.parse(timeString)))) {
                    //remove expire spot
                else if((toDay.equals(schedule.getDate())) && (scheduleData.getStartTime().equals("")) && (currentTime.after(timeFormat.parse(timeString)))){
                    //remove expire spot
                }
                else {
                    /// copping //////////////////////////////////////////
                    ScheduleDef scheduleEntity = new ScheduleDef();
                    scheduleEntity.setChannelid(schedule.getChannelid());
                    scheduleEntity.setWorkorderid(schedule.getWorkorderid());
                    scheduleEntity.setAdvertid(schedule.getAdvertid());
                    scheduleEntity.setDate(schedule.getDate());
                    scheduleEntity.setSchedulestarttime(schedule.getSchedulestarttime());
                    scheduleEntity.setScheduleendtime(schedule.getScheduleendtime());
                    scheduleEntity.setActualstarttime(schedule.getActualstarttime());
                    scheduleEntity.setActualendtime(schedule.getActualendtime());
                    scheduleEntity.setSequencenum(schedule.getSequencenum());
                    scheduleEntity.setStatus("0");
                    scheduleEntity.setComment("copyed schedule data");
                    scheduleEntity.setProgram(schedule.getProgram());
                    scheduleEntity.setRevenueLine(schedule.getRevenueLine());
                    scheduleEntity.setMonth(schedule.getMonth());

                    if (channel != null) {
                        scheduleEntity.setChannelid(channel);
                    }
                    if (advert != null) {
                        scheduleEntity.setAdvertid(advert);
                    }
                    if (revMonth != null) {
                        scheduleEntity.setMonth(revMonth);
                    }
                    if (revLine != null) {
                        scheduleEntity.setRevenueLine(revLine);
                    }
                    if (dtSchedStartTime != null) {
                        scheduleEntity.setSchedulestarttime(dtSchedStartTime);
                    }
                    if (dtSchedEndTime != null) {
                        scheduleEntity.setScheduleendtime(dtSchedEndTime);
                    }

                    if (!setChannel.contains(scheduleEntity.getChannelid().getChannelid())) {
                        setChannel.add(scheduleEntity.getChannelid().getChannelid());
                    }

                    scheduleDefList.add(scheduleEntity);
                }
            }
            LOGGER.debug("Before inserting schedules using copyScheduleRows for the work order : {} | prepared schedule count : {}", workOrder.getWorkorderid(), scheduleDefList.size());
            int insertedScheduleCount = 0;
            if (scheduleDefList.size() != 0) {
                insertedScheduleCount = scheduleAsyncService.saveScheduleListAsync(scheduleDefList);
            }
            LOGGER.debug("After inserting schedules using copyScheduleRows for the work order : {} | inserted schedule count : {}", workOrder.getWorkorderid(), insertedScheduleCount);
            retObject.setMessage(insertedScheduleCount + " Schedules Inserted.");
            retObject.setLstChannelIds(new ArrayList(setChannel));
            retObject.setDtStartDate(dtStartDate);
            retObject.setDtEndDate(dtEndDate);
            retObject.setDtStartTime(dtStartTime);
            LOGGER.debug("Completed copy schedule rows for the workorder : {}", workOrder.getWorkorderid());
            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in copyScheduleRows {}: ", e.getMessage());
            throw e;
        }
    }

    @Async("specificTaskExecutor")
    public CompletableFuture<Integer> saveScheduleGroupAsync(List<ScheduleDef> scheduleList){
        CompletableFuture<Integer> completableFuture = new CompletableFuture<>();
        try {
            completableFuture.complete(saveScheduleGroup(scheduleList));
        } catch (Exception e) {
            completableFuture.completeExceptionally(e);
        }
        return completableFuture;
    }

    public Integer saveScheduleGroup(List<ScheduleDef> scheduleList){
        int insertedScheduleCount = 0;
        for(ScheduleDef sd : scheduleList){
            int iAvailableSpotCount = this.getAvailableSpotCount(sd.getWorkorderid().getWorkorderid(), sd.getChannelid().getChannelid(), sd.getAdvertid().getAdvertid());
            if (iAvailableSpotCount > 0 && schedulerDao.saveSchedule(sd)) {
                insertedScheduleCount++;
            }
        }
        return insertedScheduleCount;
    }

    public Boolean testFunction() {
        try {
            //checkAutoStatus(new WorkOrder());
//            CheckWorkOrder();
            return true;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return false;
        }
    }

    public Boolean generateOneDayScheduleExcel(OneDayScheduleInfo infoModel, HttpServletRequest request) {
        if (infoModel.getStartHour() < infoModel.getEndHour()) {
            infoModel.setEndHour(infoModel.getEndHour() - 1);
        }
        return new ExcelGenerater().oneDayScheduleExcel(getOneDaySchedules(infoModel), request);
    }

    public void downloadOneDayScheduleExcel(HttpServletRequest request, HttpServletResponse response) {
        try {

            String webServerPath = new File(request.getServletContext().getRealPath("/")).getParent() + "/ROOT";
            String fullPath = webServerPath + "/OneDay/OneDayScheduleExcell.xls";

            File downloadFile = new File(fullPath);
            FileInputStream inputStream = new FileInputStream(downloadFile);

            String mimeType = "application/octet-stream";

            response.setContentType(mimeType);
            response.setContentLength((int) downloadFile.length());

            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            OutputStream outStream = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outStream.close();

        } catch (IOException e) {
            LOGGER.debug("Exception in SchedulerService in downloadOneDayScheduleExcel: ", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerService in downloadOneDayScheduleExcel: ", e.getMessage());
            throw e;
        }
    }

    public WorkOrderScheduleView getByScheduleDataRow(String scheduleData) {
        WorkOrderScheduleView result = null;
        ObjectMapper m = new ObjectMapper();
        try {
            List<ScheduleInfo> scheduleList = m.readValue(scheduleData, new TypeReference<List<ScheduleInfo>>() {
            });
            ScheduleInfo scheduleInfo = scheduleList.get(0);
            result = getWOScheduleViewData(scheduleInfo.getWorkOrderId(), scheduleInfo.getChannelId(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public WorkOrderScheduleView getByScheduleDataRowForPatternFill(String scheduleData) {
        WorkOrderScheduleView result = null;
        ObjectMapper m = new ObjectMapper();
        try {
            SchedulePatternInfo patternInfo = m.readValue(scheduleData, SchedulePatternInfo.class);
            if (!patternInfo.getLstChannelIds().isEmpty()) {
                result = getWOScheduleViewData(patternInfo.getWorkOrderId(), patternInfo.getLstChannelIds().get(0), true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Transactional
    public List<ScheduleDef> findCurrentDateAllValidSpot() {
        return schedulerDao.findCurrentDateAllValidSpot();
    }

    @Transactional
    public List<ScheduleDef> findCurrentDateAllMissedSpot() {
        int status = 5;
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, -100);
        return schedulerDao.findCurrentDateSpotByStatus(c.getTime(), status);
    }

    @Transactional
    public List<ScheduleDef> findCurrentDateAllAiredSpot(int channelId) {
        int status = 2;
        //  Date today=new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, -100);
//        test
        return schedulerDao.findCurrentDateSpotByStatusAndChannel(c.getTime(), status, channelId);
    }

    public List<ScheduleDef> findAllMissedAndAiredSpots(Date today, int channelId) {
        return schedulerDao.findAllMissedOrAiredSpots(today, channelId);
    }


    public List<ScheduleDef> findAllScheduleSpots() {
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        //c.add(Calendar.DATE, -100);
        return schedulerDao.findAllScheduleSpots(c.getTime());
    }

    public List<DailyMissedSpotsDto> findDailyMissedSpotsPercentages() {
        List<DailyMissedSpotsDto> spots = schedulerDao.findDailyMissedSpotsPercentages();
        int totalMissedSpots = findCurrentDateAllMissedSpot().size();

        List<DailyMissedSpotsDto> newSpotValues = new ArrayList<>();

        for (DailyMissedSpotsDto spotData : spots) {
            BigInteger percentage = spotData.getPercentage().multiply(BigInteger.valueOf(100)).divide(BigInteger.valueOf(totalMissedSpots));
            spotData.setPercentage(percentage);
            newSpotValues.add(spotData);
        }
        return newSpotValues;
    }

    public List<ScheduleDef> findAllScheduleSpots(Date today) {
        return schedulerDao.findAllScheduleSpots(today);
    }

    public List<HightTrafficChannelsWidgetDto> findHighTrafficChannelsOfTheHour() {
        return schedulerDao.findHighTrafficChannelsOfCurrentHour();
    }

    public void resumeSuspendWorkOrderSpot(WorkOrder newWorkOrder, int suspendWorkOrderId) {
        List<ScheduleDef> suspendScheduleList = schedulerDao.getSuspendScheduleByWorkOrder(suspendWorkOrderId);
        for (ScheduleDef scheduleDef : suspendScheduleList) {
            scheduleDef.setWorkorderid(newWorkOrder);
            scheduleDef.setStatus("0");
            scheduleDef.setComment("Resume workOrder");
            schedulerDao.saveSchedule(scheduleDef);
        }
    }

    @Transactional
    public List<ScheduleDef> allChannelsAllWorkOrders_v2(Date from, Date to) {
        return schedulerDao.allChannelsAllWorkOrders_v2(from, to);
    }

    @Transactional
    public List<ScheduleDef> allChannelsAllWorkOrders(Date from, Date to) {
        return schedulerDao.allChannelsAllWorkOrders(from, to);
    }

    @Transactional
    public List<ScheduleDef> allChannelsNotAllWorkOrders_v2(int workOrderId, Date from, Date to) {
        return schedulerDao.allChannelsNotAllWorkOrders_v2(workOrderId, from, to);
    }

    @Transactional
    public List<ScheduleDef> allChannelsNotAllWorkOrders(int workOrderId, Date from, Date to) {
        return schedulerDao.allChannelsNotAllWorkOrders(workOrderId, from, to);
    }

    @Transactional
    public List<ScheduleDef> notAllChannelsAllWorkOrders_v2(int channelId, Date from, Date to) {
        return schedulerDao.notAllChannelsAllWorkOrders_v2(channelId, from, to);
    }

    @Transactional
    public List<ScheduleDef> notAllChannelsAllWorkOrders(int channelId, Date from, Date to) {
        return schedulerDao.notAllChannelsAllWorkOrders(channelId, from, to);
    }

    @Transactional
    public List<ScheduleDef> notAllChannelsNotAllWorkOrders_v2(int channelId, int workorderId, Date from, Date to) {
        return schedulerDao.notAllChannelsNotAllWorkOrders_v2(channelId, workorderId, from, to);
    }

    @Transactional
    public List<ScheduleDef> notAllChannelsNotAllWorkOrders(int channelId, int workorderId, Date from, Date to) {
        return schedulerDao.notAllChannelsNotAllWorkOrders(channelId, workorderId, from, to);
    }

    @Transactional
    public List<TimeSlotScheduleMap> findByDateAndTimeBelts(Date date, List<TimeBelts> timeBelts){
        return schedulerDao.findByDateAndTimeBelts(date, timeBelts);
    }

    private void processPlayLit(List<PlayList> playList, List<TimeSlotScheduleMap> slectedScheduleList, TimeSlotScheduleMap tsm) {
        for (int j = 0; j < playList.size(); j++) {
            int playListScheduleID = playList.get(j).getSchedule().getScheduleid();
            int scheduleID = tsm.getScheduleid().getScheduleid();

            int scheduleHour = tsm.getTimeBelt().getHour();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            int hours = cal.get(Calendar.HOUR_OF_DAY);

            if (playListScheduleID == scheduleID) {
                if ((playList.get(j).getStatus().equals("0") || playList.get(j).getStatus().equals("9") || playList.get(j).getStatus().equals("7")) && scheduleHour > hours) {

                    playList.get(j).setWorkOrder(tsm.getScheduleid().getWorkorderid());
                    playList.get(j).setAdvert(tsm.getScheduleid().getAdvertid());
                    playList.get(j).setDate(tsm.getDate());
                    playList.get(j).setChannel(tsm.getChannelid());
                    playList.get(j).setScheduleStartTime(tsm.getScheduleid().getSchedulestarttime());
                    playList.get(j).setScheduleEndTime(tsm.getScheduleid().getScheduleendtime());
                    playList.get(j).setSchedule(tsm.getScheduleid());
                    playList.get(j).setScheduleHour(tsm.getTimeBelt().getHour());
                    playList.get(j).setStatus(tsm.getScheduleid().getStatus());

                }

                if (playListScheduleDao.saveAndUpdatePlayList(playList.get(j))) {
                    if (slectedScheduleList.remove(tsm)) {
                        break;
                    }
                }
            }
        }

    }
}