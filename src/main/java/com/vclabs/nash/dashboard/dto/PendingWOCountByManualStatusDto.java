package com.vclabs.nash.dashboard.dto;

import java.math.BigInteger;

/**
 * Created by Sanduni on 29/11/2018
 */
public class PendingWOCountByManualStatusDto {

    private BigInteger woCount;

    private String revenueMonth;

    private int manualStatus;

    private Integer woId;

    private Integer scheduleId;

    private String client;

    public PendingWOCountByManualStatusDto(BigInteger woCount, String revenueMonth, int manualStatus, Integer woId, Integer scheduleId, String client) {
        this.woCount = woCount;
        this.revenueMonth = revenueMonth;
        this.manualStatus = manualStatus;
        this.woId = woId;
        this.scheduleId = scheduleId;
        this.client = client;
    }

    public BigInteger getWoCount() {
        return woCount;
    }

    public void setWoCount(BigInteger woCount) {
        this.woCount = woCount;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }

    public int getManualStatus() {
        return manualStatus;
    }

    public void setManualStatus(int manualStatus) {
        this.manualStatus = manualStatus;
    }

    public Integer getWoId() {
        return woId;
    }

    public void setWoId(Integer woId) {
        this.woId = woId;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
