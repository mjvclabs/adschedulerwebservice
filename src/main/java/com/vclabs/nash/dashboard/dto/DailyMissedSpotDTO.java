package com.vclabs.nash.dashboard.dto;

import com.vclabs.nash.model.entity.ScheduleDef;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2018-12-31.
 */
public class DailyMissedSpotDTO {

    private int channelId;
    private String channelName;
    private String type;
    private List<ScheduleDef> missedSpotList = new ArrayList<>();
    private List<ScheduleDef> scheduleSpotList = new ArrayList<>();
    private int missedSpotPresentage=0;
    private int scheduledSpotPresentage=0;

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ScheduleDef> getScheduleSpotList() {
        return scheduleSpotList;
    }

    public void setScheduleSpotList(List<ScheduleDef> scheduleSpotList) {
        this.scheduleSpotList = scheduleSpotList;
    }

    public List<ScheduleDef> getMissedSpotList() {
        return missedSpotList;
    }

    public void setMissedSpotList(List<ScheduleDef> missedSpotList) {
        this.missedSpotList = missedSpotList;
    }

    public int getMissedSpotPresentage() {
        return missedSpotPresentage;
    }

    public void setMissedSpotPresentage(int missedSpotPresentage) {
        this.missedSpotPresentage = missedSpotPresentage;
    }

    public int getScheduledSpotPresentage() {
        return scheduledSpotPresentage;
    }

    public void setScheduledSpotPresentage(int scheduledSpotPresentage) {
        this.scheduledSpotPresentage = scheduledSpotPresentage;
    }
}
