package com.vclabs.nash.dashboard.dto;

import com.vclabs.nash.model.entity.ChannelDetails;

/**
 * Created by Sanduni on 12/11/2018
 */
public class ChannelUtillizationDto {

    private ChannelDetails channel;

    private int spentTimePercentage;

    private int remainingTimePercentage;

    public ChannelDetails getChannel() {
        return channel;
    }

    public void setChannel(ChannelDetails channel) {
        this.channel = channel;
    }

    public int getSpentTimePercentage() {
        return spentTimePercentage;
    }

    public void setSpentTimePercentage(int spentTimePercentage) {
        this.spentTimePercentage = spentTimePercentage;
    }

    public int getRemainingTimePercentage() {
        return remainingTimePercentage;
    }

    public void setRemainingTimePercentage(int remainingTimePercentage) {
        this.remainingTimePercentage = remainingTimePercentage;
    }
}
