package com.vclabs.nash.dashboard.dto;

/**
 * Created by Nalaka on 2019-01-17.
 */
public class RevenueLineUserDTO {

    private String name;

    private double budget;

    private long percentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public long getPercentage() {
        return percentage;
    }

    public void setPercentage(long percentage) {
        this.percentage = percentage;
    }
}
