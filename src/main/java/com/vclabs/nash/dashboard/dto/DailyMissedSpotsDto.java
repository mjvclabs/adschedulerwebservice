package com.vclabs.nash.dashboard.dto;

import java.math.BigInteger;

/**
 * Created by Sanduni on 31/12/2018
 */
public class DailyMissedSpotsDto {

    private String channelName;

    private BigInteger percentage;

    public DailyMissedSpotsDto(String channelName, BigInteger percentage) {
        this.channelName = channelName;
        this.percentage = percentage;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public BigInteger getPercentage() {
        return percentage;
    }

    public void setPercentage(BigInteger percentage) {
        this.percentage = percentage;
    }
}
