package com.vclabs.nash.dashboard.dto;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2019-01-17.
 */
public class RevenueLineClientDTO {

    private int id;
    private String name;
    private double percentage;
    private long value;

    private List<RevenueLineUserDTO> userList = new ArrayList();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        DecimalFormat df = new DecimalFormat("#.##");
        this.percentage = Double.valueOf(df.format(percentage));
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public List<RevenueLineUserDTO> getUserList() {
        return userList;
    }

    public void setUserList(List<RevenueLineUserDTO> userList) {
        this.userList = userList;
    }
}
