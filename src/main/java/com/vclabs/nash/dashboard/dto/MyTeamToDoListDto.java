package com.vclabs.nash.dashboard.dto;

/**
 * Created by Sanduni on 10/01/2019
 */
public class MyTeamToDoListDto {

    private Integer teamId;

    private String teamMember;

    private Integer woId;

    private String woName;

    public MyTeamToDoListDto(Integer teamId, String teamMember, Integer woId, String woName) {
        this.teamId = teamId;
        this.teamMember = teamMember;
        this.woId = woId;
        this.woName = woName;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getTeamMember() {
        return teamMember;
    }

    public void setTeamMember(String teamMember) {
        this.teamMember = teamMember;
    }

    public Integer getWoId() {
        return woId;
    }

    public void setWoId(Integer woId) {
        this.woId = woId;
    }

    public String getWoName() {
        return woName;
    }

    public void setWoName(String woName) {
        this.woName = woName;
    }
}
