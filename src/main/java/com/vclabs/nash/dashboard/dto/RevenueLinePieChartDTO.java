package com.vclabs.nash.dashboard.dto;

import java.text.DecimalFormat;

/**
 * Created by Nalaka on 2019-02-18.
 */
public class RevenueLinePieChartDTO {
    private String name;

    private double budget = 0.0;

    private double percentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        DecimalFormat df = new DecimalFormat("#.##");
        this.percentage = Double.valueOf(df.format(percentage));
    }

    public void addBudget(double value) {
        this.budget += value;
    }
}
