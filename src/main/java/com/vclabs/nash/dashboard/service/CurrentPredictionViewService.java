package com.vclabs.nash.dashboard.service;

/**
 * Created by Sanduni on 24/01/2019
 */
public interface CurrentPredictionViewService {

    void setCurrentPredictionViewData();
}
