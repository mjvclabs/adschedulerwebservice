package com.vclabs.nash.dashboard.service;

/**
 * Created by dperera on 20/02/2019.
 */
public interface WidgetDataBootstrapService {

    void feedDataToTopics();

}
