package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.nash.dashboard.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * Created by dperera on 20/02/2019.
 */
@Service
public class WidgetDataBootstrapServiceImpl implements WidgetDataBootstrapService {

    @Lazy
    @Autowired
    private ChannelUtillizationWidgetService channelUtillizationWidgetService;

    @Lazy
    @Autowired
    private CurrentPredictionViewService currentPredictionViewService;

    @Lazy
    @Autowired
    private DailyMissedSpotUpdateWidgetService dailyMissedSpotUpdateWidgetService;

    @Lazy
    @Autowired
    private DummyCutWidgetService dummyCutWidgetService;

    @Lazy
    @Autowired
    private HighTrafficWidgetService highTrafficWidgetService;

    @Lazy
    @Autowired
    private IngestStatusWidgetService ingestStatusWidgetService;

    @Lazy
    @Autowired
    private MyCompleteSchedulesWidgetService myCompleteSchedulesWidgetService;

    @Lazy
    @Autowired
    private MyRecordedRevenueWidgetService myRecordedRevenueWidgetService;

    @Lazy
    @Autowired
    private MyTeamSalesRevenueService myTeamSalesRevenueService;

    @Lazy
    @Autowired
    private MyTeamToDoListWidgetService myTeamToDoListWidgetService;

    @Lazy
    @Autowired
    private MyToDoListWidgetService myToDoListWidgetService;

    @Lazy
    @Autowired
    private NewWorkOrderWidgetService  newWorkOrderWidgetService;

    @Lazy
    @Autowired
    private PartiallyEnteredWorkOrderWidgetService partiallyEnteredWorkOrderWidgetService;

    @Lazy
    @Autowired
    private PendingWorkOrderWidgetService pendingWorkOrderWidgetService;

    @Lazy
    @Autowired
    private PriorityAdvertisementWidgetService priorityAdvertisementWidgetService;

    @Lazy
    @Autowired
    private RevenueLineDashboardWidgetService revenueLineDashboardWidgetService;

    @Lazy
    @Autowired
    private SchedulesReadyForInvoicingWidgetService schedulesReadyForInvoicingWidgetService;

    @Lazy
    @Autowired
    private ServerControlWidgetService serverControlWidgetService;

    @Lazy
    @Autowired
    private TotalDailyMissedSpotUpdateService totalDailyMissedSpotUpdateService;

    @Lazy
    @Autowired
    private RevisedWorkOrderMaximizeWidgetService revisedWorkOrderMaximizeWidgetService;

    @Override
    public void feedDataToTopics() {
        channelUtillizationWidgetService.setChannelUtilizationData();
        currentPredictionViewService.setCurrentPredictionViewData();
        dailyMissedSpotUpdateWidgetService.setDailyMissedSpotData();
        dummyCutWidgetService.setDummyCutWidget();
        highTrafficWidgetService.setHighTrafficData();
        ingestStatusWidgetService.setIngestStatusWidgetData();
        myCompleteSchedulesWidgetService.setMyCompleteSchedulesData();
        myRecordedRevenueWidgetService.setMyRecordedRevenueData();
        myTeamSalesRevenueService.setMyTeamSalesRevenueData();
        myTeamToDoListWidgetService.setMyTeamToDoListData();
        myToDoListWidgetService.setMyToDoListData();
        newWorkOrderWidgetService.setNewWorkOrderData();
        partiallyEnteredWorkOrderWidgetService.setPartiallyEnteredWOData();
        pendingWorkOrderWidgetService.setPendingWorkOrderData();
        priorityAdvertisementWidgetService.setPriorityAdvertisementWidgetData();
        revenueLineDashboardWidgetService.setRevenueLineDashboardData();
        schedulesReadyForInvoicingWidgetService.setSchedulesReadyForInvoicingData();
        revisedWorkOrderMaximizeWidgetService.setRevisedWorkOrderData();
        serverControlWidgetService.setServerControlData();
        totalDailyMissedSpotUpdateService.setDailyMissedSpotUpdateData();
    }
}
