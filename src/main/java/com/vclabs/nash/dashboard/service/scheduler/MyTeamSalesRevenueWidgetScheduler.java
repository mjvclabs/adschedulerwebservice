package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.MyTeamSalesRevenueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 21/01/2019
 */
@Component
public class MyTeamSalesRevenueWidgetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(MyTeamSalesRevenueWidgetScheduler.class);

    @Autowired
    private MyTeamSalesRevenueService myTeamSalesRevenueService;

    //@Scheduled(cron = "${cron.my.team.sales.revenue.widget}")
    public void myTeamSalesRevenueWidgetDataSet(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("myTeamSalesRevenue Update Schedule started");

        myTeamSalesRevenueService.setMyTeamSalesRevenueData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("myTeamSalesRevenue Update Schedule finished");
    }
}
