package com.vclabs.nash.dashboard.service;

/**
 * Created by Sanduni on 10/01/2019
 */
public interface MyTeamToDoListWidgetService {

    void setMyTeamToDoListData();
}
