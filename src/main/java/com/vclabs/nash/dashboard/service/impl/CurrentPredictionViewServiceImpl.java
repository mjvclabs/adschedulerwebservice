package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.ChannelPredictionDto;
import com.vclabs.nash.dashboard.dto.CurrentPredictionViewDto;
import com.vclabs.nash.dashboard.service.CurrentPredictionViewService;
import com.vclabs.nash.service.AdvertChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 24/01/2019
 */
@Service
public class CurrentPredictionViewServiceImpl implements CurrentPredictionViewService{

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private AdvertChannelService advertChannelService;

    @Async
    @Override
    public void setCurrentPredictionViewData() {
        List<ChannelPredictionDto> result = advertChannelService.findChannelPredictionData();
        List<CurrentPredictionViewDto> predictions = new ArrayList<>();

        for (ChannelPredictionDto data : result){
                CurrentPredictionViewDto predictionView = new CurrentPredictionViewDto();
                predictionView.setCount(data.getCount().longValue());
                predictionView.setId(data.getId());
                predictionView.setIcon(data.getName()+".png");
                predictions.add(predictionView);
        }
        dashboardManager.convertAndSend(NashDMConverterName.CURRENT_PREDICTION_VIEW,"dashboard.currentPredictionView.public",predictions);
    }
}
