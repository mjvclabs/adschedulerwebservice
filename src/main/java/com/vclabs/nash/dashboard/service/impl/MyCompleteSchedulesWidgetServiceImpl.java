package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyCompletedSchedulesDto;
import com.vclabs.nash.dashboard.service.MyCompleteSchedulesWidgetService;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.WorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 05/12/2018
 */
@Service
public class MyCompleteSchedulesWidgetServiceImpl implements MyCompleteSchedulesWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setMyCompleteSchedulesData() {
        List<MyCompletedSchedulesDto> workOrders = workOrderService.findMyCompletedSchedules();
        List<MyCompletedSchedulesDto> currentWOs = new ArrayList<>();
        if(workOrders.size()>0){
            String currentSeller = workOrders.get(0).getSeller();
            for (MyCompletedSchedulesDto wo : workOrders){
                if(currentSeller.equals(wo.getSeller())){
                    currentWOs.add(wo);
                }else {
                    dashboardManager.convertAndSend(NashDMConverterName.MY_COMPLETED_SCHEDULES,"dashboard.myCompleteSchedules." + currentSeller, currentWOs);
                    currentSeller = wo.getSeller();
                    currentWOs.clear();
                }
            }
        }
    }
}
