package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.RevenueLineDashboardWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 13/02/2019
 */
@Component
public class RevenueLineDashboardWidgetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(RevenueLineDashboardWidgetScheduler.class);

    @Autowired
    private RevenueLineDashboardWidgetService revenueLineDashboardWidgetService;

    //@Scheduled(cron = "${cron.revenue.line.dashboard.widget}")
    public void setRevenueLineDashboardWidget(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("RevenueLineDashboardWidget Schedule started");

        revenueLineDashboardWidgetService.setRevenueLineDashboardData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("RevenueLineDashboardWidget Schedule finished");
    }
}
