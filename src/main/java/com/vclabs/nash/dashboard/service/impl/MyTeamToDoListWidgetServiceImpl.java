package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyTeamToDoListDto;
import com.vclabs.nash.dashboard.service.MyTeamToDoListWidgetService;
import com.vclabs.nash.service.teams.TeamDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 10/01/2019
 */
@Service
public class MyTeamToDoListWidgetServiceImpl implements MyTeamToDoListWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private TeamDetailService teamDetailService;

    @Async
    @Override
    public void setMyTeamToDoListData() {
        List<MyTeamToDoListDto> teamMembersDetails = teamDetailService.getTeamMembersWODetail();
        if (teamMembersDetails.size() > 0){
            Integer currentTeam = teamMembersDetails.get(0).getTeamId();
            List<MyTeamToDoListDto> currentTeamDetailsList = new ArrayList<>();
            for (MyTeamToDoListDto list : teamMembersDetails){
                if(currentTeam == list.getTeamId()){
                    currentTeamDetailsList.add(list);
                }else {
                    List<String> admins = teamDetailService.findAllTeamAdmins(currentTeam);
                    for (String admin : admins){
                        if(admin != null) {
                            dashboardManager.convertAndSend(NashDMConverterName.MY_TEAM_TO_DO_LIST, "dashboard.myTeamToDoList." + admin, currentTeamDetailsList);
                        }
                    }
                    currentTeam = list.getTeamId();
                    currentTeamDetailsList.clear();
                }
            }
            if(currentTeamDetailsList.size()>0){
                List<String> admins = teamDetailService.findAllTeamAdmins(currentTeam);
                for (String admin : admins){
                    if(admin != null){
                        dashboardManager.convertAndSend(NashDMConverterName.MY_TEAM_TO_DO_LIST,"dashboard.myTeamToDoList."+ admin, currentTeamDetailsList);
                    }
                }
            }
        }


    }
}
