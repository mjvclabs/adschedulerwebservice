package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.PriorityAdvertisementWidgetDto;
import com.vclabs.nash.dashboard.service.PriorityAdvertisementWidgetService;
import com.vclabs.nash.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Sanduni on 07/12/2018
 */
@Service
public class PriorityAdvertisementWidgetServiceImpl implements PriorityAdvertisementWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private AdvertisementService advertisementService;

    @Async
    @Override
    public void setPriorityAdvertisementWidgetData() {
        List<PriorityAdvertisementWidgetDto> adverts = advertisementService.findAdvertsForPriorityAdvertisementWidget();
        dashboardManager.convertAndSend(NashDMConverterName.PRIORITY_ADVERTISEMENT,"dashboard.priorityAdvertisement.public" , adverts);
    }
}
