package com.vclabs.nash.dashboard.service;

/**
 * Created by Sanduni on 31/12/2018
 */
public interface TotalDailyMissedSpotUpdateService {

    void setDailyMissedSpotUpdateData();
}
