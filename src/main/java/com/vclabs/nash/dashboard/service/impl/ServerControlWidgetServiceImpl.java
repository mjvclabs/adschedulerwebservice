package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.service.ServerControlWidgetService;
import com.vclabs.nash.service.ServerDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Created by Nalaka on 2019-01-10.
 */
@Service
public class ServerControlWidgetServiceImpl implements ServerControlWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private ServerDetailsService serverDetailsService;

    @Async
    @Override
    public void setServerControlData() {
        dashboardManager.convertAndSend(NashDMConverterName.SERVER_CONTROL, "dashboard.serverControl.public", serverDetailsService.getServerList());
    }
}
