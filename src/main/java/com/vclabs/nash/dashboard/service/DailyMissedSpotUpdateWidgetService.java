package com.vclabs.nash.dashboard.service;

/**
 * Created by Nalaka on 2018-12-31.
 */
public interface DailyMissedSpotUpdateWidgetService {

    void setDailyMissedSpotData();
}
