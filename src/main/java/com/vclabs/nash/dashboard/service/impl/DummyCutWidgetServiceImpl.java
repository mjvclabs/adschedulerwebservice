package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.service.DummyCutWidgetService;
import com.vclabs.nash.service.ReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Nalaka on 2018-12-21.
 */
@Service
@Transactional("main")
public class DummyCutWidgetServiceImpl implements DummyCutWidgetService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private ReportingService reportingService;

    @Async
    @Override
    public void setDummyCutWidget() {
        dashboardManager.convertAndSend(NashDMConverterName.DUMMY_CUT_LIST, "dashboard.dummyCutList.public", reportingService.getDummyCutReport());
    }
}
