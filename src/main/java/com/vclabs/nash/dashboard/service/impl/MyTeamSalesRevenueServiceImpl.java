package com.vclabs.nash.dashboard.service.impl;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyTeamSalesRevenueDTO;
import com.vclabs.nash.dashboard.dto.MyTeamSalesRevenueMemberDTO;
import com.vclabs.nash.dashboard.dto.SalesRevenueDTO;
import com.vclabs.nash.dashboard.service.MyTeamSalesRevenueService;
import com.vclabs.nash.model.entity.SecurityUsersEntity;
import com.vclabs.nash.model.entity.teams.TeamDetail;
import com.vclabs.nash.service.WorkOrderService;
import com.vclabs.nash.service.teams.TeamDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Nalaka on 2019-01-13.
 */
@Service
public class MyTeamSalesRevenueServiceImpl implements MyTeamSalesRevenueService {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private TeamDetailService teamDetailService;

    @Autowired
    private WorkOrderService workOrderService;

    @Async
    @Override
    public void setMyTeamSalesRevenueData() {
        List<TeamDetail> allTeamDetails = teamDetailService.findAllTeamDetailsForTeamSalesRevenue();
        HashMap<String, List<MyTeamSalesRevenueMemberDTO>> monthMap = new HashMap<>();
        for (TeamDetail teamDetail : allTeamDetails) {
            monthMap.clear();
            if (!teamDetail.getAdmins().isEmpty()) {
                Set<SecurityUsersEntity> adminList = teamDetail.getAdmins();
                for (SecurityUsersEntity admin : adminList) {
                    getTeamMemberRevenueList(monthMap, admin.getUsername());
                }

                Set<SecurityUsersEntity> userList = teamDetail.getMembers();
                if (!userList.isEmpty()) {
                    for (SecurityUsersEntity user : userList) {
                        getTeamMemberRevenueList(monthMap, user.getUsername());
                    }
                }
            }

            Iterator teamSalesRevenueIterator = monthMap.entrySet().iterator();
            List<MyTeamSalesRevenueDTO> teamSalesRevenueDTOList = new ArrayList<>();
            while (teamSalesRevenueIterator.hasNext()) {
                Map.Entry monthDetails = (Map.Entry) teamSalesRevenueIterator.next();
                MyTeamSalesRevenueDTO myTeamSalesRevenueDTO = new MyTeamSalesRevenueDTO();
                myTeamSalesRevenueDTO.setMonth(monthDetails.getKey().toString());
                myTeamSalesRevenueDTO.setMemberLis((List<MyTeamSalesRevenueMemberDTO>) monthDetails.getValue());
                teamSalesRevenueDTOList.add(myTeamSalesRevenueDTO);
            }

            if (!teamDetail.getAdmins().isEmpty()) {
                Set<SecurityUsersEntity> adminList = teamDetail.getAdmins();
                for (SecurityUsersEntity admin : adminList) {
                    String messageBroker = String.format("dashboard.myTeamSalesRevenue.%s", admin.getUsername());
                    dashboardManager.convertAndSend(NashDMConverterName.MY_TEAM_SALES_REVENUE, messageBroker, teamSalesRevenueDTOList);
                }
            }
        }
    }

    private void getTeamMemberRevenueList(HashMap<String, List<MyTeamSalesRevenueMemberDTO>> monthMap, String user) {
        String month = "";
        double monthlyAmount = 0;
        List<SalesRevenueDTO> adminWorkOrderList = workOrderService.findSelecteUserAllWorkOrder(user);
        for (SalesRevenueDTO salesRevenueDTO : adminWorkOrderList) {
            if (month.equals(salesRevenueDTO.getRevenueMonth())) {
                monthlyAmount += salesRevenueDTO.getTotalBudget();
            } else {
                if (month.equals("")) {
                    month = salesRevenueDTO.getRevenueMonth();
                    monthlyAmount += salesRevenueDTO.getTotalBudget();
                } else {
                    addToMap(monthMap, month, monthlyAmount, user);
                    month = salesRevenueDTO.getRevenueMonth();
                    monthlyAmount = 0;
                    monthlyAmount += salesRevenueDTO.getTotalBudget();
                }
            }
        }
        addToMap(monthMap, month, monthlyAmount, user);
    }

    private void addToMap(HashMap<String, List<MyTeamSalesRevenueMemberDTO>> monthMap, String month, double monthlyAmount, String user) {
        List<MyTeamSalesRevenueMemberDTO> selasRevenueList = monthMap.get(month);
        if (selasRevenueList == null) {

            selasRevenueList = new ArrayList<>();

            MyTeamSalesRevenueMemberDTO data = new MyTeamSalesRevenueMemberDTO();
            data.setAmount((long) monthlyAmount);
            data.setMeName(user);
            selasRevenueList.add(data);

            monthMap.put(month, selasRevenueList);


        } else {
            MyTeamSalesRevenueMemberDTO data = new MyTeamSalesRevenueMemberDTO();
            data.setAmount(((long) monthlyAmount));
            data.setMeName(user);
            selasRevenueList.add(data);
        }
    }
}
