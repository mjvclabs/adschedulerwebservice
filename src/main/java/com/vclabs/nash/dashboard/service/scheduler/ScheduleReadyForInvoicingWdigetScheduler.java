package com.vclabs.nash.dashboard.service.scheduler;

import com.vclabs.nash.dashboard.service.SchedulesReadyForInvoicingWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Sanduni on 21/01/2019
 */
@Component
public class ScheduleReadyForInvoicingWdigetScheduler {

    private static final Logger logger = LoggerFactory.getLogger(ScheduleReadyForInvoicingWdigetScheduler.class);

    @Autowired
    private SchedulesReadyForInvoicingWidgetService schedulesReadyForInvoicingWidgetService;

    //@Scheduled(cron = "${cron.schedule.ready.for.invoicing.widget}")
    public void schedulesReadyForInvoicingWidgetDataSet(){
        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("schedulesReadyForInvoicing Update Schedule started");

        schedulesReadyForInvoicingWidgetService.setSchedulesReadyForInvoicingData();

        logger.debug("/////////////////////////////////////////////////////////////////");
        logger.debug("schedulesReadyForInvoicing Update Schedule finished");
    }
}
