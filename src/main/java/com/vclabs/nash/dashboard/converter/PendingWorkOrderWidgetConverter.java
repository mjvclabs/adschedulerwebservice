package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.Url;
import com.vclabs.dashboard.core.data.model.vbar_widget.VBar;
import com.vclabs.dashboard.core.data.model.vbar_widget.pwo.Group;
import com.vclabs.dashboard.core.data.model.vbar_widget.pwo.PWOModalConfig;
import com.vclabs.dashboard.core.data.model.vbar_widget.pwo.PWOModalRow;
import com.vclabs.dashboard.core.data.model.vbar_widget.pwo.VBarPendingWOWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.PendingWOCountByAutoStatusDto;
import com.vclabs.nash.model.entity.WorkOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Sanduni on 29/11/2018
 */
@Component
public class PendingWorkOrderWidgetConverter extends WidgetDMConverter {

    @Value("${nash.url.update.workorders}")
    private String UPDATE_WO_URL;

    @Value("${nash.url.update.schedule}")
    private String UPDATE_SCHEDULE_URL;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.PENDING_WORK_ORDER);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        HashMap<String, List<PendingWOCountByAutoStatusDto>> pendingWOAutoStatus = (HashMap<String, List<PendingWOCountByAutoStatusDto>>) objects[0];

        VBarPendingWOWidgetDM pendingWOWidgetDM = new VBarPendingWOWidgetDM();
        pendingWOWidgetDM.setTitle("Pending WO");
        pendingWOWidgetDM.setWidgetId("pendingWorkOrders");

        PWOModalConfig modalConfig = new PWOModalConfig();
        List<String> tableHeaders = Arrays.asList("WorkOrder ID", "Schedule No", "Client", "Status");
        modalConfig.setTableHeaders(tableHeaders);

        List<Group> groups = new ArrayList<>();

        Iterator iterator_auto = pendingWOAutoStatus.entrySet().iterator();
        while (iterator_auto.hasNext()){
            Map.Entry pair = (Map.Entry)iterator_auto.next();
            String month = pair.getKey().toString();
            List<PendingWOCountByAutoStatusDto> woMonthlyAutoDataSet = (List<PendingWOCountByAutoStatusDto>)pair.getValue();

            Group group = new Group();
            group.setId(1);
            group.setGroupName(month);

            List<VBar> chartData = new ArrayList<>();
            List<PWOModalRow> modalData = new ArrayList<>();

            for (PendingWOCountByAutoStatusDto data: woMonthlyAutoDataSet){
                modalData.add(setModalRow(data));
            }
            HashMap<String, List<PendingWOCountByAutoStatusDto>> autoStatusDataMap = setAutoStatusData(woMonthlyAutoDataSet);

            Iterator iterator1 = autoStatusDataMap.entrySet().iterator();
            while (iterator1.hasNext()) {
                Map.Entry pair1 = (Map.Entry) iterator1.next();
                List<PendingWOCountByAutoStatusDto> list = (List<PendingWOCountByAutoStatusDto>)pair1.getValue();
                VBar vBar = new VBar();
                vBar.setId(Integer.valueOf(pair1.getKey().toString()));
                vBar.setX(WorkOrder.AutoStatus.values()[Integer.valueOf(pair1.getKey().toString())].getTextValue());
                vBar.setY(list.size());
                if(chartData.size()%2==0){
                    vBar.setBackgroundColor("#ED594B");
                }else{
                    vBar.setBackgroundColor("#F99C55");
                }
                chartData.add(vBar);
            }
            group.setChartData(chartData);
            group.setModalData(modalData);
            groups.add(group);
        }
        pendingWOWidgetDM.setModalConfig(modalConfig);
        pendingWOWidgetDM.setGroups(groups);

        return pendingWOWidgetDM;
    }

    private PWOModalRow setModalRow(PendingWOCountByAutoStatusDto statusDto){
        PWOModalRow modalRow = new PWOModalRow();
        Url woUrl = new Url();
        woUrl.setLabel(statusDto.getWoId().toString());
        woUrl.setUrl(UPDATE_WO_URL + statusDto.getWoId());

        Url scheduleUrl = new Url();
        scheduleUrl.setLabel(String.valueOf(statusDto.getScheduleId()));
        scheduleUrl.setUrl(UPDATE_SCHEDULE_URL);

        modalRow.setC1(woUrl);
        modalRow.setC2(scheduleUrl);
        modalRow.setC3(statusDto.getClient());
        modalRow.setC4(WorkOrder.AutoStatus.values()[statusDto.getAutoStatus()].getTextValue());
        return modalRow;
    }

    private HashMap<String, List<PendingWOCountByAutoStatusDto>> setAutoStatusData(List<PendingWOCountByAutoStatusDto> workOrders) {
        HashMap<String, List<PendingWOCountByAutoStatusDto>> woMap = new HashMap<>();

        int statusCount = WorkOrder.AutoStatus.values().length;
        for (int i=0;  i<statusCount; i++){
            woMap.put(String.valueOf(i), new ArrayList<>());
        }
        List<PendingWOCountByAutoStatusDto> woList = new ArrayList<>();
        for (PendingWOCountByAutoStatusDto data : workOrders) {
            if(woMap.containsKey(String.valueOf(data.getAutoStatus()))){
                woList.add(data);
                woMap.get(String.valueOf(data.getAutoStatus())).add(data);
            }else {
                List<PendingWOCountByAutoStatusDto> newWoList = new ArrayList<>();
                newWoList.add(data);
                woMap.put(String.valueOf(data.getAutoStatus()), newWoList);
            }
        }
        return woMap;
    }

}
