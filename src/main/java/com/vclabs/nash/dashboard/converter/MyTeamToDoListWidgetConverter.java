package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.node_widget.Node;
import com.vclabs.dashboard.core.data.model.node_widget.NodeDetails;
import com.vclabs.dashboard.core.data.model.node_widget.NodeWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyTeamToDoListDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 10/01/2019
 */
@Component
public class MyTeamToDoListWidgetConverter extends WidgetDMConverter {

    @Value("${nash.url.update.workorders}")
    private String UPDATE_WO_URL;

    int memberListCount = 0;
    int nodeNumber = 0;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.MY_TEAM_TO_DO_LIST);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        NodeWidgetDM nodeWidgetDM = new NodeWidgetDM();
        List<MyTeamToDoListDto> myTeamToDoList = (List<MyTeamToDoListDto>) objects[0];
        List<Node> nodes = new ArrayList<>();

        memberListCount = 0;


        if(myTeamToDoList.size() > 0){
            String currentMember = myTeamToDoList.get(0).getTeamMember();
            List<MyTeamToDoListDto> currentMemberToDoList = new ArrayList<>();
            for (MyTeamToDoListDto data : myTeamToDoList){
                if(data.getTeamMember().equals(currentMember)){
                    currentMemberToDoList.add(data);
                }else {
                    Node node = createNode(currentMemberToDoList);
                    nodes.add(node);
                    currentMember = data.getTeamMember();
                    currentMemberToDoList.add(data);
                }
            }
            if(currentMemberToDoList.size()== myTeamToDoList.size()){
                Node node = createNode(currentMemberToDoList);
                nodes.add(node);
            }
        }
        nodeWidgetDM.setNodes(nodes);
        nodeWidgetDM.setTitle("My Team To Do List");
        nodeWidgetDM.setWidgetId("myTeamToDoList");
        return nodeWidgetDM;
    }

    private Node createNode(List<MyTeamToDoListDto> currentMemberToDoList){
        Node node = new Node();
        if(currentMemberToDoList.size()>0 && memberListCount<currentMemberToDoList.size()){
            nodeNumber++;
            List<NodeDetails> nodeDetails = new ArrayList<>();
            node.setId(nodeNumber);
            node.setName(currentMemberToDoList.get(memberListCount).getTeamMember());
            int count = 0;
            int currentCount = memberListCount;
            for (int i = memberListCount; i < currentMemberToDoList.size(); i++){
                NodeDetails nodeDetail = new NodeDetails();
                nodeDetail.setId(currentMemberToDoList.get(i).getWoId());
                nodeDetail.setText(currentMemberToDoList.get(i).getWoName());
                nodeDetail.setUrl(UPDATE_WO_URL + currentMemberToDoList.get(i).getWoId());
                nodeDetails.add(nodeDetail);
                count++;
            }
            memberListCount = memberListCount + count;
            node.setCount(currentMemberToDoList.size()- currentCount);
            node.setDetails(nodeDetails);
        }
        return node;
    }
}
