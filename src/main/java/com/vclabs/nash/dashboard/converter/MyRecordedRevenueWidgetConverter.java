package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.vbar_widget.VBar;
import com.vclabs.dashboard.core.data.model.vbar_widget.VBarBasicWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyRecordedRevenueDto;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 04/12/2018
 */
@Component
public class MyRecordedRevenueWidgetConverter extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.MY_RECORDED_REVENUE);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<MyRecordedRevenueDto> myRecordedRevenueDtoList = (List<MyRecordedRevenueDto>) objects[0];
        VBarBasicWidgetDM dataModel = new VBarBasicWidgetDM();
        dataModel.setTitle("My Recorded Revenue");
        dataModel.setWidgetId("myRecordedRevenue");

        List<VBar> chartData = new ArrayList<>();
        String barColor = "##56C7D7";

        for (MyRecordedRevenueDto recordedRevenue : myRecordedRevenueDtoList){
            VBar vBar = new VBar();
            vBar.setX(recordedRevenue.getRevenueMonth());
            vBar.setY(Long.valueOf(recordedRevenue.getTotalBudget().intValue()));
            vBar.setBackgroundColor(barColor);
            chartData.add(vBar);

            if(chartData.size() % 2 == 0){
                barColor = "#56C7D7";
            }else{
                barColor = "#FFD600";
            }
        }
        dataModel.setChartData(chartData);
        return dataModel;
    }
}
