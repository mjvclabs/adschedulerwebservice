package com.vclabs.nash.dashboard.converter;

import com.itextpdf.text.pdf.PRAcroForm;
import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.DropDown;
import com.vclabs.dashboard.core.data.model.table_widget.pa_widget.PAModalConfig;
import com.vclabs.dashboard.core.data.model.table_widget.pa_widget.PATableRow;
import com.vclabs.dashboard.core.data.model.table_widget.pa_widget.PATableWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.PriorityAdvertisementWidgetDto;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sanduni on 07/12/2018
 */
@Component
public class PriorityAdvertisementWidgetConverter extends WidgetDMConverter{

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.PRIORITY_ADVERTISEMENT);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<PriorityAdvertisementWidgetDto> adverts = (List<PriorityAdvertisementWidgetDto>)objects[0];
        PATableWidgetDM tableWidgetDM = new PATableWidgetDM<>();
        List<PATableRow> tableRows = new ArrayList<>();

        tableWidgetDM.setTitle("Priority Advertisement");
        tableWidgetDM.setWidgetId("priorityAdvertisement");

        PAModalConfig modalConfig = new PAModalConfig();
        List<DropDown> dropDowns = createDropDowns();

        for (PriorityAdvertisementWidgetDto ad : adverts){
            PATableRow row = new PATableRow();
            row.setId(ad.getAdvertId().toString());
            row.setC1(ad.getAdvertName());
            row.setC2("Priority No " +ad.getPriorityNo());
            row.setC3(ad.getTimeBelt());
            row.setC4(setStatus(ad.getStatus()));
            row.setC5(ad.getChannel());
            tableRows.add(row);
        }

        List<String> tableHeaders = Arrays.asList("Name", "Priority No", "Hour", "Status");
        modalConfig.setDropDowns(dropDowns);
        tableWidgetDM.setModalConfig(modalConfig);
        tableWidgetDM.setTableHeaders(tableHeaders);
        tableWidgetDM.setRows(tableRows);


        return tableWidgetDM;
    }

    private List<DropDown> createDropDowns(){
        List<DropDown> dropDowns = new ArrayList<>();

        DropDown channelDropDown = new DropDown();
        channelDropDown.setName("channels");
        channelDropDown.setParamName("c5");
        channelDropDown.setFirstOption("Select Channel...");
        dropDowns.add(channelDropDown);

        DropDown priorityDropDown = new DropDown();
        priorityDropDown.setName("priority");
        priorityDropDown.setParamName("c2");
        priorityDropDown.setFirstOption("Select Priority No...");
        dropDowns.add(priorityDropDown);

        DropDown statusDropDown = new DropDown();
        statusDropDown.setName("status");
        statusDropDown.setParamName("c4");
        statusDropDown.setFirstOption("Select Status...");
        dropDowns.add(statusDropDown);

        return dropDowns;
    }

    private String setStatus(int status){
        String statusLabel = "";

        switch (status) {
            case 1: statusLabel = "Active";
                    break;
            case 2: statusLabel = "Hold";
                    break;
            case 3: statusLabel = "Deleted";
                    break;
            default: statusLabel = "";
                    break;
        }

        return statusLabel;
    }
}
