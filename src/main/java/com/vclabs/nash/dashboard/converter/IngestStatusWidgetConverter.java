package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.Url;
import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;
import com.vclabs.dashboard.core.data.model.table_widget.WOTableRow;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.IngestStatusWidgetDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Sanduni on 06/12/2018
 */
@Component
public class IngestStatusWidgetConverter extends WidgetDMConverter{

    @Value("${nash.url.update.advertisement}")
    private String UPDATE_ADVERTISEMENT_URL;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.INGEST_STATUS);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<IngestStatusWidgetDto> adverts = (List<IngestStatusWidgetDto>) objects[0];
        TableWidgetDM<WOTableRow> tableWidgetDM = new TableWidgetDM<>();
        List<WOTableRow> tableRows = new ArrayList<>();
        List<String> tableHeaders = Arrays.asList("Cut#", "Client", "Status");

        for (IngestStatusWidgetDto advert : adverts){
            WOTableRow row = new WOTableRow();

            Url url = new Url();
            url.setLabel(advert.getCutId().toString());
            url.setUrl(UPDATE_ADVERTISEMENT_URL + advert.getCutId());

            row.setC1(url);
            row.setC2(advert.getClient());
            if(advert.getStatus()==1) {
                row.setC3(""+1);
            }else if(advert.getStatus()==2){
                row.setC3(""+2);
            }else if(advert.getStatus()==3){
                row.setC3(""+3);
            }

            row.setId(advert.getCutId().toString());

            tableRows.add(row);
        }
        tableWidgetDM.setTitle("Ingest Status");
        tableWidgetDM.setWidgetId("ingestStatus");
        tableWidgetDM.setTableHeaders(tableHeaders);
        tableWidgetDM.setRows(tableRows);
        return tableWidgetDM;
    }
}
