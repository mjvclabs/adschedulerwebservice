package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.col_widget.DataRow;
import com.vclabs.dashboard.core.data.model.col_widget.TwoColumnWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.CurrentPredictionViewDto;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 24/01/2019
 */
@Component
public class CurrentPredictionViewConverter extends WidgetDMConverter{

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.CURRENT_PREDICTION_VIEW);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<CurrentPredictionViewDto> predictionDtos = (List<CurrentPredictionViewDto>) objects[0];
        TwoColumnWidgetDM twoColumnWidgetDM = new TwoColumnWidgetDM();
        List<DataRow> dataRows = new ArrayList<>();

        for (CurrentPredictionViewDto data : predictionDtos){
            DataRow dataRow = new DataRow();
            dataRow.setId(data.getId());
            dataRow.setIcon(data.getIcon());
            dataRow.setCount(data.getCount());
            dataRows.add(dataRow);
        }
        twoColumnWidgetDM.setData(dataRows);
        twoColumnWidgetDM.setTitle("Current Prediction View");
        twoColumnWidgetDM.setWidgetId("currentPredictionView");
        return twoColumnWidgetDM;
    }
}
