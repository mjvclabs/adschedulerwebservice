package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.vbar_pie_widget.Group;
import com.vclabs.dashboard.core.data.model.vbar_pie_widget.Record;
import com.vclabs.dashboard.core.data.model.vbar_pie_widget.VBarPieChartWidget;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.MyTeamSalesRevenueDTO;
import com.vclabs.nash.dashboard.dto.MyTeamSalesRevenueMemberDTO;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Nalaka on 2019-01-13.
 */
@Component
public class MyTeamSalesRevenueConverter extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.MY_TEAM_SALES_REVENUE);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {

        List<MyTeamSalesRevenueDTO> revenueList = (List<MyTeamSalesRevenueDTO>) objects[0];

        VBarPieChartWidget vBarPieChartWidget = new VBarPieChartWidget();
        vBarPieChartWidget.setTitle("My Team Sales Revenue");
        vBarPieChartWidget.setWidgetId("myTeamSalesRevenue");
        List<Group> rowData = new ArrayList<>();
        int count = 0;
        for (MyTeamSalesRevenueDTO myTeamSalesRevenueDTO : revenueList) {
            Group group = new Group();
            group.setId(count);
            if(myTeamSalesRevenueDTO.getMonth().equals("")){
                group.setGroup("All");
            }else{
                group.setGroup(myTeamSalesRevenueDTO.getMonth());
            }

            List<Record> recordsList = new ArrayList<>();

            List<MyTeamSalesRevenueMemberDTO> memberDTOList = myTeamSalesRevenueDTO.getMemberLis();
            for (MyTeamSalesRevenueMemberDTO memberDTO : memberDTOList) {
                Record record = new Record();
                record.setName(memberDTO.getMeName());
                record.setAmount(memberDTO.getAmount());
                record.setBackgroundColor("#C995C5");
                recordsList.add(record);
            }
            group.setRecords(recordsList);
            rowData.add(group);
        }
        vBarPieChartWidget.setRowData(rowData);
        List<String> tableHeaders = Arrays.asList("ME", "Renenue");
        vBarPieChartWidget.setTableHeaders(tableHeaders);
        return vBarPieChartWidget;
    }
}
