package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.chart_config.YAxisScaleLabel;
import com.vclabs.dashboard.core.data.model.vbar_widget.VBar;
import com.vclabs.dashboard.core.data.model.vbar_widget.htc.Group;
import com.vclabs.dashboard.core.data.model.vbar_widget.htc.HTCChartConfig;
import com.vclabs.dashboard.core.data.model.vbar_widget.htc.HTCModalConfig;
import com.vclabs.dashboard.core.data.model.vbar_widget.htc.VBarHTCWidgetDM;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.HightTrafficChannelsWidgetDto;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * Created by Sanduni on 27/11/2018
 */
@Component
public class HighTrafficChannelWidgetConverter extends WidgetDMConverter {

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.HIGH_TRAFFIC_CHANNELS);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {
        List<HightTrafficChannelsWidgetDto> channelsWidgetDtos = (List<HightTrafficChannelsWidgetDto>) objects[0];
        VBarHTCWidgetDM dataModel = new VBarHTCWidgetDM();
        dataModel.setTitle("High Traffic Channels");
        dataModel.setWidgetId("highTrafficChannels");

        List<String> tableHeaders = Arrays.asList("Channel", "%", "Current User", "Login Time", "Previous Users");

        HTCModalConfig modalConfig = new HTCModalConfig();
        modalConfig.setDdAllOption("All Channels...");
        modalConfig.setTableHeaders(tableHeaders);

        YAxisScaleLabel yAxisScaleLabel = new YAxisScaleLabel();
        yAxisScaleLabel.setLabelString("Traffic");

        HTCChartConfig chartConfig = new HTCChartConfig();
        chartConfig.setYaxisScaleLabel(yAxisScaleLabel);

        List<Group> modalData = new ArrayList<>();

        List<VBar> vBars = new ArrayList<>();
        String barColor = "";

        for (HightTrafficChannelsWidgetDto channel : channelsWidgetDtos){
            Group group = new Group();
            group.setId(channel.getChannelId());
            group.setGroupName(channel.getChannelName());
            group.setRecords(new ArrayList<>());

            if(vBars.size() % 2 == 0){
                barColor = "#3DC9C6";
            }else{
                barColor = "#E22D74";
            }
            VBar vBar = new VBar();

            vBar.setId(Integer.parseInt(channel.getChannelId().toString()));
            vBar.setX(channel.getChannelName());
            vBar.setY(Integer.parseInt(channel.getSpotCount().toString()));
            vBar.setBackgroundColor(barColor);
            vBars.add(vBar);
        }
        dataModel.setModalData(modalData);
        dataModel.setChartData(vBars);
        dataModel.setChartConfig(chartConfig);
        dataModel.setModalConfig(modalConfig);
        return dataModel;
    }
}
