package com.vclabs.nash.dashboard.converter;

import com.vclabs.dashboard.core.data.convert.WidgetDMConverter;
import com.vclabs.dashboard.core.data.model.BasicWidgetDM;
import com.vclabs.dashboard.core.data.model.common.Url;
import com.vclabs.dashboard.core.data.model.table_widget.TableRow;
import com.vclabs.dashboard.core.data.model.table_widget.TableWidgetDM;
import com.vclabs.dashboard.core.data.model.table_widget.WOTableRow;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.PartiallyEnteredWODto;
import com.vclabs.nash.model.dao.WorkOrderChannelListDAO;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.service.ClientDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 27/11/2018
 */
@Component
public class PartiallyEnteredWorkOrderWidgetConverter extends WidgetDMConverter {

    @Value("${nash.url.update.workorders}")
    private String UPDATE_WO_URL;

    @Autowired
    private ClientDetailService clientDetailService;

    @Autowired
    private WorkOrderChannelListDAO workOrderChannelListDAO;

    @PostConstruct
    @Override
    public void init() {
        super.setName(NashDMConverterName.PARTIALLY_ENTERED_WORK_ORDER);
    }

    @Override
    public BasicWidgetDM convert(Object... objects) {

        TableWidgetDM tableWidgetDM = new TableWidgetDM();
        List<PartiallyEnteredWODto> newWorkOrderList = (List<PartiallyEnteredWODto>) objects[0];
        List<WOTableRow> tableRows = new ArrayList<>();
        tableWidgetDM.setTitle("Partially Entered WO");
        tableWidgetDM.setWidgetId("Partially Entered WO");

        for (PartiallyEnteredWODto newWorkOrder : newWorkOrderList) {
            WOTableRow tableRow = new WOTableRow();
            tableRow.setId(newWorkOrder.getWoId().toString());

            Url url = new Url();
            url.setUrl(UPDATE_WO_URL + newWorkOrder.getWoId());
            url.setLabel("#"+newWorkOrder.getWoId());
            tableRow.setC1(url);
            tableRow.setId(newWorkOrder.getWoId().toString());
            tableRow.setC2(newWorkOrder.getClient());
            tableRow.setC3(String.valueOf(newWorkOrder.getSpots()));
            tableRows.add(tableRow);
        }
        tableWidgetDM.setRows(tableRows);

        return tableWidgetDM;
    }

}
