package com.vclabs.nash.mosh_connector;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.controller.UserDetailsController;
import com.vclabs.nash.model.view.ScheduleView;
import com.vclabs.nash.service.PlayListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by dperera on 21/11/2019.
 */
//@Component
public class PlayListServer {

//    private static final Logger LOGGER = LoggerFactory.getLogger(PlayListServer.class);
//
//    private ServerSocket serverSocket;
//
//    @Autowired
//    private PlayListService playListService;
//
//    @Async
//    public void start(int port) throws IOException {
//        this.serverSocket = new ServerSocket(port);
//        LOGGER.debug("Playlist server is started on the port : {}", port);
//        while (true){
//            new EchoClientHandler(serverSocket.accept()).start();
//        }
//    }
//
//    public void stop() throws IOException {
//        serverSocket.close();
//    }
//
//    private class EchoClientHandler extends Thread {
//
//        private Socket clientSocket;
//        private PrintWriter out;
//        private BufferedReader in;
//
//        public EchoClientHandler(Socket socket) {
//            this.clientSocket = socket;
//        }
//
//        public void run() {
//            try {
//                out = new PrintWriter(clientSocket.getOutputStream(), true);
//                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
//                String inputLine = in.readLine();
//                String result = doGenerateAdvertPlayList(inputLine);
//                out.println(result);
//                in.close();
//                out.close();
//                clientSocket.close();
//            } catch (IOException e) {
//                LOGGER.error("An exception has occurred while generating play list after starting the hour : {} ", e.getMessage());
//            }
//        }
//
//        private String doGenerateAdvertPlayList(String jsonString ) throws IOException {
//            ObjectMapper objectMapper = new ObjectMapper();
//            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//            PlayListGenerateRequest request = objectMapper.readValue(jsonString, PlayListGenerateRequest.class);
//            LOGGER.debug("Received play list generating request : channel_id = {}, hour = {}, next_cluster = {}", request.getChannel_id(), request.getHour(), request.getNext_cluster() );
//            ScheduleView scheduleView = playListService.generateAdvertPlayList(request.getChannel_id(), request.getHour(), request.getNext_cluster());
//            LOGGER.debug("Processing completed play list generating request : channel_id = {}, hour = {}, next_cluster = {}", request.getChannel_id(), request.getHour(), request.getNext_cluster() );
//            return objectMapper.writeValueAsString(scheduleView);
//        }
//    }
}
