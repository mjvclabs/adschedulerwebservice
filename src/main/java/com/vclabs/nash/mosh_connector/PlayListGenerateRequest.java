package com.vclabs.nash.mosh_connector;

/**
 * Created by dperera on 21/11/2019.
 */
public class PlayListGenerateRequest {

    private int channel_id;

    private int hour;

    private int next_cluster;

    public int getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(int channel_id) {
        this.channel_id = channel_id;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getNext_cluster() {
        return next_cluster;
    }

    public void setNext_cluster(int next_cluster) {
        this.next_cluster = next_cluster;
    }
}
