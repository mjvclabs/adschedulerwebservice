/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Sanira Nanayakkara
 */
public class ServerBasicInfo {
    
    @JsonProperty("serverID")
    private int serverID;
    @JsonProperty("serverIP")
    private String serverIP;
    @JsonProperty("serverPort")
    private int serverPort;
    @JsonProperty("channelPort")
    private int channelPort;
    @JsonProperty("servicePath")
    private String servicePath;
    @JsonProperty("inCard")
    private int inCard;
    @JsonProperty("outCard")
    private int outCard;

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getChannelPort() {
        return channelPort;
    }

    public void setChannelPort(int channelPort) {
        this.channelPort = channelPort;
    }

    public String getServicePath() {
        return servicePath;
    }

    public void setServicePath(String servicePath) {
        this.servicePath = servicePath;
    }

    public int getInCard() {
        return inCard;
    }

    public void setInCard(int inCard) {
        this.inCard = inCard;
    }

    public int getOutCard() {
        return outCard;
    }

    public void setOutCard(int outCard) {
        this.outCard = outCard;
    }

    public int getServerID() {
        return serverID;
    }

    public void setServerID(int serverID) {
        this.serverID = serverID;
    }
}
