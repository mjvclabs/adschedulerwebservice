/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

/**
 *
 * @author hp
 */
public class InvoiceDataInfo {

    @JsonProperty("invoiceDataID")
    private Integer invoiceDataID;

    @JsonProperty("invoiceID")
    private Integer invoiceID;

    @JsonProperty("invoiceDate")
    private Date invoiceDate;

    @JsonProperty("invoiceNo")
    private Integer invoiceNo;

    @JsonProperty("handOverDate_ME")
    private String handOverDate_ME;

    @JsonProperty("handOverDate_Agency")
    private String handOverDate_Agency;

    @JsonProperty("Payment_Date")
    private Date Payment_Date;

    @JsonProperty("paidRef")
    private String paidRef;

    @JsonProperty("paidAmount")
    private double paidAmount;

    @JsonProperty("difference")
    private String difference;

    @JsonProperty("clearingDocNo")
    private String clearingDocNo;

    public InvoiceDataInfo() {
    }

    public Integer getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(Integer invoiceID) {
        this.invoiceID = invoiceID;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Integer invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getHandOverDate_ME() {
        return handOverDate_ME;
    }

    public void setHandOverDate_ME(String handOverDate_ME) {
        this.handOverDate_ME = handOverDate_ME;
    }

    public String getHandOverDate_Agency() {
        return handOverDate_Agency;
    }

    public void setHandOverDate_Agency(String handOverDate_Agency) {
        this.handOverDate_Agency = handOverDate_Agency;
    }

    public Date getPayment_Date() {
        return Payment_Date;
    }

    public void setPayment_Date(Date Payment_Date) {
        this.Payment_Date = Payment_Date;
    }

    public String getPaidRef() {
        return paidRef;
    }

    public void setPaidRef(String paidRef) {
        this.paidRef = paidRef;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getClearingDocNo() {
        return clearingDocNo;
    }

    public void setClearingDocNo(String clearingDocNo) {
        this.clearingDocNo = clearingDocNo;
    }

    public Integer getInvoiceDataID() {
        return invoiceDataID;
    }

    public void setInvoiceDataID(Integer invoiceDataID) {
        this.invoiceDataID = invoiceDataID;
    }

}
