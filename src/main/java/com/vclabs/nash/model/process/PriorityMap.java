package com.vclabs.nash.model.process;

/**
 * @author chathuranga on 11/24/2020
 */
public class PriorityMap {

    private Integer scheduleId;
    private Integer priorityId;

    public PriorityMap(Integer scheduleId, Integer priorityId) {
        this.scheduleId = scheduleId;
        this.priorityId = priorityId;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Integer priorityId) {
        this.priorityId = priorityId;
    }
}
