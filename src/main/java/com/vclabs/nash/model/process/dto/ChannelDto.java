package com.vclabs.nash.model.process.dto;

/**
 * Created by Sanduni on 05/10/2018
 */
public class ChannelDto {

    private Integer channelid;

    private String channelname;

    public Integer getChannelid() {
        return channelid;
    }

    public void setChannelid(Integer channelid) {
        this.channelid = channelid;
    }

    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname;
    }
}
