package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * @author Sanira Nanayakkara
 */
public class SchedulePatternInfo {
    @JsonProperty("workOrderId")
    private int workOrderId;
    @JsonProperty("lstChannelId")
    private List<Integer> lstChannelIds;
    @JsonProperty("advertId")
    private int advertId;
    @JsonProperty("startDate")
    private String scheduleStartDate;
    @JsonProperty("endDate")
    private String scheduleEndDate;
    
    @JsonProperty("startTime")
    private String scheduleStartTime;
    @JsonProperty("endTime")
    private String scheduleEndTime;
    
    @JsonProperty("slotCount")
    private int slotCount;
    @JsonProperty("lstDays")
    private List<Integer> lstDays;
    
    @JsonProperty("revenueMonth")
    private String revenueMonth;
    @JsonProperty("revenueLine")
    private String revenueLine;

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public List<Integer> getLstChannelIds() {
        return lstChannelIds;
    }

    public void setLstChannelIds(List<Integer> lstChannelIds) {
        this.lstChannelIds = lstChannelIds;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public String getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(String scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public String getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(String scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    public String getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(String scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public String getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(String scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public int getSlotCount() {
        return slotCount;
    }

    public void setSlotCount(int slotCount) {
        this.slotCount = slotCount;
    }

    public List<Integer> getLstDays() {
        return lstDays;
    }

    public void setLstDays(List<Integer> lstDays) {
        this.lstDays = lstDays;
    }

    public String getRevenueLine() {
        return revenueLine;
    }

    public void setRevenueLine(String revenueLine) {
        this.revenueLine = revenueLine;
    }

    public String getRevenueMonth() {
        return revenueMonth;
    }

    public void setRevenueMonth(String revenueMonth) {
        this.revenueMonth = revenueMonth;
    }
}
