package com.vclabs.nash.model.process.dto;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 * Created by Sanduni on 02/01/2019
 */
public class WODropDownDataDto {

    private Integer workorderid;

    private String ordername;

    private Date startDate;

    private Date endDate;

    public WODropDownDataDto(Integer workorderid, String ordername, Date startDate, Date endDate) {
        this.workorderid = workorderid;
        this.ordername = ordername;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(Integer workorderid) {
        this.workorderid = workorderid;
    }

    public String getOrdername() {
        return ordername;
    }

    public void setOrdername(String ordername) {
        this.ordername = ordername;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
