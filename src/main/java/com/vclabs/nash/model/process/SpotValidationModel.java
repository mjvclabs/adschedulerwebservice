/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

/**
 *
 * @author user
 */
public class SpotValidationModel {

    private int channelID;
    private int advertDuration;
    private int availableSpot = 0;
    private int requestedSpot = 0;
    private int checkSpot = 0;
    private long totalAvailableDuration = 0;

    public SpotValidationModel() {
    }

    public int getChannelID() {
        return channelID;
    }

    public void setChannelID(int channelID) {
        this.channelID = channelID;
    }

    public int getAdvertDuration() {
        return advertDuration;
    }

    public void setAdvertDuration(int advertDuration) {
        this.advertDuration = advertDuration;
    }

    public int getAvailableSpot() {
        return availableSpot;
    }

    public void setAvailableSpot(int availableSpot) {
        this.availableSpot = availableSpot;
    }

    public int getRequestedSpot() {
        return requestedSpot;
    }

    public void setRequestedSpot(int requestedSpot) {
        this.requestedSpot = requestedSpot;
    }

    public int getCheckSpot() {
        return checkSpot;
    }

    public void setCheckSpot() {
        this.checkSpot++;
    }

    public void addSpotForRE() {
        requestedSpot++;
    }

    public long getTotalAvailableDuration() {
        return totalAvailableDuration;
    }

    public void setTotalAvailableDuration(long totalAvailableDuration) {
        this.totalAvailableDuration = totalAvailableDuration;
    }

}
