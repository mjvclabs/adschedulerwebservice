/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author user
 */
public class ClientDetailInfo {

    @JsonProperty("clientId")
    private int clientId;

    @JsonProperty("clientName")
    private String clientName;

    @JsonProperty("clientType")
    private String clientType;

    @JsonProperty("address")
    private String address;

    @JsonProperty("email")
    private String email;

    @JsonProperty("vat")
    private String vat;

    @JsonProperty("contact_person")
    private String contact_person;

    @JsonProperty("telephone_num")
    private String telephone_num;

    @JsonProperty("cx_cade")
    private String cx_cade;

    @JsonProperty("contact_details")
    private String contact_details;

    @JsonProperty("balack_list")
    private int balack_list;

    public ClientDetailInfo() {
    }

    public ClientDetailInfo(String clientName, String ClientType, String address, String email, String telephone_num, String contact_details) {
        this.clientName = clientName;
        this.clientType = ClientType;
        this.address = address;
        this.email = email;
        this.telephone_num = telephone_num;
        this.contact_details = contact_details;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String ClientType) {
        this.clientType = ClientType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone_num() {
        return telephone_num;
    }

    public void setTelephone_num(String telephone_num) {
        this.telephone_num = telephone_num;
    }

    public String getContact_details() {
        return contact_details;
    }

    public void setContact_details(String contact_details) {
        this.contact_details = contact_details;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getCx_cade() {
        return cx_cade;
    }

    public void setCx_cade(String cx_cade) {
        this.cx_cade = cx_cade;
    }

    public int getBalack_list() {
        return balack_list;
    }

    public void setBalack_list(int balack_list) {
        this.balack_list = balack_list;
    }

}
