package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChannelAdvertInfo {
    @JsonProperty("channelId")
    private int channelId;

    @JsonProperty("advertId")
    private int advertId;

    @JsonProperty("percentage")
    private int percentage;

    public ChannelAdvertInfo() {
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

}
