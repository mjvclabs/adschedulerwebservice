/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;
import com.vclabs.nash.model.view.reporting.MaterialAnalysis;

/**
 *
 * @author hp
 */
@Component
public class MaterialAnalysisExcel extends Common {

    private final String WEBDIRECTORY = "MaterialAnalysisExcel";
    private final String FILEPATH = "MaterialAnalysisReport.xls";

    private CellStyle cellStyle;
    private CellStyle tableHeaderStyle;

    public void generateMaterialAnalysisReport(List<MaterialAnalysis> materialAnalysisList, HttpServletRequest request) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();

        cellStyle = setDataRowCellStyle(workbook);
        tableHeaderStyle = tableHeaderStyle(workbook);
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("MaterialAnalysisReport");

        int rowCount = 1;

        HSSFRow rowhead = sheet.createRow((short) rowCount);
        rowCount++;
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead);

        for (MaterialAnalysis materialAnalysis : materialAnalysisList) {
            int cellCount = 0;

            HSSFRow row = sheet.createRow((short) rowCount);
            rowCount++;

            Cell cell;

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(materialAnalysis.getCutId());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(materialAnalysis.getAdvertisementName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(materialAnalysis.getAgency());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(materialAnalysis.getClient());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(materialAnalysis.getCaptureDate().toString());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            if (materialAnalysis.getLastAiredDate() != null) {
                cell.setCellValue(materialAnalysis.getLastAiredDate().toString());
            } else {
                cell.setCellValue("Not aired");
            }
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(materialAnalysis.getExpiredDate().toString());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(materialAnalysis.getStatus());
            cell.setCellStyle(cellStyle);

        }

        aligncolumns(sheet, count);
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }

    public int createExcelHeaderFields(HSSFRow rowhead) {
        int fieldCount = 0;

        Cell cell;
        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Cut ID");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Advertisement");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Agency");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Client");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Capture date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Last Aired date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Expired date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Status");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        return fieldCount;
    }

    public void downloadMaterianAnalysisReportExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }
}
