package com.vclabs.nash.model.process;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2019-11-08.
 */
public class PlayListViewInfo {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH");

    private List<Integer> channelIds;
    private String sfromDate;
    private String stoDate;
    private String sfromTime;
    private String stoTime;

    private Date fromDate;
    private Date toDate;
    private Date fromTime;
    private Date toTime;


    public PlayListViewInfo() {

    }

    public List<Integer> getChannelIds() {
        return channelIds;
    }

    public void setChannelIds(List<Integer> channelIds) {
        this.channelIds = channelIds;
    }

    public String getSfromDate() {
        return sfromDate;
    }

    public void setSfromDate(String sfromDate) {
        this.sfromDate = sfromDate;
    }

    public String getStoDate() {
        return stoDate;
    }

    public void setStoDate(String stoDate) {
        this.stoDate = stoDate;
    }

    public String getSfromTime() {
        return sfromTime;
    }

    public void setSfromTime(String sfromTime) {
        this.sfromTime = sfromTime;
    }

    public String getStoTime() {
        return stoTime;
    }

    public void setStoTime(String stoTime) {
        this.stoTime = stoTime;
    }

    public Date getFromDate() throws ParseException {
        return dateFormat.parse(this.sfromDate);
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() throws ParseException {
        return dateFormat.parse(this.stoDate);
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getFromTime() throws ParseException {
        return timeFormat.parse("1970-01-01 " + sfromTime);
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public Date getToTime() throws ParseException {
        return timeFormat.parse("1970-01-01 " + stoTime);
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }
}
