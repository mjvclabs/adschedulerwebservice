/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author user
 */
public class ScheduleInfo {

    @JsonProperty("workOrderId")
    private int workOrderId;
    @JsonProperty("channelId")
    private int channelId;
    @JsonProperty("advertId")
    private int advertId;
    @JsonProperty("startTime")
    private String scheduledStartDate;
    @JsonProperty("endTime")
    private String scheduledEndDate;
    @JsonProperty("schedulDate")
    private String schedulDate;
    @JsonProperty("advertCount")
    private int advertCount;
    @JsonProperty("programm")
    private String program = "Test program";
    @JsonProperty("scheduleIds")
    private List<Integer> scheduleIds;
    @JsonProperty("comment")
    private String comment = "No comment";
    @JsonProperty("revenueLine")
    private String revenueLine;
    @JsonProperty("month")
    private String month;
    private AdvertisementInfo advertDto;

    public ScheduleInfo() {

    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public String getScheduledStartDate() {
        return scheduledStartDate;
    }

    public void setScheduledStartDate(String scheduledStartDate) {
        this.scheduledStartDate = scheduledStartDate;
    }

    public String getScheduledEndDate() {
        return scheduledEndDate;
    }

    public void setScheduledEndDate(String scheduledEndDate) {
        this.scheduledEndDate = scheduledEndDate;
    }

    public String getSchedulDate() {
        return schedulDate;
    }

    public void setSchedulDate(String schedulDate) {
        this.schedulDate = schedulDate;
    }

    public int getAdvertCount() {
        return advertCount;
    }

    public void setAdvertCount(int advertCount) {
        this.advertCount = advertCount;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public List<Integer> getScheduleIds() {
        return scheduleIds;
    }

    public void setScheduleIds(List<Integer> scheduleIds) {
        this.scheduleIds = scheduleIds;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRevenueLine() {
        return revenueLine;
    }

    public void setRevenueLine(String revenueLine) {
        this.revenueLine = revenueLine;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public AdvertisementInfo getAdvertDto() {
        return advertDto;
    }

    public void setAdvertDto(AdvertisementInfo advertDto) {
        this.advertDto = advertDto;
    }

}
