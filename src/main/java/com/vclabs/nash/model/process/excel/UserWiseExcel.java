package com.vclabs.nash.model.process.excel;

import com.vclabs.nash.model.view.reporting.UserWiseView;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Nalaka on 2019-05-17.
 */
@Component
public class UserWiseExcel extends Common {

    private final String WEBDIRECTORY = "UserWise";
    private final String FILEPATH = "UserWiseReport.xls";

    private CellStyle cellStyle;
    private CellStyle tableHeaderStyle;

    public void generateUserWiseReport(List<UserWiseView> logList, HttpServletRequest request) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();

        cellStyle = setDataRowCellStyle(workbook);
        tableHeaderStyle = tableHeaderStyle(workbook);
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("UserWiseReport");

        int rowCount = 1;

        HSSFRow rowhead = sheet.createRow((short) rowCount);
        rowCount++;
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead);

        for (UserWiseView userWiseView : logList) {
            int cellCount = 0;

            HSSFRow row = sheet.createRow((short) rowCount);
            rowCount++;

            Cell cell;

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getChannelName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getLoginDate());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getLoginTime());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getLogoutDate());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getLogoutTime());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getSessionDuration());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getUserName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(userWiseView.getIpAddress());
            cell.setCellStyle(cellStyle);

        }

        aligncolumns(sheet, count);
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }

    public int createExcelHeaderFields(HSSFRow rowhead) {
        int fieldCount = 0;

        Cell cell;
        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Channel name");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Login Date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Login Time");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Logout Date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Logout Time");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Session Duration");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("User Name");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("IP Address");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        return fieldCount;
    }

    public void downloadUserWiseReportExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }
}
