/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;
import com.vclabs.nash.model.view.reporting.PaymentDue;

/**
 *
 * @author Nalaka
 * @since 01-03-2018
 */

@Component
public class PaymentDueExcel extends Common {
    
    private final String WEBDIRECTORY = "PaymentDueExcel";
    private final String FILEPATH = "PaymentDueReport.xls";

    private CellStyle cellStyle;
    private CellStyle tableHeaderStyle;

    public void generatePaymentDueReport(List<PaymentDue> paymentDueList, HttpServletRequest request) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();

        cellStyle = setDataRowCellStyle(workbook);
        tableHeaderStyle = tableHeaderStyle(workbook);
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("PaymentDueReport");

        int rowCount = 1;

        HSSFRow rowhead = sheet.createRow((short) rowCount);
        rowCount++;
        /*Set table header fields*/
        int count = createExcelHeaderFields(rowhead);

        DecimalFormat df = new DecimalFormat("#0.00");
        df.setRoundingMode(RoundingMode.DOWN);

        for (PaymentDue paymentDue : paymentDueList) {
            int cellCount = 0;

            HSSFRow row = sheet.createRow((short) rowCount);
            rowCount++;

            Cell cell;

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getWorkOrderId());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getAgencyName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getClientName());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getWorkOrderType());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getScheduleref());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getMarketingExecutive());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(df.format(paymentDue.getPackegeAmount()));
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getWorkOrderStatus());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            if (paymentDue.getInvoiceDate() != null) {
                cell.setCellValue(paymentDue.getInvoiceDate().toString());
            } else {
                cell.setCellValue("");
            }
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getInvoiceNo());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(cellCount);
            cellCount++;
            cell.setCellValue(paymentDue.getInvoiceAmount());
            cell.setCellStyle(cellStyle);

        }

        aligncolumns(sheet, count);
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }

    public int createExcelHeaderFields(HSSFRow rowhead) {
        int fieldCount = 0;

        Cell cell;
        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("WO ID");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Agency");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Client");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Wo type");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Schedule Ref");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("ME");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Package Amount");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("WO Status");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Invoice date");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Invoice no");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        cell = rowhead.createCell(fieldCount);
        cell.setCellValue("Invoice amount");
        cell.setCellStyle(tableHeaderStyle);
        fieldCount++;

        return fieldCount;
    }

    public void downloadPaymentDueReportExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }
}
