/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Sanira Nanayakkara
 */
@Component
public class VideoConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(VideoConverter.class);

    static public boolean convert(String src_path, String dest_path) {
        try{
            long s_time = System.currentTimeMillis();

            IMediaReader mediaReader = ToolFactory.makeReader(src_path);
            IMediaWriter mediaWriter = ToolFactory.makeWriter(dest_path, mediaReader);
            mediaReader.addListener(mediaWriter);

            while (mediaReader.readPacket() == null);

            long e_time = System.currentTimeMillis();
            LOGGER.debug("Xuggle Video Converter :: Output file : {} | Time to convert : {}",dest_path, (e_time - s_time));
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
