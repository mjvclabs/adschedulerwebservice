/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;

/**
 *
 * @author user
 */
public class OrderChannelInfo {

    @JsonProperty("channelId")
    private int channelId;
    @JsonProperty("workOrderId")
    private int workOrderId;

    @JsonProperty("tvc_numOfSpots")
    private int tvc_numOfSpots = 0;
    @JsonProperty("tvc_bonusSpots")
    private int tvc_bonusSpots = 0;
    @JsonProperty("tvc_package")
    private double tvc_package_value = 0;

    @JsonProperty("logo_numOfSpots")
    private int logo_numOfSpots = 0;
    @JsonProperty("logo_bonusSpots")
    private int logo_bonusSpots = 0;
    @JsonProperty("logo_package")
    private double logo_package_value = 0;

    @JsonProperty("crowler_numOfSpots")
    private int crowler_numOfSpots = 0;
    @JsonProperty("crowler_bonusSpots")
    private int crowler_bonusSpots = 0;
    @JsonProperty("crowler_package")
    private double crowler_package_value = 0;

    @JsonProperty("v_sqeeze_numOfSpots")
    private int v_sqeeze_numOfSpots = 0;
    @JsonProperty("v_sqeeze_bonusSpots")
    private int v_sqeeze_bonusSpots = 0;
    @JsonProperty("v_sqeeze_package")
    private double v_sqeeze_package_value = 0;

    @JsonProperty("l_sqeeze_numOfSpots")
    private int l_sqeeze_numOfSpots = 0;
    @JsonProperty("l_sqeeze_bonusSpots")
    private int l_sqeeze_bonusSpots = 0;
    @JsonProperty("l_sqeeze_package")
    private double l_sqeeze_package_value = 0;

    @JsonProperty("package")
    private double package_value = 0;
    @JsonProperty("ave_package")
    private double ave_package_value = 0;
    
    @JsonProperty("tvc_brkdown")
    private HashMap<String, Integer> tvcSpotCountList = new HashMap<>();
    @JsonProperty("tvc_brkdown_b")
    private HashMap<String, Integer> tvcBonusSpotCountList = new HashMap<>();

    public OrderChannelInfo(int channelId, int workOrderId, int tvc_numOfSpots, int tvc_bonusSpots, int logo_numOfSpots, int logo_bonusSpots, int crowler_numOfSpots, int crowler_bonusSpots, int v_sqeeze_numOfSpots, int v_sqeeze_bonusSpots, int l_sqeeze_numOfSpots, int l_sqeeze_bonusSpots, int package_value, int ave_package_value) {
        this.channelId = channelId;
        this.workOrderId = workOrderId;
        this.tvc_numOfSpots = tvc_numOfSpots;
        this.tvc_bonusSpots = tvc_bonusSpots;
        this.logo_numOfSpots = logo_numOfSpots;
        this.logo_bonusSpots = logo_bonusSpots;
        this.crowler_numOfSpots = crowler_numOfSpots;
        this.crowler_bonusSpots = crowler_bonusSpots;
        this.v_sqeeze_numOfSpots = v_sqeeze_numOfSpots;
        this.v_sqeeze_bonusSpots = v_sqeeze_bonusSpots;
        this.l_sqeeze_numOfSpots = l_sqeeze_numOfSpots;
        this.l_sqeeze_bonusSpots = l_sqeeze_bonusSpots;
        this.package_value = package_value;
        this.ave_package_value = ave_package_value;
    }

    public OrderChannelInfo() {
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public int getTvc_numOfSpots() {
        return tvc_numOfSpots;
    }

    public void setTvc_numOfSpots(int tvc_numOfSpots) {
        this.tvc_numOfSpots = tvc_numOfSpots;
    }

    public int getTvc_bonusSpots() {
        return tvc_bonusSpots;
    }

    public void setTvc_bonusSpots(int tvc_bonusSpots) {
        this.tvc_bonusSpots = tvc_bonusSpots;
    }

    public int getLogo_numOfSpots() {
        return logo_numOfSpots;
    }

    public void setLogo_numOfSpots(int logo_numOfSpots) {
        this.logo_numOfSpots = logo_numOfSpots;
    }

    public int getLogo_bonusSpots() {
        return logo_bonusSpots;
    }

    public void setLogo_bonusSpots(int logo_bonusSpots) {
        this.logo_bonusSpots = logo_bonusSpots;
    }

    public int getCrowler_numOfSpots() {
        return crowler_numOfSpots;
    }

    public void setCrowler_numOfSpots(int crowler_numOfSpots) {
        this.crowler_numOfSpots = crowler_numOfSpots;
    }

    public int getCrowler_bonusSpots() {
        return crowler_bonusSpots;
    }

    public void setCrowler_bonusSpots(int crowler_bonusSpots) {
        this.crowler_bonusSpots = crowler_bonusSpots;
    }

    public int getV_sqeeze_numOfSpots() {
        return v_sqeeze_numOfSpots;
    }

    public void setV_sqeeze_numOfSpots(int v_sqeeze_numOfSpots) {
        this.v_sqeeze_numOfSpots = v_sqeeze_numOfSpots;
    }

    public int getV_sqeeze_bonusSpots() {
        return v_sqeeze_bonusSpots;
    }

    public void setV_sqeeze_bonusSpots(int v_sqeeze_bonusSpots) {
        this.v_sqeeze_bonusSpots = v_sqeeze_bonusSpots;
    }

    public int getL_sqeeze_numOfSpots() {
        return l_sqeeze_numOfSpots;
    }

    public void setL_sqeeze_numOfSpots(int l_sqeeze_numOfSpots) {
        this.l_sqeeze_numOfSpots = l_sqeeze_numOfSpots;
    }

    public int getL_sqeeze_bonusSpots() {
        return l_sqeeze_bonusSpots;
    }

    public void setL_sqeeze_bonusSpots(int l_sqeeze_bonusSpots) {
        this.l_sqeeze_bonusSpots = l_sqeeze_bonusSpots;
    }

    public double getPackage_value() {
        return package_value;
    }

    public void setPackage_value(double package_value) {
        this.package_value = package_value;
    }

    public double getAve_package_value() {
        return ave_package_value;
    }

    public void setAve_package_value(double ave_package_value) {
        this.ave_package_value = ave_package_value;
    }

    public double getTvc_package_value() {
        return tvc_package_value;
    }

    public void setTvc_package_value(double tvc_package_value) {
        this.tvc_package_value = tvc_package_value;
    }

    public double getLogo_package_value() {
        return logo_package_value;
    }

    public void setLogo_package_value(double logo_package_value) {
        this.logo_package_value = logo_package_value;
    }

    public double getCrowler_package_value() {
        return crowler_package_value;
    }

    public void setCrowler_package_value(double crowler_package_value) {
        this.crowler_package_value = crowler_package_value;
    }

    public double getV_sqeeze_package_value() {
        return v_sqeeze_package_value;
    }

    public void setV_sqeeze_package_value(double v_sqeeze_package_value) {
        this.v_sqeeze_package_value = v_sqeeze_package_value;
    }

    public double getL_sqeeze_package_value() {
        return l_sqeeze_package_value;
    }

    public void setL_sqeeze_package_value(double l_sqeeze_package_value) {
        this.l_sqeeze_package_value = l_sqeeze_package_value;
    }

    public void setAverage() {
//        if (this.package_value != 0) {
//            this.ave_package_value = this.package_value / (this.tvc_numOfSpots );//+ this.tvc_bonusSpots + this.logo_numOfSpots + this.logo_bonusSpots + this.crowler_numOfSpots + this.crowler_bonusSpots + this.v_sqeeze_numOfSpots + this.v_sqeeze_bonusSpots + this.l_sqeeze_numOfSpots + this.l_sqeeze_bonusSpots
//        }
    }    

    public HashMap<String, Integer> getTvcSpotCountList() {
        return tvcSpotCountList;
    }

    public void setTvcSpotCountList(HashMap<String, Integer> tvcSpotCountList) {
        this.tvcSpotCountList = tvcSpotCountList;
    }

    public HashMap<String, Integer> getTvcBonusSpotCountList() {
        return tvcBonusSpotCountList;
    }

    public void setTvcBonusSpotCountList(HashMap<String, Integer> tvcBonusSpotCountList) {
        this.tvcBonusSpotCountList = tvcBonusSpotCountList;
    }
}
