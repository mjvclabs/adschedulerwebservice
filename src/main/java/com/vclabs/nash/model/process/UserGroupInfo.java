/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.process;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author user
 */
public class UserGroupInfo {
    
    @JsonProperty("groupname")
    private String groupname;
    
    @JsonProperty("pagename")
    private String pagename;
    
    @JsonProperty("pageId")
    private int pageId;
    
    @JsonProperty("readAcc")
    private int readAcc;
    
    @JsonProperty("writeAcc")
    private int writeAcc;

    public UserGroupInfo() {
    }

    public UserGroupInfo(String groupname, String pagename, int pageId, int readAcc, int writeAcc) {
        this.groupname = groupname;
        this.pagename = pagename;
        this.pageId = pageId;
        this.readAcc = readAcc;
        this.writeAcc = writeAcc;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getPagename() {
        return pagename;
    }

    public void setPagename(String pagename) {
        this.pagename = pagename;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public int getReadAcc() {
        return readAcc;
    }

    public void setReadAcc(int readAcc) {
        this.readAcc = readAcc;
    }

    public int getWriteAcc() {
        return writeAcc;
    }

    public void setWriteAcc(int writeAcc) {
        this.writeAcc = writeAcc;
    }
    
}
