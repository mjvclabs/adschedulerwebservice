/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hp
 */
public class MissedSpotCountInfo {

    private String startDate;
    private String endDate;
    private String woIds;
    private String channelIds;
    private String advertisement;
    private String advertType;
    private String spotStatus;
    private String advertTimeBelt;

    private List<Integer> channelList = new ArrayList<>();
    private List<Integer> woIdList = new ArrayList<>();

    public MissedSpotCountInfo() {
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getWoIds() {
        return woIds;
    }

    public void setWoIds(String woIds) {
        this.woIds = woIds;
    }

    public String getChannelIds() {
        return channelIds;
    }

    public void setChannelIds(String channelIds) {
        this.channelIds = channelIds;
    }

    public String getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(String advertisement) {
        this.advertisement = advertisement;
    }

    public String getAdvertType() {
        return advertType;
    }

    public void setAdvertType(String advertType) {
        this.advertType = advertType;
    }

    public String getSpotStatus() {
        return spotStatus;
    }

    public void setSpotStatus(String spotStatus) {
        this.spotStatus = spotStatus;
    }

    public String getAdvertTimeBelt() {
        return advertTimeBelt;
    }

    public void setAdvertTimeBelt(String advertTimeBelt) {
        this.advertTimeBelt = advertTimeBelt;
    }

    public List<Integer> getChannelList() {

        String[] channelListString = this.channelIds.split("_");

        if (channelListString.length != 0) {
            for (String id : channelListString) {
                if (!id.equals("") && !id.equals("All")) {
                    channelList.add(Integer.parseInt(id));
                }
            }
        }
        return channelList;
    }

    public void setChannelList(List<Integer> channelList) {
        this.channelList = channelList;
    }

    public List<Integer> getWoIdList() {
        String[] woListString = this.woIds.split("_");

        if (woListString.length != 0) {
            for (String id : woListString) {
                if (!id.equals("") && !id.equals("All")) {
                    woIdList.add(Integer.parseInt(id));
                }
            }
        }
        return woIdList;
    }

    public void setWoIdList(List<Integer> woIdList) {
        this.woIdList = woIdList;
    }

}
