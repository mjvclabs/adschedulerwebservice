/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.process.excel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vclabs.nash.model.view.reporting.ComAvailabilityChannel;
import com.vclabs.nash.model.view.reporting.ComAvailabilityHourDetails;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;

/**
 *
 * @author hp
 */
@Component
public class ScheduleAnalysisExcel extends Common {
    
    private final String WEBDIRECTORY = "ScheduleAnalysis";
    private final String FILEPATH = "ScheduleAnalysis.xls";
    
    private CellStyle cellStyle;
    
    private CellStyle tableHeaderStyle;
    
    public void generateScheduleAnalysisExcel(int value, HttpServletRequest request, Map<String, Map<String, ComAvailabilityChannel>> channelSpotList) throws Exception {
        /*create work book*/
        HSSFWorkbook workbook = new HSSFWorkbook();
        
        cellStyle = setDataRowCellStyle(workbook);
        tableHeaderStyle = tableHeaderStyle(workbook);
        /*create new sheel*/
        HSSFSheet sheet = workbook.createSheet("ScheduleAnalysis");
        
        int rowCount = 0;

        /*Create row*/
        for (Map.Entry<String, Map<String, ComAvailabilityChannel>> comAvailabilityChannelList : channelSpotList.entrySet()) {
            
            HSSFRow channelRow = sheet.createRow((short) rowCount);
            rowCount++;
            channelRow.createCell(0).setCellValue("Channel:");
            channelRow.createCell(1).setCellValue(comAvailabilityChannelList.getValue().entrySet().iterator().next().getValue().getChannelName());
            
            HSSFRow rowhead = sheet.createRow((short) rowCount);
            rowCount++;
            /*Set table header fields*/
            int count = createExcelHeaderFields(rowhead, comAvailabilityChannelList.getValue());
            
            ArrayList timeMap = getTimeBelt(comAvailabilityChannelList.getValue());
            for (int i = 0; i < timeMap.size(); i++) {
                Cell cell;
                HSSFRow row = sheet.createRow((short) rowCount);
                cell = row.createCell(0);
                cell.setCellValue((String) timeMap.get(i));
                cell.setCellStyle(cellStyle);
                
                int colCount = 1;
                for (Map.Entry<String, ComAvailabilityChannel> entry : comAvailabilityChannelList.getValue().entrySet()) {
                    HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = entry.getValue().getHourDetailsMap();
                    String time = (String) timeMap.get(i);
                    int hour = Integer.parseInt(time.split(":")[0]);
                    ComAvailabilityHourDetails comAvailabilityHourDetails = hourDetailsMap.get(hour);
                    switch (value) {
                        case 1:
                            cell = row.createCell(colCount);
                            cell.setCellValue(comAvailabilityHourDetails.getAdvertCount());
                            cell.setCellStyle(cellStyle);
                            break;
                        case 2:
                            cell = row.createCell(colCount);
                            cell.setCellValue(comAvailabilityHourDetails.getAdvertDuration());
                            cell.setCellStyle(cellStyle);
                            break;
                        case 3:
                            cell = row.createCell(colCount);
                            cell.setCellValue(comAvailabilityHourDetails.getInventryUtilization());
                            cell.setCellStyle(cellStyle);
                            break;
                        case 4:
                            cell = row.createCell(colCount);
                            cell.setCellValue(comAvailabilityHourDetails.getActualUtilization());
                            cell.setCellStyle(cellStyle);
                            break;
                        default:
                            cell = row.createCell(colCount);
                            cell.setCellValue(comAvailabilityHourDetails.getAdvertCount());
                            cell.setCellStyle(cellStyle);
                            break;
                    }
                    
                    colCount++;
                }
                rowCount++;
            }
            aligncolumns(sheet, count);
            rowCount++;
        }
        writeFile(workbook, getFilePath(request, WEBDIRECTORY, FILEPATH, true));
    }
    
    public int createExcelHeaderFields(HSSFRow rowhead, Map<String, ComAvailabilityChannel> comAvailabilityChannelList) {
        
        Cell cell;
        cell = rowhead.createCell(0);
        cell.setCellValue("Time");
        cell.setCellStyle(tableHeaderStyle);
        
        int count = 1;
        if (!comAvailabilityChannelList.isEmpty()) {
            
            for (Map.Entry<String, ComAvailabilityChannel> entry : comAvailabilityChannelList.entrySet()) {
                cell = rowhead.createCell(count);
                cell.setCellValue(entry.getValue().getDate().toString());
                cell.setCellStyle(tableHeaderStyle);
                count++;
            }
            
        }
        
        return count;
    }
    
    public ArrayList getTimeBelt(Map<String, ComAvailabilityChannel> comAvailabilityChannelList) {
        ArrayList timeMap = new ArrayList();
        int count = 1;
        if (!comAvailabilityChannelList.isEmpty()) {
            HashMap<Integer, ComAvailabilityHourDetails> hourDetailsMap = comAvailabilityChannelList.entrySet().iterator().next().getValue().getHourDetailsMap();
            for (Map.Entry<Integer, ComAvailabilityHourDetails> entry : hourDetailsMap.entrySet()) {
                timeMap.add(entry.getValue().getTime());
            }
            
        }
        
        return timeMap;
    }
    
    public void downloadScheduleAnalysisExcell(HttpServletResponse response, HttpServletRequest request) {
        downloadFile(response, getFilePath(request, WEBDIRECTORY, FILEPATH, false));
    }
    
}
