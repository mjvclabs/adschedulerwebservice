package com.vclabs.nash.model.view;

import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.process.ReplaceAdvertData;

import java.util.Date;
import java.util.List;

/**
 * @author Sanira Nanayakkara
 */
public class WorkOrderScheduleView {

    private WorkOrderChannelScheduleView woChannelSpotsCount;
    private List<ScheduleUpdateView> timebeltSchedules;
    private List<TimeBeltUtilization> woTimeBeltUtilization;
    private String message = "";
    private int workOrderId=0;

    private List<ScheduleDef> scheduleTempList; // used by updateWOSchedule method of scheduleService
    private List<ScheduleDef> currentScheduleList; // used by updateWOSchedule method of scheduleService
    private List<ScheduleDef> removeScheduleList; // used by updateWOSchedule method of scheduleService

    private List<Integer> lstChannelIds;  // used by advertisementsReplace of scheduleService
    private Date dtStartDate;
    private Date dtEndDate;
    private Date dtStartTime;

    private Message messageDto;

    private Boolean rescheduleStatus = false;

    public WorkOrderChannelScheduleView getWoChannelSpotsCount() {
        return woChannelSpotsCount;
    }

    public void setWoChannelSpotsCount(WorkOrderChannelScheduleView woChannelSpotsCount) {
        this.woChannelSpotsCount = woChannelSpotsCount;
    }

    public List<ScheduleUpdateView> getTimebeltSchedules() {
        return timebeltSchedules;
    }

    public void setTimebeltSchedules(List<ScheduleUpdateView> timebeltSchedules) {
        this.timebeltSchedules = timebeltSchedules;
    }

    public List<TimeBeltUtilization> getWoTimeBeltUtilization() {
        return woTimeBeltUtilization;
    }

    public void setWoTimeBeltUtilization(List<TimeBeltUtilization> woTimeBeltUtilization) {
        this.woTimeBeltUtilization = woTimeBeltUtilization;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
     public void addMessage(String message) {
         if (this.message.equals("")) {
             this.message = message;
         }else{
             this.message += " : " + message;
         }        
    }

    public List<ScheduleDef> getScheduleTempList() {
        return scheduleTempList;
    }

    public void setScheduleTempList(List<ScheduleDef> scheduleTempList) {
        this.scheduleTempList = scheduleTempList;
    }

    public List<ScheduleDef> getCurrentScheduleList() {
        return currentScheduleList;
    }

    public void setCurrentScheduleList(List<ScheduleDef> currentScheduleList) {
        this.currentScheduleList = currentScheduleList;
    }

    public List<ScheduleDef> getRemoveScheduleList() {
        return removeScheduleList;
    }

    public void setRemoveScheduleList(List<ScheduleDef> removeScheduleList) {
        this.removeScheduleList = removeScheduleList;
    }

    public Date getDtStartDate() {
        return dtStartDate;
    }

    public void setDtStartDate(Date dtStartDate) {
        this.dtStartDate = dtStartDate;
    }

    public Date getDtEndDate() {
        return dtEndDate;
    }

    public void setDtEndDate(Date dtEndDate) {
        this.dtEndDate = dtEndDate;
    }

    public Date getDtStartTime() {
        return dtStartTime;
    }

    public void setDtStartTime(Date dtStartTime) {
        this.dtStartTime = dtStartTime;
    }

    public List<Integer> getLstChannelIds() {
        return lstChannelIds;
    }

    public void setLstChannelIds(List<Integer> lstChannelIds) {
        this.lstChannelIds = lstChannelIds;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public Message getMessageDto() {
        return messageDto;
    }

    public void setMessageDto(Message messageDto) {
        this.messageDto = messageDto;
    }

    public Boolean getRescheduleStatus() {
        return rescheduleStatus;
    }

    public void setRescheduleStatus(Boolean rescheduleStatus) {
        this.rescheduleStatus = rescheduleStatus;
    }
}
