/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.view;

/**
 *
 * @author Nalaka Dissanayake
 */
public class BillingView {

    private int channelId;
    private String channelName;

    private int tvcSpots = 0;
    private int tvcAir = 0;
    private double tvcPackageAmount = 0.0;
    private int tvcDuration = 0;
    private double tvcPackageAvarage = 0;
    private double tvcAirPackageAmount = 0;

    private int logoSpots = 0;
    private int logoAir = 0;
    private double logoSpotsPackageAmount = 0;
    private double logoSpotsPackageAvarage = 0;
    private double logoAirPackageAmount = 0;

    private int crowlerSpots = 0;
    private int crowlerAir = 0;
    private double crowlerPackageAmount = 0;
    private double crowlerPackageAvarage = 0;
    private double crowlerAirPackageAmount = 0;

    private int v_sqeezeSpots = 0;
    private int v_sqeeze_Air = 0;
    private double v_sqeezePackageAmount = 0;
    private double v_sqeezePackageAvarage = 0;
    private double v_sqeezeAirPackageAmount = 0;

    private int l_sqeezeSpots = 0;
    private int l_sqeeze_Air = 0;
    private double l_sqeezePackageAmount = 0;
    private double l_sqeezePackageAvarage = 0;
    private double l_sqeezeAirPackageAmount = 0;

    private double totalAmount;

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getTvcSpots() {
        return tvcSpots;
    }

    public void setTvcSpots(int tvcSpots) {
        this.tvcSpots = tvcSpots;
    }

    public int getTvcAir() {
        return tvcAir;
    }

    public void setTvcAir(int tvcAir) {
        this.tvcAir = tvcAir;
    }

    public int getTvcDuration() {
        return tvcDuration;
    }

    public void setTvcDuration(int tvcDuration) {
        this.tvcDuration = tvcDuration;
    }

    public int getLogoSpots() {
        return logoSpots;
    }

    public void setLogoSpots(int logoSpots) {
        this.logoSpots = logoSpots;
    }

    public int getLogoAir() {
        return logoAir;
    }

    public void setLogoAir(int logoAir) {
        this.logoAir = logoAir;
    }

    public int getCrowlerSpots() {
        return crowlerSpots;
    }

    public void setCrowlerSpots(int crowlerSpots) {
        this.crowlerSpots = crowlerSpots;
    }

    public int getCrowlerAir() {
        return crowlerAir;
    }

    public void setCrowlerAir(int crowlerAir) {
        this.crowlerAir = crowlerAir;
    }

    public int getV_sqeezeSpots() {
        return v_sqeezeSpots;
    }

    public void setV_sqeezeSpots(int v_sqeezeSpots) {
        this.v_sqeezeSpots = v_sqeezeSpots;
    }

    public int getV_sqeeze_Air() {
        return v_sqeeze_Air;
    }

    public void setV_sqeeze_Air(int v_sqeeze_Air) {
        this.v_sqeeze_Air = v_sqeeze_Air;
    }

    public int getL_sqeezeSpots() {
        return l_sqeezeSpots;
    }

    public void setL_sqeezeSpots(int l_sqeezeSpots) {
        this.l_sqeezeSpots = l_sqeezeSpots;
    }

    public int getL_sqeeze_Air() {
        return l_sqeeze_Air;
    }

    public void setL_sqeeze_Air(int l_sqeeze_Air) {
        this.l_sqeeze_Air = l_sqeeze_Air;
    }

    public double getTvcPackageAmount() {
        return tvcPackageAmount;
    }

    public void setTvcPackageAmount(double tvcPackageAmount) {
        this.tvcPackageAmount = tvcPackageAmount;
    }

    public double getTvcPackageAvarage() {
        return tvcPackageAvarage;
    }

    public void setTvcPackageAvarage() {

        if (tvcSpots != 0) {
            this.tvcPackageAvarage = tvcPackageAmount / tvcSpots;
        }
    }

    public double getLogoSpotsPackageAmount() {
        return logoSpotsPackageAmount;
    }

    public void setLogoSpotsPackageAmount(double logoSpotsPackageAmount) {
        this.logoSpotsPackageAmount = logoSpotsPackageAmount;
    }

    public double getLogoSpotsPackageAvarage() {
        return logoSpotsPackageAvarage;
    }

    public void setLogoSpotsPackageAvarage() {
        if (logoSpots != 0) {
            this.logoSpotsPackageAvarage = logoSpotsPackageAmount / logoSpots;
        }
    }

    public double getCrowlerPackageAmount() {
        return crowlerPackageAmount;
    }

    public void setCrowlerPackageAmount(double crowlerPackageAmount) {
        this.crowlerPackageAmount = crowlerPackageAmount;
    }

    public double getCrowlerPackageAvarage() {
        return crowlerPackageAvarage;
    }

    public void setCrowlerPackageAvarage() {
        if (crowlerSpots != 0) {
            this.crowlerPackageAvarage = crowlerPackageAmount / crowlerSpots;
        }
    }

    public double getV_sqeezePackageAmount() {
        return v_sqeezePackageAmount;
    }

    public void setV_sqeezePackageAmount(double v_sqeezePackageAmount) {
        this.v_sqeezePackageAmount = v_sqeezePackageAmount;
    }

    public double getV_sqeezePackageAvarage() {
        return v_sqeezePackageAvarage;
    }

    public void setV_sqeezePackageAvarage() {
        if (v_sqeezeSpots != 0) {
            this.v_sqeezePackageAvarage = v_sqeezePackageAmount / v_sqeezeSpots;
        }
    }

    public double getL_sqeezePackageAmount() {
        return l_sqeezePackageAmount;
    }

    public void setL_sqeezePackageAmount(double l_sqeezePackageAmount) {
        this.l_sqeezePackageAmount = l_sqeezePackageAmount;
    }

    public double getL_sqeezePackageAvarage() {
        return l_sqeezePackageAvarage;
    }

    public void setL_sqeezePackageAvarage() {
        if (l_sqeezeSpots != 0) {
            this.l_sqeezePackageAvarage = l_sqeezePackageAmount / l_sqeezeSpots;
        }
    }

    public void addTVC(int duration) {
        this.tvcDuration += duration;
        tvcAir = tvcDuration / 30;
        if ((tvcDuration % 30) != 0) {
            tvcAir++;
        }
        if (tvcAir > tvcSpots) {
            this.tvcAir = this.tvcSpots;
        }
    }

    public void addLOGO() {
        if (logoAir < logoSpots) {
            this.logoAir++;
        }
    }

    public void addCROWLER() {
        if (crowlerAir < crowlerSpots) {
            this.crowlerAir++;
        }
    }

    public void addV_SQEEZE() {
        if (v_sqeeze_Air < v_sqeezeSpots) {
            this.v_sqeeze_Air++;
        }
    }

    public void addL_SQEEZE() {
        if (l_sqeeze_Air < l_sqeezeSpots) {
            this.l_sqeeze_Air++;
        }
    }

    public double getTvcTotalAmount() {
        return this.tvcAir * this.tvcPackageAvarage;
    }

    public double getLogoTotalAmount() {
        return this.logoAir * this.logoSpotsPackageAvarage;
    }

    public double getCrowlerTotalAmount() {
        return this.crowlerAir * this.crowlerPackageAvarage;
    }

    public double getL_SqeezeTotalAmount() {
        return this.l_sqeeze_Air * this.l_sqeezePackageAvarage;
    }

    public double getV_SqeezeTotalAmount() {
        return this.v_sqeeze_Air * this.v_sqeezePackageAvarage;
    }

    public double getTvcAirPackageAmount() {
        return tvcAirPackageAmount;
    }

    public void setTvcAirPackageAmount() {
        this.tvcAirPackageAmount = getTvcTotalAmount();
    }

    public double getLogoAirPackageAmount() {
        return logoAirPackageAmount;
    }

    public void setLogoAirPackageAmount() {
        this.logoAirPackageAmount = getLogoTotalAmount();
    }

    public double getCrowlerAirPackageAmount() {
        return crowlerAirPackageAmount;
    }

    public void setCrowlerAirPackageAmount() {
        this.crowlerAirPackageAmount = getCrowlerTotalAmount();
    }

    public double getV_sqeezeAirPackageAmount() {
        return v_sqeezeAirPackageAmount;
    }

    public void setV_sqeezeAirPackageAmount() {
        this.v_sqeezeAirPackageAmount = getV_SqeezeTotalAmount();
    }

    public double getL_sqeezeAirPackageAmount() {
        return l_sqeezeAirPackageAmount;
    }

    public void setL_sqeezeAirPackageAmount() {
        this.l_sqeezeAirPackageAmount = getL_SqeezeTotalAmount();
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount() {
        this.totalAmount = getTvcTotalAmount();//+getLogoTotalAmount()+getCrowlerTotalAmount()+getL_SqeezeTotalAmount()+getV_SqeezeTotalAmount();
    }

}
