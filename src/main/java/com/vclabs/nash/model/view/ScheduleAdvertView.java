/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.view;

/**
 *
 * @author user
 */
public class ScheduleAdvertView implements Comparable<ScheduleAdvertView> {

    private String workOrderId;
    private String channelName;
    private String advertname;
    private String advertType;
    private int duration;
    private int priority;
    private String status;
    private String startAndEndTime;
    private String initialStartEndTime;
    private int clusterNum = 0;
    private int playOrder = -1;
    private int isFileAvailable = 0;
    private String client;
    private String palyTime = "";

    public ScheduleAdvertView() {
    }

    public ScheduleAdvertView(String workOrderId, String channelName, String advertname, int duration, int priority, String status) {
        this.workOrderId = workOrderId;
        this.channelName = channelName;
        this.advertname = advertname;
        this.duration = duration;
        this.priority = priority;
        this.status = status;
    }

    public ScheduleAdvertView(String workOrderId, String channelName, String advertname, int duration, String type, int priority, String status) {
        this.workOrderId = workOrderId;
        this.channelName = channelName;
        this.advertname = advertname;
        this.duration = duration;
        this.priority = priority;
        this.status = status;
        this.advertType = type;
    }

    public ScheduleAdvertView(String workOrderId, String channelName, String advertname, String advertType, int duration, int priority, String status, String startAndEndTime) {
        this.workOrderId = workOrderId;
        this.channelName = channelName;
        this.advertname = advertname;
        this.advertType = advertType;
        this.duration = duration;
        this.priority = priority;
        this.status = status;
        this.startAndEndTime = startAndEndTime;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getAdvertname() {
        return advertname;
    }

    public void setAdvertname(String advertname) {
        this.advertname = advertname;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdvertType() {
        return advertType;
    }

    public void setAdvertType(String advertType) {
        this.advertType = advertType;
    }

    public String getStartAndEndTime() {
        return startAndEndTime;
    }

    public void setStartAndEndTime(String startAndEndTime) {
        this.startAndEndTime = startAndEndTime;
    }

    public int getClusterNum() {
        return clusterNum;
    }

    public void setClusterNum(int clusterNum) {
        this.clusterNum = clusterNum;
    }

    public int getIsFileAvailable() {
        return isFileAvailable;
    }

    public void setIsFileAvailable(Boolean isFileAvailable) {
        if (isFileAvailable) {
            this.isFileAvailable = 1;
        }
    }

    public String getInitialStartEndTime() {
        return initialStartEndTime;
    }

    public void setInitialStartEndTime(String initialStartEndTime) {
        this.initialStartEndTime = initialStartEndTime;
    }

    public int getPlayOrder() {
        return playOrder;
    }

    public void setPlayOrder(int playOrder) {
        this.playOrder = playOrder;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getPalyTime() {
        return palyTime;
    }

    public void setPalyTime(String palyTime) {
        this.palyTime = palyTime;
    }

    @Override
    public int compareTo(ScheduleAdvertView o) {
        if (this.clusterNum == o.clusterNum) {
            return this.playOrder - o.playOrder;
        } else {
            return this.clusterNum - o.clusterNum;
        }
    }

}
