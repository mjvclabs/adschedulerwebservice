/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view;

/**
 *
 * @author user
 */
public class DashboadChannelView {

    private int channelID;
    private String ChannelName;
    private Boolean isManual = true;
    private Boolean updated = true;
    private Boolean runServer = true;
    private Boolean newlyAdded = false;
    private String logoPath = "set_path";
    private String addedtime = "00.00.00";
    private String updateedtime = "00.00.00";

    public DashboadChannelView() {
    }

    public int getChannelID() {
        return channelID;
    }

    public void setChannelID(int channelID) {
        this.channelID = channelID;
    }

    public String getChannelName() {
        return ChannelName;
    }

    public void setChannelName(String ChannelName) {
        this.ChannelName = ChannelName;
    }

    public Boolean getIsManual() {
        return isManual;
    }

    public void setIsManual(Boolean isManual) {
        this.isManual = isManual;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getRunServer() {
        return runServer;
    }

    public void setRunServer(Boolean runServer) {
        this.runServer = runServer;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public Boolean getNewlyAdded() {
        return newlyAdded;
    }

    public void setNewlyAdded(Boolean newlyAdded) {
        this.newlyAdded = newlyAdded;
    }

    public String getUpdateedtime() {
        return updateedtime;
    }

    public void setUpdateedtime(String updateedtime) {
        this.updateedtime = updateedtime;
    }

    public String getAddedtime() {
        return addedtime;
    }

    public void setAddedtime(String addedtime) {
        this.addedtime = addedtime;
    }

}
