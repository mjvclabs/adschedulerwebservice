/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author Sanira Nanayakkara
 */
public class OnedayScheduleView implements Comparable<OnedayScheduleView> {

    private int timeBeltId;
    @JsonSerialize(using = CustomTimeSerializer.class)
    private Date scheduleStartTime;
    @JsonSerialize(using = CustomTimeSerializer.class)
    private Date scheduleEndTime;
    private List<ScheduleAdvertView> scheduleItems = new ArrayList<>();
    private int channelId;

    public OnedayScheduleView() {
    }

    public OnedayScheduleView(int timeBeltId, Date schedulestarttime, Date scheduleendtime) {
        this.timeBeltId = timeBeltId;
        this.scheduleStartTime = schedulestarttime;
        this.scheduleEndTime = scheduleendtime;
    }

    public int getTimeBeltId() {
        return timeBeltId;
    }

    public void setTimeBeltId(int timeBeltId) {
        this.timeBeltId = timeBeltId;
    }

    public Date getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(Date scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public Date getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(Date scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public List<ScheduleAdvertView> getScheduleItems() {
        return scheduleItems;
    }

    public void setScheduleItems(List<ScheduleAdvertView> scheduleItems) {
        this.scheduleItems = scheduleItems;
    }

    public void addScheduleItems(ScheduleAdvertView scheduleItem) {
        this.scheduleItems.add(scheduleItem);
    }

    @Override
    public int compareTo(OnedayScheduleView o) {
        return this.timeBeltId - o.timeBeltId;
    }
    
    public void orderScheduleItems(){
        Collections.sort(scheduleItems);
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}
