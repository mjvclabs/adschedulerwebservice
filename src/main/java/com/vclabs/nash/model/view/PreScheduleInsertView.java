package com.vclabs.nash.model.view;

/**
 * @author Sanira Nanayakkara
 */
public class PreScheduleInsertView {

    private int channelId;
    private String channelName;
    private int advertDuration;
    private String advertName;
    private int initialSpotCount;
    private int availableSpotCount;
    private long initialScheduleTime;
    private long availableScheduleTime;
    private int newSlotCount;
    private int validSlotCount;

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getAdvertDuration() {
        return advertDuration;
    }

    public void setAdvertDuration(int advertDuration) {
        this.advertDuration = advertDuration;
    }

    public int getInitialSpotCount() {
        return initialSpotCount;
    }

    public void setInitialSpotCount(int initialSpotCount) {
        this.initialSpotCount = initialSpotCount;
    }

    public int getAvailableSpotCount() {
        return availableSpotCount;
    }

    public void setAvailableSpotCount(int availableSpotCount) {
        this.availableSpotCount = availableSpotCount;
    }

    public long getInitialScheduleTime() {
        return initialScheduleTime;
    }

    public void setInitialScheduleTime(long initialScheduleTime) {
        this.initialScheduleTime = initialScheduleTime;
    }

    public long getAvailableScheduleTime() {
        return availableScheduleTime;
    }

    public void setAvailableScheduleTime(long availableScheduleTime) {
        this.availableScheduleTime = availableScheduleTime;
    }

    public int getNewSlotCount() {
        return newSlotCount;
    }

    public void setNewSlotCount(int newSlotCount) {
        this.newSlotCount = newSlotCount;
    }

    public int getValidSlotCount() {
        return validSlotCount;
    }

    public void setValidSlotCount(int validSlotCount) {
        this.validSlotCount = validSlotCount;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }
}
