/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import com.vclabs.nash.model.view.CustomDateSerializer;

/**
 *
 * @author Nalaka
 * @since 01/03/2018
 */
public class PaymentDue {

    private int workOrderId;
    private String agencyName;
    private String clientName;
    private String workOrderType;
    private String scheduleref;
    private String marketingExecutive;
    private double packegeAmount;
    private String workOrderStatus;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date invoiceDate;
    private int invoiceNo;
    private String invoiceAmount = "";

    public PaymentDue() {
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getWorkOrderType() {
        return workOrderType;
    }

    public void setWorkOrderType(String workOrderType) {
        this.workOrderType = workOrderType;
    }

    public String getScheduleref() {
        return scheduleref;
    }

    public void setScheduleref(String scheduleref) {
        this.scheduleref = scheduleref;
    }

    public String getMarketingExecutive() {
        return marketingExecutive;
    }

    public void setMarketingExecutive(String marketingExecutive) {
        this.marketingExecutive = marketingExecutive;
    }

    public double getPackegeAmount() {
        return packegeAmount;
    }

    public void setPackegeAmount(double packegeAmount) {
        this.packegeAmount = packegeAmount;
    }

    public String getWorkOrderStatus() {
        return workOrderStatus;
    }

    public void setWorkOrderStatus(String workOrderStatus) {
        this.workOrderStatus = workOrderStatus;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public int getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(int invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

}
