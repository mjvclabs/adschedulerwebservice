package com.vclabs.nash.model.view;

import com.vclabs.nash.service.DateFormat;

import java.util.Date;

/**
 * Created by Nalaka on 2018-11-01.
 */
public class GeneralAuditView extends DateFormat {

    private String logTime;
    private String field;
    private String oldData;
    private String newData;
    private String user;

    public GeneralAuditView() {
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = getDataYYYMMDD_dash_HHmmss_colan(logTime);
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOldData() {
        return oldData;
    }

    public void setOldData(String oldData) {
        this.oldData = oldData;
    }

    public String getNewData() {
        return newData;
    }

    public void setNewData(String newData) {
        this.newData = newData;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
