package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Nalaka on 2019-05-29.
 */
public class ZeroAndFillerPlayListBackendView {
    private int playlistid;
    private int scheduleHour;
    @JsonSerialize(using = CustomFullDateTimeSerializer.class)
    private Date scheduleStartTime;
    @JsonSerialize(using = CustomFullDateTimeSerializer.class)
    private Date scheduleEndTime;
    @JsonSerialize(using = CustomFullDateTimeSerializer.class)
    private Date timeBeltStartTime;
    @JsonSerialize(using = CustomFullDateTimeSerializer.class)
    private Date timeBeltEndTime;
    @JsonSerialize(using = CustomFullDateTimeSerializer.class)
    private Date actualStartTime;
    @JsonSerialize(using = CustomFullDateTimeSerializer.class)
    private Date actualEndTime;
    private int playCluster;
    private int playOrder;
    private int clusterPriority;
    private String status;
    private int retryCount;
    private int advertId;
    private String advertName;
    private String advertType;
    private String advertPath;
    private String advertCommercialCategory;
    private int advertDuration;
    private int advertXPosition = 0;
    private int advertYPosition = 0;
    private int advertWidth = 720;
    private int advertHeight = 569;
    private double advertOpacity = 1.0;
    private int logoContainerId;
    private ArrayList<Integer> conflictingSchedules = new ArrayList<>();

    public int getPlaylistid() {
        return playlistid;
    }

    public void setPlaylistid(int playlistid) {
        this.playlistid = playlistid;
    }

    public int getScheduleHour() {
        return scheduleHour;
    }

    public void setScheduleHour(int scheduleHour) {
        this.scheduleHour = scheduleHour;
    }

    public Date getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(Date scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public Date getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(Date scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public Date getActualStartTime() {
        return actualStartTime;
    }

    public void setActualStartTime(Date actualStartTime) {
        this.actualStartTime = actualStartTime;
    }

    public Date getActualEndTime() {
        return actualEndTime;
    }

    public void setActualEndTime(Date actualEndTime) {
        this.actualEndTime = actualEndTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public String getAdvertType() {
        return advertType;
    }

    public void setAdvertType(String advertType) {
        this.advertType = advertType;
    }

    public String getAdvertPath() {
        return advertPath;
    }

    public void setAdvertPath(String advertPath) {
        this.advertPath = advertPath;
    }

    public String getAdvertCommercialCategory() {
        return advertCommercialCategory;
    }

    public void setAdvertCommercialCategory(String advertCommercialCategory) {
        this.advertCommercialCategory = advertCommercialCategory;
    }

    public int getAdvertDuration() {
        return advertDuration;
    }

    public void setAdvertDuration(int advertDuration) {
        this.advertDuration = advertDuration;
    }

    public int getAdvertXPosition() {
        return advertXPosition;
    }

    public void setAdvertXPosition(int advertXPosition) {
        this.advertXPosition = advertXPosition;
    }

    public int getAdvertYPosition() {
        return advertYPosition;
    }

    public void setAdvertYPosition(int advertYPosition) {
        this.advertYPosition = advertYPosition;
    }

    public int getAdvertWidth() {
        return advertWidth;
    }

    public void setAdvertWidth(int advertWidth) {
        this.advertWidth = advertWidth;
    }

    public int getAdvertHeight() {
        return advertHeight;
    }

    public void setAdvertHeight(int advertHeight) {
        this.advertHeight = advertHeight;
    }

    public double getAdvertOpacity() {
        return advertOpacity;
    }

    public void setAdvertOpacity(double advertOpacity) {
        this.advertOpacity = advertOpacity;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public int getLogoContainerId() {
        return logoContainerId;
    }

    public void setLogoContainerId(int logoContainerId) {
        this.logoContainerId = logoContainerId;
    }

    public Date getTimeBeltStartTime() {
        return timeBeltStartTime;
    }

    public void setTimeBeltStartTime(Date timeBeltStartTime) {
        this.timeBeltStartTime = timeBeltStartTime;
    }

    public Date getTimeBeltEndTime() {
        return timeBeltEndTime;
    }

    public void setTimeBeltEndTime(Date timeBeltEndTime) {
        this.timeBeltEndTime = timeBeltEndTime;
    }

    public int getPlayCluster() {
        return playCluster;
    }

    public void setPlayCluster(int playCluster) {
        this.playCluster = playCluster;
    }

    public int getPlayOrder() {
        return playOrder;
    }

    public void setPlayOrder(int playOrder) {
        this.playOrder = playOrder;
    }

    public ArrayList<Integer> getConflictingSchedules() {
        return conflictingSchedules;
    }

    public void setConflictingSchedules(ArrayList<Integer> conflictingSchedules) {
        this.conflictingSchedules = conflictingSchedules;
    }

    public int getClusterPriority() {
        return clusterPriority;
    }

    public void setClusterPriority(int clusterPriority) {
        this.clusterPriority = clusterPriority;
    }
}
