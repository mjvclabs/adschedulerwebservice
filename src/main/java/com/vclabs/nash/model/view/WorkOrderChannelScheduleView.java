/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import java.util.HashMap;

/**
 *
 * @author user
 */
public class WorkOrderChannelScheduleView {

    private int channelId = 0;
    private String channelName = "";

    private int _tvcfspot = 0;
    private int _availableTvcSpots = 0;
    private int _logoSpots = 0;
    private int _availableLogoSpots = 0;
    private int _crowlerSpots = 0;
    private int _availableCrowlerSpots = 0;
    private int _v_sqeezeSpots = 0;
    private int _availableV_SqeezeSpots = 0;
    private int _l_sqeezeSpots = 0;
    private int _availableL_SqeezeSpots = 0;

    private int numbertOfAirSpot = 0;
    private int airLogoSpots = 0;
    private int airCrowlerSpots = 0;
    private int airV_sqeezeSpots = 0;
    private int airL_sqeezeSpots = 0;
    private HashMap<Integer, Integer> tvcBreackdownMap = new HashMap<>();
    private HashMap<Integer, Integer> tvcBreackdownMapAvailable = new HashMap<>();

    private long totalTVCDuration = 0;
    private long availableTVCDuration = 0;

    public WorkOrderChannelScheduleView() {
    }

    public int getTvcfspot() {
        return _tvcfspot;
    }

    public void setTvcfspot(int _tvcfspot) {
        this._tvcfspot += _tvcfspot;
        this._availableTvcSpots = this._tvcfspot;
    }

    public int getAvailableTvcSpots() {
        return _availableTvcSpots;
    }

    public void decrAvailableTvcSpots(int _availableTvcSpots) {
        this._availableTvcSpots -= _availableTvcSpots;
    }

    public void setAvailableTvcSpots(int _availableTvcSpots) {
        this._availableTvcSpots = _availableTvcSpots;
    }

    public int getLogoSpots() {
        return _logoSpots;
    }

    public void setLogoSpots(int _logoSpots) {
        this._logoSpots += _logoSpots;
        this._availableLogoSpots = this._logoSpots;
    }

    public int getAvailableLogoSpots() {
        return _availableLogoSpots;
    }

    public void setAvailableLogoSpots(int _availableLogoSpots) {
        this._availableLogoSpots -= _availableLogoSpots;
    }

    public int getCrowlerSpots() {
        return _crowlerSpots;
    }

    public void setCrowlerSpots(int _crowlerSpots) {
        this._crowlerSpots += _crowlerSpots;
        this._availableCrowlerSpots = this._crowlerSpots;
    }

    public int getAvailableCrowlerSpots() {
        return _availableCrowlerSpots;
    }

    public void setAvailableCrowlerSpots(int _availableCrowlerSpots) {
        this._availableCrowlerSpots -= _availableCrowlerSpots;
    }

    public int getV_sqeezeSpots() {
        return _v_sqeezeSpots;
    }

    public void setV_sqeezeSpots(int _v_sqeezeSpots) {
        this._v_sqeezeSpots += _v_sqeezeSpots;
        this._availableV_SqeezeSpots = this._v_sqeezeSpots;
    }

    public int getAvailableV_SqeezeSpots() {
        return _availableV_SqeezeSpots;
    }

    public void setAvailableV_SqeezeSpots(int _availableV_SqeezeSpots) {
        this._availableV_SqeezeSpots -= _availableV_SqeezeSpots;
    }

    public int getL_sqeezeSpots() {
        return _l_sqeezeSpots;
    }

    public void setL_sqeezeSpots(int _l_sqeezeSpots) {
        this._l_sqeezeSpots += _l_sqeezeSpots;
        this._availableL_SqeezeSpots = this._l_sqeezeSpots;
    }

    public int getAvailableL_SqeezeSpots() {
        return _availableL_SqeezeSpots;
    }

    public void setAvailableL_SqeezeSpots(int _availableL_SqeezeSpots) {
        this._availableL_SqeezeSpots -= _availableL_SqeezeSpots;
    }

    public int getNumbertOfAirSpot() {
        return numbertOfAirSpot;
    }

    public void setNumbertOfAirSpot(int numbertOfAirSpot) {
        this.numbertOfAirSpot += numbertOfAirSpot;
    }

    public int getAirLogoSpots() {
        return airLogoSpots;
    }

    public void setAirLogoSpots(int airLogoSpots) {
        this.airLogoSpots += airLogoSpots;
    }

    public int getAirCrowlerSpots() {
        return airCrowlerSpots;
    }

    public void setAirCrowlerSpots(int airCrowlerSpots) {
        this.airCrowlerSpots += airCrowlerSpots;
    }

    public int getAirV_sqeezeSpots() {
        return airV_sqeezeSpots;
    }

    public void setAirV_sqeezeSpots(int airV_sqeezeSpots) {
        this.airV_sqeezeSpots += airV_sqeezeSpots;
    }

    public int getAirL_sqeezeSpots() {
        return airL_sqeezeSpots;
    }

    public void setAirL_sqeezeSpots(int airL_sqeezeSpots) {
        this.airL_sqeezeSpots += airL_sqeezeSpots;
    }

    public HashMap<Integer, Integer> getTvcBreackdownMap() {
        return tvcBreackdownMap;
    }

    public void setTvcBreackdownMap(HashMap<Integer, Integer> tvcBreackdownMap) {
        this.tvcBreackdownMap = tvcBreackdownMap;
    }

    public HashMap<Integer, Integer> getTvcBreackdownMapAvailable() {
        return tvcBreackdownMapAvailable;
    }

    public void setTvcBreackdownMapAvailable(HashMap<Integer, Integer> tvcBreackdownMapPlayed) {
        this.tvcBreackdownMapAvailable = tvcBreackdownMapPlayed;
    }

    public long getTotalTVCDuration() {
        return totalTVCDuration;
    }

    public void setTotalTVCDuration(long totalTVCDuration) {
        this.totalTVCDuration = totalTVCDuration;
    }

    public long getAvailableTVCDuration() {
        return availableTVCDuration;
    }

    public void setAvailableTVCDuration(long availableTVCDuration) {
        this.availableTVCDuration = availableTVCDuration;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

}
