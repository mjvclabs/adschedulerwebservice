/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vclabs.nash.model.view.CustomDateSerializer;

import java.util.Date;

/**
 *
 * @author Nalaka
 * @since 27/02/2018
 */
public class SalesView {

    private int workOrderId;
    private String agencyName;
    private String clientName;
    private String workOrderType;
    private String invoiceType;
    private String month;
    private String marketingExecutive;
    private String amount;
    private int primeSpotCount;
    private int nonPrimeSpotCount;
    private String rate;
    private String workOrderStatus;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date invoiceDate;
    private int invoiceNo;
    private String payment = "";

    public SalesView() {
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getWorkOrderType() {
        return workOrderType;
    }

    public void setWorkOrderType(String workOrderType) {
        this.workOrderType = workOrderType;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getME() {
        return marketingExecutive;
    }

    public void setME(String ME) {
        this.marketingExecutive = ME;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getPrimeSpotCount() {
        return primeSpotCount;
    }

    public void setPrimeSpotCount(int primeSpotCount) {
        this.primeSpotCount = primeSpotCount;
    }

    public int getNonPrimeSpotCount() {
        return nonPrimeSpotCount;
    }

    public void setNonPrimeSpotCount(int nonPrimeSpotCount) {
        this.nonPrimeSpotCount = nonPrimeSpotCount;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getWorkOrderStatus() {
        return workOrderStatus;
    }

    public void setWorkOrderStatus(String workOrderStatus) {
        this.workOrderStatus = workOrderStatus;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public int getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(int invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

}
