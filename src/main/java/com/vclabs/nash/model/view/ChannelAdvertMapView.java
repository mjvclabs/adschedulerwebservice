package com.vclabs.nash.model.view;

import java.util.HashMap;

public class ChannelAdvertMapView {

    HashMap<Integer, String> channelIdMap = new HashMap<>();
    HashMap<Integer, String> advertIdMap = new HashMap<>();
    HashMap<String, Integer> channelAdvertMap = new HashMap<>();

    public ChannelAdvertMapView() {
    }

    public HashMap<Integer, String> getChannelIdMap() {
        return channelIdMap;
    }

    public void setChannelIdMap(HashMap<Integer, String> channelIdMap) {
        this.channelIdMap = channelIdMap;
    }

    public HashMap<Integer, String> getAdvertIdMap() {
        return advertIdMap;
    }

    public void setAdvertIdMap(HashMap<Integer, String> advertIdMap) {
        this.advertIdMap = advertIdMap;
    }

    public HashMap<String, Integer> getChannelAdvertMap() {
        return channelAdvertMap;
    }

    public void setChannelAdvertMap(HashMap<String, Integer> channelAdvertMap) {
        this.channelAdvertMap = channelAdvertMap;
    }

}
