/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;

/**
 *
 * @author Sanira Nanayakkara
 */
public class TimeBeltUtilizationItem {

    private String date;
    private int adCount = 0;
    private int uCategory = 0;
    @JsonSerialize(using = CustomTimeSerializer_mmss.class)
    private Date tlTime;
    @JsonSerialize(using = CustomTimeSerializer_mmss.class)
    private Date utTime;
    private double uRatio = 0.0;

    @JsonIgnore
    private double[] TimeBeltUtilizedCategory = new double[]{0.75, 1.0};

    public TimeBeltUtilizationItem() {
        tlTime = new Date(0);
        utTime = new Date(0);
        adCount = 0;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getAdCount() {
        return adCount;
    }

    public void setAdCount(int advertCount) {
        this.adCount = advertCount;
    }

    public void addOne() {
        adCount++;
    }

    public int getUCategory() {
        return uCategory;
    }

    public void setUCategory(int utilizationCategory) {
        this.uCategory = utilizationCategory;
    }

    public Date getTlTime() {
        return tlTime;
    }

    public void setTlTime(Date totalAdvertTime) {
        this.tlTime = totalAdvertTime;
    }

    public Date getUtTime() {
        return utTime;
    }

    public void setUtTime(Date utilizedAdvertTime) {
        this.utTime = utilizedAdvertTime;
    }

    public double getuRatio() {
        return uRatio;
    }

    public void setuRatio(double uRatio) {
        this.uRatio = uRatio;
    }
    
    public void addUtilizedAdvertTimeinSeconds(long seconds) {
        this.utTime.setTime(this.utTime.getTime() + seconds * 1000);

        uRatio = this.utTime.getTime() / (double) this.tlTime.getTime();
        for (int i = 0; i < TimeBeltUtilizedCategory.length; i++) {
            if (uRatio > TimeBeltUtilizedCategory[i]) {
                this.uCategory = i + 1;
            }
        }
    }

    public void addUtilizedAdvertTimeinSeconds() {

        uRatio = this.utTime.getTime() / (double) this.tlTime.getTime();
        for (int i = 0; i < TimeBeltUtilizedCategory.length; i++) {
            if (uRatio > TimeBeltUtilizedCategory[i]) {
                this.uCategory = i + 1;
            }
        }
    }

}
