/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.view.reporting;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import com.vclabs.nash.model.view.CustomDateSerializer;

/**
 *
 * @author Nalaka
 * @since 02/03/2018
 */
public class MaterialAnalysis {

    private int cutId;
    private String advertisementName;
    private String agency;
    private String client;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date captureDate;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date lastAiredDate;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date expiredDate;
    private String status;

    public MaterialAnalysis() {
    }

    public int getCutId() {
        return cutId;
    }

    public void setCutId(int cutId) {
        this.cutId = cutId;
    }

    public String getAdvertisementName() {
        return advertisementName;
    }

    public void setAdvertisementName(String advertisementName) {
        this.advertisementName = advertisementName;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(Date captureDate) {
        this.captureDate = captureDate;
    }

    public Date getLastAiredDate() {
        return lastAiredDate;
    }

    public void setLastAiredDate(Date lastAiredDate) {
        this.lastAiredDate = lastAiredDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
   
}