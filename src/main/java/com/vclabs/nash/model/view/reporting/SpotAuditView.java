package com.vclabs.nash.model.view.reporting;

public class SpotAuditView {
    private String time;
    private String action;
    private int spotCount;

    public SpotAuditView() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getSpotCount() {
        return spotCount;
    }

    public void setSpotCount(int spotCount) {
        this.spotCount = spotCount;
    }
}
