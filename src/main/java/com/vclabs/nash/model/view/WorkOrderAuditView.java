/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.view;

/**
 *
 * @author user
 */
public class WorkOrderAuditView {

    private String date;
    private String field;
    private String oldData;
    private String newData;
    private String user;

    public WorkOrderAuditView() {
    }

    public WorkOrderAuditView(String date, String field, String oldData, String newData, String user) {
        this.date = date;
        this.field = field;
        this.oldData = oldData;
        this.newData = newData;
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOldData() {
        return oldData;
    }

    public void setOldData(String oldData) {
        this.oldData = oldData;
    }

    public String getNewData() {
        return newData;
    }

    public void setNewData(String newData) {
        this.newData = newData;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setWorkOrderTableField(String fieldName) {

        switch (fieldName) {
            case "Seller":
                setField("Marketing Ex");
                break;

            case "woType":
                setField("WO Type");
                break;

            case "schedule_ref_no":
                setField("Schedule ref");
                break;

            case "Order_name":
                setField("Product");
                break;

            case "commission":
                setField("Commission");
                break;

            case "Agency_client":
                setField("Agency Client");
                break;

            case "Client":
                setField("End Client");
                break;

            case "Bill_client":
                setField("Billing Client");
                break;

            case "Start_date":
                setField("Start Date");
                break;

            case "End_date":
                setField("End Date");
                break;

            case "totalBudget":
                setField("Total budget");
                break;

            case "Comment":
                setField("Comment");
                break;

            case "Billing_status":
                setField("Billing status");
                break;

            case "B_spot_visibility":
                setField("Visibility");
                break;

            case "Permission_status":
                setField("Status");
                if (newData.equals("4")) {
                    newData = "Suspend";
                    oldData = "Active";
                }
                break;

            case "Date":
                setField("Date");
                break;

            case "statusAuto":
                setField("Auto Status");
                break;

            case "autoStatus":
                setField("Auto Status");
                break;

            case "manualStatus":
                setField("Manual Status");
                break;

            case "revenueMonth":
                setField("Revenue month");
                break;

            default:
                setField(fieldName);
                break;
        }

    }

    public void setWorkOrderChannelListTableField(String fieldName) {

        switch (fieldName) {
            case "Numbert_of_spot":
                setField("Tvc spots");
                break;

            case "Bonus_spot":
                setField("Tvc bonus spots");
                break;

            case "Tvc_package_value":
                setField("Tvc package value");
                break;

            case "Logo_spots":
                setField("Logo spots");
                break;

            case "Logo_b_spots":
                setField("Logo bonus spots");
                break;

            case "Logo_package_value":
                setField("Logo package value");
                break;

            case "Crowler_spots":
                setField("Crowler spots");
                break;

            case "Crowler_b_spots":
                setField("Crowler bonus spots");
                break;

            case "Crowler_package_value":
                setField("Crowler package value");
                break;

            case "V_sqeez_spots":
                setField("V sqeez spots");
                break;

            case "V_sqeeze_b_spots":
                setField("V sqeeze bonus spots");
                break;

            case "V_sqeez_package_value":
                setField("V sqeez package value");
                break;

            case "L_sqeeze_spots":
                setField("L sqeeze spots");
                break;

            case "L_sqeeze_b_spots":
                setField("L sqeeze bonus spots");
                break;

            case "L_sqeez_package_value":
                setField("L sqeez package value");
                break;

            case "Package_value":
                setField("Package value");
                break;

            case "Avarage_package_value":
                setField("Avarage package value");
                break;

            case "Channel_id":
                setField("Channel Id");
                break;

            default:
                setField(fieldName);
                break;
        }

    }

}
