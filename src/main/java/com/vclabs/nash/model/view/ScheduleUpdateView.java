/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vclabs.nash.model.process.ScheduleItemModel;

/**
 *
 * @author user
 */
public class ScheduleUpdateView {

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date schedulestarttime;
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    private Date scheduleendtime;
    private String program;

    private int advertid;
    private String advertName;
    private int advertDuration;
    private String adverLang;
    private String adverCategory;
    private Boolean isDummyCut = false;
    private Boolean isSuspendCut = false;

    private int channelid = -99;
    private String channelName;
    private String revenueline;
    private String month;

    private int workorderid;

    private List<ScheduleItemModel> scheduleItem = new ArrayList<>();

    public ScheduleUpdateView() {

    }

    public Date getSchedulestarttime() {
        return schedulestarttime;
    }

    public void setSchedulestarttime(Date schedulestarttime) {
        this.schedulestarttime = schedulestarttime;
    }

    public Date getScheduleendtime() {
        return scheduleendtime;
    }

    public void setScheduleendtime(Date scheduleendtime) {
        this.scheduleendtime = scheduleendtime;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public int getAdvertid() {
        return advertid;
    }

    public void setAdvertid(int advertid) {
        this.advertid = advertid;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public int getAdvertDuration() {
        return advertDuration;
    }

    public void setAdvertDuration(int advertDuration) {
        this.advertDuration = advertDuration;
    }

    public String getAdverLang() {
        return adverLang;
    }

    public void setAdverLang(String adverLang) {
        this.adverLang = adverLang;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(int workorderid) {
        this.workorderid = workorderid;
    }

    public List<ScheduleItemModel> getScheduleItem() {
        return scheduleItem;
    }

    public void setScheduleItem(List<ScheduleItemModel> scheduleItem) {
        this.scheduleItem = scheduleItem;
    }

    public String getRevenueline() {
        return revenueline;
    }

    public void setRevenueline(String revenueline) {
        this.revenueline = revenueline;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAdverCategory() {
        return adverCategory;
    }

    public void setAdverCategory(String adverCategory) {
        this.adverCategory = adverCategory;
    }

    public Boolean getIsDummyCut() {
        return isDummyCut;
    }

    public void setIsDummyCut(Boolean isDummyCut) {
        this.isDummyCut = isDummyCut;
    }

    public Boolean getIsSuspendCut() {
        return isSuspendCut;
    }

    public void setIsSuspendCut(Boolean isSuspendCut) {
        this.isSuspendCut = isSuspendCut;
    }

}
