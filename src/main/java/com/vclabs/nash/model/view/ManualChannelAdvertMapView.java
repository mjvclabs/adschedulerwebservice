package com.vclabs.nash.model.view;

/**
 * Created by Nalaka on 2018-09-18.
 */
public class ManualChannelAdvertMapView implements Comparable<ManualChannelAdvertMapView> {

    private int zeroAdsId;
    private int channelId;
    private String channelName;
    private int advertId;
    private int advertStatus;
    private int advertEnable;
    private String advertName;
    private int orderNum;
    private int tempId;

    public ManualChannelAdvertMapView() {
    }

    public int getZeroAdsId() {
        return zeroAdsId;
    }

    public void setZeroAdsId(int zeroAdsId) {
        this.zeroAdsId = zeroAdsId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getAdvertId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public String getAdvertName() {
        return advertName;
    }

    public void setAdvertName(String advertName) {
        this.advertName = advertName;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public int getTempId() {
        return tempId;
    }

    public void setTempId(int tempId) {
        this.tempId = tempId;
    }

    public int getAdvertStatus() {
        return advertStatus;
    }

    public void setAdvertStatus(int advertStatus) {
        this.advertStatus = advertStatus;
    }

    public int getAdvertEnable() {
        return advertEnable;
    }

    public void setAdvertEnable(int advertEnable) {
        this.advertEnable = advertEnable;
    }

    @Override
    public int compareTo(ManualChannelAdvertMapView o) {
        return this.orderNum - o.orderNum;
    }
}
