/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.springframework.format.annotation.DateTimeFormat;
import com.vclabs.nash.model.entity.TimeBelts;

/**
 *
 * @author Sanira Nanayakkara
 */
public class PriorityPreviewView {

    private TimeBelts timeBelt;
    private List<PriorityPreviewDate> priorityList = new ArrayList<>();

    public TimeBelts getTimeBelt() {
        return timeBelt;
    }

    public void setTimeBelt(TimeBelts timeBelt) {
        this.timeBelt = timeBelt;
    }

    public List<PriorityPreviewDate> getPriorityList() {
        return priorityList;
    }

    public void setPriorityList(List<PriorityPreviewDate> priorityList) {
        this.priorityList = priorityList;
    }

    public void addPriorityPreviewDate(Date date, List<PriorityPreviewItem> priorityItemList) {
        PriorityPreviewDate item = new PriorityPreviewDate();
        item.setDate(date);
        item.setPriorityItemList(priorityItemList);
        this.priorityList.add(item);
    }
    
    public void sortPriorityPreviewDates(){
        Collections.sort(priorityList);
    }

    private class PriorityPreviewDate implements Comparable<PriorityPreviewDate>{

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone="IST")
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date date;        
        private List<PriorityPreviewItem> priorityItemList = new ArrayList<>();

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public List<PriorityPreviewItem> getPriorityItemList() {
            return priorityItemList;
        }

        public void setPriorityItemList(List<PriorityPreviewItem> priorityItemList) {
            this.priorityItemList = priorityItemList;
        }

        @Override
        public int compareTo(PriorityPreviewDate o) {
            return this.date.compareTo(o.getDate());
        }
    }
}
