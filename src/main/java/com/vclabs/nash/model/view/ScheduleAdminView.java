/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.view;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vclabs.nash.model.process.ScheduleItemModel;

/**
 *
 * @author user
 */
public class ScheduleAdminView {

    private int timeBeltId;
    @JsonSerialize(using = CustomTimeSerializer.class)
    private Date schedulestarttime;
    @JsonSerialize(using = CustomTimeSerializer.class)
    private Date scheduleendtime;
    private List<ScheduleItemModel> scheduleItem = new ArrayList<>();

    public ScheduleAdminView() {
    }

    public ScheduleAdminView(int timeBeltId, Date schedulestarttime, Date scheduleendtime) {
        this.timeBeltId = timeBeltId;
        this.schedulestarttime = schedulestarttime;
        this.scheduleendtime = scheduleendtime;
    }

    public int getTimeBeltId() {
        return timeBeltId;
    }

    public void setTimeBeltId(int timeBeltId) {
        this.timeBeltId = timeBeltId;
    }

    public Date getSchedulestarttime() {
        return schedulestarttime;
    }

    public void setSchedulestarttime(Date schedulestarttime) {
        this.schedulestarttime = schedulestarttime;
    }

    public Date getScheduleendtime() {
        return scheduleendtime;
    }

    public void setScheduleendtime(Date scheduleendtime) {
        this.scheduleendtime = scheduleendtime;
    }

    public List<ScheduleItemModel> getScheduleItem() {
        return scheduleItem;
    }

    public void setScheduleItem(List<ScheduleItemModel> scheduleItem) {
        this.scheduleItem = scheduleItem;
    }

}
