/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.text.SimpleDateFormat;
import java.util.*;

import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.service.utill.DateUtil;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.PlayListDAO;

/**
 *
 * @author user
 */
@Repository //get playlist for schedule
public class PlayListDAOImpl extends VCLSessionFactory implements PlayListDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayListDAOImpl.class);

    @Override ///no use
    public List<PlayList> getPlayList(int _channelId, Date _date) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override /// get valid cuts for schedule
    public List<PlayList> getPlayListAdvert(int _channelId, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.eq("aid.adverttype", "ADVERT"));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getPlayListAdvert : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override /// get valid cuts for schedule
    public List<PlayList> getPlayListCrawler(int _channelId, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq("aid.adverttype", "CRAWLER"))
                    .add(Restrictions.eq("aid.adverttype", "V_SHAPE"))
                    .add(Restrictions.eq("aid.adverttype", "L_SHAPE")));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getPlayListCrawler : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override /// get valid cuts for schedule
    public List<PlayList> getPlayListLogo(int _channelId, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();


            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.eq("aid.adverttype", "LOGO"));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getPlayListLogo : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override //get final_scheduled playlist => Insertion /// depricated
    public List<PlayList> getScheduledPlayList(int _channelId, int _hour) throws Exception {
        List<PlayList> playList = new ArrayList<PlayList>();

        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(PlayList.class);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

            Date startDate = format.parse(default_format.format(new Date()));
            Date endDate = incrementDay(startDate);
            criteria.add(Restrictions.ge("date", startDate));
            criteria.add(Restrictions.lt("date", endDate));

            criteria.add(Restrictions.eq("scheduleHour", _hour));
            //scheduled or played(if backend service load the playlist in middle of an hour)           
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq("status", "1"))
                    .add(Restrictions.eq("status", "2"))
                    .add(Restrictions.eq("status", "3"))
                    .add(Restrictions.eq("status", "4"))); //for logos
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getScheduledPlayList : ", e.getMessage());
            throw e;
        }
        return playList;
    }

    @Override //get final_scheduled playlist => Insertion
    public List<PlayList> getScheduledPlayList(int _channelId, Date start_time, Date end_time, int _hour) throws Exception {
        List<PlayList> playList = new ArrayList<PlayList>();

        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(PlayList.class);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat default_format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

            Date startDate = format.parse(default_format.format(new Date()));
            Date endDate = incrementDay(startDate);
            criteria.add(Restrictions.ge("date", startDate));
            criteria.add(Restrictions.lt("date", endDate));

            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.ge("timeBeltStartTime", start_time));
            criteria.add(Restrictions.le("timeBeltEndTime", end_time));
            //scheduled or played(if backend service load the playlist in middle of an hour)
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq("status", "1"))
                    .add(Restrictions.eq("status", "2"))
                    .add(Restrictions.eq("status", "3")));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getScheduledPlayList : ", e.getMessage());
            throw e;
        }
        return playList;
    }

    @Override /// for update_played schedules from insertion backend
    public PlayList getPlayList(int _playListId) {
        PlayList playList = null;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("playlistid", _playListId));
            playList = (PlayList) criteria.uniqueResult();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getPlayList : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return playList;
    }

    @Override
    public boolean updatePlayList(PlayList _playList) {

        int channelId = _playList.getChannel().getChannelid();
        int cluster = _playList.getPlayCluster().intValue();
        int playOrder = _playList.getPlayOrder();
        int advertId = _playList.getAdvert().getAdvertid();
        String advertName = _playList.getAdvert().getAdvertname();
        int playListId = _playList.getPlaylistId() != null ? _playList.getPlaylistId() : -1;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();

            _playList.setChannel((ChannelDetails) session.load(ChannelDetails.class, _playList.getChannel().getChannelid()));
            _playList.setAdvert((Advertisement) session.load(Advertisement.class, _playList.getAdvert().getAdvertid()));
            _playList.setSchedule((ScheduleDef) session.load(ScheduleDef.class, _playList.getSchedule().getScheduleid()));
            _playList.setWorkOrder((WorkOrder) session.load(WorkOrder.class, _playList.getWorkOrder().getWorkorderid()));

            session.saveOrUpdate(_playList);
            session.flush();
            tx.commit();
        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in updatePlayList : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        LOGGER.debug("Successfully updated playlist - Channel : {} Cluster : {} Order : {} Advert ID : {} Advert Name: {} Playlist id: {}", channelId,
                cluster, playOrder, advertId, advertName, playListId);
        return true;
    }

    @Override
    public boolean saveOrUpdate(PlayList playList) {
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.saveOrUpdate(playList);
            tx.commit();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception while saving/updating playlist : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public boolean clearTable() {
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();

            String hql = String.format("delete from PlayList");
            Query query = session.createQuery(hql);
            query.executeUpdate();

            session.flush();
            tx.commit();

            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in clearTable : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }

    }

    /*Not getting status = 9 and 6 for avilable file checking*/

    /*Not getting status = 9 and 6 for avilable file checking*/
    @Override
    public boolean updateSchedulesValidity(int _channelId, Date start_time, Date end_time, int hour) {
        Session session = null;
        try {
            //update invalide adverts
            session = this.getSessionFactory().openSession();
            Transaction tx1 = session.beginTransaction();
            List<PlayList> playList_0 = new ArrayList<>();

            Criteria criteria_0 = session.createCriteria(PlayList.class);
            criteria_0.add(Restrictions.and(Restrictions.ne("status", "9"), Restrictions.ne("status", "6")));
            criteria_0.createAlias("advert", "advertisement");
            criteria_0.add(Restrictions.or(Restrictions.eq("advertisement.isFileAvailable", false), Restrictions.eq("advertisement.Enabled", 0)));
            criteria_0.add(Restrictions.gt("scheduleEndTime", start_time));
            criteria_0.add(Restrictions.lt("scheduleStartTime", end_time));
            criteria_0.add(Restrictions.le("scheduleHour", hour));
            criteria_0.add(Restrictions.eq("channel.channelid", _channelId));
            playList_0 = (List<PlayList>) criteria_0.list();

            session.flush();
            tx1.commit();

            LOGGER.debug("[UpdateSchedulesValidity] => Invalid advert schedules size : {} ", playList_0.size());

            for (PlayList item : playList_0) {
                if(!item.getStatus().equalsIgnoreCase("2") && !item.getStatus().equalsIgnoreCase("3")
                        && !item.getStatus().equalsIgnoreCase("4")){
                    item.setStatus("8"); //invalid_advert
                    item.setComment("invalid_advert");
                    LOGGER.debug("invalid_advert {} ", item.getPlaylistId());
                    this.updatePlayList(item);
                }
            }
            playList_0.clear();

            //update notplayed schedules
            Transaction tx2 = session.beginTransaction();
            List<PlayList> playList_1 = new ArrayList<PlayList>();

            Criteria criteria_1 = session.createCriteria(PlayList.class);
            criteria_1.add(Restrictions.disjunction()
                    .add(Restrictions.eq("status", "1"))
                    .add(Restrictions.eq("status", "3"))
                    .add(Restrictions.eq("status", "11")));//scheduled or stopped
            criteria_1.add(Restrictions.le("scheduleEndTime", start_time));
            criteria_1.add(Restrictions.eq("channel.channelid", _channelId));
            playList_1 = (List<PlayList>) criteria_1.list();

            session.flush();
            tx2.commit();
            LOGGER.debug("[UpdateSchedulesValidity] => Notplayed schedules size : {} ", playList_1.size());

            for (PlayList item : playList_1) {
                item.setStatus("5"); //skipped
                item.setComment("schedule_notplayed");
                LOGGER.debug("schedule_notplayed {} ", item.getPlaylistId());
                this.updatePlayList(item);
            }
            playList_1.clear();

            Transaction tx3 = session.beginTransaction();
            List<PlayList> playList_2 = new ArrayList<PlayList>();

            Criteria criteria_2 = session.createCriteria(PlayList.class);
            criteria_2.add(Restrictions.disjunction()
                    .add(Restrictions.eq("status", "0"))
                    .add(Restrictions.eq("status", "1"))
                    .add(Restrictions.eq("status", "3"))
                    .add(Restrictions.eq("status", "7"))
                    .add(Restrictions.eq("status", "10"))
                    .add(Restrictions.eq("status", "11")));

            criteria_2.add(Restrictions.gt("scheduleEndTime", start_time));
            criteria_2.add(Restrictions.lt("scheduleStartTime", end_time));
            criteria_2.add(Restrictions.le("scheduleHour", hour));
            criteria_2.add(Restrictions.eq("channel.channelid", _channelId));
            playList_2 = (List<PlayList>) criteria_2.list();

            session.flush();
            tx3.commit();

            LOGGER.debug("[UpdateSchedulesValidity] => current eligible schedules size : {} hour : {}", playList_2.size(), hour);

            for (PlayList item : playList_2) {
                if (item.getScheduleStartTime().after(start_time)) {
                    item.setTimeBeltStartTime(item.getScheduleStartTime());
                } else {
                    item.setTimeBeltStartTime(start_time);
                }

                if (item.getScheduleEndTime().before(end_time)) {
                    item.setTimeBeltEndTime(item.getScheduleEndTime());
                } else {
                    item.setTimeBeltEndTime(end_time);
                }

                ///prevent overwriting 3.30-4.30 logos start time in 4.00 schedule generating
                if (!(item.getAdvert().getAdverttype().equals("LOGO") && item.getRetryCount() > 0)) {
                    item.setActualStartTime(start_time);
                }
                item.setActualEndTime(end_time);

                item.setScheduleHour(hour);
                item.setStatus("0");
                item.setComment("schedule_current");
                LOGGER.debug("schedule_current {} ", item.getPlaylistId());
                this.updatePlayList(item);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            session.close();
        }
        return true;
    }

    @Override
    public boolean insertPlayList(PlayList _playList) {
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();

            _playList.setChannel((ChannelDetails) session.load(ChannelDetails.class, _playList.getChannel().getChannelid()));
            _playList.setAdvert((Advertisement) session.load(Advertisement.class, _playList.getAdvert().getAdvertid()));
            _playList.setSchedule((ScheduleDef) session.load(ScheduleDef.class, _playList.getSchedule().getScheduleid()));
            _playList.setWorkOrder((WorkOrder) session.load(WorkOrder.class, _playList.getWorkOrder().getWorkorderid()));

            session.saveOrUpdate(_playList);
            session.flush();
            tx.commit();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in insertPlayList : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return true;
    }

    public Date incrementDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

    @Override       /// get valid cuts for schedule
    public List<PlayList> getPlayListAdvert(int _channelId, Date start_time, Date end_time, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.gt("scheduleEndTime", start_time));
            criteria.add(Restrictions.lt("scheduleStartTime", end_time));
            criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.ge("date", DateUtil.getStartOfToday()));
            criteria.add(Restrictions.eq("aid.adverttype", "ADVERT"));
            criteria.add(Restrictions.eq("aid.isFileAvailable", true));
            criteria.createAlias("workOrder", "wo");
            criteria.add(Restrictions.ne("wo.manualStatus", WorkOrder.ManualStatus.Hold));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getPlayListAdvert : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override      /// get valid cuts for schedule
    public List<PlayList> getPlayListCrawler(int _channelId, Date start_time, Date end_time, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.gt("scheduleEndTime", start_time));
            criteria.add(Restrictions.lt("scheduleStartTime", end_time));
            criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq("aid.adverttype", "CRAWLER"))
                    .add(Restrictions.eq("aid.adverttype", "V_SHAPE"))
                    .add(Restrictions.eq("aid.adverttype", "L_SHAPE")));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getPlayListCrawler : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override      /// get valid cuts for schedule
    public List<PlayList> getPlayListLogo(int _channelId, Date start_time, Date end_time, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            //criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.gt("scheduleEndTime", start_time));
            criteria.add(Restrictions.lt("scheduleStartTime", end_time));
            //criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.and(Restrictions.ne("status", "9"), Restrictions.ne("status", "6"), Restrictions.ne("status", "8")));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.eq("aid.adverttype", "LOGO"));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getPlayListLogo : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override
    public List<PlayList> getZeroBudgetPlayLists(int channelId, Date startTime, Date endTime, int hour) {
        String sql = "select pl.* from play_list pl left join advertisement_def ad on pl.Advert_id = ad.Advert_id " +
                " where pl.Schedule_hour = ?1 and pl.Channel_id = ?2 and pl.Work_order_id in(select w.Work_order_id from work_order w where w.totalBudget = 0)" +
                " and ad.Advert_type = 'ADVERT' and pl.Schedule_start_time < ?3 and pl.Schedule_end_time > ?4 order by ad.Duration";
        Session session = this.getSessionFactory().getCurrentSession();
        javax.persistence.Query query = session.createNativeQuery(sql, PlayList.class);
        query.setParameter(1, hour);
        query.setParameter(2, channelId);
        query.setParameter(3, endTime);
        query.setParameter(4, startTime);
        return query.getResultList();
    }

    @Override
    public int deletePlayList(int playListId) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "delete from play_list where Playlist_id = ?1 ";
        javax.persistence.Query query = session.createNativeQuery(sqlString);
        query.setParameter(1, playListId);
        return query.executeUpdate();
    }

    @Override
    public List<PlayList> getSelectedHourValidAdvert(int _channelId, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.and(Restrictions.ne("status", "2"),
                    Restrictions.ne("status", "4"),
                    Restrictions.ne("status", "9"),
                    Restrictions.ne("status", "6"),
                    Restrictions.ne("status", "8"),
                    Restrictions.ne("status", "11")));
            criteria.add(Restrictions.ge("date", DateUtil.getStartOfToday()));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.add(Restrictions.eq("lable", PlayList.Lable.COMMERICIAL));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.eq("aid.adverttype", "ADVERT"));
            criteria.add(Restrictions.eq("aid.isFileAvailable", true));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getSelectedHourValidAdvert : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override
    public int findLastPlayedAdvertOrder(int channelId, int hour,  int clusterIndex) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select max(pl.Play_order) from play_list pl left join advertisement_def ad on pl.Advert_id = ad.Advert_id " +
                " where pl.Channel_id = ?1 and (ad.Advert_type= 'ADVERT' or ad.Advert_type= 'FILLER') and pl.Schedule_hour = ?2 and  pl.Status in (2,4) and pl.Play_cluster = ?3";
        javax.persistence.Query query = session.createNativeQuery(sqlString);
        query.setParameter(1, channelId);
        query.setParameter(2, hour);
        query.setParameter(3, clusterIndex);
        Object result = query.getSingleResult();
        return result != null ? (int) result : -1;
    }

    @Override
    public List<PlayList> getLastPlayedCluster(int channelId, int hour,  int clusterIndex) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select pl.* from play_list pl left join advertisement_def ad on pl.Advert_id = ad.Advert_id " +
                " where pl.Channel_id = ?1 and (ad.Advert_type= 'ADVERT' or ad.Advert_type= 'FILLER') and pl.Schedule_hour = ?2 " +
                " and pl.Status in(2,4) and pl.Play_cluster = ?3 order by Play_order asc";
        javax.persistence.Query query = session.createNativeQuery(sqlString, PlayList.class);
        query.setParameter(1, channelId);
        query.setParameter(2, hour);
        query.setParameter(3, clusterIndex);
        return (List<PlayList>) query.getResultList();
    }

    @Override
    public List<PlayList> getSelectedHourValidFillersAndZeroAdvert(int _channelId, int _hour) {
        List<PlayList> ret_playList = new ArrayList<PlayList>();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(PlayList.class);
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.and(Restrictions.ne("status", "2"),
                    Restrictions.ne("status", "9"),
                    Restrictions.ne("status", "6"),
                    Restrictions.ne("status", "8"),
                    Restrictions.ne("status", "11")));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.add(Restrictions.or(Restrictions.eq("lable", PlayList.Lable.FILLER), Restrictions.eq("lable", PlayList.Lable.ZERO)));
            ret_playList = (List<PlayList>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in PlayListDAOImpl in getSelectedHourValidAdvert : ", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override
    public List<PlayList> getPlayingStatusLogoAdverts(int channelId, Date endTime) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select * from play_list where Channel_id = ?1 and status = 4 and Schedule_end_time > ?2";
        javax.persistence.Query query = session.createNativeQuery(sqlString, PlayList.class);
        query.setParameter(1, channelId);
        query.setParameter(2, endTime);
        return (List<PlayList>) query.getResultList();
    }

    @Override
    public PlayList getAiringPlayList(int channelId, int hour) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select * from play_list where Channel_id = ?1 and Schedule_hour = ?2 and Status = 4 limit 1";
        javax.persistence.Query query = session.createNativeQuery(sqlString, PlayList.class);
        query.setParameter(1, channelId);
        query.setParameter(2, hour);
        Object result = query.getSingleResult();
        return result != null ? (PlayList) result : null;
    }
}
