/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.ServerDetailsDAO;
import com.vclabs.nash.model.entity.ServerDetails;

/**
 * @author Sanira Nanayakkara
 */
@Repository
public class ServerDetailsDAOImpl extends VCLSessionFactory implements ServerDetailsDAO {

    private static final Logger logger = LoggerFactory.getLogger(ServerDetailsDAOImpl.class);

    @Override
    public List<ServerDetails> getServerList() {
        ArrayList<ServerDetails> serverList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ServerDetails.class);
            serverList = (ArrayList<ServerDetails>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in ServerDetailsDAOImpl in getServerList : ", e.getMessage());
            throw e;
        }
        return serverList;
    }

    @Override
    public ServerDetails getServerDetails(int iServerId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            ServerDetails serverDetails = null;
            Criteria criteria = session.createCriteria(ServerDetails.class);
            criteria.add(Restrictions.eq("serverId", iServerId));
            serverDetails = (ServerDetails) criteria.uniqueResult();
            return serverDetails;
        } catch (Exception e) {
            logger.debug("Exception in ServerDetailsDAOImpl in getServerDetails : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public int insertServerDetails(ServerDetails model) {
        try {
            if (model.getServerId() == null) {
                //logger.info("INSERT INTO `vcl_schedule_db`.`server_details` (`ServerIP`, `ServerPort`, `CardPort`, `ServicePath`, `Status`, `FreeDiskSpace`, `FreeMemory`, `InCard`, `OutCard`, `Command`, `ErrorReports`, `ChannelId`) VALUES ('" + model.getServerIP() + "', " + model.getServerPort() + ", " + model.getCardPort() + ", '" + model.getServicePath() + "', " + model.getStatus() + "," + model.getFreeDiskSpace() + ", " + model.getFreeMemory() + ", " + model.getInCard() + ", " + model.getOutCard() + ", '" + model.getCommand() + "', '" + model.getErrorReport() + "', " + model.getChannelDetails() + ");");
            } else {
               // logger.info("UPDATE `vcl_schedule_db`.`server_details` SET `ServerIP`='" + model.getServerIP() + "', `ServerPort`=" + model.getServerPort() + ", `CardPort`=" + model.getCardPort() + ", `ServicePath`='" + model.getServicePath() + "', `Status`=" + model.getStatus() + ", `FreeDiskSpace`=" + model.getFreeDiskSpace() + ", `FreeMemory`=" + model.getFreeMemory() + ", `InCard`=" + model.getInCard() + ", `OutCard`=" + model.getOutCard() + ", `Command`='" + model.getCommand() + "', `ErrorReports`='" + model.getErrorReport() + "', `ChannelId`=" + model.getChannelDetails() + " WHERE `ServerId`=" + model.getServerId() + ";");
            }
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return model.getServerId();
        } catch (Exception e) {
            logger.debug("Exception in ServerDetailsDAOImpl in insertServerDetails : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public boolean updateServerDetails(ServerDetails model) {
        try {
            logger.info("UPDATE `vcl_schedule_db`.`server_details` SET `ServerIP`='" + model.getServerIP() + "', `ServerPort`=" + model.getServerPort() + ", `CardPort`=" + model.getCardPort() + ", `ServicePath`='" + model.getServicePath() + "', `Status`=" + model.getStatus() + ", `FreeDiskSpace`=" + model.getFreeDiskSpace() + ", `FreeMemory`=" + model.getFreeMemory() + ", `InCard`=" + model.getInCard() + ", `OutCard`=" + model.getOutCard() + ", `Command`='" + model.getCommand() + "', `ErrorReports`='" + model.getErrorReport() + "', `ChannelId`=" + model.getChannelDetails() + " WHERE `ServerId`=" + model.getServerId() + ";");
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in ServerDetailsDAOImpl in updateServerDetails : ", e.getMessage());
            throw e;
        }
    }

}
