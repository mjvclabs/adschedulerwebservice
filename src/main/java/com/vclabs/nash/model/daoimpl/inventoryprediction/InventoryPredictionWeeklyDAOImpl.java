package com.vclabs.nash.model.daoimpl.inventoryprediction;

import com.vclabs.nash.model.dao.inventoryprediction.InventoryPredictionWeeklyDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryWeeklyPrediction;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
@Repository
public class InventoryPredictionWeeklyDAOImpl extends VCLSessionFactory implements InventoryPredictionWeeklyDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryPredictionWeeklyDAOImpl.class);

    @Override
    @Transactional
    public InventoryWeeklyPrediction save(InventoryWeeklyPrediction inventoryWeeklyPrediction) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            session.saveOrUpdate(inventoryWeeklyPrediction);
            LOGGER.debug("InventoryWeeklyPrediction is saved/updated successfully");
        } catch (Exception e) {
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryWeeklyPrediction;
    }

    @Override
    public InventoryWeeklyPrediction update(InventoryWeeklyPrediction inventoryWeeklyPrediction) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            session.update(inventoryWeeklyPrediction);
            LOGGER.debug("Successfully updated InventoryWeeklyPrediction");
        } catch (Exception e) {
            LOGGER.debug("Exception : {}", e.getMessage());
        }
        return inventoryWeeklyPrediction;
    }

    @Override
    public List<InventoryWeeklyPrediction> findAll() {
        Session session = this.getSessionFactory().getCurrentSession();
        List<InventoryWeeklyPrediction> inventoryDailyPredictions = session.createCriteria(InventoryWeeklyPrediction.class).list();
        return inventoryDailyPredictions;
    }

    @Override
    public List<InventoryWeeklyPrediction> findLatestWeeklyPrediction() {
        String sql = "SELECT iwp.* FROM inventory_weekly_prediction iwp left join prediction_result_meta prm on iwp.predictionResultMetaId_id=prm.id where prm.isProcess=false";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, InventoryWeeklyPrediction.class);
        return query.getResultList();
    }

    @Override
    public List<InventoryWeeklyPrediction> findLatestPredictions(Date startingDate, Date lastDate) {
        String sql = "select * from inventory_weekly_prediction idp where idp.dateAndTime between ?1 and ?2";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, InventoryWeeklyPrediction.class);
        query.setParameter(1, startingDate);
        query.setParameter(2, lastDate);
        List<InventoryWeeklyPrediction> result = query.getResultList();
        return result;
    }

}
