package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.dashboard.dto.ChannelPredictionDto;
import com.vclabs.nash.model.dao.AdvertDAO;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Sanduni on 24/01/2019
 */
@Repository
public class AdvertDAOImpl extends VCLSessionFactory implements AdvertDAO{

    @Override
    @Transactional("advert")
    public List<ChannelPredictionDto> findAll() {
        List<ChannelPredictionDto> result = null;
        try {
            Session session = this.getSessionFactorySecondary().getCurrentSession();
            Query query = session.createNativeQuery("select ci.ChannelID as id, ci.ChannelNameShort as name, count(*) as " +
                    "count from channel_inst ci left join pred_inst pi on ci.ChannelID = pi.ChannelID " +
                    "where pi.precisionStatus = 0 " +
                    "group by ci.ChannelID", "currentPredictionViewMapping");
            result = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return result;
    }
}
