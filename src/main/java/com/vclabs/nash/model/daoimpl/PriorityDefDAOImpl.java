/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */


package com.vclabs.nash.model.daoimpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.PriorityDefDAO;
import com.vclabs.nash.model.entity.PriorityDef;

import javax.persistence.Query;

/**
 *
 * @author Sanira Nanayakkara
 */
@Repository
public class PriorityDefDAOImpl extends VCLSessionFactory implements PriorityDefDAO{

    static final Logger LOGGER = LoggerFactory.getLogger(PriorityDefDAOImpl.class);


    @Override
    public Boolean savePriorityDef(PriorityDef model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in savePriorityDef : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean updatePriorityDef(PriorityDef model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in updatePriorityDef : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PriorityDef> getPriorityList() {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PriorityDef.class);
            return (List<PriorityDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in getPriorityList : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public PriorityDef getPrioritybyID(int id) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PriorityDef.class);
            criteria.add(Restrictions.eq("priorityId", id));
            return (PriorityDef)criteria.uniqueResult();
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in getPrioritybyID : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PriorityDef> getPrioritybyChannel(int id) {
        List<PriorityDef> priority = new ArrayList<PriorityDef>();
        try {
//            String hql = "from PriorityDef as priority right outer join priority.timebelt as tbelt WHERE tbelt.channelID = :id ";
//            Query query = session.createQuery(hql);
//            query.setParameter("id",id);
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PriorityDef.class);
            criteria.createAlias("timebelt", "_timebelt").add(Restrictions.eq("_timebelt.channelID", id));
            criteria.addOrder(Order.asc("timebelt"));
            criteria.addOrder(Order.asc("cluster"));
            criteria.addOrder(Order.asc("priority"));
            return (List<PriorityDef>)criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in getPrioritybyChannel : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PriorityDef> getPriorityListbyTimeBelt(int timeBeltId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PriorityDef.class);
            criteria.createAlias("timebelt", "_timebelt").add(Restrictions.eq("_timebelt.timeBeltId", timeBeltId));
            criteria.addOrder(Order.asc("cluster"));
            List<PriorityDef> ret = (List<PriorityDef>)criteria.list();
            return ret;
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in getPriorityListbyTimeBelt : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean removePriorityDef(PriorityDef model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(session.contains(model) ? model : session.merge(model));
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in removePriorityDef : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean removePriorityDefV2(PriorityDef model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery("delete from priority_def where priority_id = ?1");
            query.setParameter(1, model.getPriorityId());
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in removePriorityDef : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<PriorityDef> getPriorityListbyCluster(int timeBeltId, int cluster) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PriorityDef.class);
            criteria.createAlias("timebelt", "_timebelt").add(Restrictions.eq("_timebelt.timeBeltId", timeBeltId));
            criteria.add(Restrictions.eq("cluster", cluster));
            criteria.addOrder(Order.asc("cluster"));
            List<PriorityDef> ret = (List<PriorityDef>)criteria.list();
            return ret;
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in getPriorityListbyCluster : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public PriorityDef getPriorityDefbyPriority(int timeBeltId, int cluster, int priority) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(PriorityDef.class);
            criteria.createAlias("timebelt", "_timebelt").add(Restrictions.eq("_timebelt.timeBeltId", timeBeltId));
            criteria.add(Restrictions.eq("cluster", cluster));
            criteria.add(Restrictions.eq("priority", priority));
            Object obj = criteria.uniqueResult(); 
            if(obj == null) return null;
            PriorityDef ret = (PriorityDef)obj; 
            return ret;
        } catch (Exception e) {
            LOGGER.debug("Exception in PriorityDefDAOImpl in getPriorityDefbyPriority : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public int getSelectedTimebeltClusterCount(int timeBeltId) {
        String sql = String.format("SELECT count(DISTINCT cluster)  FROM priority_def pd left join time_belts tb on pd.timebelt_id = tb.time_belt_id where pd.timebelt_id=%s ", "" + timeBeltId);
        Session session = this.getSessionFactory().getCurrentSession();
        SQLQuery query = session.createNativeQuery(sql);
        BigInteger nonPrimeTimeSpo = (BigInteger) query.getSingleResult();
        return nonPrimeTimeSpo.intValue();
    }

}
