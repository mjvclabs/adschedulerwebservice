/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vclabs.nash.model.dao.TimeSlotScheduleMapDAO;
import com.vclabs.nash.model.dto.AdvertSpread;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

/**
 *
 * @author Nalaka
 * @since 27-02-2018
 */
@Repository
public class TimeSlotScheduleMapDAOImp extends VCLSessionFactory implements TimeSlotScheduleMapDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeSlotScheduleMapDAOImp.class);

    @Override
    public List<TimeSlotScheduleMap> getSelectedWorkOrderTimeSlot(int workOrder) {
        List<TimeSlotScheduleMap> timeSlotMap = null;

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("workOrderId", workOrder));
            criteria.createAlias("scheduleid", "schedule");
            criteria.add(Restrictions.ne("schedule.status", "6"));
            timeSlotMap = (ArrayList<TimeSlotScheduleMap>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in TimeSlotScheduleMapDAOImpl in getSelectedWorkOrderTimeSlot", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return timeSlotMap;
    }

    @Override
    public TimeSlotScheduleMap getSelectedTimeSlot(int scheduleID) {

        TimeSlotScheduleMap model = new TimeSlotScheduleMap();

        Session session = null;
        try {
            session = this.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("scheduleid.scheduleid", scheduleID));
            model = (TimeSlotScheduleMap) criteria.uniqueResult();
            return model;
        } catch (Exception e) {
            LOGGER.debug("Exception in TimeSlotScheduleMapDAOImpl in getSelectedTimeSlot", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<TimeSlotScheduleMap> getTimeSlotsOfNotCreatedPlayLists(Date date){
        String sql = "select * from time_slot_schedule_map  tsm where tsm.Date = ?1 " +
                "and tsm.Schedule_id not in (select Schedule_id from play_list)";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, TimeSlotScheduleMap.class);
        query.setParameter(1, date);
        return query.getResultList();
    }

    @Override
    public List<AdvertSpread> getAdvertSpreads(Date date, long channelId, long advertId) {
        String sql = "select sm.Time_belt_id as timebeltId, count(*) as adCount from time_slot_schedule_map sm  left join schedule_def sd " +
                "on sm.Schedule_id = sd.Schedule_id " +
                "where sd.Date = ?1 and sd.Channel_id = ?2 and sd.Status != 6 and sd.Status != 9 and sd.Advert_id = ?3 " +
                "group by sm.Time_belt_id order by sm.Time_belt_id";
        try(Session session = this.getSessionFactory().openSession()) {
            Query query = session.createNativeQuery(sql, "advertSpreadMapping");
            query.setParameter(1, date);
            query.setParameter(2, channelId);
            query.setParameter(3, advertId);
            return query.getResultList();
        } catch (Exception e){
            LOGGER.debug("Exception in while getting advert spreads", e.getMessage());
            throw e;
        }
    }

    @Override
    public void deleteTimeSlotScheduleMap(Integer channelId, Date date, int startTimeBeltId) {
        String deleteQueryString = "delete from time_slot_schedule_map where Channel_id= ?1 and Date = ?2 and Time_belt_id >= ?3";
        Session session = this.getSessionFactory().openSession(); // For transaction issue
        Transaction tx = session.beginTransaction(); // For transaction issue
        try{
            LOGGER.info("When deleting row from time_slot_schedule_map | channel Id : {} | Date : {} | TimeBelt Id : {}", channelId, date, startTimeBeltId);
//            Transaction tx = session.beginTransaction();
            Query deleteQuery = session.createNativeQuery(deleteQueryString);
            deleteQuery.setParameter(1, channelId);
            deleteQuery.setParameter(2, date);
            deleteQuery.setParameter(3, startTimeBeltId);
            LOGGER.info("Before update time_slot_schedule_map | channel Id : {} | Date : {} | TimeBelt Id : {}", channelId, date, startTimeBeltId);
            deleteQuery.executeUpdate();
            LOGGER.info("After update time_slot_schedule_map | channel Id : {} | Date : {} | TimeBelt Id : {}", channelId, date, startTimeBeltId);
            session.flush();
            tx.commit();
        } catch (Exception e) {
            LOGGER.debug("Exception in while deleting time_slot_schedule_maps ", e.getMessage());
            if(tx != null){ // For transaction issue
                tx.rollback();
            }
            throw e;
        } finally { // For transaction issue
            session.close();
        }

    }

}
