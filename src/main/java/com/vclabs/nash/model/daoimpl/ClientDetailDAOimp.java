/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;

import com.vclabs.nash.model.process.dto.ClientWoTableDto;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.ClientDetailDAO;
import com.vclabs.nash.model.entity.ClientDetails;

import javax.persistence.Query;

/**
 *
 * @author user
 */
@Repository
public class ClientDetailDAOimp extends VCLSessionFactory implements ClientDetailDAO {

    private static final Logger logger = LoggerFactory.getLogger(ClientDetailDAOimp.class);

    @Override
    public int saveAndUpdateClient(ClientDetails model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return model.getClientid();
        } catch (Exception e) {
            logger.debug("Exception in ClientDetailDAOimp in saveAndUpdateClient() : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ClientDetails> getAllClient() {
        ArrayList<ClientDetails> clientList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ClientDetails.class);
            criteria.add(Restrictions.ne("status", ClientDetails.Status.DELETE));
            criteria.add(Restrictions.and(Restrictions.ne("clientid", 0),Restrictions.ne("clientid", 1)));
            clientList = (ArrayList<ClientDetails>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in ClientDetailDAOimp in getAllClient() : {}", e.getMessage());
            throw e;
        }
        return clientList;
    }

    @Override
    public ClientDetails getSelectedClient(int clientId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            ClientDetails clientDetails = null;
            Criteria criteria = session.createCriteria(ClientDetails.class);
            criteria.add(Restrictions.eq("clientid", clientId));
            clientDetails = (ClientDetails) criteria.uniqueResult();
            return clientDetails;
        } catch (Exception e) {
            logger.debug("Exception in ClientDetailDAOimp in getSelectedClient() : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public ClientDetails getSelectedClient(String clientName) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Boolean isNum = false;
            int clientId = 0;
            ClientDetails clientDetails = null;
            try {
                clientId = Integer.parseInt(clientName);
                isNum = true;
            } catch (Exception e) {
                logger.debug("Exception in ClientDetailDAOimp in getSelectedClient() : {}", e.getMessage());
                isNum = false;
            }

            Criteria criteria = session.createCriteria(ClientDetails.class);
            if (isNum) {
                criteria.add(Restrictions.eq("clientid", clientId));
            } else {
                criteria.add(Restrictions.eq("clientname", clientName));
            }
            clientDetails = (ClientDetails) criteria.uniqueResult();

            session.flush();
            return clientDetails;
        } catch (Exception e) {
            logger.debug("Exception in ClientDetailDAOimp in getSelectedClient() : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ClientWoTableDto> getAllClientForWoTable() {
        String sql = "select Client_id,Client_name,Client_type from client_details where Client_id!=0 and Client_id!=1";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "clientWoTableMapping");
        List<ClientWoTableDto> result = query.getResultList();
        return result;
    }

    @Override
    public List<ClientDetails> getAllClientOrderByName() {
        ArrayList<ClientDetails> clientList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ClientDetails.class);
            criteria.add(Restrictions.eq("enabled", true));
            criteria.add(Restrictions.and(Restrictions.ne("clientid", 0),Restrictions.ne("clientid", 1)));
            criteria.add(Restrictions.ne("status", ClientDetails.Status.DELETE));
            criteria.addOrder(Order.asc("clientname"));
            clientList = (ArrayList<ClientDetails>) criteria.list();
            return clientList;
        } catch (Exception e) {
            logger.debug("Exception in ClientDetailDAOimp in getAllClientOrderByName() : {} ", e.getMessage());
            throw e;
        }
    }

}
