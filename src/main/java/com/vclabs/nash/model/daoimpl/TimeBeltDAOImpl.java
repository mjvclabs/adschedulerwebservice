/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.TimeBeltDAO;

import com.vclabs.nash.model.entity.TimeBelts;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author user
 */
@Repository
public class TimeBeltDAOImpl extends VCLSessionFactory implements TimeBeltDAO {

    @Override
    @Transactional
    public List<TimeBelts> getTimeBelts(int _channelId) {
        List<TimeBelts> timeBelt = new ArrayList<TimeBelts>();
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            Criteria criteria = session.createCriteria(TimeBelts.class);
            criteria.add(Restrictions.eq("channelID", _channelId));
            timeBelt = (List<TimeBelts>) criteria.list();
        } catch (Exception e) {
            throw e;
        }
        return timeBelt;
    }

    @Override
    public boolean getScheduleGenerated(int _channelId, int _hour, int period) {
        TimeBelts timeBelt = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeBelts.class);
            criteria.add(Restrictions.eq("channelID", _channelId));
            criteria.add(Restrictions.eq("hour", _hour));
            criteria.add(Restrictions.eq("period", period));
            timeBelt = (TimeBelts) criteria.uniqueResult();
        } catch (Exception e) {
            throw e;
        }
        return timeBelt.getIsScheduleGenerated();
    }

    @Override
    @Transactional
    public boolean updateTimeBelt(TimeBelts _timebelt) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(_timebelt);
            session.flush();
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public TimeBelts getTimeBelt(int _channelId, int _hour, int period) {
        TimeBelts timeBelt = null;

        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeBelts.class);
            criteria.add(Restrictions.eq("channelID", _channelId));
            criteria.add(Restrictions.eq("hour", _hour));
            criteria.add(Restrictions.eq("period", period));
            Object tmp = criteria.uniqueResult();
            if (tmp != null) {
                timeBelt = (TimeBelts) tmp;
            }
        } catch (Exception e) {
            return null;
        }
        return timeBelt;
    }

    @Override
    public boolean saveTimeBelt(TimeBelts _model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(_model);
            session.flush();
        } catch (Exception e) {
            throw e;
        }
        return true;
    }

    @Override
    public TimeBelts getTimeBeltbyID(int _id) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeBelts.class);
            criteria.add(Restrictions.eq("timeBeltId", _id));
            return (TimeBelts) criteria.uniqueResult();
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    @Override
    public List<TimeBelts> getTimeBelts(int _channelId, Date _startTime, Date _endTime) {
        List<TimeBelts> timeBelt = new ArrayList<TimeBelts>();

        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeBelts.class);
            criteria.add(Restrictions.eq("channelID", _channelId));
            criteria.add(Restrictions.ge("startTime", _startTime));
            criteria.add(Restrictions.le("endTime", _endTime));
            timeBelt = (List<TimeBelts>) criteria.list();
        } catch (Exception e) {
            throw e;
        }
        return timeBelt;
    }
}
