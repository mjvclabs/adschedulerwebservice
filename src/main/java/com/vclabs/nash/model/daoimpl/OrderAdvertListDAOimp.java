/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.OrderAdvertListDAO;

import com.vclabs.nash.model.entity.OrderAdvertList;

/**
 *
 * @author user
 */
@Repository
public class OrderAdvertListDAOimp extends VCLSessionFactory implements OrderAdvertListDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderAdvertListDAOimp.class);

    @Override
    public List<OrderAdvertList> getOrderAdvertList(int workOrderId) {
        List<OrderAdvertList> advertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(OrderAdvertList.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.createAlias("advertid", "advertId");
            criteria.add(Restrictions.ne("advertId.status", 3));
            //criteria.addOrder(Order.asc("advertId.advertname"));
            criteria.addOrder(Order.desc("advertId.advertid"));
            advertList = (ArrayList<OrderAdvertList>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }

    @Override
    public List<OrderAdvertList> getAllOrderAdvertList() {
        List<OrderAdvertList> orderAdvertList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(OrderAdvertList.class);
            criteria.createAlias("advertid", "advertId");
            criteria.add(Restrictions.eq("advertId.Enabled", 1));
            orderAdvertList = (ArrayList<OrderAdvertList>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
        return orderAdvertList;
    }

    @Override
    public Boolean setOrderAdvertList(OrderAdvertList model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean upadteOrderAdvertList(OrderAdvertList model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteOrderAdvertList(OrderAdvertList model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public OrderAdvertList getSelectedOrderAdvertList(int orderadvertlistid) {
        OrderAdvertList orderAdvert = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(OrderAdvertList.class);
            criteria.createAlias("advertid", "advertId");
            criteria.add(Restrictions.eq("advertId.Enabled", 1));
            criteria.add(Restrictions.eq("orderadvertlistid", orderadvertlistid));
            orderAdvert = (OrderAdvertList) criteria.uniqueResult();
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
        return orderAdvert;
    }

    @Override
    public int getSelectedOrderAdvertCount(int workOrderId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long channelCount = 0;
            Criteria criteria = session.createCriteria(OrderAdvertList.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.createAlias("advertid", "advertId");
            criteria.add(Restrictions.eq("advertId.Enabled", 1));
            criteria.setProjection(Projections.rowCount());
            channelCount = (long) criteria.uniqueResult();
            session.flush();
            return Integer.parseInt("" + channelCount);
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<OrderAdvertList> getWorkOrderIdList(int advertId) {
        List<OrderAdvertList> advertList = null;
        try {
            Session session =  this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(OrderAdvertList.class);
            criteria.createAlias("advertid", "advertId");
            criteria.add(Restrictions.eq("advertId.advertid", advertId));
            criteria.addOrder(Order.desc("advertId.advertid"));
            advertList = (ArrayList<OrderAdvertList>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in OrderAdvertListDAOimp : {}", e.getMessage());
            throw e;
        }
        return advertList;
    }
}
