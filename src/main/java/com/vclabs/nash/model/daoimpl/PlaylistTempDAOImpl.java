package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.PlaylistTempDAO;
import com.vclabs.nash.model.entity.playlist_preview.PlayListTemp;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dperera on 20/10/2019.
 */
@Repository
public class PlaylistTempDAOImpl  extends VCLSessionFactory  implements PlaylistTempDAO{

    @Override
    public PlayListTemp getPlayList(int playListId) {
        PlayListTemp playList = null;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(PlayListTemp.class);
            criteria.add(Restrictions.eq("id", playListId));
            playList = (PlayListTemp) criteria.uniqueResult();
        } finally {
            session.close();
        }
        return playList;
    }

    @Override
    public PlayListTemp save(PlayListTemp playListTemp) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.saveOrUpdate(playListTemp);
        session.flush();
        return playListTemp;
    }

    @Override
    public List<PlayListTemp> getAdvertPlayList(int channelId, Date date, Date startTime, Date endTime, int hour, int sequenceNum) {
        List<PlayListTemp> ret_playList;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(PlayListTemp.class);
            criteria.add(Restrictions.eq("date", date));
            criteria.add(Restrictions.eq("sequenceNumber", sequenceNum));
            criteria.add(Restrictions.eq("scheduleHour", hour));
            criteria.add(Restrictions.gt("scheduleEndTime", startTime));
            criteria.add(Restrictions.lt("scheduleStartTime", endTime));
            criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.eq("channel.channelid", channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.eq("aid.adverttype", "ADVERT"));
            ret_playList = (List<PlayListTemp>) criteria.list();
        } finally {
            session.close();
        }
        return ret_playList;
    }

    @Override
    public List<PlayListTemp> getPlayListLogo(int _channelId, Date date, Date start_time, Date end_time, int _hour, int sequenceNum) {
        List<PlayListTemp> ret_playList;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(PlayListTemp.class);
            criteria.add(Restrictions.eq("sequenceNumber", sequenceNum));
            criteria.add(Restrictions.eq("date", date));
            criteria.add(Restrictions.gt("scheduleEndTime", start_time));
            criteria.add(Restrictions.lt("scheduleStartTime", end_time));
            criteria.add(Restrictions.and(Restrictions.ne("status", "9"), Restrictions.ne("status", "6"), Restrictions.ne("status", "8")));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.eq("aid.adverttype", "LOGO"));
            ret_playList = (List<PlayListTemp>) criteria.list();
        }  finally {
            session.close();
        }
        return ret_playList;
    }

    @Override
    public List<PlayListTemp> getPlayListCrawler(int _channelId, Date date, Date start_time, Date end_time, int _hour, int sequenceNum) {
        List<PlayListTemp> ret_playList;
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();;
            Criteria criteria = session.createCriteria(PlayListTemp.class);
            criteria.add(Restrictions.eq("sequenceNumber", sequenceNum));
            criteria.add(Restrictions.eq("date", date));
            criteria.add(Restrictions.eq("scheduleHour", _hour));
            criteria.add(Restrictions.gt("scheduleEndTime", start_time));
            criteria.add(Restrictions.lt("scheduleStartTime", end_time));
            criteria.add(Restrictions.eq("status", "0"));
            criteria.add(Restrictions.eq("channel.channelid", _channelId));
            criteria.createAlias("advert", "aid");
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq("aid.adverttype", "CRAWLER"))
                    .add(Restrictions.eq("aid.adverttype", "V_SHAPE"))
                    .add(Restrictions.eq("aid.adverttype", "L_SHAPE")));
            ret_playList = (List<PlayListTemp>) criteria.list();

        } finally {
            session.close();
        }
        return ret_playList;
    }


    @Override
    public List<PlayListTemp> getSchedulesByDate(int channelId, Date date, int sequenceNumber) {

        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(PlayListTemp.class);

        criteria.add(Restrictions.eq("sequenceNumber", sequenceNumber));
        criteria.add(Restrictions.ne("comment", "filler_generated"));
        criteria.add(Restrictions.ne("status", "6"));
        criteria.add(Restrictions.eq("channel.channelid",channelId));

        criteria.add(Restrictions.and(Restrictions.eq("date",date)));
        criteria.addOrder(Order.asc("scheduleHour"));
        criteria.addOrder(Order.asc("playCluster"));
        criteria.addOrder(Order.asc("playOrder"));
        criteria.addOrder(Order.desc("scheduleStartTime"));
        criteria.addOrder(Order.desc("scheduleEndTime"));
        return (ArrayList<PlayListTemp>) criteria.list();

    }
}
