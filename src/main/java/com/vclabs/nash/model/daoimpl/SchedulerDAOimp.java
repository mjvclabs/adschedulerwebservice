/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import java.math.BigInteger;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;

import java.util.*;
import java.util.logging.Level;

import com.vclabs.nash.dashboard.dto.DailyMissedSpotsDto;
import com.vclabs.nash.dashboard.dto.HightTrafficChannelsWidgetDto;
import com.vclabs.nash.model.dao.TimeSlotScheduleMapDAO;
import com.vclabs.nash.model.dto.MissedSpotMapDto;
import com.vclabs.nash.model.dto.RescheduleTimeBelt;
import com.vclabs.nash.model.dto.ScheduleAdvertDto;
import com.vclabs.nash.model.dto.TimeSlotScheduleMapDto;
import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.model.process.MissedSpotCountInfo;
import com.vclabs.nash.service.vo.WOChannelSummary;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.*;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.process.OneDayScheduleInfo;

import com.vclabs.nash.model.process.ScheduleAdminViewInfo;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author user
 */
@Repository
public class SchedulerDAOimp extends VCLSessionFactory implements SchedulerDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerDAOimp.class);
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private TimeSlotScheduleMapDAO timeSlotScheduleMapDAO;

    @Override
    public List<ScheduleDef> getScheduleList() {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getScheduleList: {}", e.getMessage());
            throw e;
        }
        return scheduleList;
    }

    @Override
    public List<ScheduleDef> getScheduleListOrderByASC(int workOrderId) {
        //LOGGER.debug("SchedulerDAO: List<ScheduleDef> getScheduleListOrderByASC(int workOrderId): {}", workOrderId);
        Session session = this.getSessionFactory().openSession();
        try{
            Query query = session.createNativeQuery("select * from schedule_def sdd where sdd.Schedule_id in (select Schedule_id from schedule_def sd left join work_order wo on sd.Work_order_id = wo.Work_order_id\n" +
                    " where sd.Status != 6 AND wo.Work_order_id = :workOrderId)  \n" +
                    " order by sdd.Channel_id, sdd.Schedule_start_time, sdd.Schedule_end_time, sdd.Advert_id, sdd.Date", ScheduleDef.class);
            query.setParameter("workOrderId", workOrderId);
            List<ScheduleDef> resultList = query.getResultList();
            return resultList;
        }catch (Exception e){
            LOGGER.debug("Exception in getScheduleListOrderByASC: {}", e.getMessage());
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<ScheduleAdvertDto> getScheduleList(int workOrderId) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select sdd.Schedule_id as scheduleId , sdd.Status as status, ad.Advert_type as advertType, ad.Duration as duration " +
                " from schedule_def sdd left join advertisement_def ad on sdd.Advert_id = ad.Advert_id " +
                " where sdd.Schedule_id in (select Schedule_id from schedule_def sd left join work_order wo on sd.Work_order_id = wo.Work_order_id where sd.Status != 6 AND wo.Work_order_id = ?1)";
        Query query = session.createNativeQuery(sqlString, "scheduleAdvertMapping");
        query.setParameter(1, workOrderId);
        return query.getResultList();
    }

    @Override
    public List<ScheduleDef> getMissedSpot(Date fromDate, Date toDate, int channelId, int advertId) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("workorderid", "woId");
            criteria.add(Restrictions.ne("woId.permissionstatus", 3));
            criteria.add(Restrictions.and(Restrictions.ge("date", fromDate), Restrictions.le("date", toDate)));
            criteria.add(Restrictions.or(Restrictions.eq("status", "5"), Restrictions.eq("status", "8"), Restrictions.eq("status", "9")));
            criteria.createAlias("channelid", "channel");
            criteria.add(Restrictions.eq("channel.enabled", true));
            if (channelId != -111) {
                criteria.add(Restrictions.eq("channel.channelid", channelId));
            }
            criteria.createAlias("advertid", "advert");
            if (advertId != -999) {
                criteria.add(Restrictions.eq("advert.advertid", advertId));
            }
            criteria.addOrder(Order.asc("channelid"));
            criteria.addOrder(Order.asc("schedulestarttime"));
            criteria.addOrder(Order.asc("scheduleendtime"));
            criteria.addOrder(Order.asc("advertid"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getMissedSpot: {}", e.getMessage());
            throw e;
        }
        return scheduleList;
    }

    @Override
    public List<MissedSpotMapDto> getMissedSpotBySQL(Date fromDate, Date toDate, int channelId, int advertId) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {

            String sqlString = "select \n" +
                    "scd.Schedule_id,\n" +
                    "scd.Actual_start_time as schActualStartTime,\n" +
                    "(select start_time from vcl_schedule_db.time_belts where channel_id = scd.Channel_id and hour = DATE_FORMAT(scd.Schedule_start_time, \"%H\")) as 'actualStartTime',\n" +
                    "(select end_time from vcl_schedule_db.time_belts where channel_id = scd.Channel_id and hour = DATE_FORMAT(scd.Schedule_start_time, \"%H\")) as 'actualEndTime',\n" +
                    "scd.Date,\n" +
                    "scd.Status,\n" +
                    "scd.Schedule_start_time,\n" +
                    "scd.Schedule_end_time,\n" +
                    "case when scd.Priority_id IS NULL then -1 else scd.Priority_id end as Priority_id,\n" +
                    "case when scd.Priority_id IS NULL then -1 else (select cluster from vcl_schedule_db.priority_def where priority_id = scd.Priority_id) end as priorityCluster,\n" +
                    "case when scd.Priority_id IS NULL then -1 else (select priority from vcl_schedule_db.priority_def where priority_id = scd.Priority_id) end as priorityNumber,\n" +
                    "scd.Comment,\n" +
                    "wrk.Work_order_id,\n" +
                    "wrk.Order_name,\n" +
                    "wrk.End_date,\n" +
                    "cli.Client_name,\n" +
                    "cli.Client_id,\n" +
                    "adv.Advert_name,\n" +
                    "adv.Duration,\n" +
                    "adv.Advert_id,\n" +
                    "adv.Advert_type,\n" +
                    "chd.Channel_id,\n" +
                    "chd.Channel_name\n" +
                    "FROM schedule_def scd \n" +
                    "left join work_order wrk on  scd.Work_order_id = wrk.Work_order_id\n" +
                    "left join channel_detail chd on scd.Channel_id = chd.Channel_id\n" +
                    "left join client_details cli on wrk.Client = cli.Client_id\n" +
                    "left join advertisement_def adv on scd.Advert_id = adv.Advert_id\n" +
                    "where (scd.Status=5 or scd.Status=8 or scd.Status=9 or scd.Status=11) and wrk.Permission_status !=3 and wrk.Work_order_id !=0 and chd.Enabled = true and scd. Date between ?1 and ?2";

            if (channelId != -111) {
                sqlString +=" and scd.Channel_id = ?3";
            }
            if (advertId != -999) {
                sqlString +=" and scd.Advert_id = ?4";
            }
            sqlString += " order by scd.Channel_id,scd.Schedule_start_time,scd.Schedule_end_time, scd.Advert_id asc;";
            Query query = session.createNativeQuery(sqlString, "missesSpotViewMapping");
            query.setParameter(1, fromDate);
            query.setParameter(2, toDate);
            if (channelId != -111) {
                query.setParameter(3, channelId);
            }
            if (advertId != -999) {
                query.setParameter(4, advertId);
            }
            return query.getResultList();
        } catch (Exception e) {
            LOGGER.debug("Invalid date inputs : {}", e.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public List<ScheduleDef> getAllScheduleInWorkOrder(int workOrderId) {

        List<ScheduleDef> scheduleList = null;

        Session session = null;
        try {
            session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.addOrder(Order.asc("status"));
            criteria.addOrder(Order.asc("date"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            try {
                throw e;
            } catch (Exception ex) {
                LOGGER.debug("Exception in getAllScheduleInWorkOrder: {}", e.getMessage());
                throw ex;
            }
        }

        return scheduleList;
    }

    @Override
    public List<ScheduleDef> getSelectedAdvertSpotByWorkored(int workOderID, int advertId) {
        List<ScheduleDef> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOderID));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertId));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedAdvertSpotByWorkored: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> selectedScheduleFromDate(int workOrderId, Date scheduleDate) {
        List<ScheduleDef> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.eq("date", scheduleDate));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in selectedScheduleFromDate: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> selectedScheduleFromDate(Date scheduleDate) {
        List<ScheduleDef> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("date", scheduleDate));
            criteria.addOrder(Order.asc("channelid"));
            criteria.addOrder(Order.asc("schedulestarttime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in selectedScheduleFromDate: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public List<TimeSlotScheduleMap> getTimeBeltMapFromDate(int workOrderId, Date scheduleDate) {
        List<TimeSlotScheduleMap> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("workOrderId", workOrderId));
            criteria.add(Restrictions.eq("date", scheduleDate));
            scheduleList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getTimeBeltMapFromDate: {}", e.getMessage());
            throw e;
        }
        return scheduleList;
    }

    //for Jira issue NRM-813
//    @Override
//    @Transactional
//    public List<ScheduleDef> getScheduleFromDate(int workOrderId, Date scheduleDate) {
//        List<ScheduleDef> scheduleList = new ArrayList<>();
//        try {
//            Session session = this.getSessionFactory().getCurrentSession();
//            Criteria criteria = session.createCriteria(ScheduleDef.class);
//            criteria.add(Restrictions.eq("workOrderId", workOrderId));
//            criteria.add(Restrictions.eq("date", scheduleDate));
//            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
//        } catch (Exception e) {
//            LOGGER.debug("Exception in getScheduleFromDate: {}", e.getMessage());
//            throw e;
//        }
//        return scheduleList;
//    }
//
//    @Override
//    public List<ScheduleDef> getScheduleFromDate(Date scheduleDate) {
//        List<ScheduleDef> scheduleList = new ArrayList<>();
//        Session session = this.getSessionFactory().openSession();
//        try {
//            Criteria criteria = session.createCriteria(ScheduleDef.class);
//            Transaction tx = session.beginTransaction();
//            criteria.add(Restrictions.eq("date", scheduleDate));
//            criteria.addOrder(Order.asc("channelid.channelid"));
//            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
//            tx.commit();
//        } catch (Exception e) {
//            LOGGER.debug("Exception in getScheduleFromDate : {}", e.getMessage());
//            throw e;
//        } finally {
//            session.close();
//        }
//        return scheduleList;
//    }
    //end

    @Override
    public List<TimeSlotScheduleMap> findByDateAndTimeBelts(Date date, List<TimeBelts> timeBelts) {
        String sql = "select * from time_slot_schedule_map where date = ?1 and Time_belt_id in ?2 ";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, TimeSlotScheduleMap.class);
        query.setParameter(1, date);
        query.setParameter(2, timeBelts);
        return query.getResultList();
    }

    @Override
    public List<TimeSlotScheduleMap> getTimeBeltMapFromDate(Date scheduleDate) {
        List<TimeSlotScheduleMap> scheduleList = new ArrayList<>();
        Session session = this.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            Transaction tx = session.beginTransaction();
            criteria.add(Restrictions.eq("date", scheduleDate));
            criteria.addOrder(Order.asc("channelid.channelid"));
            criteria.addOrder(Order.asc("timeslotid.timeBeltId"));
            scheduleList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
            tx.commit();
        } catch (Exception e) {
            LOGGER.debug("Exception in getTimeBeltMapFromDate : {}", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
        return scheduleList;
    }

    @Override
    public Boolean saveSchedule(ScheduleDef model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            session.flush();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in saveSchedule: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public Boolean updateSchedule(ScheduleDef model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            session.flush();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in updateSchedule: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean updateSchedule(List<ScheduleDef> models) {
        try (Session session = this.getSessionFactory().openSession()){
            Transaction tx = session.beginTransaction();
            for(ScheduleDef sd: models) {
                session.update(sd);
            }
            tx.commit();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in updateSchedule: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteSchedule(ScheduleDef model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in deleteSchedule: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public ScheduleDef getSelectedSchedule(int scheduleId) {
        Session session = this.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("scheduleid", scheduleId));
            ScheduleDef model = (ScheduleDef) criteria.uniqueResult();
            return model;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedSchedule: {}", e.getMessage());
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public ScheduleDef getMaximumSpotInAdvert(int AdvertId) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            DetachedCriteria maxQuery = DetachedCriteria.forClass(ScheduleDef.class);
            maxQuery.createAlias("advertid", "advert");
            maxQuery.add(Restrictions.eq("advert.advertid", AdvertId));
            maxQuery.setProjection(Projections.max("date"));

            Criteria criteria = session.createCriteria(ScheduleDef.class);

            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", AdvertId));
            criteria.add(Property.forName("date").eq(maxQuery));

            scheduleList = (List<ScheduleDef>) criteria.list();

            if (scheduleList == null) {
                return null;
            } else if (scheduleList.isEmpty()) {
                return null;
            } else {
                return scheduleList.get(0);
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in getMaximumSpotInAdvert: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public ScheduleDef getMaximumSpotInClient(int clientId) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            DetachedCriteria maxQuery = DetachedCriteria.forClass(ScheduleDef.class);
            maxQuery.createAlias("advertid", "advert");
            maxQuery.createAlias("advert.client", "cnt");
            maxQuery.add(Restrictions.eq("cnt.clientid", clientId));
            maxQuery.setProjection(Projections.max("date"));

            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("advertid", "advert");
            criteria.createAlias("advert.client", "cnt");
            criteria.add(Restrictions.eq("cnt.clientid", clientId));
            criteria.add(Property.forName("date").eq(maxQuery));
            scheduleList = (List<ScheduleDef>) criteria.list();
            if (scheduleList == null) {
                return null;
            } else if (scheduleList.isEmpty()) {
                return null;
            } else {
                return scheduleList.get(0);
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in getMaximumSpotInClient: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public int getSelectedScheduleCount(int workOrderId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long schedulCount = 0;
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.setProjection(Projections.rowCount());
            schedulCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + schedulCount);
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedScheduleCount: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public int getSelectedChannelSlotCount(int workOrderId, int channelId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long slotCount = 0;
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.eq("channelid.channelid", channelId));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.setProjection(Projections.rowCount());
            slotCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + slotCount);
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedChannelSlotCount: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public int getSelectedWorkOrderAirSpot(int workOrderId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long slotCount = 0;
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.eq("status", "2"));
            criteria.setProjection(Projections.rowCount());
            slotCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + slotCount);
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedWorkOrderAirSpot: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getAirSpot(int workOrderId) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long slotCount = 0;
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.eq("status", "2"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getAirSpot: {}", e.getMessage());
            throw e;
        }
    }

    //--------------------------TimeSlot---------------------------
    @Override
    public List<TimeSlot> getTimeSlot() {
        ArrayList<TimeSlot> timeSlotList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeSlot.class);
            timeSlotList = (ArrayList<TimeSlot>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getTimeSlot: {}", e.getMessage());
            throw e;
        }
        return timeSlotList;
    }

    @Override
    public Boolean saveTimeSlotMap(TimeSlotScheduleMap model) {
        try {
            TimeSlotScheduleMap timeSlotScheduleMap = timeSlotScheduleMapDAO.getSelectedTimeSlot(model.getScheduleid().getScheduleid());
            if(timeSlotScheduleMap != null){
                return true;
            }
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in saveTimeSlotMap: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean saveTimeSlotMapV2(TimeSlotScheduleMap model) {
        Session session = this.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction(); // For transaction issue
        try {
//            Transaction tx = session.beginTransaction();
            LOGGER.info("Before save or update saveTimeSlotMap -> Schedule ID : {} | WorkOrder ID : {}", model.getScheduleid().getScheduleid(), model.getWorkOrderId());
            session.saveOrUpdate(model);
            LOGGER.info("After save or update saveTimeSlotMap -> Schedule ID : {} | WorkOrder ID : {}", model.getScheduleid().getScheduleid(), model.getWorkOrderId());
            session.flush();
            tx.commit();
            return true;
        } catch (Exception e) {
            if(e instanceof ConstraintViolationException) {
                LOGGER.debug("Tried to save already exist schedule : {}", model.getScheduleid());
            }
            if(tx != null){ // For transaction issue
                tx.rollback();
            }
            LOGGER.debug("Exception in saveTimeSlotMapV2: {}", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Boolean updateTimeSlotMap(TimeSlotScheduleMap model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in updateTimeSlotMap: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteTimeSlotMap(TimeSlotScheduleMap model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            LOGGER.debug("Exception in deleteTimeSlotMap: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<TimeSlotScheduleMapDto> getFinalAllScheduleV2(ScheduleAdminViewInfo model){
        Session session = this.getSessionFactory().getCurrentSession();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date fromDate = dateFormat.parse(model.getStartDate());
            Date toDate = dateFormat.parse(model.getEndDate());

            String sqlString = "select tsmap.Schedule_id, tsmap.time_belt_id, tsmap.Date from time_slot_schedule_map tsmap left join schedule_def sd on tsmap.Schedule_id = sd.Schedule_id " +
                    "left join advertisement_def ad on ad.Advert_id = sd.Advert_id " +
                    "where tsmap.Date >= ?1 and tsmap.Date < ?2 and (sd.Status != 6 or sd.Status != 9) and ad.Advert_type = 'ADVERT' ";

            if (model.getChannelId() != -111) {
                sqlString += "and tsmap.Channel_id = ?3 ";
            }
            if (model.getWorkOrderId() != -111) {
                sqlString += "and tsmap.workOrderId = ?4 ";
            }
            if (model.getAdvertId() != -111) {
                sqlString += "and ad.Advert_id = ?5 ";
            }
            sqlString += "order by tsmap.Time_belt_id asc, tsmap.Date asc";

            Query query = session.createNativeQuery(sqlString, "TimeSlotScheduleMapping"); //"TimeSlotScheduleMapping"
            query.setParameter(1, fromDate);
            query.setParameter(2, toDate);
            //set other extra parameters
            if (model.getChannelId() != -111) {
                query.setParameter(3, model.getChannelId());
            }
            if (model.getWorkOrderId() != -111) {
                query.setParameter(4, model.getWorkOrderId());
            }
            if (model.getAdvertId() != -111) {
                query.setParameter(5, model.getAdvertId());
            }
            return query.getResultList();
        } catch (ParseException e) {
            LOGGER.debug("Invalid date inputs : {}", e.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public List<TimeSlotScheduleMap> getSchedulesByIds(List<Integer> ids) {
        String sqlString = "select * from time_slot_schedule_map where Schedule_id in ?1 order by Time_belt_id";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sqlString, TimeSlotScheduleMap.class);
        query.setParameter(1, ids);
        return query.getResultList();
    }

    @Override
    public List<TimeSlotScheduleMap> getFinalAllSchedule(ScheduleAdminViewInfo model) {
        List<TimeSlotScheduleMap> scheduleMapList = new ArrayList<>();

        try {
            Session session = this.getSessionFactory().getCurrentSession();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateOne = dateFormat.parse(model.getStartDate());
            Date dateTwo = dateFormat.parse(model.getEndDate());

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.ge("date", dateOne));
            criteria.add(Restrictions.le("date", dateTwo));

            criteria.createAlias("scheduleid", "sid");
            criteria.add(Restrictions.ne("sid.status", "6"));
            criteria.add(Restrictions.ne("sid.status", "9"));

            criteria.createAlias("sid.advertid", "adid");
            criteria.add(Restrictions.eq("adid.adverttype", "ADVERT"));

            if (model.getChannelId() != -111) {
                criteria.add(Restrictions.eq("channelid.channelid", model.getChannelId()));
            }

            if (model.getWorkOrderId() != -111) {
                criteria.add(Restrictions.eq("workOrderId", model.getWorkOrderId()));
            }

            if (model.getAdvertId() != -111) {
                criteria.add(Restrictions.eq("adid.advertid", model.getAdvertId()));
            }

            criteria.addOrder(Order.asc("timeslotid.timeBeltId"));
            criteria.addOrder(Order.asc("date"));
            scheduleMapList = new ArrayList<TimeSlotScheduleMap>();
            List tmp = criteria.list();
            if (tmp != null) {
                scheduleMapList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
            }
            return scheduleMapList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getFinalAllSchedule: {}", e.getMessage());
        }
        return scheduleMapList;
    }

    @Override
    public List<TimeSlotScheduleMap> getSelectedDateSchedule(Date selectDate, int timeBelt, ScheduleAdminViewInfo model) {

        List<TimeSlotScheduleMap> scheduleMapList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("date", selectDate));
            criteria.add(Restrictions.eq("timeslotid.timeBeltId", timeBelt));
            criteria.createAlias("scheduleid", "sid");
            criteria.add(Restrictions.ne("sid.status", "6"));
            criteria.add(Restrictions.ne("sid.status", "9"));
            criteria.addOrder(Order.asc("sid.actualstarttime"));

            criteria.createAlias("sid.advertid", "adid");
            criteria.add(Restrictions.eq("adid.adverttype", "ADVERT"));

            if (model.getChannelId() != -111) {
                criteria.add(Restrictions.eq("channelid.channelid", model.getChannelId()));
            }

            if (model.getWorkOrderId() != -111) {
                criteria.add(Restrictions.eq("workOrderId", model.getWorkOrderId()));
            }

            if (model.getAdvertId() != -111) {
                criteria.add(Restrictions.eq("adid.advertid", model.getAdvertId()));
            }

            scheduleMapList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
            return scheduleMapList;

        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedDateSchedule: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<TimeSlotScheduleMap> getSelectedDateSchedule(Date selectDate) {

        List<TimeSlotScheduleMap> scheduleMapList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("date", selectDate));
            criteria.createAlias("scheduleid", "sid");
            criteria.add(Restrictions.ne("sid.status", "6"));
            criteria.add(Restrictions.ne("sid.status", "9"));
            scheduleMapList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
            return scheduleMapList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedDateSchedule: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<TimeSlotScheduleMap> getSelectedTimeSlotSchedule(int workOrderId) {

        List<TimeSlotScheduleMap> scheduleMapList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("workOrderId", workOrderId));
            scheduleMapList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
            return scheduleMapList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedTimeSlotSchedule: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> allChannelsAllWorkOrders(Date fromDate, Date toDate) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.add(Restrictions.and(Restrictions.ge("date", fromDate), Restrictions.le("date", toDate)));
            criteria.addOrder(Order.asc("channelid.channelid"));
            criteria.addOrder(Order.asc("date"));
            criteria.addOrder(Order.asc("actualstarttime"));
            criteria.addOrder(Order.asc("actualendtime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in allChannelsAllWorkOrders: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> allChannelsAllWorkOrders_v2(Date fromDate, Date toDate) {
      /* String sqlString = "select * from schedule_def sd right join\n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast from vcl_schedule_db.schedule_def where (Date between ?1 and ?2) and (Status = 5 or Status = 8)\n" +
                "union\n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast from schedule_def where (Date between ?3 and ?4) and Status !=6 and Status != 5 and Status != 8 and Status != 9\n" +
                "order by Channel_id asc, Date asc, ast asc) as a on sd.Schedule_id = a.sid;";*/

        String sqlString = "select * from schedule_def sd right join \n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast, Schedule_start_time as sst from vcl_schedule_db.schedule_def \n" +
                "where (Date between ?1 and ?2) and (Status = 5 or Status = 8)\n" +
                "union \n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast, Schedule_start_time as sst from schedule_def \n" +
                "where (Date between ?3 and ?4) and Status not in (5, 8, 9, 0)\n" +//remove 6
                "union\n" +
                "select scd.Channel_id, scd.Schedule_id as sid, scd.Date, DATE_FORMAT(tmb.start_time, \"%H:%i:%s\") as ast, Schedule_start_time as sst from schedule_def scd \n" +
                "left join  time_slot_schedule_map tsm on scd.Schedule_id = tsm.Schedule_id\n" +
                "left join  time_belts tmb on tmb.time_belt_id = tsm.Time_belt_id\n" +
                "where (scd.Date between ?3 and ?4) and scd.Status = 0\n" +
                "order by Channel_id asc, Date asc, sst asc, ast asc) as a on sd.Schedule_id = a.sid;";

        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sqlString, ScheduleDef.class);
        query.setParameter(1, fromDate);
        query.setParameter(2, toDate);
        query.setParameter(3, fromDate);
        query.setParameter(4, toDate);
        return query.getResultList();
    }

    @Override
    public List<ScheduleDef> allChannelsNotAllWorkOrders(int workOrder, Date fromDate, Date toDate) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrder));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.add(Restrictions.and(Restrictions.ge("date", fromDate), Restrictions.le("date", toDate)));
            criteria.addOrder(Order.asc("channelid.channelid"));
            criteria.addOrder(Order.asc("date"));
            criteria.addOrder(Order.asc("actualstarttime"));
//            criteria.addOrder(Order.asc("schedulestarttime"));
//            criteria.addOrder(Order.asc("scheduleendtime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in allChannelsNotAllWorkOrders: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> allChannelsNotAllWorkOrders_v2(int workOrder, Date fromDate, Date toDate) {
        /*String sqlString = "select * from schedule_def sd right join\n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast from schedule_def where Work_order_id= ?1 and  (Date between ?2 and ?3) and (Status = 5 or Status = 8) \n" +
                "union\n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast from schedule_def where Work_order_id = ?4 and (Date between ?5 and ?6) and Status !=6 and Status != 5 and Status != 8 and Status != 9 \n" +
                "order by Channel_id asc,Date asc, ast asc) as a on sd.Schedule_id = a.sid;";*/

        String sqlString="select * from schedule_def sd right join \n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast, Schedule_start_time as sst from vcl_schedule_db.schedule_def \n" +
                "where Work_order_id= ?1 and (Date between ?2 and ?3) and (Status = 5 or Status = 8)\n" +
                "union \n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast, Schedule_start_time as sst from schedule_def \n" +
                "where Work_order_id= ?4 and (Date between ?5 and ?6) and Status not in (5, 6, 8, 9, 0)\n" +
                "union\n" +
                "select scd.Channel_id, scd.Schedule_id as sid, scd.Date, DATE_FORMAT(tmb.start_time, \"%H:%i:%s\") as ast, Schedule_start_time as sst from schedule_def scd \n" +
                "left join  time_slot_schedule_map tsm on scd.Schedule_id = tsm.Schedule_id\n" +
                "left join  time_belts tmb on tmb.time_belt_id = tsm.Time_belt_id\n" +
                "where scd.Work_order_id= ?4 and (scd.Date between ?5 and ?6) and scd.Status = 0\n" +
                "order by Channel_id asc, Date asc, sst asc, ast asc) as a on sd.Schedule_id = a.sid;";

        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sqlString, ScheduleDef.class);
        query.setParameter(1, workOrder);
        query.setParameter(2, fromDate);
        query.setParameter(3, toDate);
        query.setParameter(4, workOrder);
        query.setParameter(5, fromDate);
        query.setParameter(6, toDate);
        return query.getResultList();
    }

    @Override
    public List<ScheduleDef> notAllChannelsAllWorkOrders(int i_channelId, Date fromDate, Date toDate) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("channelid.channelid", i_channelId));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.add(Restrictions.and(Restrictions.ge("date", fromDate), Restrictions.le("date", toDate)));
            criteria.addOrder(Order.asc("channelid.channelid"));
            criteria.addOrder(Order.asc("date"));
            criteria.addOrder(Order.asc("actualstarttime"));
            criteria.addOrder(Order.asc("actualendtime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in notAllChannelsAllWorkOrders: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> notAllChannelsAllWorkOrders_v2(int i_channelId, Date fromDate, Date toDate) {
        /*String sqlString = "select * from schedule_def sd right join\n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast from schedule_def where Channel_id= ?1 and  (Date between ?2 and ?3) and (Status = 5 or Status = 8) \n" +
                "union\n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast from schedule_def where Channel_id = ?4 and (Date between ?5 and ?6) and Status !=6 and Status != 5 and Status != 8 and Status != 9 \n" +
                "order by Channel_id asc, Date asc, ast asc) as a on sd.Schedule_id = a.sid;";*/

        String sqlString="select * from schedule_def sd right join \n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast, Schedule_start_time as sst from vcl_schedule_db.schedule_def \n" +
                "where Channel_id= ?1 and (Date between ?2 and ?3) and (Status = 5 or Status = 8)\n" +
                "union \n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast, Schedule_start_time as sst from schedule_def \n" +
                "where Channel_id= ?4 and (Date between ?5 and ?6) and Status not in (5, 6, 8, 9, 0)\n" +
                "union\n" +
                "select scd.Channel_id, scd.Schedule_id as sid, scd.Date, DATE_FORMAT(tmb.start_time, \"%H:%i:%s\") as ast, Schedule_start_time as sst from schedule_def scd \n" +
                "left join  time_slot_schedule_map tsm on scd.Schedule_id = tsm.Schedule_id\n" +
                "left join  time_belts tmb on tmb.time_belt_id = tsm.Time_belt_id\n" +
                "where scd.Channel_id= ?4 and (scd.Date between ?5 and ?6) and scd.Status = 0\n" +
                "order by Channel_id asc, Date asc, sst asc, ast asc) as a on sd.Schedule_id = a.sid;";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sqlString, ScheduleDef.class);
        query.setParameter(1, i_channelId);
        query.setParameter(2, fromDate);
        query.setParameter(3, toDate);
        query.setParameter(4, i_channelId);
        query.setParameter(5, fromDate);
        query.setParameter(6, toDate);
        return query.getResultList();
    }

    @Override
    public List<ScheduleDef> notAllChannelsNotAllWorkOrders(int i_channelId, int i_workOrderId, Date fromDate, Date toDate) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", i_workOrderId));
            criteria.add(Restrictions.eq("channelid.channelid", i_channelId));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.add(Restrictions.and(Restrictions.ge("date", fromDate), Restrictions.le("date", toDate)));
            criteria.addOrder(Order.asc("channelid.channelid"));
            criteria.addOrder(Order.asc("date"));
            criteria.addOrder(Order.asc("actualstarttime"));
            criteria.addOrder(Order.asc("actualendtime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();

            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in notAllChannelsNotAllWorkOrders: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> notAllChannelsNotAllWorkOrders_v2(int i_channelId, int i_workOrderId, Date fromDate, Date toDate) {
        /*String sqlString = "select * from schedule_def sd right join\n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast from schedule_def where Channel_id= ?1 and Work_order_id= ?2 and (Date between ?3 and ?4) and (Status = 5 or Status = 8) \n" +
                "union\n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast from schedule_def where Channel_id = ?5 and Work_order_id= ?6 and(Date between ?7 and ?8) and Status !=6 and Status != 5 and Status != 8 and Status != 9 \n" +
                "order by Channel_id asc, Date asc, ast asc) as a on sd.Schedule_id = a.sid;";*/
        String sqlString="select * from schedule_def sd right join \n" +
                "(select Channel_id, Schedule_id as sid, Date, Actual_end_time as ast, Schedule_start_time as sst from vcl_schedule_db.schedule_def \n" +
                "where Channel_id= ?1 and Work_order_id= ?2 and (Date between ?3 and ?4) and (Status = 5 or Status = 8)\n" +
                "union \n" +
                "select Channel_id, Schedule_id as sid, Date, Actual_start_time as ast, Schedule_start_time as sst from schedule_def \n" +
                "where Channel_id= ?5 and Work_order_id= ?6 and (Date between ?7 and ?8) and Status not in (5, 6, 8, 9, 0)\n" +
                "union\n" +
                "select scd.Channel_id, scd.Schedule_id as sid, scd.Date, DATE_FORMAT(tmb.start_time, \"%H:%i:%s\") as ast, Schedule_start_time as sst from schedule_def scd \n" +
                "left join  time_slot_schedule_map tsm on scd.Schedule_id = tsm.Schedule_id\n" +
                "left join  time_belts tmb on tmb.time_belt_id = tsm.Time_belt_id\n" +
                "where scd.Channel_id= ?5 and scd.Work_order_id= ?6 and (scd.Date between ?7 and ?8) and scd.Status = 0\n" +
                "order by Channel_id asc, Date asc, sst asc, ast asc) as a on sd.Schedule_id = a.sid;";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sqlString, ScheduleDef.class);
        query.setParameter(1, i_channelId);
        query.setParameter(2, i_workOrderId);
        query.setParameter(3, fromDate);
        query.setParameter(4, toDate);
        query.setParameter(5, i_channelId);
        query.setParameter(6, i_workOrderId);
        query.setParameter(7, fromDate);
        query.setParameter(8, toDate);
        return query.getResultList();
    }

    @Override
    public List<List<ScheduleDef>> getAllSchedulesBetweenDates(int i_channelId, Date fromDate, Date toDate, Date dtStartTime, String type) {
        List<List<ScheduleDef>> retList = new ArrayList<>();

        LocalDate start = fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate end = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Session session = this.getSessionFactory().openSession();
        try {
            for (LocalDate date = start; (date.isBefore(end) || date.isEqual(end)); date = date.plusDays(1)) {
                List<ScheduleDef> scheduleList = null;

                Criteria criteria = session.createCriteria(ScheduleDef.class);
                criteria.add(Restrictions.eq("channelid.channelid", i_channelId));
                criteria.add(Restrictions.and(Restrictions.ne("status", "6"), (Restrictions.ne("status", "9"))));
                // criteria.add(Restrictions.isNull("priority"));

                criteria.createAlias("advertid", "aid");
                if (type.equals("Advert")) {
                    criteria.add(Restrictions.eq("aid.adverttype", "ADVERT"));
                } else if (type.equals("Crawler")) {
                    criteria.add(Restrictions.disjunction()
                            .add(Restrictions.eq("aid.adverttype", "CRAWLER"))
                            .add(Restrictions.eq("aid.adverttype", "V_SHAPE"))
                            .add(Restrictions.eq("aid.adverttype", "L_SHAPE")));
                } else if (type.equals("Logo")) {
                    criteria.add(Restrictions.eq("aid.adverttype", "LOGO"));
                } else if (type.equals("Advert_Crawler")) {
                    criteria.add(Restrictions.disjunction()
                            .add(Restrictions.eq("aid.adverttype", "ADVERT"))
                            .add(Restrictions.eq("aid.adverttype", "CRAWLER"))
                            .add(Restrictions.eq("aid.adverttype", "V_SHAPE"))
                            .add(Restrictions.eq("aid.adverttype", "L_SHAPE")));
                } else if (type.equals("All")) {
                    criteria.add(Restrictions.disjunction()
                            .add(Restrictions.eq("aid.adverttype", "ADVERT"))
                            .add(Restrictions.eq("aid.adverttype", "LOGO"))
                            .add(Restrictions.eq("aid.adverttype", "CRAWLER"))
                            .add(Restrictions.eq("aid.adverttype", "V_SHAPE"))
                            .add(Restrictions.eq("aid.adverttype", "L_SHAPE")));
                }

                criteria.add(Restrictions.eq("date", Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant())));

                if (date.equals(start)) {
                    criteria.add(Restrictions.ge("schedulestarttime", dtStartTime));
                }

                scheduleList = new ArrayList<ScheduleDef>();
                if (criteria.list() != null) {
                    scheduleList = (ArrayList<ScheduleDef>) criteria.list();
                }
                retList.add(scheduleList);
            }

        } catch (Exception e) {
            LOGGER.debug("Exception in getAllSchedulesBetweenDates: {}", e.getMessage());
            throw e;
        }finally {
            session.close();
        }
        return retList;
    }

    @Override
    public List<TimeSlotScheduleMap> getOneDaySchedules(OneDayScheduleInfo scheduleInfo) {
        List<TimeSlotScheduleMap> scheduleMapList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("date", scheduleInfo.getDate()));
            criteria.createAlias("scheduleid", "sid");
            criteria.add(Restrictions.ne("sid.status", "6"));
            //criteria.add(Restrictions.ne("sid.status", "9"));

            if (scheduleInfo.getChannelId() != -111) {
                criteria.add(Restrictions.eq("channelid.channelid", scheduleInfo.getChannelId()));
            }

            if (scheduleInfo.getWorkOrderId() != -111) {
                criteria.add(Restrictions.eq("workOrderId", scheduleInfo.getWorkOrderId()));
            }

            if ((scheduleInfo.getAdvertId() != -111) || !scheduleInfo.getAdvertType().equals("-111")) {
                criteria.createAlias("sid.advertid", "advert");
            }

            if (scheduleInfo.getAdvertId() != -111) {
                criteria.add(Restrictions.eq("advert.advertid", scheduleInfo.getAdvertId()));
            }

            if (!scheduleInfo.getAdvertType().equals("-111")) {
                criteria.add(Restrictions.eq("advert.adverttype", scheduleInfo.getAdvertType()));
            }

            if (!scheduleInfo.getAdvertType().equals("-111")) {
                criteria.add(Restrictions.eq("advert.adverttype", scheduleInfo.getAdvertType()));
            }

            if (scheduleInfo.getEndHour() != -111 && scheduleInfo.getStartHour() != -111) {
                criteria.createAlias("timeslotid", "slotId");
                criteria.add(Restrictions.and(Restrictions.ge("slotId.hour", scheduleInfo.getStartHour()), (Restrictions.le("slotId.hour", scheduleInfo.getEndHour()))));
            }

            criteria.addOrder(Order.asc("timeslotid.timeBeltId"));
            scheduleMapList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
            return scheduleMapList;
        } catch (Exception e) {
            LOGGER.debug("Exception in SchedulerDAOimp: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getScheduleListFromAdvertAndWorkOrde(int workOrderId, int advertID) {

        List<ScheduleDef> schedulList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertID));
            schedulList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getOneDaySchedules: {}", e.getMessage());
            throw e;
        }
        return schedulList;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<TimeSlotScheduleMap> getSchedulesbyTimebeltAdvert(int workOrderID, int timeBeltId, int advertId, Date startDate, Date enddate) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.eq("timeslotid.timeBeltId", timeBeltId));
            criteria.createAlias("scheduleid", "schedule");
            criteria.add(Restrictions.eq("schedule.advertid.advertid", advertId));
            criteria.add(Restrictions.isNull("schedule.priority"));
            criteria.add(Restrictions.eq("workOrderId", workOrderID));
            criteria.add(Restrictions.between("date", startDate, enddate));
            criteria.add(Restrictions.ne("schedule.status", "6"));

            List<TimeSlotScheduleMap> schedulList = (List<TimeSlotScheduleMap>) criteria.list();
            return schedulList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSchedulesbyTimebeltAdvert: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getSchedulePriorityMapList(PriorityDef priorityDf, Date dtStartDate, Date dtEndDate) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("priority", priorityDf));
            criteria.add(Restrictions.between("date", dtStartDate, dtEndDate));
            criteria.addOrder(Order.asc("date"));

            List<ScheduleDef> scheduleList = (List<ScheduleDef>) criteria.list();
            return scheduleList;

        } catch (Exception e) {
            LOGGER.debug("Exception in getSchedulePriorityMapList: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<TimeSlotScheduleMap> getSchedulePriorityMapListbyTimeBelt(int timeBeltId, int cluster, int priority, int workOrderID, int advertId, Date dtStartDate, Date dtEndDate, int ichannelID) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.createAlias("scheduleid", "schedule");

            criteria.add(Restrictions.isNotNull("schedule.priority"));
            criteria.createAlias("schedule.priority", "priority");

            criteria.add(Restrictions.between("date", dtStartDate, dtEndDate));

            if (ichannelID != -1) {
                criteria.add(Restrictions.eq("schedule.channelid.channelid", ichannelID));
            }

            if (timeBeltId != -111) {
                criteria.add(Restrictions.eq("timeslotid.timeBeltId", timeBeltId));
            }
            if (cluster != -111) {
                criteria.add(Restrictions.eq("priority.cluster", cluster));
            }
            if (priority != -1) {
                criteria.add(Restrictions.eq("priority.priority", priority));
            }
            if (advertId != -1) {
                criteria.add(Restrictions.eq("schedule.advertid.advertid", advertId));
            }
            if (workOrderID != -1) {
                criteria.add(Restrictions.eq("workOrderId", workOrderID));
            }

            List<TimeSlotScheduleMap> schedulList = (List<TimeSlotScheduleMap>) criteria.list();
            return schedulList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSchedulePriorityMapListbyTimeBelt: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getSchedulePrioritybyTimeBelt(int timeBeltId, Date date) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.createAlias("scheduleid", "schedule");
            criteria.add(Restrictions.isNotNull("schedule.priority"));
            criteria.add(Restrictions.eq("timeslotid.timeBeltId", timeBeltId));
            criteria.add(Restrictions.eq("date", date));
            List<TimeSlotScheduleMap> schedulList = (List<TimeSlotScheduleMap>) criteria.list();
            List<ScheduleDef> returnList = new ArrayList<>();
            for (TimeSlotScheduleMap item : schedulList) {
                returnList.add(item.getScheduleid());
            }
            return returnList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSchedulePrioritybyTimeBelt: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    @Transactional
    public List<ScheduleDef> getScheduleListFromAdvertAndNotScheduleAndMissed(int advertID) {
        List<ScheduleDef> schedulList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertID));
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq("status", "0"))
                    .add(Restrictions.eq("status", "1"))
                    .add(Restrictions.eq("status", "3"))
                    .add(Restrictions.eq("status", "7")));
            schedulList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getScheduleListFromAdvertAndNotScheduleAndMissed: {}", e.getMessage());
            throw e;
        }

        return schedulList;
    }

    @Override
    public List<ScheduleDef> getRamainingSpot(int workOrderId, Date toDay) {
        List<ScheduleDef> scheduleList = null;
        try {

            Session session = this.getSessionFactory().getCurrentSession();
            //Greater than or equal(ge)
            //Equal(eq)
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.ge("date", toDay));
            criteria.add(Restrictions.eq("status", "0"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getRamainingSpot: {}", e.getMessage());
            throw e;
        }
        return scheduleList;
    }

    @Override
    public List<ScheduleDef> getRamainingSpot_v2(int workOrderId, Date toDay) {
        List<ScheduleDef> scheduleList = null;
        try {

            Session session = this.getSessionFactory().getCurrentSession();
            //Greater than or equal(ge)
            //Equal(eq)
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));

            //Get only ADVERT (TVC) media
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.adverttype", "ADVERT"));

            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getRamainingSpot_v2: {}", e.getMessage());
            throw e;
        }
        return scheduleList;
    }

    @Override
    public List<ScheduleDef> getAllSpotOrderByDate(int workOrderId, int duration, int channelID) {
        List<ScheduleDef> scheduleList = null;

        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.add(Restrictions.eq("channelid.channelid", channelID));
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.duration", duration));
            criteria.addOrder(Order.asc("date"));
            criteria.addOrder(Order.asc("actualstarttime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getAllSpotOrderByDate: {}", e.getMessage());
            throw e;
        }
        return scheduleList;
    }

    //Active spots are gave according to advetID
    @Override
    @Transactional
    public List<ScheduleDef> getScheduledSpotByAdvertID(int advertID) {
        List<ScheduleDef> schedulList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertID));
            criteria.add(Restrictions.eq("status", "0"));
            schedulList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getScheduledSpotByAdvertID: {}", e.getMessage());
            throw e;
        }
        return schedulList;
    }

    //Suspend advert are gave according to advertID
    @Override
    public List<ScheduleDef> getSuspendByAdvertID(int advertID) {
        List<ScheduleDef> schedulList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            //Greater than or equal(ge)
            //Greater (gt)
            //Equal (eq)
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertID));
            criteria.add(Restrictions.eq("status", "9"));
            criteria.add(Restrictions.eq("comment", "Advertisement Suspend"));
            schedulList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getSuspendByAdvertID: {}", e.getMessage());
            throw e;
        }
        return schedulList;
    }

    @Override
    public List<ScheduleDef> getRemaimingSpotSelectedAdvert(int workOrderId, int advertID) {
        List<ScheduleDef> scheduleList = null;
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date toDay = dateFormat.parse(dateFormat.format(new Date()));
            Session session = this.getSessionFactory().getCurrentSession();
            //Greater than or equal(ge)
            //Equal(eq)
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.ge("date", toDay));

            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertID));
            criteria.add(Restrictions.eq("advert.adverttype", "ADVERT"));

            criteria.add(Restrictions.and((Restrictions.ne("status", "6")), (Restrictions.ne("status", "9"))));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            try {
                throw e;
            } catch (Exception ex) {
                LOGGER.debug("Exception in getRemaimingSpotSelectedAdvert: {}", e.getMessage());
            }
        }

        return scheduleList;
    }

    @Override
    public List<ScheduleDef> getAvailabilitySelectedAdvert(int workOrderId, int advertID) {
        List<ScheduleDef> scheduleList = null;
        try {

            Session session = this.getSessionFactory().getCurrentSession();
            //Greater than or equal(ge)
            //Equal(eq)
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));

            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertID));
            criteria.add(Restrictions.eq("advert.adverttype", "ADVERT"));

            criteria.add(Restrictions.and((Restrictions.ne("status", "6")), (Restrictions.ne("status", "9"))));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            try {
                throw e;
            } catch (Exception ex) {
                LOGGER.debug("Exception in getAvailabilitySelectedAdvert: {}", e.getMessage());
            }
        }
        return scheduleList;
    }

    @Override
    public WorkOrderChannelList  getWorkOrderAvailableSpotCount(int workOrderID) { //tested
        Session session = null;
        try {
            session = this.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("advertid", "advert");

            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.groupProperty("advert.adverttype"))
                    .add(Projections.rowCount());
            criteria.setProjection(projectionList);

            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderID));
            criteria.add(Restrictions.and(Restrictions.ne("status", "6"), (Restrictions.ne("status", "9"))));

            List<Object[]> scheduleList = criteria.list();
            tx.commit();

            /// get tvc breakdown /////////////////////////////////////////////////
            Transaction tx2 = session.beginTransaction();
            Criteria criteria2 = session.createCriteria(ScheduleDef.class);
            criteria2.createAlias("advertid", "advert");

            ProjectionList projectionList2 = Projections.projectionList();
            projectionList2.add(Projections.groupProperty("advert.duration"))
                    .add(Projections.rowCount());
            criteria2.setProjection(projectionList2);

            criteria2.add(Restrictions.eq("advert.adverttype", "ADVERT"));
            criteria2.add(Restrictions.eq("workorderid.workorderid", workOrderID));
            criteria2.add(Restrictions.and(Restrictions.ne("status", "6"), (Restrictions.ne("status", "9"))));

            List<Object[]> breakdownList = criteria2.list();
            tx2.commit();

            WorkOrderChannelList retObject = new WorkOrderChannelList();
            for (Object[] item : scheduleList) {
                switch (item[0].toString()) {
                    case "ADVERT":
                        retObject.setNumbertofspot(Integer.parseInt(item[1].toString()));
                        break;
                    case "LOGO":
                        retObject.setLogospots(Integer.parseInt(item[1].toString()));
                        break;
                    case "CRAWLER":
                        retObject.setCrowlerspots(Integer.parseInt(item[1].toString()));
                        break;
                    case "V_SHAPE":
                        retObject.setVsqeezspots(Integer.parseInt(item[1].toString()));
                        break;
                    case "L_SHAPE":
                        retObject.setLsqeezespots(Integer.parseInt(item[1].toString()));
                        break;
                }
            }

            for (Object[] item : breakdownList) {
                TVCDurationSpotCount tvcSpot = new TVCDurationSpotCount();
                tvcSpot.setDuration(Integer.parseInt(item[0].toString()));
                tvcSpot.setSpotCount(Integer.parseInt(item[1].toString()));
                retObject.addTvcSpot(tvcSpot);
            }

            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in getWorkOrderAvailableSpotCount: {}", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override  ///tvc spot count isn't in 30 second spots count
    public WorkOrderChannelList getWorkOrderAvailableSpotCount(int workOrderID, int channelID) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("advertid", "advert");
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.groupProperty("advert.adverttype"))
                    .add(Projections.groupProperty("advert.duration"))
                    .add(Projections.rowCount());
            criteria.setProjection(projectionList);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderID));
            criteria.add(Restrictions.eq("channelid.channelid", channelID));
            criteria.add(Restrictions.and(Restrictions.ne("status", "6"), (Restrictions.ne("status", "9"))));

            List<Object[]> scheduleList = criteria.list();
            WorkOrderChannelList retObject = new WorkOrderChannelList();
            for (Object[] item : scheduleList) {
                switch (item[0].toString()) {
                    case "ADVERT": {
                        retObject.setNumbertofspot(retObject.getNumbertofspot() + Integer.parseInt(item[2].toString()));
                        TVCDurationSpotCount tvcSpot = new TVCDurationSpotCount();
                        tvcSpot.setDuration(Integer.parseInt(item[1].toString()));
                        tvcSpot.setSpotCount(Integer.parseInt(item[2].toString()));
                        retObject.addTvcSpot(tvcSpot);
                        break;
                    }
                    case "LOGO":
                        retObject.setLogospots(retObject.getLogospots() + Integer.parseInt(item[2].toString()));
                        break;
                    case "CRAWLER":
                        retObject.setCrowlerspots(retObject.getCrowlerspots() + Integer.parseInt(item[2].toString()));
                        break;
                    case "V_SHAPE":
                        retObject.setVsqeezspots(retObject.getVsqeezspots() + Integer.parseInt(item[2].toString()));
                        break;
                    case "L_SHAPE":
                        retObject.setLsqeezespots(retObject.getLsqeezespots() + Integer.parseInt(item[2].toString()));
                        break;
                }
            }
            return retObject;
        } catch (Exception e) {
            LOGGER.debug("Exception in getWorkOrderAvailableSpotCount: {}", e.getMessage());
            throw e;
        }
    }

    public List<ScheduleDef> getTodaySchedule(int channelId) {
        List<ScheduleDef> scheduleList = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date today = dateFormat.parse(dateFormat.format(new Date()));

            Session session = this.getSessionFactory().getCurrentSession();
            //Greater than or equal(ge)
            //Equal(eq)
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("channelid.channelid", channelId));
            criteria.add(Restrictions.eq("date", new Date()));

            criteria.add(Restrictions.or((Restrictions.ne("status", "6")), (Restrictions.ne("status", "9"))));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getTodaySchedule: {}", e.getMessage());
        }
        return scheduleList;
    }

    @Override
    public List<List<ScheduleDef>> getAllSchedulesBetweenDates(int i_channelId, Date fromDate, Date toDate, Date dtStartTime, int advertId) {
        List<List<ScheduleDef>> retList = new ArrayList<>();
        try {
            LocalDate start = fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate end = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            for (LocalDate date = start; (date.isBefore(end) || date.isEqual(end)); date = date.plusDays(1)) {
                List<ScheduleDef> scheduleList = null;
                Session session = this.getSessionFactory().getCurrentSession();
                Criteria criteria = session.createCriteria(ScheduleDef.class);
                criteria.add(Restrictions.eq("channelid.channelid", i_channelId));
                criteria.add(Restrictions.and(Restrictions.ne("status", "6")));//, (Restrictions.ne("status", "9"))
                criteria.add(Restrictions.eq("advertid.advertid", advertId));

                criteria.add(Restrictions.eq("date", Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant())));

                if (date.equals(start)) {
                    criteria.add(Restrictions.ge("schedulestarttime", dtStartTime));
                }

                scheduleList = new ArrayList<ScheduleDef>();
                if (criteria.list() != null) {
                    scheduleList = (ArrayList<ScheduleDef>) criteria.list();
                }
                retList.add(scheduleList);
            }
            return retList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getAllSchedulesBetweenDates: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getAllSchedulesBetweenDates(int workOrder, Date fromDate, Date toDate, Date dtStartTime) {
        List<ScheduleDef> retList = new ArrayList<>();
        try {
            LocalDate start = fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate end = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            for (LocalDate date = start.plusDays(0); (date.isBefore(end) || date.isEqual(end)); date = date.plusDays(1)) {
                List<ScheduleDef> scheduleList = null;
                Session session = this.getSessionFactory().getCurrentSession();
                Criteria criteria = session.createCriteria(ScheduleDef.class);
                criteria.add(Restrictions.eq("workorderid.workorderid", workOrder));
                criteria.add(Restrictions.ne("status", "6"));
                criteria.add(Restrictions.eq("date", Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant())));
                if (date.isEqual(start)) {
                    criteria.add(Restrictions.ge("schedulestarttime", dtStartTime));
                }
                scheduleList = new ArrayList<ScheduleDef>();
                if (criteria.list() != null) {
                    scheduleList = (ArrayList<ScheduleDef>) criteria.list();
                }
                retList.addAll(scheduleList);
            }
        } catch (Exception e) {
            LOGGER.debug("Exception in getAllSchedulesBetweenDates(int workOrder, Date fromDate, Date toDate, Date dtStartTime): {}", e.getMessage());
            throw e;
        }
        return retList;
    }

    @Override
    public List<ScheduleDef> getAllSchedulesByIds(List<Integer> ids) {
        String sqlString = "select * from schedule_def where Schedule_id in ?1 ";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sqlString, ScheduleDef.class);
        query.setParameter(1, ids);
        return query.getResultList();
    }

    @Override
    public int getSelectedWorkOrderValidSpot(int workOrderId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long slotCount = 0;
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.ne("status", "9"));
            criteria.setProjection(Projections.rowCount());
            slotCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + slotCount);
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedWorkOrderValidSpot: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getTodaySchedule(int channelId, Date date) {
        List<ScheduleDef> scheduleList = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date selectedDate = dateFormat.parse(dateFormat.format(date));

            Session session = this.getSessionFactory().getCurrentSession();
            //Greater than or equal(ge)
            //Equal(eq)
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("channelid.channelid", channelId));
            criteria.add(Restrictions.eq("date", selectedDate));

            criteria.add(Restrictions.or((Restrictions.ne("status", "6")), (Restrictions.ne("status", "9"))));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getTodaySchedule(int channelId, Date date) {}", e.getMessage());
        }
        return scheduleList;
    }

    @Override
    public int getSelectedAdvertScheduleCount(int workOrderId, int advertId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long slotCount = 0;

            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.eq("advertid.advertid", advertId));
            criteria.add(Restrictions.ne("status", "6"));
            criteria.setProjection(Projections.rowCount());
            slotCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + slotCount);
        } catch (Exception e) {
            LOGGER.debug("Exception in getSelectedAdvertScheduleCount: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getCommercialAvailability(int channelId, Date fromDate, Date toDate) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(ScheduleDef.class);
            if (channelId != -99) {
                criteria.add(Restrictions.eq("channelid.channelid", channelId));
            }
            criteria.add(Restrictions.ne("status", "6"));
            criteria.add(Restrictions.and(Restrictions.ge("date", fromDate), Restrictions.le("date", toDate)));
            criteria.addOrder(Order.asc("channelid.channelid"));
            criteria.addOrder(Order.asc("date"));
            criteria.addOrder(Order.asc("actualstarttime"));
            criteria.addOrder(Order.asc("actualendtime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();

            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getCommercialAvailability: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getScheduleAnalysisData(List<Integer> workOrdersIDs, List<Integer> agencyIDs, int clientId) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(ScheduleDef.class);

            //criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            if (!workOrdersIDs.isEmpty()) {
                criteria.add(Property.forName("workorderid.workorderid").in(workOrdersIDs));
            }
            criteria.add(Restrictions.ne("status", "6"));
            criteria.createAlias("workorderid", "woid");
            if (!agencyIDs.isEmpty()) {
                criteria.add(Property.forName("woid.agencyclient").in(agencyIDs));
            }
            if (clientId != -111) {
                criteria.add(Restrictions.eq("woid.client", clientId));
            }
            criteria.addOrder(Order.asc("channelid.channelid"));
            criteria.addOrder(Order.asc("date"));
            criteria.addOrder(Order.asc("actualstarttime"));
            criteria.addOrder(Order.asc("actualendtime"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();

            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getScheduleAnalysisData: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public ScheduleDef getLastAiredSpot(int advertID) {
        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("advertid", "advert");
            criteria.add(Restrictions.eq("advert.advertid", advertID));
            criteria.addOrder(Order.desc("date"));
            criteria.addOrder(Order.desc("actualstarttime"));
            criteria.setMaxResults(1);
            return (ScheduleDef) criteria.uniqueResult();
        } catch (Exception e) {
            LOGGER.debug("Exception in getLastAiredSpot: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ScheduleDef> getSuspendWorkOrderSpot(int workOderID) {

        List<ScheduleDef> scheduleList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOderID));
            //criteria.add(Restrictions.ge("date", toDay));
            criteria.add(Restrictions.eq("status", "6"));
            criteria.add(Restrictions.eq("comment", " Work order Hold"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getSuspendWorkOrderSpot: {}", e.getMessage());
            throw e;
        }
        return scheduleList;
    }

    @Override
    public List<ScheduleDef> getMissedSpotSchedule(MissedSpotCountInfo filterData) {

        List<ScheduleDef> scheduleList = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.createAlias("workorderid", "woId");
            criteria.add(Restrictions.ne("woId.permissionstatus", 3));
            criteria.add(Restrictions.and(Restrictions.ge("date", dateFormat.parse(filterData.getStartDate())), Restrictions.le("date", dateFormat.parse(filterData.getEndDate()))));
            criteria.add(Restrictions.or(Restrictions.eq("status", "5"), Restrictions.eq("status", "8"), Restrictions.eq("status", "9"), Restrictions.eq("status", "11")));
            criteria.createAlias("channelid", "channel");
            criteria.add(Restrictions.eq("channel.enabled", true));
            if (!filterData.getChannelIds().isEmpty()) {
                criteria.add(Property.forName("channel.channelid").in(filterData.getChannelList()));
            }
            if (!filterData.getWoIdList().isEmpty()) {
                criteria.add(Property.forName("woId.workorderid").in(filterData.getWoIdList()));
            }
            if (!filterData.getSpotStatus().equals("-1")) {
                criteria.add(Restrictions.eq("status", filterData.getSpotStatus()));
            }

            criteria.createAlias("advertid", "advert");
            if (!filterData.getAdvertisement().equals("")) {
                criteria.add(Restrictions.like("advert.advertname", filterData.getAdvertisement(), MatchMode.ANYWHERE));
            }
            if (!filterData.getAdvertType().equals("-111")) {
                criteria.add(Restrictions.eq("advert.adverttype", filterData.getAdvertType()));
            }

            criteria.addOrder(Order.asc("channelid"));
            criteria.addOrder(Order.asc("schedulestarttime"));
            criteria.addOrder(Order.asc("scheduleendtime"));
            criteria.addOrder(Order.asc("date"));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();

            session.flush();
        } catch (Exception e) {
            LOGGER.debug("Exception in getMissedSpotSchedule {}", e.getMessage());
        }
        return scheduleList;
    }

    @Override
    public List<TimeSlotScheduleMap> getTimeSlotForMissedSpot(MissedSpotCountInfo filterData) {
        List<TimeSlotScheduleMap> scheduleMapList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(TimeSlotScheduleMap.class);
            criteria.add(Restrictions.and(Restrictions.ge("date", dateFormat.parse(filterData.getStartDate())), Restrictions.le("date", dateFormat.parse(filterData.getEndDate()))));
            criteria.createAlias("scheduleid", "sid");
            criteria.add(Restrictions.or(Restrictions.eq("sid.status", "5"), Restrictions.eq("sid.status", "8"), Restrictions.eq("sid.status", "9"), Restrictions.eq("sid.status", "11")));

            criteria.createAlias("channelid", "chnId");
            criteria.add(Restrictions.eq("chnId.enabled", true));
            if (!filterData.getChannelIds().isEmpty()) {
                criteria.add(Property.forName("chnId.channelid").in(filterData.getChannelList()));
            }

            if (!filterData.getWoIdList().isEmpty()) {
                criteria.add(Property.forName("workOrderId").in(filterData.getWoIdList()));
            }

            criteria.createAlias("sid.advertid", "advert");
            if (!filterData.getAdvertisement().equals("")) {
                criteria.add(Restrictions.like("advert.advertname", filterData.getAdvertisement(), MatchMode.ANYWHERE));
            }
            if (!filterData.getAdvertType().equals("-111")) {
                criteria.add(Restrictions.eq("advert.adverttype", filterData.getAdvertType()));
            }

            criteria.addOrder(Order.asc("timeslotid.timeBeltId"));
            scheduleMapList = (ArrayList<TimeSlotScheduleMap>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in getTimeSlotForMissedSpot: {}", e.getMessage());
        }
        return scheduleMapList;
    }

    @Override
    public int getPrimeTimeSpotCount(int workOrdersId) {

        String sql = String.format("select count(*) from schedule_def sc left join time_slot_schedule_map tssm on sc.Schedule_id=tssm.Schedule_id left join time_belts tb on tssm.Time_belt_id=tb.time_belt_id where sc.Work_order_id=%s and sc.Status!='6' and tb.start_time >='1970-01-01 18:00:00' and tb.start_time < '1970-01-01 23:00:00'", "" + workOrdersId);
        Session session = this.getSessionFactory().getCurrentSession();
        SQLQuery query = session.createNativeQuery(sql);
        BigInteger primeTimeSpo = (BigInteger) query.getSingleResult();
        return primeTimeSpo.intValue();
    }

    @Override
    public int getNonPrimeTimeSpotCount(int workOrdersId) {
        String sql = String.format("select count(*) from schedule_def sc left join time_slot_schedule_map tssm on sc.Schedule_id=tssm.Schedule_id left join time_belts tb on tssm.Time_belt_id=tb.time_belt_id where sc.Work_order_id=%s and sc.Status!='6' and tb.start_time >='1970-01-01 09:00:00' and tb.start_time < '1970-01-01 18:00:00'", "" + workOrdersId);
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        BigInteger nonPrimeTimeSpo = (BigInteger) query.getSingleResult();
        return nonPrimeTimeSpo.intValue();
    }
    /*----------------------------------Dashboard core---------------------------*/
    @Override
    public List<ScheduleDef> findCurrentDateAllValidSpot() {
        //SET CURRENT DATE AS DATE PARAMETAR THIS VALUE FOR THE ONLY TESTING
        //dateFormat.format(new Date())
        String sql = String.format("select * from schedule_def sd left join work_order wo on sd.Work_order_id = wo.Work_order_id " +
                "left join channel_detail cd on sd.Channel_id = cd.Channel_id where sd.Date=%s and sd.status!='9' and sd.status!='6' and sd.status!='8' and wo.Permission_status !=3 and wo.Permission_status !=4 and cd.Enabled=1;",  "'"+dateFormat.format(new Date())+"'");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, ScheduleDef.class);
        return query.getResultList();
    }

    @Override
    public List<ScheduleDef> findCurrentDateSpotByStatus(Date today,int status) {
        //SET CURRENT DATE AS DATE PARAMETAR THIS VALUE FOR THE ONLY TESTING

        String sql = String.format("select * from schedule_def sd left join work_order wo on sd.Work_order_id = wo.Work_order_id left join channel_detail cd on sd.Channel_id = cd.Channel_id where sd.Date=%s and sd.status='%d' and wo.Permission_status !=3 and wo.Permission_status !=4 and cd.Enabled=1;", "'"+dateFormat.format(new Date())+"'",status);
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, ScheduleDef.class);
        return query.getResultList();
    }

    @Override
    public List<ScheduleDef> findCurrentDateSpotByStatusAndChannel(Date today, int status, int channelId) {
        String sql = String.format("select * from schedule_def sd left join work_order wo on sd.Work_order_id = wo.Work_order_id left join channel_detail cd on sd.Channel_id = cd.Channel_id where sd.Date='%s' and sd.status='%d' and cd.Channel_id='%d' and wo.Permission_status !=3 and wo.Permission_status !=4 and cd.Enabled=1;", dateFormat.format(today), status, channelId);
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, ScheduleDef.class);
        return query.getResultList();
    }

    @Override
    public List<ScheduleDef> findAllMissedOrAiredSpots(Date today, int channelId) {
        try {
            String sql = String.format("select * from schedule_def sd left join work_order wo" +
                            " on wo.Work_order_id = sd.Work_order_id where sd.Date=%s and sd.Channel_id =%d " +
                            "and (sd.Status=5 or sd.Status=2) and wo.Permission_status != 3;","'"+ today +"'" , channelId);
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, ScheduleDef.class);
            List<ScheduleDef> scheduleDefList = query.getResultList();
            return scheduleDefList;
        }catch (Exception e){
            LOGGER.debug("Exception in SchedulerDAOimp findAllMissedOrAiredSpots: {}", e.getMessage());
            return null;
        }
    }

    @Override
    public List<ScheduleDef> findAllScheduleSpots(Date today) {
        try {
            String sql = String.format("select * from schedule_def sd left join work_order wo " +
                            "on wo.Work_order_id = sd.Work_order_id where sd.Date='%s' " +
                            "and (sd.Status!=8 and sd.Status!=2 and sd.Status!=9 and sd.Status!=6) " +
                            "and wo.Permission_status != 3;",
                    dateFormat.format(today));
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, ScheduleDef.class);
            List<ScheduleDef> scheduleDefList = query.getResultList();
            return scheduleDefList;
        }catch (Exception e){
            LOGGER.debug("Exception in SchedulerDAOimp findAllScheduleSpots: {}", e.getMessage());
            return null;
        }
    }

    /*****************Dashboard High Traffic Channel Widget*************/
    @Override
    public List<HightTrafficChannelsWidgetDto> findHighTrafficChannelsOfCurrentHour() {
        try {
            Date currentDate = new Date();
            DateUtils.truncate(new Date(), java.util.Calendar.DAY_OF_MONTH);
            String sql = String.format("select count(*) as spotCount, tb.channel_id as channelId, cd.Channel_name as channelName from time_belts tb left join time_slot_schedule_map tssm on tb.time_belt_id = tssm.Time_belt_id " +
                    "left join schedule_def sd on sd.Schedule_id = tssm.Schedule_id left join channel_detail cd on cd.Channel_id = tb.channel_id where tssm.Date = %s and " +
                    "(sd.Status!=8 and sd.Status!=9 and sd.Status!=6) group by tb.channel_id order by count(*) DESC","'"+dateFormat.format(currentDate)+"'");
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, "hightTrafficChannelsWidgetMapping");
            List<HightTrafficChannelsWidgetDto> channelList = query.getResultList();
            return channelList;
        }catch (Exception e){
            LOGGER.debug("Exception in SchedulerDAOimp findHighTrafficChannelsOfCurrentHour: {}", e.getMessage());
            return null;
        }
    }

    @Override
    public List<DailyMissedSpotsDto> findDailyMissedSpotsPercentages() {
        try {
            Date currentDate = new Date();
            DateUtils.truncate(new Date(), java.util.Calendar.DAY_OF_MONTH);
            String sql = String.format("select cd.Channel_name as channelName, count(*) as percentage from schedule_def sd \n" +
                    "left join work_order wo " +
                    "on wo.Work_order_id = sd.Work_order_id " +
                    "left join channel_detail cd " +
                    "on sd.Channel_id = cd.Channel_id " +
                    "where sd.Date=%s and sd.Status=5 " +
                    "and wo.Permission_status != 3 group by cd.Channel_id", "'"+dateFormat.format(currentDate)+"'");

            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, "dailyMissedSpotsMapping");
            List<DailyMissedSpotsDto> channelList = query.getResultList();
            return channelList;
        }catch (Exception e){
            LOGGER.debug("Exception in SchedulerDAOimp findDailyMissedSpotsPercentages: {}", e.getMessage());
            return null;
        }
    }

    @Override
    public List<ScheduleDef> getSuspendScheduleByWorkOrder(int workOrderId) {
        List<ScheduleDef> scheduleList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ScheduleDef.class);
            criteria.add(Restrictions.eq("workorderid.workorderid", workOrderId));
            criteria.add(Restrictions.eq("status", "6"));
            criteria.add(Restrictions.like("comment", "Work order Hold", MatchMode.ANYWHERE));
            scheduleList = (ArrayList<ScheduleDef>) criteria.list();
            return scheduleList;
        } catch (Exception e) {
            LOGGER.debug("Exception in getSuspendScheduleByWorkOrder: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public void deleteTimeSlotScheduleMapByScheduleId(List<Integer> scheduleIds) {
        Session session = this.getSessionFactory().openSession();
        try {
            Transaction tx = session.beginTransaction();
            String sqlString = "delete from time_slot_schedule_map where Schedule_id in ?1 ";
            Query query = session.createNativeQuery(sqlString);
            query.setParameter(1, scheduleIds);
            query.executeUpdate();
            session.flush();
            tx.commit();
        } catch (Exception e) {
            LOGGER.debug("Exception has occurred while deleting time slot schedule map: {} | exception: {}",
                    scheduleIds, e.getMessage());
            throw e;
        }finally {
            session.close();
        }
    }

    @Override
    public List<RescheduleTimeBelt> getPartiallyProcessedSchedules(Date date, Time time) {
        String sql = "select sd.Channel_id as channelId, sd.Date as date,  Min(sd.Schedule_start_time) as startTime," +
                " Max(sd.Schedule_end_time) as endTime from schedule_def sd where sd.Date = ?1 and sd.Status != 6 and sd.Status != 9  " +
                " and sd.Schedule_start_time >= ?2 and Schedule_id " +
                " not in (select Schedule_id FROM time_slot_schedule_map) group by sd.Channel_id, sd.Date order by sd.Date";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "rescheduleTimeBeltList");
        query.setParameter(1, date);
        query.setParameter(2, time);
        return query.getResultList();
    }

    @Override
    public List<RescheduleTimeBelt> getPartiallyProcessedSchedules(Date date) {
        String sql = "select sd.Channel_id as channelId, sd.Date as date,  Min(sd.Schedule_start_time) as startTime," +
                " Max(sd.Schedule_end_time) as endTime from schedule_def sd where sd.Date > ?1 and sd.Status != 6 and sd.Status != 9 " +
                " and Schedule_id " +
                " not in (select Schedule_id FROM time_slot_schedule_map) group by sd.Channel_id, sd.Date order by sd.Date";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "rescheduleTimeBeltList");
        query.setParameter(1, date);
        return query.getResultList();
    }

    @Override
    public List<WOChannelSummary> getWorkOrderChannelSummaries(int channelId, List<Integer> woIds) {
        String sql = "select sd.Work_order_id as workOrderId, sum(ad.duration) as consumedDuration from schedule_def sd left join" +
                " advertisement_def ad on sd.Advert_id = ad.Advert_id  where sd.Channel_id = ?2 and sd.Work_order_id in ?1 " +
                " and sd.Status = 2 and ad.Advert_type = 'ADVERT' group by sd.Work_order_id";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "channelSummary");
        query.setParameter(1, woIds);
        query.setParameter(2, channelId);
        return query.getResultList();
    }

}
