package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.LogoAuditDAO;
import com.vclabs.nash.model.entity.LogoAudit;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nalaka on 2019-01-28.
 */
@Repository
public class LogoAuditDAOImpl extends VCLSessionFactory implements LogoAuditDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceTaxDetailsDAOimp.class);

    @Override
    public LogoAudit save(LogoAudit logoAudit) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(logoAudit);
            return logoAudit;
        } catch (Exception e) {
            LOGGER.debug("Exception in LogoAuditDAOImpl while trying to save LogoAudit: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<LogoAudit> getSelectedPlaylistId(int playlistId, int hour) {

        ArrayList<LogoAudit> logoAuditList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(LogoAudit.class);
            criteria.add(Restrictions.eq("playlist.playlistid", playlistId));
            criteria.add(Restrictions.eq("hour", hour));
            criteria.add(Restrictions.eq("status", "2"));
            criteria.addOrder(Order.desc("createTime"));
            logoAuditList = (ArrayList<LogoAudit>) criteria.list();
        } catch (Exception e) {
            LOGGER.debug("Exception in LogoAuditDAOImpl while trying to getSelectedPlaylistId: {}", e.getMessage());
            throw e;
        }
        return logoAuditList;
    }
}
