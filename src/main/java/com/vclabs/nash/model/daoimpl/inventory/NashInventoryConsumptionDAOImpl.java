package com.vclabs.nash.model.daoimpl.inventory;

import com.vclabs.nash.model.dao.inventory.NashInventoryConsumptionDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventory.NashInventoryConsumption;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * Created by Nalaka on 2018-10-04.
 */
@Repository
public class NashInventoryConsumptionDAOImpl  extends VCLSessionFactory implements NashInventoryConsumptionDAO {

    @Override
    public NashInventoryConsumption findByInventoryRowId(long inventoryRowId) {
        String sql = "select ic from NashInventoryConsumption ic where ic.inventoryRow.id = :irid";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery(sql, NashInventoryConsumption.class);
        query.setParameter("irid", inventoryRowId);
        Optional<NashInventoryConsumption> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public NashInventoryConsumption save(NashInventoryConsumption nashInventoryConsumption) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.saveOrUpdate(nashInventoryConsumption);
        session.flush();
        return nashInventoryConsumption;
    }

    @Override
    public List<NashInventoryConsumption> findHighestInventoryUtilizedChannels(){
        String sql = String.format("select * from nash_inventory_consumption order by spentTime desc limit 5");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, NashInventoryConsumption.class);
        List<NashInventoryConsumption> result = query.getResultList();
        return result;
    }
}
