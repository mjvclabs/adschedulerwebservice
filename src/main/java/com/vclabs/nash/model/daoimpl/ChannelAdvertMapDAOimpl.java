package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.ChannelAdvertMapDAO;
import com.vclabs.nash.model.entity.ChannelAdvertMap;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ChannelAdvertMapDAOimpl extends VCLSessionFactory implements ChannelAdvertMapDAO {
    @Override
    public List<ChannelAdvertMap> getAllChannelAdvertMap() {
        ArrayList<ChannelAdvertMap> channelAdvertMapList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(ChannelAdvertMap.class);
            criteria.add(Restrictions.eq("status", 1));
            channelAdvertMapList = (ArrayList<ChannelAdvertMap>) criteria.list();

        } catch (Exception e) {
            throw e;
        }
        return channelAdvertMapList; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean saveOrUpdateChannelAdvertMap(ChannelAdvertMap model) {
        Session session = null;
        try {

            session = this.getSessionFactory().getCurrentSession();

         //   Transaction tx = session.beginTransaction();
            session.saveOrUpdate(model);
//            session.flush();
//            tx.commit();

            return true;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<ChannelAdvertMap> getChannelAdvertMap(int channelId) {
        ArrayList<ChannelAdvertMap> channelAdvertMapList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(ChannelAdvertMap.class);
            criteria.add(Restrictions.eq("status", 1));
            criteria.add(Restrictions.eq("channelid.channelid", channelId));
            criteria.addOrder(Order.asc("percentage"));
            channelAdvertMapList = (ArrayList<ChannelAdvertMap>) criteria.list();

        } catch (Exception e) {
            throw e;
        }
        return channelAdvertMapList;
    }

    @Override
    public List<ChannelAdvertMap> getChannelAdvertMapByAdvertId(int advertId) {
        ArrayList<ChannelAdvertMap> channelAdvertMapList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(ChannelAdvertMap.class);
            criteria.add(Restrictions.eq("status", 1));
            criteria.add(Restrictions.eq("advertid.advertid", advertId));
            criteria.addOrder(Order.asc("percentage"));
            channelAdvertMapList = (ArrayList<ChannelAdvertMap>) criteria.list();

        } catch (Exception e) {
            throw e;
        }
        return channelAdvertMapList;
    }
}
