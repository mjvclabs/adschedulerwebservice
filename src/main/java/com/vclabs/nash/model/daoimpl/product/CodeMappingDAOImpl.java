package com.vclabs.nash.model.daoimpl.product;

import com.vclabs.nash.model.dao.product.CodeMappingDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.product.CodeMapping;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import java.util.Optional;

/**
 * Created by dperera on 30/08/2019.
 */
@Repository
public class CodeMappingDAOImpl extends VCLSessionFactory implements CodeMappingDAO {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CodeMappingDAOImpl.class);

    @Override
    public CodeMapping save(CodeMapping codeMapping) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(codeMapping);
            return codeMapping;
        } catch (Exception e) {
            LOGGER.debug("Exception has occurred while saving product with code {}", codeMapping.getCode());
            return null;
        }
    }

    @Override
    public List<CodeMapping> findAllCodeMapping(int size, int offset, String clientId, String productName, String productId) {
        ArrayList<CodeMapping> codeMappingList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(CodeMapping.class);
            criteria.addOrder(Order.desc("id"));
            if (!clientId.equals("-111")) {
                int clien_id = Integer.parseInt(clientId);
                criteria.createAlias("client", "clid");
                criteria.add(Restrictions.eq("clid.clientid", clien_id));
            }
            if (!productName.equals("") || !productId.equals("")) {
                criteria.createAlias("product", "pdct");
            }
            if (!productName.equals("")) {
                criteria.add(Restrictions.like("pdct.name", productName, MatchMode.ANYWHERE));
            }
            if (!productId.equals("")) {
                criteria.add(Restrictions.like("pdct.id", Long.parseLong("" + productId)));
            }
            criteria.setFirstResult(offset);
            criteria.setMaxResults(size);
            codeMappingList = (ArrayList<CodeMapping>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in CodeMappingDAOImpl in findAllCodeMapping : {}", e.getMessage());
            throw e;
        }
        return codeMappingList;
    }

    @Override
    public List<CodeMapping> findByCode(String code) {
        ArrayList<CodeMapping> codeMappingList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(CodeMapping.class);
            criteria.add(Restrictions.eq("code", code));

            codeMappingList = (ArrayList<CodeMapping>) criteria.list();

        } catch (Exception e) {
            LOGGER.debug("Exception in CodeMappingDAOImpl in findByCode : {}", e.getMessage());
            throw e;
        }
        return codeMappingList;
    }

    @Override
    public int findAllCodeMappingCount(String clientId, String productName, String productId) {
        Long productCount = 0L;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(CodeMapping.class);
            if (!clientId.equals("-111")) {
                int clien_id = Integer.parseInt(clientId);
                criteria.createAlias("client", "clid");
                criteria.add(Restrictions.eq("clid.clientid", clien_id));
            }
            if (!productName.equals("") || !productId.equals("")) {
                criteria.createAlias("product", "pdct");
            }
            if (!productName.equals("")) {
                criteria.add(Restrictions.like("pdct.name", productName, MatchMode.ANYWHERE));
            }
            if (!productId.equals("")) {
                criteria.add(Restrictions.like("pdct.id", productName, MatchMode.ANYWHERE));
            }
            criteria.setProjection(Projections.rowCount());
            productCount = (long) criteria.uniqueResult();

        } catch (Exception e) {
            LOGGER.debug("Exception in CodeMappingDAOImpl in findAllCodeMapping : {}", e.getMessage());
            throw e;
        }
        return Integer.parseInt("" + productCount);
    }

    public List<CodeMapping> findByClientId(long clientId) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select * from code_mapping where client_id = ?1";
        Query query = session.createNativeQuery(sqlString, CodeMapping.class);
        query.setParameter(1, clientId);
        return  query.getResultList();
    }

    @Override
    public CodeMapping findById(long id) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "select * from code_mapping where id = ?1";
        Query query = session.createNativeQuery(sqlString, CodeMapping.class);
        query.setParameter(1, id);
        Optional<CodeMapping> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }
}
