package com.vclabs.nash.model.daoimpl.inventoryprediction;

import com.vclabs.nash.model.dao.inventoryprediction.PredictionResultMetaDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventoryprediction.PredictionResultMeta;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Nalaka on 2018-10-11.
 */
@Repository
public class PredictionResultMetaDAOImpl extends VCLSessionFactory implements PredictionResultMetaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredictionResultMetaDAOImpl.class);

    @Override
    public PredictionResultMeta save(PredictionResultMeta predictionResultMeta) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.save(predictionResultMeta);
        LOGGER.debug("PredictionResultMeta is saved successfully");
        return predictionResultMeta;
    }

    @Override
    public PredictionResultMeta update(PredictionResultMeta predictionResultMeta) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.update(predictionResultMeta);
        LOGGER.debug("PredictionResultMeta is updated successfully");
        return predictionResultMeta;
    }

    @Override
    public List<PredictionResultMeta> findLatestPredictionResultMetaByInventoryType(PredictionResultMeta.InventoryType inventoryType) {
        String sql = "SELECT * FROM prediction_result_meta where inventoryType=?1 and isProcess=false";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, PredictionResultMeta.class);
        query.setParameter(1, inventoryType.ordinal());
        return query.getResultList();
    }
}
