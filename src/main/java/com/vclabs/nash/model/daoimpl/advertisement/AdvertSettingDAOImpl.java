package com.vclabs.nash.model.daoimpl.advertisement;

import com.vclabs.nash.model.dao.Advertisement.AdvertSettingDAO;
import com.vclabs.nash.model.daoimpl.AdvertCategoryDAOImp;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.AdvertSetting;
import com.vclabs.nash.model.entity.WorkOrderChannelList;
import com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

/**
 * Created by dperera on 18/09/2019.
 */
@Repository
public class AdvertSettingDAOImpl extends VCLSessionFactory implements AdvertSettingDAO {

    static final Logger logger = LoggerFactory.getLogger(AdvertSettingDAOImpl.class);

    @Override
    public AdvertSetting save(AdvertSetting advertSetting) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(advertSetting);
            return advertSetting;
        } catch (Exception e) {
            logger.debug("Exception in AdvertSettingDAOImpl while saving advert setting  : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public boolean hasIntersection(AdvertSetting advertSetting) {
        String sql = String.format("select count(*) from advert_setting where channel_id = ?1 and min_limit <= ?2 and max_limit >= ?3 and type = ?4");
        Session session = this.getSessionFactory().getCurrentSession();
        SQLQuery query = session.createNativeQuery(sql);
        query.setParameter(1, advertSetting.getChannelDetails().getChannelid());
        query.setParameter(2, advertSetting.getMinLimit());
        query.setParameter(3, advertSetting.getMinLimit());
        query.setParameter(4, advertSetting.getType().ordinal());
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue() > 0;
    }

    @Override
    public AdvertSetting finMaxAllowedSlots(int cid, int duration) {
        String sql = String.format("select * from advert_setting where channel_id = ?1 and (min_limit <= ?2 and max_limit >= ?3) and type = ?4 ");
        Session session = this.getSessionFactory().getCurrentSession();
        SQLQuery query = session.createNativeQuery(sql, AdvertSetting.class);
        query.setParameter(1, cid);
        query.setParameter(2, duration);
        query.setParameter(3, duration);
        query.setParameter(4, AdvertSetting.Type.MAX_ADVERT_COUNT.ordinal());
        Optional<AdvertSetting> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public AdvertSetting findLapDuration(int cid, int duration) {
        String sql = String.format("select * from advert_setting where channel_id = ?1 and (min_limit <= ?2 and max_limit >= ?3) and type = ?4 ");
        Session session = this.getSessionFactory().getCurrentSession();
        SQLQuery query = session.createNativeQuery(sql, AdvertSetting.class);
        query.setParameter(1, cid);
        query.setParameter(2, duration);
        query.setParameter(3, duration);
        query.setParameter(4, AdvertSetting.Type.LAP_DURATION.ordinal());
        Optional<AdvertSetting> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public List<AdvertSetting> finByChannelId(int cid, AdvertSetting.Type type) {
        String sql = String.format("select * from advert_setting where channel_id = ?1 and type = ?2 order by min_limit");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, AdvertSetting.class);
        query.setParameter(1, cid);
        query.setParameter(2, type.ordinal());
        return query.getResultList();
    }

    @Override
    public Boolean delete(AdvertSetting advertSetting) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "delete from advert_setting where id = ?1 ";
        Query query = session.createNativeQuery(sqlString);
        query.setParameter(1, advertSetting.getId());
        query.executeUpdate();
        return true;
    }

    @Override
    @Transactional
    public int getMaxLapDuration(int channelId) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(AdvertSetting.class);
            criteria.add(Restrictions.eq("channelDetails.channelid", channelId));
            criteria.setProjection(Projections.max("lap"));
            Object result = criteria.uniqueResult();
            return result != null ? (Integer) result : 0;
        } catch (Exception e) {
            logger.debug("Exception in AdvertisementDAOImpl in getSelectedAdvertisement : {}", e.getMessage());
            throw e;
        }
    }
}
