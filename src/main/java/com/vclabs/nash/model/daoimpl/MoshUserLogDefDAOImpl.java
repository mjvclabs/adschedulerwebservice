package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.MoshUserLogDefDAO;
import com.vclabs.nash.model.entity.MoshUserLogDef;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2019-04-08.
 */
@Repository
public class MoshUserLogDefDAOImpl extends VCLSessionFactory implements MoshUserLogDefDAO {

    @Override
    public MoshUserLogDef save(MoshUserLogDef moshUserLogDef) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.save(moshUserLogDef);
        return moshUserLogDef;
    }

    @Override
    public MoshUserLogDef update(MoshUserLogDef moshUserLogDef) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.update(moshUserLogDef);
        return moshUserLogDef;
    }

    @Override
    public MoshUserLogDef getById(int id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(MoshUserLogDef.class);
        criteria.add(Restrictions.eq("id", id));
        return (MoshUserLogDef) criteria.uniqueResult();
    }

    @Override
    public List<MoshUserLogDef> getselectedLogs(ArrayList<Integer> channelIds, Date startDate, Date endDate, ArrayList<String> userNames) {

        List<MoshUserLogDef> logList = null;

        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(MoshUserLogDef.class);
        criteria.add(Restrictions.and(Restrictions.ge("loginTime", startDate), Restrictions.or(Restrictions.le("logoutTime", endDate),Restrictions.isNull("logoutTime"))));
        if (channelIds.size() != 0) {
            criteria.add(Property.forName("channelDetails.channelid").in(channelIds));
        }
        if (userNames.size() != 0) {
            criteria.add(Property.forName("user").in(userNames));
        }
        logList = (ArrayList<MoshUserLogDef>) criteria.list();

        return logList;
    }
}
