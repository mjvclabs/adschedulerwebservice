package com.vclabs.nash.model.daoimpl.inventory;

import com.vclabs.nash.model.dao.inventory.InventoryRowDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.sql.Time;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Nalaka on 2018-10-04.
 */
@Repository
public class InventoryRowDAOImpl extends VCLSessionFactory implements InventoryRowDAO {

    @Override
    public InventoryRow findByChannelAndDateAndTimeBelt(long channelId, Date date, Time fromTime, Time toTime) {
        String sql = "select ir.* from inventory_row ir left join inventory inv on inv.id = ir.inventoryId where ir.fromTime = ?3 and ir.toTime = ?4 and inv.date = ?2 and inv.channelId = ?1";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, InventoryRow.class);
        query.setParameter(1, channelId);
        query.setParameter(2, date);
        query.setParameter(3, fromTime);
        query.setParameter(4, toTime);
        Optional<InventoryRow> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }
}
