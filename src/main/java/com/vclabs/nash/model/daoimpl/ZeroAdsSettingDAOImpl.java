package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.ZeroAdsSettingDAO;
import com.vclabs.nash.model.entity.ZeroAdsSetting;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Nalaka on 2018-09-18.
 */
@Repository
public class ZeroAdsSettingDAOImpl extends VCLSessionFactory implements ZeroAdsSettingDAO {

    @Override
    public ZeroAdsSetting save(ZeroAdsSetting zeroAdsSetting) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.save(zeroAdsSetting);
        return zeroAdsSetting;
    }

    @Override
    public ZeroAdsSetting update(ZeroAdsSetting zeroAdsSetting) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.update(zeroAdsSetting);
        return zeroAdsSetting;
    }

    @Override
    public List<ZeroAdsSetting> findAll() {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(ZeroAdsSetting.class);
            List<ZeroAdsSetting> resultList = (List<ZeroAdsSetting>) criteria.list();
            return resultList;
        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }

    @Override
    public ZeroAdsSetting findByChannelId(int channelId) {
        Session session = this.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(ZeroAdsSetting.class);
        criteria.add(Restrictions.eq("channelId.channelid", channelId));
        ZeroAdsSetting zeroAdsSetting = (ZeroAdsSetting) criteria.uniqueResult();
        return zeroAdsSetting;
    }
}

