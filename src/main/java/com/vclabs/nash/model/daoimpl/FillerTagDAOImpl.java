/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.model.dao.FillerTagDAO;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.FillerTag;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.product.Brand;
import com.vclabs.nash.model.process.AdvertFilter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Nalaka on 2020-01-02.
 */
@Repository
public class FillerTagDAOImpl extends VCLSessionFactory implements FillerTagDAO {

    private static final Logger logger = LoggerFactory.getLogger(FillerTagDAOImpl.class);

    @Override
    public FillerTag saveFillerTag(FillerTag model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return model;
        } catch (Exception e) {
            logger.debug("Exception in FillerTagDAOImpl in saveFillerTag() : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<FillerTag> getByAdvertId(long advertId) {
        String sql = "select * from filler_tag where advert_id = ?1 ;";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, FillerTag.class);
        query.setParameter(1, advertId);
        return query.getResultList();
    }

    @Override
    public void delete(List<Long> ids) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlString = "delete from filler_tag where id in ?1 ";
        Query query = session.createNativeQuery(sqlString);
        query.setParameter(1, ids);
        query.executeUpdate();
    }

    @Override
    public FillerTag updateFillerTag(FillerTag model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return model;
        } catch (Exception e) {
            logger.debug("Exception in FillerTagDAOImpl in updateFillerTag() : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<FillerTag> getAllFillerTag() {
        ArrayList<FillerTag> fillerTagsList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(FillerTag.class);
            fillerTagsList = (ArrayList<FillerTag>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in FillerTagDAOImpl in getAllFillerTag() : {}", e.getMessage());
            throw e;
        }
        return fillerTagsList;
    }

    @Override
    public List<Advertisement> getFillers(AdvertFilter advertFilter, int limit, int offset) {

        Session session = this.getSessionFactory().getCurrentSession();
        String sqlQuery = "select distinct ad.* from advertisement_def ad ";
        if(advertFilter.getChannelId() != -111){
            sqlQuery += "right join filler_tag ft on ad.Advert_id = ft.advert_id ";
        }
        sqlQuery += "where ad.Advert_type = 'FILLER' and ad.status = 1 and ad.enabled = 1 and ad.Advert_id != 1 and ad.Advert_id != 0 and ad.status != 3 ";
        if (!advertFilter.getAdvertId().equals("-111")) {
            sqlQuery += "and ad.Advert_id = ?1 ";
        }
        if (!advertFilter.getClient().equals("-111")) {
            sqlQuery += "and ad.client = ?2 ";
        }
        if (!advertFilter.getAdvertName().equals("-111")) {
            sqlQuery += "and ad.Advert_name like ?3 ";
        }
        if(advertFilter.getChannelId() != -111) {
            sqlQuery += "and ft.channel_id = ?6 ";
        }
        sqlQuery += "order by ad.Advert_id limit ?4 offset ?5";

        Query query = session.createNativeQuery(sqlQuery, Advertisement.class);
        if (!advertFilter.getAdvertId().equals("-111")) {
            query.setParameter(1, Integer.parseInt(advertFilter.getAdvertId()));
        }
        if (!advertFilter.getClient().equals("-111")) {
            query.setParameter(2, Integer.parseInt(advertFilter.getClient()));
        }
        if (!advertFilter.getAdvertName().equals("-111")) {
            query.setParameter(3, advertFilter.getAdvertName());
        }
        query.setParameter(4, limit);
        query.setParameter(5, offset);
        if(advertFilter.getChannelId() != -111) {
            query.setParameter(6, advertFilter.getChannelId());
        }
        return query.getResultList();
    }

    @Override
    public int getFillerCount(AdvertFilter advertFilter) {

        Session session = this.getSessionFactory().getCurrentSession();
        String sqlQuery = "select count(distinct ad.Advert_id) from advertisement_def ad ";
        if(advertFilter.getChannelId() != -111) {
            sqlQuery += "right join filler_tag ft on ad.Advert_id = ft.advert_id ";
        }
        sqlQuery += "where ad.Advert_type = 'FILLER' and ad.status = 1  and ad.enabled = 1 and ad.Advert_id != 1 and ad.Advert_id != 0 and ad.status != 3 ";
        if (!advertFilter.getAdvertId().equals("-111")) {
            sqlQuery += "and ad.Advert_id = ?1 ";
        }
        if (!advertFilter.getClient().equals("-111")) {
            sqlQuery += "and ad.client = ?2 ";
        }
        if (!advertFilter.getAdvertName().equals("-111")) {
            sqlQuery += "and ad.Advert_name like ?3 ";
        }
        if(advertFilter.getChannelId() != -111) {
            sqlQuery += "and ft.channel_id = ?4 ";
        }

        Query query = session.createNativeQuery(sqlQuery);
        if (!advertFilter.getAdvertId().equals("-111")) {
            query.setParameter(1, Integer.parseInt(advertFilter.getAdvertId()));
        }
        if (!advertFilter.getClient().equals("-111")) {
            query.setParameter(2, Integer.parseInt(advertFilter.getClient()));
        }
        if (!advertFilter.getAdvertName().equals("-111")) {
            query.setParameter(3, advertFilter.getAdvertName());
        }
        if(advertFilter.getChannelId() != -111) {
            query.setParameter(4, advertFilter.getChannelId());
        }
        Optional<BigInteger> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get().intValue() : 0;
    }

    @Override
    public List<Advertisement> getFillersByChannelId(int channelId) {
        Session session = this.getSessionFactory().getCurrentSession();
        String sqlQuery = "select distinct ad.* from advertisement_def ad " +
                "right join filler_tag ft on ad.Advert_id = ft.advert_id " +
                "where ad.Advert_type = 'FILLER' and ad.status = 1 and ad.enabled = 1 " +
                "and ad.Advert_id != 1 and ad.Advert_id != 0 and ad.status != 3 " +
                "and ft.channel_id = ?1 order by ad.Advert_id";
        Query query = session.createNativeQuery(sqlQuery, Advertisement.class);
        query.setParameter(1, channelId);
        return query.getResultList();
    }
}
