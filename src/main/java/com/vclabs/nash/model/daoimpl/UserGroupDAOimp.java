/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.daoimpl;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.vclabs.nash.model.dao.UserGroupDAO;
import com.vclabs.nash.model.entity.UserGroup;

/**
 *
 * @author user
 */
@Repository
public class UserGroupDAOimp extends VCLSessionFactory implements UserGroupDAO {

    private static final Logger logger = LoggerFactory.getLogger(UserGroupDAOimp.class);

    @Override
    public Boolean saveUserGroup(UserGroup model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.save(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in UserGroupDAOimp in saveUserGroup() : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<UserGroup> getAllUserGroup() {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(UserGroup.class);
            criteria.addOrder( Order.asc("userGroupName"));
            criteria.addOrder( Order.asc("pageid"));
            List<UserGroup> usergroupList = (List<UserGroup>) criteria.list();
            return usergroupList;
        } catch (Exception e) {
            logger.debug("Exception in UserGroupDAOimp in getAllUserGroup() : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean updateUserGroup(UserGroup model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.update(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in UserGroupDAOimp in updateUserGroup() : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteUserGroup(UserGroup model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in UserGroupDAOimp in deleteUserGroup() : ", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<UserGroup> getSelectedUserGroup(String group) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(UserGroup.class);
            criteria.add( Restrictions.eq("userGroupName",group));
            List<UserGroup> usergroupList = (List<UserGroup>) criteria.list();
            return usergroupList;
        } catch (Exception e) {
            logger.debug("Exception in UserGroupDAOimp in getSelectedUserGroup() : ", e.getMessage());
            throw e;
        }
    }
}
