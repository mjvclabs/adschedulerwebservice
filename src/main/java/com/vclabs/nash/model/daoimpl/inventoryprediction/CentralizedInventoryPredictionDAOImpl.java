package com.vclabs.nash.model.daoimpl.inventoryprediction;

import com.vclabs.nash.model.dao.inventoryprediction.CentralizedInventoryPredictionDAO;
import com.vclabs.nash.model.daoimpl.VCLSessionFactory;
import com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by Sanduni on 11/10/2018
 */
@Repository
public class CentralizedInventoryPredictionDAOImpl extends VCLSessionFactory implements CentralizedInventoryPredictionDAO{

    private static final Logger LOGGER = LoggerFactory.getLogger(CentralizedInventoryPredictionDAOImpl.class);

    @Override
    public CentralizedInventoryPrediction save(CentralizedInventoryPrediction centralizedInventoryPrediction) {
        Session session = this.getSessionFactory().getCurrentSession();
        try {
            session.saveOrUpdate(centralizedInventoryPrediction);
            LOGGER.debug("Successfully saved/updated CentralizedInventoryPredictionDAOImpl");
        }catch (Exception e){
            LOGGER.debug("Exception : CentralizedInventoryPrediction save");
        }
        return centralizedInventoryPrediction;
    }

    @Override
    public CentralizedInventoryPrediction findCentralizedInventoryPrediction(Date dateAndTime, Integer channelId){
        String sql = "select cip.* from centralized_inventory_prediction cip where cip.dateAndTime = ?1 and cip.channelDetails_Channel_id = ?2";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, CentralizedInventoryPrediction.class);
        query.setParameter(1, dateAndTime);
        query.setParameter(2, channelId);
        Optional<CentralizedInventoryPrediction> result = query.getResultList().stream().findFirst();
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public List<CentralizedInventoryPrediction> findUpdatedInventory() {
        String sql = "select * from centralized_inventory_prediction where hasUpdated=1";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, CentralizedInventoryPrediction.class);
        return query.getResultList();
    }

    @Override
    public List<CentralizedInventoryPrediction> findByDateAndChannel(Date date, Integer channelId){
        String sql = "select cip.* from centralized_inventory_prediction cip where cip.date = ?1 and cip.channelDetails_Channel_id = ?2";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, CentralizedInventoryPrediction.class);
        query.setParameter(1, date);
        query.setParameter(2, channelId);
        return query.getResultList();
    }
}
