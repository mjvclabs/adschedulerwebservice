/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.daoimpl;

import com.vclabs.nash.dashboard.dto.*;
import com.vclabs.nash.model.dao.WorkOrderDAO;
import com.vclabs.nash.model.entity.WorkOrder;
import com.vclabs.nash.model.process.FiltersModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.vclabs.nash.model.process.dto.WODropDownDataDto;
import com.vclabs.nash.service.vo.WOChannelSummary;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

/**
 *
 * @author user
 */
@Repository
public class WorkOrderDAOimp extends VCLSessionFactory implements WorkOrderDAO {

    static final Logger logger = LoggerFactory.getLogger(WorkOrderDAOimp.class);

    @Value("${dashboard.record.limit}")
    private int RECORD_LIMIT;

    @Override
    public List<WorkOrder> getActiveWorkOrederList() {

        ArrayList<WorkOrder> currentWorkOrderList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.add(Restrictions.or(Restrictions.eq("permissionstatus", 2), Restrictions.eq("permissionstatus", 0), Restrictions.eq("permissionstatus", 4)));
            currentWorkOrderList = (ArrayList<WorkOrder>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getActiveWorkOrederList : {}", e.getMessage());
            throw e;
        }
        return currentWorkOrderList;
    }

    @Override
    public List<WorkOrder> getWorkOrederList(String seller) { ///by seller
        ArrayList<WorkOrder> currentWorkOrderList = null;

        try {
            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(WorkOrder.class);
            //criteria.add( Restrictions.eq("seller",seller));
            currentWorkOrderList = (ArrayList<WorkOrder>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getWorkOrederList : {}", e.getMessage());
            throw e;
        }
        return currentWorkOrderList;
    }

    @Override
    public List<WorkOrder> getAllWorkOrderList() {
        ArrayList<WorkOrder> allWorkOrderList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.add(Restrictions.ne("workorderid", 1));
            criteria.add(Restrictions.ne("workorderid", 0));
            allWorkOrderList = (ArrayList<WorkOrder>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getAllWorkOrderList : {}", e.getMessage());
            throw e;
        }
        return allWorkOrderList;
    }

    @Override
    public List<WODropDownDataDto> getWODropDownData(){
        try {

            String sql = String.format("select Work_order_id as workorderid, Order_name as ordername, Start_date, End_date from work_order where Work_order_id > 1 order by Work_order_id desc");
            Session session = this.getSessionFactory().getCurrentSession();
            Query query = session.createNativeQuery(sql, "WODropDownDataMapping");
            List<WODropDownDataDto> data = query.getResultList();
            return data;
       }catch (Exception e){
            logger.debug("Exception in WorkOrderDAOimp getWODropDownData: {}", e.getMessage());
            return null;
        }
    }

    @Override
    public int saveWorkOrder(WorkOrder model) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Session session = this.getSessionFactory().getCurrentSession();
            session.saveOrUpdate(model);
            return model.getWorkorderid();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in saveWorkOrder : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean upadteWorkOrder(WorkOrder model) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Session session = this.getSessionFactory().getCurrentSession();
            session.merge(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in upadteWorkOrder : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean deleteWorkOrder(WorkOrder model) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            session.delete(model);
            return true;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in deleteWorkOrder : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public WorkOrder getSelectedWorkOrder(int workOrderId) {
        WorkOrder workorder = null;
        Session session = null;
        try {
            //Set openSession
            session = this.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.add(Restrictions.eq("workorderid", workOrderId));
            workorder = (WorkOrder) criteria.uniqueResult();
            return workorder;
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getSelectedWorkOrder : {}", e.getMessage());
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public List<WorkOrder> getIsExistingWorkOrder(String refe) {
        List<WorkOrder> existList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            //criteria.add(Restrictions.like("scheduleRef", refe, MatchMode.ANYWHERE));
            criteria.add(Restrictions.eq("scheduleRef", refe));
            existList = (ArrayList<WorkOrder>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getIsExistingWorkOrder : {}", e.getMessage());
            throw e;
        }
        return existList;
    }

    @Override
    public List<WorkOrder> getIsExistingWorkOrder(String refe, int workOrderId) {
        List<WorkOrder> existList = new ArrayList<>();
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            //criteria.add(Restrictions.like("scheduleRef", refe, MatchMode.ANYWHERE));
            criteria.add(Restrictions.ne("workorderid", workOrderId));
            criteria.add(Restrictions.eq("scheduleRef", refe));
            existList = (ArrayList<WorkOrder>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getIsExistingWorkOrder : {}", e.getMessage());
            throw e;
        }
        return existList;
    }

    @Override
    public List<WorkOrder> getWorkOrderListForChackStatus() {
        ArrayList<WorkOrder> currentWorkOrderList = null;
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.add(Restrictions.and(Restrictions.ne("autoStatus", WorkOrder.AutoStatus.CompleteInvoicing), Restrictions.ne("autoStatus", WorkOrder.AutoStatus.CompleteAiring)));
            currentWorkOrderList = (ArrayList<WorkOrder>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getWorkOrderListForChackStatus : {}", e.getMessage());
            throw e;
        }
        return currentWorkOrderList;
    }

    @Override
    public List<Integer> getWorkOrderListForCheckStatus() {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery("select Work_order_id from work_order where (autoStatus != 4 and  autoStatus !=  3)");
        return query.getResultList();
    }

    @Override
    public int getAllWorkOrderCount(int woID, FiltersModel filter) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long workOrderCount = 0;

            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.add(Restrictions.ne("workorderid", 1));
            criteria.add(Restrictions.ne("workorderid", 0));

            if (woID != 0) {
                criteria.add(Restrictions.like("workorderid", woID));
            }

            if (filter.getAgent() != null && !filter.getAgent().equals("")) {
                criteria.add(Restrictions.like("agencyclient", Integer.parseInt(filter.getAgent())));
            }

            if (filter.getClient() != null && !filter.getClient().equals("")) {
                criteria.add(Restrictions.like("client", Integer.parseInt(filter.getClient())));
            }

            if (filter.getProduct() != null && !filter.getProduct().equals("")) {
                criteria.add(Restrictions.like("ordername", filter.getProduct(), MatchMode.ANYWHERE));
            }
            if (filter.getScheduleRef() != null && !filter.getScheduleRef().equals("")) {
                criteria.add(Restrictions.like("scheduleRef", filter.getScheduleRef(), MatchMode.ANYWHERE));
            }
            if (filter.getMe() != null && !filter.getMe().equals("")) {
                criteria.add(Restrictions.like("seller", filter.getMe(), MatchMode.ANYWHERE));
            }
            if (filter.getStart() != null && !filter.getStart().equals("")) {
                criteria.add(Restrictions.eq("startdate", filter.getStartDate()));
            }
            if (!filter.getEnd().equals("")) {
                criteria.add(Restrictions.eq("enddate", filter.getEndDate()));
            }
            if (!filter.getModifyDateS().equals("")) {
                criteria.add(Restrictions.ge("lastModifyDate", filter.getModifyDate()));
            }

            if (filter.getAutoStatusB()) {
                criteria.add(Restrictions.eq("autoStatus", filter.getAutoStatus()));
            }

            if (filter.getManualmaStatusB()) {
                criteria.add(Restrictions.eq("manualStatus", filter.getManualStatus()));
            }
            criteria.setProjection(Projections.rowCount());
            workOrderCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + workOrderCount);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getAllWorkOrderCount : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<WorkOrder> getLimitedWorkOrderList(int size, int offset, int woID, FiltersModel filter) {
        ArrayList<WorkOrder> allWorkOrderList = null;
        Session session = null;
        try {
            session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.addOrder(Order.desc("workorderid"));
            if (woID != 0) {
                criteria.add(Restrictions.like("workorderid", woID));
            }
            if (filter.getAgent() != null && !filter.getAgent().equals("")) {
                criteria.add(Restrictions.like("agencyclient", Integer.parseInt(filter.getAgent())));
            }

            if (filter.getClient() != null && !filter.getClient().equals("")) {
                criteria.add(Restrictions.like("client", Integer.parseInt(filter.getClient())));
            }

            if (filter.getProduct() != null && !filter.getProduct().equals("")) {
                criteria.add(Restrictions.like("ordername", filter.getProduct(), MatchMode.ANYWHERE));
            }
            if (filter.getScheduleRef() != null && !filter.getScheduleRef().equals("")) {
                criteria.add(Restrictions.like("scheduleRef", filter.getScheduleRef(), MatchMode.ANYWHERE));
            }
            if (filter.getMe() != null && !filter.getMe().equals("")) {
                criteria.add(Restrictions.like("seller", filter.getMe(), MatchMode.ANYWHERE));
            }
            if (filter.getStart() != null && !filter.getStart().equals("")) {
                criteria.add(Restrictions.eq("startdate", filter.getStartDate()));
            }
            if (!filter.getEnd().equals("")) {
                criteria.add(Restrictions.eq("enddate", filter.getEndDate()));
            }
            if (!filter.getModifyDateS().equals("")) {
                criteria.add(Restrictions.ge("lastModifyDate", filter.getModifyDate()));
            }

            if (filter.getAutoStatusB()) {
                criteria.add(Restrictions.eq("autoStatus", filter.getAutoStatus()));
            }

            if (filter.getManualmaStatusB()) {
                criteria.add(Restrictions.eq("manualStatus", filter.getManualStatus()));
            }

            criteria.setFirstResult(offset);
            criteria.setMaxResults(size);
            allWorkOrderList = (ArrayList<WorkOrder>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getLimitedWorkOrderList : {}", e.getMessage());
            throw e;
        }
        return allWorkOrderList;
    }

    @Override
    public List<WorkOrder> getLimitedWorkOrderListForSchedule(int size, int offset, int woID, FiltersModel filter) {
        ArrayList<WorkOrder> allWorkOrderList = null;
        try {

            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.addOrder(Order.desc("workorderid"));

            if (woID != 0) {
                criteria.add(Restrictions.like("workorderid", woID));
            }
            if (filter.getAgent() != null && !filter.getAgent().equals("")) {
                criteria.add(Restrictions.like("agencyclient", Integer.parseInt(filter.getAgent())));
            }

            if (filter.getClient() != null && !filter.getClient().equals("")) {
                criteria.add(Restrictions.like("client", Integer.parseInt(filter.getClient())));
            }

            if (filter.getProduct() != null && !filter.getProduct().equals("")) {
                criteria.add(Restrictions.like("ordername", filter.getProduct(), MatchMode.ANYWHERE));
            }

            if (filter.getScheduleRef() != null && !filter.getScheduleRef().equals("")) {
                criteria.add(Restrictions.like("scheduleRef", filter.getScheduleRef(), MatchMode.ANYWHERE));
            }

            if (filter.getMe() != null && !filter.getMe().equals("")) {
                criteria.add(Restrictions.like("seller", filter.getMe(), MatchMode.ANYWHERE));
            }

            if (filter.getStart() != null && !filter.getStart().equals("")) {
                criteria.add(Restrictions.eq("startdate", filter.getStartDate()));
            }

            if (!filter.getEnd().equals("")) {
                criteria.add(Restrictions.eq("enddate", filter.getEndDate()));
            }

            if (!filter.getModifyDateS().equals("")) {
                criteria.add(Restrictions.ge("lastModifyDate", filter.getModifyDate()));
            }

            if (filter.getAutoStatusB()) {
                criteria.add(Restrictions.eq("autoStatus", filter.getAutoStatus()));
            }

            if (filter.getManualmaStatusB()) {
                criteria.add(Restrictions.eq("manualStatus", filter.getManualStatus()));
            }

            criteria.setFirstResult(offset);
            criteria.setMaxResults(size);
            allWorkOrderList = (ArrayList<WorkOrder>) criteria.list();
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getLimitedWorkOrderListForSchedule : {}", e.getMessage());
            throw e;
        }
        return allWorkOrderList;
    }

    @Override
    public int getAllWorkOrderCountForSchedule(int woID, FiltersModel filter) {
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            long workOrderCount = 0;
            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.add(Restrictions.ne("workorderid", 1));
            criteria.add(Restrictions.ne("workorderid", 0));
            if (woID != 0) {
                criteria.add(Restrictions.like("workorderid", woID));
            }

            if (filter.getAgent() != null && !filter.getAgent().equals("")) {
                criteria.add(Restrictions.like("agencyclient", Integer.parseInt(filter.getAgent())));
            }

            if (filter.getClient() != null && !filter.getClient().equals("")) {
                criteria.add(Restrictions.like("client", Integer.parseInt(filter.getClient())));
            }

            if (filter.getProduct() != null && !filter.getProduct().equals("")) {
                criteria.add(Restrictions.like("ordername", filter.getProduct(), MatchMode.ANYWHERE));
            }
            if (filter.getScheduleRef() != null && !filter.getScheduleRef().equals("")) {
                criteria.add(Restrictions.like("scheduleRef", filter.getScheduleRef(), MatchMode.ANYWHERE));
            }
            if (filter.getMe() != null && !filter.getMe().equals("")) {
                criteria.add(Restrictions.like("seller", filter.getMe(), MatchMode.ANYWHERE));
            }
            if (filter.getStart() != null && !filter.getStart().equals("")) {
                criteria.add(Restrictions.eq("startdate", filter.getStartDate()));
            }
            if (!filter.getEnd().equals("")) {
                criteria.add(Restrictions.eq("enddate", filter.getEndDate()));
            }
            if (!filter.getModifyDateS().equals("")) {
                criteria.add(Restrictions.ge("lastModifyDate", filter.getModifyDate()));
            }

            if (filter.getAutoStatusB()) {
                criteria.add(Restrictions.eq("autoStatus", filter.getAutoStatus()));
            }

            if (filter.getManualmaStatusB()) {
                criteria.add(Restrictions.eq("manualStatus", filter.getManualStatus()));
            }
            criteria.setProjection(Projections.rowCount());
            workOrderCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + workOrderCount);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getAllWorkOrderCountForSchedule : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<WorkOrder> getLimitedWorkOrderListForBilling(int size, int offset, FiltersModel filter) {
        ArrayList<WorkOrder> allWorkOrderList = null;
        try {

            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.addOrder(Order.desc("workorderid"));

            criteria.add(Restrictions.ne("workorderid", 1));
            criteria.add(Restrictions.ne("workorderid", 0));

            if (filter.getWorkOrderId() != 0) {
                criteria.add(Restrictions.like("workorderid", filter.getWorkOrderId()));
            }
            if (filter.getAgent() != null && !filter.getAgent().equals("")) {
                criteria.add(Restrictions.like("agencyclient", Integer.parseInt(filter.getAgent())));
            }

            if (filter.getClient() != null && !filter.getClient().equals("")) {
                criteria.add(Restrictions.like("client", Integer.parseInt(filter.getClient())));
            }

            if (filter.getProduct() != null && !filter.getProduct().equals("")) {
                criteria.add(Restrictions.like("ordername", filter.getProduct(), MatchMode.ANYWHERE));
            }

            if (filter.getMe() != null && !filter.getMe().equals("")) {
                criteria.add(Restrictions.like("seller", filter.getMe(), MatchMode.ANYWHERE));
            }

            if (filter.getWoType() != null && !filter.getWoType().equals("")) {
                criteria.add(Restrictions.like("woType", filter.getWoType(), MatchMode.ANYWHERE));
            }

            if (filter.getScheduleRef() != null && !filter.getScheduleRef().equals("")) {
                criteria.add(Restrictions.like("scheduleRef", filter.getScheduleRef(), MatchMode.ANYWHERE));
            }

            if (filter.getRevMonth() != null && !filter.getRevMonth().equals("")) {
                criteria.add(Restrictions.eq("revenueMonth", filter.getRevMonth()));
            }

            if (!filter.getEnd().equals("")) {
                criteria.add(Restrictions.eq("enddate", filter.getEndDate()));
            }

            if (filter.getAutoStatusB()) {
                criteria.add(Restrictions.eq("autoStatus", filter.getAutoStatus()));
            }

            if (filter.getManualmaStatusB()) {
                criteria.add(Restrictions.eq("manualStatus", filter.getManualStatus()));
            }

            if (filter.getCommission() != -999) {
                criteria.add(Restrictions.like("commission", filter.getCommission()));
            }

            if (filter.getAgencyclient() != -999) {
                if (filter.getAgencyclient() == 0) {
                    criteria.add(Restrictions.eq("agencyclient", 0));
                } else {
                    criteria.add(Restrictions.ne("agencyclient", 0));
                }
            }

            criteria.setFirstResult(offset);
            criteria.setMaxResults(size);
            allWorkOrderList = (ArrayList<WorkOrder>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getLimitedWorkOrderListForBilling : {}", e.getMessage());
            throw e;
        }
        return allWorkOrderList;
    }

    @Override
    public int getAllWorkOrderCountForBilling(FiltersModel filter) {
        try {

            Session session = this.getSessionFactory().getCurrentSession();

            Criteria criteria = session.createCriteria(WorkOrder.class);
            criteria.addOrder(Order.desc("workorderid"));
            criteria.add(Restrictions.ne("workorderid", 1));
            criteria.add(Restrictions.ne("workorderid", 0));

            if (filter.getWorkOrderId() != 0) {
                criteria.add(Restrictions.like("workorderid", filter.getWorkOrderId()));
            }
            if (filter.getAgent() != null && !filter.getAgent().equals("")) {
                criteria.add(Restrictions.like("agencyclient", Integer.parseInt(filter.getAgent())));
            }

            if (filter.getClient() != null && !filter.getClient().equals("")) {
                criteria.add(Restrictions.like("client", Integer.parseInt(filter.getClient())));
            }

            if (filter.getProduct() != null && !filter.getProduct().equals("")) {
                criteria.add(Restrictions.like("ordername", filter.getProduct(), MatchMode.ANYWHERE));
            }

            if (filter.getMe() != null && !filter.getMe().equals("")) {
                criteria.add(Restrictions.like("seller", filter.getMe(), MatchMode.ANYWHERE));
            }

            if (filter.getWoType() != null && !filter.getWoType().equals("")) {
                criteria.add(Restrictions.like("woType", filter.getWoType(), MatchMode.ANYWHERE));
            }

            if (filter.getScheduleRef() != null && !filter.getScheduleRef().equals("")) {
                criteria.add(Restrictions.like("scheduleRef", filter.getScheduleRef(), MatchMode.ANYWHERE));
            }

            if (filter.getRevMonth() != null && !filter.getRevMonth().equals("")) {
                criteria.add(Restrictions.eq("revenueMonth", filter.getRevMonth()));
            }

            if (!filter.getEnd().equals("")) {
                criteria.add(Restrictions.eq("enddate", filter.getEndDate()));
            }

            if (filter.getAutoStatusB()) {
                criteria.add(Restrictions.eq("autoStatus", filter.getAutoStatus()));
            }

            if (filter.getManualmaStatusB()) {
                criteria.add(Restrictions.eq("manualStatus", filter.getManualStatus()));
            }

            if (filter.getCommission() != -999) {
                criteria.add(Restrictions.like("commission", filter.getCommission()));
            }

            if (filter.getAgencyclient() != -999) {
                if (filter.getAgencyclient() == 0) {
                    criteria.add(Restrictions.eq("agencyclient", 0));
                } else {
                    criteria.add(Restrictions.ne("agencyclient", 0));
                }
            }
            criteria.setProjection(Projections.rowCount());
            long workOrderCount = (long) criteria.uniqueResult();
            return Integer.parseInt("" + workOrderCount);
        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getAllWorkOrderCountForBilling : {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public List<WorkOrder> getSelectedClientWorkOrder(int clientId, List<Integer> clientIDs) {
        ArrayList<WorkOrder> currentWorkOrderList = null;//130
        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);
            //criteria.add(Restrictions.eq("client", clientId));
            if (!clientIDs.isEmpty()) {
                criteria.add(Property.forName("client").in(clientIDs));
            }
            currentWorkOrderList = (ArrayList<WorkOrder>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getSelectedClientWorkOrder : {}", e.getMessage());
            throw e;
        }
        return currentWorkOrderList;
    }

    @Override
    public List<WorkOrder> getSelectedUserWorkOrederList(List<String> woType, List<String> meList, List<String> monthList, List<Integer> clientIntList, List<Integer> agencyIntList) {
        ArrayList<WorkOrder> currentWorkOrderList = null;

        try {
            Session session = this.getSessionFactory().getCurrentSession();
            Criteria criteria = session.createCriteria(WorkOrder.class);

            if (!woType.isEmpty()) {
                criteria.add(Property.forName("woType").in(woType));
            }
            if (!meList.isEmpty()) {
                criteria.add(Property.forName("seller").in(meList));
            }
            if (!monthList.isEmpty()) {
                criteria.add(Property.forName("revenueMonth").in(monthList));
            }

            if (!clientIntList.isEmpty()) {
                criteria.add(Property.forName("client").in(clientIntList));
            }

            if (!agencyIntList.isEmpty()) {
                criteria.add(Property.forName("agencyclient").in(agencyIntList));
            }
            currentWorkOrderList = (ArrayList<WorkOrder>) criteria.list();

        } catch (Exception e) {
            logger.debug("Exception in WorkOrderDAOimp in getSelectedUserWorkOrederList : {}", e.getMessage());
            throw e;
        }
        return currentWorkOrderList;
    }

  /*----------------------------------Dashboard core---------------------------*/

    @Override
    public List<WorkOrder> findPenddingWorkOrderByUser(String user) {
        String sql = String.format("select * from work_order where Seller=%s and autoStatus =%d and manualStatus!=%d and Permission_status!=3;", "'" + user + "'", WorkOrder.AutoStatus.Initial.ordinal(), WorkOrder.ManualStatus.PaymentDone.ordinal());
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, WorkOrder.class);
        return query.getResultList();
    }

    @Override
    public List<WorkOrder> findPenddingWorkOrderByRevenueMonth(String month) {
        String sql = String.format("select * from work_order where autoStatus !=%d and autoStatus!=%d and manualStatus!=%d and Permission_status!=3 %s ;", WorkOrder.AutoStatus.Initial.ordinal(), WorkOrder.AutoStatus.CompleteInvoicing.ordinal(), WorkOrder.ManualStatus.PaymentDone.ordinal(), month == "ALL" ? "" : " and revenueMonth = '" + month + "'");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, WorkOrder.class);
        return query.getResultList();
    }

    @Override
    public List<PendingWOCountByAutoStatusDto> findAllPendingWOAutoStatusCount() {
        String sql = String.format("select 0 as woCount, wo.revenueMonth as revenueMonth, wo.autoStatus as autoStatus, wo.Work_order_id as woId, " +
                        "cd.Client_name as client, sd.Schedule_id as scheduleId from work_order wo " +
                "left join schedule_def sd on wo.Work_order_id = sd.Work_order_id " +
                        "left join client_details cd on cd.Client_id = wo.Client " +
                        "where wo.autoStatus !=%d and wo.autoStatus!=%d and wo.manualStatus!=%d and wo.Permission_status!=3 and wo.revenueMonth != ''" +
                "order by wo.revenueMonth Asc, wo.autoStatus Asc limit %d"
                , WorkOrder.AutoStatus.Initial.ordinal(), WorkOrder.AutoStatus.CompleteInvoicing.ordinal(), WorkOrder.ManualStatus.PaymentDone.ordinal(), RECORD_LIMIT);
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "pendingWOAutoStatusCountMapping");
        return query.getResultList();
    }

    @Override
    public List<PendingWOCountByManualStatusDto> findAllPendingWOManualStatusCount() {
        String sql = String.format("select 0 as woCount, wo.revenueMonth as revenueMonth, wo.manualStatus as manualStatus, wo.Work_order_id as woId, " +
                        "cd.Client_name as client, sd.Schedule_id as scheduleId from work_order wo " +
                        "left join schedule_def sd on wo.Work_order_id = sd.Work_order_id " +
                        "left join client_details cd on cd.Client_id = wo.Client " +
                        "where wo.autoStatus !=%d and wo.autoStatus!=%d and wo.manualStatus!=%d and wo.Permission_status!=3 and wo.revenueMonth != ''" +
                        "order by wo.revenueMonth Asc, wo.manualStatus Asc limit %d"
                , WorkOrder.AutoStatus.Initial.ordinal(), WorkOrder.AutoStatus.CompleteInvoicing.ordinal(), WorkOrder.ManualStatus.PaymentDone.ordinal(), RECORD_LIMIT);
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "pendingWOManualStatusCountMapping");
        return query.getResultList();
    }

    @Override
    public List<WorkOrder> findCompletedSchedulesWorkOrderByUser(String user) {
        String sql = String.format("select * from work_order where autoStatus =%d %s;", WorkOrder.AutoStatus.CompleteSchedule.ordinal(), user == "ALL" ? "" : "and Seller='" + user + "'");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, WorkOrder.class);
        return query.getResultList();
    }

    @Override
    public List<WorkOrder> findNewWorkOrders(String seller) {
        String sql = String.format("select * from work_order where autoStatus=0 and Permission_status != 3 and Seller=%s limit %d;", "'"+seller+"'",RECORD_LIMIT);
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, WorkOrder.class);
        List<WorkOrder> workOrdersList = query.getResultList();
        return workOrdersList;
    }

    @Override
    public List<String> findNewWorkOrdersSellers() {
        String sql = String.format("select distinct Seller from work_order where autoStatus=0 and Permission_status != 3;");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        List<String> sellers = query.getResultList();
        return sellers;
    }

    @Override
    public List<WorkOrder> findRevisedWorkOrders() {
        String sql = String.format("select * from work_order where manualStatus=1 order by Seller limit %d;", RECORD_LIMIT);
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, WorkOrder.class);
        List<WorkOrder> workOrdersList = query.getResultList();
        return workOrdersList;
    }

    @Override
    public List<SchedulesReadyForInvoicingDto> findSchedulesReadyForInvoicing() {
        String sql = String.format("select wo.Work_order_id as woId, cd.Client_name as client, wo.End_date as endDate,\n" +
                "wo.Seller as seller from work_order wo\n" +
                "left join client_details cd on cd.Client_id = wo.Client\n" +
                "where wo.autoStatus=3 order by woId;");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "schedulesReadyForInvoicingDtoMapping");
        List<SchedulesReadyForInvoicingDto> resultList = query.getResultList();
        return resultList;
    }

    @Override
    public List<WorkOrder> findPartiallyEnteredWorkOrders() {
        String sql = String.format("select * from work_order wo where wo.autoStatus=1 order by Seller");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, WorkOrder.class);
        List<WorkOrder> workOrdersList = query.getResultList();
        return workOrdersList;
    }

    @Override
    public double getTotalRevenueForMonthBySeller(String month, String seller) {
        String sql = String.format("select sum(totalBudget) from work_order where revenueMonth = %s and Seller = %s;", "'"+month+"'", "'"+seller+"'");
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        return (double)query.getSingleResult();
    }

    @Override
    public List<MyRecordedRevenueDto> findMyRecordedRevenue(String userName){
        String sql = "select sum(totalBudget) as totalBudget, revenueMonth from work_order where seller = ?1 group by revenueMonth";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "revenueMonthDtoMapping");
        query.setParameter(1, userName);
        List<MyRecordedRevenueDto> result = query.getResultList();
        return result;
    }

    @Override
    public List<String>findAllSellers(){
        String sql = "select distinct seller from work_order";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        List<String> result = query.getResultList();
        return result;
    }

    @Override
    public List<MyCompletedSchedulesDto>  findMyCompletedSchedules() {
        String sql = "select count(*) as completedSchedules, cd.Client_name as client, wo.Seller as seller from work_order wo left join client_details cd " +
                "on wo.`Client` = cd.Client_id where autoStatus = ?1 group by wo.`Client`,seller order by wo.Seller limit ?2";

        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "myCompletedSchedulesDtoMapping");
        query.setParameter(1, WorkOrder.AutoStatus.CompleteSchedule.ordinal());
        query.setParameter(2, RECORD_LIMIT);
        List<MyCompletedSchedulesDto> result = query.getResultList();
        return result;
    }

    @Override
    public List<SalesRevenueDTO> findSelecteUserAllWorkOrder(String user) {
        String sql = "SELECT Work_order_id, Seller, revenueMonth, totalBudget, Start_date, End_date FROM work_order where Seller=?1 order by revenueMonth asc";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "SalesRevenueDTOMapping");
        query.setParameter(1, user);
        List<SalesRevenueDTO> result = query.getResultList();
        return result;
    }

    @Override
    public List<RevenueLineDbDTO> findAllWorkOrderForRevenueLine() {
        String sql = "select wo.Seller, wo.Start_date, wo.End_date, wo.totalBudget, wo.woType, cl.Client_name from work_order wo left join client_details cl on wo.Bill_client=cl.Client_id order by cl.Client_name, wo.Seller";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "RevenueLineDTOMapping");
        List<RevenueLineDbDTO> result = query.getResultList();
        return result;
    }

    @Override
    public List<Integer> findZeroBudgetWorkOrders() {
        String sql = "select w.Work_order_id from work_order w where w.totalBudget = 0";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql);
        List<Integer> result = query.getResultList();
        return result;
    }

    @Override
    public List<WOChannelSummary> getWorkOrderChannelSummary(int channelId, List<Integer> woIds) {
        String sql = "select wo.totalBudget as totalBudget, wcl.Channel_id as channelId, wcl.Work_order_id as workOrderId, wcl.Numbert_of_spot * 30 as paidDuration, wcl.Bonus_spot * 30 as bonusDuration" +
                " from work_order_channel_list wcl left join work_order wo on wo.Work_order_id = wcl.Work_order_id  where wcl.Work_order_id in ?1 and wcl.Channel_id = ?2 order by wcl.Channel_id";
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createNativeQuery(sql, "woChannelSummary");
        query.setParameter(1, woIds);
        query.setParameter(2, channelId);
        return query.getResultList();
    }
}
