package com.vclabs.nash.model.daoimpl;


import com.vclabs.nash.model.dao.PlaylistTempSequenceDAO;
import com.vclabs.nash.model.entity.PlaylistTempSequence;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by dperera on 29/10/2019.
 */
@Repository
public class PlaylistTempSequenceDAOImpl extends VCLSessionFactory implements PlaylistTempSequenceDAO {

    @Override
    public PlaylistTempSequence save(PlaylistTempSequence plts) {
        Session session = this.getSessionFactory().getCurrentSession();
        session.save(plts);
        return plts;
    }
}
