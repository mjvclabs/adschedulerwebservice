package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.ZeroAdsSetting;

import java.util.List;

/**
 * Created by Nalaka on 2018-09-18.
 */
public interface ZeroAdsSettingDAO {

    ZeroAdsSetting save(ZeroAdsSetting zeroAdsSetting);

    ZeroAdsSetting update(ZeroAdsSetting zeroAdsSetting);

    ZeroAdsSetting findByChannelId(int channelId);

    List<ZeroAdsSetting> findAll();
}
