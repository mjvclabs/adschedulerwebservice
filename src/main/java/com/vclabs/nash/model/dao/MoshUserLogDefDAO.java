package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.MoshUserLogDef;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2019-04-08.
 */
public interface MoshUserLogDefDAO {

    MoshUserLogDef save(MoshUserLogDef moshUserLogDef);

    MoshUserLogDef update(MoshUserLogDef moshUserLogDef);

    MoshUserLogDef getById(int id);

    List<MoshUserLogDef> getselectedLogs(ArrayList<Integer> channelIds, Date startDate, Date endDate, ArrayList<String> userNames);
}
