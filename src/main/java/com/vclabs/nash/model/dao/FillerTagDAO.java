package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.FillerTag;
import com.vclabs.nash.model.process.AdvertFilter;

import java.util.List;

/**
 * Created by Nalaka on 2020-01-02.
 */
public interface FillerTagDAO {

    FillerTag saveFillerTag(FillerTag model);

    FillerTag updateFillerTag(FillerTag model);

    List<FillerTag> getAllFillerTag();

    List<FillerTag> getByAdvertId(long advertId);

    void delete(List<Long> ids);

    List<Advertisement> getFillers(AdvertFilter advertFilter, int limit, int offset);

    int getFillerCount(AdvertFilter advertFilter);

    List<Advertisement> getFillersByChannelId(int channelId);
}
