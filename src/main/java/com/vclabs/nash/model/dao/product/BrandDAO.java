package com.vclabs.nash.model.dao.product;

import com.vclabs.nash.model.entity.product.Brand;
import com.vclabs.nash.model.entity.product.Product;

import java.util.List;

/**
 * Created by dperera on 29/08/2019.
 */
public interface BrandDAO {

    void save(Brand brand);

    Brand findByName(String name);

    List<Brand> findAll();

}
