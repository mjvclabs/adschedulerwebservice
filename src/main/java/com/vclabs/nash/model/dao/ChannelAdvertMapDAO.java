package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.ChannelAdvertMap;

import java.util.List;

public interface ChannelAdvertMapDAO {
    List<ChannelAdvertMap> getAllChannelAdvertMap();

    Boolean saveOrUpdateChannelAdvertMap(ChannelAdvertMap model);

    List<ChannelAdvertMap> getChannelAdvertMap(int channelId);

    List<ChannelAdvertMap> getChannelAdvertMapByAdvertId(int advertId);
}
