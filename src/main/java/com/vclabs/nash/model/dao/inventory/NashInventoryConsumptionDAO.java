package com.vclabs.nash.model.dao.inventory;

import com.vclabs.nash.model.entity.inventory.NashInventoryConsumption;

import java.util.List;

/**
 * Created by Nalaka on 2018-10-04.
 */
public interface NashInventoryConsumptionDAO {

    NashInventoryConsumption findByInventoryRowId(long inventoryRowId);

    NashInventoryConsumption save(NashInventoryConsumption nashInventoryConsumption);

    List<NashInventoryConsumption> findHighestInventoryUtilizedChannels();

}
