/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.dao.Advertisement;

import java.util.Date;
import java.util.List;

import com.vclabs.nash.dashboard.dto.IngestStatusWidgetDto;
import com.vclabs.nash.dashboard.dto.PriorityAdvertisementWidgetDto;
import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.process.AdvertFilter;
import com.vclabs.nash.model.process.dto.AdvertisementDropDownDataDto;

/**
 *
 * @author user
 */
public interface AdvertisementDAO { //tested

    List<Advertisement> getFillerAdvertisements();

    List<Advertisement> getFilterAdvertisements(String clientId, String AdvertCatogory, String advertName,String advertId);

    Advertisement getSelectedAdvertisement(int advertisementId);

    Advertisement getAdvertisementForAudit(int advertisementId);

    Boolean updateAdvertisement(Advertisement model);

    List<Advertisement> getAdvertisementAll();

    List<Advertisement> getAdvertisementAllValid();

    List<Advertisement> getAdvertisementsTobeExpiredOrSuspended(Date date);

    List<Advertisement> getLimitedAdvert(int size, int offset, AdvertFilter advertFilter);

    int getLimitedAdvertCount(AdvertFilter advertFilter);

    List<AdvertisementDropDownDataDto> getAdvertisementAllOrderByName();

    List<Advertisement> getAdvertisementList(int client);

    List<Advertisement> getAdvertisementListValid(int client);

    List<Advertisement> getClientAdvertisement(int clientintOne, int clientTwo); //tested

    Boolean saveAdvertisement(Advertisement model);

    Boolean deleteAdvertisement(Advertisement model);

    List<Advertisement> getDummyCuts();

    List<Advertisement> getAdvertisementByMultyIDSAndClient(List<Integer> cutIds, List<Integer> clientList);

    List<Integer> getAdvertisementIDList();

    List<Advertisement> getDeletedAdvertList();

    List<Advertisement> getDummyCutsForDashboard();

    Integer getSpotCountOfDummyCutsForDashboard(Integer advertId);

    List<IngestStatusWidgetDto> findAllAdvertsForIngestStatusWidget();

    List<PriorityAdvertisementWidgetDto> findPriorityAdvertisementsDetails();

    Advertisement getAdvertisementWithMinDuration(List<Integer> adIds);
}
