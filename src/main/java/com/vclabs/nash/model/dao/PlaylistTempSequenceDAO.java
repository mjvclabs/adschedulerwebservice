package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.PlaylistTempSequence;


/**
 * Created by dperera on 29/10/2019.
 */
public interface PlaylistTempSequenceDAO {

    PlaylistTempSequence save(PlaylistTempSequence plts);
}
