package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.LogoAudit;

import java.util.List;

/**
 * Created by Nalaka on 2019-01-28.
 */
public interface LogoAuditDAO {

    LogoAudit save(LogoAudit logoAudit);

    List<LogoAudit> getSelectedPlaylistId(int playlistId, int hour);
}
