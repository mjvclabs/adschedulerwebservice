package com.vclabs.nash.model.dao.inventoryprediction;

import com.vclabs.nash.model.entity.inventoryprediction.InventoryMonthlyPrediction;

import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
public interface InventoryPredictionMonthlyDAO  {

    InventoryMonthlyPrediction save(InventoryMonthlyPrediction inventoryMonthlyPrediction);

    InventoryMonthlyPrediction update(InventoryMonthlyPrediction inventoryMonthlyPrediction);

    List<InventoryMonthlyPrediction> findAll();

    List<InventoryMonthlyPrediction> findLatestMonthlyPrediction();

    List<InventoryMonthlyPrediction> findLatestPredictions(Date startingDate, Date lastDate);

}
