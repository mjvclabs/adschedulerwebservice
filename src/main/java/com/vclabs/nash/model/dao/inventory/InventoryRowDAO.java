package com.vclabs.nash.model.dao.inventory;

import com.vclabs.nash.model.entity.inventory.InventoryRow;

import java.sql.Time;
import java.util.Date;

/**
 * Created by Nalaka on 2018-10-04.
 */
public interface InventoryRowDAO {

    InventoryRow findByChannelAndDateAndTimeBelt(long channelId, Date date, Time fromTime, Time toTime);
}
