package com.vclabs.nash.model.dao.inventory;

import com.vclabs.nash.model.entity.inventory.InventoryConsumption;

/**
 * Created by Sanduni on 02/10/2018
 */
public interface InventoryConsumptionDAO {

//    @Query("select ic from InventoryConsumption ic where ic.fromTime > ?1 and ic.channel.id = ?2 ")
//    List<InventoryConsumption> findConsumptionAfterTimeAndChannelId(Date time, Long channelId);

    InventoryConsumption findByInventoryRowId(long inventoryRowId);

    InventoryConsumption saveAndFlush(InventoryConsumption inventoryConsumption);
}
