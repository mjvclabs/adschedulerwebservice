/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.dao;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.vclabs.nash.dashboard.dto.DailyMissedSpotsDto;
import com.vclabs.nash.dashboard.dto.HightTrafficChannelsWidgetDto;
import com.vclabs.nash.model.dto.MissedSpotMapDto;
import com.vclabs.nash.model.dto.RescheduleTimeBelt;
import com.vclabs.nash.model.dto.ScheduleAdvertDto;
import com.vclabs.nash.model.dto.TimeSlotScheduleMapDto;
import com.vclabs.nash.model.entity.*;
import com.vclabs.nash.model.process.MissedSpotCountInfo;
import com.vclabs.nash.model.process.OneDayScheduleInfo;
import com.vclabs.nash.model.process.ScheduleAdminViewInfo;
import com.vclabs.nash.service.vo.WOChannelSummary;

/**
 *
 * @author user
 */
public interface SchedulerDAO {

    /*Give all schedule in data base*/
    List<ScheduleDef> getScheduleList();

    List<ScheduleDef> getScheduleListOrderByASC(int workOrderId);

    List<ScheduleAdvertDto> getScheduleList(int workOrderId);

    /*Give all Active spot (Not given status=6 schedule)*/
    List<ScheduleDef> getScheduleListFromAdvertAndWorkOrde(int workOrderId, int advertID);

    List<ScheduleDef> getRemaimingSpotSelectedAdvert(int workOrderId, int advertID);

    List<ScheduleDef> getAvailabilitySelectedAdvert(int workOrderId, int advertID);

    /*Give all valid remaining spot upto current hour according to work order id*/
    List<ScheduleDef> getRamainingSpot(int workOrderId, Date toDay);

    /*Give all valid remaining spot upto current hour according to work order id*/
    List<ScheduleDef> getRamainingSpot_v2(int workOrderId, Date toDay);

    /*Give all valid spot upto current hour according to work order id and advet duration*/
    List<ScheduleDef> getAllSpotOrderByDate(int workOrderId, int duration, int channelID);

    /*Give all valid spot upto current hour according to advert id*/
    List<ScheduleDef> getScheduledSpotByAdvertID(int advertID);

    /*Give all suspend according to advert id and date*/
    List<ScheduleDef> getSuspendByAdvertID(int advertID);

    List<ScheduleDef> getScheduleListFromAdvertAndNotScheduleAndMissed(int advertID);

    List<ScheduleDef> selectedScheduleFromDate(int workOrderId, Date scheduleDate);

    List<ScheduleDef> selectedScheduleFromDate(Date scheduleDate);

    List<TimeSlotScheduleMap> getTimeBeltMapFromDate(int workOrderId, Date scheduleDate);

    List<TimeSlotScheduleMap> getTimeBeltMapFromDate(Date scheduleDate);

    //for Jira issue NRM-813
//    List<ScheduleDef> getScheduleFromDate(int workOrderId, Date scheduleDate);

//    List<ScheduleDef> getScheduleFromDate(Date scheduleDate);
    //end

    List<TimeSlotScheduleMap> findByDateAndTimeBelts(Date date, List<TimeBelts> timeBelts);

    @Deprecated
    List<ScheduleDef> allChannelsAllWorkOrders(Date fromDate, Date toDate);

    List<ScheduleDef> allChannelsAllWorkOrders_v2(Date fromDate, Date toDate);

    @Deprecated
    List<ScheduleDef> allChannelsNotAllWorkOrders(int workOrder, Date fromDate, Date toDate);

    List<ScheduleDef> allChannelsNotAllWorkOrders_v2(int workOrder, Date fromDate, Date toDate);

    @Deprecated
    List<ScheduleDef> notAllChannelsAllWorkOrders(int i_channelId, Date fromDate, Date toDate);

    List<ScheduleDef> notAllChannelsAllWorkOrders_v2(int i_channelId, Date fromDate, Date toDate);

    @Deprecated
    List<ScheduleDef> notAllChannelsNotAllWorkOrders(int i_channelId, int i_workOrderId, Date fromDate, Date toDate);

    List<ScheduleDef> notAllChannelsNotAllWorkOrders_v2(int i_channelId, int i_workOrderId, Date fromDate, Date toDate);

    List<List<ScheduleDef>> getAllSchedulesBetweenDates(int i_channelId, Date fromDate, Date toDate, Date dtStartTime, String type);

    List<List<ScheduleDef>> getAllSchedulesBetweenDates(int i_channelId, Date fromDate, Date toDate, Date dtStartTime, int advertId);

    List<ScheduleDef> getAllSchedulesBetweenDates(int workOrder, Date fromDate, Date toDate, Date dtStartTime);

    List<ScheduleDef> getAllSchedulesByIds(List<Integer> ids);

    List<ScheduleDef> getAirSpot(int workOrderId);

    List<ScheduleDef> getMissedSpot(Date fromDate, Date toDate, int channelId, int advertId); //tested

    List<MissedSpotMapDto> getMissedSpotBySQL(Date fromDate, Date toDate, int channelId, int advertId);

    List<ScheduleDef> getAllScheduleInWorkOrder(int workOrderId);

    List<ScheduleDef> getSelectedAdvertSpotByWorkored(int workOderID,int advertId);

    //return today schedule list according to channel id
    List<ScheduleDef> getTodaySchedule(int channelId, Date date);

    Boolean saveSchedule(ScheduleDef model);

    Boolean updateSchedule(ScheduleDef model);

    Boolean updateSchedule(List<ScheduleDef> models);

    Boolean deleteSchedule(ScheduleDef model);

    ScheduleDef getSelectedSchedule(int scheduleId);

    //This methord return last schedule record of selected advertisement
    ScheduleDef getMaximumSpotInAdvert(int AdvertId);

    ScheduleDef getMaximumSpotInClient(int clientId);

    int getSelectedScheduleCount(int workOrderId);

    int getSelectedAdvertScheduleCount(int workOrderId, int advertId);

    int getSelectedChannelSlotCount(int workOrderId, int channelId);

    int getSelectedWorkOrderAirSpot(int workOrderId);

    int getSelectedWorkOrderValidSpot(int workOrderId); //tested

    List<TimeSlot> getTimeSlot();

    Boolean saveTimeSlotMap(TimeSlotScheduleMap model);

    Boolean saveTimeSlotMapV2(TimeSlotScheduleMap model);

    Boolean updateTimeSlotMap(TimeSlotScheduleMap model);

    Boolean deleteTimeSlotMap(TimeSlotScheduleMap model);

    List<TimeSlotScheduleMap> getFinalAllSchedule(ScheduleAdminViewInfo model); //tested

    List<TimeSlotScheduleMapDto> getFinalAllScheduleV2(ScheduleAdminViewInfo model);

    List<TimeSlotScheduleMap> getSchedulesByIds(List<Integer> ids);

    List<TimeSlotScheduleMap> getSelectedDateSchedule(Date selectDate, int timeBelt, ScheduleAdminViewInfo model);

    List<TimeSlotScheduleMap> getSelectedDateSchedule(Date selectDate);

    List<TimeSlotScheduleMap> getSelectedTimeSlotSchedule(int workOrderId);

    List<TimeSlotScheduleMap> getOneDaySchedules(OneDayScheduleInfo scheduleInfo);

    //////////////////////////////////////////////////////////////////////////////
    List<ScheduleDef> getSchedulePriorityMapList(PriorityDef priorityDf, Date dtStartDate, Date dtEndDate);

    List<TimeSlotScheduleMap> getSchedulesbyTimebeltAdvert(int workOrderID, int timeBeltId, int advertId, Date startDate, Date enddate);

    List<TimeSlotScheduleMap> getSchedulePriorityMapListbyTimeBelt(int timeBeltId, int cluster, int priority, int workOrderID, int advertId, Date dtStartDate, Date dtEndDate, int ichannelID);

    //for playlist generate
    List<ScheduleDef> getSchedulePrioritybyTimeBelt(int timeBeltId, Date date);

    WorkOrderChannelList getWorkOrderAvailableSpotCount(int workOrderID); //tested

    WorkOrderChannelList getWorkOrderAvailableSpotCount(int workOrderID, int channelID); //tested

    /*------------------------Report model----------------*/
    List<ScheduleDef> getCommercialAvailability(int channelId, Date fromDate, Date toDate);

    List<ScheduleDef> getScheduleAnalysisData(List<Integer> workOrdersIDs, List<Integer> agencyIDs, int clientId);

    ScheduleDef getLastAiredSpot(int advertID);

    List<ScheduleDef> getSuspendWorkOrderSpot(int workOderID);

    List<ScheduleDef> getMissedSpotSchedule(MissedSpotCountInfo filterData);

    List<TimeSlotScheduleMap> getTimeSlotForMissedSpot(MissedSpotCountInfo filterData);

    int getPrimeTimeSpotCount(int workOrdersId);

    int getNonPrimeTimeSpotCount(int workOrdersId);

    /*----------------------------------Dashboard core---------------------------*/
    List<ScheduleDef> findCurrentDateAllValidSpot();

    List<ScheduleDef> findCurrentDateSpotByStatus(Date today,int status);

    List<ScheduleDef> findCurrentDateSpotByStatusAndChannel(Date today,int status,int channelId);

    List<ScheduleDef> findAllMissedOrAiredSpots(Date today, int channelId);

    List<ScheduleDef> findAllScheduleSpots(Date today);

    List<HightTrafficChannelsWidgetDto> findHighTrafficChannelsOfCurrentHour();

    List<DailyMissedSpotsDto> findDailyMissedSpotsPercentages();

    List<ScheduleDef> getSuspendScheduleByWorkOrder(int workOrderId);

    void deleteTimeSlotScheduleMapByScheduleId(List<Integer> scheduleIds);

    List<RescheduleTimeBelt> getPartiallyProcessedSchedules(Date date);

    List<RescheduleTimeBelt> getPartiallyProcessedSchedules(Date date, Time time);

    List<WOChannelSummary> getWorkOrderChannelSummaries(int channelId, List<Integer> woIds);
}
