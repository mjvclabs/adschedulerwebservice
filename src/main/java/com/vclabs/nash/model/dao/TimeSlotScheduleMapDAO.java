/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.dto.AdvertSpread;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;

import java.util.Date;
import java.util.List;

/**
 *
 * @author hp
 */
public interface TimeSlotScheduleMapDAO {

    List<TimeSlotScheduleMap> getSelectedWorkOrderTimeSlot(int workOrder);

    TimeSlotScheduleMap getSelectedTimeSlot(int scheduleID);

    List<TimeSlotScheduleMap> getTimeSlotsOfNotCreatedPlayLists(Date date);

    List<AdvertSpread> getAdvertSpreads(Date date, long channelId, long advertId);

    void deleteTimeSlotScheduleMap(Integer channelId, Date date, int startTimeBeltId);

}
