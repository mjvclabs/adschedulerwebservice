package com.vclabs.nash.model.dao.inventoryprediction;

import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;

import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
public interface InventoryPredictionDailyDAO {

    InventoryDailyPrediction save(InventoryDailyPrediction inventoryDailyPrediction);

    List<InventoryDailyPrediction> save(List<InventoryDailyPrediction> inventoryDailyPredictions);

    InventoryDailyPrediction update(InventoryDailyPrediction inventoryDailyPrediction);

    List<InventoryDailyPrediction> findAll();

    List<InventoryDailyPrediction> findLatestPredictions(Date startingDate, Date lastDate);

    InventoryDailyPrediction findById(long id);

    List<InventoryDailyPrediction> findUnProcessedPredictions();
}
