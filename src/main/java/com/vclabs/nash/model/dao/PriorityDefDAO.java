/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.dao;

import java.util.List;
import com.vclabs.nash.model.entity.PriorityDef;

/**
 *
 * @author Administrator
 */
public interface PriorityDefDAO {  //tested
    
    Boolean savePriorityDef(PriorityDef model);
    Boolean updatePriorityDef(PriorityDef model);
    Boolean removePriorityDef(PriorityDef model);
    Boolean removePriorityDefV2(PriorityDef model);
    List<PriorityDef> getPriorityList();
    PriorityDef getPrioritybyID(int id);
    List<PriorityDef> getPrioritybyChannel(int id);
    List<PriorityDef> getPriorityListbyTimeBelt(int timeBeltId);
    List<PriorityDef> getPriorityListbyCluster(int timeBeltId, int cluster);
    PriorityDef getPriorityDefbyPriority(int timeBeltId, int cluster, int priority);
    int getSelectedTimebeltClusterCount(int timeBeltId);
}
