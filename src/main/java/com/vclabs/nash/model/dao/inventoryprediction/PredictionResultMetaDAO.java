package com.vclabs.nash.model.dao.inventoryprediction;

import com.vclabs.nash.model.entity.inventoryprediction.PredictionResultMeta;

import java.util.List;

/**
 * Created by Nalaka on 2018-10-11.
 */
public interface PredictionResultMetaDAO {

    PredictionResultMeta save(PredictionResultMeta predictionResultMeta);

    PredictionResultMeta update(PredictionResultMeta predictionResultMeta);

    List<PredictionResultMeta> findLatestPredictionResultMetaByInventoryType(PredictionResultMeta.InventoryType inventoryType);
}
