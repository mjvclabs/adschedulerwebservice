/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.InvoiceData;

/**
 *
 * @author hp
 */
public interface InvoiceDataDAO { //tested

    InvoiceData getInvoiceData(int invoiceId);

    InvoiceData saveInvoiceData(InvoiceData invoice);
    
    InvoiceData updateInvoiceData(InvoiceData invoice);
    
    InvoiceData getSelectedInvoiceData(int invoiceDataId);

}
