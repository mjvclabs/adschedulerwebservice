package com.vclabs.nash.model.dao;

import com.vclabs.nash.model.entity.playlist_preview.PlayListTemp;

import java.util.Date;
import java.util.List;

/**
 * Created by dperera on 21/10/2019.
 */
public interface PlaylistTempDAO {

    PlayListTemp getPlayList(int playListId);

    PlayListTemp save(PlayListTemp playListTemp);

    List<PlayListTemp> getAdvertPlayList(int channelId, Date date, Date startTime, Date endTime, int hour, int sequenceNum);

    List<PlayListTemp> getPlayListLogo(int _channelId, Date date, Date start_time, Date end_time, int _hour, int sequenceNum);

    List<PlayListTemp> getPlayListCrawler(int _channelId, Date date, Date start_time, Date end_time, int _hour, int sequenceNum);

    List<PlayListTemp> getSchedulesByDate(int channelId, Date date, int seqNum);

}
