package com.vclabs.nash.model.dto;

import java.util.Date;

/**
 * Created by Nalaka on 2019-05-02.
 */
public class MissedSpotMapDto {

    private int scheduleId;

    private Date scheduleActualStartTime;

    private Date actualStartTime;

    private Date actualEndTime;

    private Date date;

    private String status;

    private Date scheduleStartTime;

    private Date schedulEndTime;

    private int priorityId;

    private int priorityCluster;

    private int priorityNumber;

    private String comment;

    private int workOrderId;

    private String workOrderName;

    private Date workOrderEndDate;

    private String clientName;

    private  int clientId;

    private String advertName;

    private int duration;

    private int advertId;

    private String advertType;

    private int channelId;

    private String channelName;



    public int getScheduleId() {
        return scheduleId;
    }

    public MissedSpotMapDto(int scheduleId, Date scheduleActualStartTime, Date actualStartTime, Date actualEndTime, Date date, String status, Date scheduleStartTime, Date schedulEndTime, int priorityId, int priorityCluster, int priorityNumber, String comment, int workOrderId, String workOrderName, Date workOrderEndDate, String clientName, int clientId, String advertName, int duration, int advertId, String advertType, int channelId, String channelName) {
        this.scheduleId = scheduleId;
        this.scheduleActualStartTime = scheduleActualStartTime;
        this.actualStartTime = actualStartTime;
        this.actualEndTime = actualEndTime;
        this.date = date;
        this.status = status;
        this.scheduleStartTime = scheduleStartTime;
        this.schedulEndTime = schedulEndTime;
        this.priorityId = priorityId;
        this.priorityCluster = priorityCluster;
        this.priorityNumber = priorityNumber;
        this.comment = comment;
        this.workOrderId = workOrderId;
        this.workOrderName = workOrderName;
        this.workOrderEndDate = workOrderEndDate;
        this.clientName = clientName;
        this.clientId = clientId;
        this.advertName = advertName;
        this.duration = duration;
        this.advertId = advertId;
        this.advertType = advertType;
        this.channelId = channelId;
        this.channelName = channelName;
    }

    public Date getActualStartTime() {
        return actualStartTime;
    }

    public Date getActualEndTime() {
        return actualEndTime;
    }

    public Date getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public Date getScheduleStartTime() {
        return scheduleStartTime;
    }

    public Date getSchedulEndTime() {
        return schedulEndTime;
    }

    public int getPriorityId() {
        return priorityId;
    }

    public int getPriorityCluster() {
        return priorityCluster;
    }

    public int getPriorityNumber() {
        return priorityNumber;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public String getWorkOrderName() {
        return workOrderName;
    }

    public Date getWorkOrderEndDate() {
        return workOrderEndDate;
    }

    public String getClientName() {
        return clientName;
    }

    public int getClientId() {
        return clientId;
    }

    public String getAdvertName() {
        return advertName;
    }

    public int getDuration() {
        return duration;
    }

    public int getAdvertId() {
        return advertId;
    }

    public String getAdvertType() {
        return advertType;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getComment() {
        return comment;
    }

    public Date getScheduleActualStartTime() {
        return scheduleActualStartTime;
    }
}
