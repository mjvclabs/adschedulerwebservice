package com.vclabs.nash.model.dto;

import java.util.Date;

/**
 * Created by dperera on 22/01/2020.
 */
public class RescheduleTimeBelt {

    private int channelId;

    private Date date;

    private Date startTime;

    private Date endTime;

    public RescheduleTimeBelt(int channelId, Date date, Date startTime, Date endTime) {
        this.channelId = channelId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
