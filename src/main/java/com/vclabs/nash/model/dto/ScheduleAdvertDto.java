package com.vclabs.nash.model.dto;

/**
 * Created by dperera on 29/03/2019.
 */
public class ScheduleAdvertDto {

    private Integer scheduleId;

    private String status;

    private String advertType;

    private Integer duration;

    public ScheduleAdvertDto(Integer scheduleId, String status, String advertType, Integer duration) {
        this.scheduleId = scheduleId;
        this.status = status;
        this.advertType = advertType;
        this.duration = duration;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdvertType() {
        return advertType;
    }

    public void setAdvertType(String advertType) {
        this.advertType = advertType;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
