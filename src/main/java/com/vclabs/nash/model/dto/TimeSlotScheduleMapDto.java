package com.vclabs.nash.model.dto;
import java.util.Date;

/**
 * Created by dperera on 21/03/2019.
 */
public class TimeSlotScheduleMapDto implements Comparable<TimeSlotScheduleMapDto> {

    private Integer scheduleId;

    private Integer timeBeltId;

    private Date date;

    public TimeSlotScheduleMapDto(Integer scheduleId, Integer timeBeltId, Date date) {
        this.scheduleId = scheduleId;
        this.timeBeltId = timeBeltId;
        this.date = date;
    }

    public Integer getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getTimeBeltId() {
        return timeBeltId;
    }

    public void setTimeBeltId(Integer timeBeltId) {
        this.timeBeltId = timeBeltId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(TimeSlotScheduleMapDto o) {
        if (o.getTimeBeltId() == this.getTimeBeltId()) {
            return (int) (this.getDate().getTime() - o.getDate().getTime());
        } else {
            return this.getTimeBeltId() - o.getTimeBeltId();
        }
    }
}
