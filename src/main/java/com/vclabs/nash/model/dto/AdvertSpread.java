package com.vclabs.nash.model.dto;

import java.math.BigInteger;

public class AdvertSpread {

    private int timebeltId;

    private int adCount;

    public AdvertSpread(Integer timebeltId, BigInteger adCount) {
        this.timebeltId = timebeltId;
        this.adCount = adCount.intValue();
    }

    public int getTimebeltId() {
        return timebeltId;
    }

    public void setTimebeltId(int timebeltId) {
        this.timebeltId = timebeltId;
    }

    public int getAdCount() {
        return adCount;
    }

    public void setAdCount(int adCount) {
        this.adCount = adCount;
    }
}
