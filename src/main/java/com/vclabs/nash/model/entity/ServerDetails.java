/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Sanira Nanayakkara
 */
@Entity
@Table(name = "server_details")
public class ServerDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ServerId")
    private Integer serverId;
    @Column(name = "ServerIP")
    private String serverIP;
    @Column(name = "ServerPort")
    private int serverPort;
    @Column(name = "ServicePath")
    private String servicePath;
    @Column(name = "Status")
    private Integer status;
    @Column(name = "FreeDiskSpace")
    private BigInteger freeDiskSpace;
    @Column(name = "FreeMemory")
    private BigInteger freeMemory;
    @Column(name = "Command")
    private String command;
    
    @Column(name = "CardPort")
    private Integer cardPort;
    @Column(name = "InCard")
    private Integer inCard;
    @Column(name = "OutCard")
    private Integer outCard;
    @Column(name = "ErrorReports")
    private String errorReport;
    @Column(name = "ChannelId")
    private Integer channelDetails;
    

    public ServerDetails() {
    }

    public ServerDetails(Integer serverId) {
        this.serverId = serverId;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public String getServicePath() {
        return servicePath;
    }

    public void setServicePath(String servicePath) {
        this.servicePath = servicePath;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigInteger getFreeDiskSpace() {
        return freeDiskSpace;
    }

    public void setFreeDiskSpace(BigInteger freeDiskSpace) {
        this.freeDiskSpace = freeDiskSpace;
    }

    public BigInteger getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(BigInteger freeMemory) {
        this.freeMemory = freeMemory;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serverId != null ? serverId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServerDetails)) {
            return false;
        }
        ServerDetails other = (ServerDetails) object;
        if ((this.serverId == null && other.serverId != null) || (this.serverId != null && !this.serverId.equals(other.serverId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.ServerDetails[ serverId=" + serverId + " ]";
    }

    public Integer getCardPort() {
        return cardPort;
    }

    public void setCardPort(Integer cardPort) {
        this.cardPort = cardPort;
    }

    public Integer getInCard() {
        return inCard;
    }

    public void setInCard(Integer inCard) {
        this.inCard = inCard;
    }

    public Integer getOutCard() {
        return outCard;
    }

    public void setOutCard(Integer outCard) {
        this.outCard = outCard;
    }

    public String getErrorReport() {
        return errorReport;
    }

    public void setErrorReport(String errorReport) {
        this.errorReport = errorReport;
    }

    public Integer getChannelDetails() {
        return channelDetails;
    }

    public void setChannelDetails(Integer channelDetails) {
        this.channelDetails = channelDetails;
    }

}
