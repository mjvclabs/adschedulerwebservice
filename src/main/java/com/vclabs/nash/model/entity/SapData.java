/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vclabs.nash.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "sap_data")
public class SapData implements Serializable {

    private static final long serialVersionID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sap_data_id")
    private Integer sapDataId;

    @Basic(optional = false)
    @Column(name = "file_name_ate")
    private String fileNameDate;

    @Basic(optional = false)
    @Column(name = "count")
    private Integer count;

    public SapData() {
    }

    public Integer getSapDataId() {
        return sapDataId;
    }

    public void setSapDataId(Integer sapDataId) {
        this.sapDataId = sapDataId;
    }

    public String getFileNameDate() {
        return fileNameDate;
    }

    public void setFileNameDate(String fileNameDate) {
        this.fileNameDate = fileNameDate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
}
