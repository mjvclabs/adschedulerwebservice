package com.vclabs.nash.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by dperera on 29/10/2019.
 */
@Entity
@Table(name = "play_list_temp_sequence")
public class PlaylistTempSequence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "Schedule_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    public PlaylistTempSequence() {
        this.createdTime = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
}
