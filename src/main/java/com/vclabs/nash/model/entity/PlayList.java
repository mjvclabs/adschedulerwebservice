/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import com.vclabs.nash.model.daoimpl.PlayListDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.*;

/**
 *
 * @author user
 */
@Entity
@Table(name = "play_list")

public class PlayList implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayList.class);

    public static enum Lable {

        COMMERICIAL("Commercial"),
        ZERO("Zero"),
        FILLER("Filler");

        private String textValue;

        Lable(String textValue) {
            this.textValue = textValue;
        }

        public String getTextValue() {
            return textValue;
        }

        public void setTextValue(String textValue) {
            this.textValue = textValue;
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Playlist_id")
    private Integer playlistid;
    @Basic(optional = false)
    @Column(name = "Schedule_hour")
    private Integer scheduleHour;
    @Basic(optional = false)
    @Column(name = "Schedule_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleStartTime;
    @Basic(optional = false)
    @Column(name = "Schedule_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleEndTime;
    @Basic(optional = false)
    @Column(name = "TimeBelt_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeBeltStartTime;
    @Basic(optional = false)
    @Column(name = "TimeBelt_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeBeltEndTime;
    @Basic(optional = false)
    @Column(name = "Actual_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualStartTime;
    @Basic(optional = false)
    @Column(name = "Actual_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualEndTime;
    
    @Basic(optional = false)
    @Column(name = "Cluster_priority")
    private Integer clusterPriority = -1;

    // ##### 0-Initial_State #####
    // ##### 1-Scheduled_State #####
    // ##### 2-Played_State #####
    // ##### 3-Stopped_State #####
    // ##### 5-NotPlayed_State #####  
    // ##### 6-StoppedbyUser_State ##### 
    // ##### 7-Rescheduled_State ##### 
    // ##### 8-Invalid_State ##### //No_MediaFile
    // ##### 9-Hold_advert #####
    // ##### 10-Partially airing ##-This status only for the LOGO-###
    // ##### 11-Skipped###
    @Basic(optional = false)
    @Column(name = "Status")
    private String status;

    @Basic(optional = false)
    @Column(name = "Play_cluster")
    private Integer playCluster;
    @Basic(optional = false)
    @Column(name = "Play_order")
    private Integer playOrder;
    @Basic(optional = false)
    @Column(name = "Comment")
    private String comment;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @Column(name = "Retry_count")
    private int retryCount;
    @JoinColumn(name = "Advert_id", referencedColumnName = "Advert_id")
    @ManyToOne(optional = false)
    private Advertisement advert;
    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channel;
    @JoinColumn(name = "Schedule_id", referencedColumnName = "Schedule_id")
    @ManyToOne(optional = false)
    private ScheduleDef schedule;
    @JoinColumn(name = "Work_order_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workOrder;
    @Basic(optional = false)
    @Column(name = "conflicting_schedules")
    private ArrayList<Integer> conflictingSchedules = new ArrayList<>();

    private Lable lable = Lable.COMMERICIAL;

    @Transient
    private boolean isC3;

    public PlayList() {
    }

    public PlayList(Integer playlistid) {
        this.playlistid = playlistid;
    }

    public Integer getPlaylistId() {
        return playlistid;
    }

    public void setPlaylistId(Integer playlistid) {
        this.playlistid = playlistid;
    }

    public Date getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(Date scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public Date getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(Date scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public Date getActualStartTime() {
        return actualStartTime;
    }

    public void setActualStartTime(Date actualStartTime) {
        this.actualStartTime = actualStartTime;
    }

    public Date getActualEndTime() {
        return actualEndTime;
    }

    public void setActualEndTime(Date actualEndTime) {
        this.actualEndTime = actualEndTime;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusbyName() {
        String sStatus = "Undefined";
        if (status.equals("0")) {
            sStatus = "Not Scheduled";
        } else if (status.equals("1")) {
            sStatus = "Scheduled";
        } else if (status.equals("2")) {
            sStatus = "Played";
        } else if (status.equals("3")) {
            sStatus = "Stopped";
        } else if (status.equals("4")) {
            sStatus = "Playing";
        } else if (status.equals("5")) {
            sStatus = "Timed Out";
        } else if (status.equals("7")) {
            sStatus = "Rescheduled";
        } else if (status.equals("8")) {
            sStatus = "No Media";
        } else if (status.equals("9")) {
            sStatus = "Suspended";
        }else if (status.equals("10")) {
            sStatus = "Partially airing";
        }else if (status.equals("11")) {
            sStatus = "Skipped";
        }

        return sStatus;
    }

    public Lable getLable() {
        return lable;
    }

    public void setLable(Lable lable) {
        this.lable = lable;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public Advertisement getAdvert() {
        return advert;
    }

    public void setAdvert(Advertisement advertid) {
        this.advert = advertid;
    }

    public ChannelDetails getChannel() {
        return channel;
    }

    public void setChannel(ChannelDetails channelid) {
        this.channel = channelid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (playlistid != null ? playlistid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlayList)) {
            return false;
        }
        PlayList other = (PlayList) object;
        if ((this.playlistid == null && other.playlistid != null) || (this.playlistid != null && !this.playlistid.equals(other.playlistid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.PlayList[ playlistid=" + playlistid + " ]";
    }

    public Integer getScheduleHour() {
        return scheduleHour;
    }

    public void setScheduleHour(Integer scheduleHour) {
        this.scheduleHour = scheduleHour;
    }

    public Date getTimeBeltStartTime() {
        return timeBeltStartTime;
    }

    public void setTimeBeltStartTime(Date timeBeltStartTime) {
        this.timeBeltStartTime = timeBeltStartTime;
    }

    public Date getTimeBeltEndTime() {
        return timeBeltEndTime;
    }

    public void setTimeBeltEndTime(Date timeBeltEndTime) {
        this.timeBeltEndTime = timeBeltEndTime;
    }

    public ScheduleDef getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleDef schedule) {
        this.schedule = schedule;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public Integer getPlayOrder() {
        return playOrder;
    }

    public void setPlayOrder(Integer playOrder) {
        LOGGER.debug("Updated play-order from {} to {} of playlist id {}", this.playOrder, playOrder, playlistid);
        this.playOrder = playOrder;
    }

    public ArrayList<Integer> getConflicting_schedules() {
        return conflictingSchedules;
    }

    public void addConflicting_schedules(Integer conflicting_schedules) {
        this.conflictingSchedules.add(conflicting_schedules);
    }

    public void clearConflicting_schedules() {
        this.conflictingSchedules.clear();
    }

    public Integer getPlayCluster() {
        return playCluster;
    }

    public void setPlayCluster(Integer playCluster) {
        this.playCluster = playCluster;
    }

    public Integer getClusterPriority() {
        return clusterPriority;
    }

    public void setClusterPriority(Integer clusterPriority) {
        this.clusterPriority = clusterPriority;
    }

    public boolean isC3() {
        return isC3;
    }

    public void setC3(boolean c3) {
        isC3 = c3;
    }
}
