/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "order_advert_list")

public class OrderAdvertList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Order_advert_list_id")
    private Integer orderadvertlistid;
    @JoinColumn(name = "Advert_id", referencedColumnName = "Advert_id")
    @ManyToOne(optional = false)
    private Advertisement advertid;
    @JoinColumn(name = "Work_order_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workorderid;

    public OrderAdvertList() {
    }

    public OrderAdvertList(Integer orderadvertlistid) {
        this.orderadvertlistid = orderadvertlistid;
    }

    public Integer getOrderadvertlistid() {
        return orderadvertlistid;
    }

    public void setOrderadvertlistid(Integer orderadvertlistid) {
        this.orderadvertlistid = orderadvertlistid;
    }

    public Advertisement getAdvertid() {
        return advertid;
    }

    public void setAdvertid(Advertisement advertid) {
        this.advertid = advertid;
    }

    public WorkOrder getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(WorkOrder workorderid) {
        this.workorderid = workorderid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderadvertlistid != null ? orderadvertlistid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderAdvertList)) {
            return false;
        }
        OrderAdvertList other = (OrderAdvertList) object;
        if ((this.orderadvertlistid == null && other.orderadvertlistid != null) || (this.orderadvertlistid != null && !this.orderadvertlistid.equals(other.orderadvertlistid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.OrderAdvertList[ orderadvertlistid=" + orderadvertlistid + " ]";
    }

}
