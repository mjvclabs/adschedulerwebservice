/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author user
 */
@Entity
@Table(name = "play_list_history")
public class PlayListHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Play_list_history_id")
    private Integer playlisthistoryid;
    @Basic(optional = false)
    @Column(name = "Play_list_id")
    private int playlistid;
    @Basic(optional = false)
    @Column(name = "Channel_id")
    private int channelid;
    @Basic(optional = false)
    @Column(name = "Work_order_id")
    private int workorderid;
    @Basic(optional = false)
    @Column(name = "Advert_id")
    private int advertid;
    @Basic(optional = false)
    @Column(name = "Schedule_hour")
    private int schedulehour;
    @Basic(optional = false)
    @Column(name = "Schedule_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date schedulestarttime;
    @Basic(optional = false)
    @Column(name = "Schedule_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleendtime;
    @Basic(optional = false)
    @Column(name = "TimeBelt_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeBeltstarttime;
    @Basic(optional = false)
    @Column(name = "TimeBelt_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeBeltendtime;
    @Basic(optional = false)
    @Column(name = "Actual_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualstarttime;
    @Basic(optional = false)
    @Column(name = "Actual_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualendtime;
    @Basic(optional = false)
    @Column(name = "Status")
    private String status;
    @Basic(optional = false)
    @Column(name = "Play_cluster")
    private int playcluster;
    @Basic(optional = false)
    @Column(name = "Play_order")
    private int playorder;
    @Basic(optional = false)
    @Column(name = "Comment")
    private String comment;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @Column(name = "conflicting_schedules")
    private ArrayList<Integer> conflictingSchedules = new ArrayList<>();

    @JoinColumn(name = "Schedule_id", referencedColumnName = "Schedule_id")
    @ManyToOne(optional = false)
    private ScheduleDef scheduleid;

    private PlayList.Lable label = PlayList.Lable.COMMERICIAL;;

    public PlayListHistory() {
    }

    public PlayListHistory(Integer playlisthistoryid) {
        this.playlisthistoryid = playlisthistoryid;
    }

    public PlayListHistory(Integer playlisthistoryid, int playlistid, int channelid, int workorderid, int advertid, int schedulehour, Date schedulestarttime, Date scheduleendtime, Date timeBeltstarttime, Date timeBeltendtime, Date actualstarttime, Date actualendtime, String status, int playcluster, int playorder, String comment, Date date) {
        this.playlisthistoryid = playlisthistoryid;
        this.playlistid = playlistid;
        this.channelid = channelid;
        this.workorderid = workorderid;
        this.advertid = advertid;
        this.schedulehour = schedulehour;
        this.schedulestarttime = schedulestarttime;
        this.scheduleendtime = scheduleendtime;
        this.timeBeltstarttime = timeBeltstarttime;
        this.timeBeltendtime = timeBeltendtime;
        this.actualstarttime = actualstarttime;
        this.actualendtime = actualendtime;
        this.status = status;
        this.playcluster = playcluster;
        this.playorder = playorder;
        this.comment = comment;
        this.date = date;
    }

    public Integer getPlaylisthistoryid() {
        return playlisthistoryid;
    }

    public void setPlaylisthistoryid(Integer playlisthistoryid) {
        this.playlisthistoryid = playlisthistoryid;
    }

    public int getPlaylistid() {
        return playlistid;
    }

    public void setPlaylistid(int playlistid) {
        this.playlistid = playlistid;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }

    public int getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(int workorderid) {
        this.workorderid = workorderid;
    }

    public void setAdvertid(int advertid) {
        this.advertid = advertid;
    }

    public int getAdvertid() {
        return advertid;
    }

    public int getSchedulehour() {
        return schedulehour;
    }

    public void setSchedulehour(int schedulehour) {
        this.schedulehour = schedulehour;
    }

    public Date getSchedulestarttime() {
        return schedulestarttime;
    }

    public void setSchedulestarttime(Date schedulestarttime) {
        this.schedulestarttime = schedulestarttime;
    }

    public Date getScheduleendtime() {
        return scheduleendtime;
    }

    public void setScheduleendtime(Date scheduleendtime) {
        this.scheduleendtime = scheduleendtime;
    }

    public Date getTimeBeltstarttime() {
        return timeBeltstarttime;
    }

    public void setTimeBeltstarttime(Date timeBeltstarttime) {
        this.timeBeltstarttime = timeBeltstarttime;
    }

    public Date getTimeBeltendtime() {
        return timeBeltendtime;
    }

    public void setTimeBeltendtime(Date timeBeltendtime) {
        this.timeBeltendtime = timeBeltendtime;
    }

    public Date getActualstarttime() {
        return actualstarttime;
    }

    public void setActualstarttime(Date actualstarttime) {
        this.actualstarttime = actualstarttime;
    }

    public Date getActualendtime() {
        return actualendtime;
    }

    public void setActualendtime(Date actualendtime) {
        this.actualendtime = actualendtime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPlaycluster() {
        return playcluster;
    }

    public void setPlaycluster(int playcluster) {
        this.playcluster = playcluster;
    }

    public int getPlayorder() {
        return playorder;
    }

    public void setPlayorder(int playorder) {
        this.playorder = playorder;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Integer> getConflicting_schedules() {
        return conflictingSchedules;
    }

    public void addConflicting_schedules(Integer conflicting_schedules) {
        this.conflictingSchedules.add(conflicting_schedules);
    }

    public ScheduleDef getScheduleid() {
        return scheduleid;
    }

    public void setScheduleid(ScheduleDef scheduleid) {
        this.scheduleid = scheduleid;
    }

    public String getStatusbyName() {
        String sStatus = "Undefined";
        if (status.equals("0")) {
            sStatus = "Not Scheduled";
        } else if (status.equals("1")) {
            sStatus = "Scheduled";
        } else if (status.equals("2")) {
            sStatus = "Played";
        } else if (status.equals("3")) {
            sStatus = "Stopped";
        } else if (status.equals("5")) {
            sStatus = "Timed Out";
        } else if (status.equals("7")) {
            sStatus = "Rescheduled";
        } else if (status.equals("8")) {
            sStatus = "No Media";
        } else if (status.equals("9")) {
            sStatus = "Suspended";
        }

        return sStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (playlisthistoryid != null ? playlisthistoryid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlayListHistory)) {
            return false;
        }
        PlayListHistory other = (PlayListHistory) object;
        if ((this.playlisthistoryid == null && other.playlisthistoryid != null) || (this.playlisthistoryid != null && !this.playlisthistoryid.equals(other.playlisthistoryid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.PlayListHistory[ playlisthistoryid= " + playlisthistoryid + " playlistid= " + playlistid + "]";
    }


    public PlayList.Lable getLabel() {
        return label;
    }

    public void setLabel(PlayList.Lable label) {
        this.label = label;
    }
}
