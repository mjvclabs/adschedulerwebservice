package com.vclabs.nash.model.entity.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vclabs.nash.model.entity.ClientDetails;

import javax.persistence.*;

/**
 * Created by dperera on 29/08/2019.
 */
@Entity
@Table(name = "code_mapping")
public class CodeMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "client_id")
    private ClientDetails client;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;

    private String code;

    //@JsonIgnore
    public ClientDetails getClient() {
        return client;
    }

    @JsonProperty
    public void setClient(ClientDetails client) {
        this.client = client;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "CodeMapping{" +
                "id=" + id +
                ", client=" + client +
                ", product=" + product +
                ", brand=" + brand +
                ", code='" + code + '\'' +
                '}';
    }
}
