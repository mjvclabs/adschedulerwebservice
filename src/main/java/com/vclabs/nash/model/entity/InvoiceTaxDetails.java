/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "invoice_tax_details")

public class InvoiceTaxDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Invoice_tax_details_id")
    private Integer invoiceTaxDetailsId;
    @Basic(optional = false)
    @Column(name = "Tax_category")
    private String taxCategory;
    @Basic(optional = false)
    @Column(name = "Tax")
    private String tax;
    @Basic(optional = false)
    @Column(name = "Tax_rate")
    private double taxRate;
    @JoinColumn(name = "Invoice_id", referencedColumnName = "Invoice_id")
    @ManyToOne(optional = false)
    private InvoiceDef invoiceId;

    public InvoiceTaxDetails() {
    }

    public InvoiceTaxDetails(Integer invoiceTaxDetailsId) {
        this.invoiceTaxDetailsId = invoiceTaxDetailsId;
    }

    public InvoiceTaxDetails(Integer invoiceTaxDetailsId, String taxCategory, String tax, int taxRate, InvoiceDef invoiceId) {
        this.invoiceTaxDetailsId = invoiceTaxDetailsId;
        this.taxCategory = taxCategory;
        this.tax = tax;
        this.taxRate = taxRate;
        this.invoiceId = invoiceId;
    }

    public Integer getInvoiceTaxDetailsId() {
        return invoiceTaxDetailsId;
    }

    public void setInvoiceTaxDetailsId(Integer invoiceTaxDetailsId) {
        this.invoiceTaxDetailsId = invoiceTaxDetailsId;
    }

    public String getTaxCategory() {
        return taxCategory;
    }

    public void setTaxCategory(String taxCategory) {
        this.taxCategory = taxCategory;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public InvoiceDef getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(InvoiceDef invoiceId) {
        this.invoiceId = invoiceId;
    }

}
