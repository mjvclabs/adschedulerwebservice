/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.model.entity;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author user
 */
@Entity
@Table(name = "invoice_def")

public class InvoiceDef implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Invoice_id")
    private Integer invoiceid;
    @Basic(optional = false)
    @Column(name = "Invoice_type")
    private int invoicetype;
    @Basic(optional = false)
    @Column(name = "client")
    private String client;
    @Basic(optional = false)
    @Column(name = "Billing_client")
    private String billingclient;
    @Basic(optional = false)
    @Column(name = "Commission_client")
    private String commissionclient;
    @Basic(optional = false)
    @Column(name = "Commission_rate")
    private int commissionrate;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @Column(name = "Total_amount")
    private String totalamount;
    @Basic(optional = false)
    @Column(name = "Adjust_amount")
    private String adjustAmount = "0";
    @Basic(optional = false)
    @Column(name = "Print_status")
    private int printstatus;
    @Basic(optional = false)
    @Column(name = "sap_file_path")
    private String sapFilePath = "0";
    @JoinColumn(name = "Work_order_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workorderid;
    @Basic(optional = false)
    @Column(name = "Work_order_peri0d")
    private String workOrdPeriod = "";

    public InvoiceDef() {
    }

    public InvoiceDef(Integer invoiceid) {
        this.invoiceid = invoiceid;
    }

    public InvoiceDef(Integer invoiceid, int invoicetype, String client, String billingclient, String commissionclient, int commissionrate, Date date, String totalamount) {
        this.invoiceid = invoiceid;
        this.invoicetype = invoicetype;
        this.client = client;
        this.billingclient = billingclient;
        this.commissionclient = commissionclient;
        this.commissionrate = commissionrate;
        this.date = date;
        this.totalamount = totalamount;
    }

    public Integer getInvoiceid() {
        return invoiceid;
    }

    public void setInvoiceid(Integer invoiceid) {
        this.invoiceid = invoiceid;
    }

    public int getInvoicetype() {
        return invoicetype;
    }

    public void setInvoicetype(int invoicetype) {
        this.invoicetype = invoicetype;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBillingclient() {
        return billingclient;
    }

    public void setBillingclient(String billingclient) {
        this.billingclient = billingclient;
    }

    public String getCommissionclient() {
        return commissionclient;
    }

    public void setCommissionclient(String commissionclient) {
        this.commissionclient = commissionclient;
    }

    public int getCommissionrate() {
        return commissionrate;
    }

    public void setCommissionrate(int commissionrate) {
        this.commissionrate = commissionrate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public int getPrintstatus() {
        return printstatus;
    }

    public void setPrintstatus(int printstatus) {
        this.printstatus = printstatus;
    }

    public WorkOrder getWorkorderid() {
        return workorderid;
    }

    public void setWorkorderid(WorkOrder workorderid) {
        this.workorderid = workorderid;
    }

    public String getAdjustAmount() {
        return adjustAmount;
    }

    public void setAdjustAmount(String adjustAmount) {
        this.adjustAmount = adjustAmount;
    }

    public String getWorkOrdPeriod() {
        return workOrdPeriod;
    }

    public void setWorkOrdPeriod(String workOrdPeriod) {
        this.workOrdPeriod = workOrdPeriod;
    }

    public String getSapFilePath() {
        return sapFilePath;
    }

    public void setSapFilePath(String sapFilePath) {
        this.sapFilePath = sapFilePath;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invoiceid != null ? invoiceid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoiceDef)) {
            return false;
        }
        InvoiceDef other = (InvoiceDef) object;
        if ((this.invoiceid == null && other.invoiceid != null) || (this.invoiceid != null && !this.invoiceid.equals(other.invoiceid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.InvoiceDef[ invoiceid=" + invoiceid + " ]";
    }

}
