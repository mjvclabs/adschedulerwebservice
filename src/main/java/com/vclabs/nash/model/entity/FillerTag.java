package com.vclabs.nash.model.entity;

import javax.persistence.*;

/**
 * Created by Nalaka on 2020-01-02.
 */
@Entity
@Table(name = "filler_tag")
public class FillerTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "channel_id")
    private ChannelDetails channelDetails;

    @ManyToOne
    @JoinColumn(name = "advert_id")
    private Advertisement advertisement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChannelDetails getChannelDetails() {
        return channelDetails;
    }

    public void setChannelDetails(ChannelDetails channelDetails) {
        this.channelDetails = channelDetails;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }
}
