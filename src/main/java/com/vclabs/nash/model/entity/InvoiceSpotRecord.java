/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "invoice_spot_record")

public class InvoiceSpotRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Invoice_Spot_record_id")
    private Integer invoiceSpotrecordId;
    @Basic(optional = false)
    @Column(name = "Content_type")
    private String ContentType;
    @Basic(optional = false)
    @Column(name = "Air_spot")
    private int airSpot;
    @Basic(optional = false)
    @Column(name = "All_spots")
    private int allSpots;
    @Basic(optional = false)
    @Column(name = "Package_amount")
    private String packageAmount;

    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channelId;

    @JoinColumn(name = "Invoice_id", referencedColumnName = "Invoice_id")
    @ManyToOne(optional = false)
    private InvoiceDef invoiceId;

    public InvoiceSpotRecord() {
    }

    public InvoiceSpotRecord(Integer invoiceSpotrecordId) {
        this.invoiceSpotrecordId = invoiceSpotrecordId;
    }

    public InvoiceSpotRecord(Integer invoiceSpotrecordId, String ContentType, int airSpot, int allSpots, String packageAmount, InvoiceDef invoiceId) {
        this.invoiceSpotrecordId = invoiceSpotrecordId;
        this.ContentType = ContentType;
        this.airSpot = airSpot;
        this.allSpots = allSpots;
        this.packageAmount = packageAmount;
        this.invoiceId = invoiceId;
    }

    public Integer getInvoiceSpotrecordId() {
        return invoiceSpotrecordId;
    }

    public void setInvoiceSpotrecordId(Integer invoiceSpotrecordId) {
        this.invoiceSpotrecordId = invoiceSpotrecordId;
    }

    public String getContentType() {
        return ContentType;
    }

    public void setContentType(String ContentType) {
        this.ContentType = ContentType;
    }

    public int getAirSpot() {
        return airSpot;
    }

    public void setAirSpot(int airSpot) {
        this.airSpot = airSpot;
    }

    public int getAllSpots() {
        return allSpots;
    }

    public void setAllSpots(int allSpots) {
        this.allSpots = allSpots;
    }

    public String getPackageAmount() {
        return packageAmount;
    }

    public void setPackageAmount(String packageAmount) {
        this.packageAmount = packageAmount;
    }

    public InvoiceDef getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(InvoiceDef invoiceId) {
        this.invoiceId = invoiceId;
    }

    public ChannelDetails getChannelId() {
        return channelId;
    }

    public void setChannelId(ChannelDetails channelId) {
        this.channelId = channelId;
    }

}
