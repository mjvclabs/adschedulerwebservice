/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author Sanira Nanayakkara
 */
@Entity
@Table(name = "time_belts")
public class TBelt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "time_belt_id")
    private Integer timeBeltId;
    @Basic(optional = false)
    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channelid;
    @Basic(optional = false)
    @Column(name = "hour")
    private int hour;
    @Column(name = "period")
    private Integer period;
    @Basic(optional = false)
    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @Basic(optional = false)
    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @Basic(optional = false)
    @Column(name = "cluster_count")
    private int clusterCount;
    @Column(name = "is_schedule_generated")
    private Integer isScheduleGenerated;
    @Column(name = "schedule_generated_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleGeneratedTime;
    @Column(name = "total_schedule_time")
    private long totalScheduleTime = 0; //seconds
    @Transient
    private long utilizedScheduleTime = 0;

    public TBelt() {
    }

    public TBelt(Integer timeBeltId) {
        this.timeBeltId = timeBeltId;
    }

    public TBelt(Integer timeBeltId, int hour, Date startTime, Date endTime) {
        this.timeBeltId = timeBeltId;
        this.hour = hour;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getTimeBeltId() {
        return timeBeltId;
    }

    public void setTimeBeltId(Integer timeBeltId) {
        this.timeBeltId = timeBeltId;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Boolean getIsScheduleGenerated() {
        return isScheduleGenerated != 0;
    }

    public void setIsScheduleGenerated(Boolean isScheduleGenerated) {
        this.isScheduleGenerated = (isScheduleGenerated) ? 1 : 0;
    }

    public Date getScheduleGeneratedTime() {
        return scheduleGeneratedTime;
    }

    public void setScheduleGeneratedTime(Date scheduleGeneratedTime) {
        this.scheduleGeneratedTime = scheduleGeneratedTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (timeBeltId != null ? timeBeltId.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.TimeBelts[ timeBeltId=" + timeBeltId + " ]";
    }

    public ChannelDetails getChannelid() {
        return channelid;
    }

    public void setChannelid(ChannelDetails channelid) {
        this.channelid = channelid;
    }

    public int getClusterCount() {
        return clusterCount;
    }

    public void setClusterCount(int clusterCount) {
        this.clusterCount = clusterCount;
    }

    public long getTotalScheduleTime() {
        return totalScheduleTime;
    }

    public void setTotalScheduleTime(long totalScheduleTime) {
        this.totalScheduleTime = totalScheduleTime;
    }

    public long getUtilizedScheduleTime() {
        return utilizedScheduleTime;
    }

    public void setUtilizedScheduleTime(long utilizedScheduleTime) {
        this.utilizedScheduleTime = utilizedScheduleTime;
    }

    public void addUtilizedScheduleTime(long utilizedScheduleTime) {
        this.utilizedScheduleTime += utilizedScheduleTime;
    }

}
