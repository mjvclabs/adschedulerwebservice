/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Sanira Nanayakkara
 */
@Entity
@Table(name = "logo_container")
public class LogoContainer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "container_id")
    private Integer containerId;
    @Basic(optional = false)
    @Column(name = "container_name")
    private String containerName;
    @Basic(optional = false)
    @Column(name = "x_coordinate")
    private Integer xCoordinate;
    @Basic(optional = false)
    @Column(name = "y_coordinate")
    private Integer yCoordinate;
    @Basic(optional = false)
    @Column(name = "width")
    private Integer width;
    @Basic(optional = false)
    @Column(name = "height")
    private Integer height;
    @Basic(optional = false)
    @Column(name = "alignment")
    private String alignment;
    @Basic(optional = false)
    @Column(name = "opacity")
    private Integer opacity;
    @Basic(optional = false)
    @Column(name = "interval")
    private Integer interval;

    public LogoContainer() {
    }

    ;
    
    public LogoContainer(String containerName, Integer xCoordinate, Integer yCoordinate, Integer width, Integer height, String alignment, double opacity, Integer interval) {
        this.containerId = 0;
        this.containerName = containerName;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.width = width;
        this.height = height;
        this.alignment = alignment;
        this.setOpacity(opacity);
        this.interval = interval;
    }

    public LogoContainer(int id, String containerName, Integer xCoordinate, Integer yCoordinate, Integer width, Integer height, String alignment, double opacity, Integer interval) {
        this.containerId = id;
        this.containerName = containerName;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.width = width;
        this.height = height;
        this.alignment = alignment;
        this.setOpacity(opacity);
        this.interval = interval;
    }

    public Integer getContainerId() {
        return containerId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public Integer getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(Integer xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Integer getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(Integer yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getAlignment() {
        return alignment;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public double getOpacity() {
        return opacity / 1000;
    }

    public void setOpacity(double opacity) {
        this.opacity = (int) (opacity * 1000);
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }
}
