/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import com.vclabs.nash.model.dto.AdvertSpread;
import com.vclabs.nash.model.dto.TimeSlotScheduleMapDto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 *
 * @author user
 */
@Entity
@Table(name = "time_slot_schedule_map")
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "TimeSlotScheduleMapping",
                classes = {
                        @ConstructorResult(
                                targetClass = TimeSlotScheduleMapDto.class,
                                columns = {
                                        @ColumnResult(name = "Schedule_id"),
                                        @ColumnResult(name = "time_belt_id"),
                                        @ColumnResult(name = "Date")
                                }
                        )
                }
        ),
        @SqlResultSetMapping(
                name = "advertSpreadMapping",
                classes = {
                        @ConstructorResult(
                                targetClass = AdvertSpread.class,
                                columns = {
                                        @ColumnResult(name = "timebeltId"),
                                        @ColumnResult(name = "adCount")
                                }
                        )
                }
        )
})
public class TimeSlotScheduleMap implements Serializable, Comparable<TimeSlotScheduleMap> {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @Column(name = "workOrderId")
    private int workOrderId;
    @JoinColumn(name = "Time_belt_id", referencedColumnName = "time_belt_id")
    @ManyToOne(optional = false)
    private TimeBelts timeslotid;
    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channelid;

    @Id
    @JoinColumn(name = "Schedule_id", referencedColumnName = "Schedule_id")
    @OneToOne(optional = false)
    private ScheduleDef scheduleid;

    public TimeSlotScheduleMap() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(int workOrderId) {
        this.workOrderId = workOrderId;
    }

    public TimeBelts getTimeBelt() {
        return timeslotid;
    }

    public void setTimeBelt(TimeBelts timeslotid) {
        this.timeslotid = timeslotid;
    }

    public ChannelDetails getChannelid() {
        return channelid;
    }

    public void setChannelid(ChannelDetails channelid) {
        this.channelid = channelid;
    }

    public ScheduleDef getScheduleid() {
        return scheduleid;
    }

    public void setScheduleid(ScheduleDef scheduleid) {
        this.scheduleid = scheduleid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (scheduleid.getScheduleid() != null ? scheduleid.getScheduleid().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TimeSlotScheduleMap)) {
            return false;
        }
        TimeSlotScheduleMap other = (TimeSlotScheduleMap) object;
        if ((this.scheduleid.getScheduleid() == null && other.scheduleid.getScheduleid() != null) || (this.scheduleid.getScheduleid() != null && !this.scheduleid.getScheduleid().equals(other.scheduleid.getScheduleid()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.TimeSlotScheduleMap[ timeslotschedulemapid=" + scheduleid.getScheduleid() + " ]";
    }

    @Override
    public int compareTo(TimeSlotScheduleMap o) {
        if (o.getTimeBelt().getTimeBeltId() == this.getTimeBelt().getTimeBeltId()) {
            return (int) (this.getDate().getTime() - o.getDate().getTime());
        } else {
            return this.getTimeBelt().getTimeBeltId() - o.getTimeBelt().getTimeBeltId();
        }
    }
}
