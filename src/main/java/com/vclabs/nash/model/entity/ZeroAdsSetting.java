package com.vclabs.nash.model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Nalaka on 2018-09-18.
 */
@Entity
public class ZeroAdsSetting implements Serializable {

    public enum PlayListType {

        MANUALPLAYLIST("MANUALPLAYLIST"), AUTOPLAYLIST("AUTOPLAYLIST");

        private String value;

        private PlayListType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    };
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private ChannelDetails channelId;

    private PlayListType pListType = PlayListType.MANUALPLAYLIST;

    public ZeroAdsSetting() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ChannelDetails getChannelId() {
        return channelId;
    }

    public void setChannelId(ChannelDetails channelId) {
        this.channelId = channelId;
    }

    public PlayListType getpListType() {
        return pListType;
    }

    public void setpListType(PlayListType pListType) {
        this.pListType = pListType;
    }
}
