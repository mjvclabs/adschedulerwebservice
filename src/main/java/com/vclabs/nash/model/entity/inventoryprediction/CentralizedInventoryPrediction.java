package com.vclabs.nash.model.entity.inventoryprediction;

import com.vclabs.nash.model.entity.ChannelDetails;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

/**
 * Created by Sanduni on 11/10/2018
 */
@Entity
@Table(name = "centralized_inventory_prediction")
public class CentralizedInventoryPrediction {

    public enum PredictionType {
        DAILY_INVENTORY,
        WEEKLY_INVENTORY,
        MONTHLY_INVENTORY
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date dateAndTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date date;

    private Time fromTime;

    private Time toTime;

    private double inventory;

    @OneToOne
    private ChannelDetails channelDetails;

    private PredictionType predictionType;

    private long reference;

    private Boolean hasUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getFromTime() {
        return fromTime;
    }

    public void setFromTime(Time fromTime) {
        this.fromTime = fromTime;
    }

    public Time getToTime() {
        return toTime;
    }

    public void setToTime(Time toTime) {
        this.toTime = toTime;
    }

    public double getInventory() {
        return inventory;
    }

    public void setInventory(double inventory) {
        this.inventory = inventory;
    }

    public ChannelDetails getChannelDetails() {
        return channelDetails;
    }

    public void setChannelDetails(ChannelDetails channelDetails) {
        this.channelDetails = channelDetails;
    }

    public PredictionType getPredictionType() {
        return predictionType;
    }

    public void setPredictionType(PredictionType predictionType) {
        this.predictionType = predictionType;
    }

    public long getReference() {
        return reference;
    }

    public void setReference(long reference) {
        this.reference = reference;
    }

    public Boolean getHasUpdated() {
        return hasUpdated;
    }

    public void setHasUpdated(Boolean hasUpdated) {
        this.hasUpdated = hasUpdated;
    }
}
