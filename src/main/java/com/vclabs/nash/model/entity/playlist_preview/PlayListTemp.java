package com.vclabs.nash.model.entity.playlist_preview;

import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.entity.WorkOrder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dperera on 21/10/2019.
 */
@Entity
@Table(name = "play_list_temp")
public class PlayListTemp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "Schedule_hour")
    private Integer scheduleHour;

    @Basic(optional = false)
    @Column(name = "Schedule_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleStartTime;

    @Basic(optional = false)
    @Column(name = "Schedule_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleEndTime;

    @Basic(optional = false)
    @Column(name = "TimeBelt_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeBeltStartTime;

    @Basic(optional = false)
    @Column(name = "TimeBelt_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeBeltEndTime;

    @Basic(optional = false)
    @Column(name = "Actual_start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualStartTime;

    @Basic(optional = false)
    @Column(name = "Actual_end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualEndTime;

    @Basic(optional = false)
    @Column(name = "Cluster_priority")
    private Integer clusterPriority = -1;

    // ##### 0-Initial_State #####
    // ##### 1-Scheduled_State #####
    // ##### 2-Played_State #####
    // ##### 3-Stopped_State #####
    // ##### 5-NotPlayed_State #####
    // ##### 6-StoppedbyUser_State #####
    // ##### 7-Rescheduled_State #####
    // ##### 8-Invalid_State ##### //No_MediaFile
    // ##### 9-Hold_advert #####
    // ##### 10-Partially airing ##-This status only for the LOGO-###
    @Basic(optional = false)
    @Column(name = "Status")
    private String status;

    @Basic(optional = false)
    @Column(name = "Play_cluster")
    private Integer playCluster;

    @Basic(optional = false)
    @Column(name = "Play_order")
    private Integer playOrder;

    @Basic(optional = false)
    @Column(name = "Comment")
    private String comment;

    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Basic(optional = false)
    @Column(name = "Retry_count")
    private int retryCount;

    @JoinColumn(name = "Advert_id", referencedColumnName = "Advert_id")
    @ManyToOne(optional = false)
    private Advertisement advert;

    @JoinColumn(name = "Channel_id", referencedColumnName = "Channel_id")
    @ManyToOne(optional = false)
    private ChannelDetails channel;

    @JoinColumn(name = "Schedule_id", referencedColumnName = "Schedule_id")
    @ManyToOne(optional = false)
    private ScheduleDef schedule;

    @JoinColumn(name = "Work_order_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workOrder;

    @Basic(optional = false)
    @Column(name = "conflicting_schedules")
    private ArrayList<Integer> conflictingSchedules = new ArrayList<>();

    @Basic(optional = false)
    @Column(name = "sequence_num")
    private int sequenceNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScheduleHour() {
        return scheduleHour;
    }

    public void setScheduleHour(Integer scheduleHour) {
        this.scheduleHour = scheduleHour;
    }

    public Date getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(Date scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public Date getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(Date scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public Date getTimeBeltStartTime() {
        return timeBeltStartTime;
    }

    public void setTimeBeltStartTime(Date timeBeltStartTime) {
        this.timeBeltStartTime = timeBeltStartTime;
    }

    public Date getTimeBeltEndTime() {
        return timeBeltEndTime;
    }

    public void setTimeBeltEndTime(Date timeBeltEndTime) {
        this.timeBeltEndTime = timeBeltEndTime;
    }

    public Date getActualStartTime() {
        return actualStartTime;
    }

    public void setActualStartTime(Date actualStartTime) {
        this.actualStartTime = actualStartTime;
    }

    public Date getActualEndTime() {
        return actualEndTime;
    }

    public void setActualEndTime(Date actualEndTime) {
        this.actualEndTime = actualEndTime;
    }

    public Integer getClusterPriority() {
        return clusterPriority;
    }

    public void setClusterPriority(Integer clusterPriority) {
        this.clusterPriority = clusterPriority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPlayCluster() {
        return playCluster;
    }

    public void setPlayCluster(Integer playCluster) {
        this.playCluster = playCluster;
    }


    public Integer getPlayOrder() {
        return playOrder;
    }

    public void setPlayOrder(Integer playOrder) {
        this.playOrder = playOrder;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public Advertisement getAdvert() {
        return advert;
    }

    public void setAdvert(Advertisement advert) {
        this.advert = advert;
    }

    public ChannelDetails getChannel() {
        return channel;
    }

    public void setChannel(ChannelDetails channel) {
        this.channel = channel;
    }

    public ScheduleDef getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleDef schedule) {
        this.schedule = schedule;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public ArrayList<Integer> getConflictingSchedules() {
        return conflictingSchedules;
    }

    public void setConflictingSchedules(ArrayList<Integer> conflictingSchedules) {
        this.conflictingSchedules = conflictingSchedules;
    }

    public void addConflicting_schedules(Integer conflicting_schedules) {
        this.conflictingSchedules.add(conflicting_schedules);
    }


}
