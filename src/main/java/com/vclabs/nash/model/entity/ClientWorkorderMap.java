/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "client_workorder_map")
public class ClientWorkorderMap implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Client_workOrder_id")
    private Integer clientworkOrderid;
    @JoinColumn(name = "End_client", referencedColumnName = "Client_id")
    @ManyToOne(optional = false)
    private ClientDetails endclient;
    @JoinColumn(name = "First_client", referencedColumnName = "Client_id")
    @ManyToOne(optional = false)
    private ClientDetails firstclient;
    @JoinColumn(name = "WorkOrder_id", referencedColumnName = "Work_order_id")
    @ManyToOne(optional = false)
    private WorkOrder workOrderid;

    public ClientWorkorderMap() {
    }

    public ClientWorkorderMap(Integer clientworkOrderid) {
        this.clientworkOrderid = clientworkOrderid;
    }

    public Integer getClientworkOrderid() {
        return clientworkOrderid;
    }

    public void setClientworkOrderid(Integer clientworkOrderid) {
        this.clientworkOrderid = clientworkOrderid;
    }

    public ClientDetails getEndclient() {
        return endclient;
    }

    public void setEndclient(ClientDetails endclient) {
        this.endclient = endclient;
    }

    public ClientDetails getFirstclient() {
        return firstclient;
    }

    public void setFirstclient(ClientDetails firstclient) {
        this.firstclient = firstclient;
    }

    public WorkOrder getWorkOrderid() {
        return workOrderid;
    }

    public void setWorkOrderid(WorkOrder workOrderid) {
        this.workOrderid = workOrderid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientworkOrderid != null ? clientworkOrderid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientWorkorderMap)) {
            return false;
        }
        ClientWorkorderMap other = (ClientWorkorderMap) object;
        if ((this.clientworkOrderid == null && other.clientworkOrderid != null) || (this.clientworkOrderid != null && !this.clientworkOrderid.equals(other.clientworkOrderid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.ClientWorkorderMap[ clientworkOrderid=" + clientworkOrderid + " ]";
    }

}
