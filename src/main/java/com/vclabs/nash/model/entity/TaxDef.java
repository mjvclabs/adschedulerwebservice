/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "tax_def")

public class TaxDef implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("taxid")
    @Basic(optional = false)
    @Column(name = "Tax_id")
    private Integer taxid;
    
    @JsonProperty("taxcategory")
    @Basic(optional = false)
    @Column(name = "Tax_category")
    private String taxcategory;
    
    @JsonProperty("rate")
    @Basic(optional = false)
    @Column(name = "rate")
    private double rate;

    public TaxDef() {
    }

    public TaxDef(Integer taxid) {
        this.taxid = taxid;
    }

    public TaxDef(Integer taxid, String taxcategory, int rate) {
        this.taxid = taxid;
        this.taxcategory = taxcategory;
        this.rate = rate;
    }

    public Integer getTaxid() {
        return taxid;
    }

    public void setTaxid(Integer taxid) {
        this.taxid = taxid;
    }

    public String getTaxcategory() {
        return taxcategory;
    }

    public void setTaxcategory(String taxcategory) {
        this.taxcategory = taxcategory;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (taxid != null ? taxid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TaxDef)) {
            return false;
        }
        TaxDef other = (TaxDef) object;
        if ((this.taxid == null && other.taxid != null) || (this.taxid != null && !this.taxid.equals(other.taxid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vclabs.nash.model.entity.TaxDef[ taxid=" + taxid + " ]";
    }

}
