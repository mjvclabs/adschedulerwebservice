package com.vclabs.nash.model.entity.inventoryprediction;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Nalaka on 2018-10-11.
 */
@Entity
@Table(name = "prediction_result_meta")
public class PredictionResultMeta {

    public enum InventoryType {

        DAILY, WEEKLY, MONTHLY
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date date;

    private String fileName;

    private InventoryType inventoryType;

    private Boolean isProcess = Boolean.FALSE;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public InventoryType getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(InventoryType inventoryType) {
        this.inventoryType = inventoryType;
    }

    public Boolean getProcess() {
        return isProcess;
    }

    public void setProcess(Boolean process) {
        isProcess = process;
    }
}
