package com.vclabs.nash.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/dashboard")
public class DashboardModuleController {

    @RequestMapping(value = {"/manual"}, method = RequestMethod.GET)
    public String viewManualChannelPage () {
        return "dashboard/index-manual";
    }

    @RequestMapping(value = {"/auto"}, method = RequestMethod.GET)
    public String viewAutoChannelPage () {
        return "dashboard/index-auto";
    }
}
