package com.vclabs.nash.controller.mvc;


import com.vclabs.nash.model.view.DashboadChannelView;
import com.vclabs.nash.service.DashboardService;
import com.vclabs.nash.service.ReportingURLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/reporting")
public class ReportingModuleController {

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private ReportingURLService reportingURLService;

    @RequestMapping(value = {"/user-access"}, method = RequestMethod.GET)
    public String userAccessPage () {
        return reportingURLService.selectedUserAccessPage();
    }

    @RequestMapping(value = {"/dummy-cut"}, method = RequestMethod.GET)
    public String viewDummyCutPage () {
        return "reporting/dummy-cut";
    }

    @RequestMapping(value = {"/commercial-availability-hourly"}, method = RequestMethod.GET)
    public String viewCommercialAvailabilityPage () {
        return "reporting/commercial-availability-hourly";
    }

    @RequestMapping(value = {"/commercial-availability-channelwise"}, method = RequestMethod.GET)
    public String viewCommercialAvailabilityChannelWisePage () {
        return "reporting/commercial-availability-channelwise";
    }

    @RequestMapping(value = {"/schedule-print"}, method = RequestMethod.GET)
    public String viewSchedulePrintPage () {
        return "reporting/schedule-print";
    }

    @RequestMapping(value = {"/schedule-analysis"}, method = RequestMethod.GET)
    public String viewScheduleAnalysisPage () {
        return "reporting/schedule-analysis";
    }

    @RequestMapping(value = {"/client-revenue-report"}, method = RequestMethod.GET)
    public String clientRevenuePage () {
        return "reporting/client-revenue-report";
    }

    @RequestMapping(value = {"/agency-revenue-report"}, method = RequestMethod.GET)
    public String agencyRevenuePage () {
        return "reporting/agency-revenue-report";
    }

    @RequestMapping(value = {"/sales-report"}, method = RequestMethod.GET)
    public String salesReportPage () {
        return "reporting/sales-report";
    }

    @RequestMapping(value = {"/paymentdue"}, method = RequestMethod.GET)
    public String paymentduePage () {
        return "reporting/paymentdue";
    }

    @RequestMapping(value = {"/material-analysis"}, method = RequestMethod.GET)
    public String materialAnalysisPage () {
        return "reporting/material-analysis";
    }

    @RequestMapping(value = {"/media-deletion-history"}, method = RequestMethod.GET)
    public String mediaDeletionHistoryPage () {
        return "reporting/media-deletion-history";
    }

    @RequestMapping(value = {"/missed-spot-count"}, method = RequestMethod.GET)
    public String missedSpotCountPage () {
        return "reporting/missed-spot-count";
    }

    @RequestMapping(value = {"/schedule-view"}, method = RequestMethod.GET)
    public String scheduleViewPage (@RequestParam("work_id") int woId, @RequestParam("work_name") String woName,
                                        @RequestParam("start_date") String startDate,  @RequestParam("end_date") String endDate, ModelMap model) {
        model.addAttribute("work_id", woId);
        model.addAttribute("work_name", woName);
        model.addAttribute("start_date", startDate);
        model.addAttribute("end_date", endDate);
        return "reporting/schedule-view";
    }

    @RequestMapping(value = {"/dashboard-auto"}, method = RequestMethod.GET)
    public String dashBoardAutoChannelPage () {
        return "reporting/dashboard-auto";
    }

    @RequestMapping(value = {"/dashboard-manual"}, method = RequestMethod.GET)
    public String dashBoardManualChannelPage () {
        return "reporting/dashboard-manual";
    }

    @RequestMapping(value = "/dashboard-auto/getAutoChannel", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DashboadChannelView> getAutoAdvertisement(@RequestParam("selectedDate") String selectedDate) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dashboardService.getManualORAutoChannelList(0, dateFormat.parse(selectedDate));
    }

    @RequestMapping(value = "dashboard-manual/getManualChannel", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DashboadChannelView> getManualAdvertisement(@RequestParam("selectedDate") String selectedDate) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dashboardService.getManualORAutoChannelList(1, dateFormat.parse(selectedDate));
    }

    @RequestMapping(value = "user-wise-report", method = RequestMethod.GET, produces = "application/json")
    public String getUserWiseReportPage() {
        return "reporting/user-wise-report";
    }

}