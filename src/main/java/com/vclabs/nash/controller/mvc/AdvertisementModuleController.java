package com.vclabs.nash.controller.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/advertisement-v4")
public class AdvertisementModuleController {

    @RequestMapping(method = RequestMethod.GET)
    public String viewAdvertisementHomePage(HttpServletRequest request, Model model) {
        return "all-advertisement";
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    public String viewCreateAdvertisementPage(HttpServletRequest request, Model model) {
        return "add-advertisement";
    }

    @RequestMapping(value = {"/update"}, method = RequestMethod.GET)
    public String viewUpdateAdvertisementPage(HttpServletRequest request, Model model) {
        return "update-advertisement";
    }

}