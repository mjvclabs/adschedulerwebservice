package com.vclabs.nash.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Nalaka on 2018-10-30.
 */
@Controller
@RequestMapping("/error")
public class ExceptionCatchingController {

    @RequestMapping(value = {"/access-denied"}, method = RequestMethod.GET)
    public String accessDeniedError(){
        return "permission-denied";
    }
}
