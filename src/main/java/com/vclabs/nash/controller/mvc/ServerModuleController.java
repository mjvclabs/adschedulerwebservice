package com.vclabs.nash.controller.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dperera on 17/05/18.
 */

@Controller
@RequestMapping("/servers")
public class ServerModuleController {

    @RequestMapping(value = {"/list"}, method = RequestMethod.GET)
    public String viewGeneratedBillPage(HttpServletRequest request, Model model) {
        return "all-servers";
    }

}