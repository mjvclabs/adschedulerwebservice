package com.vclabs.nash.controller.login;


import com.vclabs.nash.controller.utill.UrlUtilsComponent;
import com.vclabs.nash.model.view.UserView;
import com.vclabs.nash.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dperera on 3/27/17.
 */

@Controller
@RequestMapping
public class LoginController {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UrlUtilsComponent urlUtilsComponent;

    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String viewSigninPage(HttpServletRequest request, Model model) {
        return "login";
    }

    @RequestMapping(value = "/signin-error", method = RequestMethod.GET)
    public String viewLoginErrorPage(Model model,  final RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("loginError", true);
        return "redirect:/signin";
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String viewHomePage(HttpServletRequest request, Model model) {
        String user = userDetailsService.getLoginUser().getUserName();
        model.addAttribute("dashboardUrl", urlUtilsComponent.getDashboardRootUrl(request));
        model.addAttribute("current_user", user);
        return "home";
    }

}