/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.vclabs.nash.model.entity.Advertisement;

import com.vclabs.nash.model.entity.LogoContainer;
import com.vclabs.nash.service.LogoContainerService;

/**
 * @author Sanira Nanayakkara
 */
@RestController
@RequestMapping("/logo_container")
public class LogoContainerController {

    @Autowired
    private LogoContainerService logoService;

    @RequestMapping(value = "/getall", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ArrayList<LogoContainer> getAllLogoContainer() {
        return logoService.getLogoContainerList();
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean logoContainerUpdate(
            @RequestParam("id") int id,
            @RequestParam("name") String containerName,
            @RequestParam("x_coordinate") int xCoordinate,
            @RequestParam("y_coordinate") int yCoordinate,
            @RequestParam("width") int width,
            @RequestParam("height") int height,
            @RequestParam("alignment") String alignment,
            @RequestParam("opacity") double opacity,
            @RequestParam("interval") int interval) {

        return logoService.updateLogoContainer(new LogoContainer(id, containerName, xCoordinate, yCoordinate, width, height, alignment, height, interval));
    }

    @RequestMapping(value = "/insert", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean logoContainerInsert(
            @RequestParam("name") String containerName,
            @RequestParam("x_coordinate") int xCoordinate,
            @RequestParam("y_coordinate") int yCoordinate,
            @RequestParam("width") int width,
            @RequestParam("height") int height,
            @RequestParam("alignment") String alignment,
            @RequestParam("opacity") double opacity,
            @RequestParam("interval") int interval) {
        return logoService.insertLogoContainer(new LogoContainer(containerName, xCoordinate, yCoordinate, width, height, alignment, opacity, interval));
    }
    
    @RequestMapping(value = "/getLogoListbyId/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Advertisement> getLogoList(@RequestParam("id") int id) {
        return logoService.getLogoList(id);
    }
    
    @RequestMapping(value = "/insertLogoListtoContainer/json", method = RequestMethod.GET, produces = "application/json")
    public
    Boolean insertLogoList(@RequestParam("container_id") int id, @RequestParam("logo_ids") List<Integer> lstLogos) {
        logoService.insertLogoList(id, lstLogos);
        return true;
    }
}
