/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vclabs.nash.service.ScheduleAsyncService;
import com.vclabs.nash.service.SchedulerSubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vclabs.nash.model.entity.TimeSlot;
import com.vclabs.nash.model.process.MissedSpotFilters;
import com.vclabs.nash.model.process.OneDayScheduleInfo;
import com.vclabs.nash.model.view.AdvertisementDefView;
import com.vclabs.nash.model.view.Message;
import com.vclabs.nash.model.view.MissedSpotView;
import com.vclabs.nash.model.view.OnedayScheduleView;
import com.vclabs.nash.model.view.PreScheduleInsertView;
import com.vclabs.nash.model.view.PreScheduleReplaceView;
import com.vclabs.nash.model.view.RemainingSpotView;
import com.vclabs.nash.model.view.ScheduleAdminView;
import com.vclabs.nash.model.view.ScheduleAdvertView;
import com.vclabs.nash.model.view.ScheduleUpdateView;
import com.vclabs.nash.model.view.ScheduleWebView;
import com.vclabs.nash.model.view.TimeBeltUtilization;
import com.vclabs.nash.model.view.WorkOrderChannelScheduleView;
import com.vclabs.nash.model.view.WorkOrderScheduleView;
import com.vclabs.nash.service.SchedulerService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/scheduler")
public class ScheduleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleController.class);

    @Autowired
    private SchedulerService scheduleService;

    @Autowired
    private ScheduleAsyncService scheduleAsyncService;

    @Autowired
    private SchedulerSubService schedulerSubService;

    @RequestMapping(value = "/scheduleList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ScheduleWebView> scheduleList() {
        return scheduleService.getScheduleList();
    }

    @RequestMapping(value = "/scheduleListASC/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ScheduleUpdateView> scheduleListASC(@RequestParam("workOrderId") int workOrderId) {
        return scheduleService.getScheduleListOrderByASC(workOrderId);
    }

    @RequestMapping(value = "/getWOScheduleView/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    WorkOrderScheduleView getWOScheduleView(@RequestParam("workOrderId") int workOrderId, @RequestParam("channelId") int selectedChannelId) {
        WorkOrderScheduleView workOrderScheduleView = null;
        try{
          //workOrderScheduleView = scheduleService.getWOScheduleViewData(workOrderId, selectedChannelId, true);
            workOrderScheduleView = schedulerSubService.getWOScheduleViewData(workOrderId, selectedChannelId, true);
        }catch (Exception e){
            LOGGER.debug(e.getMessage());
        }
        return workOrderScheduleView;
    }

    @RequestMapping(value = "/scheduleSave/json", method = RequestMethod.POST)
    public @ResponseBody
    WorkOrderScheduleView scheduleSave(@RequestParam("scheduleData") String scheduleData) {
        WorkOrderScheduleView savedScheduleView = scheduleService.saveWOSchedule(scheduleData);
        scheduleService.afterUpdateWOSchedule(savedScheduleView);
        WorkOrderScheduleView byScheduleDataRow = scheduleService.getByScheduleDataRow(scheduleData);
        byScheduleDataRow.setMessageDto(savedScheduleView.getMessageDto());
        return byScheduleDataRow;
    }

    @RequestMapping(value = "/scheduleSaveAndWorkOrderUpdate/json", method = RequestMethod.POST)
    public @ResponseBody
    WorkOrderScheduleView scheduleSaveAndWorkOrderUpdate(@RequestParam("scheduleData") String scheduleData) {
        WorkOrderScheduleView savedScheduleView = scheduleService.setWorkOrderStatusAdnSetSchedule(scheduleData);
        scheduleService.afterUpdateWOSchedule(savedScheduleView);
        WorkOrderScheduleView byScheduleDataRow = scheduleService.getByScheduleDataRow(scheduleData);
        byScheduleDataRow.setMessageDto(savedScheduleView.getMessageDto());
        return byScheduleDataRow;
    }

    @RequestMapping(value = "/scheduleSaveAndUpdate/json", method = RequestMethod.POST)
    public @ResponseBody
    WorkOrderScheduleView scheduleSaveAndUpdate(@RequestParam("scheduleData") String scheduleData) {
        WorkOrderScheduleView result = null;
        try {
            WorkOrderScheduleView savedScheduleView = scheduleService.updateWOSchedule(scheduleData);
            scheduleService.afterUpdateWOSchedule(savedScheduleView);
            result = scheduleService.getByScheduleDataRow(scheduleData);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = "/prorityListCount/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Integer prorityListCount(@RequestParam("scheduleIds")  String scheduleIdsData) {
        return scheduleService.checkProrityScheduleCount(scheduleIdsData);
    }

    @ResponseBody
    @RequestMapping(value = "/getMissedSpot/json", method = RequestMethod.POST)
    public List<MissedSpotView> getMissedSpot(@RequestBody MissedSpotFilters data) {
        return scheduleService.getMissedSpot_v2(data.getFromDate(), data.getToDate(), data.getChannelIDs(), data.getAdvertID(), data.getClientID(),data.getHour());
    }

    @RequestMapping(value = "/saveMissedSpot/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean saveMissedSpot(@RequestParam("scheduleData") String scheduleData, @RequestParam("scheduleId") int scheduleId) {
        return scheduleService.setRescheduleSpot(scheduleData, scheduleId);
    }

    @RequestMapping(value = "/saveMissedSpot_v2/json", method = RequestMethod.POST)
    public @ResponseBody
    Message saveMissedSpot(@RequestParam("scheduleData") String scheduleData, @RequestParam("scheduleId") String scheduleId) {
        WorkOrderScheduleView savedScheduleView = scheduleService.setRescheduleSpot_V2(scheduleData, scheduleId);
        if (savedScheduleView.getScheduleTempList() != null) {
            scheduleService.afterUpdateWOSchedule(savedScheduleView);
        }
        return savedScheduleView.getMessageDto();
    }

    @RequestMapping(value = "/saveReschedulSpot/json", method = RequestMethod.POST)
    public @ResponseBody
    Message saveReschedulSpot(@RequestParam("scheduleIds") String scheduleIds) {
        WorkOrderScheduleView savedScheduleView = scheduleService.setReschedulSoptToToday(scheduleIds);
        if(savedScheduleView.getScheduleTempList()!=null) {
            scheduleService.afterUpdateWOSchedule(savedScheduleView);
        }
        return savedScheduleView.getMessageDto();
    }

    @RequestMapping(value = "/relaseDummyCuts/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Message relaseDummyCuts(@RequestParam("dummyCutIds") String dummyCutIds) {
        return scheduleService.releseDummySpot(dummyCutIds);
    }

    @RequestMapping(value = "/scheduleDelete/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean scheduleDelete(@RequestParam("scheduleId") int scheduleId) {
        return scheduleService.deleteSchedule(scheduleId);
    }

    @RequestMapping(value = "/selectedChannelSlotCount/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    int selectedChannelSlotCount(@RequestParam("workOrderId") int workOrderId, @RequestParam("channelId") int channelId) {
        return scheduleService.getSelectedChannelSlotCount(workOrderId, channelId);
    }

    //tested
    @RequestMapping(value = "/selectedScheduleAvailableSpotsCount/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    WorkOrderChannelScheduleView selectedScheduleAvailableSpotsCount(@RequestParam("workOrderId") int workOrderId, @RequestParam("channelId") int channelId) {
         return scheduleService.getScheduleAvailableSpotCount(workOrderId, channelId);
    }

    @RequestMapping(value = "/selectedScheduleAirSpotsCount/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    WorkOrderChannelScheduleView selectedScheduleAirSpotsCount(@RequestParam("workOrderId") int workOrderId) {
        return scheduleService.getScheduleAirSpotCount(workOrderId);
    }

    @RequestMapping(value = "/selectedScheduleCount/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    int selectedScheduleAvailableSpotsCount(@RequestParam("workOrderId") int workOrderId) {
        return scheduleService.getSelectedScheduleCount(workOrderId);
    }

    @RequestMapping(value = "/selectedScheduleFromDate/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ScheduleWebView> selectedScheduleFromDate(@RequestParam("workOrderId") int workOrderId, @RequestParam("scheduleDate") String scheduleDate) {
        return scheduleService.getselectedSchedul(workOrderId, scheduleDate);

    }

    @RequestMapping(value = "/timeSlotList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TimeSlot> timeSlotList() {
        return scheduleService.getTimeSlotList();
    }

    //tested
    @RequestMapping(value = "/clientOrderAdvertList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdvertisementDefView> clientOrderAdvertList(@RequestParam("workOrderId") int workOrderId, @RequestParam("advertType") String advertType) {
        return scheduleService.getSelectedAdvert(workOrderId, advertType);
    }

    @RequestMapping(value = "/finalAllSchedule/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ScheduleAdminView> FinalAllSchedule(@RequestParam("filterData") String filterData) {
        return scheduleService.getFinalAllSchedule(filterData);
    }

    @RequestMapping(value = "/getSelectedScheduleAdvert/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ScheduleAdvertView> getSelectedScheduleAdvert(@RequestParam("selectdate") String selectdate, @RequestParam("filterData") String filterData) {
        return scheduleService.getSelectedTimeSlotAdvert(selectdate, filterData);
    }

    @RequestMapping(value = "/getOneDaySchedules/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<OnedayScheduleView> getOneDaySchedules(
            @RequestParam("workOrderId") int workOrderId,
            @RequestParam("channelId") String channelIds,
            @RequestParam("advertId") int advertId,
            @RequestParam("advertType") String advertType,
            @RequestParam("date") String date,
            @RequestParam("startHour") int startHour,
            @RequestParam("endHour") int endHour
    ) throws ParseException, IOException, ExecutionException, InterruptedException {
        ObjectMapper m = new ObjectMapper();
        List<Integer> channalIdList = m.readValue(channelIds, new TypeReference<List<Integer>>() {
        });
        List<CompletableFuture< List<OnedayScheduleView>>> allFutures = new ArrayList<>();
        List<OnedayScheduleView> returnTempList = new ArrayList<>();

        for(Integer channelId : channalIdList){
            OneDayScheduleInfo infoModel = new OneDayScheduleInfo(workOrderId, channelId, advertId, advertType, new SimpleDateFormat("yyyy/MM/dd").parse(date),startHour,endHour);
            allFutures.add(scheduleService.getScheduleOneDayAsync(infoModel) );
        }
        CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[allFutures.size() - 1])).join();
        for(CompletableFuture curruntFuture: allFutures){
            CompletableFuture<List<OnedayScheduleView>> future = curruntFuture;
            List<OnedayScheduleView> returnTempList1 = future.get();
            for(OnedayScheduleView curruntRow : returnTempList1){
                returnTempList.add(curruntRow);
            }
        }
        return returnTempList;
    }

    @RequestMapping(value = "/getOneDaySchedulesView/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<OnedayScheduleView> getOneDaySchedulesView(
            @RequestParam("workOrderId") int workOrderId,
            @RequestParam("channelId") int channelId,
            @RequestParam("advertId") int advertId,
            @RequestParam("advertType") String advertType,
            @RequestParam("date") String date,
            @RequestParam("startHour") int startHour,
            @RequestParam("endHour") int endHour
    ) throws ParseException {
        OneDayScheduleInfo infoModel = new OneDayScheduleInfo(workOrderId, channelId, advertId, advertType, new SimpleDateFormat("yyyy/MM/dd").parse(date),startHour,endHour);
        return scheduleService.getOneDaySchedulesView(infoModel);
    }

    @RequestMapping(value = "/generateOneDayScheduleExcel/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean generateOneDayScheduleExcel(
            @RequestParam("workOrderId") int workOrderId,
            @RequestParam("channelId") int channelId,
            @RequestParam("advertId") int advertId,
            @RequestParam("advertType") String advertType,
            @RequestParam("date") String date,
            @RequestParam("startHour") int startHour,
            @RequestParam("endHour") int endHour,
            HttpServletRequest request
    ) throws ParseException {
        OneDayScheduleInfo infoModel = new OneDayScheduleInfo(workOrderId, channelId, advertId, advertType, new SimpleDateFormat("yyyy/MM/dd").parse(date),startHour,endHour);
        return scheduleService.generateOneDayScheduleExcel(infoModel, request);
    }
    
    @RequestMapping(value = "/downloadOneDayScheduelExcell/json", method = RequestMethod.GET)
    public void downloadOneDaySheduleExcel(HttpServletRequest request, HttpServletResponse response) {
        scheduleService.downloadOneDayScheduleExcel(request, response);
    }

    @RequestMapping(value = "/conformSchedule_Client/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean getSelectedScheduleAdvert(@RequestParam("workOrderId") int workOrderId) {
        return scheduleService.setPlayPermissionForWorkOrder(-102, new Date());
    }

    @RequestMapping(value = "/clearTable/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean getSelectedScheduleAdvert() {
        return scheduleService.clearPlaylist();
    }

    @RequestMapping(value = "/playListGenerat/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean playListGenerate() throws ParseException {
        Date date = new SimpleDateFormat().parse(scheduleService.addDay(new Date(), -1));
        return scheduleService.playListGenerateCaller(date);
    }

    @RequestMapping(value = "/test/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean test() {
        return scheduleService.testFunction();
    }

    @RequestMapping(value = "/slotUtilization/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TimeBeltUtilization> ScheduleSlotUtilization(@RequestParam("workoder_id") Integer iWOId) {
        return scheduleService.getScheduleSlotUtilization(iWOId);
    }

    @RequestMapping(value = "/getWorkOrderRemainSecond/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    RemainingSpotView getWorkOrderRemainSecond(@RequestParam("workOrderId") int workOrderId, @RequestParam("channelId") int channelId) throws Exception {
        return scheduleService.getWorkOrderRemainSpotAsSeconds(workOrderId, channelId);
    }

    @RequestMapping(value = "/getAvailbleAdvertCount/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<String> getAvailbleAdvertCount(@RequestParam("workOrderId") int workOrderId, @RequestParam("advertId") int advertId) throws Exception {
        return scheduleService.getAvailableAdvertSpot(workOrderId, advertId);
    }

    @ResponseBody
    @RequestMapping(value = "/generateExcel/json", method = RequestMethod.POST)
    Boolean generateExcel(HttpServletRequest request, @RequestBody MissedSpotFilters data) throws Exception {
        return scheduleService.generateExcwl(request, data.getFromDate(), data.getToDate(), data.getChannelIDs(), data.getAdvertID(), data.getClientID(),data.getHour());
    }

    @RequestMapping(value = "/downloadExcell/json", method = RequestMethod.GET)
    public void downloadPDF(HttpServletRequest request, HttpServletResponse response) {
        scheduleService.downloadMissedSpotExcel(request, response);
    }

    @RequestMapping(value = "/schedulePatternSave/json", method = RequestMethod.POST)
    public @ResponseBody
    WorkOrderScheduleView schedulePatternSave(@RequestParam("scheduleData") String scheduleData) throws Exception {
        LOGGER.debug("Started save schedule using pattern fill");
        WorkOrderScheduleView savePattenResult = scheduleService.saveSchedulePattern(scheduleData);
        //scheduleService.recreateDailyScheduleForPatternFillAndReplaceAdvert(savePattenResult);
        scheduleAsyncService.recreateDailyScheduleForPatternFillAndReplaceAdvert(savePattenResult);
        WorkOrderScheduleView returnData = scheduleService.getByScheduleDataRowForPatternFill(scheduleData);
        returnData.setMessage(savePattenResult.getMessage());
        LOGGER.debug("Completed save schedule using pattern fill");
        return returnData;
    }

    @RequestMapping(value = "/prePatternInsert/json", method = RequestMethod.POST)
    public @ResponseBody
    List<PreScheduleInsertView> prePatternInsert(@RequestParam("scheduleData") String scheduleData) throws Exception {
        return scheduleService.prePatternInsert(scheduleData);
    }

    @RequestMapping(value = "/advertReplace/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    WorkOrderScheduleView advertReplace(@RequestParam("replaceData") String replaceData) throws Exception {
        WorkOrderScheduleView savedScheduleView = scheduleService.advertisementsReplace(replaceData);
        scheduleService.recreateDailyScheduleForPatternFillAndReplaceAdvert(savedScheduleView);
        // For Jira issues NRM-823 and NRM-827
        List<ScheduleUpdateView> lstSchedule = scheduleService.getScheduleListOrderByASC(savedScheduleView.getTimebeltSchedules().get(0).getWorkorderid());
        savedScheduleView.setTimebeltSchedules(lstSchedule);
        // End
        return savedScheduleView;
    }

    @RequestMapping(value = "/preAdvertReplace/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<PreScheduleReplaceView> preAdvertReplace(@RequestParam("replaceData") String replaceData) throws Exception {
        return scheduleService.preAdvertisementsReplace(replaceData);
    }

    @RequestMapping(value = "/copyScheduleRows/json", method = RequestMethod.POST)
    public @ResponseBody
    WorkOrderScheduleView copyScheduleRows(@RequestParam("rowsData") String rowsData) throws Exception {
        WorkOrderScheduleView savedScheduleView = scheduleService.copyScheduleRows(rowsData);
        scheduleAsyncService.recreateDailyScheduleForPatternFillAndReplaceAdvert(savedScheduleView);
        savedScheduleView = scheduleService.getWOScheduleViewData(savedScheduleView.getWorkOrderId(), -99, true);
        return savedScheduleView;
    }

    @RequestMapping(value = "/preCopyScheduleRows/json", method = RequestMethod.POST)
    public @ResponseBody
    List<PreScheduleInsertView> preCopyScheduleRows(@RequestParam("rowsData") String rowsData) throws Exception {
        return scheduleService.preCopyScheduleRows(rowsData);
    }

    @RequestMapping(value = "/getTimeUtilization/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TimeBeltUtilization> getTimeUtilization(@RequestParam("workOrderId") int workOrderId) throws Exception {
        return scheduleService.getScheduleSlotUtilization(workOrderId);
    }
}
