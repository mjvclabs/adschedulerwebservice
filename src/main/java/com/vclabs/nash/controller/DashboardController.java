/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import com.vclabs.dashboard.core.data.dto.User;
import com.vclabs.dashboard.core.data.dto.UserGroup;
import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.dashboard.service.WidgetDataBootstrapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.vclabs.nash.model.view.DashboadChannelView;
import com.vclabs.nash.service.DashboardService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/dashboard")
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WidgetDataBootstrapService widgetDataBootstrapService;

    @RequestMapping(value = "/getManualChannel", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DashboadChannelView> getManualAdvertisement(@RequestParam("selectedDate") String selectedDate) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dashboardService.getManualORAutoChannelList(1, dateFormat.parse(selectedDate));
    }

    @RequestMapping(value = "/getAutoChannel", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DashboadChannelView> getAutoAdvertisement(@RequestParam("selectedDate") String selectedDate) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dashboardService.getManualORAutoChannelList(0, dateFormat.parse(selectedDate));
    }

    @RequestMapping(value = "/getAllDashboardGroups", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<UserGroup> getAllDashboardGroups(){
        return dashboardManager.getUserGroups();
    }

    @ResponseBody
    @RequestMapping(value = {"/feed-widget-data"}, method = RequestMethod.GET)
    public ResponseEntity<?> feedWidgetData () {
        widgetDataBootstrapService.feedDataToTopics();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/getDashboardUser", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    User getDashboardUser(@RequestParam("username") String  username){
        return dashboardManager.getUser(username);
    }
}
