/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vclabs.nash.model.process.MissedSpotItemModel;
import com.vclabs.nash.model.view.reporting.*;
import com.vclabs.nash.service.report.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vclabs.nash.model.view.ReportView;
import com.vclabs.nash.service.ReportingService;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/Reports")
public class ReportController {

    @Autowired
    private ReportingService reportingService;
    @Autowired
    private ClientAndAgencyRevenueService clientAndAgencyRenenueService;
    @Autowired
    private SalesReportService sellersReportService;
    @Autowired
    private PaymentDueService paymentDueService;
    @Autowired
    private MaterialAnalysisService materialAnalysisService;
    @Autowired
    private MediaDeleteService mediaDeleteService;
    @Autowired
    private MissedSpotCountService missedSpotCountService;
    @Autowired
    private MoshUserService moshUserService;

    public ReportController() {
    }

    @RequestMapping(value = "/transemissionReport/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ReportView> transmissionReport(@RequestParam("channelId") String channelID, @RequestParam("workOrderId") String workOrderId, @RequestParam("to") String to, @RequestParam("from") String from) throws ParseException {
        return reportingService.transeMissionServicereport_V2(channelID, workOrderId, from, to);
    }

    //tested
    @RequestMapping(value = "/transemissionReportPrint/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean transmissionReportPrint(HttpServletRequest request, @RequestParam("channelId") String channelID, @RequestParam("workOrderId") String workOrderId, @RequestParam("to") String to, @RequestParam("from") String from) throws Exception {
        return reportingService.transeMissionReportPrintService(request, channelID, workOrderId, from, to);
    }
    
    @RequestMapping(value = "/transemissionReportPrintXMS/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean transmissionReportPrintXMS(HttpServletRequest request, @RequestParam("channelId") String channelID, @RequestParam("workOrderId") String workOrderId, @RequestParam("to") String to, @RequestParam("from") String from) throws Exception {
        return reportingService.transeMissionReportPrintServiceExcel(request, channelID, workOrderId, from, to);
    }

    @RequestMapping(value = "/downloadPDF1/json", method = RequestMethod.GET)
    public void downloadPDF(HttpServletRequest request, HttpServletResponse response) {
        reportingService.downloadPDF(request, response);
    }
    
    @RequestMapping(value = "/downloadExcel/json", method = RequestMethod.GET)
    public void downloadExcel(HttpServletRequest request, HttpServletResponse response) {
        reportingService.downloadExcel(request, response);
    }

    /*--------------------------------DummyCut Report-----------------------------------------*/
    @RequestMapping(value = "/getDummyCutReport/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DummyCutView> getDummyCutReport() {
        return reportingService.getDummyCutReport();
    }

    @RequestMapping(value = "/writeDummyCutReport/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean writeDummyCutReport(HttpServletRequest request) throws Exception {
        return reportingService.generateDummyCutExcel(request);
    }

    @RequestMapping(value = "/downloadDummyCutReport/json", method = RequestMethod.GET)
    public void downloadDummyCutReport(HttpServletRequest request, HttpServletResponse response) {
        reportingService.downloadDummyCutExcell(response, request);
    }

    /*--------------------------------ComAvailabilityHourly Report-----------------------------------------*/
    @RequestMapping(value = "/getComAvailabilityHourly/json", method = RequestMethod.GET)
    public Map<Integer, ComAvailabilityChannel> getComAvailabilityHourly(@RequestParam("date") String date) {
        return reportingService.getComAvailabilityHourly(date, date);
    }

    @RequestMapping(value = "/writeComAvailabilityHourlyReport/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean writeComAvailabilityHourlyReport(HttpServletRequest request, String date, int value) throws Exception {
        return reportingService.generateComAvailabilityHourlyExcel(request, date, value);
    }

    @RequestMapping(value = "/downloadComAvailabilityHourlyReport/json", method = RequestMethod.GET)
    public void downloadComAvailabilityHourlyReport(HttpServletRequest request, HttpServletResponse response) {
        reportingService.downloadComAvailabilityHourlyExcell(response, request);
    }

    /*--------------------------------ComAvailability-Channel wise Report-----------------------------------------*/
    @RequestMapping(value = "/getComAvailabilityChannelWise/json", method = RequestMethod.GET)
    public Map<String, ComAvailabilityChannel> getComAvailabilityChannelWise(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, @RequestParam("channelID") int channelId) {
        return reportingService.getComAvailabilityChannelWise(startDate, endDate, channelId);
    }

    @RequestMapping(value = "/writeComAvailabilityChannelWiseReport/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean writeComAvailabilityChannelWiseReport(HttpServletRequest request, String startDate, String endDate, int channelId, int value) throws Exception {
        return reportingService.generateComAvailabilityChannelWiseExcel(request, startDate, endDate, value, channelId);
    }

    @RequestMapping(value = "/downloadComAvailabilityChannelWiseReport/json", method = RequestMethod.GET)
    public void downloadComAvailabilityChannelWiseReport(HttpServletRequest request, HttpServletResponse response) {
        reportingService.downloadComAvailabilityChannelWiseExcell(response, request);
    }

    /*--------------------------------Schedule print Report-----------------------------------------*/
    @RequestMapping(value = "/writeSchedulePrintReport/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean writeSchedulePrintReport(HttpServletRequest request, int workOrderId) throws Exception {
        return reportingService.generateScheduleExcel(request, workOrderId);
    }

    @RequestMapping(value = "/downloadSchedulePrintReport/json", method = RequestMethod.GET)
    public void downloadSchedulePrintReport(HttpServletRequest request, HttpServletResponse response) {
        reportingService.downloadSchedulePrintExcell(response, request);
    }

    /*--------------------------------Schedule Analysis Report-----------------------------------------*/
    @RequestMapping(value = "/getScheduleAnalysis/json", method = RequestMethod.POST)
    public Map<String, Map<String, ComAvailabilityChannel>> getScheduleAnalysis(@RequestParam("workOrderID") int workOrderID, @RequestParam("workOrderIDS") String[] workOrderIDs, @RequestParam("agencyIDS") String[] agencyIDS, @RequestParam("clientId") int clientId) {
        List<Integer> woIDS = new ArrayList<>();
        List<Integer> agnyIDS = new ArrayList<>();
        if (workOrderIDs.length != 0) {
            for (String id : workOrderIDs) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    woIDS.add(Integer.parseInt(id));
                }
            }
        }
        if (agencyIDS.length != 0) {
            for (String id : agencyIDS) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    agnyIDS.add(Integer.parseInt(id));
                }
            }
        }
        if (agnyIDS.isEmpty() && woIDS.isEmpty() && clientId == -111) {
            return new TreeMap<>();
        }
        return reportingService.getScheduleAnalysis(workOrderID, woIDS, agnyIDS, clientId);
    }

    @RequestMapping(value = "/writeScheduleAnalysisReport/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Boolean writeScheduleAnalysisReport(HttpServletRequest request, @RequestParam("workOrderID") int workOrderID, @RequestParam("workOrderIDS") String[] workOrderIDs, @RequestParam("agencyIDS") String[] agencyIDS, @RequestParam("clientId") int clientId, @RequestParam("value") int value) throws Exception {
        List<Integer> woIDS = new ArrayList<>();
        List<Integer> agnyIDS = new ArrayList<>();
        if (workOrderIDs.length != 0) {
            for (String id : workOrderIDs) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    woIDS.add(Integer.parseInt(id));
                }
            }
        }
        if (agencyIDS.length != 0) {
            for (String id : agencyIDS) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    agnyIDS.add(Integer.parseInt(id));
                }
            }
        }
        if (agnyIDS.isEmpty() && woIDS.isEmpty() && clientId == -111) {
            return false;
        }

        return reportingService.generateScheduleAnalusisExcel(request, workOrderID, woIDS, agnyIDS, clientId, value);
    }

    @RequestMapping(value = "/downloadScheduleAnalysisReport/json", method = RequestMethod.GET)
    public void downloadScheduleAnalysisReport(HttpServletRequest request, HttpServletResponse response) {
        reportingService.downloadScheduleAnalusisExcel(response, request);
    }

    /*--------------------------------Client Revenue Report-----------------------------------------*/
    @RequestMapping(value = "/getClientRevenue/json", method = RequestMethod.GET)
    public Map<Integer, ClientOrAgencyList> getClientRevenue(@RequestParam("clientIDS") String[] clientIDS, @RequestParam("year") int year, @RequestParam("month") String month) {
        List<Integer> clientIdList = new ArrayList<>();
        if (clientIDS.length != 0) {
            for (String id : clientIDS) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    clientIdList.add(Integer.parseInt(id));
                }
            }
        }
//        if (clientIdList.isEmpty()) {
//            return new TreeMap<Integer, ClientOrAgencyList>();
//        }
        return clientAndAgencyRenenueService.getClientOrAgencyRevenue(clientIdList, year, month, true);
    }

    @RequestMapping(value = "/getYearsValue/json", method = RequestMethod.GET)
    public Map<Integer, Integer> getYearsValue() {
        return clientAndAgencyRenenueService.getYears();
    }

    @RequestMapping(value = "/writeClientRevenueReport/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Boolean writeClientRevenueReport(HttpServletRequest request, @RequestParam("clientIDS") String[] clientIDS, @RequestParam("year") int year, @RequestParam("month") String month) throws Exception {

        List<Integer> clientIdList = new ArrayList<>();
        if (clientIDS.length != 0) {
            for (String id : clientIDS) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    clientIdList.add(Integer.parseInt(id));
                }
            }
        }

        return clientAndAgencyRenenueService.generateClientRevenueExcel(request, clientIdList, year, month);
    }

    @RequestMapping(value = "/downloadClientRevenueReport/json", method = RequestMethod.GET)
    public void downloadClientRevenueReport(HttpServletRequest request, HttpServletResponse response) {
        clientAndAgencyRenenueService.downloadClientRevenueExcell(response, request);
    }

    /*--------------------------------Agency Revenue Report-----------------------------------------*/
    @RequestMapping(value = "/getAgencyRevenue/json", method = RequestMethod.GET)
    public Map<Integer, ClientOrAgencyList> getAgencyRevenue(@RequestParam("clientIDS") String[] clientIDS, @RequestParam("year") int year, @RequestParam("month") String month) {
        List<Integer> clientIdList = new ArrayList<>();
        if (clientIDS.length != 0) {
            for (String id : clientIDS) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    clientIdList.add(Integer.parseInt(id));
                }
            }
        }
//        if (clientIdList.isEmpty()) {
//            return new TreeMap<Integer, ClientOrAgencyList>();
//        }
        return clientAndAgencyRenenueService.getClientOrAgencyRevenue(clientIdList, year, month, false);
    }

    @RequestMapping(value = "/writeAgencyRevenueReport/json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Boolean writeAgencyRevenueReport(HttpServletRequest request, @RequestParam("clientIDS") String[] clientIDS, @RequestParam("year") int year, @RequestParam("month") String month) throws Exception {

        List<Integer> clientIdList = new ArrayList<>();
        if (clientIDS.length != 0) {
            for (String id : clientIDS) {
                id = id.replace("[", "");
                id = id.replace("]", "");
                if (!id.equals("") && !id.equals("-111")) {
                    clientIdList.add(Integer.parseInt(id));
                }
            }
        }

        return clientAndAgencyRenenueService.generateAgencyRevenueExcel(request, clientIdList, year, month);
    }

    @RequestMapping(value = "/downloadAgencyRevenueReport/json", method = RequestMethod.GET)
    public void downloadAgencyRevenueReport(HttpServletRequest request, HttpServletResponse response) {
        clientAndAgencyRenenueService.downloadAgencyRevenueExcell(response, request);
    }

    /*--------------------------------Seller Report-----------------------------------------*/
    @RequestMapping(value = "/getSellerReport/json", method = RequestMethod.GET)
    public List<SalesView> getSellerReport(@RequestParam("woType") String woType, @RequestParam("meList") String meList, @RequestParam("monthList") String monthList, @RequestParam("yearList") String yearList, @RequestParam("clientList") String clientList, @RequestParam("agencyList") String agencyList) {

        String[] woTypeArray = woType.split(",");
        List<String> woTypeStringList = new ArrayList<>();
        if (woTypeArray.length != 0) {
            for (String id : woTypeArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    woTypeStringList.add(id);
                }
            }
        }

        String[] meArray = meList.split(",");
        List<String> meStringList = new ArrayList<>();
        if (meArray.length != 0) {
            for (String id : meArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    meStringList.add(id);
                }
            }
        }

        String[] monthArray = monthList.split(",");
        List<String> monthStringList = new ArrayList<>();
        if (monthArray.length != 0) {
            for (String id : monthArray) {
                if (!id.equals("") && !id.equals("All")) {
                    monthStringList.add(id);
                }
            }
        }

        String[] yearArray = yearList.split(",");
        List<Integer> yearStringList = new ArrayList<>();
        if (yearArray.length != 0) {
            for (String id : yearArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    yearStringList.add(Integer.parseInt(id));
                }
            }
        }

        String[] clinetArray = clientList.split(",");
        List<Integer> clientIntList = new ArrayList<>();
        if (clinetArray.length != 0) {
            for (String id : clinetArray) {
                if (!id.equals("") && !id.equals("All")) {
                    clientIntList.add(Integer.parseInt(id));
                }
            }
        }

        String[] agencyArray = agencyList.split(",");
        List<Integer> agencyIntList = new ArrayList<>();
        if (agencyArray.length != 0) {
            for (String id : agencyArray) {
                if (!id.equals("") && !id.equals("All")) {
                    agencyIntList.add(Integer.parseInt(id));
                }
            }
        }

        return sellersReportService.getSellerReport(woTypeStringList, meStringList, monthStringList, yearStringList, clientIntList, agencyIntList);
        //return null;
    }

    @RequestMapping(value = "/writeSellerReport/json", method = RequestMethod.POST)
    public Boolean writeSellerReport(HttpServletRequest request, @RequestParam("woType") String woType, @RequestParam("meList") String meList, @RequestParam("monthList") String monthList, @RequestParam("yearList") String yearList, @RequestParam("clientList") String clientList, @RequestParam("agencyList") String agencyList) throws Exception {

        String[] woTypeArray = woType.split(",");
        List<String> woTypeStringList = new ArrayList<>();
        if (woTypeArray.length != 0) {
            for (String id : woTypeArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    woTypeStringList.add(id);
                }
            }
        }

        String[] meArray = meList.split(",");
        List<String> meStringList = new ArrayList<>();
        if (meArray.length != 0) {
            for (String id : meArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    meStringList.add(id);
                }
            }
        }

        String[] monthArray = monthList.split(",");
        List<String> monthStringList = new ArrayList<>();
        if (monthArray.length != 0) {
            for (String id : monthArray) {
                if (!id.equals("") && !id.equals("All")) {
                    monthStringList.add(id);
                }
            }
        }

        String[] yearArray = yearList.split(",");
        List<Integer> yearStringList = new ArrayList<>();
        if (yearArray.length != 0) {
            for (String id : yearArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    yearStringList.add(Integer.parseInt(id));
                }
            }
        }

        String[] clinetArray = clientList.split(",");
        List<Integer> clientIntList = new ArrayList<>();
        if (clinetArray.length != 0) {
            for (String id : clinetArray) {
                if (!id.equals("") && !id.equals("All")) {
                    clientIntList.add(Integer.parseInt(id));
                }
            }
        }

        String[] agencyArray = agencyList.split(",");
        List<Integer> agencyIntList = new ArrayList<>();
        if (agencyArray.length != 0) {
            for (String id : agencyArray) {
                if (!id.equals("") && !id.equals("All")) {
                    agencyIntList.add(Integer.parseInt(id));
                }
            }
        }

        return sellersReportService.generateSellerReportExcel(request, woTypeStringList, meStringList, monthStringList, yearStringList, clientIntList, agencyIntList);
    }

    @RequestMapping(value = "/downloadSellerReport/json", method = RequestMethod.GET)
    public void downloadSellerReport(HttpServletRequest request, HttpServletResponse response) {
        sellersReportService.downloadSellerReportExcel(response, request);
    }

    @RequestMapping(value = "/test/json", method = RequestMethod.GET)
    public List<SalesView> test() {
        //sellersReportService.getPrimeTimeNonPrimeTimeSpotCount(368);//clientAndAgencyRenenueService.getClientRevenue();0771570703 chanux bro
        return sellersReportService.getSellerReport(new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
    }

    /*--------------------------------Payment Due Report-----------------------------------------*/
    @RequestMapping(value = "/getPaymentDueReport/json", method = RequestMethod.GET)
    public List<PaymentDue> getPaymentDueReport(@RequestParam("monthList") String monthList, @RequestParam("yearList") String yearList, @RequestParam("clientList") String clientList, @RequestParam("agencyList") String agencyList) {

        String[] monthArray = monthList.split(",");
        List<String> monthStringList = new ArrayList<>();
        if (monthArray.length != 0) {
            for (String id : monthArray) {
                if (!id.equals("") && !id.equals("All")) {
                    monthStringList.add(id);
                }
            }
        }

        String[] yearArray = yearList.split(",");
        List<Integer> yearStringList = new ArrayList<>();
        if (yearArray.length != 0) {
            for (String id : yearArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    yearStringList.add(Integer.parseInt(id));
                }
            }
        }

        String[] clinetArray = clientList.split(",");
        List<Integer> clientIntList = new ArrayList<>();
        if (clinetArray.length != 0) {
            for (String id : clinetArray) {
                if (!id.equals("") && !id.equals("All")) {
                    clientIntList.add(Integer.parseInt(id));
                }
            }
        }

        String[] agencyArray = agencyList.split(",");
        List<Integer> agencyIntList = new ArrayList<>();
        if (agencyArray.length != 0) {
            for (String id : agencyArray) {
                if (!id.equals("") && !id.equals("All")) {
                    agencyIntList.add(Integer.parseInt(id));
                }
            }
        }

        return paymentDueService.getPaymentDueReport(clientIntList, agencyIntList, yearStringList, monthStringList);
    }

    @RequestMapping(value = "/writePaymentDueReport/json", method = RequestMethod.POST)
    public Boolean writePaymentDueReport(HttpServletRequest request, @RequestParam("monthList") String monthList, @RequestParam("yearList") String yearList, @RequestParam("clientList") String clientList, @RequestParam("agencyList") String agencyList) throws Exception {

        String[] monthArray = monthList.split(",");
        List<String> monthStringList = new ArrayList<>();
        if (monthArray.length != 0) {
            for (String id : monthArray) {
                if (!id.equals("") && !id.equals("All")) {
                    monthStringList.add(id);
                }
            }
        }

        String[] yearArray = yearList.split(",");
        List<Integer> yearStringList = new ArrayList<>();
        if (yearArray.length != 0) {
            for (String id : yearArray) {
                if (!id.equals("") && !id.equals("-111")) {
                    yearStringList.add(Integer.parseInt(id));
                }
            }
        }

        String[] clinetArray = clientList.split(",");
        List<Integer> clientIntList = new ArrayList<>();
        if (clinetArray.length != 0) {
            for (String id : clinetArray) {
                if (!id.equals("") && !id.equals("All")) {
                    clientIntList.add(Integer.parseInt(id));
                }
            }
        }

        String[] agencyArray = agencyList.split(",");
        List<Integer> agencyIntList = new ArrayList<>();
        if (agencyArray.length != 0) {
            for (String id : agencyArray) {
                if (!id.equals("") && !id.equals("All")) {
                    agencyIntList.add(Integer.parseInt(id));
                }
            }
        }

        return paymentDueService.generatePaymentDueReportExcel(request, clientIntList, agencyIntList, yearStringList, monthStringList);
    }

    @RequestMapping(value = "/downloadPaymentDueReport/json", method = RequestMethod.GET)
    public void downloadPaymentDueReport(HttpServletRequest request, HttpServletResponse response) {
        paymentDueService.downloadPaymentDueReportExcel(response, request);
    }

    /*--------------------------------Material analysis Report-----------------------------------------*/
    @RequestMapping(value = "/getMaterialAnalysisReport/json", method = RequestMethod.GET)
    public List<MaterialAnalysis> getMaterialAnalysisReport(@RequestParam("clientList") String clientList, @RequestParam("cutIds") String cutIdsList) {

        String[] clinetArray = clientList.split(",");
        List<Integer> clientIntList = new ArrayList<>();
        if (clinetArray.length != 0) {
            for (String id : clinetArray) {
                if (!id.equals("") && !id.equals("All")) {
                    clientIntList.add(Integer.parseInt(id));
                }
            }
        }

        String[] cutIdsArray = cutIdsList.split(",");
        List<Integer> cutIdsIntList = new ArrayList<>();
        if (cutIdsArray.length != 0) {
            for (String id : cutIdsArray) {
                if (!id.equals("") && !id.equals("All")) {
                    cutIdsIntList.add(Integer.parseInt(id));
                }
            }
        }

        return materialAnalysisService.getMaterialAnalysisReport(cutIdsIntList, clientIntList);
    }

    @RequestMapping(value = "/getAdvertIDs/json", method = RequestMethod.GET)
    public List<Integer> getAdvertIDs() {
        return materialAnalysisService.getAdvertList();
    }

    @RequestMapping(value = "/writeMaterialAnalysisReport/json", method = RequestMethod.POST)
    public Boolean writeMaterialAnalysisReport(HttpServletRequest request, @RequestParam("clientList") String clientList, @RequestParam("cutIds") String cutIdsList) throws Exception {

        String[] clinetArray = clientList.split(",");
        List<Integer> clientIntList = new ArrayList<>();
        if (clinetArray.length != 0) {
            for (String id : clinetArray) {
                if (!id.equals("") && !id.equals("All")) {
                    clientIntList.add(Integer.parseInt(id));
                }
            }
        }

        String[] cutIdsArray = cutIdsList.split(",");
        List<Integer> cutIdsIntList = new ArrayList<>();
        if (cutIdsArray.length != 0) {
            for (String id : cutIdsArray) {
                if (!id.equals("") && !id.equals("All")) {
                    cutIdsIntList.add(Integer.parseInt(id));
                }
            }
        }

        return materialAnalysisService.generateMaterialAnalysisReportExcel(request, cutIdsIntList, clientIntList);
    }

    @RequestMapping(value = "/downloadMaterialAnalysisReport/json", method = RequestMethod.GET)
    public void downloadMaterialAnalysisReport(HttpServletRequest request, HttpServletResponse response) {
        materialAnalysisService.downloadMaterialAnalysisReportExcel(response, request);
    }

    /*--------------------------------Media Delete Report-----------------------------------------*/
    @RequestMapping(value = "/getMediaDeleteHistory/json", method = RequestMethod.GET)
    public List<MediaDeletionView> getMediaDeleteHistory() {
        return mediaDeleteService.getDeleteMediaFile();
    }

    @RequestMapping(value = "/writeMediaDeleteHistoryReport/json", method = RequestMethod.POST)
    public Boolean writeMediaDeleteHistoryReport(HttpServletRequest request) throws Exception {
        return mediaDeleteService.generateMediaDeletionHistoryReportExcel(request);
    }

    @RequestMapping(value = "/downloadMediaDeletionHistoryReport/json", method = RequestMethod.GET)
    public void downloadMediaDeletionHistoryReport(HttpServletRequest request, HttpServletResponse response) {
        mediaDeleteService.downloadMediaDeletionHistoryExcel(response, request);
    }

    /*--------------------------------Missed spot  Report-----------------------------------------*/
    @RequestMapping(value = "/missed", method = RequestMethod.GET)
    public List<MissedSpotItemModel> test_missed(@RequestParam("filterData") String filterData) throws IOException {
        return missedSpotCountService.getMissedSpotCount(filterData);
    }

    @RequestMapping(value = "/getMissedSpotData", method = RequestMethod.GET)
    public List<MissedSpotItemModel> getMissedSpotData(@RequestParam("filterData") String filterData) throws IOException {
        return missedSpotCountService.getMissedSpotCount(filterData);
    }


    @RequestMapping(value = "/writeMissedSpotCountReport/json", method = RequestMethod.POST)
    public Boolean writeMissedSpotCountReport(HttpServletRequest request,@RequestParam("filterData") String filterData) throws Exception {
        return missedSpotCountService.generateMissedSpotCountReportExcel(request,filterData);
    }

    @RequestMapping(value = "/downloadMissedSpotCountReport/json", method = RequestMethod.GET)
    public void downloadMissedSpotCountReport(HttpServletRequest request, HttpServletResponse response) {
        missedSpotCountService.downloadMissedSpotCountExcel(response, request);
    }

    @RequestMapping(value = "/getMoshUserReport", method = RequestMethod.GET)
    public List<UserWiseView> getMoshUserReport(@RequestParam("filterData") String filterData) throws IOException {
        return moshUserService.getUserLogdata(filterData);
    }

    @RequestMapping(value = "/writeMoshUserReport/json", method = RequestMethod.POST)
    public Boolean writeMoshUserReport(HttpServletRequest request,@RequestParam("filterData") String filterData) {
        return moshUserService.generateUsrWiseReportExcel(request,filterData);
    }

    @RequestMapping(value = "/downloadMoshUserReport/json", method = RequestMethod.GET)
    public void downloadMoshUserReport(HttpServletRequest request, HttpServletResponse response) {
        moshUserService.downloadUserWiseExcel(response, request);
    }
}
