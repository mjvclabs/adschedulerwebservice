package com.vclabs.nash.controller.dto;

import java.util.List;

public class WorkOrderBillRequest {

    private List<Integer> woIds;

    private boolean fullBill; //to keep the attribute whether billing is doing for full schedule or aired spots

    public List<Integer> getWoIds() {
        return woIds;
    }

    public void setWoIds(List<Integer> woIds) {
        this.woIds = woIds;
    }

    public boolean isFullBill() {
        return fullBill;
    }

    public void setFullBill(boolean fullBill) {
        this.fullBill = fullBill;
    }
}
