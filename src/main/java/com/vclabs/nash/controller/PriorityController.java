/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs
 * (pvt)) Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */

package com.vclabs.nash.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;

import com.vclabs.nash.model.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vclabs.nash.service.PriorityService;

/**
 *
 * @author Sanira Nanayakkara
 */
@RestController
@RequestMapping("/priority")
public class PriorityController {

    @Autowired
    private PriorityService priorityService;
    
    @RequestMapping(value = "/getListbyChannel/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<PriorityUpdateView> priorityListbyChannel(@RequestParam("channelId") int channelID) {
        return priorityService.getPriorityListbyChannel(channelID);
    }
    
    @RequestMapping(value = "/updatePriorityCountList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean updateClusterCount(@RequestParam("priorityList")String jsonInput) throws Exception{        
        ObjectMapper mapper = new ObjectMapper();
        List<Pair<Long,Integer>> dataList = mapper.readValue(jsonInput, new TypeReference<List<Pair>>(){});

        return priorityService.updatePriorityCount(dataList);
    }
    
    @RequestMapping(value = "/insertPriority/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Message InsertPriority(PriorityInsertView pPriorityInsert) throws Exception{
        return priorityService.InsertPriority(pPriorityInsert);
    }
    
    @RequestMapping(value = "/viewPriorityList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<PriorityPreviewView> getPriorityList(PriorityInsertView pPriorityInsert) throws Exception{        
        return priorityService.getPriorityList(pPriorityInsert);
    }
    
    @RequestMapping(value = "/removePriorities/json", method = RequestMethod.GET, produces = "application/json")
    public boolean removePriorities(PriorityInsertView pPriorityInsert){        
        return priorityService.removePriorities(pPriorityInsert);
    }
}
