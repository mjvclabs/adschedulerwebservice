package com.vclabs.nash.controller;

import com.vclabs.nash.model.entity.Advertisement;
import com.vclabs.nash.service.FillerTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.vclabs.nash.model.entity.FillerTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by dperera on 30/01/2020.
 */
@Controller
@RequestMapping("/fillers")
public class FillerTagController {

    @Autowired
    private FillerTagService fillerTagService;

    @RequestMapping(value = {"/tagging"}, method = RequestMethod.GET)
    public String tagFillersPage(@RequestParam("advertID") int advertID){
        return "filler-tagging";
    }

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public String allFillersPage(){
        return "all-fillers";
    }

    @RequestMapping(value = {"/save"}, method = RequestMethod.POST)
    public ResponseEntity<?> saveOrUpdate(@RequestParam("advertId") int advertId, @RequestBody List<FillerTag> tags){
        fillerTagService.saveFillerTag(advertId, tags);
        return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = {"/channels"}, method = RequestMethod.GET)
    public List<Integer> getChannelsByAdvert(@RequestParam("advertId") int advertId) {
        List<FillerTag> result = fillerTagService.getByAdvertId(advertId);
        if(!result.isEmpty()){
            return result.stream().map(t -> t.getChannelDetails().getChannelid()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @ResponseBody
    @RequestMapping(value = "/search")
    public Page<Advertisement> getAllAdvertList(@RequestParam Map<String, String> filterRequest) {
        return fillerTagService.getFillers(filterRequest);
    }
}
