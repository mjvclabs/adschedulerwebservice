package com.vclabs.nash.controller.inventory.dto;

import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.service.inventory.vo.InventoryConflict;
import com.vclabs.nash.service.inventory.vo.InventoryConflictRow;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-26.
 */
public class InventoryContainer {

    private Inventory inventory;

    private List<InventoryConflict> conflicts = new ArrayList<>();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fromDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date toDate;

    private boolean dateBased;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    private boolean overrideOperation;

    private InventoryWeek inventoryWeek = new InventoryWeek();

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public boolean isDateBased() {
        return dateBased;
    }

    public void setDateBased(boolean dateBased) {
        this.dateBased = dateBased;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isOverrideOperation() {
        return overrideOperation;
    }

    public void setOverrideOperation(boolean overrideOperation) {
        this.overrideOperation = overrideOperation;
    }

    public List<InventoryConflict> getConflicts() {
        return conflicts;
    }

    public List<InventoryConflictRow> getConflictRows(){
        List<InventoryConflictRow> rows = new ArrayList<>();
        for(InventoryConflict conflict : conflicts){
            for(InventoryConflictRow row: conflict.getRows()){
                rows.add(row);
            }
        }
        return rows;
    }

    public void setConflicts(List<InventoryConflict> conflicts) {
        this.conflicts = conflicts;
    }

    public void addConflict(InventoryConflict conflict){
        if(conflict.getRows().size() > 0){
            conflicts.add(conflict);
        }
    }

    public InventoryWeek getInventoryWeek() {
        return inventoryWeek;
    }

    public void setInventoryWeek(InventoryWeek inventoryWeek) {
        this.inventoryWeek = inventoryWeek;
    }
}
