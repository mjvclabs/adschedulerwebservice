package com.vclabs.nash.controller.inventory;

import com.vclabs.nash.controller.inventory.dto.InventoryContainer;
import com.vclabs.nash.controller.utill.Alert;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventoryprediction.CentralizedInventoryPrediction;
import com.vclabs.nash.model.view.Message;
import com.vclabs.nash.service.ChannelDetailService;
import com.vclabs.nash.service.inventory.InventoryService;
import com.vclabs.nash.service.inventoryprediction.CentralizedInventoryPredictionService;
import com.vclabs.nash.service.inventoryprediction.PredicationConfigurationFileHandler;
import com.vclabs.nash.service.utill.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 21/09/2018
 */

@Controller
@RequestMapping("/inventory")
public class InventoryController {

    @Autowired
    private ChannelDetailService channelDetailService;

    @Autowired
    private InventoryWebService inventoryWebService;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private CentralizedInventoryPredictionService centralizedInventoryPredictionService;

    @Autowired
    private PredicationConfigurationFileHandler predicationConfigurationFileHandler;

    @Value("${inventory.save.success}")
    private String MSG_INVENTORY_SAVE_SUCCESS;

    @Value("${inventory.update.success}")
    private String MSG_INVENTORY_UPDATE_SUCCESS;

    public InventoryController() {
    }

    @RequestMapping(value = "/assign")
    public String viewInventoryByChannel( ModelMap model, @RequestParam(name="channelId", required = false) Integer channelId) {
        if(channelId == null){
            channelId = channelDetailService.AllChannelList().get(0).getChannelid();
        }
        InventoryContainer inventoryContainer = inventoryWebService.loadByChannel(channelId);
        inventoryContainer.setDateBased(false);
        inventoryContainer.setFromDate(new Date());
        inventoryContainer.setToDate(DateUtil.findDateAfter(28));
        model.addAttribute("iContainer", inventoryContainer);
        return "inventory/assign-inventory";
    }

    @RequestMapping(value = "/assign-inventory", method = RequestMethod.GET)
    public String assignInventory( ModelMap model, @RequestParam(name="channelId", required = false) Integer channelId) {

        if(channelId == null) {
            channelId = channelDetailService.ChannelListOrderByName().get(0).getChannelid();
        }

        InventoryContainer inventoryContainer = inventoryWebService.loadByChannel(channelId);
        inventoryContainer.setDateBased(false);
        inventoryContainer.setFromDate(new Date());
        inventoryContainer.setToDate(DateUtil.findDateAfter(28));
        model.addAttribute("iContainer", inventoryContainer);
        return "inventory/assign-inventory";
    }

    @RequestMapping(value = "/save-inventory", method = RequestMethod.POST)
    public String saveInventory(@ModelAttribute("iContainer") InventoryContainer ic,  final RedirectAttributes redirectAttributes) {
        Inventory inventory = ic.getInventory();
        if(inventory.getId() == null){
            redirectAttributes.addFlashAttribute("alert", new Alert(Alert.Type.SUCCESS, String.format(MSG_INVENTORY_SAVE_SUCCESS)));
        }else {
            redirectAttributes.addFlashAttribute("alert", new Alert(Alert.Type.SUCCESS, String.format(MSG_INVENTORY_UPDATE_SUCCESS)));
        }
        inventoryWebService.save(ic);
        if(!ic.getConflictRows().isEmpty()){
            redirectAttributes.addFlashAttribute("hasConflictSummary", true);
            redirectAttributes.addFlashAttribute("conflicts", ic.getConflictRows());
        }
        return "redirect:/inventory/assign?channelId="+inventory.getChannel().getChannelid();
    }

    @RequestMapping(value = "/edit-inventory")
    public String viewEditInventoryByDate(ModelMap model, @RequestParam(name="channelId") Integer channelId, @RequestParam(name="date") @DateTimeFormat(pattern="yyyy-MM-dd") Date date){
        InventoryContainer inventoryContainer = inventoryWebService.loadByChannelAndDate(channelId, date);
        inventoryContainer.setOverrideOperation(true);
        model.addAttribute("iContainer", inventoryContainer);
        return "inventory/assign-inventory";
    }

    @RequestMapping(value = "/view-inventory", method = RequestMethod.GET)
    public String viewInventory( ModelMap model, @RequestParam(name="channelId", required = false) Integer channelId
            , @RequestParam (name="date", required = false)@DateTimeFormat(pattern = "yyyy-MM-dd") Date date){

        if(channelId == null){
            channelId = channelDetailService.ChannelListOrderByName().get(0).getChannelid();
        }
        if(date == null){
            date = new Date();
        }

        model.addAttribute("date", date);

        Inventory inventory = inventoryService.findByChannelAndDate(channelId, date);

        if(inventory == null){
            Inventory emptyInventory = new Inventory();
            emptyInventory.setChannel(channelDetailService.getChannel(channelId));
            emptyInventory.setDate(date);
            model.addAttribute("inventory", emptyInventory);
        }
        else{
            model.addAttribute("inventory", inventory);
        }
        return "inventory/view-inventory";
    }

    @RequestMapping(value = "/view-inventory-prediction", method = RequestMethod.GET)
    public String viewInventoryPrediction( ModelMap model, @RequestParam(name="channelId", required = false) Integer channelId
            , @RequestParam (name="date", required = false)@DateTimeFormat(pattern = "yyyy-MM-dd") Date date){
        ChannelDetails channel = new ChannelDetails();
        if(channelId == null){
            channel = channelDetailService.ChannelListOrderByName().get(0);
        }else{
            channel = channelDetailService.getChannel(channelId);
        }
        if(date == null){
            date = new Date();
        }
        model.addAttribute("channel", channel);
        model.addAttribute("date", date);

        List<CentralizedInventoryPrediction> inventoryPredictions = centralizedInventoryPredictionService.findByDateAndChannel(date, channel.getChannelid());
        if(inventoryPredictions.size() == 0){
            boolean isEmpty = true;
            model.addAttribute("isEmpty", isEmpty);
        }
        model.addAttribute("inventoryPredictions", inventoryPredictions);
        return "inventory/view-inventory-prediction";
    }

    @ResponseBody
    @RequestMapping(value = "/exist-dates")
    public List<Date> findInventoryExistDates(@RequestParam(name="channelId") Integer channelId,
                                              @RequestParam(name="fromDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,
                                              @RequestParam(name="toDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate){

        return inventoryService.findInventoryExistDates(channelId, fromDate, toDate);
    }

    @RequestMapping(value = "/file-uploader", method = RequestMethod.GET)
    public String csvFileUploader( ModelMap model){
        return "inventory/csv-file-uploader";
    }

    @ResponseBody
    @RequestMapping(value = "/file-validation", method = RequestMethod.GET)
    public Message csvFileValidation() {
        Message msg = new Message();
        try {
            predicationConfigurationFileHandler.copyConfigurationFile();
            msg.setMessage("File successfully validated");
        } catch (Exception e) {
            msg.setMessage(e.getMessage());
        }
        return msg;
    }

    @ModelAttribute("allChannels")
    public List<ChannelDetails> populateAllChannels() {
        return channelDetailService.ChannelListOrderByName();
    }

}
