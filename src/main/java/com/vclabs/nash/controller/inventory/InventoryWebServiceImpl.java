package com.vclabs.nash.controller.inventory;

import com.vclabs.nash.controller.inventory.dto.InventoryContainer;
import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryPriceRange;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.service.ChannelDetailService;
import com.vclabs.nash.service.inventory.InventoryService;
import com.vclabs.nash.service.inventory.vo.InventoryConflict;
import com.vclabs.nash.service.utill.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2018-09-26.
 */
@Service
public class InventoryWebServiceImpl implements InventoryWebService {

    @Autowired
    private ChannelDetailService channelDetailService;

    @Autowired
    private InventoryService inventoryService;

    @Override
    @Transactional
    public InventoryContainer loadByChannelAndDate(int channelId, Date date) {
        InventoryContainer inventoryContainer;
        Inventory inventory = inventoryService.findByChannelAndDate(channelId, date);
        if(inventory == null){
            inventoryContainer = loadByChannel(channelId);
        }else {
            inventoryContainer = new InventoryContainer();
            inventory.setChannel(channelDetailService.getChannel(channelId));
            inventoryContainer.setInventory(inventory);
        }
        inventoryContainer.setDateBased(true);
        inventoryContainer.setDate(date);
        return inventoryContainer;
    }

    @Override
    public InventoryContainer loadByChannel(int channelId) {
        InventoryContainer inventoryContainer = new InventoryContainer();
        Inventory inventory = inventoryService.findByChannel(channelId);
        inventory.setChannel(channelDetailService.getChannel(channelId));
        inventoryContainer.setInventory(inventory);
        return inventoryContainer;
    }

    @Override
    public void save(InventoryContainer inventoryContainer) {
        if(inventoryContainer.isDateBased()){
            Inventory inventory = inventoryContainer.getInventory();
            ChannelDetails channel = inventory.getChannel();
            Date date = inventoryContainer.getDate();
            if(!inventoryContainer.isOverrideOperation() && inventoryService.isExistInventory(channel.getChannelid().longValue(), date)){
                return;
            }
            inventory.setDate(date);
            InventoryConflict iConflict = new InventoryConflict(inventory.getDate());
            inventoryContainer.setInventory(inventoryService.saveOrUpdate(inventory, iConflict));
            inventoryContainer.addConflict(iConflict);
        }else {
            LocalDate startDate = DateUtil.toLocalDate(inventoryContainer.getFromDate());
            LocalDate endDate = DateUtil.toLocalDate(inventoryContainer.getToDate());
            Inventory originalInventory = inventoryContainer.getInventory();
            ChannelDetails channel = originalInventory.getChannel();
            for (LocalDate localDate = startDate; localDate.isBefore(endDate) || localDate.isEqual(endDate); localDate = localDate.plusDays(1)){
                Date date = DateUtil.fromLocalDate(localDate);
                DayOfWeek dayOfWeek = localDate.getDayOfWeek();
                if(!inventoryContainer.isOverrideOperation() && inventoryService.isExistInventory(channel.getChannelid().longValue(), date) ||
                        !inventoryContainer.getInventoryWeek().containsDayOfWeek(dayOfWeek)){
                    continue;
                }
                Inventory currentInventory = createInventory(originalInventory, date);
                InventoryConflict iConflict = new InventoryConflict(currentInventory.getDate());
                inventoryService.saveOrUpdate(currentInventory, iConflict);
                inventoryContainer.addConflict(iConflict);
            }
        }
    }

    @Override
    public Inventory createInventory(Inventory originalInventory, Date date){

        Inventory inventory = inventoryService.generateInitialInventory();
        inventory.setChannel(originalInventory.getChannel());
        inventory.setDate(date);

        List<InventoryRow> newRows =  inventory.getInventoryRows();
        List<InventoryRow> originalRows = originalInventory.getInventoryRows();
        for (int i = 0; i < 24; i++) {
            InventoryRow newRow = newRows.get(i);
            InventoryRow originalRow = originalRows.get(i);

            newRow.setClusters(originalRow.getClusters());
            newRow.setTotalTvcDuration(originalRow.getTotalTvcDuration());
            newRow.setWebTvcDuration(originalRow.getWebTvcDuration());

            newRow.setLogoSpots(originalRow.getLogoSpots());
            newRow.setWebLogoSpots(originalRow.getWebLogoSpots());
            newRow.setLogoPrice(originalRow.getLogoPrice());

            newRow.setCrawlerSpots(originalRow.getCrawlerSpots());
            newRow.setWebCrawlerSpots(originalRow.getWebCrawlerSpots());
            newRow.setCrawlerPrice(originalRow.getCrawlerPrice());

            newRow.setLCrawlerSpots(originalRow.getLCrawlerSpots());
            newRow.setWebLCrawlerSpots(originalRow.getWebLCrawlerSpots());
            newRow.setlCrawlerPrice(originalRow.getlCrawlerPrice());

            List<InventoryPriceRange> newRowPriceRanges = newRow.getPriceRanges();
            List<InventoryPriceRange> originalRowPriceRanges = originalRow.getPriceRanges();
            for(int j = 0; j < originalRowPriceRanges.size(); j++){
                InventoryPriceRange newPriceRange = newRowPriceRanges.get(j);
                InventoryPriceRange originalPriceRange = originalRowPriceRanges.get(j);
                newPriceRange.setPrice(originalPriceRange.getPrice());
            }
        }
        return inventory;
    }
}
