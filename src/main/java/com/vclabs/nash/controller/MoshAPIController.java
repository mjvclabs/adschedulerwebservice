/**
 * **********************************************************************************************
 * vcLABs Intellectual Property Copyright (C) 2017 Visual Concept Labs (pvt))
 * Limited a.k.a vcLABs All Rights Reserved.
 *
 * The source code contained or described herein, comments in the source code,
 * all supporting communications including but not limited to email and phone
 * communications and all documents related to the source code ("Contents") are
 * owned by vcLABS and are intellectual properties of vcLABS. Title to the
 * Contents remain with vcLABs The Contents are protected by worldwide copyright
 * and trade secret laws and treaty provisions. The Content contain trade
 * secrets and proprietary and confidential information of vcLABs or its
 * subsidiaries, partners, suppliers and licensors. No part of the Contents may
 * be used, copied, reproduced, modified, published, uploaded, posted,
 * transmitted, distributed, or disclosed in any way.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Contents, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * expressed and approved by vcLABs board of directors in writing.
 * *********************************************************************************************************************
 */
package com.vclabs.nash.controller;

import com.vclabs.nash.model.process.MoshDetails;
import com.vclabs.nash.model.view.MoshMsg;
import com.vclabs.nash.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/mosh-api")
public class MoshAPIController {

    @Autowired
    private UserDetailsService userdetailservice;

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public MoshMsg login(@RequestParam("username") String userName, @RequestParam("password") String passWord,@RequestParam("ipAddress") String ipAddress,@RequestParam("channelId") int channnelId) {
        MoshDetails moshDetails = new MoshDetails();
        moshDetails.setPassWord(passWord);
        moshDetails.setUser(userName);
        moshDetails.setIpAddress(ipAddress);
        moshDetails.setChannelId(channnelId);
        return userdetailservice.getLoginPermission(userName, passWord, moshDetails);
    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.POST, produces = "application/json")
    public void logout(@RequestParam("id") int id) {
        MoshDetails moshDetails = new MoshDetails();
        moshDetails.setId(id);
        userdetailservice.moshUserLogout(moshDetails);
    }
}
