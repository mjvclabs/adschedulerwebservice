package com.vclabs.nash.controller;

import com.vclabs.nash.model.entity.ZeroAdsSetting;
import com.vclabs.nash.model.view.ChannelAdvertMapView;
import com.vclabs.nash.model.view.ManualChannelAdvertMapView;
import com.vclabs.nash.model.view.PlayListBackendView;
import com.vclabs.nash.model.view.ZeroAndFillerPlayListBackendView;
import com.vclabs.nash.service.ChannelAdvertMapService;
import com.vclabs.nash.service.SpotSpreadService;
import com.vclabs.nash.service.ZeroAdsSettingService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/channeladvert")
public class ChannelAdvertMapController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ChannelAdvertMapController.class);

    @Lazy
    @Autowired
    private ChannelAdvertMapService channelAdvertMapService;
    @Autowired
    private ZeroAdsSettingService zeroAdsSettingService;

    @RequestMapping(value = "/getAllChannelAdvertMap/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ChannelAdvertMapView getAllChannelAdvertMap() {
        return channelAdvertMapService.getAllChannelAdvertMap();
    }

    @RequestMapping(value = "/saveChannelAdvertMap/json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean saveChannelAdvertMap(@RequestParam("mapData") String mapData) throws Exception {
        return channelAdvertMapService.saveChannelAdvertMap(mapData);
    }

    @RequestMapping(value = "/saveAdvertMap/json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean saveAdvertMap(@RequestParam("mapData") String mapData) throws Exception {
        return channelAdvertMapService.saveAdvertMap(mapData);
    }

    @RequestMapping(value = "/getPlayList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ZeroAndFillerPlayListBackendView saveChannelAdvertMap(@RequestParam("channelId") int channelId) {
        LOGGER.info("Received request to get zero advertisements for the channel: {}", channelId);
        ZeroAndFillerPlayListBackendView result = channelAdvertMapService.getPlayList(channelId);
        LOGGER.info("Before sending zero ad response | advertId: {}, advertName: {}, playlistId: {}, status: {}, channelId: {}"
                , result.getAdvertId(), result.getAdvertName(), result.getPlaylistid(), result.getStatus(), channelId);
        return result;
    }

    @RequestMapping(value = "/clearPlayList/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean clearPlayList(@RequestParam("channelId") int channelId) {
        return channelAdvertMapService.clearPalyLis(channelId);
    }

    @RequestMapping(value = "/getZeroAdsManualPlayList", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ManualChannelAdvertMapView> getZeroAdsManualPlayList(@RequestParam("channelId") int channelId) {
        return channelAdvertMapService.getZeroAdsManualPlayList(channelId);
    }

    @RequestMapping(value = "/saveZeroAdsManualPlayList", method = RequestMethod.POST)
    public @ResponseBody
    Boolean saveZeroAdsManualPlayList(@RequestParam("channelData") String channeldata, @RequestParam("channelId") int channelId) {
        return channelAdvertMapService.saveOrUpdateZeroAdsRecorde(channelId, channeldata);
    }

    @RequestMapping(value = "/getZeroAdsSetting", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ZeroAdsSetting> getZeroAdsSetting() {
        return zeroAdsSettingService.getZeroAdsSetting();
    }

    @RequestMapping(value = "/saveZeroAdsSettingMap/json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean saveZeroAdsSettingMap(@RequestParam("mapData") String mapData) {
        try {
            return zeroAdsSettingService.saveZeroAdsSetting(mapData);
        } catch (IOException e) {
            return false;
        }
    }
}
