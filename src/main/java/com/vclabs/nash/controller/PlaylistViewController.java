package com.vclabs.nash.controller;

import com.vclabs.nash.model.process.PlayListViewInfo;
import com.vclabs.nash.service.async.ScheduleAsyncServiceImpl;
import com.vclabs.nash.service.playlist_preview.PlayListPreviewHandler;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Created by Nalaka on 2019-11-08.
 */
@RestController
@RequestMapping("/playlist-view")
public class PlaylistViewController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PlaylistViewController.class);

    @Autowired
    private PlayListPreviewHandler playListPreviewHandler;

    @ResponseBody
    @RequestMapping(value = "/generatePlayList", method = RequestMethod.POST,produces = "application/json")
    public int generatePlaylistView(HttpServletRequest request, @RequestBody PlayListViewInfo plv){
        try {
           return playListPreviewHandler.generateDailySchedulesForPreview(request,
                    plv.getChannelIds(), plv.getFromDate(), plv.getToDate(), plv.getFromTime(), plv.getToTime());
        }catch (Exception e){
            LOGGER.warn(e.getMessage());
        }
        return -1;
    }

    @RequestMapping(value = "/downloadPlaylistPreviewReport", method = RequestMethod.GET)
    public void downloadPlaylistPreviewReport(HttpServletRequest request, HttpServletResponse response, @RequestParam int seqNum) {
        playListPreviewHandler.downloadSchedulePreviewFile(response, request, seqNum);
    }
}
