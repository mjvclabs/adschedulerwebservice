package com.vclabs.nash.controller.product;

import com.vclabs.nash.model.entity.product.CodeMapping;
import com.vclabs.nash.model.entity.product.Product;

import com.vclabs.nash.model.view.product.ProductView;
import com.vclabs.nash.service.product.CodeMappingService;
import com.vclabs.nash.service.product.ProductService;
import com.vclabs.nash.service.product.data.DataExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Nalaka on 2019-08-29.
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private DataExportService dataExportService;

    @Autowired
    private CodeMappingService codeMappingService;

    @RequestMapping(value = "/allProducts/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<Product> allProductList() {
        return productService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/allProductList/page")
    public Page<ProductView> getAllProductList(@RequestParam Map<String, String> filterRequest) {
        return codeMappingService.findAllCodeMapping(filterRequest);
    }

    @RequestMapping(value = "/codeMappings/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<CodeMapping> productsByClient(@RequestParam("clientId") long clientId) {
        return codeMappingService.findByClientId(clientId);
    }

    @RequestMapping(value = "/selectedCodeMapping", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody CodeMapping selectedCodeMapping(@RequestParam("mapId") long mapId) {
        return codeMappingService.findById(mapId);
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<?> exportData() {
        dataExportService.export();
        return new ResponseEntity("{ status : 'success' }", HttpStatus.OK);
    }
}
