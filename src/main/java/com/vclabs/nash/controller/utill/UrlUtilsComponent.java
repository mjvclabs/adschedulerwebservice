package com.vclabs.nash.controller.utill;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by dperera on 25/02/2019.
 */

@Component
public class UrlUtilsComponent {

    @Value("${dashboard.port}")
    private String dashbaordPort;

    public String getDashboardRootUrl(HttpServletRequest request){
        String url = request.getRequestURL().toString();
        String[] urlParts = url.split(":");
        StringBuilder rootUrlBuilder = new StringBuilder();
        if(urlParts.length >= 1){
            rootUrlBuilder
                    .append(urlParts[0])
                    .append(":")
                    .append(urlParts[1])
                    .append(":")
                    .append(dashbaordPort);
        }
        return rootUrlBuilder.toString();
    }
}
