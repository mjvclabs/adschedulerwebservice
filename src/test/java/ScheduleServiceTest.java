import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.dto.MissedSpotMapDto;
import com.vclabs.nash.model.entity.ScheduleDef;
import com.vclabs.nash.model.view.MissedSpotView;
import com.vclabs.nash.service.SchedulerService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Nalaka on 2019-04-04.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class ScheduleServiceTest {

    @Autowired
    private SchedulerService schedulerService;
    @Autowired
    private SchedulerDAO schedulerDao;

    @Test
    public void TestallChannelsAllWorkOrders_v2() throws ParseException {
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = null;
        Date toDate = null;
        fromDate = dtFormat.parse("2018-12-01");
        toDate = dtFormat.parse("2018-12-02");
        List<ScheduleDef> list = schedulerService.allChannelsAllWorkOrders_v2(fromDate, toDate);
        List<ScheduleDef> list2 = schedulerService.allChannelsAllWorkOrders(fromDate, toDate);
        Assert.assertTrue("Correct Resullt (allChannelsAllWorkOrders)", list.size() == list2.size());
    }

    @Test
    public void TestallChannelsNotAllWorkOrders_v2() {
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = dtFormat.parse("2018-12-01");
            toDate = dtFormat.parse("2018-12-31");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<ScheduleDef> list = schedulerService.allChannelsNotAllWorkOrders_v2(1290, fromDate, toDate);
        List<ScheduleDef> list2 = schedulerService.allChannelsNotAllWorkOrders(1290, fromDate, toDate);
        Assert.assertTrue("Correct Resullt (allChannelsNotAllWorkOrders)", list.size() == list2.size());
    }

    @Test
    public void TestnotAllChannelsAllWorkOrders_v2() {
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = dtFormat.parse("2018-12-01");
            toDate = dtFormat.parse("2018-12-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<ScheduleDef> list = schedulerService.notAllChannelsAllWorkOrders_v2(7, fromDate, toDate);
        List<ScheduleDef> list2 = schedulerService.notAllChannelsAllWorkOrders(7, fromDate, toDate);
        Assert.assertTrue("Correct Resullt (notAllChannelsAllWorkOrders)", list.size() == list2.size());
    }

    @Test
    public void notAllChannelsNotAllWorkOrders_v2() {
        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = null;
        Date toDate = null;
        try {
            fromDate = dtFormat.parse("2018-12-01");
            toDate = dtFormat.parse("2018-12-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<ScheduleDef> list = schedulerService.notAllChannelsNotAllWorkOrders_v2(7, 1290, fromDate, toDate);
        List<ScheduleDef> list2 = schedulerService.notAllChannelsNotAllWorkOrders(7, 1290, fromDate, toDate);
        Assert.assertTrue("Correct Resullt (notAllChannelsNotAllWorkOrders)", list.size() == list2.size());
    }

    @Test
    public void getMissedSpotBySQL(){
        int[] intArray = new int[]{ 1,2,3,4,5,6,7,8,9,10 };
        List<MissedSpotView> missedSpotMapDtos;
        missedSpotMapDtos = schedulerService.getMissedSpot_v2("2019-01-13","2019-01-25","_3",-999, intArray,"all");
        missedSpotMapDtos.size();
    }
}
