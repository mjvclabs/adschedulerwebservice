import com.vclabs.nash.dashboard.dto.ChannelPredictionDto;
import com.vclabs.nash.model.dao.SchedulerDAO;
import com.vclabs.nash.model.entity.TimeSlotScheduleMap;
import com.vclabs.nash.service.AdvertChannelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanduni on 24/01/2019
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class AdvertDbTest {

    @Autowired
    private AdvertChannelService advertChannelService;

    @Test
    public void findAllTest() throws Exception {
        List<ChannelPredictionDto> result = advertChannelService.findChannelPredictionData();
    }
}
