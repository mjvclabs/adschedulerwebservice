import com.vclabs.nash.model.dao.TimeBeltDAO;
import com.vclabs.nash.model.entity.PlayList;
import com.vclabs.nash.model.entity.playlist_preview.PlayListTemp;
import com.vclabs.nash.model.entity.TimeBelts;
import com.vclabs.nash.service.playlist_preview.PlayListPreviewService;
import com.vclabs.nash.service.playlist_preview.PlayListTempService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class PlayListTempServiceTest {

    @Autowired
    private PlayListTempService playListTempService;

    @Autowired
    private PlayListPreviewService playListTempAsyncService;

    @Autowired
    private TimeBeltDAO timeBeltDao;

    @Test
    public void generatePlayListTest() {
        Date startDate = new Date(119, 10, 07);
        Date endDate = new Date(119, 10, 07);
        Date startTime = new Date(70, 0, 1, 0, 0, 0);
        Date endTime = new Date(70, 0, 1, 23, 0, 0);
        playListTempAsyncService.generatePlayListPreview(6, 6,startDate, endDate, startTime, endTime);
    }

    //@Test
    public void getAdvertPlayListTest() {
        Date startTime = new Date(70, 0, 1, 12, 0, 0);
        Date endTime = new Date(70, 0, 1, 13, 0, 0);
        Date date = new Date(119, 9, 16, 0, 0, 0);
        List<PlayList> advertList = playListTempService.getAdvertPlayList(6, date, startTime, endTime, 12, 5 );
        Assert.assertNotNull(advertList);
    }

    //@Test
    public void getPlayListCrawlerTest() {
        Date startTime = new Date(70, 0, 1, 12, 0, 0);
        Date endTime = new Date(70, 0, 1, 13, 0, 0);
        Date date = new Date(119, 9, 10, 0, 0, 0);
        List<PlayList> advertList = playListTempService.getPlayListCrawler(6, date, startTime, endTime, 12, 4 );
        Assert.assertNotNull(advertList);
    }

    //@Test
    public void getPlayListLogoTest() {
        Date startTime = new Date(70, 0, 1, 12, 0, 0);
        Date endTime = new Date(70, 0, 1, 13, 0, 0);
        Date date = new Date(119, 9, 10, 0, 0, 0);
        List<PlayList> advertList = playListTempService.getPlayListLogo(6, date, startTime, endTime, 12, 4 );
        Assert.assertNotNull(advertList);
    }


    //@Test
    public void generatePlayListBackEndTest(){
        Date startTime = new Date(70, 0, 1, 18, 0, 0);
        Date endTime = new Date(70, 0, 1, 19, 0, 0);
        Date date = new Date(119, 9, 16, 0, 0, 0);
        List<TimeBelts> timeBelts = timeBeltDao.getTimeBelts(6, startTime, endTime);
        playListTempService.generatePlayListBackEnd(6, date, timeBelts.get(0), 18, 5);
        List<PlayList> advertList = playListTempService.getAdvertPlayList(6, date, startTime, endTime, 18, 5 );
        Assert.assertNotNull(advertList);
    }


    //@Test
    public void getSchedulesByDateTest(){
        Date date = new Date(119, 10, 6, 0, 0, 0);
        List<PlayListTemp>  playLists = playListTempService.getSchedulesByDate(6, date, 18);
        Assert.assertNotNull(playLists);
    }
}
