import com.vclabs.nash.service.inventoryprediction.InventorySyncService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Nalaka on 2018-10-15.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class InventorySyncServiceTest {

    @Autowired
    private InventorySyncService inventorySyncService;

    @Test
    public void dateTimeValidationTest() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        Date dateAndTime = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateAndTime);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, 1);
        Date startDate = calendar.getTime();
        String st = simpleDateFormat.format(startDate);
        String sn = simpleDateFormat.format(new Date());
        long t = startDate.getTime();
        long n = new Date().getTime();
        Assert.assertEquals(true, startDate.before(new Date()));
    }

    @Test
    public void syncPredictionInventoryWithSystemInventoryTest() {
        inventorySyncService.syncPredictionInventoryWithSystemInventory();
    }
}
