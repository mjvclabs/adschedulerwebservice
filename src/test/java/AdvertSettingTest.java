import com.vclabs.nash.model.entity.AdvertSetting;
import com.vclabs.nash.service.AdvertSettingService;
import com.vclabs.nash.service.ChannelDetailService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class AdvertSettingTest {

    @Autowired
    private ChannelDetailService channelDetailService;

    @Autowired
    private AdvertSettingService advertSettingService;

    @Test
    public void saveAdvertSettingTest(){
        AdvertSetting advertSetting = new AdvertSetting();
        advertSetting.setChannelDetails(channelDetailService.getChannel(5));
        advertSetting.setMaxLimit(61);
        advertSetting.setMinLimit(90);
        advertSetting.setMaxSlots(5);
        advertSettingService.save(advertSetting);
    }

    @Test
    public void finMaxAllowedSlots(){
        int maxSlots = advertSettingService.finMaxAllowedSlots(5, 30);
        Assert.assertEquals("Max allowed slots should be 5 for 30s adverts",5L, maxSlots);
    }

}
