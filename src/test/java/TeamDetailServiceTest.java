import com.vclabs.nash.model.entity.SecurityUsersEntity;
import com.vclabs.nash.model.entity.teams.TeamDetail;
import com.vclabs.nash.service.UserDetailsService;
import com.vclabs.nash.service.teams.TeamDetailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.Set;

/**
 * Created by Sanduni on 03/01/2019
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class TeamDetailServiceTest {

    @Autowired
    private TeamDetailService teamDetailService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Test
    public void saveTest() throws Exception {

        TeamDetail teamDetail = new TeamDetail();
         Set<SecurityUsersEntity> admins = (Set<SecurityUsersEntity>)userDetailsService.getAllUsers();
        teamDetail.setAdmins(admins);
        teamDetail.setMembers(admins);
        teamDetail.setName("test team 1");
        teamDetailService.save(teamDetail);
    }

    @Test
    public void findOneTest() throws Exception {
        TeamDetail teamDetail = teamDetailService.findById(Integer.valueOf(1));
        teamDetail.getAdmins();
    }

    @Test
    public void deleteTest() throws Exception {
        teamDetailService.deleteTeamMember(1, "Dananja");
    }


    @Test
    public void findTeamDetailsTest() throws Exception {
        TeamDetail teamDetail = teamDetailService.findById(1);
        teamDetail.getAdmins();
    }

    @Test
    public void findMembersTest() throws Exception {
        teamDetailService.findAllTeamMembers(1);
    }

    @Test
    public void myTeamToDoList() throws Exception {
        teamDetailService.getTeamMembersWODetail();
    }

    @Test
    public void checkTeamNameTest(){
        teamDetailService.checkTeamName("Test one");
    }
}
