import com.vclabs.nash.service.ChannelDetailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Sanduni on 04/10/2018
 */

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class ChannelDetailServiceTest {

    @Autowired
    private ChannelDetailService channelDetailService;

    @Test
    public void createChannelTest(){
        channelDetailService.createNewChannel("Test Channel Name");
    }

    @Test
    public void getAllChannelsTest(){
        channelDetailService.getAllChannels();
    }
}
