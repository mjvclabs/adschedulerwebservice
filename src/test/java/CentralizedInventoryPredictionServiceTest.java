import com.vclabs.nash.model.entity.inventoryprediction.InventoryDailyPrediction;
import com.vclabs.nash.service.inventoryprediction.CentralizedInventoryPredictionService;
import com.vclabs.nash.service.inventoryprediction.InventoryPredictionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Sanduni on 11/10/2018
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class CentralizedInventoryPredictionServiceTest {

    @Autowired
    private CentralizedInventoryPredictionService centralizedInventoryPredictionService;

    @Autowired
    private InventoryPredictionService inventoryPredictionService;

    @Test
    public void centralizedIPS() {
        InventoryDailyPrediction inventoryDailyPrediction = inventoryPredictionService.findDailyPredictionById(1L);
        centralizedInventoryPredictionService.overrideByDailyPrediction(inventoryDailyPrediction);
    }

    @Test
    public void overrideByWeeklyPredictionTest() {
        centralizedInventoryPredictionService.overrideByWeeklyPrediction();
    }

    @Test
    public void overrideByDailyPredictionTest() {
        centralizedInventoryPredictionService.saveDailyPredictionsInCentralizedInventory();
    }

    @Test
    public void cronJobTest() {
        centralizedInventoryPredictionService.syncPredicteDataWithCentralizedInventory();
    }

    @Test
    public void findUpdatedListTest() {
        int size = centralizedInventoryPredictionService.findUpdatedInventory().size();
        Assert.assertEquals(0,size);
    }

    @Test
    public void dateTest(){
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        simpleDateFormat.setTimeZone(timeZone);

        System.out.println("Time zone: " + timeZone.getID());
        System.out.println("default time zone: " + TimeZone.getDefault().getID());
        System.out.println();

        System.out.println("UTC:     " + simpleDateFormat.format(calendar.getTime()));
        System.out.println("Default: " + calendar.getTime());
    }
}
