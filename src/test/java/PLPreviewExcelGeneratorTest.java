import com.vclabs.nash.service.playlist_preview.PLPreviewExcelGenerator;
import com.vclabs.nash.service.playlist_preview.PlayListPreviewHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;
import java.util.concurrent.ExecutionException;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class PLPreviewExcelGeneratorTest {

    @Autowired
    private PLPreviewExcelGenerator plPreviewExcelGenerator;

    @Autowired
    private PlayListPreviewHandler playListPreviewHandler;

    @Test
    public void generateSchedulePreviewExcelTest() throws ExecutionException, InterruptedException {
        Date startDate = new Date(119, 10, 11);
        Date endDate = new Date(119, 10, 12);
        Date startTime = new Date(70, 0, 1, 0, 0, 0);
        Date endTime = new Date(70, 0, 1, 23, 0, 0);
        List<Integer> channels = new ArrayList<>();
        channels.add(6);
        channels.add(7);
        playListPreviewHandler.generateDailySchedulesForPreview(null, channels, startDate, endDate, startTime, endTime );
    }

}
