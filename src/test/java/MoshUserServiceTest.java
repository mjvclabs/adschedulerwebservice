package com.vclabs.nash;

import com.vclabs.nash.service.report.MoshUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.text.ParseException;

/**
 * Created by Nalaka on 2019-05-15.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class MoshUserServiceTest {

    @Autowired
    private MoshUserService moshUserService;

    @Test
    public void getUserLogdataTest() {
        moshUserService.getUserLogdata("");
    }
}