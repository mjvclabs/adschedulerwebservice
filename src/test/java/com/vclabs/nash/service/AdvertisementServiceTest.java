package com.vclabs.nash.service;

import com.vclabs.nash.RootApplicationConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Sanduni on 09/11/2018
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class AdvertisementServiceTest {

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private ChannelAdvertMapService channelAdvertMapService;

    @Test
    public void findDummyCutsForDashboardTest(){
        advertisementService.findDummyCutsForDashboard();
    }

    @Test
    public void getSpotCountTest(){
        advertisementService.getSpotCountOfDummyCuts(2);
    }

    @Test
    public void createDummyCutListForDashboardWidgetTest(){
        advertisementService.createDummyCutListForDashboardWidget();
    }

    @Test
    public void priorityAdvertisementTest() throws Exception {
        advertisementService.findAdvertsForPriorityAdvertisementWidget();
    }

    @Test
    public void removeSuspendOrExpireAdverFromZeroAdsTest(){
        channelAdvertMapService.removeSuspendOrExpireAdverFromZeroAds(7999);
    }
}
