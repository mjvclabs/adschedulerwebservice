package com.vclabs.nash.service;

import com.vclabs.nash.RootApplicationConfiguration;
import com.vclabs.nash.service.inventory.NashInventoryConsumptionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Sanduni on 12/11/2018
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class NashInventoryConsumptionServiceTest {

    @Autowired
    private NashInventoryConsumptionService nashInventoryConsumptionService;

    @Test
    public void findHighestInventoryUtilizationTest(){
        nashInventoryConsumptionService.findHighestInventoryUtilizedChannels();
    }

    @Test
    public void findChannelUtillizationDetailsTest(){
        nashInventoryConsumptionService.findChannelUtillizationDetails();
    }
}
