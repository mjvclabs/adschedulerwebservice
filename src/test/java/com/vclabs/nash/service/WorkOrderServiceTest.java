package com.vclabs.nash.service;

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.RootApplicationConfiguration;
import com.vclabs.nash.dashboard.NashDMConverterName;
import com.vclabs.nash.dashboard.dto.*;
import com.vclabs.nash.model.entity.WorkOrder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by Nalaka on 2018-11-07.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class WorkOrderServiceTest {

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private WorkOrderService workOrderService;

    @Test
    public void findPenddingWorkOrderByUserTest() {
        //if you wata all user set parameter as ALL
        List<WorkOrder> workOrderList = workOrderService.findPenddingWorkOrderByUser("nirushan_02495");
        Assert.assertEquals("Selected work order list size should be 142", 142, workOrderList.size());
    }

    @Test
    public void findPenddingWorkOrderByRevenueMonthTest() {
        //if you wata all month set parameter as ALL
        List<WorkOrder> workOrderList = workOrderService.findPenddingWorkOrderByRevenueMonth("October");
        Assert.assertEquals("Selected work order list size should be 2", 2, workOrderList.size());
    }

    @Test
    public void findCompletedSchedulesWorkOrderByUserTest() {
        //if you wata all month set parameter as ALL
        List<WorkOrder> workOrderList = workOrderService.findCompletedSchedulesWorkOrderByUser("nirushan_02495");
        Assert.assertEquals("Selected work order list size should be 47", 47, workOrderList.size());
    }

   @Test
    public void convertAndSendTest(){
        dashboardManager.convertAndSend(NashDMConverterName.MY_TO_DO_LIST,"dashboard.myToDoList.andrea",workOrderService.findPenddingWorkOrderByUser("SU"));
    }

    @Test
    public void findNewWorkOrdersTest() {
        workOrderService.findNewWorkOrders("andrea");
    }

    @Test
    public void findRevisedWorkOrderTest() {
        List<WorkOrder> workOrderList = workOrderService.findRevisedWorkOrders();
    }

    @Test
    public void createRevisedWOListTest() {
        List<RevisedWODto> revisedWODtos = workOrderService.createRevisedWOList();
    }

    @Test
    public void findSchedulesReadyForInvoicingTest() {
        List<SchedulesReadyForInvoicingDto> workOrderList = workOrderService.findSchedulesReadyForInvoicing();
    }

    @Test
    public void findPartiallyEnteredWOsTest() {
        List<PartiallyEnteredWODto> dtos = workOrderService.findPartiallyEnteredWOs();
        Assert.assertEquals("test", 0, dtos.size());
    }

    @Test
    public void getTotalRevenueForMonthBySellerTest() {
        double totalRevenue = workOrderService.getTotalRevenueForMonthBySeller("August", "ashan_08131");
        Assert.assertEquals((double) 360000, totalRevenue, 0.1);
    }

    @Test
    public void getMyRecordedRevenueTest() throws Exception {
        List<MyRecordedRevenueDto> recordedRevenueDtos = workOrderService.getMyRecordedRevenue("andrea");
    }

    @Test
    public void getSelecteUserAllWorkOrderTest() {
        List<SalesRevenueDTO> salesRevenueDTOS = workOrderService.findSelecteUserAllWorkOrder("ashan_08131");
        Assert.assertEquals(60, salesRevenueDTOS.size());
    }
}
