package com.vclabs.nash.service;

import com.vclabs.nash.RootApplicationConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Nalaka on 2019-11-21.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class AdvertSettingServiceTest {

    @Autowired
    private AdvertSettingService advertSettingService;

    @Test
    public void getMaxLapDuration(){
        System.out.println("OK");
        advertSettingService.getMaxLapDuration(6);
    }
}
