package com.vclabs.nash.service;

/**
 * Created by Nalaka on 2018-11-08.
*/

import com.vclabs.dashboard.core.service.DashboardManager;
import com.vclabs.nash.RootApplicationConfiguration;
import com.vclabs.nash.model.entity.ScheduleDef;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.List;

/**
 * Created by Sanduni on 08/11/2018
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class SchedulerServiceTest {

    @Autowired
    private SchedulerService schedulerService;
    @Autowired
    private DashboardManager dashboardManager;

    @Test
    public void findCurrentDateAllValidSpotTest() {
        List<ScheduleDef> scheduleDefList = schedulerService.findCurrentDateAllValidSpot();
        Assert.assertTrue("Current All Spot array size can't be zero", !scheduleDefList.isEmpty());
    }

    @Test
    public void findCurrentDateAllMissedSpotTest() {
        List<ScheduleDef> scheduleDefList = schedulerService.findCurrentDateAllMissedSpot();
        Assert.assertTrue("Curent All missed spot array size can't be zero", !scheduleDefList.isEmpty());
    }

    @Test
    public void findCurrentDateAllAiredSpotTest() {
        List<ScheduleDef> scheduleDefList = schedulerService.findCurrentDateAllAiredSpot(7);
        Assert.assertTrue("Curent All aired spot array size can't be zero", !scheduleDefList.isEmpty());
    }

    @Test
    public void getMissedAndAiredSpotsTest(){
        schedulerService.findAllMissedAndAiredSpots(new Date(), 6);
    }

    @Test
    public void findDailyMissedSpotsPercentagesTest() throws Exception {
        schedulerService.findDailyMissedSpotsPercentages();
    }

    @Test
    public void getAllSpotsTest(){
        schedulerService.findAllScheduleSpots();
    }

    @Test
    public void clearPlayListTest() throws Exception {
        schedulerService.clearPlaylist();
    }
}

