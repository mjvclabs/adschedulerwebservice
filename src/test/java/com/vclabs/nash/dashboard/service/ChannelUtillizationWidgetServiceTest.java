package com.vclabs.nash.dashboard.service;

import com.vclabs.nash.RootApplicationConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Nalaka on 2018-11-21.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class ChannelUtillizationWidgetServiceTest {

    @Autowired
    private ChannelUtillizationWidgetService channelUtillizationWidgetService;

    @Test
    public void setChannelUtilizationDataTest(){
        channelUtillizationWidgetService.setChannelUtilizationData();
    }
}
