package com.vclabs.nash.dashboard.service;

import com.vclabs.nash.RootApplicationConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Nalaka on 2019-01-14.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class MyTeamSalesRevenueWidgetServiceTest {

    @Autowired
    MyTeamSalesRevenueService myTeamSalesRevenueService;

    @Test
    public void setMyTeamSalesRevenueWidgetTest(){
        myTeamSalesRevenueService.setMyTeamSalesRevenueData();
    }
}
