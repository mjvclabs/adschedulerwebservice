package com.vclabs.nash.dashboard.service;

import com.vclabs.nash.RootApplicationConfiguration;
import com.vclabs.nash.dashboard.service.impl.MyToDoListWidgetServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Sanduni on 29/11/2018
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class DashboardWidgetServiceTest {

    @Autowired
    private MyToDoListWidgetService myToDoListWidgetServiceImpl;

    @Autowired
    private HighTrafficWidgetService highTrafficWidgetService;

    @Autowired
    private NewWorkOrderWidgetService newWorkOrderWidgetService;

    @Autowired
    private MyRecordedRevenueWidgetService myRecordedRevenueWidgetService;

    @Autowired
    private PartiallyEnteredWorkOrderWidgetService partiallyEnteredWorkOrderWidgetService;

    @Autowired
    private RevisedWorkOrderMaximizeWidgetService revisedWorkOrderMaximizeWidgetService;

    @Autowired
    private PendingWorkOrderWidgetService pendingWorkOrderWidgetService;

    @Autowired
    private SchedulesReadyForInvoicingWidgetService schedulesReadyForInvoicingWidgetService;

    @Autowired
    private MyCompleteSchedulesWidgetService myCompleteSchedulesWidgetService;

    @Autowired
    private IngestStatusWidgetService ingestStatusWidgetService;

    @Autowired
    private PriorityAdvertisementWidgetService priorityAdvertisementWidgetService;

    @Autowired
    private TotalDailyMissedSpotUpdateService totalDailyMissedSpotUpdateService;

    @Autowired
    private MyTeamToDoListWidgetService myTeamToDoListWidgetService;

    @Test
    public void myToDoListWidgetServiceTest() throws Exception {
        myToDoListWidgetServiceImpl.setMyToDoListData();
    }

    @Test
    public void highTrafficWidgetServiceTest(){
        highTrafficWidgetService.setHighTrafficData();
    }

    @Test
    public void setNewWorkOrderWidgetTest(){
        newWorkOrderWidgetService.setNewWorkOrderData();
    }

    @Test
    public void partiallyEnteredWOServiceTest(){
        partiallyEnteredWorkOrderWidgetService.setPartiallyEnteredWOData();
    }

    @Test
    public void revisedWorkOrderTest(){
        revisedWorkOrderMaximizeWidgetService.setRevisedWorkOrderData();
    }

    @Test
    public void pendingWorkOrderTest() {
        pendingWorkOrderWidgetService.setPendingWorkOrderData();
    }

    @Test
    public void myRecordedRevenueTest() {
        myRecordedRevenueWidgetService.setMyRecordedRevenueData();
    }

    @Test
    public void schedulesReadyForInvoicingTest() {
        schedulesReadyForInvoicingWidgetService.setSchedulesReadyForInvoicingData();
    }

    @Test
    public void myCompletedSchedulesTest() throws Exception {
        myCompleteSchedulesWidgetService.setMyCompleteSchedulesData();
    }

    @Test
    public void ingestStatusTest() throws Exception {
        ingestStatusWidgetService.setIngestStatusWidgetData();
    }

    @Test
    public void priorityAdvertisementTest() throws Exception {
        priorityAdvertisementWidgetService.setPriorityAdvertisementWidgetData();
    }

    @Test
    public void dailyMissedSpotUpdateTest() throws Exception {
        totalDailyMissedSpotUpdateService.setDailyMissedSpotUpdateData();
    }

    @Test
    public void myTeamToDoListTest() throws Exception {
        myTeamToDoListWidgetService.setMyTeamToDoListData();
    }
}
