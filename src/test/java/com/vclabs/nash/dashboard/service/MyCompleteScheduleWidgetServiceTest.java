package com.vclabs.nash.dashboard.service;

import com.vclabs.nash.RootApplicationConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Sanduni on 06/02/2019
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class MyCompleteScheduleWidgetServiceTest {

    @Autowired
    private MyCompleteSchedulesWidgetService myCompleteSchedulesWidgetService;

    @Test
    public void mycompleteschedules() throws Exception {
        myCompleteSchedulesWidgetService.setMyCompleteSchedulesData();
    }
}
