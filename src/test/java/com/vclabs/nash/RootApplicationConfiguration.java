package com.vclabs.nash;

import com.vclabs.nash.configuration.DevEnvConfiguration;
import com.vclabs.nash.configuration.DevJPAConfiguration;
import com.vclabs.nash.configuration.WebApplicationConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by Nalaka on 2018-11-07.
 */
@Import({ DevEnvConfiguration.class, WebApplicationConfiguration.class,
        DevJPAConfiguration.class })
@PropertySource(value = "classpath:db.properties")
public class   RootApplicationConfiguration {

    //    public static String TMP_UPLOAD_FOLDER_PATH = "E:\\home\\smbuser\\sambashare"; //local environment
    //public static String TMP_UPLOAD_FOLDER_PATH = "/home/vcl/nash/home/smbuser/sambashare"; //staging environment
    public static String TMP_UPLOAD_FOLDER_PATH = "/home/smbuser/sambashare"; // DTV Testing environment

}
