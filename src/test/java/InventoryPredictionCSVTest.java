import com.vclabs.nash.service.exception.PredicationConfigException;
import com.vclabs.nash.service.inventoryprediction.PredicationConfigurationFileHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Sanduni on 04/10/2018
 */

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class InventoryPredictionCSVTest {

    @Autowired
    private PredicationConfigurationFileHandler predicationConfigurationFileHandler;

    @Test
    public void validateCSVFileTest(){
        try {
            predicationConfigurationFileHandler.copyConfigurationFile();
        } catch (PredicationConfigException e) {
            e.printStackTrace();
        }
    }

}
