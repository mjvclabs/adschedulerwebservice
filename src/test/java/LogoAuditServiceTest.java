import com.vclabs.nash.model.entity.PlayList;
import com.vclabs.nash.service.LogoAuditService;
import com.vclabs.nash.service.PlayListService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Nalaka on 2019-01-30.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class LogoAuditServiceTest {

    @Autowired
    private LogoAuditService logoAuditService;

    @Autowired
    private PlayListService playListService;

    @Test
    public void setSelectedPlaylistItemStatusTest(){
        PlayList playList=playListService.getSelectedPlayList(2325109);
        logoAuditService.setSelectedPlaylistItemStatus(playList);
    }
}
