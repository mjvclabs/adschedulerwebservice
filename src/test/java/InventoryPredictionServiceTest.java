import com.vclabs.nash.model.entity.inventoryprediction.InventoryMonthlyPrediction;
import com.vclabs.nash.model.entity.inventoryprediction.InventoryWeeklyPrediction;
import com.vclabs.nash.service.inventoryprediction.InventoryPredictionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by Sanduni on 08/10/2018
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class InventoryPredictionServiceTest {

    @Autowired
    private InventoryPredictionService inventoryPredictionService;

    @Test
    public void getLatestWeeklyInventoryTest(){
        List<InventoryWeeklyPrediction> weeklyList=inventoryPredictionService.getLatestWeeklyInventory();
        Assert.assertEquals(0,weeklyList.size()%168);
    }

    @Test
    public void updateWeeklyPredictionResultMeta(){
        inventoryPredictionService.updateWeeklyPredictionResultMeta();
    }

    @Test
    public void getLatestMonthlyInventoryTest(){
        List<InventoryMonthlyPrediction> MonthlyList=inventoryPredictionService.getLatestMonthlyInventory();
        Assert.assertEquals(0,MonthlyList.size()%672);
    }
}
