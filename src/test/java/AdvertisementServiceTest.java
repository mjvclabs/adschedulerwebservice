import com.vclabs.nash.RootApplicationConfiguration;
import com.vclabs.nash.service.AdvertisementService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Nalaka on 2019-03-11.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class AdvertisementServiceTest {

    @Autowired
    private AdvertisementService advertisementService;

    @Test
    public void checkAdvertCategoryTest(){
        advertisementService.checkAdvertisementCategoryName("Food");
    }
}
