import com.vclabs.nash.model.entity.ChannelDetails;
import com.vclabs.nash.model.entity.inventory.Inventory;
import com.vclabs.nash.model.entity.inventory.InventoryPriceRange;
import com.vclabs.nash.model.entity.inventory.InventoryRow;
import com.vclabs.nash.service.ChannelDetailService;
import com.vclabs.nash.service.inventory.InventoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootApplicationConfiguration.class)
public class InventoryServiceTest {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private ChannelDetailService channelDetailService;

    //@Autowired
    //private DealEntryAllocationRepository repository;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(InventoryServiceTest.class);

    //@Test
    public void testInventoryDateRage() {
        inventoryService.findInventoryExistDates(6,new Date(),new Date());
    }

    @Test
    public void testSaveDefaultInventory() {
        List<ChannelDetails> channels = channelDetailService.ChannelListOrderByName();
        for(ChannelDetails channel: channels){
            Inventory inventory = inventoryService.generateInitialInventory();
            //channel.
            inventory.setChannel(channel);
            setTimeBeltPrice(inventory);
            inventoryService.save(inventory);
        }
    }

    private void setTimeBeltPrice(Inventory inventory){
        for(InventoryRow row : inventory.getInventoryRows()){
            row.setTotalTvcDurationMinutes(12);  //12
            row.setWebTvcDurationPercentage(50); //50
            row.setLogoPrice(new BigDecimal(3000)); //3000
            row.setCrawlerPrice(new BigDecimal(1500)); //1500
            row.setCrawlerSpots(4);
            row.setWebCrawlerSpots(2);
            row.setLCrawlerSpots(4);
            row.setWebLCrawlerSpots(2);
            row.setlCrawlerPrice(new BigDecimal(1500));
            row.setLogoSpots(2);
            row.setWebLogoSpots(1);
            row.setClusters(3);
            int i = 0;
            for(InventoryPriceRange priceRange : row.getPriceRanges()){
                priceRange.setPrice(getPriceByTimeRange(row.getFromTime(), row.getToTime(), i));
                i++;
            }
        }
    }

    private BigDecimal getPriceByTimeRange(Time fromTime, Time toTime, int i){

        Time time_00 = new Time(0,0,0);
        Time time_09 = new Time(9,0,0);
        Time time_17 = new Time(17,0,0);
        Time time_19 = new Time(19,0,0);
        Time time_21 = new Time(19,0,0);
        Time time_22 = new Time(22,0,0);

        BigDecimal price = new BigDecimal(0);
        if(time_00.getTime() <= fromTime.getTime() && toTime.getTime() <= time_09.getTime()){
            return price;
        }else if(time_09.getTime() <= fromTime.getTime() && toTime.getTime() <= time_17.getTime()){
            switch (i){
                case 0:
                    price = new BigDecimal(100);
                    break;
                case 1:
                    price = new BigDecimal(200);
                    break;
                case 2:
                    price = new BigDecimal(900);
                    break;
            }
            return price;
        }else if(time_17.getTime() <= fromTime.getTime() && toTime.getTime() <= time_19.getTime()){
            switch (i){
                case 0:
                    price = new BigDecimal(3000);
                    break;
                case 1:
                    price = new BigDecimal(3500);
                    break;
                case 2:
                    price = new BigDecimal(6500);
                    break;
            }
            return price;
        }else if(time_19.getTime() <= fromTime.getTime() && toTime.getTime() <= time_21.getTime()){
            switch (i){
                case 0:
                    price = new BigDecimal(5000);
                    break;
                case 1:
                    price = new BigDecimal(5900);
                    break;
                case 2:
                    price = new BigDecimal(7500);
                    break;
            }
            return price;
        }else if(time_21.getTime() <= fromTime.getTime() && toTime.getTime() <= time_22.getTime()){
            switch (i){
                case 0:
                    price = new BigDecimal(3000);
                    break;
                case 1:
                    price = new BigDecimal(3500);
                    break;
                case 2:
                    price = new BigDecimal(6500);
                    break;
            }
            return price;
        }
        return price;
    }
}
